#include <iostream>
#include <vector>
#include <queue>
using namespace std;


//class Solution {
//public:
//    vector<int> maxAltitude(vector<int>& heights, int limit) {
//        vector<int> ret;
//        deque<int> q;   //这里用来存放递减数列
//        int left = 1 - limit, right = 0, n = heights.size();
//        while (right < n)
//        {
//            //如果队列中的头部元素是left的前一个位置的元素，需要将其出列
//            if (left >= 1 && q.front() == heights[left - 1]) q.pop_front();
//            //当前right的数据是大于队首的数据，就需要将队列中的所有数据进行出队
//            while (q.size() > 0 && heights[right] > q.front()) q.pop_front();
//            //如果当前元素是大于队尾元素的，就需要把队尾元素出队
//            while (q.size() > 0 && heights[right] > q.back()) q.pop_back();
//            //将当前元素添加到队列的尾部
//            q.push_back(heights[right]);
//            //如果已经形成了合法窗口就将队列中的头部元素添加到ret中
//            if (left >= 0) ret.push_back(q.front());
//            //更新窗口
//            ++left;
//            ++right;
//        }
//        return ret;
//    }
//};



//class Checkout {
//public:
//    int get_max() {
//        return dq.size() > 0 ? dq.front() : -1;
//    }
//
//    void add(int value) {
//        q.push(value);
//        //如果dq的头部节点小于val，就需要把的全部元素都pop，然后再入val
//        while (dq.size() > 0 && dq.front() < value) dq.pop_front();
//        //如果dq的后面的节点小于val，就要把后面的节点都pop出来
//        while (dq.size() > 0 && dq.back() < value) dq.pop_back();
//        dq.push_back(value);
//    }
//
//    int remove() {
//        //如果dq的头部元素和q中的头部元素相同就都需要pop
//        if (q.size() == 0) return -1;
//        if (dq.front() == q.front()) dq.pop_front();
//        int ret = q.front();
//        q.pop();
//        return ret;
//    }
//private:
//    queue<int> q;  //用来保存数据
//    deque<int> dq; //用来保证当前保存的数据中的非严格递减数据
//};


//class Solution {
//public:
//    vector<int> statisticalResult(vector<int>& arrayA) {
//        //开辟两个数组  -- 前缀积 、 后缀积
//        int n = arrayA.size();
//        vector<int> frontmul(n, 1), backmul(n, 1);
//        //初始化数组
//        for (int i = 1; i < n; ++i)
//        {
//            frontmul[i] = frontmul[i - 1] * arrayA[i - 1];
//        }
//        for (int i = n - 2; i >= 0; --i)
//        {
//            backmul[i] = backmul[i + 1] * arrayA[i + 1];
//        }
//        vector<int> ret(n);
//        for (int i = 0; i < n; ++i)
//        {
//            ret[i] = frontmul[i] * backmul[i];
//        }
//        return ret;
//    }
//};


