#include <iostream>
#include <vector>
using namespace std;


//class Solution {
//    vector<int> tmp;
//public:
//    int reversePairs(vector<int>& nums) {
//        int n = nums.size();
//        tmp.resize(n);
//        int ret = MergeSort(nums, 0, n - 1);
//        return ret;
//    }
//    int MergeSort(vector<int>& nums, int left, int right)
//    {
//        //返回
//        if (left >= right) return 0;
//        int mid = left + (right - left) / 2;
//        //[left,mid] [mid+1,right] 
//        //左右找并排序
//        int ret = 0;
//        ret += MergeSort(nums, left, mid);
//        ret += MergeSort(nums, mid + 1, right);
//        //先找翻转对
//        //采用升序解决
//        int cur1 = left, cur2 = mid + 1;
//        while (cur2 <= right)
//        {
//            while (cur1 <= mid && nums[cur1] / 2.0 <= nums[cur2]) ++cur1;
//            //考虑越界情况
//            if (cur1 > mid) break;
//            //收集结果
//            ret += (mid - cur1 + 1);
//            ++cur2;
//        }
//        //后排序
//        cur1 = left; cur2 = mid + 1;
//        int i = left;
//        while (cur1 <= mid && cur2 <= right)
//        {
//            if (nums[cur1] > nums[cur2]) tmp[i++] = nums[cur2++];
//            else tmp[i++] = nums[cur1++];
//        }
//        //处理后面没有排序的数据
//        while (cur1 <= mid) tmp[i++] = nums[cur1++];
//        while (cur2 <= right) tmp[i++] = nums[cur2++];
//        //还原
//        for (int j = left; j <= right; ++j) nums[j] = tmp[j];
//        return ret;
//    }
//};
//
//int main()
//{
//    vector<int> nums = { 1,3,2,3,1 };
//    int ret = Solution().reversePairs(nums);
//    cout << ret << endl;
//    return 0;
//}

//class Solution {
//    int tmp[50010];
//public:
//    int reversePairs(vector<int>& nums) {
//        int n = nums.size();
//        int ret = MergeSort(nums, 0, n - 1);
//        return ret;
//    }
//    int MergeSort(vector<int>& nums, int left, int right)
//    {
//        //返回
//        if (left >= right) return 0;
//        int mid = left + (right - left) / 2;
//        //[left,mid] [mid+1,right] 
//        //左右找并排序
//        int ret = 0;
//        ret += MergeSort(nums, left, mid);
//        ret += MergeSort(nums, mid + 1, right);
//        //先找翻转对
//        //采用降序解决
//        int cur1 = left, cur2 = mid + 1;
//        while (cur1 <= mid)
//        {
//            while (cur2 <= right && nums[cur1] / 2.0 <= nums[cur2]) ++cur2;
//            //考虑越界情况
//            if (cur2 > right) break;
//            //收集结果
//            ret += (right - cur2 + 1);
//            ++cur1;
//        }
//        //后排序
//        cur1 = left; cur2 = mid + 1;
//        int i = left;
//        while (cur1 <= mid && cur2 <= right)
//        {
//            if (nums[cur1] > nums[cur2]) tmp[i++] = nums[cur1++];
//            else tmp[i++] = nums[cur2++];
//        }
//        //处理后面没有排序的数据
//        while (cur1 <= mid) tmp[i++] = nums[cur1++];
//        while (cur2 <= right) tmp[i++] = nums[cur2++];
//        //还原
//        for (int j = left; j <= right; ++j) nums[j] = tmp[j];
//        return ret;
//    }
//};


//class Solution {
//public:
//    ListNode* addTwoNumbers(ListNode* l1, ListNode* l2) {
//        ListNode* grand = new ListNode(); //哨兵位头节点
//        ListNode* tail = grand;
//        int carry = 0; //进位
//        while (l1 && l2)
//        {
//            int sum = (carry + l1->val + l2->val);
//            carry = sum >= 10 ? 1 : 0;
//            sum %= 10;
//            tail->next = new ListNode(sum);
//            tail = tail->next;
//            l1 = l1->next;
//            l2 = l2->next;
//        }
//        //处理一个链表过长的情况
//        while (l1)
//        {
//            int sum = carry + l1->val;
//            carry = sum >= 10 ? 1 : 0;
//            sum %= 10;
//            tail->next = new ListNode(sum);
//            tail = tail->next;
//            l1 = l1->next;
//        }
//        while (l2)
//        {
//            int sum = carry + l2->val;
//            carry = sum >= 10 ? 1 : 0;
//            sum %= 10;
//            tail->next = new ListNode(sum);
//            tail = tail->next;
//            l2 = l2->next;
//        }
//        //处理最后carry=1
//        if (carry == 1)
//        {
//            tail->next = new ListNode(1);
//        }
//        ListNode* head = grand->next;
//        delete grand;
//        return head;
//    }
//};

//struct ListNode {
//    int val;
//    ListNode* next;
//    ListNode() : val(0), next(nullptr) {}
//    ListNode(int x) : val(x), next(nullptr) {}
//    ListNode(int x, ListNode* next) : val(x), next(next) {}
//};
//
//
//class Solution {
//public:
//    ListNode* swapPairs(ListNode* head) {
//        if (!head || !head->next) return head;
//        ListNode* pre = head;
//        ListNode* cur = pre->next;
//        ListNode* tail = cur->next;
//        head = cur; //更新头
//        while (tail && tail->next) //分别对应奇数个和偶数个的情况
//        {
//            //修改指向
//            cur->next = pre;
//            pre->next = tail->next;
//            //更新
//            cur = tail->next;
//            pre = tail;
//            tail = cur->next;
//        }
//        //处理最后一次
//        cur->next = pre;
//        pre->next = tail;
//        return head;
//    }
//};
//
//int main()
//{
//    ListNode* n1 = new ListNode(1);
//    ListNode* n2 = new ListNode(2);
//    ListNode* n3 = new ListNode(3);
//    ListNode* n4 = new ListNode(4);
//    n1->next = n2;
//    n2->next = n3;
//    n3->next = n4;
//    n4->next = nullptr;
//    Solution().swapPairs(n1);
//    return 0;
//}

