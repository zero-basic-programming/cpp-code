#include <iostream>
#include <vector>
using namespace std;

void Qsort(vector<int>& arr, int l, int r)
{
	if (l >= r) return;
	int key = arr[rand() % (r - l + 1) + l];
	int left = l - 1, right = r + 1, i = l;
	while (i < right)
	{
		if (arr[i] < key) swap(arr[i++], arr[++left]);
		else if (arr[i] == key) ++i;
		else swap(arr[i], arr[--right]);
	}
	//递归
	Qsort(arr, l, left);
	Qsort(arr, right, r);
}


void MergeSort(vector<int>& arr, int left, int right, vector<int>& tmp)
{
	if (left >= right) return;
	int mid = (left + right) >> 1;
	//[left,mid] [mid+1,right]
	//先左右进行归并
	MergeSort(arr, left, mid, tmp);
	MergeSort(arr, mid + 1, right, tmp);
	int begin1 = left, end1 = mid;
	int begin2 = mid + 1, end2 = right;
	int i = left;
	while (begin1 <= end1 && begin2 <= end2)
	{
		tmp[i++] = arr[begin1] < arr[begin2] ? arr[begin1++] : arr[begin2++];
	}
	while (begin1 <= end1)
	{
		tmp[i++] = arr[begin1++];
	}
	while (begin2 <= end2)
	{
		tmp[i++] = arr[begin2++];
	}
	//还原回原数组
	for (int j = left; j <= right; ++j)
	{
		arr[j] = tmp[j];
	}
}


void MergeSortNonR(vector<int>& arr, vector<int>& tmp)
{
	int n = arr.size();
	int gap = 1;
	while (gap < n)
	{
		for (int i = 0; i < n; i += 2 * gap)
		{
			int begin1 = i, end1 = i + gap - 1;
			int begin2 = i + gap, end2 = i + 2 * gap - 1;
			//处理边界条件
			if (begin2 >= n) break; //这里没有最后一组，直接跳过
			if (end2 >= n) end2 = n - 1;
			int j = i, k = i;
			while (begin1 <= end1 && begin2 <= end2)
			{
				tmp[j++] = arr[begin1] < arr[begin2] ? arr[begin1++] : arr[begin2++];
			}
			while (begin1 <= end1)
			{
				tmp[j++] = arr[begin1++];
			}
			while (begin2 <= end2)
			{
				tmp[j++] = arr[begin2++];
			}
			//还原回原数组
			for (; k <= end2; ++k)
			{
				arr[k] = tmp[k];
			}
		}
		gap *= 2;
	}
}

int main()
{
	srand(time(nullptr));
	vector<int> arr = { 1,5,3,6,4,8,9,6,0,2 };
	int n = arr.size();
	vector<int> tmp(n);
	//Qsort(arr, 0, n - 1);
	//MergeSort(arr, 0, n - 1, tmp);
	MergeSortNonR(arr, tmp);
	for (auto e : arr)
	{
		cout << e << " ";
	}

	return 0;
}