#include <iostream>
using namespace std;
//#include <stack>
//int main() {
//    int n;
//    cin >> n;
//    while (n--)
//    {
//        string str;
//        cin >> str;
//        if (str.size() < 10)
//        {
//            cout << str << endl;
//        }
//        else
//        {
//            string ret;
//            int size = str.size() - 2;//中间的元素个数
//            ret += str[0];
//            stack<char> s;
//            while (size)
//            {
//                char tmp = size % 10 + '0';
//                s.push(tmp);
//                size /= 10;
//            }
//            while (!s.empty())
//            {
//                ret += s.top();
//                s.pop();
//            }
//            ret += str[str.size() - 1];
//            cout << ret << endl;
//        }
//    }
//    return 0;
//}
#include <string>
class Solution {
public:
    string replaceSpaces(string s, int length) {
        int num = 0;//记录空格个数
        int n = 0;
        for (int i = 0; i < length; ++i)
        {
            if (s[i] == ' ')
            {
                ++num;
            }
        }
        int last = length + 2 * num;
        int first = length;
        while (first >= 0)
        {
            if (first != ' ')
            {
                s[last--] = s[first--];
            }
            else
            {
                s[last--] = '0';
                s[last--] = '2';
                s[last--] = '%';
                --first;
            }
        }
        return s;
    }
};

int main()
{
    Solution().replaceSpaces("Mr John Smith    ", 13);
    return 0;
}