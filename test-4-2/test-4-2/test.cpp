//#include <iostream>
//#include <unordered_map>
//using namespace std;
//
////判断大写
//bool isbigger(char ch) {
//    if ('A' <= ch && 'Z' >= ch) {
//        return true;
//    }
//    return false;
//}
//int main() {
//    string str, result;
//    while (getline(cin, str)) {
//        unordered_map<char, char> mp;
//        char j = 'V';
//        //先把大小写的映射关系放入mp中
//        for (char i = 'A'; i <= 'Z'; ++i) {
//            if (j > 'Z')
//                j = 'A';
//            mp[i] = j;
//            ++j;
//        }
//        for (auto& ch : str) {
//            char tmp = ch;
//            if (isbigger(ch)) {
//                tmp = (mp.find(ch)->second);
//            }
//            result += tmp;
//        }
//        cout << result << endl;
//        result.clear();//多组输入
//    }
//    return 0;
//}


//#include <iostream>
//#include <cmath>
//using namespace std;
//
//bool isprime(int n) {
//    for (int i = 2; i < sqrt(n); ++i) {
//        if (n % i == 0) {
//            return false;
//        }
//    }
//    return true;
//}
//int main() {
//    int n;
//    while (cin >> n) {
//        int count = 0;
//        int flag1 = 1;//记录2是否出现过
//        int flag2 = 1;//记录3是否出现过
//        while (!isprime(n)) {
//            if (n % 2 == 0) {
//                n /= 2;
//                if (flag1)
//                    ++count;
//                flag1 = 0;
//            }
//            if (n % 3 == 0) {
//                n /= 3;
//                if (flag2)
//                    ++count;
//                flag1 = 0;
//            }
//        }
//        if (n != 0)
//            ++count;
//        cout << count << endl;
//    }
//    return 0;
//}

#include <iostream>
#include <thread>
#include <vector>
using namespace std;

//int main()
//{
//	int n, m;
//	cin >> n >> m;
//	vector<thread> v;
//	v.resize(n);
//	for (auto& t : v)
//	{
//		t = thread([m]
//			{
//				for (int i = 0; i < m; ++i)
//				{
//					cout << this_thread::get_id() <<":"<< i << endl;
//				}
//			});
//	}
//	for (auto& e : v)
//	{
//		e.join();
//	}
//	return 0;
//}
#include <mutex>

//int val = 0;
//mutex mtx;
//void Func1(int m)
//{
//	for (size_t i = 0; i < m; ++i)
//	{
//		mtx.lock();
//		++val;
//		cout << val << endl;
//		mtx.unlock();
//	}
//}
//外面++效率会更高一些
//void Func1(int m)
//{
//	mtx.lock();
//	for (size_t i = 0; i < m; ++i)
//	{
//		++val;
//		cout << val << endl;
//	}
//	mtx.unlock();
//}

////使++是原子的
//atomic<int> aval = 0;
//void Func1(int m)
//{
//	for (size_t i = 0; i < m; ++i)
//	{
//		++aval;
//	}
//}
//
//void Func2(int m)
//{
//	for (size_t i = 0; i < m; ++i)
//	{
//		mtx.lock();
//		++val;
//		mtx.unlock();
//	}
//}
//
//int main()
//{
//	int m = 1000000;
//	thread t1(Func1,m);
//	thread t2(Func1, 2 * m);//因为线程拥有自己独立的栈
//	//thread t2(Func2,2*m);
//
//	t1.join();
//	t2.join();
//	cout << aval << endl;
//	return 0;
//}


//int main()
//{
//	atomic<int> aval = 0;
//	mutex mtx;
//	正常是不用使用全局变量的
//	int m = 1000000;
//	auto func = [&aval,&mtx](int n){
//		for (int i = 0; i < n; ++i)
//		{
//			mtx.lock();
//			++aval;
//			this_thread::sleep_for(chrono::milliseconds(500));
//			cout << aval << endl;
//			mtx.unlock();
//		}
//	};
//	thread t1(func, m);
//	thread t2(func, 2*m);
//
//	t1.join();
//	t2.join();
//	cout << aval << endl;
//	return 0;
//}

//int main()
//{
//	atomic<int> aval = 0;
//	mutex mtx;
//	//正常是不用使用全局变量的
//	int m = 1000000;
//	auto func = [&aval, &mtx](int n) {
//		for (int i = 0; i < n; ++i)
//		{
//			//使用RAII的方式对锁进行管理
//			{       //这里括号保证锁的生命周期
//				lock_guard<mutex> lg(mtx);
//				this_thread::sleep_for(chrono::milliseconds(500));
//				cout << aval << endl;
//			}
//			++aval;
//
//		}
//	};
//	thread t1(func, m);
//	thread t2(func, 2 * m);
//
//	t1.join();
//	t2.join();
//	cout << aval << endl;
//	return 0;
//}

// 支持两个线程交替打印，t1一个打印奇数，t2一个打印偶数,打印到100

//方式一，这种方法可以，但是效率并不高
//int main()
//{
//	int i = 0;
//	mutex mtx;
//	//打印奇数
//	thread t1([&i,&mtx]
//		{
//			while (i < 100)
//			{
//				if (i % 2 != 0)
//				{
//					++i;
//					cout << this_thread::get_id() << ":" << i << endl;		
//				}
//			}
//		});
//	//打印偶数
//	thread t2([&i,&mtx]
//		{
//			while (i <= 100)
//			{
//				if (i % 2 == 0)
//				{
//					++i;
//					cout << this_thread::get_id() << ":" << i << endl;
//				}
//			}
//		});
//	t1.join();
//	t2.join();
//	return 0;
//}
//这种方法是不行的
//int main()
//{
//	int i = 0;
//	mutex mtx;
//	thread t1([&i, &mtx] {
//		while (i < 100)
//		{
//			mtx.lock();
//			cout << this_thread::get_id() << "->" << i << endl;
//			++i;
//			mtx.unlock();
//		}
//		});
//
//	thread t2([&i, &mtx] {
//		while (i <= 100)
//		{
//			mtx.lock();
//			cout << this_thread::get_id() << "->" << i << endl;
//			++i;
//			mtx.unlock();
//		}
//		});
//
//	t1.join();
//	t2.join();
//
//	return 0;
//}


//int main()
//{
//	int i = 0;
//	thread t1([&i] {
//		while (i < 100)
//		{
//			if (i % 2)
//			{
//				cout << this_thread::get_id() << "->" << i << endl;
//				++i;
//			}
//		}
//		});
//
//	thread t2([&i] {
//		while (i <= 100)
//		{
//			if (i % 2 == 0)
//			{
//				cout << this_thread::get_id() << "->" << i << endl;
//				++i;
//			}
//		}
//		});
//
//	t1.join();
//	t2.join();
//
//	return 0;
//}

////最佳方式 ,使用信号量
//int main()
//{
//	int i = 0;
//	mutex mtx;
//	condition_variable cv;
//	thread t1([&] {
//		while (i < 100)
//		{
//			unique_lock<mutex> ul(mtx);
//			cv.wait(ul, [&i] {return i % 2; });
//			cout << this_thread::get_id() << "->" << i << endl;
//			++i;
//			//唤醒另外一个线程
//			cv.notify_one();
//		}
//		});
//	//一开始是偶数开始打印
//	thread t2([&] {
//		while (i <= 100)
//		{
//			unique_lock<mutex> ul(mtx);
//			cv.wait(ul, [&i] {return i % 2 == 0; });
//			cout << this_thread::get_id() << "->" << i << endl;
//			++i;
//			cv.notify_one();
//		}
//		});
//
//	t1.join();
//	t2.join();
//
//	return 0;
//}

//class A
//{
//public:
//	A(int a, int b)
//		:_a(a)
//		, _b(b)
//	{}
//	operator int()
//	{
//		return _a + _b;
//	}
//private:
//	int _a;
//	int _b;
//};
//
//int main()
//{
//	A aa(1, 2);
//	//这里通过operator重载的方式，实现自定义类型到内置类型的转换
//	int b = aa;
//	cout << b << endl;
//	return 0;
//}

class Date
{
	friend ostream& operator << (ostream& out, const Date& d);
	friend istream& operator >> (istream& in, Date& d);
public:
	Date(int year = 1, int month = 1, int day = 1)
		:_year(year)
		, _month(month)
		, _day(day)
	{}

	operator bool()
	{
		// 这里是随意写的，假设输入_year为0，则结束
		if (_year == 0)
			return false;
		else
			return true;
	}
private:
	int _year;
	int _month;
	int _day;
};

istream& operator >> (istream& in, Date& d)
{
	in >> d._year >> d._month >> d._day;
	return in;
}

ostream& operator << (ostream& out, const Date& d)
{
	out << d._year << " " << d._month << " " << d._day;
	return out;
}
//
//int main()
//{
//	Date d(2023, 4, 2);
//	cout << d << endl;
//	while (d)//这里通过operator()转换成了bool类型
//	{
//		//这里输入0就会结束
//		cin >> d;
//		cout << d << endl;
//	}
//}

#include <fstream>
#include <sstream>
//C++文件操作
//二进制读写 , 这个使用的并不多
//struct ServerInfo
//{
//	char _address[32];
//	//string _address; 这里尽量不要使用string，因为该对象出了作用域会销毁，多进程下不要使用
//	int _port;
//
//	//Date _date;
//};
//
//class ConfigManager
//{
//public:
//	ConfigManager(const char* filename)
//		:_filename(filename)
//	{}
//	void WriteBin(const ServerInfo& info)
//	{
//		ofstream ofs(_filename, ios_base::out | ios_base::binary);
//		ofs.write((char*)&info, sizeof(info));
//	}
//	void ReadBin(const ServerInfo& info)
//	{
//		ifstream ifs(_filename, ios_base::in | ios_base::binary);
//		ifs.read((char*)&info, sizeof(info));
//	}
//private:
//	string _filename; // 配置文件
//};
//
//int main()
//{
//	ConfigManager cm("log.txt");
//	ServerInfo winfo = { "hello !", 10 };
//	cm.WriteBin(winfo);
//
//	ServerInfo rinfo;
//	cm.ReadBin(rinfo);
//	cout << rinfo._address << " " << rinfo._port << endl;
//	return 0;
//}

//#include <fstream>
////文本读写
//
//struct ServerInfo
//{
//	//char _address[32];
//	string _address; 
//	int _port;
//	Date _date;
//};
//
//class ConfigManager
//{
//public:
//	ConfigManager(const char* filename)
//		:_filename(filename)
//	{}
//	void WriteBin(const ServerInfo& info)
//	{
//		ofstream ofs(_filename, ios_base::out | ios_base::binary);
//		ofs.write((char*)&info, sizeof(info));
//	}
//	void ReadBin(ServerInfo& info)
//	{
//		ifstream ifs(_filename, ios_base::in | ios_base::binary);
//		ifs.read((char*)&info, sizeof(info));
//	}
//	void WriteText(const ServerInfo& info)
//	{
//		ofstream ofs(_filename);
//		ofs << info._address << endl;
//		ofs << info._port<< endl;
//		ofs << info._date << endl;
//	}
//	void ReadText(ServerInfo& info)
//	{
//		ifstream ifs(_filename);
//		ifs >> info._address;
//		ifs >> info._port;
//		ifs >> info._date;
//	}
//private:
//	string _filename; // 配置文件
//};
//
//int main()
//{
//	ConfigManager cm("test.txt");
//	ServerInfo winfo = { "hello C++",1,{2023,4,2} };
//	cm.WriteText(winfo);
//
//	ServerInfo rinfo;
//	cm.ReadText(rinfo);
//	cout << rinfo._address << endl;
//	cout << rinfo._port << endl;
//	cout << rinfo._date << endl;
//
//	return 0;
//}


//// 序列化和反序列化
//struct ChatInfo
//{
//	string _name; // 名字
//	int _id;      // id
//	Date _date;   // 时间
//	string _msg;  // 聊天信息
//};
//
//
//
//int main()
//{
//	// 序列化
//	ChatInfo winfo = { "张三", 135246, { 2023, 4, 1 }, "晚上一起看电影吧" };
//	stringstream oss;
//	oss << winfo._name << endl;
//	oss << winfo._id << endl;
//	oss << winfo._date << endl;
//	oss << winfo._msg << endl;
//	string str = oss.str();
//	cout << "网络发送：" << str << endl;
//
//	// 反序列化
//	ChatInfo rInfo;
//	stringstream iss(str);
//	iss >> rInfo._name >> rInfo._id >> rInfo._date >> rInfo._msg;
//	cout << "-------------------------------------------------------" << endl;
//	cout << "姓名：" << rInfo._name << "(" << rInfo._id << ") ";
//	cout << rInfo._date << endl;
//	cout << rInfo._name << ":>" << rInfo._msg << endl;
//	cout << "-------------------------------------------------------" << endl;
//
//	return 0;
//}

#include <iostream>
#include <cmath>
using namespace std;
bool isprime(int a)
{
    for (int i = 2; i < sqrt(a); ++i)
    {
        if (a % i == 0)
        {
            return false;
        }
    }
    return true;
}

int main()
{
    int a = 0;
    while (cin >> a)
    {
        string result = (to_string(a));
        result += " = ";
        while (!isprime(a))
        {
            if (a % 2 == 0)
            {
                result += to_string(2);
                result += " * ";
                a /= 2;
            }
            if (a % 3 == 0)
            {
                result += to_string(3);
                result += " * ";
                a /= 3;
            }
        }
        if (a == 0)
            result.pop_back();
        else
            result += to_string(a);
        //输出结果
        cout << result << endl;
    }
    return 0;
}