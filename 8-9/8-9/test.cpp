#include <iostream>
#include <fstream>
#include <string>

int main() {
    // 文件路径
    std::string filePath = "D:\\VS\\tmp.txt";

    // 使用std::ios::binary标志打开文件以二进制模式
    std::ifstream file(filePath, std::ios::binary);

    // 检查文件是否成功打开
    if (!file.is_open()) {
        std::cerr << "无法打开文件: " << filePath << std::endl;
        return 1;
    }

    // 使用char数组来读取文件内容
    char buffer[256]; // 定义一个足够大的缓冲区
    while (file.read(buffer, sizeof(buffer))) {
        // 读取成功，打印缓冲区内容
        for (size_t i = 0; i < file.gcount(); ++i) {
            std::cout << std::hex << static_cast<int>(static_cast<unsigned char>(buffer[i])) << " ";
        }
        std::cout << std::endl;
    }

    // 如果文件结束，关闭文件
    file.close();

    return 0;
}