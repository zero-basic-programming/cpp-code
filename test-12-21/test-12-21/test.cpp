#include <iostream>
#include <vector>
using namespace std;

//记忆化搜索
class Solution {
    vector<int> momery;  //备忘录
public:
    int fib(int n) {
        momery.resize(31, -1);
        return dfs(n);
    }
    int dfs(int n) {
        if (n == 0 || n == 1) return n;
        //查找当前有没有记录过
        if (momery[n] != -1) return momery[n];
        //当前数值已经计算过，放入当备忘录中
        momery[n] = dfs(n - 1) + dfs(n - 2);
        return momery[n];
    }
};


//动态规划
class Solution {
public:
    int fib(int n) {
        int dp[31];
        dp[0] = 0, dp[1] = 1;
        for (int i = 2; i <= n; ++i)
            dp[i] = dp[i - 1] + dp[i - 2];
        return dp[n];
    }
};


class Solution {
public:
    int uniquePaths(int m, int n) {
        vector<vector<int>> dp(m + 1, vector<int>(n + 1, 0));
        //初始化
        dp[0][1] = 1;
        for (int i = 1; i <= m; ++i)
        {
            for (int j = 1; j <= n; ++j)
            {
                dp[i][j] = dp[i - 1][j] + dp[i][j - 1];
            }
        }
        return dp[m][n];
    }
};


class Solution {
public:
    int uniquePaths(int m, int n) {
        vector<int> dp(n, 1);
        for (int i = 0; i < m - 1; ++i)
        {
            for (int j = 1; j < n; ++j)
            {
                dp[j] += dp[j - 1];
            }
        }
        return dp[n - 1];
    }
};