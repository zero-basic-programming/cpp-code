#include <iostream>
#include <vector>
using namespace std;


//class Solution {
//    int dx[4] = { 1,-1,0,0 };
//    int dy[4] = { 0,0,1,-1 };
//    bool check[51][51]; //用来检查当前格子是否已经检查过
//    int m, n;
//
//public:
//    vector<vector<int>> floodFill(vector<vector<int>>& image, int sr, int sc, int color) {
//        m = image.size(), n = image[0].size();
//        if (image[sr][sc] == color) return image; //边界条件
//        dfs(image, sr, sc, image[sr][sc], color);
//        image[sr][sc] = color;
//        return image;
//    }
//    void dfs(vector<vector<int>>& image, int i, int j, int cur, int color)
//    {
//        check[i][j] = true;
//        for (int k = 0; k < 4; ++k)
//        {
//            int x = i + dx[k], y = j + dy[k];
//            //判断边界条件，是否符合条件
//            if (x >= 0 && x < m && y >= 0 && y < n && image[x][y] == cur && !check[x][y])
//            {
//                image[x][y] = color;
//                dfs(image, x, y, cur, color);
//            }
//        }
//    }
//};


//class Solution {
//    int dx[4] = { 1,-1,0,0 };
//    int dy[4] = { 0,0,1,-1 };
//    int m, n;
//
//public:
//    vector<vector<int>> floodFill(vector<vector<int>>& image, int sr, int sc, int color) {
//        if (image[sr][sc] == color) return image; //边界条件
//        m = image.size(), n = image[0].size();
//        dfs(image, sr, sc, image[sr][sc], color);
//        return image;
//    }
//    void dfs(vector<vector<int>>& image, int i, int j, int cur, int color)
//    {
//        image[i][j] = color;
//        for (int k = 0; k < 4; ++k)
//        {
//            int x = i + dx[k], y = j + dy[k];
//            //判断边界条件，是否符合条件
//            if (x >= 0 && x < m && y >= 0 && y < n && image[x][y] == cur)
//            {
//                dfs(image, x, y, cur, color);
//            }
//        }
//    }
//};


//class Solution {
//    int dx[4] = { 0,0,1,-1 };
//    int dy[4] = { 1,-1,0,0 };
//    int ret, m, n;
//    bool check[301][301];
//public:
//    int numIslands(vector<vector<char>>& grid) {
//        //找1
//        m = grid.size(), n = grid[0].size();
//        for (int i = 0; i < m; ++i)
//        {
//            for (int j = 0; j < n; ++j)
//            {
//                if (grid[i][j] == '1' && !check[i][j])
//                {
//                    dfs(grid, i, j);
//                    ++ret;
//                }
//            }
//        }
//        return ret;
//    }
//    void dfs(vector<vector<char>>& grid, int i, int j)
//    {
//        check[i][j] = true;
//        for (int k = 0; k < 4; ++k)
//        {
//            int x = i + dx[k], y = j + dy[k];
//            if (x >= 0 && x < m && y >= 0 && y < n && !check[x][y] && grid[x][y] == '1')
//            {
//                dfs(grid, x, y);
//            }
//        }
//    }
//};


