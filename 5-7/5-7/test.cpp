#include <iostream>
#include <vector>
using namespace std;

//class Solution {
//public:
//    int hammingWeight(uint32_t n) {
//        int count = 0;
//        while (n)
//        {
//            n = n & (n - 1);
//            ++count;
//        }
//        return count;
//    }
//};


//class Solution {
//public:
//    vector<int> spiralArray(vector<vector<int>>& array) {
//        vector<int> ret;
//        if (array.size() == 0) return ret;
//        int left = 0, right = array[0].size() - 1, top = 0, bottom = array.size() - 1;
//        int i = 0, j = 0;
//        while (true)
//        {
//            //第一行行
//            for (j = left; j < right; ++j)   ret.push_back(array[top][j]);
//            if (j > right) break;
//            //最后一列
//            for (i = top; i < bottom; ++i) ret.push_back(array[i][right]);
//            if (i > bottom) break;
//            //最后一行
//            for (j = right; j > 0; --j) ret.push_back(array[bottom][j]);
//            if (j < left) break;
//            //最后一列
//            for (i = bottom; i > 0; --i) ret.push_back(array[i][left]);
//            if (i < top) break;
//            --bottom;
//            --right;
//            ++left;
//            ++top;
//        }
//        return ret;
//    }
//};


//class Solution {
//public:
//    Node* copyRandomList(Node* head) {
//        //利用hash
//        unordered_map<Node*, Node*> hash;  //用来记录原来链表的节点和新链表的节点
//        Node* cur = head;
//        while (cur)
//        {
//            hash[cur] = new Node(cur->val);
//            cur = cur->next;
//        }
//        //把新的链表的连接关系建立好
//        cur = head;
//        while (cur)
//        {
//            hash[cur]->next = hash[cur->next];
//            hash[cur]->random = hash[cur->random];
//            cur = cur->next;
//        }
//        return hash[head];
//    }
//};


//class Solution {
//public:
//    Node* copyRandomList(Node* head) {
//        //1.将原来的链表和新的链表进行拼接
//        Node* cur = head;
//        while (cur)
//        {
//            Node* tmp = new Node(cur->val);
//            tmp->next = cur->next;
//            cur->next = tmp;
//            cur = tmp->next;
//        }
//        //2.把随机节点的值获取到
//        cur = head;
//        while (cur)
//        {
//            //新链表的随机节点就是在原链表随机节点的后面
//            if (cur->random)
//                cur->next->random = cur->random->next;
//            cur = cur->next->next;
//        }
//        //3.分开链表，得到新的链表
//        cur = head;
//        Node* guard = new Node(0);
//        Node* tail = guard;
//        while (cur)
//        {
//            tail->next = cur->next;
//            cur->next = cur->next->next;
//            tail = tail->next;
//            cur = cur->next;
//        }
//        Node* newhead = guard->next;
//        delete guard;
//        return newhead;
//    }
//};


