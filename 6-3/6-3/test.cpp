#include <iostream>
#include <vector>
using namespace std;

//class Solution {
//public:
//    string dynamicPassword(string password, int target) {
//        //字符串逆序，[0,target-1]  [target,n-1]
//        reverse(password.begin(), password.begin() + target);
//        reverse(password.begin() + target, password.end());
//        reverse(password.begin(), password.end());
//        return password;
//    }
//};


//class Solution {
//public:
//    void Myreverse(string& str, int left, int right)
//    {
//        while (left < right)
//        {
//            char ch = str[right];
//            str[right] = str[left];
//            str[left] = ch;
//            ++left;
//            --right;
//        }
//    }
//    string dynamicPassword(string password, int target) {
//        //字符串逆序，[0,target-1]  [target,n-1]
//        int n = password.size();
//        Myreverse(password, 0, target - 1);
//        Myreverse(password, target, n - 1);
//        Myreverse(password, 0, n - 1);
//        return password;
//    }
//};

//
//class Solution {
//public:
//    int dismantlingAction(string arr) {
//        int hash[128] = { 0 };   //hash用来记录元素出现的次数
//        int left = 0, right = 0, n = arr.size();
//        int ret = 0;
//        while (right < n)
//        {
//            //进窗口
//            hash[arr[right]]++;
//            //判断
//            while (hash[arr[right]] > 1)
//            {
//                //出窗口
//                --hash[arr[left++]];
//            }
//            ret = max(ret, right - left + 1);
//            ++right;
//        }
//        return ret;
//    }
//};


//class Solution {
//    int count = 1;  //用来记录最大的个数
//    int ret = 0;    //用来记录结果
//public:
//    bool dfs(TreeNode* root, int cnt)
//    {
//        if (root == nullptr) return false;
//        if (dfs(root->right, cnt)) return true;
//        if (cnt == count)
//        {
//            ret = root->val;
//            return true;
//        }
//        ++count;
//        if (dfs(root->left, cnt)) return true;
//        return false;
//    }
//    int findTargetNode(TreeNode* root, int cnt) {
//        //通过右根左的遍历方式来倒序输出结果
//        dfs(root, cnt);
//        return ret;
//    }
//};


