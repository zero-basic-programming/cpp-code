#include <iostream>
#include <vector>
using namespace std;


//class Solution {
//public:
//    string convert(string s, int numRows) {
//        int n = s.size();
//        if (numRows == 1) return s; //特殊情况
//        int d = 2 * numRows - 2; //公差
//        string ret;
//        for (int i = 0; i < numRows; ++i) // 遍历0到n-1行
//        {
//            if (i == 0 || i == numRows - 1)
//            {
//                //第一行和最后一行需要特殊处理一下
//                int k = i;
//                while (k < n)
//                {
//                    ret += s[k];
//                    k += d;
//                }
//            }
//            else { //其他情况 k -> d-k
//                int k = i;
//                int j = d - k;
//                while (k < n)
//                {
//                    ret += s[k];
//                    if (j >= n) break;
//                    ret += s[j];
//                    //迭代
//                    k = d + k;
//                    j = j + d;
//                }
//            }
//        }
//        return ret;
//    }
//};

//class Solution {
//public:
//    int longestArithSeqLength(vector<int>& nums) {
//        int n = nums.size();
//        int ret = 2;
//        vector<vector<int>> dp(n, vector<int>(n, 2));
//        unordered_map<int, int> hash; //用来记录前面已经出现过的数字和其下标的映射关系
//        for (int i = 0; i < n; ++i)
//        {
//            for (int j = i - 1; j > 0; --j)
//            {
//                int a = 2 * nums[j] - nums[i]
//                    if (hash.find(a) == hash.end())
//                    {
//                        //没找到
//                        dp[j][i] = 2;
//                    }
//                    else
//                    {
//                        //前面那个下标可以比j大，要舍弃
//                        int k = hash[2 * nums[j] - nums[i]];
//                        if (k >= j) dp[k][j] = 2;
//                        else dp[j][i] = dp[k][j] + 1;
//                    }
//                ret = max(ret, dp[j][i]);
//            }
//            hash[nums[i]] = i;
//        }
//        return ret;
//    }
//};

//class Solution {
//public:
//    void sortColors(vector<int>& nums) {
//        unordered_map<int, int> hash; //记录数字和次数
//        for (auto e : nums) hash[e]++;
//        int i = 0;
//        for (auto& pair : hash)
//        {
//            while (pair.second > 0)
//            {
//                nums[i] = pair.first;
//                --pair.second;
//            }
//        }
//    }
//};