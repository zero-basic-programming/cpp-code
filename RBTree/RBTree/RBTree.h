#pragma once
#include <iostream>
using namespace std;
#include <time.h>

//定义红黑
enum Colour
{
	RED,
	BLACK,
};

//红黑树节点结构
template<class K, class V>
struct RBTreeNode
{
	pair<K, V> _kv;
	RBTreeNode<K, V>* _left;
	RBTreeNode<K, V>* _right;
	RBTreeNode<K, V>* _parent;
	Colour _col;

	RBTreeNode(const pair<K, V>& kv)
		:_kv(kv)
		, _left(nullptr)
		, _right(nullptr)
		, _parent(nullptr)
		, _col(RED)
	{}
};

//红黑树实现
template <class K, class V>
class RBTree
{
	typedef RBTreeNode<K, V> Node;
public:
	bool insert(const pair<K, V>& kv)
	{
		//第一个直接插入，并改为黑
		if (_root == nullptr)
		{
			_root = new Node(kv);
			_root->_col = BLACK;
			return true;
		}
		Node* cur = _root;
		Node* parent = nullptr;
		//找到空，插入节点
		while (cur)
		{
			if (cur->_kv.first < kv.first)
			{
				parent = cur;
				cur = cur->_right;
			}
			else if(cur->_kv.first > kv.first)
			{
				parent = cur;
				cur = cur->_left;
			}
			else
			{
				return false;
			}
		}
		cur = new Node(kv);
		cur->_parent = parent;
		if (parent->_kv.first > kv.first)
		{
			parent->_left = cur;
		}
		else
		{
			parent->_right = cur;
		}
		//判断是否满足红黑树的规则
		while (parent && parent->_col == RED)
		{
			Node* grandfather = parent->_parent;
			//parent在grandfather的左
			if (grandfather->_left == parent)
			{
				Node* uncle = grandfather->_right;
				//分3种情况
				//1.uncle存在&&uncle和parent都是红，直接把他们改成黑，然后grandfather改成红迭代上去即可
				if (uncle && uncle->_col == RED)
				{
					uncle->_col = parent->_col = BLACK;
					grandfather->_col = RED;
					//迭代向上更新，有可能grandfather上面是红
					cur = grandfather;
					parent = cur->_parent;
				}
				else
				{
					//情况2或者3
					//情况2：cur在parent左边，形成直线型，直接右旋变色即可，parent变成黑，grandfather变成红
					if (parent->_left == cur)
					{
						RotateR(grandfather);
						parent->_col = BLACK;
						grandfather->_col = RED;
					}
					else
					{
						//情况3：形成折线型，先左旋parent再右旋grandfather然后变色，cur最终变黑，grandfather变红
						RotateL(parent);
						RotateR(grandfather);
						cur->_col = BLACK;
						grandfather->_col = RED;
					}
					//无论是情况2还是3，最终上面的节点都是黑，就不需要更新了，直接跳出
					break;
				}
			}
			else
			{
				//parent在grandfather的右
				Node* uncle = grandfather->_left;
				//情况1
				if (uncle && uncle->_col == RED)
				{
					uncle->_col = parent->_col = BLACK;
					grandfather->_col = RED;
					//迭代
					cur = grandfather;
					parent = cur->_parent;
				}
				else
				{
					//情况2
					if (parent->_right == cur)
					{
						//左旋加变色，parent变黑，grandfather变红
						RotateL(grandfather);
						parent->_col = BLACK;
						grandfather->_col = RED;
					}
					else
					{
						//情况3：先右旋再左旋，cur变黑，grandfather变红
						RotateR(parent);
						RotateL(grandfather);
						cur->_col = BLACK;
						grandfather->_col = RED;
					}
				}
			}
		}
		//最终根一定是黑的
		_root->_col = BLACK;
	}
	void Inorder()
	{
		_Inorder(_root);
	}
	bool IsvalidRBTree()
	{
		//空树是红黑树
		if (_root == nullptr)
			return true;
		//根是黑色
		if (_root->_col != BLACK)
		{
			cout << "不满足规则2：根节点必须为黑色" << endl;
			return false;
		}
		//每个路径，黑色节点数量相同
		Node* cur = _root;
		int blackcount = 0;//记录黑色节点数量
		while (cur)
		{
			if (cur->_col == BLACK)
				++blackcount;
			cur = cur->_left;
		}
		int k = 0;//用来记录每个路径的黑色节点个数
		return _IsvaildRBTree(_root, k, blackcount);
	}
private:
	bool _IsvaildRBTree(Node* root, int k,const int blackcount)
	{
		if (root == nullptr)
		{
			if (k != blackcount)
			{
				cout << "不满足规则4，每个路径的黑色节点数相同" << endl;
				return false;
			}
			return true;
		}
		Node* parent = root->_parent;
		//往上找是否存在相连的红色节点
		if (parent && parent->_col == root->_col && root->_col == RED)
		{
			cout << "存在相连的红节点，不满足规则3" << endl;
			return false;
		}
		if (root->_col == BLACK)
			++k;
		return _IsvaildRBTree(root->_left, k, blackcount) && _IsvaildRBTree(root->_right, k, blackcount);
	}

	void _Inorder(Node* root)
	{
		if (root == nullptr)
			return;
		_Inorder(root->_left);
		cout << root->_kv.first << ":" << root->_kv.second << endl;
		_Inorder(root->_right);
	}
	void RotateR(Node* parent)
	{
		Node* subL = parent->_left;
		Node* subLR = subL->_right;
		parent->_left = subLR;
		if (subLR)
		{
			subLR->_parent = parent;
		}
		Node* ppNode = parent->_parent;
		parent->_parent = subL;
		subL->_right = parent;
		if (ppNode == nullptr)//等价于_root == parent
		{
			_root = subL;
			_root->_parent = nullptr;
		}
		else
		{
			if (ppNode->_left == parent)
			{
				ppNode->_left = subL;
			}
			else
			{
				ppNode->_right = subL;
			}
			subL->_parent = ppNode;
		}
	}
	void RotateL(Node* parent)
	{
		Node* subR = parent->_right;
		Node* subRL = subR->_left;
		parent->_right = subRL;
		if (subRL)
		{
			subRL->_parent = parent;
		}
		Node* ppNode = parent->_parent;
		parent->_parent = subR;
		subR->_left = parent;
		//ppNode可能为空
		if (ppNode == nullptr)
		{
			_root = subR;
			_root->_parent = nullptr;
		}
		else
		{
			//parent可能是ppNode的左右孩子
			if (ppNode->_left == parent)
			{
				ppNode->_left = subR;
			}
			else
			{
				ppNode->_right = subR;
			}
			subR->_parent = ppNode;
		}
	}
	Node* _root = nullptr;
};

void TestRBTree1()
{
	//int a[] = { 16, 3, 7, 11, 9, 26, 18, 14, 15 };
	int a[] = { 4, 2, 6, 1, 3, 5, 15, 7, 16 ,14};
	RBTree<int, int> t;
	for (auto e : a)
	{
		t.insert(make_pair(e, e)); 
		//cout << e << " "<< t.IsvalidRBTree() << endl;
	}
	cout << endl;
	t.Inorder();
}

void TestRBTree2()
{
	srand(time(0));
	RBTree<int, int> t;
	for (int i = 0; i < 100000; ++i)
	{
		int x = rand();
		t.insert(make_pair(x, x));
	}
	//判断是否满足红黑树的规则
	//t.Inorder();
	cout << t.IsvalidRBTree() << endl;
}