#include <iostream>
#include <vector>
using namespace std;


//class Solution {
//public:
//    bool isInterleave(string s1, string s2, string s3) {
//        int m = s1.size();
//        int n = s2.size();
//        if (m + n != s3.size()) return false;
//        s1 = " " + s1;
//        s2 = " " + s2;
//        s3 = " " + s3;
//        vector<vector<bool>> dp(m + 1, vector<bool>(n + 1, false));
//        //初始化
//        dp[0][0] = true;
//        //第一行初始化
//        for (int j = 1; j <= n; ++j)
//        {
//            if (s2[j] == s3[j]) dp[0][j] = true;
//            else break;
//        }
//        //初始化第一行
//        for (int i = 1; i <= m; ++i)
//        {
//            if (s1[i] == s3[i]) dp[i][0] = true;
//            else break;
//        }
//        for (int i = 1; i <= m; ++i)
//        {
//            for (int j = 1; j <= n; ++j)
//            {
//                dp[i][j] = (s1[i] == s3[i + j] && dp[i - 1][j])
//                    || (s2[j] == s3[i + j] && dp[i][j - 1]);
//            }
//        }
//        return dp[m][n];
//    }
//};


//class Solution {
//public:
//    int minimumDeleteSum(string s1, string s2) {
//        int m = s1.size(), n = s2.size();
//        vector<vector<int>> dp(m + 1, vector<int>(n + 1));
//        for (int i = 1; i <= m; ++i)
//        {
//            for (int j = 1; j <= n; ++j)
//            {
//                dp[i][j] = max(dp[i - 1][j], dp[i][j - 1]);
//                if (s1[i - 1] == s2[j - 1]) dp[i][j] = max(dp[i][j], dp[i - 1][j - 1] + s1[i - 1]);
//            }
//        }
//        int sum = 0;
//        for (auto ch : s1) sum += ch;
//        for (auto ch : s2) sum += ch;
//        return sum - 2 * dp[m][n];
//    }
//};


//class Solution {
//public:
//    int findLength(vector<int>& nums1, vector<int>& nums2) {
//        int m = nums1.size();
//        int n = nums2.size();
//        vector<vector<int>> dp(m + 1, vector<int>(n + 1));
//        int ret = 0;
//        for (int i = 1; i <= m; ++i)
//        {
//            for (int j = 1; j <= n; ++j)
//            {
//                if (nums1[i - 1] == nums2[j - 1])
//                    dp[i][j] = dp[i - 1][j - 1] + 1;
//                ret = max(ret, dp[i][j]);
//            }
//        }
//        return ret;
//    }
//};