#include <iostream>
#include <vector>
using namespace std;

//排升序
void _MergeSort(vector<int>& arr, int left, int right, vector<int>& tmp)
{
	if (left >= right) return;
	//找中间
	int mid = left + (right - left) / 2;
	//[left,mid] [mid+1,right]
	_MergeSort(arr, left, mid, tmp);
	_MergeSort(arr, mid+1, right, tmp);
	int begin1 = left; int end1 = mid;
	int begin2 = mid + 1; int end2 = right;
	int i = left;
	//把当前数组的数据排序好放入到tmp中
	while (begin1 <= end1 && begin2 <= end2)
	{
		if (arr[begin1] < arr[begin2])
		{
			tmp[i++] = arr[begin1++];
		}
		else
		{
			tmp[i++] = arr[begin2++];
		}
	}
	//这里有可能还有一些数据没有排完
	while (begin1 <= end1)
	{
		tmp[i++] = arr[begin1++];
	}
	while (begin2 <= end2)
	{
		tmp[i++] = arr[begin2++];
	}
	//把数据拷贝回原数组
	for (int j = left; j <= right; ++j)
	{
		arr[j] = tmp[j];
	}
}

void MergeSort(vector<int>& arr)
{
	//创建一个新的数组用来排序
	vector<int> tmp(arr.size());
	_MergeSort(arr, 0, arr.size() - 1, tmp);
}

//进行一次归并排序
void _MergeSortNonR(vector<int>& arr, vector<int>& tmp, int begin1, int end1, int begin2, int end2)
{
	int i = begin1; int left = i;
	while (begin1 <= end1 && begin2 <= end2)
	{
		if (arr[begin1] < arr[begin2])
		{
			tmp[i++] = arr[begin1++];
		}
		else
		{
			tmp[i++] = arr[begin2++];
		}
	}
	//处理还没有处理的数组
	while (begin1 <= end1)
	{
		tmp[i++] = arr[begin1++];
	}
	while (begin2 <= end2)
	{
		tmp[i++] = arr[begin2++];
	}
	//拷贝回原来的数组
	for (int j = left; j <= end2; ++j)
	{
		arr[j] = tmp[j];
	}
}

//归并排序非递归
void MergeSortNonR(vector<int>& arr)
{
	//开辟一个用来排序的数组
	vector<int> tmp(arr.size());
	//这里并不需要数据结构来辅助实现，两两为一组
	int gap = 1;
	int n = arr.size();
	while(gap < n)
	{
		for (int i = 0; i < n; i += gap * 2)
		{
			int begin1 = i; int end1 = i + gap - 1;
			int begin2 = i + gap; int end2 = i + 2 * gap - 1;
			//这里要讨论边界条件
			if (begin2 >= n) break;  //这里没有最后一组的数据，倒数第二组不需要操作
			if (end2 >= n) end2 = n - 1;  //这里是最后一组数据不全，所以要处理一下边界
			//进行一次排序
			_MergeSortNonR(arr, tmp, begin1, end1, begin2, end2);
		}
		//最后gap指数型增长
		gap *= 2;
	}
}


//计数排序
void CountSort(vector<int>& arr)
{
	//遍历一次找最大和最小
	int maxnum = INT_MIN; int minnum = INT_MAX;
	for (auto e : arr)
	{
		if (e > maxnum)maxnum = e;
		if (e < minnum)minnum = e;
	}
	//根据他们之间的差来决定开多大的数组
	int sz = maxnum - minnum +1;
	vector<int> hash(sz);
	for (auto e : arr)
	{
		++hash[e - minnum];
	}
	//放回原数组
	int j = 0;
	for (int i = 0; i < hash.size(); ++i)
	{
		while (hash[i]--)
		{
			arr[j++] = (i + minnum);
		}
	}
}


int main()
{
	vector<int> arr = { 3,2,5,4,6,8,7,0,9,1 ,2,3,24,3,5,2,6,8};
	CountSort(arr);
	for (auto e : arr)
	{
		cout << e << " ";
	}
	return 0;
}