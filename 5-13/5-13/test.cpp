#include <iostream>
#include <vector>
using namespace std;


//int main()
//{
//	return 0;
//	//(int*)func(int, int);   //这个是错误的，因为这样就不是一个指针类型了
//	int* func(int,int);   //这个写法是正确的
//}


//class Solution {
//public:
//    bool validateBookSequences(vector<int>& putIn, vector<int>& takeOut) {
//        stack<int> st;
//        int i = 0, j = 0;
//        while (st.size() || i < putIn.size())
//        {
//            if (!st.empty() && st.top() != takeOut[j] && i == putIn.size()) return false;
//            else if (st.empty() || st.top() != takeOut[j])
//            {
//                st.push(putIn[i++]);
//            }
//            else
//            {
//                st.pop();
//                ++j;
//            }
//        }
//        return true;
//    }
//};


//class Solution {
//public:
//    bool validateBookSequences(vector<int>& putIn, vector<int>& takeOut) {
//        stack<int> st;
//        int i = 0;
//        for (auto e : putIn)
//        {
//            st.push(e);
//            while (!st.empty() && st.top() == takeOut[i])
//            {
//                st.pop();
//                ++i;
//            }
//        }
//        return st.empty();
//    }
//};


//class Solution {
//    vector<string> ret;
//    string path;   //用来记录当前路径下的组合
//    vector<bool> check;  //用来检测当前的字符是否在前面使用过
//    int n;
//public:
//    void dfs(string& goods)
//    {
//        if (n == path.size())
//        {
//            ret.push_back(path);
//            return;
//        }
//        for (int i = 0; i < n; ++i)
//        {
//            if (check[i] || (i > 0 && goods[i] == goods[i - 1] && check[i - 1] == false)) continue;
//            check[i] = true;
//            path += goods[i];
//            dfs(goods);
//            path.pop_back();
//            check[i] = false;  //回溯
//        }
//    }
//    vector<string> goodsOrder(string goods) {
//        n = goods.size();
//        sort(goods.begin(), goods.end());
//        check.resize(n, false);
//        dfs(goods);
//        return ret;
//    }
//};