#include <iostream>
#include <vector>
using namespace std;


//class Solution {
//public:
//    bool lemonadeChange(vector<int>& bills) {
//        int five = 0, ten = 0;
//        for (auto e : bills)
//        {
//            if (e == 5) five++;
//            else if (e == 10)
//            {
//                //没有5块就不行
//                if (five == 0) return false;
//                five--; ten++;
//            }
//            else {
//                //贪心 如果有10先使用10，5的用法更多
//                if (five && ten) {
//                    ten--;
//                    five--;
//                }
//                else if (five >= 3)
//                {
//                    //找回15
//                    five -= 3;
//                }
//                else return false;
//            }
//        }
//        return true;
//    }
//};



//class Solution {
//public:
//    int halveArray(vector<int>& nums) {
//        priority_queue<double> heap;
//        double sum = 0;
//        for (auto e : nums)
//        {
//            sum += e;
//            heap.push(e);
//        }
//        sum /= 2.0; //比这个结果大就可以了
//        int count = 0;
//        //贪心：优先选择大的来操作
//        while (sum > 0)
//        {
//            double tmp = heap.top() / 2.0;
//            heap.pop();
//            sum -= tmp;
//            heap.push(tmp);
//            ++count;
//        }
//        return count;
//    }
//};


//class Solution
//{
//public:
//    string largestNumber(vector<int>&nums)
//    {
//        string ret;
//        vector<string> vs;
//        //先进行排序，通过特定的规则
//        for (auto e : nums)
//        {
//            vs.push_back(to_string(e));
//        }
//        sort(vs.begin(),vs.end(),[&](const string& s1,const string& s2) {
//            return s1 + s2 > s2 + s1;
//        });
//        for (auto& str : vs) ret += str;
//        if (ret[0] == '0') return "0"; //处理前导0
//        return ret;
//    }
//};