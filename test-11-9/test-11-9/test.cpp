#include <iostream>
#include <vector>
using namespace std;

//class Solution {
//    const int INF = 0x3f3f3f3f;
//public:
//    int coinChange(vector<int>& coins, int amount) {
//        int n = coins.size();
//        vector<vector<int>> dp(n + 1, vector<int>(amount + 1));
//        //初始化
//        for (int j = 1; j <= amount; ++j) dp[0][j] = INF;
//        for (int i = 1; i <= n; ++i)
//        {
//            for (int j = 0; j <= amount; ++j)
//            {
//                dp[i][j] = dp[i - 1][j]; //不选
//                if (j >= coins[i - 1])
//                    dp[i][j] = min(dp[i][j], dp[i][j - coins[i - 1]] + 1);
//            }
//        }
//        return dp[n][amount] >= INF ? -1 : dp[n][amount];
//    }
//};


//利用滚动数组优化
//class Solution {
//    const int INF = 0x3f3f3f3f;
//public:
//    int coinChange(vector<int>& coins, int amount) {
//        int n = coins.size();
//        vector<int> dp(amount + 1);
//        //初始化
//        for (int j = 1; j <= amount; ++j) dp[j] = INF;
//        for (int i = 1; i <= n; ++i)
//        {
//            for (int j = coins[i - 1]; j <= amount; ++j)
//            {
//                dp[j] = min(dp[j], dp[j - coins[i - 1]] + 1);
//            }
//        }
//        return dp[amount] >= INF ? -1 : dp[amount];
//    }
//};


//class Solution {
//public:
//    int change(int amount, vector<int>& coins) {
//        int n = coins.size();
//        vector<int> dp(amount + 1);
//        //初始化
//        dp[0] = 1;
//        for (int i = 1; i <= n; ++i)
//        {
//            for (int j = coins[i - 1]; j <= amount; ++j)
//                dp[j] += dp[j - coins[i - 1]];
//        }
//        return dp[amount];
//    }
//};

//
//class Solution {
//public:
//    int numSquares(int n) {
//        vector<int> dp(n + 1);
//        //初始化
//        dp[0] = 0;
//        dp[1] = 1;
//        for (int i = 2; i <= n; ++i)
//        {
//            dp[i] = dp[i - 1] + 1; //至少等于前一个+1
//            for (int j = 2; j * j <= i; ++j)
//            {
//                dp[i] = min(dp[i], dp[i - j * j] + 1);
//            }
//        }
//        return dp[n];
//    }
//};


//class Solution {
//public:
//    int findMaxForm(vector<string>& strs, int m, int n) {
//        vector<vector<int>> dp(m + 1, vector<int>(n + 1));
//        for (int i = 0; i < strs.size(); ++i)
//        {
//            //统计当前字符串中的0和1的个数
//            int zero = 0, one = 0;
//            for (auto ch : strs[i])
//            {
//                if (ch == '0') zero++;
//                else ++one;
//            }
//            for (int j = m; j >= zero; --j)
//            {
//                for (int k = n; k >= one; --k)
//                {
//                    dp[j][k] = max(dp[j][k], dp[j - zero][k - one] + 1);
//                }
//            }
//        }
//        return dp[m][n];
//    }
//};


