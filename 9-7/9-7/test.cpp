#include <iostream>
#include <vector>
#include <unordered_map>
using namespace std;

//int main() {
//    int n = 3;
//    vector<int> arr = {1,2,3};
//    int singleNum = 0;
//    double singleSum = 0.0;   //统计单数位之和
//    int doubleNum = 0;
//    double doubleSum = 0.0;   //统计双数位之和
//    for (auto e : arr)
//    {
//        if (e % 2 == 0)
//        {
//            ++doubleNum;
//            doubleSum += e;
//        }
//        else {
//            ++singleNum;
//            singleSum += e;
//        }
//    }
//    if (singleNum + singleSum < doubleNum + doubleSum) cout << singleNum + singleSum << endl;
//    else cout << doubleNum + doubleSum << endl;
//    return 0;
//}


//int GetSum(int val)
//{
//    //C32
//    int ret = 1;
//    for (int i = 0; i < 2; ++i)
//    {
//        ret *= val;
//        --val;
//    }
//    return ret / 2;
//}
//
//int main() {
//    int T;
//    cin >> T;
//    while (T--)
//    {
//        int n;
//        cin >> n;
//        unordered_map<int, pair<int, int>> hash;   //用来映射当前根和子树的数量关系
//        while (--n)   //输入n-1组
//        {
//            int root, son;
//            cin >> root >> son;
//            //这里放入hash中的时候需要把两个节点都放入到hash中
//            if (hash.count(root) == 0)
//            {
//                hash[root].first = son;
//                hash[root].second = 0;
//            }
//            else
//            {
//                if (hash[root].first == 0) hash[root].first = son;
//                else hash[root].second = son;   //这里前面已经记录过另外一个子节点
//            }
//            //添加子节点
//            if (hash.count(son) == 0)
//            {
//                hash[son].first = 0;
//                hash[son].second = 0;
//            }
//        }
//        int zero = 0;  //没有子节点
//        int one = 0;
//        int two = 0;
//        int sum = 0;   //统计所有相似子节点的个数
//        for (auto& it : hash)
//        {
//            if (it.second.first == 0) ++zero;
//            else if (it.second.second == 0) ++one;
//            else ++two;
//        }
//        //利用排列组合的计算方式来计算总和
//        sum += GetSum(zero);
//        sum += GetSum(one);
//        sum += GetSum(two);
//        cout << sum << endl;
//    }
//    return 0;
//}



//#include <iostream>
//using namespace std;
//
//int GetVal(int val)
//{
//    int num = 0;
//    while (val > 0)
//    {
//        int last = val % 10;
//        if (last == 4 || last == 6) ++num;
//        val /= 10;
//    }
//    return num;
//}
//
//int main() {
//    int m, n;
//    cin >> m >> n;
//    int biggest = m;   //相同4，6数量的最大的那个数
//    int num = 0;       //4,6的个数
//    for (int i = m; i <= n; ++i)
//    {
//        int sum = GetVal(i);   //获取到一个数中4，6的个数
//        if (sum >= num)
//        {
//            num = sum;
//            biggest = i;
//        }
//    }
//    cout << biggest << endl;
//    return 0;
//}



//#include <iostream>
//#include <string>
//#include <vector>
//using namespace std;
//
//int m, n;
//int dx[4] = { 0, 0, 1, -1 };
//int dy[4] = { 1, -1, 0, 0 };
//int ret = 0;
//
//void dfs(vector<string>& vs, vector<vector<bool>>& vis, int i, int j,
//    bool flag, int& total) {
//    ++total;
//    for (int k = 0; k < 4; ++k) {
//        int x = i + dx[k], y = j + dy[k];
//        if (x >= 0 && x < m && y >= 0 && y < n && (vs[x][y] == '.' ||
//            vs[x][y] == '#')) {
//            if (vs[x][y] == '.') {
//                if (flag == true) {
//                    vs[x][y] = ' ';
//                    vis[x][y] = true;
//                }
//                else {
//                    vs[x][y] = '.';
//                    vis[x][y] = false;
//                }
//            }
//            dfs(vs, vis, x, y, flag, total);
//        }
//    }
//}
//
//void FindSum(vector<string>& vs, vector<vector<bool>>& vis) {
//    //找到其中一个红海，然后计算这片红海和陆地的总面积
//    for (int i = 0; i < m; ++i) {
//        for (int j = 0; j < n; ++j) {
//            if (vs[i][j] == '.') {
//                //把点换成空格，后续再还原回来
//                int sum = 0;
//                dfs(vs, vis, i, j, true, sum);
//                //获取到结果
//                ret = max(ret, sum);
//                //还原
//                dfs(vs, vis, i, j, false, sum);
//            }
//        }
//    }
//}
//
//
//int main() {
//    cin >> m >> n;
//    vector<string> vs(m, string(n, ' '));
//    vector<vector<bool>> vis(m, vector<bool>(n, false));
//    for (int i = 0; i < m; ++i) {
//        for (int j = 0; j < n; ++j) {
//            cin >> vs[i][j];
//            if (vs[i][j] == '#') vis[i][j] = true;
//        }
//    }
//    FindSum(vs, vis);
//    cout << ret << endl;
//    return 0;
//}


#include <iostream>
#include <string>
#include <vector>
using namespace std;

int m, n;
int dx[4] = { 0, 0, 1, -1 };
int dy[4] = { 1, -1, 0, 0 };
int ret = 0;

void dfs(vector<string>& vs, int i, int j,
    int& total) {
    ++total;
    for (int k = 0; k < 4; ++k) {
        int x = i + dx[k], y = j + dy[k];
        if (vs[i][j] == '#' && vs[x][y] == '.') continue;  //这里是不满足条件的
        if (x >= 0 && x < m && y >= 0 && y < n && vs[x][y] != ' ')
        {
            if (vs[x][y] == '.') {
                vs[x][y] = ' ';
            }
            dfs(vs, x, y, total);
        }
    }
}

void FindSum(vector<string>& vs) {
    //找到其中一个红海，然后计算这片红海和陆地的总面积
    for (int i = 0; i < m; ++i) {
        for (int j = 0; j < n; ++j) {
            if (vs[i][j] == '.') {
                //把点换成空格
                int sum = 0;
                dfs(vs, i, j, sum);
                //获取到结果
                ret = max(ret, sum);
            }
        }
    }
}


int main() {
    cin >> m >> n;
    vector<string> vs(m, string(n, ' '));
    for (int i = 0; i < m; ++i) {
        for (int j = 0; j < n; ++j) {
            cin >> vs[i][j];
        }
    }
    FindSum(vs);
    cout << ret << endl;
    return 0;
}