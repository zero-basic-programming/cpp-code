#include <iostream>
#include <vector>
using namespace std;

//class Solution {
//public:
//    int maxDepthHelper(TreeNode* root)
//    {
//        if (root == nullptr) return 0;
//        int left = maxDepthHelper(root->left);
//        int right = maxDepthHelper(root->right);
//        return left > right ? left + 1 : right + 1;
//    }
//    int maxDepth(TreeNode* root) {
//        if (root == nullptr) return 0;
//        return maxDepthHelper(root);
//    }
//};

//class Solution {
//public:
//    int maxDepth(TreeNode* root) {
//        if (root == nullptr) return 0;
//        queue<TreeNode*> q;
//        q.push(root);
//        int size = 0;//记录层数
//        while (!q.empty())
//        {
//            int count = q.size();
//            while (count--)
//            {
//                TreeNode* node = q.front();
//                q.pop();
//                //放入左右节点
//                if (node->left)  q.push(node->left);
//                if (node->right)  q.push(node->right);
//            }
//            ++size;
//        }
//        return size;
//    }
//};

//class Solution {
//public:
//    int maxDepth(Node* root) {
//        if (root == nullptr) return 0;
//        int maxsize = 0;
//        for (auto& e : root->children)
//        {
//            maxsize = max(maxsize, maxDepth(e));
//        }
//        return maxsize + 1;
//    }
//};


//class Solution {
//public:
//    int maxDepth(Node* root) {
//        if (root == nullptr) return 0;
//        queue<Node*> q;
//        q.push(root);
//        int depth = 0;
//        while (!q.empty())
//        {
//            int size = q.size();
//            while (size--)
//            {
//                Node* node = q.front();
//                q.pop();
//                //把子树非空节点放入队列
//                for (auto& e : node->children)
//                {
//                    if (e)
//                    {
//                        q.push(e);
//                    }
//                }
//            }
//            ++depth;
//        }
//        return depth;
//    }
//};


//class Solution {
//public:
//    int minDepth(TreeNode* root) {
//        //主要是处理根的左右节点是否为空的问题
//        if (root == nullptr) return 0;
//        //找左右子树的高度
//        int leftdepth = minDepth(root->left);
//        int rightdepth = minDepth(root->right);
//        if (root->left == nullptr && root->right != nullptr)
//            return rightdepth + 1;
//        if (root->left != nullptr && root->right == nullptr)
//            return leftdepth + 1;
//        //左右都不为空
//        return 1 + min(leftdepth, rightdepth);
//    }
//};

//#include <queue>
//class Solution {
//public:
//    int minDepth(TreeNode* root) {
//        //非递归 
//        if (root == nullptr) return 0;
//        queue<TreeNode*> q;
//        q.push(root);
//        int depth = 0;
//        while (!q.empty())
//        {
//            int size = q.size();
//            ++depth;
//            while (size--)
//            {
//                TreeNode* node = q.front();
//                q.pop();
//                //放入左右节点
//                if (node->left)  q.push(node->left);
//                if (node->right)  q.push(node->right);
//                if (!node->left && !node->right)
//                {
//                    return depth;
//                }
//            }
//        }
//        return depth;
//    }
//};


//class Solution {
//public:
//    int countNodesHelper(TreeNode* root, int& count)
//    {
//        if (root == nullptr) return 0;
//        ++count;
//        countNodesHelper(root->left, count);
//        countNodesHelper(root->right, count);
//        return count;
//    }
//    int countNodes(TreeNode* root) {
//        if (root == nullptr) return 0;
//        int count = 0;
//        return countNodesHelper(root, count);
//    }
//};


//class Solution {
//public:
//    bool getHeight(TreeNode* root)
//    {
//        if (root == nullptr) return 0;
//        int left = getHeight(root->left);
//        if (left == -1)  return -1;
//        int right = getHeight(root->right);
//        if (right == -1)  return -1;
//        return abs(left - right) > 1 ? -1 : 1 + max(left, right);
//
//    }
//    bool isBalanced(TreeNode* root) {
//        return getHeight(root) == -1 ? false : true;
//    }
//};

//class Solution {
//public:
//    void binaryTreePathsHelper(TreeNode* root, vector<string>& result, vector<int>& tmp)
//    {
//        //把路径放入临时tmp中,这里必须放在这里，因为如果放后面那么最后一个节点就没有放入路径中了
//        tmp.push_back(root->val);
//        if (root->left == nullptr && root->right == nullptr)
//        {
//            //转换成字符串
//            string s;
//            for (int i = 0; i < tmp.size() - 1; ++i)
//            {
//                s += to_string(tmp[i]);
//                s += "->";
//            }
//            //保存最后一个
//            s += to_string(tmp[tmp.size() - 1]);
//            //保存路径
//            result.push_back(s);
//            return;
//        }
//        if (root->left)
//        {
//            binaryTreePathsHelper(root->left, result, tmp);
//            //回退
//            tmp.pop_back();
//        }
//        if (root->right)
//        {
//            binaryTreePathsHelper(root->right, result, tmp);
//            //回退
//            tmp.pop_back();
//        }
//    }
//    vector<string> binaryTreePaths(TreeNode* root) {
//        vector<string> result;
//        if (root == nullptr) return result;
//        vector<int> tmp; // 保存当前路径
//        binaryTreePathsHelper(root, result, tmp);
//        return result;
//    }
//};

