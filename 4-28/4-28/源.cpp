#include <iostream>
#include <vector>
#include <stack>
using namespace std;

void Qsort(vector<int>& arr, int l, int r)
{
	if (l >= r) return;
	int key = arr[rand() % (r - l + 1) + l];
	int left = l - 1, right = r + 1, i = l;
	while (i < right)
	{
		if (arr[i] < key) swap(arr[i++], arr[++left]);
		else if (arr[i] == key) ++i;
		else swap(arr[--right], arr[i]);
	}
	Qsort(arr, l, left);
	Qsort(arr, right, r);
}

void QsortNonR(vector<int>& arr)
{
	int left = 0, right = arr.size() - 1;
	stack<int> st;
	st.push(right);
	st.push(left);
	while (st.size())
	{
		int l = st.top(); st.pop();
		int r = st.top(); st.pop();
		int key = arr[rand() % (r - l + 1) + l];
		int ll = l - 1, rr = r + 1, i = l;
		while (i < rr)
		{
			if (arr[i] < key) swap(arr[i++], arr[++ll]);
			else if (arr[i] == key) ++i;
			else swap(arr[--rr], arr[i]);
		}
		if (l < ll)
		{
			st.push(ll); 
			st.push(l);
		}
		if (rr < r) {
			st.push(r); 
			st.push(rr);
		}
	}
}

void MergeSort(vector<int>& arr, int left, int right, vector<int>& tmp)
{
	if (left >= right) return;
	int mid = (left + right) >> 1;
	MergeSort(arr, left, mid, tmp);
	MergeSort(arr, mid + 1, right, tmp);
	int begin1 = left, end1 = mid;
	int begin2 = mid + 1, end2 = right;
	int i = left;
	while (begin1 <= end1 && begin2 <= end2)
	{
		tmp[i++] = arr[begin1] < arr[begin2] ? arr[begin1++] : arr[begin2++];
	}
	while (begin1 <= end1)
	{
		tmp[i++] = arr[begin1++];
	}
	while (begin2 <= end2)
	{
		tmp[i++] = arr[begin2++];
	}
	//还原
	for (int j = left; j <= right; ++j)
		arr[j] = tmp[j];
}

void MergeSortNonR(vector<int>& arr, vector<int>& tmp)
{
	int n = arr.size();
	int gap = 1;
	while (gap < n)
	{
		for (int i = 0; i < n; i += 2 * gap)
		{
			int begin1 = i, end1 = i + gap - 1;
			int begin2 = i + gap, end2 = i + 2 * gap - 1;
			//判断边界条件
			if (begin2 >= n) break;  //最后一组的第二组不存在
			if (end2 >= n) end2 = n - 1;
			//单次的归并排序
			int j = i, k = i;  //j用来，k用来还原
			while (begin1 <= end1 && begin1 <= end2)
			{
				tmp[j++] = arr[begin1] < arr[begin2] ? arr[begin1++] : arr[begin2++];
			}
			while (begin1 <= end1)
			{
				tmp[j++] = arr[begin1++];
			}
			while (begin2 <= end2)
			{
				tmp[j++] = arr[begin2++];
			}
			//还原
			for (; k <= end2; ++k)
			{
				arr[k] = tmp[k];
			}
		}
		gap *= 2;
	}
}


int main()
{
	vector<int> arr = { 1,3,6,4,4,2,5,6,7,8,9,0,8 };
	int n = arr.size();
	vector<int> tmp(n);
	//Qsort(arr, 0, n - 1);
	//QsortNonR(arr);
	//MergeSort(arr, 0, n - 1, tmp);
	MergeSortNonR(arr, tmp);
	for (auto e : arr)
	{
		cout << e << " ";
	}
	return 0;
}