#include <iostream>
#include <vector>
using namespace std;

////快速选择排序
//void Qsort(vector<int>& arr, int l, int r)
//{
//	if (l >= r) return;
//	int key = arr[rand() % (r - l + 1) + l];
//	int left = l - 1, right = r + 1;
//	int i = l;
//	while (i < right)
//	{
//		if (arr[i] < key) swap(arr[i++], arr[++left]);
//		else if (arr[i] == key) ++i;
//		else swap(arr[--right], arr[i]);
//	}
//	Qsort(arr, l, left);
//	Qsort(arr, right, r);
//}
//
////堆排序 
//void MergeSort(vector<int>& arr,int left,int right,vector<int>& tmp)
//{
//	if (left >= right) return;
//	int mid = left + (right - left)/2;
//	MergeSort(arr, left, mid, tmp);
//	MergeSort(arr, mid+1, right,tmp);
//	int begin1 = left, end1 = mid;
//	int begin2 = mid + 1, end2 = right;
//	int i = left;
//	while (begin1 <= end1 && begin2 <= end2)
//	{
//		tmp[i++] = arr[begin1] < arr[begin2] ? arr[begin1++] : arr[begin2++];
//	}
//	while (begin1 <= end1)
//	{
//		tmp[i++] = arr[begin1++];
//	}
//	while (begin2 <= end2) tmp[i++] = arr[begin2++];
//	//还原
//	for (int j = left; j <= right; ++j) arr[j] = tmp[j];
//
//}
//
//int main()
//{
//	vector<int> arr = { 21,43,3,5,26,7,5,78,8,0 };
//	int n = arr.size();
//	//Qsort(arr, 0, n - 1);
//	vector<int> tmp = arr;
//	MergeSort(arr, 0, n - 1, tmp);
//	for (auto e : arr)
//	{
//		cout << e << endl;
//	}
//	return 0;
//}


vector<int> GetNext(string& t)
{
	vector<int> next;
	int prefix_len = 0, i = 1;
	while (i < t.size())
	{
		if (t[prefix_len] == t[i])
		{
			++prefix_len;
			next.push_back(prefix_len);
			++i;
		}
		else
		{
			if (prefix_len == 0)
			{
				//此时说明已经没有前缀了，这时候直接收集结果
				next.push_back(0);
				++i;
			}
			else
			{
				//通过前缀来获取到其有可能有的更优的结果
				prefix_len = next[prefix_len - 1];
			}
		}
	}
	return next;
}

int main()
{
	string s = "abababcaa";
	string t = "ababc";
	vector<int> next = GetNext(t);
	//找出相同的数组的起始下标，如果没有相同就是-1
	int i = 0, j = 0;
	while (i < s.size())
	{
		if (s[i] == t[j])
		{
			++i;
			++j;
		}
		else
		{
			j = next[j - 1];
		}
		if (j == t.size()) break;
	}
	cout << i << endl;
	return 0;
}