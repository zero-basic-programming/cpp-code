#include <iostream>
#include <vector>
using namespace std;


//class Solution {
//public:
//    int lengthOfLongestSubstring(string s) {
//        int left = 0, right = 0;
//        int hash[128] = { 0 };
//        int ret = 0;
//        while (right < s.size())
//        {
//            hash[s[right]]++; //进窗口
//            //判断
//            while (hash[s[right]] > 1)
//            {
//                //出窗口
//                hash[s[left++]]--;
//            }
//            //收集结果
//            ret = max(ret, right - left + 1);
//            ++right;
//        }
//        return ret;
//    }
//};


//class Solution {
//    //使用hash的方式去记录横排纵列以及格子里的元素是否出现过
//    bool row[9][10];
//    bool col[9][10];
//    bool lattic[3][3][10];
//public:
//    bool isValidSudoku(vector<vector<char>>& board) {
//        int m = board.size(), n = board[0].size();
//        for (int i = 0; i < m; ++i)
//        {
//            for (int j = 0; j < n; ++j)
//            {
//                if (board[i][j] != '.')
//                {
//                    int num = board[i][j] - '0';
//                    if (row[i][num] || col[j][num] || lattic[i / 3][j / 3][num]) return false;
//                    row[i][num] = col[j][num] = lattic[i / 3][j / 3][num] = true;
//                }
//            }
//        }
//        return true;
//    }
//};



//class Solution {
//public:
//    vector<int> spiralOrder(vector<vector<int>>& matrix) {
//        vector<int> ret;
//        int m = matrix.size(), n = matrix[0].size();
//        int times = min(m / 2, n / 2) + 1; //求出循环次数
//        int x = 0, y = 0; //循环起始坐标
//        int offset = 1; //偏移量
//        int i, j;
//        while (times--)
//        {
//            i = x, j = y;
//            //4个for直接遍历一层
//            for (int j = y; j < n - offset; ++j)
//            {
//                ret.push_back(matrix[x][j]);
//            }
//            for (int i = x; i < m - offset; ++i)
//            {
//                ret.push_back(matrix[i][j]);
//            }
//            for (; j > y; --j)
//            {
//                ret.push_back(matrix[i][j]);
//            }
//            for (; i > x; --i)
//            {
//                ret.push_back(matrix[i][j]);
//            }
//            ++x, ++y;
//            ++offset;
//        }
//        return ret;
//    }
//};


//class Solution {
//public:
//    bool canConstruct(string ransomNote, string magazine) {
//        int hash[26] = { 0 };
//        for (auto ch : magazine)
//        {
//            hash[ch - 'a']++;
//        }
//        for (auto ch : ransomNote)
//        {
//            if (hash[ch - 'a'] > 0)
//            {
//                --hash[ch - 'a'];
//            }
//            else return false;
//        }
//        return true;
//    }
//};


