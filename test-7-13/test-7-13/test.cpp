#include <iostream>
#include <cmath>
using namespace std;

//int main() {
//    int n;
//    while (cin >> n)
//    {
//        int res = pow(n - 1, n - 2);
//        cout << res << endl;
//    }
//}



#include <vector>


int main() {
    int n;
    while (cin >> n)
    {
        vector<int> nums(n, 0);
        for (auto& v : nums)
        {
            int tmp;
            cin >> tmp;
            v = tmp;
        }
        vector<int> dp(n, 0);
        //dp数组初始化
        dp[0] = 1;
        long long result = 0;
        //遍历，并推转移方程
        for (int i = 1; i < n; ++i)
        {
            //这层遍历dp
            for (int j = 0; j < i; ++j)
            {
                //遍历原来的数据，和当前i的数据进行比较
                if (nums[i] > nums[j])
                {
                    //转移方程
                    dp[i] = max(dp[i], dp[j] + 1);
                }
                //这里保存一下结果，后面就少一次遍历
                if (result < dp[i])  result = dp[i];
            }
        }
        cout << result << endl;
    }
    return 0;
}