#include <iostream>
using namespace std;

//class Solution {
//public:
//    vector<int> preorderTraversal(TreeNode* root) {
//        //使用非递归
//        stack<TreeNode*> st;
//        vector<int> result;
//        if (root == nullptr)     return result;
//        st.push(root);
//        while (!st.empty())
//        {
//            TreeNode* cur = st.top();
//            result.push_back(cur->val);
//            st.pop();
//            //放入左右子树
//            if (cur->right) st.push(cur->right);
//            if (cur->left) st.push(cur->left);
//        }
//        return result;
//    }
//};

//class Solution {
//public:
//    vector<int> inorderTraversal(TreeNode* root) {
//        //非递归
//        stack<TreeNode*> st;
//        vector<int> result;
//        if (root == nullptr)     return result;
//        TreeNode* cur = root;
//        while (cur || !st.empty())
//        {
//            if (cur)
//            {
//                st.push(cur);
//                cur = cur->left;
//            }
//            else
//            {
//                cur = st.top();
//                st.pop();
//                result.push_back(cur->val);
//                cur = cur->right;
//            }
//        }
//        return result;
//    }
//};

//class Solution {
//public:
//    vector<int> postorderTraversal(TreeNode* root) {
//        vector<int> v;
//        stack<TreeNode*> st;
//        TreeNode* cur = root;
//        TreeNode* prev = nullptr;//用来记录根的右子树是否被访问
//        while (!st.empty() || cur)
//        {
//            while (cur)
//            {
//                st.push(cur);
//                cur = cur->left;
//            }
//            TreeNode* top = st.top();
//            //如果右子树为空或者到最右端返回的时候就回收结果
//            if (top->right == nullptr || top->right == prev)
//            {
//                st.pop();
//                v.push_back(top->val);
//                prev = top;//从最右端回来的时候起重要作用
//            }
//            else
//            {
//                //这时候要往右迭代
//                cur = top->right;
//            }
//        }
//        return v;
//    }
//};

