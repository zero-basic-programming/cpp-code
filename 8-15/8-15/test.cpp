#include <iostream>
#include <vector>
using namespace std;


//class Solution {
//public:
//    bool checkInclusion(string s1, string s2) {
//        int hash1[26] = { 0 };   //用来记录s1中的字符与数量的映射关系
//        int hash2[26] = { 0 };   //用来记录窗口
//        for (auto ch : s1) hash1[ch - 'a']++;
//        int left = 0, right = 0, n = s2.size();
//        int count = 0, sz = s1.size();   //count用来记录当前窗口中已经覆盖了s1中的个数
//        while (right < n)
//        {
//            //进窗口
//            int i = s2[right] - 'a';
//            ++hash2[i];
//            if (hash2[i] <= hash1[i]) ++count;
//            //判断
//            if (hash1[i] == 0)
//            {
//                //这里说明字符是没有出现过的，直接将窗口移动到right位置
//                left = right;
//                right++;
//                count = 0;
//                continue;
//            }
//            while (hash2[i] > hash1[i])
//            {
//                --count;
//                --hash2[s2[left++] - 'a'];
//            }
//            //收集结果
//            if (count == sz) return true;
//            ++right;
//        }
//        return false;
//    }
//};


//class Solution {
//public:
//    bool checkInclusion(string s1, string s2) {
//        int hash[26] = { 0 };   //用来记录字符和次数的关系
//        int n = s2.size();
//        int len = s1.size();   //这里用于比较窗口的大小是否与len长度相等
//        for (auto ch : s1) --hash[ch - 'a'];
//        //滑动窗口
//        for (int left = 0, right = 0; right < n; ++right)
//        {
//            //进窗口
//            int i = s2[right] - 'a';   //right字符对应hash的下标
//            ++hash[i];
//            while (hash[i] > 0)
//            {
//                //出窗口
//                --hash[s2[left++] - 'a'];
//            }
//            //收集结果
//            if (right - left + 1 == len) return true;
//        }
//        return false;
//    }
//};


//class Solution {
//public:
//    vector<int> findAnagrams(string s, string p) {
//        int hash[26] = { 0 };
//        int len = p.size();
//        int n = s.size();
//        vector<int> ret;
//        for (auto ch : p) --hash[ch - 'a'];
//        for (int left = 0, right = 0; right < n; ++right)
//        {
//            int i = s[right] - 'a';
//            ++hash[i];
//            while (hash[i] > 0)
//            {
//                --hash[s[left++] - 'a'];
//            }
//            //收集结果
//            if (right - left + 1 == len) ret.push_back(left);
//        }
//        return ret;
//    }
//};


//class Solution {
//public:
//    string minWindow(string s, string t) {
//        int len = t.size(), n = s.size();
//        int hash[128] = { 0 };
//        for (auto ch : t) --hash[ch];
//        int count = 0;   //用来记录窗口实际包含t中字符的数量
//        string ret = "";
//        for (int left = 0, right = 0; right < n; ++right)
//        {
//            //进窗口
//            if (++hash[s[right]] <= 0) ++count;
//            //判断
//            while (count == len)
//            {
//                //收集结果+出窗口
//                if (ret.empty() || right - left + 1 < ret.size())
//                    ret = s.substr(left, right - left + 1);
//                if (--hash[s[left++]] < 0) --count;
//            }
//        }
//        return ret;
//    }
//};



