#include <iostream>
#include <stack>
#include <vector>
using namespace std;

//int main()
//{
//    int T = 0;
//    cin >> T;//记录组数
//    while (T--)
//    {
//        int n, k;
//        cin >> n >> k;
//        vector<int> v1;//存放前面N个数
//        vector<int> v2;//存放后n个数
//        int tmp = n;
//        while (tmp--)
//        {
//            int x;
//            cin >> x;
//            v1.push_back(x);
//        }
//        tmp = n;
//        while (tmp--)
//        {
//            int x;
//            cin >> x;
//            v2.push_back(x);
//        }
//        stack<int> s;
//        while (k--)
//        {
//            int i = n - 1;
//            //将元素放入栈中
//            tmp = n;
//            while (tmp--)
//            {
//                s.push(v2[i]);
//                s.push(v1[i--]);
//            }
//            v1.clear();
//            v2.clear();
//            //重新放入vector中
//            tmp = n;
//            while (tmp--)
//            {
//                v1.push_back(s.top());
//                s.pop();
//            }
//            while (!s.empty())
//            {
//                v2.push_back(s.top());
//                s.pop();
//            }
//        }
//        //打印结果
//        for (auto& e : v1)
//        {
//            cout << e << " ";
//        }
//        for (auto& e : v2)
//        {
//            cout << e << " ";
//        }
//        cout << endl;
//    }
//    return 0;
//}

//#include <iostream>
//#include <unordered_map>
//#include <string>
//using namespace std;
//
//int main()
//{
//    string s1, s2;
//    cin >> s1 >> s2;
//    //使用hash保存他们的kv值
//    unordered_map<char, int> mp;
//    for (auto& ch : s1)
//    {
//        mp[ch]++;
//    }
//    for (auto& ch : s2)
//    {
//        if (mp.find(ch) != mp.end())
//        {
//            --mp[ch];
//        }
//        else
//        {
//            ++mp[ch];
//        }
//    }
//    int result = 0;
//    for (auto& e : mp)
//    {
//        result += abs(e.second);
//    }
//    cout << result << endl;
//    return 0;
//}

//class Gift {
//public:
//    int getValue(vector<int> gifts, int n) {
//        //使用抵消的方法
//        int tmp = gifts[0];//用来记录当前的数据
//        int count = 1;//用来记录当前出现过的次数
//        for (int i = 1; i < n; ++i)
//        {
//            if (gifts[i] != tmp)
//            {
//                --count;
//            }
//            else
//            {
//                ++count;
//            }
//            //更新count
//            if (count == 0 && i < n - 1)
//            {
//                tmp = gifts[i++];
//                count = 1;
//            }
//        }
//        //到这里没有结果，所以还要遍历一次
//        count = 0;
//        for (auto& e : gifts)
//        {
//            if (e == tmp)
//            {
//                ++count;
//            }
//        }
//        return count > n / 2 ? tmp : 0;
//    }
//};