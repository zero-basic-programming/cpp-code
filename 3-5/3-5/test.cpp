#include <iostream>
using namespace std;

//class LRUCache {
//    typedef list<pair<int, int>>::iterator Liter;
//public:
//    LRUCache(int capacity)
//        :_capacity(capacity) {
//
//    }
//
//    int get(int key) {
//        //在hash中查找，如果没有找到，直接返回-1
//        //找到了，更新数据到list的最后，然后返回值
//        auto it = _hash.find(key);
//        if (it == _hash.end())
//        {
//            return -1;
//        }
//        else
//        {
//            Liter iter = it->second;
//            _list.splice(_list.end(), _list, iter);   //这里可以保证迭代器不失效
//            return iter->second;
//        }
//    }
//
//    void put(int key, int value) {
//        //在hash中查找，如果找到就更新，同时把其放入到最后一个位置
//        //没有找到就插入,如果list的个数超过capacity就需要进行头删,同时需要维护好hash
//        auto it = _hash.find(key);
//        if (it == _hash.end())
//        {
//            //没找到
//            if (_capacity == _list.size())
//            {
//                //插入加删除
//                _hash.erase(_list.front().first);
//                _list.pop_front();
//            }
//            _list.push_back(make_pair(key, value));
//            cout << "key : " << key;
//            _hash[key] = _list.end();
//        }
//        else
//        {
//            cout << "key: " << key;
//            //找到了，更新
//            Liter iter = _hash[key];
//            iter->second = value;  //更新
//            _list.splice(_list.end(), _list, iter);   //这里可以保证迭代器不失效
//        }
//    }
//private:
//    list<pair<int, int>> _list;  //用来存放数据
//    //第一个用来存放K，第二个存放list的迭代器，用来访问list
//    unordered_map<int, Liter> _hash;
//    int _capacity;
//};