#include <iostream>
#include <vector>
using namespace std;

//class Solution {
//public:
//    int longestCommonSubsequence(string text1, string text2) {
//        int m = text1.size(), n = text2.size();
//        vector<vector<int>> dp(m + 1, vector<int>(n + 1, 0));
//        for (int i = 1; i <= m; ++i)
//        {
//            for (int j = 1; j <= n; ++j)
//            {
//                if (text1[i - 1] == text2[j - 1]) dp[i][j] = dp[i - 1][j - 1] + 1;
//                else dp[i][j] = max(dp[i - 1][j], dp[i][j - 1]);
//            }
//        }
//        return dp[m][n];
//    }
//};


//class Solution {
//public:
//    int maxUncrossedLines(vector<int>& nums1, vector<int>& nums2) {
//        int m = nums1.size(), n = nums2.size();
//        vector<vector<int>> dp(m + 1, vector<int>(n + 1));
//        for (int i = 1; i <= m; ++i)
//        {
//            for (int j = 1; j <= n; ++j)
//            {
//                if (nums1[i - 1] == nums2[j - 1]) dp[i][j] = dp[i - 1][j - 1] + 1;
//                else dp[i][j] = max(dp[i - 1][j], dp[i][j - 1]);
//            }
//        }
//        return dp[m][n];
//    }
//};


//class Solution {
//public:
//    int numDistinct(string s, string t) {
//        int n = s.size(), m = t.size();
//        s = ' ' + s;
//        t = ' ' + t;
//        vector<vector<double>> dp(m + 1, vector<double>(n + 1));
//        //第一行初始化为1
//        for (int i = 0; i <= n; ++i) dp[0][i] = 1;
//        for (int i = 1; i <= m; ++i)
//        {
//            for (int j = 1; j <= n; ++j)
//            {
//                dp[i][j] = dp[i][j - 1];  //继承上一次的结果
//                if (s[j] == t[i]) dp[i][j] += dp[i - 1][j - 1];
//            }
//        }
//        return dp[m][n];
//    }
//};



//class Solution {
//public:
//    bool isMatch(string s, string p) {
//        int m = s.size(), n = p.size();
//        vector<vector<bool>> dp(m + 1, vector<bool>(n + 1, false));
//        s = ' ' + s;
//        p = ' ' + p;
//        //初始化
//        dp[0][0] = true;
//        for (int j = 1; j <= n; ++j)
//            if (p[j] == '*') dp[0][j] = true;
//            else break;
//        for (int i = 1; i <= m; ++i)
//        {
//            for (int j = 1; j <= n; ++j)
//            {
//                if (p[j] == '*') dp[i][j] = dp[i][j - 1] || dp[i - 1][j];
//                else dp[i][j] = (p[j] == '?' || s[i] == p[j]) && dp[i - 1][j - 1];
//            }
//        }
//        return dp[m][n];
//    }
//};



//class Solution {
//public:
//    int lengthOfLongestSubstring(string s) {
//        int hash[128] = { 0 };  //用来统计元素个数
//        int left = 0, right = 0, n = s.size();
//        int len = 0;
//        while (right < n)
//        {
//            //进窗口
//            hash[s[right]]++;
//            //判断
//            while (hash[s[right]] > 1)
//            {
//                --hash[s[left++]];
//            }
//            //收集结果
//            if (right - left + 1 > len) len = max(len, right - left + 1);
//            ++right;
//        }
//        return len;
//    }
//};


//class Solution {
//public:
//    bool isMatch(string s, string p) {
//        int m = s.size(), n = p.size();
//        vector<vector<bool>> dp(m + 1, vector<bool>(n + 1, false));
//        s = ' ' + s;
//        p = ' ' + p;
//        //初始化
//        dp[0][0] = true;
//        for (int j = 2; j <= n; j += 2)
//            if (p[j] == '*') dp[0][j] = true;
//            else break;
//        for (int i = 1; i <= m; ++i)
//        {
//            for (int j = 1; j <= n; ++j)
//            {
//                if (p[j] == '*')
//                {
//                    dp[i][j] = dp[i][j - 2] || ((p[j - 1] == '.' || p[j - 1] == s[i]) && dp[i - 1][j]);
//                }
//                else
//                {
//                    dp[i][j] = (s[i] == p[j] || p[j] == '.') && dp[i - 1][j - 1];
//                }
//            }
//        }
//        return dp[m][n];
//    }
//};


//class Solution {
//public:
//    bool isInterleave(string s1, string s2, string s3) {
//        int i = 0, j = 0, k = 0;
//        int n = s3.size();
//        while (k < n)
//        {
//            if (s1[i] == s3[k])
//            {
//                ++i;
//                ++k;
//            }
//            else if (s2[j] == s3[k])
//            {
//                ++j;
//                ++k;
//            }
//            else return false;
//        }
//        return true;
//    }
//};


//class Solution {
//public:
//    bool isInterleave(string s1, string s2, string s3) {
//        int m = s1.size(), n = s2.size();
//        if (m + n != s3.size()) return false;
//        s1 = ' ' + s1;
//        s2 = ' ' + s2;
//        s3 = ' ' + s3;
//        vector<vector<bool>> dp(m + 1, vector<bool>(n + 1, false));
//        //初始化
//        dp[0][0] = true;
//        for (int j = 1; j <= n; ++j)
//            if (s2[j] == s3[j]) dp[0][j] = true;
//            else break;
//        for (int i = 1; i <= m; ++i)
//            if (s1[i] == s3[i]) dp[i][0] = true;
//            else break;
//        for (int i = 1; i <= m; ++i)
//        {
//            for (int j = 1; j <= n; ++j)
//            {
//                dp[i][j] = ((s1[i] == s3[i + j]) && dp[i - 1][j]) ||
//                    (s2[j] == s3[i + j] && dp[i][j - 1]);
//            }
//        }
//        return dp[m][n];
//    }
//};


