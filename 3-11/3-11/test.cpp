#include <iostream>
#include <vector>
using namespace std;

//class Solution {
//public:
//    void moveZeroes(vector<int>& nums) {
//        for (int cur = 0, dest = -1; cur < nums.size(); ++cur)
//        {
//            if (nums[cur] != 0) swap(nums[++dest], nums[cur]);
//        }
//    }
//};


//class Solution {
//public:
//    void duplicateZeros(vector<int>& arr) {
//        //1.确定最后一个元素
//        int cur = 0, dest = -1, n = arr.size();
//        while (cur < n)
//        {
//            if (arr[cur]) ++dest;
//            else dest += 2;  //不等于0就跳过两次
//            if (dest >= n - 1) break;
//            ++cur;
//        }
//        //判断dest有可能越界
//        if (dest == n)
//        {
//            arr[dest - 1] = 0;  //越界的情况一定是最后一个为0
//            --cur;
//            dest -= 2;
//        }
//        while (cur >= 0)
//        {
//            if (arr[cur] == 0)
//            {
//                arr[dest--] = 0;
//                arr[dest--] = 0;
//                --cur;
//            }
//            else {
//                arr[dest--] = arr[cur--];
//            }
//        }
//    }
//};


//class Solution {
//public:
//    int maxArea(vector<int>& height) {
//        int n = height.size();
//        int left = 0, right = n - 1;
//        int ret = 0;
//        while (left < right)
//        {
//            //计算出矩形的面积
//            int v = (right - left) * min(height[left], height[right]);
//            ret = max(ret, v);
//            //更新较小的边
//            if (height[left] < height[right]) ++left;
//            else --right;
//        }
//        return ret;
//    }
//};


//class Solution {
//public:
//    int triangleNumber(vector<int>& nums) {
//        int n = nums.size();
//        int ret = 0;
//        sort(nums.begin(), nums.end());
//        for (int i = n - 1; i >= 2; --i)  //固定最后一个大的数
//        {
//            int left = 0, right = i - 1;
//            while (left < right)
//            {
//                if (nums[left] + nums[right] <= nums[i])
//                {
//                    ++left;
//                }
//                else
//                {
//                    //收集结果
//                    ret += (right - left);
//                    --right;  //缩小区间
//                }
//            }
//        }
//        return ret;
//    }
//};


//class Solution {
//public:
//    vector<vector<int>> threeSum(vector<int>& nums) {
//        vector<vector<int>> ret;
//        int n = nums.size();
//        sort(nums.begin(), nums.end());
//        int i = 0;
//        while (i < n - 2)
//        {
//            int left = i + 1, right = n - 1;
//            int target = -nums[i];  //变成两数之和的问题
//            while (left < right)
//            {
//                if (nums[left] + nums[right] < target) ++left;
//                else if (nums[left] + nums[right] > target) --right;
//                else {
//                    //收集结果然后去重
//                    ret.push_back({ nums[i],nums[left++],nums[right--] });
//                    while (left < right && nums[left] == nums[left - 1]) ++left;
//                    while (left < right && nums[right] == nums[right + 1]) --right;
//                }
//            }
//            ++i;  //跳过该字符，然后进行去重
//            while (i < n - 2 && nums[i] == nums[i - 1]) ++i;
//        }
//        return ret;
//    }
//};


//class Solution {
//public:
//    int minSubArrayLen(int target, vector<int>& nums) {
//        int n = nums.size();
//        int left = 0, right = 0;
//        int sum = 0;
//        int len = INT_MAX;
//        while (right < n)
//        {
//            //进窗口
//            sum += nums[right];
//            //判断
//            while (sum >= target)
//            {
//                len = min(len, right - left + 1);
//                //出窗口
//                sum -= nums[left++];
//            }
//            ++right;
//        }
//        return len == INT_MAX ? 0 : len;
//    }
//};


//class Solution {
//public:
//    int lengthOfLongestSubstring(string s) {
//        int n = s.size();
//        int left = 0, right = 0;
//        int hash[128] = { 0 };  //用来映射字符出现的次数
//        int len = 0;
//        while (right < n)
//        {
//            //进窗口
//            hash[s[right]]++;
//            //判断
//            while (hash[s[right]] >= 2)
//            {
//                //出窗口
//                hash[s[left++]]--;
//            }
//            //收集结果
//            len = max(len, right - left + 1);
//            ++right;
//        }
//        return len;
//    }
//};


//class Solution {
//public:
//    int longestOnes(vector<int>& nums, int k) {
//        int zero = 0;
//        int n = nums.size();
//        int left = 0, right = 0;
//        int len = 0;
//        while (right < n)
//        {
//            //进窗口
//            if (nums[right] == 0) ++zero;
//            //判断
//            while (zero > k)
//            {
//                //出窗口
//                if (nums[left] == 0) --zero;
//                ++left;
//            }
//            //收集结果
//            len = max(len, right - left + 1);
//            ++right;
//        }
//        return len;
//    }
//};


//class Solution {
//public:
//    int minOperations(vector<int>& nums, int x) {
//        //正难则反
//        //1.算出整个数组的和
//        int sum = 0;
//        for (auto e : nums) sum += e;
//        //2.然后减去x就是中间段所求的操作数
//        int target = sum - x;
//        if (target < 0) return -1; //细节问题
//        int left = 0, right = 0;
//        int n = nums.size();
//        int len = -1;
//        int tmp = 0;
//        while (right < n)
//        {
//            //进窗口
//            tmp += nums[right];
//            while (target < tmp)
//            {
//                tmp -= nums[left++];
//            }
//            //收集结果
//            if (target == tmp)
//            {
//                len = max(len, right - left + 1);
//            }
//            ++right;
//        }
//        //3.最后用数组的长度减去中间段的长度
//        if (len == -1) return len;
//        return n - len;
//    }
//};


//int main() {
//    int n;
//    cin >> n;
//    vector<int> arr(n);
//    for (auto& e : arr)
//    {
//        cin >> e;
//    }
//    if (n == 1) return arr[0];
//    //排序加取最后两个数字进行相乘操作
//    sort(arr.begin(), arr.end());
//    int ret = 0;
//    if (arr[n - 2] == 1) ret = arr[n - 1] + arr[n - 2];
//    else ret = arr[n - 1] * arr[n - 2];
//    for (int i = 0; i <= n - 3; ++i)
//    {
//        ret += arr[i];
//    }
//    cout << ret << endl;
//    return 0;
//}


//int main() {
//    int q;
//    cin >> q;
//    while (q--)
//    {
//        int m, x;
//        cin >> m >> x;
//        int ret = x % m;
//        if (ret == 0) ret = m;
//        cout << ret << endl;
//    }
//}


//int main() {
//    int t;
//    cin >> t;
//    while (t--)
//    {
//        int n, k;
//        cin >> n >> k;
//        vector<int> arr(n);
//        for (auto& e : arr)
//        {
//            cin >> e;
//        }
//        int u, v;
//        while (k--)
//        {
//            cin >> u >> v;
//            arr[u - 1]++;
//            arr[v - 1]--;
//        }
//        //查看是否为非降序
//        int tmp = arr[0];
//        bool flag = true;
//        for (int i = 1; i < n; ++i)
//        {
//            if (arr[i] < tmp)
//            {
//                cout << "No" << endl;
//                flag = false;
//                break;
//            }
//            else {
//                tmp = arr[i];
//            }
//        }
//        if (flag) cout << "Yes" << endl;
//    }
//    return 0;
//}


//#include <string>
//#include <unordered_map>
//#include <unordered_set>
//using namespace std;
//
//bool IsRegular(string& str)
//{
//    for (auto ch : str)
//    {
//        if ('a' >= ch || 'z' <= ch) return true;
//    }
//    return false;
//}
//
//int main() {
//    int n;
//    cin >> n;
//    unordered_map<string, unordered_set<string>> hash;  //key存商家 val存地址,其中第一个为主店
//    while (n--)
//    {
//        string bus, addr;  //商家和地址
//        cin >> bus >> addr;
//        //判断合法性
//        if (IsRegular(bus) == false || IsRegular(addr) == false) continue;  //不存储
//        if (hash.count(bus) == 0)
//        {
//            //插入主店
//            hash.insert(bus, addr);
//        }
//        else {
//            //查找分店，同时插入
//            if (hash[bus].count(addr) == 0)
//            {
//                //插入
//                hash.insert(bus, addr);
//            }
//        }
//    }
//    //输出
//    for (auto& iter : hash)
//    {
//        cout << iter.first << " " << *iter.second.begin() << " " << iter.second.size() - 1 << endl;
//    }
//    return 0;
//}


//class Solution {
//public:
//    int tribonacci(int n) {
//        if (n == 0 || n == 1) return n;
//        if (n == 2) return 1;
//        vector<int> dp(n + 1);
//        //初始化
//        dp[0] = 0;
//        dp[1] = dp[2] = 1;
//        for (int i = 3; i <= n; ++i)
//        {
//            dp[i] = dp[i - 1] + dp[i - 2] + dp[i - 3];
//        }
//        return dp[n];
//    }
//};



