#include <iostream>
#include <vector>
#include <stack>
using namespace std;

int ThreeOfMid(vector<int>& arr, int left, int right)
{
	int mid = left + (right - left) / 2;
	if (arr[mid] > arr[left])
	{
		if (arr[right] > arr[mid]) return mid;
		else if (arr[left] > arr[right]) return left;
		else return  right;
	}
	else  //arr[mid] <= arr[left]
	{
		if (arr[mid] > arr[right]) return mid;
		else if (arr[right] > arr[left]) return left;
		else return right;
	}
}

//前后指针法
int _QuickSort(vector<int>& arr,int left,int right)
{
	//三数取中
	int mid = ThreeOfMid(arr, left, right);  //优化有序的情况
	swap(arr[mid], arr[left]);
	//arr[left]为key值
	int keyi = left;
	int pre = left; //找大
	int cur = pre + 1; //找小
	while (cur <= right)
	{
		if (arr[cur] < arr[keyi] && ++pre != cur)
		{
			swap(arr[cur], arr[pre]);
		}
		++cur;
	}
	swap(arr[keyi], arr[pre]);
	return pre;
}

void QuickSort(vector<int>& arr,int left,int right)
{
	if (left >= right) return;
	//找keyi
	int keyi = _QuickSort(arr,left,right);
	//[left,keyi-1] [keyi+1,right]
	QuickSort(arr, left, keyi - 1);
	QuickSort(arr, keyi+1, right);
}


void QuickSortNR(vector<int>& arr, int left, int right)
{
	//利用栈来模拟递归的过程
	stack<int> st;
	st.push(left);
	st.push(right);
	while (!st.empty())
	{
		int r = st.top();
		st.pop();
		int l = st.top();
		st.pop();
		int keyi = _QuickSort(arr, l, r);
		//[left,keyi-1] [keyi+1,right]
		//放入栈中
		if (left < keyi - 1)
		{
			st.push(left);
			st.push(keyi-1);
		}
		if (keyi + 1 < right)
		{
			st.push(keyi+1);
			st.push(right);
		}
	}
}


int main()
{
	vector<int> arr = { 3,2,5,6,7,4,1,9,8 };
	QuickSortNR(arr,0,arr.size()-1);
	for (auto e : arr)
	{
		cout << e << " ";
	}
	return 0;
}