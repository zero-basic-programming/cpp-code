#include <iostream>
#include <vector>
using namespace std;


//class Solution {
//    int m, n, row, col;
//    int dx[4] = { 0,0,1,-1 };
//    int dy[4] = { 1,-1,0,0 };
//public:
//    int nearestExit(vector<vector<char>>& maze, vector<int>& entrance) {
//        m = maze.size(), n = maze[0].size();
//        row = entrance[0], col = entrance[1];
//        vector<vector<bool>> vis(m, vector<bool>(n, false));
//        int ret = bfs(maze, vis);
//        return ret == 0 ? -1 : ret;
//    }
//    int bfs(vector<vector<char>>& maze, vector<vector<bool>>& vis)
//    {
//        queue<pair<int, int>> q;
//        q.push({ row,col });
//        vis[row][col] = true;
//        int count = 0;
//        while (q.size())
//        {
//            ++count;
//            int sz = q.size();
//            for (int i = 0; i < sz; ++i)
//            {
//                auto [a, b] = q.front();
//                q.pop();
//                for (int k = 0; k < 4; ++k)
//                {
//                    int x = a + dx[k], y = b + dy[k];
//                    if (x >= 0 && x < m && y >= 0 && y < n
//                        && !vis[x][y] && maze[x][y] == '.')
//                    {
//                        if (x == 0 || x == m - 1 || y == 0 || y == n - 1) return count;
//                        q.push({ x,y });
//                        vis[x][y] = true;
//                    }
//                }
//            }
//        }
//        return 0; //不管ret等于多少，到这里的一定没有找到边界
//    }
//};


void Qsort(vector<int>& arr, int l, int r)
{
	if (l >= r) return;
	//选基准值
	int key = arr[(rand() % (r - l + 1) + l)];
	int left = l - 1, right = r + 1, i = l;
	while (i < right)
	{
		if (arr[i] < key) swap(arr[i++], arr[++left]);
		else if (arr[i] == key) ++i;
		else swap(arr[i], arr[--right]);
	}
	Qsort(arr, l, left);
	Qsort(arr, right, r);
}

void MergeSort(vector<int>& arr, int left, int right,vector<int>& tmp)
{
	if (left >= right) return;
	int mid = (left + right) >> 1;
	MergeSort(arr, left, mid,tmp);
	MergeSort(arr, mid + 1, right,tmp);
	//单趟排序
	int begin1 = left, end1 = mid;
	int begin2 = mid + 1, end2 = right;
	int i = left;
	while (begin1 <= end1 && begin2 <= end2)
	{
		tmp[i++] = arr[begin1] < arr[begin2] ? arr[begin1++] : arr[begin2++];
	}
	while (begin1 <= end1)
	{
		tmp[i++] = arr[begin1++];
	}
	while (begin2 <= end2)
	{
		tmp[i++] = arr[begin2++];
	}
	//还原
	for (int j = left; j <= right; ++j)
		arr[j] = tmp[j];
}
int main()
{
	srand(time(nullptr));
	vector<int> arr = { 1,2,5,3,4,6,3,5,7,6,8 };
	int n = arr.size();
	vector<int> tmp(n);
	//Qsort(arr, 0, n-1);
	MergeSort(arr, 0, n - 1, tmp);
	for (auto e : arr)
	{
		cout << e << " ";
	}
	return 0;
}