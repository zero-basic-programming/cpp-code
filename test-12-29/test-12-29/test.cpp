#include <iostream>
#include <vector>
#include <thread>
#include <condition_variable>
#include <mutex>
using namespace std;


//class Solution {
//public:
//    bool is_legal(char ch)
//    {
//        if('0' <= ch && ch <= '9') return true;
//        else if('a' <= ch && ch <='z') return true;
//        else if('A' <= ch && ch <='Z') return true;
//        return false;
//    }
//    bool isPalindrome(string s) {
//        int n = s.size();
//        int left = 0,right = n-1;
//        //先全部转化为小写
//        for(auto& ch : s)
//        {
//            if('A' <= ch && ch <= 'Z')
//            {
//                ch += 32;
//            }
//        }
//        while(left < right)
//        {
//            while(left < right && !is_legal(s[left])) ++left;
//            while(left < right && !is_legal(s[right])) --right;
//            if(s[left] != s[right])
//            {   
//                return false;
//            }
//            ++left;
//            --right;
//        }
//        return true;
//    }
//};


//class Solution {
//public:
//    bool isSubsequence(string s, string t) {
//        int i = 0, j = 0;
//        while (i < s.size() && j < t.size())
//        {
//            //在t中匹配s中的字符
//            while (j < t.size() && s[i] != t[j]) ++j;
//            if (j == t.size()) return false;
//            ++i; ++j;
//        }
//        return i == s.size() ? true : false;
//    }
//};


//class Solution {
//public:
//    int minSubArrayLen(int target, vector<int>& nums) {
//        int n = nums.size();
//        int left = 0, right = 0;
//        int sum = 0; //记录总和
//        int len = INT_MAX; //记录结果长度
//        while (right < n)
//        {
//            //进窗口
//            sum += nums[right];
//            //判断
//            while (sum >= target)
//            {
//                len = min(len, right - left + 1);
//                sum -= nums[left++];
//            }
//            ++right;
//        }
//        return len == INT_MAX ? 0 : len;
//    }
//};

//采用RAII的方式进行申请线程
//class myThread
//{
//public:
//	myThread(thread& t)
//		:_t(t)
//	{}
//	~myThread()
//	{
//		if (_t.joinable())
//		{
//			_t.join();
//		}
//	}
//	myThread(thread& t) = delete;
//	thread& operator=(const thread& t) = delete;
//private:
//	thread& _t;
//};

//void func(mutex& mtx)
//{
//	//mtx.lock();
//	for (int i = 1; i < 100; ++i)
//	{
//		mtx.lock();
//		cout << i << " ";
//		mtx.unlock();
//	}
//	//mtx.unlock();
//}

//使用原子操作，保证结果的正确性
//void func(atomic<int>& n, int times)
//{
//	for (int i = 0; i < times; ++i) ++n;
//}
//
//int main()
//{
//	mutex mtx;
//	atomic<int> n = 0;
//	int times = 10000;
//	thread t1(func, ref(n),times);
//	thread t2(func,ref(n),times);
//	t1.join();
//	t2.join();
//	cout << n << endl; //这个结果是小于times的
//	return 0;
//}



//int main()
//{
//	int n = 100;
//	bool flag = true; //配合条件变量控制打印
//	condition_variable cv;
//	mutex mtx;
//	thread t1([&] {
//		int i = 1;
//	while (i < 100)
//	{
//		unique_lock<mutex> ul(mtx);
//		cv.wait(ul, [&]()->bool {return flag; });
//		cout << this_thread::get_id() << " : " << i << endl;
//		i += 2;
//		flag = !flag;
//		cv.notify_one();
//	}
//		
//		});
//	thread t2([&] {
//		int i = 2;
//	while (i < 100)
//	{
//		unique_lock<mutex> ul(mtx);
//		cv.wait(ul, [&]()->bool {return !flag; });
//		cout << this_thread::get_id() << " : " << i << endl;
//		i += 2;
//		flag = !flag;
//		cv.notify_one();
//	}
//
//		}
//	);
//	t1.join();
//	t2.join();
//	return 0;
//}


int main()
{
	bool flag = true;
	condition_variable cv;
	mutex mtx;
	thread t1([&]
		{
			int i = 1;
			while (i <= 100)
			{
				unique_lock<mutex> ul(mtx);
				cv.wait(ul,[&]()->bool {return flag; });
				cout << this_thread::get_id() << " : " << i << endl;
				i += 2;
				flag = !flag;
				cv.notify_one();
			}
		}
	);
	thread t2([&]
		{
			int i = 2;
			while (i <= 100)
			{
				unique_lock<mutex> ul(mtx);
				cv.wait(ul, [&]()->bool {return !flag; });
				cout << this_thread::get_id() << " : " << i << endl;
				i += 2;
				flag = !flag;
				cv.notify_one();
			}
		});
	t1.join();
	t2.join();
	return 0;
}