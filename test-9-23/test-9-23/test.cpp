#include <iostream>
#include <vector>
using namespace std;


//class Solution {
//public:
//    void AdjustDown(vector<int>& nums, int parent)
//    {
//        int n = nums.size();
//        int child = 2 * parent + 1;
//        while (child < n)
//        {
//            //找左右较大的孩子
//            if (child + 1 < n && nums[child] < nums[child + 1])
//            {
//                child += 1;
//            }
//            if (nums[child] > nums[parent])
//            {
//                swap(nums[child], nums[parent]);
//                //迭代
//                parent = child;
//                child = 2 * parent + 1;
//            }
//        }
//    }
//    int findKthLargest(vector<int>& nums, int k) {
//        //建堆
//        int n = nums.size();
//        for (int i = (n - 1 - 1) / 2; i >= 0; --i)
//        {
//            AdjustDown(nums, i);
//        }
//        //找到第k大的元素,排除前面n-1个元素
//        for (int i = 1; i < k; ++i)
//        {
//            swap(nums[0], nums[n - i]);
//            AdjustDown(nums, 0);
//        }
//        return nums[0];
//    }
//};
// 
//class Solution {
//public:
//    void AdjustDown(vector<int>& nums, int parent, int n)
//    {
//        int child = 2 * parent + 1;
//        while (child < n)
//        {
//            //找左右较小的孩子
//            if (child + 1 < n && nums[child] > nums[child + 1])
//            {
//                child += 1;
//            }
//            if (nums[child] < nums[parent])
//            {
//                swap(nums[child], nums[parent]);
//                //迭代
//                parent = child;
//                child = 2 * parent + 1;
//            }
//            else break; //这步还是必须的，否则可能会死循环
//        }
//    }
//    void findKthLargest(vector<int>& nums, int k) {
//        //建k个元素的小堆
//        int n = nums.size();
//        for (int i = (k - 1 - 1) / 2; i >= 0; --i)
//        {
//            AdjustDown(nums, i, k);
//        }
//        //找到第k大的元素,排除前面n-1个元素
//        for (int i = k; i < n; ++i)
//        {
//            if (nums[i] > nums[0])
//            {
//                nums[0] = nums[i];
//                AdjustDown(nums, 0, k);
//            }
//        }
//    }
//};
//
//
//class Solution {
//public:
//    int findKthLargest(vector<int>& nums, int k) {
//        //使用stl中的priority_queue来建堆
//        priority_queue<int, vector<int>, greater<int>> pq(nums.begin(), nums.begin() + k);
//        //在剩下元素不断向堆中排序
//        for (int i = k; i < nums.size(); ++i)
//        {
//            if (nums[i] > pq.top())
//            {
//                pq.pop();
//                pq.push(nums[i]);
//            }
//        }
//        return pq.top();
//    }
//};
//
//int main()
//{
//    vector<int> nums = { 1,2,6,3,4,5,8,7,9 };
//    Solution().findKthLargest(nums, 4);
//    for (auto& e : nums)
//    {
//        cout << e << " ";
//    }
//    return 0;
//}


