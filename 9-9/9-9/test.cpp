#include <iostream>
#include <vector>
using namespace std;



//class Solution {
//public:
//    bool isAlienSorted(vector<string>& words, string order) {
//        //建立下标的映射
//        vector<int> index(26);
//        for (int i = 0; i < 26; ++i)
//        {
//            index[order[i] - 'a'] = i;
//        }
//        //两两比较字符串
//        int i = 0;
//        int n = words.size();
//        while (i < n - 1)
//        {
//            int prevStrSize = words[i].size();
//            int nextStrSize = words[i + 1].size();
//            bool flag = false;   //判断是否比较的字符串都相等
//            int j = 0;
//            while (j < prevStrSize && j < nextStrSize)
//            {
//                if (index[words[i][j] - 'a'] > index[words[i + 1][j] - 'a']) return false;
//                else if (index[words[i][j] - 'a'] < index[words[i + 1][j] - 'a'])
//                {
//                    flag = true;
//                    break;
//                }
//                ++j;
//            }
//            //这里可能前面比较的都是相等的
//            if (flag == false && nextStrSize < prevStrSize) return false;
//            ++i;
//        }
//        return true;
//    }
//};

//class Solution
//{
//    vector<vector<int>> SumOfThreeNumber(vector<int>& nums)
//    {
//        //排序 + 查找 同时去重
//        sort(nums.begin(), nums.end());
//        int i = 0;
//        int n = nums.size();
//        vector<vector<int>> ret;
//        while (i < n - 2)
//        {
//            int left = i + 1, right = n - 1;
//            int target = -(nums[i]);  //转换成两数之和
//            while (left < right)
//            {
//                if (nums[left] + nums[right] < target) ++left;
//                else if (nums[left] + nums[right] > target) --right;
//                else
//                {
//                    //收集结果+去重
//                    ret.push_back(nums[i], nums[left++], nums[right--]);
//                    while (left < right && nums[left] == nums[left - 1]) ++left;
//                    while (left < right && nums[right] == nums[right + 1]) ++right;
//                }
//            }
//            //对i去重
//            ++i;
//            while (i < n && nums[i] == nums[i - 1]) ++i;
//        }
//        return ret;
//    }
//}



//class Solution {
//public:
//    bool isInterleave(string s1, string s2, string s3) {
//        int m = s1.size(), n = s2.size();
//        if (m + n != s3.size()) return false;
//        vector<vector<bool>> dp(m + 1, vector<bool>(n + 1, false));
//        //初始化
//        dp[0][0] = true;
//        s1 = ' ' + s1;
//        s2 = ' ' + s2;
//        s3 = ' ' + s3;
//        for (int i = 1; i <= m; ++i)
//        {
//            if (s1[i] == s3[i]) dp[i][0] = true;
//            else break;  //这里后面一定是错误的
//        }
//        for (int j = 1; j <= n; ++j)
//        {
//            if (s2[j] == s3[j]) dp[0][j] = true;
//            else break;
//        }
//        for (int i = 1; i <= m; ++i)
//        {
//            for (int j = 1; j <= n; ++j)
//            {
//                dp[i][j] = (s1[i] == s3[i + j] && dp[i - 1][j])
//                    || (s2[j] == s3[i + j] && dp[i][j - 1]);
//            }
//        }
//        return dp[m][n];
//    }
//};


int main()
{
	int a = 12;
	a += a -= a * a;   // -264
	cout << a << endl;
	return 0;
}