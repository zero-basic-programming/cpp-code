#include <iostream>
#include <vector>
using namespace std;


//class Solution {
//public:
//    int uniquePathsWithObstacles(vector<vector<int>>& obstacleGrid) {
//        int row = obstacleGrid.size();
//        int col = obstacleGrid[0].size();
//        //如果起始和终点有障碍就直接返回
//        if (obstacleGrid[0][0] == 1 || obstacleGrid[row - 1][col - 1] == 1)
//            return 0;
//        vector<vector<int>> dp(row, vector<int>(col, 0));
//        //对左边和上面两行初始化
//        for (int i = 0; i < col; ++i)
//        {
//            if (obstacleGrid[0][i] == 0)
//                dp[0][i] = 1;
//            else   break; //走到这里后面肯定都还是0次
//        }
//        for (int i = 1; i < row; ++i)
//        {
//            if (obstacleGrid[i][0] == 0)
//                dp[i][0] = 1;
//            else   break;
//        }
//        for (auto& v : dp)
//        {
//            for (auto& e : v)
//            {
//                cout << e << " ";
//            }
//            cout << endl;
//        }
//        //dp
//        for (int i = 1; i < row; ++i)
//        {
//            for (int j = 1; j < col; ++j)
//            {
//                //状态转移方程
//                if (obstacleGrid[i][j] == 1)
//                    dp[i][j] = 0;
//                else
//                    dp[i][j] = dp[i - 1][j] + dp[i][j - 1];
//            }
//        }
//        return dp[row - 1][col - 1];
//    }
//};
//
//int main()
//{
//    vector<vector<int>> vv(1, vector<int>(2, 0));
//  /*  for (auto& v : vv)
//    {
//        for (auto& e : v)
//        {
//            cout << e << " ";
//        }
//        cout << endl;
//    }*/
//    Solution().uniquePathsWithObstacles(vv);
//    return 0;
//}

//class Solution {
//public:
//    int uniquePathsWithObstacles(vector<vector<int>>& obstacleGrid) {
//        //使用一维数组解决
//        int n = obstacleGrid.size();
//        int m = obstacleGrid[0].size();
//        vector<int> dp(m, 1);
//        //初始化
//        for (int i = 0; i < m; ++i)
//        {
//            if (obstacleGrid[0][i] == 1)
//            {
//                dp[i] = 0;
//            }
//            else if (i == 0)
//            {
//                dp[i] = 1;
//            }
//            else
//                dp[i] = dp[i - 1];
//        }
//        //dp
//        for (int i = 1; i < n; ++i)
//        {
//            for (int j = 0; j < m; ++j)
//            {
//                if (obstacleGrid[i][j] == 1)
//                    dp[j] = 0;
//                //状态转移方程
//                else if (j != 0)
//                {
//                    dp[j] = dp[j] + dp[j - 1];
//                }
//                //第一列不需要处理，因为初始化dp时就已经处理了
//            }
//        }
//        return dp[m - 1];
//    }
//};


class Solution {
public:
    int integerBreak(int n) {
        //dp
        vector<int> dp(n + 1, 0);
        //初始化，前3个位置不需要，dp[2] = 1
        dp[2] = 1;
        for (int i = 3; i <= n; ++i)
        {
            for (int j = 1; j <= i / 2; ++j)
            {
                //状态转移方程 --> 两个数相乘和多个数相乘的较大者和当前值比较取较大的那个
                dp[i] = max(dp[i], max(dp[i - j] * j, (i - j) * j));
            }
        }
        return dp[n];
    }
};