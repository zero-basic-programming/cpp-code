#include <iostream>
#include <vector>
using namespace std;

//class Solution {
//public:
//    int minCostClimbingStairs(vector<int>& cost) {
//        int n = cost.size();
//        vector<int> dp(n, 0);
//        //初始化
//        dp[0] = cost[0];
//        dp[1] = cost[1];
//        for (int i = 2; i < n; ++i)
//        {
//            //状态转移方程
//            dp[i] = min(dp[i - 1], dp[i - 2]) + cost[i];
//        }
//        return min(dp[n - 1], dp[n - 2]);
//    }
//};

class Solution {
public:
    int numDecodings(string s) {
        int n = s.size();
        vector<int> dp(n, 0);
        //初始化
        dp[0] = 1;
        for (int i = 1; i < n; ++i)
        {
            if (s[i - 1] < '3' && s[i - 1] != '0')
            {
                dp[i] = dp[i - 1] + 1;
            }
            else
            {
                dp[i] = dp[i - 1];
            }
        }

        return dp[n - 1];
    }
};

int main()
{
    Solution().numDecodings("12");
    return 0;
}