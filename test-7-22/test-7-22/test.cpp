#include <iostream>
using namespace std;

//int main()
//{
//	int arr[10] = { 9,1,2,4,3,6,8,7,0 };
//	for (int i = 0; i < 10; ++i)
//	{
//		for (int j = 1; j <sizeof(arr)/sizeof(int) - i; ++j)
//		{
//			//每次得到一个数据的顺序
//			if (arr[j-1] > arr[j])
//			{
//				int tmp = arr[j];
//				arr[j] = arr[j + 1];
//				arr[j + 1] = tmp;
//			}
//		}
//	}
//	for (auto& e : arr)
//	{
//		cout << e << " ";
//	}
//	return 0;
//}


//class MaxGap {
//public:
//    int findMaxGap(vector<int> A, int n) {
//        //左边可以通过每次k的移动来确定最大值，但是右边是一定需要遍历才能确定的
//        int leftmax = A[0];
//        int rightmax = INT_MIN;
//        for (int i = 1; i < n; ++i) {
//            if (rightmax < A[i]) {
//                rightmax = A[i];
//            }
//        }
//        int gapmax = abs(leftmax - rightmax);
//        for (int k = 1; k < n - 1; ++k) {
//            if (A[k] > leftmax) {
//                //更新左边的最大值
//                leftmax = A[k];
//            }
//            //比较左右两边的最大值
//            if (A[k] == rightmax) {
//                //说明这里是需要更新的
//                rightmax = INT_MIN;
//                for (int j = k + 1; j < n; ++j) {
//                    if (A[j] > rightmax)
//                    {
//                        rightmax = A[j];
//                    }
//                }
//            }
//            gapmax = gapmax < abs(rightmax - leftmax) ? abs(rightmax - leftmax) : gapmax;
//        }
//        return gapmax;
//    }
//};


#include <vector>
class Printer {
public:
    vector<int> clockwisePrint(vector<vector<int>>& mat, int n, int m) {
        // write code here
        vector<int> result;
        int i = 0;
        int j = 0;
        int x = 0; //x，y是用来控制边界的
        int y = 0;
        int loop = n / 2; //用来控制循环的次数
        while (loop--)
        {
            //每次回来必须重新给i和j赋值，否则会有问题
            i = x;
            j = y;
            //第一行[)
            for (; j < n - y-1; ++j)
            {
                result.push_back(mat[i][j]);
            }
            //最后一列
            for (; i < m - x-1; ++i)
            {
                result.push_back(mat[i][j]);
            }
            //下行
            for (; j > y; --j)
            {
                result.push_back(mat[i][j]);
            }
            //左列
            for (; i > x; --i)
            {
                result.push_back(mat[i][j]);
            }
            ++x;
            ++y;
        }
        return result;
    }
};


int main()
{
    vector<vector<int>> vv(2,vector<int>(2,0));
    int n = 2;
    int m = 2;
    vv[0][0] = 1;
    vv[0][1] = 2;
    vv[1][0] = 3;
    vv[1][1] = 4;
    vector<int> result = Printer().clockwisePrint(vv, n, m);
    for (auto& e : result)
    {
        cout << e << " ";
    }
    return 0;
}
    /*vector<int> r;
    r.push_back(vv[0][0]);
    r.push_back(2);
    for (auto& e : r)
    {
        cout << e << endl;
    }*/
//    return 0;
//}