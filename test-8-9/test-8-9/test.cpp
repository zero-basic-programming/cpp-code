#include <iostream>
#include <vector>
using namespace std;

//class Solution {
//public:
//    int longestOnes(vector<int>& nums, int k) {
//        int zero = 0; //用来统计换过的0的个数
//        int left = 0;
//        int right = 0;
//        int n = nums.size();
//        int ret = 0;
//        while (right < n)
//        {
//            //进窗口
//            //如果有0就++计数器
//            if (nums[right] == 0) ++zero;
//            //判断
//            while (zero > k)
//            {
//                if (nums[left] == 0) --zero;
//                ++left;
//            }
//            //收集结果
//            ret = max(ret, right - left + 1);
//            ++right;
//        }
//        return ret;
//    }
//};


//class Solution {
//public:
//    int minCost(vector<vector<int>>& costs) {
//        int n = costs.size();
//        vector<vector<int>> dp(n, vector<int>(3));
//        //初始化
//        dp[0][0] = costs[0][0];
//        dp[0][1] = costs[0][1];
//        dp[0][2] = costs[0][2];
//        //填表
//        for (int i = 1; i < n; ++i)
//        {
//            dp[i][0] = min(dp[i - 1][1], dp[i - 1][2]) + costs[i][0];
//            dp[i][1] = min(dp[i - 1][0], dp[i - 1][2]) + costs[i][1];
//            dp[i][2] = min(dp[i - 1][1], dp[i - 1][0]) + costs[i][2];
//        }
//        return min(dp[n - 1][0], min(dp[n - 1][1], dp[n - 1][2]));
//    }
//};


