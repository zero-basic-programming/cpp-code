#include <iostream>
using namespace std;
#include <map>

//int main() {
//    map<string, int> m;
//    string str;
//    //利用string，cin输入时以空格断开
//    while (cin >> str)
//    {
//        //因为每个单词的首个字母有可能是大写,还有可能存在句号
//        if (str[0] >= 'A' && str[0] <= 'Z')
//        {
//            str[0] = ('a' + (str[0] - 'A'));
//        }
//        if (str[str.size() - 1] == '.')
//        {
//            str.pop_back();
//        }
//        m[str]++;
//    }
//    for (auto& e : m)
//    {
//        cout << e.first << ":" << e.second << endl;
//    }
//    return 0;
//}

//class Solution {
//public:
//    int minSubArrayLen(int target, vector<int>& nums) {
//        //定义两个变量，使用滑动窗口的思想
//        int cur = 0;
//        int prev = 0;
//        int minresult = pow(10, 9);//记录最小结果
//        int flag = 0;//如果存在就更新为1
//        while (prev < nums.size())
//        {
//            int tmp = 0;//记录单次的结果
//            for (int i = cur; i <= prev; ++i)
//                tmp += nums[i];
//
//            if (tmp >= target)
//            {
//                if ((prev - cur + 1) < minresult)
//                {
//                    minresult = (prev - cur + 1);
//                    //cout<<minresult<<nums[prev]<<nums[cur]<<endl;
//                    flag = 1;
//                }
//                ++cur;
//            }
//            else
//            {
//                ++prev;
//            }
//        }
//        return flag == 0 ? 0 : minresult;
//    }
//};
#include <vector>
class Solution {
public:
    int minSubArrayLen(int s, vector<int>& nums) {
        int n = nums.size();
        if (n == 0) {
            return 0;
        }
        int ans = INT_MAX;
        int start = 0, end = 0;
        int sum = 0;
        while (end < n) {
            sum += nums[end];
            while (sum >= s) {
                ans = min(ans, end - start + 1);
                sum -= nums[start];
                start++;
            }
            end++;
        }
        return ans == INT_MAX ? 0 : ans;
    }
};
