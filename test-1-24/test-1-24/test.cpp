#include <iostream>
using namespace std;
#include <vector>

//class Solution {
//public:
//    vector<int> sortedSquares(vector<int>& nums) {
//        if (nums.empty())
//        {
//            return nums;
//        }
//        vector<int> result;
//        if (nums.size() == 1)
//        {
//            result.push_back(pow(nums[0], 2));
//            return result;
//        }
//        //先找到最小
//        int i = 0;
//        int j = 1;
//        int ri = pow(nums[i], 2);
//        int rj = pow(nums[j], 2);
//        while (j < nums.size())
//        {
//            ri = pow(nums[i], 2);
//            rj = pow(nums[j], 2);
//            if (ri >= rj)
//            {
//                ++i;
//                ++j;
//            }
//            else
//            {
//                break;
//            }
//        }
//        //其实i下标就是最小的
//        while (i >= 0 && j < nums.size())
//        {
//            ri = pow(nums[i], 2);
//            rj = pow(nums[j], 2);
//            if (ri <= rj)
//            {
//                result.push_back(ri);
//                i--;
//            }
//            else {
//                result.push_back(rj);
//                ++j;
//            }
//        }
//        while (i >= 0)
//        {
//            ri = pow(nums[i], 2);
//            result.push_back(ri);
//            --i;
//        }
//        while (j < nums.size())
//        {
//            rj = pow(nums[j], 2);
//            result.push_back(rj);
//            ++j;
//        }
//        return result;
//    }
//};


//class Solution {
//public:
//    vector<int> sortedSquares(vector<int>& nums) {
//        int n = nums.size();
//        int negative = -1;
//        for (int i = 0; i < n; ++i) {
//            if (nums[i] < 0) {
//                negative = i;
//            }
//            else {
//                break;
//            }
//        }
//
//        vector<int> ans;
//        int i = negative, j = negative + 1;
//        while (i >= 0 || j < n) {
//            if (i < 0) {
//                ans.push_back(nums[j] * nums[j]);
//                ++j;
//            }
//            else if (j == n) {
//                ans.push_back(nums[i] * nums[i]);
//                --i;
//            }
//            else if (nums[i] * nums[i] < nums[j] * nums[j]) {
//                ans.push_back(nums[i] * nums[i]);
//                --i;
//            }
//            else {
//                ans.push_back(nums[j] * nums[j]);
//                ++j;
//            }
//        }
//
//        return ans;
//    }
//};

//class Solution {
//public:
//    vector<int> sortedSquares(vector<int>& nums) {
//        //头尾指针，因为平方之后最大的那个必然是第一个或者是最后一个的平方
//        int n = nums.size();
//        vector<int> result(n);
//        int i = 0; int j = n - 1;
//        int pos = n - 1;//从最后一个位置开始逆序插入
//        while (i <= j)
//        {
//            if (nums[i] * nums[i] > nums[j] * nums[j])
//            {
//                result[pos] = nums[i] * nums[i];
//                ++i;
//            }
//            else
//            {
//                result[pos] = nums[j] * nums[j];
//                --j;
//            }
//            --pos;
//        }
//        return result;
//    }
//};


//class Solution {
//public:
//    bool backspaceCompare(string s, string t) {
//        //分别记录两个字符串有多少退格符
//        int count1 = 0; int count2 = 0;
//        int si = s.size() - 1;
//        int ti = t.size() - 1;
//        while (si >= 0 && ti >= 0)
//        {
//            if (s[si] == '#')
//            {
//                ++count1;
//                --si;
//                continue;
//            }
//            if (t[ti] == '#')
//            {
//                ++count2;
//                --ti;
//                continue;
//            }
//            //此时是一定没有退格符的
//            if (count1)
//            {
//                --si;
//                --count1;
//                continue;
//            }
//            if (count2)
//            {
//                --ti;
//                --count2;
//                continue;
//            }
//            //这里开始就可以比较了
//            if (s[si] == t[ti])
//            {
//                --si;
//                --ti;
//            }
//            else
//            {
//                return false;
//            }
//        }
//        if (si != ti)
//        {
//            return false;
//        }
//        return true;
//    }
//};
//
//int main()
//{
//    string s1("ab##");
//    string s2("c#d#");
//    bool flag = Solution().backspaceCompare(s1,s2);
//    cout << flag << endl;
//    return 0;
//}

