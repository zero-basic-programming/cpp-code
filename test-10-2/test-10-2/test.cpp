#include <iostream>
#include <vector>
using namespace std;


//class Solution {
//public:
//    int reversePairs(vector<int>& record) {
//        vector<int> tmp(record.size());
//        int ret = MergeSort(record, 0, record.size() - 1, tmp);
//        return ret;
//    }
//    int MergeSort(vector<int>& nums, int left, int right, vector<int>& tmp)
//    {
//        //返回
//        if (left >= right) return 0;
//        int mid = (left + right) >> 1;
//        //[left mid][mid+1 right]
//        //左右选结果加排序
//        int ret = 0;
//        ret += MergeSort(nums, left, mid, tmp);
//        ret += MergeSort(nums, mid + 1, right, tmp);
//        //一左一右选结果
//        int cur1 = left, cur2 = mid + 1, i = left;
//        while (cur1 <= mid && cur2 <= right)
//        {
//            if (nums[cur1] > nums[cur2])
//            {
//                ret += (mid - cur1 + 1); //cur1右边的结果全部都大于cur2
//                tmp[i++] = nums[cur2++];
//            }
//            else tmp[i++] = nums[cur1++];
//        }
//        //完成归并排序后序
//        while (cur1 <= mid) tmp[i++] = nums[cur1++];
//        while (cur2 <= right) tmp[i++] = nums[cur2++];
//        //还原回原数组
//        for (int j = left; j <= right; ++j) nums[j] = tmp[j];
//        return ret;
//    }
//};


//降序处理
//class Solution {
//public:
//    int reversePairs(vector<int>& record) {
//        vector<int> tmp(record.size());
//        int ret = MergeSort(record, 0, record.size() - 1, tmp);
//        return ret;
//    }
//    int MergeSort(vector<int>& nums, int left, int right, vector<int>& tmp)
//    {
//        //返回
//        if (left >= right) return 0;
//        int mid = (left + right) >> 1;
//        //[left mid][mid+1 right]
//        //左右选结果加排序
//        int ret = 0;
//        ret += MergeSort(nums, left, mid, tmp);
//        ret += MergeSort(nums, mid + 1, right, tmp);
//        //一左一右选结果
//        int cur1 = left, cur2 = mid + 1, i = left;
//        while (cur1 <= mid && cur2 <= right)
//        {
//            if (nums[cur1] > nums[cur2])
//            {
//                ret += (right - cur2 + 1); //cur1右边的结果全部都大于cur2
//                tmp[i++] = nums[cur1++];
//            }
//            else tmp[i++] = nums[cur2++];
//        }
//        //完成归并排序后序
//        while (cur1 <= mid) tmp[i++] = nums[cur1++];
//        while (cur2 <= right) tmp[i++] = nums[cur2++];
//        //还原回原数组
//        for (int j = left; j <= right; ++j) nums[j] = tmp[j];
//        return ret;
//    }
//};


//class Solution {
//    vector<int> ret; //记录结果
//    int tmp[500010]; //完成归并排序
//    int index[500010]; //建立元素和下标的映射关系
//    vector<int> numsindex; //最开始的映射关系
//public:
//    vector<int> countSmaller(vector<int>& nums) {
//        int n = nums.size();
//        ret.resize(n);
//        numsindex.resize(n);
//        for (int i = 0; i < n; ++i) numsindex[i] = i;
//        MergeSort(nums, 0, n - 1);
//        return ret;
//    }
//    void MergeSort(vector<int>& nums, int left, int right)
//    {
//        //返回
//        if (left >= right) return;
//        int mid = (left + right) >> 1;
//        //[left mid] [mid+1 right]
//        MergeSort(nums, left, mid);
//        MergeSort(nums, mid + 1, right);
//        int cur1 = left, cur2 = mid + 1, i = 0;
//        while (cur1 <= mid && cur2 <= right)  //降序
//        {
//            if (nums[cur1] > nums[cur2])
//            {
//                ret[index[cur1]] += (right - cur2 + 1);
//                tmp[i] = nums[cur1];
//                //建立下标映射
//                index[i++] = numsindex[cur1++];
//            }
//            else
//            {
//                tmp[i] = nums[cur2];
//                index[i++] = numsindex[cur2++];
//            }
//        }
//        //处理完归并排序剩下的数据
//        while (cur1 <= mid)
//        {
//            tmp[i] = nums[cur1];
//            index[i++] = numsindex[cur1++];
//        }
//        while (cur2 <= right)
//        {
//            tmp[i] = nums[cur2];
//            index[i++] = numsindex[cur2++];
//        }
//        //还原
//        for (int j = left; j <= right; ++j)
//        {
//            nums[j] = tmp[j - left];
//            numsindex[j] = index[j - left];
//        }
//    }
//};
//
//int main()
//{
//    vector<int> nums = { 5,2,6,1 };
//    vector<int> ret = Solution().countSmaller(nums);
//    for (auto e : ret) cout << e << " ";
//    return 0;
//}


class Solution
{
	vector<int> ret;
	vector<int> index; // 记录 nums 中当前元素的原始下标
	int tmpNums[500010];
	int tmpIndex[500010];
public:
	vector<int> countSmaller(vector<int>& nums)
	{
		int n = nums.size();
		ret.resize(n);
		index.resize(n);
		// 初始化?下 index 数组
		for (int i = 0; i < n; i++)
			index[i] = i;
		mergeSort(nums, 0, n - 1);
		return ret;
	}

	void mergeSort(vector<int>& nums, int left, int right)
	{
		if (left >= right) return;
		// 1. 根据中间元素，划分区间
		int mid = (left + right) >> 1;
		// [left, mid] [mid + 1, right]
		// 2. 先处理左右两部分
		mergeSort(nums, left, mid);
		mergeSort(nums, mid + 1, right);
		// 3. 处理?左?右的情况
		int cur1 = left, cur2 = mid + 1, i = 0;
		while (cur1 <= mid && cur2 <= right) // 降序
		{
			if (nums[cur1] <= nums[cur2])
			{
				tmpNums[i] = nums[cur2];
				tmpIndex[i++] = index[cur2++];
			}
			else
			{
				ret[index[cur1]] += right - cur2 + 1; // 重点
				tmpNums[i] = nums[cur1];
				tmpIndex[i++] = index[cur1++];
			}
		}
		// 4. 处理剩下的排序过程
		while (cur1 <= mid)
		{
			tmpNums[i] = nums[cur1];
			tmpIndex[i++] = index[cur1++];
		}
		while (cur2 <= right)
		{
			tmpNums[i] = nums[cur2];
			tmpIndex[i++] = index[cur2++];
		}
		for (int j = left; j <= right; j++)
		{
			nums[j] = tmpNums[j - left];
			index[j] = tmpIndex[j - left];
		}
	}
};