#include <iostream>
#include <vector>
#include <algorithm>
using namespace std;


//int main()
//{
//    int n, m;
//    cin >> n >> m;
//    vector<int> a(n);
//    vector<int> b(m);
//    for (auto& e : a) cin >> e;
//    for (auto& e : b) cin >> e;
//    //需要排序，这样方便后序直接通过坐标乘积计算出结果
//    sort(a.begin(), a.end());
//    sort(b.begin(), b.end());
//    int count = 0;  //统计结果
//    for (int i = 0; i < n; ++i)
//    {
//        for (int j = 0; j < m; ++j)
//        {
//            //后面如果出现重复的也是可以选择的
//            int x = i, y = j;
//            while (x + 1 < n && a[x] == a[x + 1]) ++x;
//            while (y + 1 < m && b[y] == b[y + 1]) ++y;
//            count += (x + 1) * (y + 1);
//        }
//    }
//    cout << count << endl;
//    return 0;
//}
//
//int main()
//{
//    int n;
//    cin >> n;
//    if (n <= 2)
//    {
//        cout << n << endl;
//        return 0;
//    }
//    vector<int> a(n);
//    for (auto& e : a) cin >> e;
//    //使用动态规划
//    vector<int> f(n);  //选择开机当前机器
//    vector<int> g(n);  //不开当前机器
//    //初始化
//    f[0] = 1; f[1] = 2;
//    for (int i = 2; i < n; ++i)
//    {
//        if (a[i - 2] + a[i] >= a[i - 1])
//        {
//            g[i] = max(f[i - 1], g[i - 1]);
//            f[i] = g[i - 2] + 2;
//        }
//        else {
//            f[i] = max(f[i - 1], g[i - 1]) + 1;
//            g[i] = max(f[i - 1], g[i - 1]);
//        }
//    }
//    cout << max(f[n - 1], g[n - 1]) << endl;
//    return 0;
//}


//class Solution {
//public:
//    int search(vector<int>& nums, int target) {
//        int left = 0, right = nums.size() - 1;
//        while (left <= right)
//        {
//            int mid = (left + right) >> 1;
//            if (nums[mid] > target) right = mid - 1;
//            else if (nums[mid] < target) left = mid + 1;
//            else {
//                return mid;
//            }
//        }
//        return -1;
//    }
//};


//class Solution {
//public:
//    vector<int> searchRange(vector<int>& nums, int target) {
//        int n = nums.size();
//        if (n == 0) return { -1,-1 };
//        //先找左边界
//        int resLeft = 0;
//        int left = 0, right = n - 1;
//        while (left < right)
//        {
//            int mid = left + (right - left) / 2;  //防止溢出
//            if (nums[mid] >= target) right = mid;
//            else left = mid + 1;
//        }
//        //这里必定==判断结果是否为target
//        if (nums[left] != target) return { -1,-1 };
//        else  resLeft = left;
//        //找右边界
//        left = 0, right = n - 1;
//        while (left < right)
//        {
//            int mid = left + (right - left + 1) / 2;
//            if (nums[mid] <= target) left = mid;
//            else right = mid - 1;
//        }
//        return { resLeft,right };
//    }
//};


//class Solution {
//public:
//    int searchInsert(vector<int>& nums, int target) {
//        int left = 0, right = nums.size() - 1;
//        while (left < right)
//        {
//            int mid = left + (right - left) / 2;
//            if (nums[mid] >= target) right = mid;
//            else left = mid + 1;
//        }
//        //这里要么找到左边界，要么就是需要插入的位置
//        if (nums[left] < target) return left + 1;
//        else return right;
//    }
//};


//class Solution {
//public:
//    int mySqrt(int x) {
//        if (x < 1) return 0;
//        int left = 1, right = x;
//        while (left < right)
//        {
//            long long mid = left + (right - left + 1) / 2;
//            if (mid * mid > x) right = mid - 1;
//            else left = mid;
//        }
//        //这里已经找到答案
//        return right;
//    }
//};


//class Solution {
//public:
//    int peakIndexInMountainArray(vector<int>& arr) {
//        int left = 0, right = arr.size() - 1;
//        while (left < right)
//        {
//            int mid = left + (right - left + 1) / 2;
//            if (arr[mid] > arr[mid - 1]) left = mid;
//            else right = mid - 1;
//        }
//        return right;
//    }
//};


//class Solution {
//public:
//    int findPeakElement(vector<int>& nums) {
//        int left = 0, right = nums.size() - 1;
//        while (left < right)
//        {
//            int mid = left + (right - left + 1) / 2;
//            if (nums[mid] <= nums[mid - 1]) right = mid - 1;
//            else left = mid;
//        }
//        return right;
//    }
//};

//
//class Solution {
//public:
//    int findMin(vector<int>& nums) {
//        int left = 0, right = nums.size() - 1;
//        int x = nums[right];  //左边区域一定是大于x，右边区域一定是小于等于x
//        while (left < right)
//        {
//            int mid = left + (right - left) / 2;
//            if (nums[mid] > x) left = mid + 1;
//            else right = mid;
//        }
//        return nums[left];
//    }
//};


//class Solution {
//public:
//    int takeAttendance(vector<int>& records) {
//        //二段性：下标与元素的关系
//        int left = 0, right = records.size();
//        while (left < right)
//        {
//            int mid = left + (right - left) / 2;
//            if (records[mid] == mid) left = mid + 1;
//            else right = mid;
//        }
//        return left;
//    }
//};


