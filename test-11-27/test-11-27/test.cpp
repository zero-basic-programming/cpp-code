#include <iostream>
#include <vector>
using namespace std;

//class Solution {
//    int ret = 0;
//public:
//    int subsetXORSum(vector<int>& nums) {
//        dfs(nums, 0, 0);
//        return ret;
//    }
//    void dfs(vector<int>& nums, int m, int tmp)
//    {
//        ret += tmp;
//        for (int i = m; i < nums.size(); ++i)
//        {
//            dfs(nums, i + 1, tmp ^ nums[i]);
//        }
//    }
//};


//class Solution {
//    int ret = 0;
//    int tmp = 0;
//public:
//    int subsetXORSum(vector<int>& nums) {
//        dfs(nums, 0);
//        return ret;
//    }
//    void dfs(vector<int>& nums, int m)
//    {
//        ret += tmp;
//        for (int i = m; i < nums.size(); ++i)
//        {
//            tmp ^= nums[i];
//            dfs(nums, i + 1);
//            tmp ^= nums[i];  //回溯
//        }
//    }
//};


//class Solution {
//    vector<vector<int>> ret;
//    vector<int> path;
//    bool check[8];
//public:
//    vector<vector<int>> permuteUnique(vector<int>& nums) {
//        //先排序，为了保证当前判断的时候剪枝
//        sort(nums.begin(), nums.end());
//        dfs(nums);
//        return ret;
//    }
//    void dfs(vector<int>& nums)
//    {
//        if (path.size() == nums.size())
//        {
//            ret.push_back(path);
//            return;
//        }
//        //单层
//        for (int i = 0; i < nums.size(); ++i)
//        {
//            if (i > 0 && nums[i] == nums[i - 1] && check[i - 1] == false) continue; //剪枝
//            if (check[i]) continue; //使用过了
//            check[i] = true;
//            path.push_back(nums[i]);
//            dfs(nums);
//            //回溯
//            check[i] = false;
//            path.pop_back();
//        }
//    }
//};


