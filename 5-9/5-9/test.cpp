#include <iostream>
#include <vector>
#include <queue>
using namespace std;


//class Solution {
//public:
//    int maxSales(vector<int>& sales) {
//        //动态规划
//        int res = INT_MIN;
//        int sum = 0;
//        for (int i = 0; i < sales.size(); ++i)
//        {
//            if (sum < 0) sum = sales[i];
//            else sum += sales[i];
//            res = max(res, sum);
//        }
//        return res;
//    }
//};



//class MedianFinder {
//public:
//    /** initialize your data structure here. */
//    void addNum(int num) {
//        //判断当前数是否比左侧堆顶元素大
//        if (left.size() == 0) left.push(num);
//        else
//        {
//            if (left.top() > num)
//            {
//                //判断左右堆的大小
//                if (left.size() == right.size()) left.push(num);
//                else
//                {
//                    //把左边堆顶数据放入到右边，如果插入
//                    right.push(left.top());
//                    left.pop();
//                    left.push(num);
//                }
//            }
//            else
//            {
//                if (right.size() < left.size()) right.push(num);
//                else {
//                    //把右边的堆顶放入到左边，然后插入 到右边
//                    left.push(right.top());
//                    right.pop();
//                    right.push(num);
//                }
//            }
//        }
//    }
//    double findMedian() {
//        return left.size() == right.size() ? (left.top() + right.top()) / 2.0 : left.top();
//    }
//private:
//    priority_queue<int> left;   //左边是大堆
//    priority_queue<int, vector<int>, greater<int>> right;  //右边是小堆
//};


//class Solution {
//    Node* pre = nullptr;  //记录上一个节点
//    Node* head = nullptr; //记录头节点
//public:
//    void dfs(Node* cur)
//    {
//        if (cur == nullptr) return;
//        //判断pre节点是否为空，空就说明是头节点
//        dfs(cur->left);
//        if (pre == nullptr) head = cur;
//        else
//        {
//            //修改指针指向
//            cur->left = pre;
//            pre->right = cur;
//        }
//        pre = cur;
//        dfs(cur->right);
//    }
//    Node* treeToDoublyList(Node* root) {
//        if (root == nullptr) return root;
//        dfs(root);
//        //连接头尾节点，使之循环
//        head->left = pre;
//        pre->right = head;
//        return head;
//    }
//};


