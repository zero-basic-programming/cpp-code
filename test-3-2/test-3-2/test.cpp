//class Solution {
//public:
//    ListNode* removeNthFromEnd(ListNode* head, int n) {
//        //创建头节点
//        ListNode* guard = new ListNode(0);
//        guard->next = head;
//        //前后指针法，前指针走n-1步，然后迭代走删除即可
//        ListNode* front = head;
//        ListNode* tail = guard;
//        while (--n)
//        {
//            front = front->next;
//        }
//        while (front->next)
//        {
//            front = front->next;
//            tail = tail->next;
//        }
//        //删除
//        ListNode* tmp = tail->next;
//        tail->next = front;
//        ListNode* newhead = guard->next;
//        delete tmp;
//        delete guard;
//        return newhead;
//    }
//};

//class Solution {
//public:
//    int findlen(ListNode* cur)
//    {
//        int count = 0;
//        while (cur)
//        {
//            ++count;
//            cur = cur->next;
//        }
//        return count;
//    }
//    ListNode* getIntersectionNode(ListNode* headA, ListNode* headB) {
//        //遍历两个链表计算它们的长度，然后前指针先走长度差步，前后指针再一起走，如果相等就是焦点
//        ListNode* cur1 = headA;
//        ListNode* cur2 = headB;
//        int lenA = findlen(cur1);
//        int lenB = findlen(cur2);
//        ListNode* longlist = headA;
//        ListNode* shortlist = headB;
//        if (lenB > lenA)
//        {
//            longlist = headB;
//            shortlist = headA;
//        }
//        //长list先走差距步
//        int gap = abs(lenA - lenB);
//        while (gap--)
//        {
//            longlist = longlist->next;
//        }
//        while (longlist != shortlist)
//        {
//            longlist = longlist->next;
//            shortlist = shortlist->next;
//        }
//        return longlist;
//    }
//};

//class Solution {
//public:
//    ListNode* getIntersectionNode(ListNode* headA, ListNode* headB) {
//        //两个指针，遍历完当前链表就走下个链表，如果遇到nullptr就是不存在，否则必定遇到相交节点
//        if (headA == nullptr || headB == nullptr)
//        {
//            return nullptr;
//        }
//        ListNode* pa = headA;
//        ListNode* pb = headB;
//        while (pa != pb)
//        {
//            pa = pa == nullptr ? headB : pa->next;
//            pb = pb == nullptr ? headA : pb->next;
//        }
//        return pa;
//    }
//};

//class Solution {
//public:
//    ListNode* detectCycle(ListNode* head) {
//        if (head == nullptr || head->next == nullptr)
//        {
//            return nullptr;
//        }
//        //使用快慢指针找到相遇节点，然后一个节点从头开始，一个节点从相遇节点开始，它们相遇就是第一个节点
//        ListNode* slow = head;
//        ListNode* fast = head;
//        while (fast && fast->next)
//        {
//            slow = slow->next;
//            fast = fast->next->next;
//            //找到相遇点
//            if (slow == fast)
//            {
//                ListNode* meet = fast;
//                while (meet != head)
//                {
//                    meet = meet->next;
//                    head = head->next;
//                }
//                return meet;
//            }
//        }
//        //快指针遇到空就说明没有相交
//        return nullptr;
//    }
//};


//class Solution {
//public:
//    vector<vector<int>> generateMatrix(int n) {
//        //就使用正常的顺序去走即可，但是我们需要控制一些变量
//        int x = 0;//考虑到可能有几圈，所以我们要记录每次一圈的起始位置
//        int y = 0;
//        int loop = n / 2;//总共需要走的圈数
//        int offset = 1;//这里我们使用左闭右开的方式，每走一圈都需要++
//        int count = 1;//放置的结果
//        int i, j;//记录行列
//        vector<vector<int>> result(n, vector<int>(n, 0));//记录结果
//        while (loop--)
//        {
//            //每次走完一圈都需要更新一下i，j
//            i = x;
//            j = y;
//            //左到右
//            for (j = y; j < n - offset; ++j)
//            {
//                result[i][j] = count++;
//            }
//            //上到下
//            for (i = x; i < n - offset; ++i)
//            {
//                result[i][j] = count++;
//            }
//            //右到左
//            for (; j > y; --j)
//            {
//                result[i][j] = count++;
//            }
//            //下到上
//            for (; i > x; --i)
//            {
//                result[i][j] = count++;
//            }
//            //更新x和y
//            ++x;
//            ++y;
//            ++offset;
//        }
//        //这里看需不需要补最中间的数，如果n%2有余数就需要
//        if (n % 2)
//        {
//            result[x][y] = count;
//        }
//        return result;
//    }
//};

