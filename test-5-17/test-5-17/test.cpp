#include <iostream>
using namespace std;

//class Solution {
//public:
//    TreeNode* deleteNode(TreeNode* root, int key) {
//        if (root == nullptr)
//        {
//            //遍历完整棵树没有该节点
//            return nullptr;
//        }
//        if (root->val == key)
//        {
//            //找到了
//            //1.该节点是叶子节点，直接删除，然后返回空即可
//            if (root->left == nullptr && root->right == nullptr)
//            {
//                delete root;
//                return nullptr;
//            }
//            //2.不是叶子节点，但是其左孩子为空
//            if (root->left == nullptr)
//            {
//                //删除该节点，然后返回其右节点
//                TreeNode* right = root->right;
//                delete root;
//                return right;
//            }
//            //3.不是叶子节点，但是其右孩子为空
//            if (root->right == nullptr)
//            {
//                //删除该节点，然后返回其左节点
//                TreeNode* left = root->left;
//                delete root;
//                return left;
//            }
//            //4.左右节点都不为空，把左孩子托孤给右孩子的最左节点
//            TreeNode* nodeleft = root->left;
//            TreeNode* noderight = root->right;
//            TreeNode* tmp = root->right;
//            while (tmp->left)
//            {
//                tmp = tmp->left;
//            }
//            //托孤
//            tmp->left = nodeleft;
//            delete root;
//            return noderight;
//        }
//        //往左右遍历
//        if (root->val < key)  root->right = deleteNode(root->right, key);
//        if (root->val > key)  root->left = deleteNode(root->left, key);
//        return root;
//    }
//};


//class Solution {
//public:
//    TreeNode* deleteNode(TreeNode* root, int key) {
//        if (root == nullptr) return nullptr;
//        TreeNode* node = root;
//        TreeNode* parent = nullptr;
//        while (node)
//        {
//            parent = node;
//            if (node->val < key)
//            {
//                node = node->right;
//            }
//            else if (node->val > key)
//            {
//                node = node->left;
//            }
//            else {
//                //找到了
//                //只有一个节点
//                if (node == parent)
//                {
//                    delete node;
//                    return nullptr;
//                }
//                //1.为叶子节点
//                if (!node->left && !node->right)
//                {
//                    if (parent->left == node)
//                    {
//                        parent->left = nullptr;
//                    }
//                    else {
//                        parent->right == nullptr;
//                    }
//                    delete node;
//                }
//                //2.左为空
//                if (!node->left)
//                {
//                    if (parent->left == node)
//                    {
//                        parent->left = node->right;
//                    }
//                    else {
//                        parent->right == node->right;
//                    }
//                    delete node;
//                }
//                //3.右为空
//                if (!node->right)
//                {
//                    if (parent->left == node)
//                    {
//                        parent->left = node->left;
//                    }
//                    else {
//                        parent->right == node->left;
//                    }
//                    delete node;
//                }
//                //4.左右都不为空，托孤
//                TreeNode* noderight = node->right;
//                TreeNode* nodeleft = node->left;
//                TreeNode* tmp = node->right;
//                while (tmp->left)
//                {
//                    tmp = tmp->left;
//                }
//                tmp->left = nodeleft;
//                if (parent->left == node)
//                {
//                    parent->left = noderight;
//                }
//                else {
//                    parent->right == noderight;
//                }
//                delete node;
//            }
//        }
//    }
//    //没找到
//    if (node == nullptr)
//    {
//        return nullptr;
//    }
//    return root;
//}
//};


//class Solution {
//public:
//    TreeNode* trimBST(TreeNode* root, int low, int high) {
//        if (root == nullptr) return nullptr; //遇到叶子节点直接返回
//        //如果遇到小于low的值，那就把其右节点给上一层
//        if (root->val < low)
//        {
//            TreeNode* noderight = trimBST(root->right, low, high);
//            return noderight;
//        }
//        //如果遇到大于的值，那就把其左节点给上一层
//        if (root->val > high)
//        {
//            TreeNode* nodeleft = trimBST(root->left, low, high);
//            return nodeleft;
//        }
//        root->left = trimBST(root->left, low, high);
//        root->right = trimBST(root->right, low, high);
//        return root;
//    }
//};


//class Solution {
//public:
//    TreeNode* trimBST(TreeNode* root, int low, int high) {
//        //使用非递归
//        if (root == nullptr) return nullptr;
//        //处理头节点，保证头节点在指定的范围内
//        while (root && (root->val < low || root->val > high))
//        {
//            if (root->val < low)
//            {
//                //往右走
//                root = root->right;
//            }
//            else
//            {
//                root = root->left;  // 大于大区间
//            }
//        }
//        //这里的root节点一定是在指定区间内的
//        TreeNode* cur = root; // 往root的左右两边进行遍历，只需要遍历两次就可以修改完成
//        while (cur)
//        {
//            while (cur->left && cur->left->val < low)
//            {
//                //往其右树遍历
//                cur->left = cur->left->right;
//            }
//            cur = cur->left;
//        }
//        //遍历root的右树
//        cur = root;
//        while (cur)
//        {
//            while (cur->right && cur->right->val > high)
//            {
//                cur->right = cur->right->left;
//            }
//            cur = cur->right;
//        }
//        return root;
//    }
//};

