#include <iostream>
#include <vector>
using namespace std;


//class Solution {
//public:
//    bool dfs(TreeNode* t1, TreeNode* t2)
//    {
//        if (t1 == nullptr && t2) return false;
//        if (t1 && t2 == nullptr) return false;
//        if (t1 == nullptr && t2 == nullptr) return true;
//        if (t1->val != t2->val) return false;
//        return dfs(t1->left, t2->right) && dfs(t1->right, t2->left);
//    }
//    bool checkSymmetricTree(TreeNode* root) {
//        if (root == nullptr) return true;
//        if (dfs(root->left, root->right)) return true;
//        return false;
//    }
//};


//class Solution {
//public:
//    bool dfs(TreeNode* t1, TreeNode* t2)
//    {
//        if (t1 == nullptr && t2 == nullptr) return true;
//        if (t1 == nullptr || t2 == nullptr) return false;
//        return t1->val == t2->val && dfs(t1->left, t2->right) && dfs(t1->right, t2->left);
//    }
//    bool checkSymmetricTree(TreeNode* root) {
//        return dfs(root, root);
//    }
//};


//class Solution {
//public:
//    bool is_digit(char ch)
//    {
//        if ('0' <= ch && ch <= '9') return true;
//        return false;
//    }
//    bool validNumber(string s) {
//        int i = 0;
//        int n = s.size();
//        //去掉收尾空格
//        while (i < n && s[i] == ' ') ++i;
//        s = s.substr(i);
//        while (i < s.size())
//        {
//            //处理空格
//            while (i < n && s[i] == ' ') ++i;
//            if (i == n) break;
//            if (s[i] == 'e' || s[i] == 'E')
//            {
//                //处理前面那个字符是e，后面接整数
//                if (s[i] != '+' && s[i] != '-' && !is_digit(s[i])) return false;
//                ++i;
//                while (i < n && is_digit(s[i])) ++i;
//                if (i == n) break;
//            }
//            //这里必须以+-号或者数字开始，接小数或者整数
//            if (s[i] != '+' && s[i] != '-' && !is_digit(s[i])) return false;
//            ++i;
//            while (i < n && is_digit(s[i])) ++i;
//            if (i == n) break;
//            //这里可能出现. 
//            if (s[i] == '.')
//            {
//                //跳过该字符，然后继续找数字字符
//                ++i;
//                while (i < n && is_digit(s[i])) ++i;
//                //后面是任何非数字字符都需要下一次进行处理
//            }
//        }
//        return s.empty() ? false : true;
//    }
//};


//class Solution {
//public:
//    vector<int> trainingPlan(vector<int>& actions) {
//        //前后双指针
//        int n = actions.size();
//        if (n == 0 || n == 1 || n == 2) return actions;
//        int front = 1, tail = 2;
//        while (tail < n)
//        {
//            swap(actions[front], actions[tail]);
//            ++front;
//            tail += 2;
//        }
//        return actions;
//    }
//};


//class Solution {
//public:
//    vector<int> trainingPlan(vector<int>& actions) {
//        int n = actions.size();
//        if (n == 0 || n == 1) return actions;
//        int front = -1, tail = 0;  //tail找奇数，tail找偶数
//        while (tail < n)
//        {
//            while (tail < n && actions[tail] % 2 == 0) ++tail;
//            if (tail == n) break;  //没有奇数就不需要找了
//            if (++front != tail)
//            {
//                swap(actions[front], actions[tail]);
//            }
//            ++tail;
//        }
//        return actions;
//    }
//};
//
//
//int main()
//{
//    vector<int> arr = { 1,2,3,4,5 };
//    Solution().trainingPlan(arr);
//    return 0;
//}

