#include <iostream>

//class Solution {
//private:
//    vector<vector<string>>result;
//    //row控制当前层，每一层用循环处理，row用递归处理
//    void backtracking(int row, vector<string>& vs, int n)
//    {
//        //终止条件
//        if (row == n)
//        {
//            result.push_back(vs);
//            return;
//        }
//        //单层逻辑
//        for (int i = 0; i < n; ++i)
//        {
//            if (is_vaild(vs, row, i, n))
//            {
//                vs[row][i] = 'Q';
//                backtracking(row + 1, vs, n); //如果填完的当前层那就往下一层进行处理，否则继续往后找
//                vs[row][i] = '.'; // 回溯
//            }
//        }
//        //如果当前层没有结果就直接返回
//        return;
//    }
//    bool is_vaild(vector<string>& vs, int row, int col, int n)
//    {
//        //在整个棋盘中查找当前这个元素是否符合规则
//        //行在递归函数已经处理过了
//        //判断列
//        for (int i = 0; i < row; ++i) //这里剪枝了，因为对于后面的数据是后面不需要判断的
//        {
//            if (vs[i][col] == 'Q')   return false;
//        }
//        //判断45斜线
//        for (int i = row - 1, j = col - 1; i >= 0 && j >= 0; --i, --j)
//        {
//            if (vs[i][j] == 'Q') return false;
//        }
//        //判断135斜线
//        for (int i = row - 1, j = col + 1; i >= 0 && j < n; --i, ++j)
//        {
//            if (vs[i][j] == 'Q') return false;
//        }
//        return true;
//    }
//public:
//    vector<vector<string>> solveNQueens(int n) {
//        vector<string> vs(n, string(n, '.')); // 用来存放一次的结果
//        backtracking(0, vs, n);
//        return result;
//    }
//};


//class Solution {
//public:
//    bool backtracking(vector<vector<char>>& board)
//    {
//        //这里如果循环完整个流程自然就结束了，如果遇到错就会立刻返回
//        for (int i = 0; i < board.size(); ++i)
//        {
//            for (int j = 0; j < board[0].size(); ++j)
//            {
//                //判断1到9之间的数放入其中是否符合条件
//                if (board[i][j] == '.')
//                {
//                    for (char k = '1'; k <= '9'; ++k)
//                    {
//                        if (is_valid(board, i, j, k))
//                        {
//                            board[i][j] = k;
//                            if (backtracking(board)) return  true; // 如果找到一组就立刻返回
//                            //回溯
//                            board[i][j] = '.';
//                        }
//                    }
//                    //这里1到9试过都不行，说明已经不符合条件，直接返回
//                    return false;
//                }
//            }
//        }
//        return true;
//    }
//    bool is_valid(vector<vector<char>>& board, int row, int col, char val)
//    {
//        //判断当前行
//        for (int i = 0; i < 9; ++i)
//        {
//            if (board[row][i] == val)
//            {
//                return false;
//            }
//        }
//        //判断当前列
//        for (int i = 0; i < 9; ++i)
//        {
//            if (board[i][col] == val)
//            {
//                return false;
//            }
//        }
//        //判断当前9宫格
//        int x = (row / 3) * 3; int y = (col / 3) * 3; //这样可以确保能到当前9宫格的第一个
//        for (int i = x; i < x + 3; ++i)
//        {
//            for (int j = y; j < y + 3; ++j)
//            {
//                if (board[i][j] == val)
//                {
//                    return false;
//                }
//            }
//        }
//        return true;
//    }
//    void solveSudoku(vector<vector<char>>& board) {
//        //这里为了防止递归太深，当搜索到错误的时候应该立刻返回，来提高效率
//        backtracking(board);
//    }
//};