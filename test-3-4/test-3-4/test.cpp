#include <iostream>
using namespace std;


//class Solution {
//public:
//    int fourSumCount(vector<int>& nums1, vector<int>& nums2, vector<int>& nums3, vector<int>& nums4) {
//        //使用hash放第一和第二个数组的和，然后遍历3，4数组
//        unordered_map<int, int> m;//第一个放和，第二个放和出现的个数
//        for (auto e : nums1)
//        {
//            for (auto c : nums2)
//            {
//                m[e + c]++;
//            }
//        }
//        //遍历3，4数组找结果
//        int count = 0;
//        for (auto e : nums3)
//        {
//            for (auto c : nums4)
//            {
//                if (m.find(0 - (e + c)) != m.end())
//                {
//                    count += m[0 - (e + c)];
//                }
//            }
//        }
//        return count;
//    }
//};

//class Solution {
//public:
//    bool canConstruct(string ransomNote, string magazine) {
//        unordered_map<char, int> m;
//        for (auto& ch : magazine)
//        {
//            m[ch]++;
//        }
//        for (auto& ch : ransomNote)
//        {
//            auto it = m.find(ch);
//            if (it == m.end())
//            {
//                return false;
//            }
//            --it->second;
//            if (it->second == 0)
//            {
//                m.erase(it);
//            }
//        }
//        return true;
//    }
//};

