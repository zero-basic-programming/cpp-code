#include <iostream>
#include <vector>
using namespace std;

//class Solution {
//public:
//    int maxProfit(vector<int>& prices) {
//        int n = prices.size();
//        vector<vector<int>> dp(n, vector<int>(3));
//        //初始化
//        dp[0][0] = -prices[0];
//        //填表
//        for (int i = 1; i < n; ++i)
//        {
//            //0：买入  1：冷冻期  2：可交易
//            dp[i][0] = max(dp[i - 1][2] - prices[i], dp[i - 1][0]);
//            dp[i][1] = dp[i - 1][0] + prices[i];
//            dp[i][2] = max(dp[i - 1][1], dp[i - 1][2]);
//        }
//        //最终结果不可能是买入状态，因为还有钱在买股票
//        return max(dp[n - 1][1], dp[n - 1][2]);
//    }
//};


//class Solution {
//public:
//    int maxProfit(vector<int>& prices, int fee) {
//        int n = prices.size();
//        //f:买入  g:可交易
//        vector<int> f(n);
//        auto g = f;
//        //初始化 
//        f[0] = -prices[0];
//        for (int i = 1; i < n; ++i)
//        {
//            f[i] = max(f[i - 1], g[i - 1] - prices[i]);
//            g[i] = max(f[i - 1] + prices[i] - fee, g[i - 1]);
//        }
//        return g[n - 1];
//    }
//};


