#include <iostream>
#include <vector>
using namespace std;


//class Solution {
//public:
//    void hanota(vector<int>& A, vector<int>& B, vector<int>& C) {
//        dfs(A, B, C, A.size());
//    }
//    void dfs(vector<int>& a, vector<int>& b, vector<int>& c, int n)
//    {
//        if (n == 1)
//        {
//            c.push_back(a.back());
//            a.pop_back();
//            return;
//        }
//        dfs(a, c, b, n - 1);
//        c.push_back(a.back());
//        a.pop_back();
//        dfs(b, a, c, n - 1);
//    }
//};


//class Solution {
//public:
//    ListNode* mergeTwoLists(ListNode* list1, ListNode* list2) {
//        if (list1 == nullptr) return list2;
//        if (list2 == nullptr) return list1;
//        if (list1->val < list2->val)
//        {
//            list1->next = mergeTwoLists(list1->next, list2);
//            return list1;
//        }
//        else
//        {
//            list2->next = mergeTwoLists(list1, list2->next);
//            return list2;
//        }
//    }
//};


//class Solution {
//public:
//    ListNode* reverseList(ListNode* head) {
//        if (head == nullptr || head->next == nullptr) return head;
//        ListNode* newhead = reverseList(head->next);
//        head->next->next = head;
//        head->next = nullptr;
//        return newhead;
//    }
//};


//class Solution {
//public:
//    ListNode* swapPairs(ListNode* head) {
//        if (head == nullptr || head->next == nullptr) return head;
//        ListNode* next = head->next;
//        ListNode* last = swapPairs(next->next);
//        next->next = head;
//        head->next = last;
//        return next;
//    }
//};


//class Solution {
//public:
//    double myPow(double x, int n) {
//        return n < 0 ? 1.0 / (pow(x, -(long long)n)) : pow(x, (long long)n);
//    }
//    double pow(double x, long long n)
//    {
//        if (n == 0) return 1;
//        double tmp = pow(x, n / 2);
//        return n % 2 == 1 ? tmp * tmp * x : tmp * tmp;
//    }
//};



//class Solution {
//    unordered_map<string, int> hash;   //映射元素和下标的关系
//    vector<int> ret;
//    int n;
//public:
//    bool dfs(string& s, int start)
//    {
//        for (int i = start; i < n; ++i)
//        {
//            //判断到结尾
//            if (i == n - 1)
//            {
//                string tmp = s.substr(start, i - start + 1);
//                if (hash.count(tmp) > 0)
//                {
//                    ret.push_back(hash[tmp]);
//                    return true;
//                }
//                else return false;   //后续需要进行回退
//            }
//            string tmpstr = s.substr(start, i - start + 1);
//            if (hash.count(tmpstr) > 0)
//            {
//                ret.push_back(hash[tmpstr]);
//                if (dfs(s, i + 1)) return true;
//                ret.pop_back();
//                continue;  //后面错了，那就继续往后找
//            }
//            //没找到，继续找
//        }
//        return true;
//    }
//    vector<int> StringAssemble(string destStr, vector<string>& elements) {
//        n = destStr.size();
//        for (int i = 0; i < elements.size(); ++i)
//        {
//            hash[elements[i]] = i;
//        }
//        dfs(destStr, 0);   //通过函数递归调用来完成错误情况的回溯处理
//        return ret;
//    }
//};


