#include <iostream>
#include <vector>
#include <string>
using namespace std;


class Solution {
public:
    vector<int> findSubstring(string s, vector<string>& words) {
        int sz = words.size();
        int n = words[0].size();
        int sn = s.size();
        vector<int> res;
        vector<int> arr(sz, 1); //进行伪删除
        string tmp; //用来记录一次搜索的结果
        int flag = 0; //用来标记连续
        int count = 0;
        int flag1 = 0;
        for (int left = 0, right = 0; right < sn; ++right)
        {
            //进窗口
            tmp += s[right];
            if (right - left + 1 == n)
            {
                //遍历查找是否存在相同的string
                for (int i = 0; i < sz; ++i)
                {
                    if (arr[i] == 0) continue; //已经删除过了
                    if (tmp == words[i])
                    {
                        arr[i] = 0;//进行标记删除
                        left += n; //跳过当前字串
                        flag = 1;
                        ++count;
                        flag1 = 2;
                        tmp.clear();
                        break;
                    }
                }
                //当次没有结果
                if (flag1 == 2)
                {
                    //清空标记删除
                    if (flag == 1)
                    {
                        for (auto e : arr)    e = 1;
                    }
                    tmp.erase(0);
                }
                if (flag1 == 0)
                {
                    ++left;
                }
                flag1 = 0;
                //收集结果
                if (count == sz)
                {
                    res.push_back(right - sz * n + 1);
                    count = 0;
                    for (auto& e : arr)    e = 1;
                }
            }
        }
        return res;
    }
};


int main()
{
    vector<string> vs = { "foo","bar" };
    vector<int> res = Solution().findSubstring("barfoothefoobarman", vs);
    for (auto e : res) cout << e << endl;
    return 0;
}