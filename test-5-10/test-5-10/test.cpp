#include <iostream>
using namespace std;

//class Solution {
//private:
//    TreeNode* prev = nullptr; // 用来记录root的上一个节点的位置
//    int result = INT_MAX; // 保存结果
//public:
//    int getMinimumDifference(TreeNode* root) {
//        //利用中序记录其上一个节点，然后比较当前节点和上一个节点的差值即可
//        if (root == nullptr) return result;
//        getMinimumDifference(root->left);
//        if (prev)
//        {
//            //比较prev和当前节点的差值和result
//            result = min(result, root->val - prev->val);
//        }
//        //更新prev
//        prev = root;
//        getMinimumDifference(root->right);
//        return result;
//    }
//};


//class Solution {
//public:
//    int getMinimumDifference(TreeNode* root) {
//        //使用栈模拟递归
//        stack<TreeNode*> st;
//        int result = INT_MAX;
//        TreeNode* prev = nullptr;
//        TreeNode* cur = root;
//        while (cur || !st.empty())
//        {
//            //左不为空就一直进栈
//            if (cur)
//            {
//                st.push(cur);
//                cur = cur->left;
//            }
//            else
//            {
//                //开始出栈，并往右走
//                cur = st.top();
//                st.pop();
//                //比较
//                if (prev)    result = min(result, cur->val - prev->val);
//                prev = cur;
//                cur = cur->right;
//            }
//        }
//        return result;
//    }
//};


//class Solution {
//private:
//    int maxcount = INT_MIN;//记录放入的最大count
//    int count = 1;// 用来记录次数
//    int preval = INT_MIN; //用来记录前一个的值
//public:
//    void traversal(TreeNode* cur, vector<int>& v)
//    {
//        if (cur == nullptr)  return;
//        traversal(cur->left, v);
//        if (preval == INT_MIN)
//        {
//        }
//        else if (preval == cur->val)
//        {
//            ++count;
//        }
//        else {
//            //考虑放入结果集中
//            if (count > maxcount)
//            {
//                v.clear(); // 清除之前记录的数据
//                v.push_back(preval);
//                maxcount = count;
//            }
//            else if (count == maxcount)
//            {
//                v.push_back(preval);
//            }
//            else {
//                count = 1;
//            }
//        }
//        preval = cur->val;
//        traversal(cur->right, v);
//    }
//    vector<int> findMode(TreeNode* root) {
//        vector<int> result;
//        traversal(root, result);
//        return result;
//    }
//};