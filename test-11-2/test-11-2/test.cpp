#include <iostream>
#include <vector>
using namespace std;

//class Solution {
//public:
//    bool isMatch(string s, string p) {
//        int m = s.size();
//        int n = p.size();
//        s = " " + s; p = " " + p;
//        vector<vector<bool>> dp(m + 1, vector<bool>(n + 1, false));
//        //初始化
//        dp[0][0] = true;
//        for (int j = 2; j <= n; j += 2)
//        {
//            if (p[j] == '*') dp[0][j] = true;
//            else break;
//        }
//        for (int i = 1; i <= m; ++i)
//        {
//            for (int j = 1; j <= n; ++j)
//            {
//                if (p[j] == '*')
//                {
//                    dp[i][j] = dp[i][j - 2] || (p[j - 1] == '.' || p[j - 1] == s[i])
//                        && dp[i - 1][j];
//                }
//                else
//                    dp[i][j] = (s[i] == p[j] || p[j] == '.') && dp[i - 1][j - 1];
//            }
//        }
//        return dp[m][n];
//
//    }
//};



//class Solution {
//public:
//    bool isInterleave(string s1, string s2, string s3) {
//        int m = s1.size(), n = s2.size();
//        if (m + n != s3.size()) return false;
//        s1 = " " + s1;
//        s2 = " " + s2;
//        s3 = " " + s3;
//        vector<vector<bool>>dp(m + 1, vector<bool>(n + 1, false));
//        //初始化
//        dp[0][0] = true;
//        //第一行
//        for (int j = 1; j <= n; ++j) dp[0][j] = (s2[j] == s3[j]) && dp[0][j - 1];
//        //第一列
//        for (int i = 1; i <= m; ++i) dp[i][0] = (s1[i] == s3[i]) && dp[i - 1][0];
//        for (int i = 1; i <= m; ++i)
//        {
//            for (int j = 1; j <= n; ++j)
//            {
//                dp[i][j] = ((s1[i] == s3[i + j]) && dp[i - 1][j]) || ((s2[j] == s3[i + j])
//                    && dp[i][j - 1]);
//            }
//        }
//        return dp[m][n];
//    }
//};


