#include <iostream>
#include <vector>
using namespace std;

//class Solution {
//public:
//    int findMaxLength(vector<int>& nums) {
//        unordered_map<int, int> hash;
//        hash[0] = -1; //处理边界情况
//        int sum = 0; // 这里做一个转换，如果该元素是0那么就转换为-1，这样相当于找和为0的结果
//        int ret = 0;
//        for (int i = 0; i < nums.size(); ++i)
//        {
//            sum += nums[i] == 0 ? -1 : 1;
//            if (hash.count(sum))   ret = max(ret, i - hash[sum]);
//            else hash[sum] = i;
//        }
//        return ret;
//    }
//};



//class Solution {
//public:
//    vector<vector<int>> matrixBlockSum(vector<vector<int>>& mat, int k) {
//        int m = mat.size();
//        int n = mat[0].size();
//        vector<vector<int>> dp(m + 1, vector<int>(n + 1));
//        vector<vector<int>> ret(m, vector<int>(n));
//        //填写前缀和dp
//        for (int i = 1; i <= m; ++i)
//        {
//            for (int j = 1; j <= n; ++j)
//            {
//                dp[i][j] = mat[i - 1][j - 1] + dp[i - 1][j] + dp[i][j - 1] - dp[i - 1][j - 1];
//            }
//        }
//        //填写返回表
//        for (int i = 0; i < m; ++i)
//        {
//            for (int j = 0; j < n; ++j)
//            {
//                //找到左上和右下的边界位置
//                int x1 = max(i - k, 0) + 1;
//                int y1 = max(j - k, 0) + 1;
//                int x2 = min(i + k, m - 1) + 1;
//                int y2 = min(j + k, n - 1) + 1;
//                ret[i][j] = dp[x2][y2] - dp[x2][y1 - 1] - dp[x1 - 1][y2] + dp[x1 - 1][y1 - 1];
//            }
//        }
//        return ret;
//    }
//};



//class Solution {
//public:
//    bool wordBreak(string s, vector<string>& wordDict) {
//        //优化，更快找到结果
//        unordered_set<string> hash;
//        for (auto& str : wordDict) hash.insert(str);
//        int n = s.size();
//        vector<bool> dp(n + 1);
//        dp[0] = true;
//        s = ' ' + s; //保证字符串和hash下标是一致的
//        for (int i = 1; i <= n; ++i)
//        {
//            for (int j = i; j >= 1; --j)
//            {
//                if (hash.count(s.substr(j, i - j + 1)) && dp[j - 1]) {
//                    dp[i] = true;
//                    break;
//                }
//            }
//        }
//        return dp[n];
//    }
//};