//# include <bits/stdc++.h>
//using namespace std;
//
//struct list_node {
//    int val;
//    struct list_node* next;
//};
//
//list_node* input_list(void)
//{
//    int n, val;
//    list_node* phead = new list_node();
//    list_node* cur_pnode = phead;
//    scanf("%d", &n);
//    for (int i = 1; i <= n; ++i) {
//        scanf("%d", &val);
//        if (i == 1) {
//            cur_pnode->val = val;
//            cur_pnode->next = NULL;
//        }
//        else {
//            list_node* new_pnode = new list_node();
//            new_pnode->val = val;
//            new_pnode->next = NULL;
//            cur_pnode->next = new_pnode;
//            cur_pnode = new_pnode;
//        }
//    }
//    return phead;
//}
//
//
//list_node* reverse_list(list_node* head, int L, int R)
//{
//    //////在下面完成代码
//    if (L == R)
//        return head;
//    list_node* cur = head;
//    //找到第一个位置
//    while (cur->val != L)
//    {
//        cur = cur->next;
//    }
//    //反转部分链表
//    list_node* n1 = cur;
//    list_node* n2 = cur->next;
//    list_node* n3 = nullptr;
//    while (n1->val != R)
//    {
//        n3 = n2->next;
//        n2->next = n1;
//        //迭代
//        n1 = n2;
//        n2 = n3;
//    }
//    cur->next = n2;
//    if (cur == head)
//        head = n1;
//    return head;
//}
//
//void print_list(list_node* head)
//{
//    while (head != NULL) {
//        printf("%d ", head->val);
//        head = head->next;
//    }
//    puts("");
//}
//
//
//int main()
//{
//    int L, R;
//    list_node* head = input_list();
//    scanf("%d%d", &L, &R);
//    list_node* new_head = reverse_list(head, L, R);
//    print_list(new_head);
//    return 0;
//}

//list_node* reverse_list(list_node* head, int L, int R)
//{
//    //////在下面完成代码
//    if (L == R && head->next == nullptr)
//        return head;
//    list_node* guard = new list_node();
//    guard->next = head;
//    list_node* prev = guard;
//    list_node* cur = head;
//    //找到第一个位置
//    while (cur->val != L)
//    {
//        prev = prev->next;
//        cur = cur->next;
//    }
//    //反转部分链表
//    list_node* n1 = cur;
//    list_node* n2 = cur->next;
//    list_node* n3 = nullptr;
//    while (n1->val != R)
//    {
//        n3 = n2->next;
//        n2->next = n1;
//        //迭代
//        n1 = n2;
//        n2 = n3;
//    }
//    cur->next = n2;
//    if (cur == head)
//        head = n1;
//    else
//        prev->next = n1;
//    delete guard;
//    return head;
//}


