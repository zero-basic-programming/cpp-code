#include <iostream>
#include <vector>
using namespace std;


//class Solution {
//public:
//    int removeDuplicates(vector<int>& nums) {
//        int n = nums.size();
//        if (n <= 2) return n;
//        int front = 2, tail = 2;
//        while (tail < n)
//        {
//            //尾指针比较，前指针来排除
//            if (nums[tail] != nums[front - 2])
//            {
//                nums[front] = nums[tail];
//                ++front;
//            }
//            ++tail;
//        }
//        return front;
//    }
//};


//class Solution {
//public:
//    int majorityElement(vector<int>& nums) {
//        //利用抵消的思想，因为最多的那个数是比n/2大的
//        int num = nums[0];
//        int count = 1;
//        for (int i = 1; i < nums.size(); ++i)
//        {
//            //这里有可能上一次抵消完了
//            if (count == 0)
//            {
//                count = 1;
//                num = nums[i];
//                continue;
//            }
//            if (nums[i] != num)
//            {
//                --count;
//            }
//            else ++count;
//        }
//        return num;
//    }
//};


//class Solution {
//public:
//    vector<int> twoSum(vector<int>& numbers, int target) {
//        //因为这里的数组是有序的，所以，我们可以通过双指针来进行操作
//        int left = 0, right = numbers.size() - 1;
//        while (left < right)
//        {
//            if (numbers[left] + numbers[right] > target)
//            {
//                --right;
//            }
//            else if (numbers[left] + numbers[right] < target)
//            {
//                ++left;
//            }
//            else return { left + 1,right + 1 };
//        }
//        return { -1,-1 };
//    }
//};


//void CountSort(vector<int>& arr)
//{
//	int n = arr.size();
//	//找到最大和最小那个
//	int maxnum = INT_MIN;
//	int minnum = INT_MAX;
//	for (auto e : arr)
//	{
//		if (e > maxnum) maxnum = e;
//		if (e < minnum) minnum = e;
//	}
//	int gap = maxnum - minnum + 1; //这里计算出直接差并且需要+1，为了防止有0这个存在需要多开空间
//	vector<int> hash(gap);
//	//关联映射关系
//	for (auto e : arr)
//	{
//		hash[e - minnum]++;
//	}
//	//还原回原数组
//	int j = 0;
//	for (int i = 0; i < gap; ++i)
//	{
//		while (hash[i] > 0)
//		{
//			arr[j++] = i + minnum;
//			--hash[i];
//		}
//	}
//}
//
//int main()
//{
//	vector<int> arr = { 3,5,6,3,6,7,8,0,9,7,4,2,1,0 };
//	CountSort(arr);
//	for (auto e : arr)
//	{
//		cout << e << " ";
//	}
//	return 0;
//}


