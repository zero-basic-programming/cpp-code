#include <iostream>
#include <vector>
using namespace std;

//int main() {
//    int n, q;
//    cin >> n >> q;
//    vector<long long> nums(n);
//    vector<long long> dp(n + 1);  //前缀和数组
//    for (auto& e : nums) cin >> e;
//    //初始化前缀和数组
//    for (int i = 1; i <= n; ++i)
//    {
//        dp[i] = dp[i - 1] + nums[i - 1];
//    }
//    while (q--)
//    {
//        int l, r;
//        cin >> l >> r;
//        cout << dp[r] - dp[l - 1] << endl;
//    }
//    return 0;
//}

//int main() {
//    int n, m, q;
//    cin >> n >> m >> q;
//    vector<vector<long long>> nums(n, vector<long long>(m));
//    for (auto& a : nums)
//    {
//        for (auto& e : a)
//            cin >> e;
//    }
//    vector<vector<long long>> dp(n + 1, vector<long long>(m + 1)); //前缀和数组
//    //初始化dp
//    for (int i = 1; i <= n; ++i)
//    {
//        for (int j = 1; j <= m; ++j)
//        {
//            dp[i][j] = dp[i - 1][j] + dp[i][j - 1] - dp[i - 1][j - 1] + nums[i - 1][j - 1];
//        }
//    }
//    //输出结果
//    while (q--)
//    {
//        int x1, y1, x2, y2;
//        cin >> x1 >> y1 >> x2 >> y2;
//        cout << dp[x2][y2] - dp[x1 - 1][y2]
//            - dp[x2][y1 - 1] + dp[x1 - 1][y1 - 1] << endl;
//    }
//    return 0;
//}


//class Solution {
//public:
//    int pivotIndex(vector<int>& nums) {
//        int n = nums.size();
//        vector<int> ldp(n);
//        auto rdp = ldp;
//        //初始化前缀和数组
//        for (int i = 1; i < n; ++i) ldp[i] = ldp[i - 1] + nums[i - 1];
//        for (int i = n - 2; i >= 0; --i) rdp[i] = rdp[i + 1] + nums[i + 1];
//        //从左到右找第一个满足条件的位置
//        for (int i = 0; i < n; ++i)
//        {
//            if (ldp[i] == rdp[i]) return i;
//        }
//        return -1;
//    }
//};

