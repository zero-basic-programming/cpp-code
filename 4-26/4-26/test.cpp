//#include <iostream>
//#include <string>
//#include <unordered_set>
//using namespace std;
//
//int main()
//{
//	unordered_set<string> hash;  //记录元素的个数,不重复
//	int n;
//	cin >> n;
//	while (n--)
//	{
//		int m;
//		cin >> m;
//		while (m--)
//		{
//			string str;
//			cin >> str;
//			hash.insert(str);
//		}
//	}
//	cout << hash.size() << endl;
//	return 0;
//}

//#include <iostream>
//#include <string>
//#include <queue>
//#include <vector>
//#include <algorithm>
//using namespace std;
//
//class cmp
//{
//public:
//    bool operator()(const pair<int, pair<int, int>>& p1, const pair<int, pair<int, int>>& p2)
//    {
//        //这里是建大堆
//        if (p1.second.first < p2.second.first) return true;
//        else if (p1.second.first > p2.second.first) return false;
//        else
//        {
//            //相等就看收藏数，如果收藏数还是相等就比下标的大小
//            if (p1.second.second < p2.second.second) return true;
//            else if (p1.second.second > p2.second.second) return false;
//            else return p1.first < p2.first;
//        }
//    }
//};
//
//int main()
//{
//    priority_queue<pair<int, pair<int, int>>, vector<pair<int, pair<int, int>>>, cmp> heap;  //下标，总支持，收藏
//    int n, k;
//    cin >> n >> k;
//    for (int i = 1; i <= n; ++i)
//    {
//        int love, learn;  //点赞和收藏
//        cin >> love >> learn;
//        int sum = love + 2 * learn;
//        heap.push(make_pair(i, make_pair(sum, learn)));
//    }
//    vector<int> ret;
//    for (int i = 0; i < k; ++i) {
//        ret.push_back(heap.top().first);
//        heap.pop();
//    }
//    sort(ret.begin(), ret.end());
//    for (auto e : ret) cout << e << " ";
//    return 0;
//}


#include <vector>
#include <iostream>
#include <algorithm>
using namespace std;

int main()
{
    int n = 5 ,x = 8;
    vector<int> nums = { 1,2,3,4,10 };
    //排序
    sort(nums.begin(), nums.end());
    int sum = 0, i = n - 1;
    bool flag = true;   //只能使用一次多发送
    int count = 0;  //统计个数
    while (sum < x && i >= 0)
    {
        int res = x - sum;  //计算得到目标值的差距
        if (nums[i] <= res && flag)
        {
            //这时候需要当前账号发送多次文章
            flag = false;
            while (sum <= x)
            {
                sum += nums[i] / 2;
            }
            //这里一定大于等于了
            sum -= nums[i] / 2;
            ++count;
        }
        else if (nums[i] / 2 <= res)
        {
            //这里一个账号只能发送一次
            sum += nums[i] / 2;
            ++count;
            //如果大了，就放弃
            if (sum > x)
            {
                sum -= nums[i] / 2;
                --count;
            }
        }
        //如果当前值还大于x，直接跳过下一个
        --i;
        if (sum == x) break;
    }
    int ret = count == 0 ? -1 : count;
    cout << ret << endl;
    return 0;
}


#include <vector>
#include <iostream>
using namespace std;

int main()
{
    int n, x;
    cin >> n >> x;
    vector<int> nums(n);
    for (int i = 0; i < n; ++i)
    {
        cin >> nums[i];
        nums[i] /= 2;
    }
    vector<vector<int>> f(n + 1, vector<int>(x + 1, 0xffffff));   //没有多选
    vector<vector<int>> g(n + 1, vector<int>(x + 1, 0xffffff));   //多选
    for (int i = 1; i <= n; ++i)
    {
        for (int j = 0; j <= x; ++j)
        {
            f[i][j] = f[i - 1][j];
            g[i][j] = g[i - 1][j];
            if (j >= nums[i]) f[i][j] = min(f[i][j], f[i - 1][j - nums[i]] + 1);
            if (j >= 2 * nums[i]) g[i][j] = min(g[i][j], f[i - 1][j - 2 * nums[i]] + f[i - 1][j - nums[i]]);
        }
    }
    int ret = 0xffffff;
    for (int i = 1; i <= n; ++i)
    {
        ret = min(ret, min(f[i][x], g[i][x]));
    }
    cout << ret << endl;
    return 0;
}