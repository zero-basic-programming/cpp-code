#include <iostream>
#include <vector>
using namespace std;


//class Solution {
//public:
//    bool canJump(vector<int>& nums) {
//        int n = nums.size();
//        vector<bool> dp(n);
//        //初始化
//        dp[0] = true;
//        for (int i = 1; i < n; ++i)
//        {
//            //找之前的直接看看能不能跳到当前节点
//            for (int j = 0; j < i; ++j)
//            {
//                if (j + nums[j] == i)
//                {
//                    dp[i] = true;
//                }
//            }
//            //这里遍历完之后就是没有结果
//            dp[i] = false;
//        }
//        return dp[n - 1];
//    }
//};

void Qsort(vector<int>& arr, int l, int r)
{
	if (l >= r) return;
	//找基准点
	int key = arr[rand() % (r - l + 1) + l];
	int left = l - 1, right = r + 1;
	int i = l;
	while (i < right)
	{
		if (arr[i] < key) swap(arr[i++], arr[++left]);
		else if (arr[i] == key) ++i;
		else swap(arr[i], arr[--right]);
	}
	Qsort(arr, l, left);
	Qsort(arr, right, r);
}

void MergeSort(vector<int>& arr, int left, int right, vector<int>& tmp)
{
	if (left >= right) return;
	int mid = (left + right) >> 1;
	MergeSort(arr, left, mid, tmp);
	MergeSort(arr, mid + 1, right, tmp);
	int begin1 = left, end1 = mid;
	int begin2 = mid + 1, end2 = right;
	int i = left;
	while (begin1 <= end1 && begin2 <= end2)
	{
		tmp[i++] = arr[begin1] < arr[begin2] ? arr[begin1++] : arr[begin2++];
	}
	while (begin1 <= end1)
	{
		tmp[i++] = arr[begin1++];
	}
	while (begin2 <= end2)
	{
		tmp[i++] = arr[begin2++];
	}
	for (int j = left; j <= right; ++j)
		arr[j] = tmp[j];
}


int main()
{
	vector<int> arr = { 3,2,5,4,7,8,0,9,8,5,1 };
	int n = arr.size();
	vector<int> tmp(n);
	//Qsort(arr, 0, n - 1);
	MergeSort(arr, 0, n - 1, tmp);
	for (auto e : arr)
	{
		cout << e << " ";
	}
	return 0;
}