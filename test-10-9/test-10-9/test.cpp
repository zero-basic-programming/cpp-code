#include <iostream>
#include <vector>
#include <stack>
#include <time.h>
using namespace std;

//class Solution {
//public:
//    vector<vector<string>> groupAnagrams(vector<string>& strs) {
//        unordered_map<string, vector<string>> hash;//用来建立排序好的单词和原来的单词的映射关系
//        vector<vector<string>> ret;
//        for (auto& str : strs)
//        {
//            string sortstr = str;
//            sort(sortstr.begin(), sortstr.end());
//            hash[sortstr].push_back(str);
//        }
//        //在hash中找结果
//        for (auto& pair : hash)
//        {
//            ret.push_back(pair.second);
//        }
//        return ret;
//    }
//};


//快排非递归
void qsort(vector<int>& nums)
{
	stack<int> st;
	st.push(0);
	st.push(nums.size()-1);
	while (!st.empty())
	{
		//取出左右区间
		int right = st.top();
		st.pop();
		int left = st.top();
		st.pop();
		int mid = (left + right) >> 1;
		//[left,mid] [mid+1,right]
		//选随机数
		int key = nums[rand() % (right - left + 1) + left];
		int l = left - 1;
		int r = right + 1;
		int i = left;
		while (i < r)
		{
			if (nums[i] < key) swap(nums[++l], nums[i++]);
			else if (nums[i] == key) ++i;
			else swap(nums[--r], nums[i]);
		}
		//[left,l] [l+1,r-1] [r,right]
		if (left < l)
		{
			st.push(left);
			st.push(l);
		}
		if (r < right)
		{
			st.push(r);
			st.push(right);
		}
	}
}


//归并排序
void _MergeSort(vector<int>& nums, int left, int right, vector<int>& tmp)
{
	if (left >= right) return;
	int mid = (left + right) >> 1;
	//[left,mid] [mid+1,right]
	_MergeSort(nums, left, mid, tmp);
	_MergeSort(nums, mid+1, right, tmp);
	int cur1 = left, cur2 = mid + 1;
	int i = left;
	while (cur1 <= mid && cur2 <= right)
	{
		tmp[i++] = nums[cur1] < nums[cur2] ? nums[cur1++] : nums[cur2++];
	}
	//把剩下的数据处理
	while (cur1 <= mid)
	{
		tmp[i++] = nums[cur1++];
	}
	while (cur2 <= right)
	{
		tmp[i++] = nums[cur2++];
	}
	//还原
	for (int j = left; j <= right; ++j)
	{
		nums[j] = tmp[j];
	}
}

void MergeSort(vector<int>& nums)
{
	vector<int> tmp(nums.size()); //辅助排序
	_MergeSort(nums, 0, nums.size() - 1, tmp);
}

void MergeSortNonR(vector<int>& nums)
{
	int n = nums.size();
	vector<int> tmp(n);
	int gap = n;
	while (gap > 1)
	{
		for (int i = 0; i < n; i += 2 * gap)
		{

		}
	}
}

int main()
{
	srand(time(nullptr));
	vector<int> nums = { 2,3,5,3,3,1,5,7,6,8,5,9,0,8 };
	//qsort(nums);
	MergeSort(nums);
	for (auto e : nums)
	{
		std::cout << e << " ";
	}
	return 0;
}