#include <iostream>
#include <vector>
using namespace std;


//class Solution {
//public:
//    bool evaluateTree(TreeNode* root) {
//        if (root == nullptr || root->val == 1) return true;
//        if (root->val == 0) return false;
//        //利用后序遍历的思想
//        bool left = evaluateTree(root->left);
//        bool right = evaluateTree(root->right);
//        if (root->val == 2) return left | right;
//        else return left & right;
//    }
//};


//class Solution {
//    int ret = 0;  //用来收集总的结果
//public:
//    void dfs(TreeNode* root, int val)
//    {
//        if (root == nullptr) return;
//        if (root->left == nullptr && root->right == nullptr)
//        {
//            ret += (val * 10 + root->val);
//            return;
//        }
//        dfs(root->left, val * 10 + root->val);
//        dfs(root->right, val * 10 + root->val);
//    }
//    int sumNumbers(TreeNode* root) {
//        dfs(root, 0);
//        return ret;
//    }
//};


//class Solution {
//public:
//    TreeNode* pruneTree(TreeNode* root) {
//        if (root == nullptr) return root;
//        root->left = pruneTree(root->left);
//        root->right = pruneTree(root->right);
//        if (root->left == nullptr && root->right == nullptr && root->val == 0)
//        {
//            delete root;
//            root = nullptr;
//        }
//        return root;
//    }
//};


//class Solution {
//    int ret;
//    int count = 0;
//public:
//    void dfs(TreeNode* root, int k)
//    {
//        if (root == nullptr) return;
//        dfs(root->left, k);
//        ++count;
//        if (count == k)
//        {
//            ret = root->val;
//            return;
//        }
//        dfs(root->right, k);
//    }
//    int kthSmallest(TreeNode* root, int k) {
//        dfs(root, k);
//        return ret;
//    }
//};



//class Solution {
//    vector<string> ret;   //保存结果
//public:
//    void dfs(TreeNode* root, string path)
//    {
//        path += to_string(root->val);
//        if (!root->left && !root->right)
//        {
//            ret.push_back(path);
//        }
//        path += "->";
//        if (root->left) dfs(root->left, path);
//        if (root->right) dfs(root->right, path);
//    }
//    vector<string> binaryTreePaths(TreeNode* root) {
//        dfs(root, "");
//        return ret;
//    }
//};


#include <iostream>
#include <queue>
#include <vector>
#include <string>
using namespace std;


//class Solution {
//public:
//    void Reverse(string& s, int left, int right)
//    {
//        while (left < right)
//        {
//            char tmp = s[left];
//            s[left] = s[right];
//            s[right] = tmp;
//            ++left;
//            --right;
//        }
//    }
//    string ReverseString(string originStr) {
//        int start = 0, i = 0;
//        for (; i < originStr.size(); ++i)
//        {
//            if (originStr[i] == ' ')
//            {
//                Reverse(originStr, start, i - 1);
//                start = i + 1;
//            }
//        }
//        //处理最后一个字符串
//        Reverse(originStr, start, i - 1);
//        return originStr;
//    }
//};



//class Solution {
//    class cmp
//    {
//    public:
//        bool operator()(const pair<int, int>& p1, const pair<int, int>& p2)
//        {
//            return p1.first > p2.first;
//        }
//    };
//public:
//    vector<int> solution(vector<int>& costs, int coins) {
//        //建立小堆
//        priority_queue<pair<int, int>, vector<pair<int, int>>, cmp> heap;  //元素和下标的映射关系
//        for (int i = 0; i < costs.size(); ++i)
//        {
//            heap.push(make_pair(costs[i], i));
//        }
//        vector<pair<int, int>> tmp;
//        while (coins > 0)
//        {
//            auto top = heap.top();
//            heap.pop();
//            if (top.first > coins)
//            {
//                break;
//            }
//            tmp.push_back(top);
//            coins -= top.first;
//        }
//        //将tmp以second进行排序
//        sort(tmp.begin(), tmp.end(), [](const pair<int, int>& p1, const pair<int, int>& p2)
//            {
//                return p1.second < p2.second;
//            });
//        vector<int> ret;
//        for (auto e : tmp)
//        {
//            ret.push_back(e.first);
//        }
//        return ret;
//    }
//};
//
//
//int main()
//{
//    vector<int> arr = { 2,1,3,4,5};
//    vector<int> ret = Solution().solution(arr, 10);
//    for (auto e : ret) cout << e << " ";
//    return 0;
//}


//class Solution {
//public:
//    string LargestNum(vector<int>& nums) {
//        //通过对nums进行特定的排序，然后再把排序后的结果按顺序放入到s中
//        vector<string> tmp;
//        for (auto e : nums)
//        {
//            string s = to_string(e);
//            tmp.push_back(s);
//        }
//        sort(tmp.begin(), tmp.end(), [&](const string& x, const string& y) {
//            int xsz = x.size(), ysz = y.size(), j = 0;
//            for (int i = 0; i < xsz && i < ysz; ++i,++j)
//            {
//                if (x[i] > y[i]) return true;
//                if (x[i] < y[i]) return false;
//            }
//            //这里可能前面都相等，返回后面的值更大那个
//            if (xsz > ysz)
//            {
//                int k = 0;
//                while (j < xsz)
//                {
//                    if (x[j] > x[k]) return true;
//                    if (x[j] < x[k]) return false;
//                    ++j, ++k;
//                }
//            }
//            else if (xsz == ysz) return true;
//            else
//            {
//                int k = 0;
//                while (j < ysz)
//                {
//                    if (y[j] > y[k]) return true;
//                    if (y[j] < y[k]) return false;
//                    ++j, ++k;
//                }
//            }
//            });
//        string ret;
//        for (auto str : tmp)
//        {
//            ret += str;
//        }
//        return ret;
//    }
//};
//
//
//int main()
//{
//    vector<int> arr = { 110,1100 };
//    cout << Solution().LargestNum(arr);
//    return 0;
//}

