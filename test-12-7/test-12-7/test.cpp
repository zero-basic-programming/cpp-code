#include <iostream>
#include <vector>
using namespace std;


//class Solution {
//    //通过hash的方式快速找到当前位置是否符合条件
//    bool row[9][10];
//    bool col[9][10];
//    bool lattic[3][3][10];
//public:
//    void solveSudoku(vector<vector<char>>& board) {
//        //初始化
//        for (int i = 0; i < 9; ++i)
//        {
//            for (int j = 0; j < 9; ++j)
//            {
//                if (board[i][j] != '.')
//                {
//                    int num = board[i][j] - '0';
//                    row[i][num] = true;
//                    col[j][num] = true;
//                    lattic[i / 3][j / 3][num] = true;
//                }
//            }
//        }
//        dfs(board);
//    }
//    bool dfs(vector<vector<char>>& board)
//    {
//        for (int i = 0; i < 9; ++i)  //当前行试验列
//        {
//            for (int j = 0; j < 9; ++j)  //每一个格子试验
//            {
//                if (board[i][j] == '.')
//                {
//                    //每一个格子从1到9中挑选
//                    for (int num = 1; num < 10; ++num)
//                    {
//                        if (row[i][num] || col[j][num] || lattic[i / 3][j / 3][num]) continue;
//                        board[i][j] = '0' + num;
//                        row[i][num] = col[j][num] = lattic[i / 3][j / 3][num] = true;
//                        if (dfs(board)) return true;
//                        //回溯
//                        board[i][j] = '.';
//                        row[i][num] = col[j][num] = lattic[i / 3][j / 3][num] = false;
//                    }
//                    return false;
//                }
//            }
//        }
//        return true;
//    }
//};


//class A
//{
//public:
//	virtual int GetVal()
//	{
//		return a;
//	}
//private:
//	int a = 1;
//};
//
//class B : public A
//{
//public:
//	virtual int GetVal()
//	{
//		return b;
//	}
//private:
//	int b = 2;
//};
//
//int main()
//{
//	//实现多态
//	A* a = new A();
//	A* b = new B();
//	cout << a->GetVal() << endl;
//	cout << b->GetVal() << endl;
//	return 0;
//}


