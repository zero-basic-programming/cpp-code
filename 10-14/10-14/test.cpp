#include <iostream>
#include <vector>
using namespace std;

//class Solution {
//public:
//    int minPathSum(vector<vector<int>>& grid) {
//        int m = grid.size(), n = grid[0].size();
//        vector<vector<int>> dp(m + 1, vector<int>(n + 1, INT_MAX));
//        //初始化
//        dp[0][1] = dp[1][0] = 0;
//        for (int i = 1; i <= m; ++i)
//        {
//            for (int j = 1; j <= n; ++j)
//            {
//                dp[i][j] = min(dp[i][j - 1], dp[i-1][j]) + grid[i - 1][j - 1];
//            }
//        }
//        return dp[m][n];
//    }
//};
//
//
//int main()
//{
//    vector<vector<int>> nums = { {1,3,1},{1,5,1},{4,2,1} };
//    int ret = Solution().minPathSum(nums);
//    cout << ret;
//    return 0;
//}


//class Solution {
//    int m;   //用来记录层数
//    int ret = INT_MAX;  //收集结果
//public:
//    void dfs(vector<vector<int>>& triangle, int level, int idx, int sum)
//    {
//        if (level == m)
//        {
//            //收集结果返回
//            ret = min(sum, ret);
//            return;
//        }
//        dfs(triangle, level + 1, idx, sum + triangle[level][idx]);
//        dfs(triangle, level + 1, idx + 1, sum + triangle[level][idx + 1]);
//    }
//    int minimumTotal(vector<vector<int>>& triangle) {
//        m = triangle.size();
//        dfs(triangle, 1, 0, triangle[0][0]);
//        return ret;
//    }
//};


//class Solution {
//public:
//    int minimumTotal(vector<vector<int>>& triangle) {
//        int m = triangle.size();
//        if (m == 1) return triangle[0][0];
//        for (int i = 1; i < m; ++i)
//        {
//            for (int j = 0; j < triangle[i].size(); ++j)
//            {
//                if (j == 0)
//                {
//                    //这里只有一种出来方式
//                    triangle[i][j] += triangle[i - 1][j];
//                }
//                else if (j == triangle[i].size() - 1)
//                {
//                    triangle[i][j] += triangle[i - 1][j - 1];
//                }
//                else
//                {
//                    triangle[i][j] += min(triangle[i - 1][j], triangle[i - 1][j - 1]);
//                }
//            }
//        }
//        //在最后一行中找到最小
//        int ret = INT_MAX;
//        for (int j = 0; j < triangle[m - 1].size(); ++j)
//        {
//            ret = min(ret, triangle[m - 1][j]);
//        }
//        return ret;
//    }
//};


//class Solution {
//public:
//    bool canPartition(vector<int>& nums) {
//        int n = nums.size();
//        int sum = 0;
//        for (auto e : nums) sum += e;
//        if (sum % 2 == 1) return false;  //如果是奇数的情况一定不能分割
//        int aim = sum / 2;
//        //转换成在数组中找到和为aim
//        vector<bool> dp(aim + 1, false);
//        dp[0] = true;   //没有元素的时候是可以分割成0的和
//        for (int i = 1; i <= n; ++i)
//        {
//            for (int j = aim; j >= nums[i - 1]; --j)
//            {
//                dp[j] = dp[j] || dp[j - nums[i - 1]];
//            }
//        }
//        return dp[aim];
//    }
//};

//class Solution {
//public:
//    int findTargetSumWays(vector<int>& nums, int target) {
//        int sum = 0;
//        for (auto e : nums) sum += e;
//        int aim = (target + sum) / 2;
//        if ((target + sum) % 2 || aim < 0) return 0;
//        int n = nums.size();
//        vector<vector<int>> dp(n + 1, vector<int>(aim + 1));
//        dp[0][0] = 1;
//        for (int i = 1; i <= n; ++i)
//        {
//            for (int j = 0; j <= aim; ++j)
//            {
//                dp[i][j] = dp[i - 1][j];
//                if (j >= nums[i - 1]) dp[i][j] += dp[i - 1][j - nums[i - 1]];
//            }
//        }
//        return dp[n][aim];
//    }
//};


//class Solution {
//public:
//    int findTargetSumWays(vector<int>& nums, int target) {
//        int sum = 0;
//        for (auto e : nums) sum += e;
//        int aim = (target + sum) / 2;
//        if ((target + sum) % 2 || aim < 0) return 0;
//        int n = nums.size();
//        vector<int> dp(aim + 1);
//        dp[0] = 1;
//        for (int i = 1; i <= n; ++i)
//        {
//            for (int j = aim; j >= nums[i - 1]; --j)
//            {
//                dp[j] += dp[j - nums[i - 1]];
//            }
//        }
//        return dp[aim];
//    }
//};


//class Solution {
//    int ret = 0;
//    int m;
//public:
//    void dfs(vector<int>& nums, int target, int i, int sum)
//    {
//        if (i == m)
//        {
//            if (sum == target)
//                ++ret;
//            return;
//        }
//        dfs(nums, target, i + 1, sum + nums[i]);
//        dfs(nums, target, i + 1, sum - nums[i]);
//    }
//    int findTargetSumWays(vector<int>& nums, int target) {
//        //每个元素可以选择正负两种方式，这样可以利用2叉树的方式去列举出全部的结果
//        m = nums.size();
//        dfs(nums, target, 0, 0);
//        return ret;
//    }
//};



