#include <iostream>
#include <vector>
using namespace std;


//class Solution{
//public:
//    int Binary(vector<int>&arr,int num)
//    {
//        //找到num>arr中的最后一个值的下标
//        int left = 0;
//        int right = arr.size() - 1;
//        while (left < right)
//        {
//            int mid = (left + right) >> 1;
//            if (arr[mid] < num)
//            {
//                left = mid + 1; //这些都是淘汰的
//            }
//            else {
//                right = mid;
//            }
//        }
//        return left;
//    }
//    int lengthOfLIS(vector<int>&nums) {
//        int n = nums.size();
//        vector<int> tmp(n,INT_MAX);
//        int ret = 0;
//        for (auto e : nums)
//        {
//            //通过二分算法来找出其应该在的区间
//            int index = Binary(tmp,e);
//            tmp[index] = e; //直接替换，贪心
//            ret = max(ret,index);
//            cout << index << endl;
//        }
//        return ret + 1;
//    }
//};


////快排 -> 快速选择算法
//void Qsort(vector<int>& arr, int l, int r)
//{
//	if (l >= r) return;
//	//选取一个基准元素，采用随机值的方式来保证效率
//	int key = arr[(rand() % (r - l + 1) + l)];
//	int i = l, left = l - 1, right = r + 1;
//	while (i < right)
//	{
//		if (key > arr[i]) swap(arr[i++], arr[++left]);
//		else if (key == arr[i]) ++i;
//		else swap(arr[i], arr[--right]);
//	}
//	//[l,left] [left+1,right-1] [right,r]
//	//递归排序左右
//	Qsort(arr,l, left);
//	Qsort(arr, right, r);
//}
//
//int main()
//{
//	srand(time(nullptr));
//	vector<int> arr = { 1,5,3,6,7,3,5,4,2,9,8,9,0 };
//	Qsort(arr, 0, arr.size() - 1);
//	for (auto e : arr)
//	{
//		cout << e << " ";
//	}
//	return 0;
//}


//class Solution {
//public:
//    bool increasingTriplet(vector<int>& nums) {
//        int first = INT_MAX, second = INT_MAX;
//        for (auto e : nums)
//        {
//            if (e > second) return true; //这里必定有3个满足题意了
//            else if (e > first) second = min(second, e);
//            else first = min(first, e);
//        }
//        return false;
//    }
//};

void Mergesort(vector<int>& arr, int l, int r, vector<int>& tmp)
{
	if (l >= r) return;
	int mid = (l + r) >> 1;
	//[l,mid][mid+1,r]
	//左右归并排序
	Mergesort(arr, l, mid, tmp);
	Mergesort(arr, mid+1, r, tmp);
	//归并
	int i = l, begin1 = l, end1 = mid, begin2 = mid + 1, end2 = r;
	while (begin1 < end1 && begin2 < end2)
	{
		tmp[i++] = arr[begin1] <= arr[begin2] ? arr[begin1++] : arr[begin2++];
	}
	//处理最后两个没有归并的其中一个数组
	while (begin1 <= end1)
	{
		tmp[i++] = arr[begin1++];
	}
	while (begin2 <= end2)
	{
		tmp[i++] = arr[begin2++];
	}
	//还原数组
	for (int j = l; j <= r; ++j)
		arr[j] = tmp[j];
}

int main()
{
	srand(time(nullptr));
	vector<int> arr = { 1,5,3,6,7,3,5,4,2,9,8,9,0 };
	vector<int> tmp(arr.size());
	Mergesort(arr, 0, arr.size() - 1,tmp);
	for (auto e : arr)
	{
		cout << e << " ";
	}
	return 0;
}