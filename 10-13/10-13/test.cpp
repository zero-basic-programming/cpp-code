#include <iostream>
#include <vector>
using namespace std;


//class Solution {
//    int m, n;
//    vector<vector<bool>> vis;
//    int ret = 0;
//    int dx[4] = { 0,0,1,-1 };
//    int dy[4] = { 1,-1,0,0 };
//public:
//    bool dfs(vector<vector<int>>& data, int i, int j)
//    {
//        vis[i][j] = true;
//        for (int k = 0; k < 4; ++k)
//        {
//            int x = i + dx[k], y = j + dy[k];
//            if (x >= 0 && x < m && y >= 0 && y < n && data[x][y] == 2) return false;
//            if (x >= 0 && x < m && y >= 0 && y < n && !vis[x][y] && data[x][y] == 1)
//            {
//                if (dfs(data, x, y) == false) return false;
//            }
//        }
//        return true;
//    }
//    int CountEmptyIsland(vector<vector<int>>& data) {
//        m = data.size();
//        n = data[0].size();
//        vis.resize(m, vector<bool>(n, false));
//        for (int i = 0; i < m; ++i)
//        {
//            for (int j = 0; j < n; ++j)
//            {
//                if (data[i][j] == 1 && vis[i][j] == false)
//                {
//                    if (dfs(data, i, j)) ++ret;
//                }
//            }
//        }
//        return ret;
//    }
//};
//
//
//int main()
//{
//    vector<vector<int>> nums = { {0,1,1,0},{2,1,1,0},{1,0,0,1},{0,1,1,1} };
//    Solution().CountEmptyIsland(nums);
//    return 0;
//}


//#include <iostream>
//#include <string>
//using namespace std;
//
//int main() {
//    string str;
//    cin >> str;
//    //最少需要字符串长12
//    //字符串第5-8位是后面的长度需要拿到
//    //主体内容就是长度*2，就是后面的字符串对应的内容
//    //如果后面的内容不符合计算的就忽略
//    int n = str.size();
//    int i = 0;
//    while (n - i >= 12)
//    {
//        i += 4;   //前4位不需要输出
//        //拿到长度
//        string lenStr = str.substr(i, 2);
//        string tmpStr = str.substr(i + 2, 2);
//        lenStr = tmpStr + lenStr;
//        int len = stoi(lenStr, nullptr, 16);
//        len -= 2;  //减去协议号长度，这里就是正文长度
//        //拿到协议号
//        i += 4;
//        string VersionStr = str.substr(i, 2);
//        tmpStr = str.substr(i + 2, 2);
//        VersionStr = tmpStr + VersionStr;
//        int version = stoi(VersionStr, nullptr, 16);
//        i += 4;
//        //得到正文，但这里可能是部分正文，需要舍弃
//        string tmp = to_string(version) + ' ';
//        if (n - i < 2 * len) break;  //舍弃
//        while (len--)
//        {
//            //每次拿一个字符，16->10 10->str
//            string ch = str.substr(i, 2);
//            int TmpInt = stoi(ch, nullptr, 16);
//            tmp += (TmpInt - 'a' + 'a');
//            i += 2;
//        }
//        cout << tmp << endl;
//    }
//    return 0;
//}


//class Solution {
//public:
//    int Fibonacci(int n) {
//        if (n == 1 || n == 2) return 1;
//        int first = 1;
//        int second = 1;
//        int third = 2;
//        for (int i = 3; i <= n; ++i)
//        {
//            third = first + second;
//            //迭代
//            first = second;
//            second = third;
//        }
//        return third;
//    }
//};


//class Solution {
//    int m, n;
//    vector<vector<bool>> vis;
//    int ret = 0;
//    int dx[4] = { 0,0,1,-1 };
//    int dy[4] = { 1,-1,0,0 };
//public:
//    void dfs(vector<vector<int>>& data, int i, int j)
//    {
//        vis[i][j] = true;
//        for (int k = 0; k < 4; ++k)
//        {
//            int x = i + dx[k], y = j + dy[k];
//            if (x >= 0 && x < m && y >= 0 && y < n && !vis[x][y] && data[x][y] == 1)
//            {
//                dfs(data, x, y);
//            }
//        }
//    }
//    void dfs1(vector<vector<int>>& data, int i, int j)
//    {
//        vis[i][j] = true;
//        for (int k = 0; k < 4; ++k)
//        {
//            int x = i + dx[k], y = j + dy[k];
//            if (x >= 0 && x < m && y >= 0 && y < n && !vis[x][y] && data[x][y] == 1)
//            {
//                data[x][y] = 2;
//                dfs1(data, x, y);
//                vis[x][y] = false;
//            }
//        }
//    }
//    int CountEmptyIsland(vector<vector<int>>& data) {
//        m = data.size();
//        n = data[0].size();
//        vis.resize(m, vector<bool>(n, false));
//        for (int i = 0; i < m; ++i)
//        {
//            for (int j = 0; j < n; ++j)
//            {
//                if (data[i][j] == 2)
//                {
//                    dfs1(data, i, j);
//                }
//            }
//        }
//        for (int i = 0; i < m; ++i)
//        {
//            for (int j = 0; j < n; ++j)
//            {
//                if (data[i][j] == 1 && vis[i][j] == false)
//                {
//                    dfs(data, i, j);
//                    ++ret;
//                }
//            }
//        }
//        return ret;
//    }
//};


