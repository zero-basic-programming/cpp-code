#include <iostream>
#include <vector>
using namespace std;


//class Solution
//{
//public:
//	bool FindOne(unsigned int x)
//	{
//		int key = -1;  //记录上一个出现1的向右移动的位数
//		for (int i = 0; i < 32; ++i)
//		{
//			if (x >> i == 1)
//			{
//				if (key + 1 == i)
//				{
//					//这里覆盖上一次的结果
//					key = i;
//				}
//				else return true;  //满足条件
//			}
//		}
//		return false;
//	}
//};


//class BitMap
//{
//public:
//	BitMap()
//	{
//		bitset.resize(4, 0);  //提前开辟部分空间
//	}
//	//将这个数字设置进位图
//	void set(int x)
//	{
//		int i = x / 64;  //找到第几个数存储的下标
//		if (i > bitset.size())
//		{
//			//扩容
//			bitset.resize(i + 1, 0);
//		}
//		//设置位图中
//		int j = x % 64;
//		bitset[i] |= (1<<j);
//	}
//	//将数字移除位图
//	void unset(int x)
//	{
//		int i = x / 64;
//		int j = x % 64;
//		if (i > bitset.size())
//		{
//			//扩容
//			bitset.resize(i + 1, 0);
//			return;  //这里扩容完之后是0
//		}
//		bitset[i] &= ~(1 << j);
//	}
//	//查看当前数字是否在位图中
//	bool test(int x)
//	{
//		int i = x / 64;  //找到第几个数存储的下标
//		if (i > bitset.size())
//		{
//			//扩容
//			bitset.resize(i + 1, 0);
//			return false;
//		}
//		//查看是否在位图中
//		int j = x % 64;
//		long long tmp = bitset[i];
//		if (tmp & (1 << j)) return true;
//		return false;
//	}
//private:
//	vector<long long> bitset;
//};


//int main()
//{
//	//cout << Solution().FindOne(5);
//	BitMap bit;
//	bit.set(5);
//	bit.set(15);
//	bit.set(55);
//	cout << bit.test(5);
//	cout << bit.test(55);
//	bit.unset(55);
//	cout << bit.test(55);
//	return 0;
//}



//class PriorityQueue
//{
//public:
//	//插入数据
//	void push(int x)
//	{
//		heap.push_back(x);
//		//向上调整
//		AdjustUp(heap.size()-1);
//	}
//	//弹出堆顶数据
//	void pop()
//	{
//		//交换首尾，清除最后一个元素，然后向下调整
//		swap(heap[0], heap.back());
//		heap.pop_back();
//		AdjustDown(0);
//	}
//	//获得到堆顶数据
//	int top()
//	{
//		return heap[0];
//	}
//	int sz()
//	{
//		return heap.size();
//	}
//private:
//	//向下调整算法
//	template<class CMP = less<int>>
//	void AdjustDown(int parent)
//	{
//		CMP cmp;
//		int n = heap.size();
//		int child = 2 * parent + 1;
//		while (child < n)
//		{
//			if (child + 1 < n && cmp(heap[child], heap[child + 1])) ++child;
//			if (cmp(heap[parent],heap[child]))
//			{
//				swap(heap[child], heap[parent]);
//				parent = child;
//				child = 2 * parent + 1;
//			}
//			else break;   //已成堆
//		}
//	}
//	template<class CMP = less<int>>
//	void AdjustUp(int child)
//	{
//		CMP cmp;
//		int parent = (child - 1) / 2;
//		while (child > 0)
//		{
//			if (cmp(heap[parent], heap[child]))
//			{
//				swap(heap[parent], heap[child]);
//				//向上迭代
//				child = parent;
//				parent = (child - 1) / 2;
//			}
//			else break;
//		}
//	}
//	vector<int> heap;
//};
//
//int main()
//{
//	PriorityQueue pq;
//	pq.push(2);
//	pq.push(5);
//	pq.push(8);
//	pq.push(1);
//	while (pq.sz())
//	{
//		cout << pq.top() << endl;
//		pq.pop();
//	}
//	return 0;
//}


//class Solution {
//public:
//    void qsort(vector<int>& stock, int l, int r, int k)
//    {
//        if (l >= r) return;
//        int key = stock[rand() % (r - l + 1) + l];
//        int left = l - 1, right = r + 1, i = l;
//        while (i < right)
//        {
//            if (stock[i] < key) swap(stock[i++], stock[++left]);
//            else if (stock[i] == key) ++i;
//            else swap(stock[i], stock[--right]);
//        }
//        //[l,left] [left+1,right-1] [right,r]
//        int a = left - l + 1, b = right - left - 1;
//        if (a > k) qsort(stock, l, left, k);
//        else if (a + b >= k) return;
//        else qsort(stock, right, r, k - a - b);
//    }
//    vector<int> inventoryManagement(vector<int>& stock, int cnt) {
//        qsort(stock, 0, stock.size() - 1, cnt);
//        return vector<int>(stock.begin(), stock.begin() + cnt);
//    }
//};


//class MinStack {
//public:
//    void push(int x) {
//        //如果插入当前的数据比最小栈栈顶数据要小，那么最小栈也需要插入
//        if (_nums.empty())
//        {
//            _nums.push(x);
//            _min.push(x);
//            return;
//        }
//        if (x <= _min.top())
//        {
//            _min.push(x);
//        }
//        _nums.push(x);
//    }
//
//    void pop() {
//        //如果数据栈中的最小值和最小栈中的栈顶值相等，那么就都pop
//        if (_min.top() == _nums.top())
//        {
//            _min.pop();
//        }
//        _nums.pop();
//    }
//
//    int top() {
//        return _nums.top();
//    }
//
//    int getMin() {
//        return _min.top();
//    }
//private:
//    stack<int> _nums;  //数据栈
//    stack<int> _min;   //最小栈
//};


