#include <iostream>
#include <vector>
using namespace std;


namespace liang
{
	template <class T>
	class unique_ptr
	{
	public:
		unique_ptr(T* ptr):_ptr(ptr){}
		~unique_ptr()
		{
			if (_ptr)
			{
				delete _ptr;
				cout << "delete ptr" << endl;
				_ptr = nullptr;
			}
		}
		//赋值运算符重载
		T& operator*()
		{
			return *_ptr;
		}
		T* operator->()
		{
			return _ptr;
		}
		//需要把拷贝构造和赋值禁用
		unique_ptr(unique_ptr<T>& ptr) = delete;
		unique_ptr& operator=(unique_ptr<T>& ptr) = delete;
	private:
		T* _ptr;   //管理资源对应的指针
	};
	//shared_ptr
	template <class T>
	class share_ptr
	{
	public:
		share_ptr(T& ptr) :_ptr(ptr), _count(new int(1)) {};
		~share_ptr()
		{
			if (--(*_count) == 0)
			{
				//这里需要释放对应的资源
				if (_ptr)
				{
					delete _ptr;
					_ptr = nullptr;
				}
				//释放count内存
				delete _count;
				_count = nullptr;
			}
		}
		//拷贝构造以及赋值
		share_ptr(share_ptr<T>& sp)
			:_ptr(sp._ptr),_count(sp._count)
		{
			++(*_count);
		}
		share_ptr& operator=(share_ptr<T>& sp)
		{
			//判断这两个ptr是不是同一个，同一个没有必要赋值
			if (sp._ptr != _ptr)
			{
				if (--(*_count) == 0)
				{
					delete _ptr;
					delete _count;
				}
				//将新的赋值给当前的对象
				_ptr = sp._ptr;
				_count = sp._count;
			}
		}

	private:
		T* _ptr;   //管理的资源
		int* _count;  //对应的指针数量
	};
}



