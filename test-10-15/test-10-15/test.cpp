#include <iostream>
#include <vector>
#include <string>
using namespace std;

//class Solution {
//public:
//    string longestPalindrome(string s) {
//        int n = s.size();
//        int ret = 0; //长度
//        int index = 0; //长度最长的字串的起始位置
//        for (int i = 0; i < n; ++i) //定位一个元素来查找以他为中心的回文字符串
//        {
//            int left = i, right = i;//奇数情况
//            while (left >= 0 && right < n && s[left] == s[right])
//            {
//                ++right; --left;
//            }
//            if (right - left - 1 > ret)
//            {
//                ret = right - left - 1;
//                index = left + 1;
//            }
//            left = i; right = i + 1; //偶数情况
//            while (left >= 0 && right < n && s[left] == s[right])
//            {
//                ++right; --left;
//            }
//            if (right - left - 1 > ret)
//            {
//                ret = right - left - 1;
//                index = left + 1;
//            }
//        }
//        return s.substr(index, ret);
//    }
//};


class Solution {
public:
    int countSubstrings(string s) {
        int n = s.size();
        vector<vector<bool>> dp(n, vector<bool>(n));
        int ret = 0;
        for (int i = n - 1; i >= 0; --i)  //从下往上
        {
            for (int j = i; j < n; ++j)
            {
                if (s[i] == s[j])
                {
                    if (i == j) dp[i][j] = true;
                    else if (i + 1 == j) dp[i][j] = true;
                    else dp[i][j] = dp[i + 1][j - 1];
                }
                if (dp[i][j] == true) ++ret;
            }
        }
        return ret;
    }
};