#include <iostream>
#include <vector>
#include <stack>
using namespace std;

//归并排序
void MergeSort(vector<int>& arr,int left,int right,vector<int>& tmp)
{
	if (left >= right) return;
	//取中
	int mid = (left + right) >> 1;
	//[left,mid] [mid+1,right]
	//归并左右
	MergeSort(arr, left, mid, tmp);
	MergeSort(arr, mid+1, right,tmp);
	//排序
	int begin1 = left, begin2 = mid + 1, i = left;
	while (begin1 <= mid && begin2 <= right)
	{
		//升序
		tmp[i++] = arr[begin1] < arr[begin2] ? arr[begin1++] : arr[begin2++];
	}
	//处理剩下没有处理完的数据
	while (begin1 <= mid)
	{
		tmp[i++] = arr[begin1++];
	}
	while (begin2 <= right)
	{
		tmp[i++] = arr[begin2++];
	}
	//还原回原来的数组
	for (int j = left; j <= right; ++j)
		arr[j] = tmp[j];
}

//快排 -- 快速选择算法
void Qsort(vector<int>& arr, int l, int r)
{
	if (l >= r) return;
	//选取一个基准点
	int key = arr[rand() % (r - l + 1) + l];
	int left = l - 1, right = r + 1, i = l;
	while (i < right)
	{
		if (arr[i] < key) swap(arr[i++], arr[++left]);
		else if (arr[i] == key) ++i;
		else swap(arr[i], arr[--right]);
	}
	//[l,left] [left+1,right-1] [right,r]
	Qsort(arr, l, left);
	Qsort(arr, right, r);
}

void AdjustDown(vector<int>& arr, int parent, int n)
{
	int child = 2 * parent + 1;
	while (child < n)
	{
		//比较左右孩子，哪一个大
		if (child + 1 < n && arr[child] < arr[child + 1])
		{
			++child;
		}
		if (arr[parent] < arr[child])
		{
			//交换
			swap(arr[parent], arr[child]);
			//迭代
			parent = child;
			child = 2 * parent + 1;
		}
		else 
		{
			//已成堆
			break;
		}
	}
}

//堆排 -- 升序排大堆
void HeapSort(vector<int>& arr)
{
	//通过向下调整算法快速建堆 ,建大堆
	int n = arr.size();
	for (int i = (n - 1 - 1) / 2; i >= 0; --i)
	{
		AdjustDown(arr, i,n);
	}
	//堆排
	for (int i = 1; i < n; ++i)
	{
		swap(arr[0], arr[n - i]);
		AdjustDown(arr, 0, n - i);
	}
}

//冒泡排序
void BufferSort(vector<int>& arr)
{
	int flag = 0; //判断数组是否已经有序，有序就不需要再排了
	int n = arr.size();
	for (int i = 0; i < n; ++i)
	{
		for (int j = 0; j < n - i - 1; ++j) //趟数
		{
			if (arr[j] > arr[j + 1])
			{
				swap(arr[j], arr[j + 1]);
				flag = 1;
			}
		}
		if (flag == 0) break; //说明已经是有序的
	}
}

//选择排序 
void SelectSort(vector<int>& arr)
{
	//一次选一个最大和最小
	int n = arr.size();
	int left = 0, right = n - 1;
	while (left < right)
	{
		int maxi = left, mini = left;
		for (int i = left; i <= right; ++i)
		{
			if (arr[i] > arr[maxi]) maxi = i;
			if (arr[i] < arr[mini]) mini = i;
		}
		//更新最大值
		swap(arr[maxi], arr[right]);
		//这里要防止更新最大值的时候把最小值交换了，即最小值在right的位置
		if (mini == right) mini = maxi;
		swap(arr[mini], arr[left]);
		++left;
		--right;
	}
}

//直接插入排序
void InsertSort(vector<int>& arr)
{
	int n = arr.size();
	for (int i = 0; i < n - 1; ++i)
	{
		int end = i; //最后一个元素
		int tmp = arr[end + 1]; //每次排这个元素的正确位置
		while (end >= 0)
		{
			if (arr[end] > tmp)
			{
				arr[end+1] = arr[end];
			}
			else break;
			--end;
		}
		arr[end + 1] = tmp;
	}
}

//希尔排序
void ShellSort(vector<int>& arr)
{
	int n = arr.size();
	int gap = n; //分成gap组
	while (gap > 1)
	{
		gap /= 2;
		for (int i = 0; i < n - gap; ++i)
		{
			int end = i;
			int tmp = arr[end + gap];
			while (end >= 0)
			{
				if (arr[end] > tmp)
				{
					arr[end + gap] = arr[end];
				}
				else break;
				end -= gap;
			}
			arr[end + gap] = tmp; //填写当次应该处理的数据
		}
	}
}

//快排非递归 -- 快速选择排序
void QsortNonR(vector<int>& arr, int lv, int rv)
{
	stack<int> st;
	st.push(lv);
	st.push(rv);
	while (!st.empty())
	{
		//选择基准元素
		int r = st.top();
		st.pop();
		int l = st.top();
		st.pop();
		int key = arr[(rand() % (r - l + 1) + l)];
		int left = l - 1, right = r + 1, i = l;
		while (i < right)
		{
			if (arr[i] < key) swap(arr[i++], arr[++left]);
			else if (arr[i] == key) ++i;
			else swap(arr[i], arr[--right]);
		}
		//[l,left] [left+1,right-1] [right,r]
		if (l < left) {
			st.push(l);
			st.push(left);
		}
		if (right < r)
		{
			st.push(right);
			st.push(r);
		}
	}
}

void _MergeSortNonR(vector<int>& arr, vector<int>& tmp, int begin1, int end1, int begin2, int end2)
{
	int i = begin1;
	int j = begin1; //这里要提前定义，不然后面会改变这个值
	while (begin1 <= end1 && begin2 <= end2)
	{
		tmp[i++] = arr[begin1] < arr[begin2] ? arr[begin1++] : arr[begin2++];
	}
	//处理后序数据
	while (begin1 <= end1)
	{
		tmp[i++] = arr[begin1++];
	}
	while (begin2 <= end2)
	{
		tmp[i++] = arr[begin2++];
	}
	//还原回原数组
	for (; j <= end2; ++j)
	{
		arr[j] = tmp[j];
	}
}

//归并排序 -- 非递归
void MergeSortNonR(vector<int>& arr,vector<int>& tmp)
{
	int gap = 1; //每次归并的个数
	int n = arr.size();
	while (gap < n)
	{
		for (int i = 0; i < n; i += 2 * gap)
		{
			int begin1 = i, end1 = i + gap - 1;
			int begin2 = i + gap, end2 = i + 2 * gap - 1;
			//判断边界条件
			if (begin2 >= n) break; //说明最后一组没有数据，就前面那组的话是已经有序的
			if (end2 >= n) end2 = n - 1; //最后一组有数据，处理边界情况
			//开始进行归并
			_MergeSortNonR(arr, tmp, begin1, end1, begin2, end2);
		}
		//更新gap
		gap *= 2;
	}
}

//计数排序 -- 适合元素相对集中
void CountSort(vector<int>& arr)
{
	int n = arr.size();
	int maxval = INT_MIN;
	int minval = INT_MAX;
	for (auto e : arr)
	{
		maxval = max(maxval, e);
		minval = min(minval, e);
	}
	int sz = (maxval - minval + 1);
	vector<int> countarr(sz);
	//计数
	for (auto e : arr)
	{
		countarr[e - minval]++;
	}
	//放回到原数组
	int j = 0;
	for (int i = 0; i < sz; ++i)
	{
		while (countarr[i]--)
		{
			arr[j++] = i + minval;
		}
	}
}

int main()
{
	srand(time(nullptr));
	vector<int> arr = { 1,4,6,3,5,2,3,7,8,7,8,9,9,0 };
	int n = arr.size();
	vector<int> tmp(n);
	//Qsort(arr, 0, n - 1);
	//HeapSort(arr);
	/*MergeSort(arr,0,n-1,tmp);*/
	//SelectSort(arr);
	//BufferSort(arr);
	//InsertSort(arr);
	//ShellSort(arr);
	//QsortNonR(arr, 0, n - 1);
	//MergeSortNonR(arr, tmp);
	CountSort(arr);
	for (auto e : arr)
	{
		cout << e << " ";
	}
	return 0;
}




