#include <iostream>
#include <vector>
using namespace std;


//class Solution {
//public:
//    int calculateMinimumHP(vector<vector<int>>& dungeon) {
//        int m = dungeon.size(), n = dungeon[0].size();
//        vector<vector<int>> dp(m + 1, vector<int>(n + 1, INT_MAX));
//        //初始化
//        dp[m][n - 1] = dp[m - 1][n] = 1;
//        for (int i = m - 1; i >= 0; --i)
//        {
//            for (int j = n - 1; j >= 0; --j)
//            {
//                dp[i][j] = min(dp[i + 1][j], dp[i][j + 1]) - dungeon[i][j];
//                //这里如果生命值小于0显示是不行的
//                if (dp[i][j] <= 0) dp[i][j] = 1;
//            }
//        }
//        return dp[0][0];
//    }
//};


//class Solution {
//public:
//    int massage(vector<int>& nums) {
//        int n = nums.size();
//        if (n == 0) return 0;
//        vector<int> f(n);  //表示当前选择该位置的结果
//        auto g = f;   //表示当前不选择该位置
//        //初始化
//        f[0] = nums[0];
//        for (int i = 1; i < n; ++i)
//        {
//            f[i] = g[i - 1] + nums[i];
//            g[i] = max(f[i - 1], g[i - 1]);
//        }
//        return max(f[n - 1], g[n - 1]);
//    }
//};


//class Solution {
//public:
//    int rob(vector<int>& nums) {
//        int n = nums.size();
//        if (n == 1) return nums[0];
//        //分成两种情况
//        //1.不选第一个，从下标1到n-1中选择
//        //2.不选最后一个，从下标0到n-2中选择
//        return max(rob1(nums, 1, n - 1), rob1(nums, 0, n - 2));
//    }
//    int rob1(vector<int>& nums, int left, int right)
//    {
//        int n = nums.size();
//        vector<int> f(n);
//        auto g = f;
//        //初始化
//        f[left] = nums[left];
//        for (int i = left + 1; i <= right; ++i)
//        {
//            f[i] = g[i - 1] + nums[i];
//            g[i] = max(f[i - 1], g[i - 1]);
//        }
//        return max(f[right], g[right]);
//    }
//};


//class Solution {
//public:
//    int deleteAndEarn(vector<int>& nums) {
//        const int N = 10001;
//        // 1. 预处理
//        int arr[N] = { 0 };
//        for (auto x : nums)
//            arr[x] += x;
//        // 2. 在 arr 数组上，做?次 “打家劫舍” 问题
//        // 创建 dp 表
//        vector<int> f(N);
//        auto g = f;
//        // 填表
//        for (int i = 1; i < N; i++) {
//            f[i] = g[i - 1] + arr[i];
//            g[i] = max(f[i - 1], g[i - 1]);
//        }
//        // 返回结果
//        return max(f[N - 1], g[N - 1]);
//    }
//};


//class Solution {
//public:
//    int totalFruit(vector<int>& fruits) {
//        unordered_map<int, int> hash;  //用来记录水果的类型和个数的关系
//        int n = fruits.size();
//        int left = 0, right = 0;
//        int ret = 0;  //统计结果
//        int count = 0;  //记录水果的种类
//        while (right < n)
//        {
//            //进窗口
//            if (hash.count(fruits[right]) == 0)
//                ++count;
//            hash[fruits[right]]++;
//            while (count > 2)
//            {
//                //出窗口
//                if (--hash[fruits[left]] == 0)
//                {
//                    --count;
//                    hash.erase(fruits[left]);
//                }
//                ++left;
//            }
//            //收集结果
//            ret = max(ret, right - left + 1);
//            ++right;
//        }
//        return ret;
//    }
//};


//class Solution {
//public:
//    int totalFruit(vector<int>& fruits) {
//        unordered_map<int, int> hash;  //用来记录水果的类型和个数的关系
//        int n = fruits.size();
//        int left = 0, right = 0;
//        int ret = 0;  //统计结果
//        while (right < n)
//        {
//            //进窗口
//            hash[fruits[right]]++;
//            while (hash.size() > 2)
//            {
//                //出窗口
//                if (--hash[fruits[left]] == 0)
//                    hash.erase(fruits[left]);
//                ++left;
//            }
//            //收集结果
//            ret = max(ret, right - left + 1);
//            ++right;
//        }
//        return ret;
//    }
//};


//class Solution {
//public:
//    vector<int> findAnagrams(string s, string p) {
//        int hashp[26] = { 0 };
//        int hashs[26] = { 0 };
//        int n = s.size();
//        int m = p.size();
//        for (auto ch : p) hashp[ch - 'a']++;
//        vector<int> ret;
//        int count = 0;  //用来统计s中的满足p的个数
//        int left = 0, right = 0;
//        while (right < n)
//        {
//            //进窗口
//            char in = s[right];
//            //维护hash以及count
//            if (++hashs[in - 'a'] <= hashp[in - 'a']) ++count;
//            //判断不满足条件
//            if (right - left + 1 > m)
//            {
//                //出窗口
//                char out = s[left++];
//                if (hashs[out - 'a']-- <= hashp[out - 'a']) --count;
//            }
//            //收集结果
//            if (count == m)  ret.push_back(left);
//            ++right;
//        }
//        return ret;
//    }
//};