#include <iostream>
#include <vector>
using namespace std;

//class Solution {
//    vector<string> ret;
//    string path;
//    int left = 0; //左括号的数量
//    int right = 0; //右括号的数量
//public:
//    vector<string> generateParenthesis(int n) {
//        dfs(n);
//        return ret;
//    }
//    void dfs(int n)
//    {
//        //返回
//        if (right == n)
//        {
//            ret.push_back(path);
//            return;
//        }
//        if (left < n)
//        {
//            path += '(';
//            ++left;
//            dfs(n);
//            path.pop_back();
//            --left;
//        }
//        if (right < left)
//        {
//            //右
//            path += ')';
//            ++right;
//            dfs(n);
//            path.pop_back();
//            --right;
//        }
//
//    }
//};


//class Solution {
//    vector<vector<int>> ret;
//    vector<int> path;
//public:
//    vector<vector<int>> combine(int n, int k) {
//        dfs(n, k, 1);
//        return ret;
//    }
//    void dfs(int n, int k, int x)
//    {
//        //返回
//        if (path.size() == k)
//        {
//            ret.push_back(path);
//            return;
//        }
//        for (int i = x; i <= n; ++i)
//        {
//            path.push_back(i);
//            dfs(n, k, i + 1);
//            path.pop_back();
//        }
//    }
//};


//class Solution {
//    int ret = 0;
//public:
//    int findTargetSumWays(vector<int>& nums, int target) {
//        dfs(nums, target, 0, 0);
//        return ret;
//    }
//    void dfs(vector<int>& nums, int target, int sum, int i)
//    {
//        //返回
//        if (i == nums.size())
//        {
//            if (sum == target)   ++ret;
//            return; //没有结果
//        }
//        dfs(nums, target, sum + nums[i], i + 1);
//        dfs(nums, target, sum - nums[i], i + 1);
//    }
//};


