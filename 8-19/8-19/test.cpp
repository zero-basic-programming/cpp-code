#include <iostream>
#include <vector>
using namespace std;

//struct ListNode {
//    int val;
//    ListNode* next;
//    ListNode() : val(0), next(nullptr) {}
//    ListNode(int x) : val(x), next(nullptr) {}
//    ListNode(int x, ListNode* next) : val(x), next(next) {}
//
//};
//
//class Solution {
//public:
//    ListNode* removeNthFromEnd(ListNode* head, int n) {
//        ListNode* guard = new ListNode(0);
//        guard->next = head;
//        ListNode* pre = guard, * tail = head;
//        while (n--)
//        {
//            tail = tail->next;
//        }
//        //两个指针同时向后移动
//        while (tail)
//        {
//            tail = tail->next;
//            pre = pre->next;
//        }
//        //这里要删除pre后一个节点
//        ListNode* del = pre->next;
//        if (del == head) head = head->next;  //可能是头删
//        pre->next = del->next;
//        delete del;
//        delete guard;
//        return head;
//    }
//};
//
//
//int main()
//{
//    ListNode* node1 = new ListNode(0);
//    ListNode* node2 = new ListNode(1);
//    node1->next = node2;
//    Solution().removeNthFromEnd(node1, 1);
//    return 0;
//}


//class Solution {
//public:
//    ListNode* detectCycle(ListNode* head) {
//        //快慢双指针
//        ListNode* slow = head, * fast = head;
//        while (fast && fast->next)
//        {
//            slow = slow->next;
//            fast = fast->next->next;
//            //相遇时，从相遇节奏和head同时走，就可以走到第一个入环节点
//            if (slow == fast)
//            {
//                ListNode* meet = fast;
//                ListNode* tmp = head;
//                while (tmp != meet)
//                {
//                    tmp = tmp->next;
//                    meet = meet->next;
//                }
//                return meet;
//            }
//        }
//        return nullptr;   //这里说明该链表不是环形链表
//    }
//};


//class Solution {
//public:
//    ListNode* getIntersectionNode(ListNode* headA, ListNode* headB) {
//        //算出两个链表的长度，然后通过前后指针法求出结果
//        int lenA = 0, lenB = 0;
//        ListNode* tmpA = headA, * tmpB = headB;
//        while (tmpA)
//        {
//            ++lenA;
//            tmpA = tmpA->next;
//        }
//        while (tmpB)
//        {
//            ++lenB;
//            tmpB = tmpB->next;
//        }
//        ListNode* longList = headA, * shortList = headB;
//        if (lenA < lenB)
//        {
//            longList = headB;
//            shortList = headA;
//        }
//        int gap = abs(lenA - lenB);
//        //长链表先走gap步
//        while (gap--)
//        {
//            longList = longList->next;
//        }
//        //同时走
//        while (longList)
//        {
//            if (longList == shortList) return longList;
//            longList = longList->next;
//            shortList = shortList->next;
//        }
//        return nullptr;
//    }
//};



//class Solution {
//public:
//    ListNode* reverseList(ListNode* head) {
//        if (!head || !head->next) return head;  //处理空或者一个的情况
//        ListNode* n1 = head, * n2 = head->next, * n3 = nullptr;
//        n1->next = nullptr;
//        while (n2)
//        {
//            n3 = n2->next;
//            n2->next = n1;
//            //迭代
//            n1 = n2;
//            n2 = n3;
//        }
//        return n1;
//    }
//};


