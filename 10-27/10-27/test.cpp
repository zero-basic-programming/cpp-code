#include <iostream>
#include <vector>
#include <list>
#include <string>
using namespace std;


//class Solution {
//public:
//    vector<int> largestValues(TreeNode* root) {
//        //层序遍历
//        vector<int> ret;
//        if (root == nullptr) return ret;
//        queue<TreeNode*> q;
//        q.push(root);
//        while (q.size())
//        {
//            int sz = q.size();
//            int maxNum = INT_MIN;
//            for (int i = 0; i < sz; ++i)
//            {
//                TreeNode* node = q.front();
//                q.pop();
//                maxNum = max(maxNum, node->val);
//                if (node->left) q.push(node->left);
//                if (node->right) q.push(node->right);
//            }
//            ret.push_back(maxNum);
//        }
//        return ret;
//    }
//};


//class Solution {
//public:
//    int findBottomLeftValue(TreeNode* root) {
//        queue<TreeNode*> q;
//        q.push(root);
//        int ret = 0;   //返回值
//        while (q.size())
//        {
//            int sz = q.size();
//            bool flag = true;
//            for (int i = 0; i < sz; ++i)
//            {
//                TreeNode* node = q.front();
//                q.pop();
//                if (flag)
//                {
//                    flag = false;
//                    ret = node->val;
//                }
//                //放入子节点
//                if (node->left) q.push(node->left);
//                if (node->right) q.push(node->right);
//            }
//        }
//        return ret;
//    }
//};


//class Solution {
//public:
//    vector<int> rightSideView(TreeNode* root) {
//        queue<TreeNode*> q;
//        vector<int> ret;
//        if (root == nullptr) return ret;
//        q.push(root);
//        while (q.size())
//        {
//            int sz = q.size();
//            for (int i = 0; i < sz; ++i)
//            {
//                TreeNode* node = q.front();
//                q.pop();
//                if (i == sz - 1)
//                {
//                    ret.push_back(node->val);
//                }
//                if (node->left) q.push(node->left);
//                if (node->right) q.push(node->right);
//            }
//        }
//        return ret;
//    }
//};



//观察者模式

//订阅者
class IObserver
{
public:
	//析构函数
	virtual ~IObserver() = 0;
	//更新数据，打印
	virtual void Update(const string& message_from_subject) = 0;
};

//发布者
class ISubject
{
public:
	//析构函数
	virtual ~ISubject() = 0;
	//添加订阅者
	virtual void AddObserver(IObserver* observer) = 0;
	//删除订阅者
	virtual void DelObserver(IObserver* observer) = 0;
	//通知订阅者
	virtual void Nodify() = 0;
};


//具体发布者     --该版本不能做到随机添加
class Subject : public ISubject
{
public:
	~Subject() override
	{
		//打印即可
		cout << "I am Subject,quit now!" << endl;
	}
	void AddObserver(IObserver* observer) override
	{
		_observers.push_back(observer);
	}
	void DelObserver(IObserver* observer) override
	{
		_observers.remove(observer);
	}
	void Nodify() override
	{
		//遍历链表拿到对应的订阅者，然后更新他们的数据即可
		for (auto& obs : _observers)
		{
			obs->Update(_message);
		}
	}
	//发布者添加对应的数据
	void CreateMessage(string& str)
	{
		_message = str;
		//通知对应的订阅者
		Nodify();
	}
private:
	string _message;					//发布者要发布的数据
	list<IObserver*> _observers;		//对应的订阅者链表
};

//具体订阅者
class Observer : public IObserver
{
public:
	Observer(Subject& subject) : _subject(subject)
	{
		//添加当前订阅者到发布者链表
		_subject.AddObserver(this);
		cout << "I am Observer,my number is: " << ++_number << endl;
	}
	~Observer() override
	{
		RemoveCurObsFromList();
		cout << "I am Observer,quit now! number is: " << _number << endl;
	}
	void Update(const string& message_from_subject) override
	{
		_message_from_subject = message_from_subject;
		PrintMsg();
	}
	//打印出发送过来的数据
	void PrintMsg()
	{
		cout << "subject send message: " << _message_from_subject << endl;
	}
	//删除当前的发布者
	void RemoveCurObsFromList()
	{
		_subject.DelObserver(this);
		cout << "the number: " << _number << " remove from list" << endl;
	}
private:
	Subject& _subject;				//发布者对象
	string _message_from_subject;	//发布者发送过来的数据
	int _number = 0;				//订阅者数量
};

void ClientCode()
{
	Subject* sub = new Subject();
	Observer* obs1 = new Observer(*sub);
	Observer* obs2 = new Observer(*sub);
	Observer* obs3 = new Observer(*sub);
	Observer* obs4;
	Observer* obs5;

	//发送数据
	string sendMsg = "hello world";
	sub->CreateMessage(sendMsg);

	obs3->RemoveCurObsFromList();
	sendMsg = "replace send";
	sub->CreateMessage(sendMsg);

	obs4 = new Observer(*sub);
	obs5 = new Observer(*sub);
	sendMsg = "last send";
	sub->CreateMessage(sendMsg);

	//释放空间
	delete sub;
	delete obs1;
	delete obs2;
	delete obs3;
	delete obs4;
	delete obs5;
}

int main()
{
	ClientCode();
	return 0;
}