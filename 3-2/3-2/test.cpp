#include <iostream>
#include <vector>
using namespace std;

//class Solution {
//public:
//    vector<vector<int>> threeSum(vector<int>& nums) {
//        int n = nums.size();
//        sort(nums.begin(), nums.end());
//        vector<vector<int>> ret;
//        int i = 0;
//        while (i < n - 2)  //边界条件
//        {
//            int left = i + 1, right = n - 1;
//            while (left < right)
//            {
//                if (nums[left] + nums[right] + nums[i] == 0)
//                {
//                    ret.push_back({ nums[i],nums[left++],nums[right--] });
//                    //对结果进行去重
//                    while (left < right && nums[left] == nums[left - 1]) ++left;
//                    while (left < right && nums[right] == nums[right + 1]) --right;
//                }
//                else if (nums[left] + nums[right] + nums[i] > 0) --right;
//                else ++left;
//            }
//            ++i;
//            //对i进行去重
//            if (nums[i] == nums[i - 1])
//            {
//                while (i < n - 2 && nums[i] == nums[i - 1]) ++i;
//            }
//        }
//        return ret;
//    }
//};


//class Solution {
//public:
//    int HappyNum(int n)
//    {
//        int ret = 0;
//        while (n)
//        {
//            int tmp = n % 10;
//            ret += tmp * tmp;
//            n /= 10;
//        }
//        return ret;
//    }
//    bool isHappy(int n) {
//        //如果其不是快乐数，那么肯定会陷入一个循环计算中
//        unordered_map<int, int> hash;  //计算完之后放入到这个容器中
//        hash[n]++;
//        int tmp = n;
//        while (true)
//        {
//            int ret = HappyNum(tmp);
//            cout << ret << endl;
//            if (ret == 1) return true;
//            //判断是否有重复
//            if (hash[ret] > 0)  return false;
//            tmp = ret;
//            hash[ret]++;
//        }
//        return false;
//    }
//};


//class Solution {
//public:
//    int Happy(int n)
//    {
//        int ret = 0;
//        while (n)
//        {
//            int tmp = n % 10;
//            ret += tmp * tmp;
//            n /= 10;
//        }
//        return ret;
//    }
//    bool isHappy(int n) {
//        //使用快慢双指针的思想来解决
//        int slow = n, fast = Happy(n);  //如果快指针能追上慢指针，那么就一定说明事件循环了  
//        while (slow != fast)
//        {
//            slow = Happy(slow);
//            fast = Happy(Happy(fast));
//            if (slow == fast) break;
//        }
//        return  slow == 1 ? true : false;  //这里追上可能是都等于1
//    }
//};


//class Solution {
//public:
//    ListNode* mergeTwoLists(ListNode* list1, ListNode* list2) {
//        ListNode* guard = new ListNode(0);  //设置一个哨兵位的头节点方便尾插
//        ListNode* tail = guard;
//        while (list1 && list2)
//        {
//            if (list1->val < list2->val)
//            {
//                tail->next = list1;
//                list1 = list1->next;
//            }
//            else
//            {
//                tail->next = list2;
//                list2 = list2->next;
//            }
//            tail = tail->next;
//        }
//        //最后把剩下那个链表尾插到结果中
//        tail->next = list1 == nullptr ? list2 : list1;
//        ListNode* newhead = guard->next;
//        delete guard;  //保证内存不泄露
//        return newhead;
//    }
//};