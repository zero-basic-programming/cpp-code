#include <iostream>
#include <vector>
using namespace std;



//int main()
//{
//    int T;
//    cin >> T;
//    while (T--)
//    {
//        int n;
//        cin >> n;
//        vector<int> nums(n);
//        for (auto& e : nums) cin >> e;
//        //滑动窗口
//        int left = 0, right = 0;
//        int change = 1;    //用于反转
//        int ret = 0;  //记录结果
//        while (right < n)
//        {
//            if (nums[right] == 1) --change;
//            //判断
//            while (change < 0)
//            {
//                if (nums[left] == 1)
//                {
//                    ++change;
//                }
//                ++left;
//            }
//            //这里已经是一个合法的结果
//            ret = max(ret, right - left + 1);
//            ++right;
//        }
//        cout << ret << endl;
//    }
//    return 0;
//}


//int main()
//{
//    int n;
//    cin >> n;
//    vector<int> nums(n);
//    for (auto& e : nums) cin >> e;
//    priority_queue<pair<int, int>, vector<pair<int, int>>, greater<pair<int, int>>> q;   //用来存放小堆
//    for (int i = 0; i < n; ++i)
//    {
//        if (nums[i] < 0) q.push(make_pair(nums[i], i));
//    }
//    //这里对堆中数据进行操作，直到堆为0
//    while (q.size())
//    {
//        auto pair = q.top();
//        int x = pair.first;
//        int idx = pair.second;
//        //这里的数据可能修改过
//        if (x != nums[idx])
//        {
//            q.pop();
//            continue;
//        }
//        if (idx + 2 < n && nums[idx + 2] < 0)
//        {
//            if (nums[idx + 1] < 0)
//            {
//                //找更小的，两个都置换为正数
//                if (nums[idx + 1] < nums[idx + 2])   nums[idx + 1] = -nums[idx + 1];
//                else    nums[idx + 2] = -nums[idx + 2];
//            }
//            else
//            {
//                //交换当前和下下个
//                nums[idx + 2] = -nums[idx + 2];
//            }
//            nums[idx] = -nums[idx];
//        }
//        else if (idx + 1 < n && nums[idx + 1] < 0)
//        {
//            nums[idx + 1] = -nums[idx + 1];
//            nums[idx] = -nums[idx];
//        }
//        else
//        {
//            //这里就没有交换的方式
//        }
//        q.pop();
//    }
//    //计算数组大小的和
//    int sum = 0;
//    for (auto e : nums) sum += e;
//    cout << sum << endl;
//    return 0;
//}



//int main()
//{
//    int n;
//    cin >> n;
//    vector<int> nums(n);
//    for (auto& e : nums) cin >> e;
//    for (int i = 0; i < n; ++i)
//    {
//        if (nums[i] < 0)
//        {
//            if (i + 2 < n && nums[i + 2] < 0)
//            {
//                if (nums[i + 1] < 0)
//                {
//                    //找更小的，两个都置换为正数
//                    if (nums[i + 1] < nums[i + 2])   nums[i + 1] = -nums[i + 1];
//                    else    nums[i + 2] = -nums[i + 2];
//                }
//                else
//                {
//                    //交换当前和下下个
//                    nums[i + 2] = -nums[i + 2];
//                }
//                nums[i] = -nums[i];
//            }
//            else if (i + 1 < n && nums[i + 1] < 0)
//            {
//                nums[i + 1] = -nums[i + 1];
//                nums[i] = -nums[i];
//            }
//        }
//    }
//    //计算数组大小的和
//    long long sum = 0;
//    for (auto e : nums) sum += e;
//    cout << sum << endl;
//    return 0;
//}



