#include <iostream>
#include <vector>
using namespace std;

//class Solution {
//public:
//    vector<int> sortArray(vector<int>& nums) {
//        //归并排序
//        vector<int> tmp(nums.size());  //辅助完成排序
//        MergeSort(nums, 0, nums.size() - 1, tmp);
//        return nums;
//    }
//    void MergeSort(vector<int>& nums, int left, int right, vector<int>& tmp)
//    {
//        //返回
//        if (left >= right) return;
//        //找中间值
//        int mid = left + (right - left) / 2;
//        //[left,mid] [mid+1,right]
//        MergeSort(nums, left, mid, tmp);
//        MergeSort(nums, mid + 1, right, tmp);
//        //处理两个有序数组
//        int cur1 = left, cur2 = mid + 1, i = left;
//        while (cur1 <= mid && cur2 <= right)
//            tmp[i++] = nums[cur1] <= nums[cur2] ? nums[cur1++] : nums[cur2++];
//        //处理其中没有处理完的数组
//        while (cur1 <= mid)
//            tmp[i++] = nums[cur1++];
//        while (cur2 <= right)
//            tmp[i++] = nums[cur2++];
//        //还原回原数组
//        for (int i = left; i <= right; ++i)
//        {
//            nums[i] = tmp[i];
//        }
//    }
//};

