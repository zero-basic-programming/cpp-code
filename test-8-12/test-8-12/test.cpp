class Solution {
public:
    int minOperations(vector<int>& nums, int x) {
        //转换为滑动窗口问题
        int sum = 0;
        for (auto& e : nums)   sum += e;
        int target = sum - x;
        if (target < 0) return -1;
        //相当于在中间这段区域找到最长的子数组使其等于target
        int left = 0, right = 0;
        int n = nums.size();
        int total = 0, len = -1;
        while (right < n)
        {
            //进窗口
            total += nums[right];
            //判断
            while (total > target)
            {
                //出窗口
                total -= nums[left++];
            }
            //收集结果
            if (total == target) len = max(len, right - left + 1);
            ++right;
        }
        return len == -1 ? -1 : n - len;
    }
};



class Solution {
public:
    int maxProfit(vector<int>& prices) {
        const int INF = 0x3f3f3f3f;
        int n = prices.size();
        //f表示买入状态  g表示可交易状态
        vector<vector<int>> f(n, vector<int>(3, -INF));
        auto g = f;
        //初始化
        f[0][0] = -prices[0];
        g[0][0] = 0;
        for (int i = 1; i < n; ++i)
        {
            for (int j = 0; j < 3; ++j)
            {
                f[i][j] = max(f[i - 1][j], g[i - 1][j] - prices[i]);
                g[i][j] = g[i - 1][j];
                if (j >= 1)    g[i][j] = max(g[i][j], f[i - 1][j - 1] + prices[i]);
            }
        }
        //最后一行找出最大值
        int ret = 0;
        for (int i = 0; i < 3; ++i) ret = max(ret, g[n - 1][i]);
        return ret;
    }
};


class Solution {
public:
    int maxProfit(int k, vector<int>& prices) {
        const int INF = 0x3f3f3f3f;
        int n = prices.size();
        vector<vector<int>> f(n, vector<int>(k + 1, -INF));
        auto g = f;
        //初始化
        f[0][0] = -prices[0];
        g[0][0] = 0;
        for (int i = 1; i < n; ++i)
        {
            for (int j = 0; j <= k; ++j)
            {
                f[i][j] = max(f[i - 1][j], g[i - 1][j] - prices[i]);
                g[i][j] = g[i - 1][j];
                if (j > 0) g[i][j] = max(g[i][j], f[i - 1][j - 1] + prices[i]);
            }
        }
        //结果是最后一行中最大的那个
        int ret = 0;
        for (int i = 0; i <= k; ++i) ret = max(ret, g[n - 1][i]);
        return ret;
    }
};