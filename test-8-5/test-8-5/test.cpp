#include <iostream>
#include <vector>
using namespace std;


//class Solution {
//public:
//    int triangleNumber(vector<int>& nums) {
//        int n = nums.size();
//        if (n < 3)   return 0;
//        sort(nums.begin(), nums.end());
//        int end = n - 1;
//        int count = 0;
//        while (end > 1)
//        {
//            int left = 0;
//            int right = end - 1;
//            while (left < right)
//            {
//                if (nums[left] + nums[right] > nums[end])
//                {
//                    count += (right - left); //中间的数一定满足条件
//                    --right;
//                }
//                else
//                    ++left; //这里right无论怎么移动必定不会满足条件
//            }
//            --end;
//        }
//        return count;
//    }
//};


//class Solution {
//public:
//    vector<int> twoSum(vector<int>& nums, int target) {
//        vector<int> result;
//        int left = 0;
//        int right = nums.size() - 1;
//        while (left < right)
//        {
//            int tmp = nums[left] + nums[right];
//            if (tmp > target) --right;
//            else if (tmp < target) ++left;
//            else
//            {
//                result.push_back(nums[left]);
//                result.push_back(nums[right]);
//                break;
//            }
//        }
//        return result;
//    }
//};



//class Solution {
//public:
//    int calculateMinimumHP(vector<vector<int>>& dungeon) {
//        int m = dungeon.size();
//        int n = dungeon[0].size();
//        vector<vector<int>> dp(m + 1, vector<int>(n + 1, INT_MIN));
//        //初始化
//        dp[m - 1][n] = dp[m][n - 1] = 1;
//        for (int i = m - 1; i >= 0; --i)
//        {
//            for (int j = n - 1; j >= 0; --j)
//            {
//                dp[i][j] = min(dp[i + 1][j], dp[i][j + 1]) - dungeon[i][j];
//                //如果是负数，就处理好正好满足情况
//                //这里处理正数的情况
//                dp[i][j] = max(dp[i][j], 1); //正数减完最后为负数，说明不需要那么多健康值
//            }
//        }
//        return dp[0][0];
//    }
//};


