#include <iostream>
#include <vector>
#include <stack>
using namespace std;


void Qsort(vector<int>& arr, int l, int r)
{
	if (l >= r) return;
	//找基准值
	int key = arr[rand() % (r - l + 1) + l];
	int left = l - 1, right = r + 1;
	int i = l;
	while (i < right)
	{
		if (arr[i] < key) swap(arr[i++], arr[++left]);
		else if (arr[i] == key) ++i;
		else swap(arr[i], arr[--right]);
	}
	//递归
	Qsort(arr,l, left);
	Qsort(arr, right, r);
}

void QsortNonR(vector<int>& arr)
{
	int n = arr.size();
	stack<int> st;

}


void MergeSort(vector<int>& arr,int left,int right, vector<int>& tmp)
{
	if (left >= right) return;
	int mid = (left + right) >> 1;
	//[left,mid] [mid+1,right]
	MergeSort(arr, left, mid,tmp);
	MergeSort(arr, mid + 1, right,tmp);
	int begin1 = left, end1 = mid;
	int begin2 = mid + 1, end2 = right;
	int i = left;
	while (begin1 <= end1 && begin2 <= end2)
	{
		tmp[i++] = arr[begin1] < arr[begin2] ? arr[begin1++] : arr[begin2++];
	}
	//处理剩下的数据
	while (begin1 <= end1)
	{
		tmp[i++] = arr[begin1++];
	}
	while (begin2 <= end2)
	{
		tmp[i++] = arr[begin2++];
	}
	//还原到原数组
	for (int j = left; j <= right; ++j)
		arr[j] = tmp[j];
}

int main()
{
	srand(time(0));
	vector<int> arr = { 2,3,5,4,8,7,5,6,9,0 };
	vector<int> tmp = arr;
	int n = arr.size();
	//Qsort(arr, 0, n - 1);
	MergeSort(arr, 0, n - 1, tmp);
	for (auto e : arr)
	{
		cout << e << " ";
	}
	return 0;
}