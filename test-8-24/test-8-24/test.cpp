#include <iostream>
#include <vector>
using namespace std;

//class Solution {
//public:
//    int findMin(vector<int>& nums) {
//        int left = 0, right = nums.size() - 1;
//        while (left < right)
//        {
//            int mid = left + (right - left) / 2;
//            if (nums[mid] <= nums[right])
//            {
//                right = mid;
//            }
//            else if (nums[mid] >= nums[left])
//            {
//                left = mid + 1;
//            }
//        }
//        return nums[right];
//    }
//};

//class Solution {
//public:
//    int missingNumber(vector<int>& nums) {
//        //使用位运算
//        int ret = 0;
//        for (auto e : nums) {
//            ret ^= e;
//        }
//        int n = nums[nums.size() - 1];
//        for (int i = 0; i <= n; ++i)
//        {
//            ret ^= i;
//        }
//        //有可能是最后一个数
//        return ret == 0 ? n + 1 : ret;
//    }
//};



//class Solution {
//public:
//    int missingNumber(vector<int>& nums) {
//        //使用二分法,找缺失的数字的前一个
//        int left = 0, right = nums.size() - 1;
//        while (left < right)
//        {
//            int mid = left + (right - left + 1) / 2;
//            if (nums[mid] == mid)
//            {
//                left = mid;
//            }
//            else {
//                right = mid - 1;
//            }
//        }
//        return nums[left] == left ? left + 1 : left;
//    }
//};


//class Solution {
//public:
//    int missingNumber(vector<int>& nums) {
//        int ret = 0;
//        int n = nums.size() + 1;
//        for (int i = 1; i < n; ++i) {
//            ret ^= i;
//        }
//        for (auto e : nums) {
//            ret ^= e;
//        }
//        return ret;
//    }
//};


#include <iostream>
#include <vector>


//int main() {
//    int n, q;
//    cin >> n >> q;
//    vector<int> v(n);
//    for (auto& e : v) {
//        int tmp;
//        cin >> tmp;
//        e = tmp;
//    }
//    while (q--) {
//        int l, r;
//        cin >> l >> r;
//        l -= 1;
//        r -= 1;
//        int sum = 0;
//        while (l <= r) {
//            sum += v[l++];
//        }
//        cout << sum << endl;
//    }
//}

//class Solution {
//public:
//    int maxProduct(vector<int>& nums) {
//        //创建两个dp表
//        int n = nums.size();
//        vector<int> f(n); //计算最大
//        vector<int> g(n); //计算最小
//        int fmax = nums[0];
//        //初始化
//        f[0] = g[0] = nums[0];
//        for (int i = 1; i < n; ++i)
//        {
//            if (nums[i] >= 0)
//            {
//                f[i] = max(f[i - 1] * nums[i], nums[i]);
//                g[i] = min(g[i - 1] * nums[i], nums[i]);
//                fmax = max(f[i], fmax);
//            }
//            else {
//                f[i] = max(g[i - 1] * nums[i], nums[i]);
//                g[i] = min(f[i - 1] * nums[i], nums[i]);
//                fmax = max(f[i], fmax);
//            }
//        }
//        return fmax;
//    }
//};


//class Solution {
//public:
//    int getMaxLen(vector<int>& nums) {
//        int n = nums.size();
//        int fmax = 1, gmin = 1;
//        vector<int> f(n);
//        vector<int> g(n);
//        for (int i = 1; i < n; ++i)
//        {
//            if (nums[i] > 0) {
//                ++fmax;
//                ++gmin;
//                f[i] = max(f[i - 1] * nums[i], nums[i]);
//                g[i] = min(g[i - 1] * nums[i], nums[i]);
//            }
//            else if (nums[i] < 0)
//            {
//                if (g[i - 1] * nums[i] > 0) fmax = gmin + 1;
//                else fmax = 0;
//                if (f[i - 1] * nums[i] < 0) gmin = fmax + 1;
//                else gmin = 0;
//                f[i] = max(g[i - 1] * nums[i], nums[i]);
//                g[i] = min(f[i - 1] * nums[i], nums[i]);
//            }
//            else {
//                fmax = gmin = 0;
//                f[i] = f[i - 1];
//                g[i] = g[i - 1];
//            }
//        }
//        return fmax;
//    }
//};

