#include  <iostream>

//class Solution {
//public:
//    vector<int> path; //用来保存每次得到的路径
//    vector<vector<int>> result; //用来保存结果
//public:
//    void tratravel(int begin, int k, int n)
//    {
//        if (k == path.size())
//        {
//            //收集结果
//            result.push_back(path);
//            return;
//        }
//        //遍历每一层数据
//        for (int i = begin; i <= (n - (k - path.size()) + 1); ++i) // 这里是可以优化的
//        {
//            path.push_back(i);
//            tratravel(i + 1, k, n);
//            path.pop_back(); //回退
//        }
//        return;
//    }
//    vector<vector<int>> combine(int n, int k) {
//        tratravel(1, k, n);
//        return result;
//    }
//};


//class Solution {
//private:
//    int ret = 0; //用来存放单次的结果
//    vector<int> path; // 用来存放单次路径
//    vector<vector<int>> result; //存放最后的结果
//public:
//    void backtracking(int first, int k, int n)
//    {
//        if (ret > n) return;
//        if (path.size() == k)
//        {
//            if (ret == n)
//            {
//                result.push_back(path);
//            }
//            return;
//        }
//        //遍历1到9的数字，查找
//        for (int i = first; i <= 9; ++i)
//        {
//            path.push_back(i);
//            ret += i;
//            backtracking(i + 1, k, n);
//            ret -= i;
//            path.pop_back();
//        }
//        return;
//    }
//    vector<vector<int>> combinationSum3(int k, int n) {
//        backtracking(1, k, n);
//        return result;
//    }
//};


//class Solution {
//private:
//    string path; // 保存路径
//    vector<string> result; //保存结果
//    string ret[10] = { "","","abc","def","ghi","jkl","mno","pqrs","tuv","wxyz" }; //用来构建映射关系
//public:
//    void backtracking(vector<string>& vs, int j)
//    {
//        if (j == vs.size())
//        {
//            //收集结果
//            result.push_back(path);
//            return;
//        }
//        //遍历vs当前层的元素
//        for (int i = 0; i < vs[j].size(); ++i)
//        {
//            path += vs[j][i];
//            backtracking(vs, j + 1);
//            path.pop_back(); //回退
//        }
//        return;
//    }
//    vector<string> letterCombinations(string digits) {
//        if (digits == "")    return result;
//        vector<string> vs; //得到结果
//        for (auto ch : digits)
//        {
//            int i = (ch - '0');
//            vs.push_back(ret[i]);
//        }
//        //回溯遍历
//        backtracking(vs, 0);
//        return result;
//    }
//};


