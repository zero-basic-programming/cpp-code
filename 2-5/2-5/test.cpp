#include <iostream>
#include <vector>
#include <stack>
using namespace std;

void Qsort(vector<int>& arr, int l, int r)
{
	if (l >= r) return;
	//选择基准值
	int key = arr[rand() % (r - l + 1) + l];
	int left = l - 1, right = r + 1;
	int i = l;
	while (i < right)
	{
		if (arr[i] < key) swap(arr[i++], arr[++left]);
		else if (arr[i] == key) ++i;
		else swap(arr[i], arr[--right]);
	}
	Qsort(arr, l, left);
	Qsort(arr, right, r);
}

//快排非递归实现
void QsortNonR(vector<int>& arr)
{
	int n = arr.size();
	int left = 0, right = n - 1;
	stack<int> st;
	st.push(left);
	st.push(right);
	while (st.size())
	{
		//取出两个数据，然后找基准值
		int r = st.top(); 
		st.pop();
		int l = st.top(); 
		st.pop();
		int key = arr[rand() % (r - l + 1) + l];
		int i = l;
		int left1 = l - 1, right1 = r + 1;
		while (i < right1)
		{
			if (arr[i] < key) swap(arr[i++], arr[++left1]);
			else if (arr[i] == key) ++i;
			else swap(arr[i], arr[--right1]);
		}
		// [l,left1] [right1,r]
		if (left1 > l)
		{
			st.push(l); 
			st.push(left1);
		}
		if (right1 < r)
		{
			st.push(right1); 
			st.push(r);
		}
	}
}

//归并排序
void MergeSort(vector<int>& arr, int left, int right, vector<int>& tmp)
{
	if (left >= right) return;
	//找中间值，然后先归并左右，然后再整体归并
	int mid = (left + right) >> 1;
	//[left,mid] [mid+1,right]
	MergeSort(arr, left, mid,tmp);
	MergeSort(arr, mid + 1, right,tmp);
	int begin1 = left, end1 = mid;
	int begin2 = mid + 1, end2 = right;
	int i = left;
	while (begin1 <= end1 && begin2 <= end2)
	{
		tmp[i++] = arr[begin1] < arr[begin2] ? arr[begin1++] : arr[begin2++];
	}
	//处理剩下的数据
	while (begin1 <= end1)
	{
		tmp[i++] = arr[begin1++];
	}
	while (begin2 <= end2)
	{
		tmp[i++] = arr[begin2++];
	}
	//还原回原数组
	for (int j = left; j <= right; ++j)
		arr[j] = tmp[j];
}

//归并非递归
void MergeSortNonR(vector<int>& arr, vector<int>& tmp)
{
	int n = arr.size();
	int sep = 1;
	while (sep < n)
	{
		//单次的归并排序的逻辑
		for (int i = 0; i < n; i += 2 * sep)
		{
			int begin1 = i, end1 = i + sep - 1;
			int begin2 = i + sep, end2 = i + 2 * sep - 1;
			//处理边界条件
			if (begin2 >= n) break; //最后一组的后面那组是没有数据的
			if (end2 >= n) end2 = n - 1; //最后一组数据是不全的
			int j = i, k = i;   //j用来tmp数组的使用，k用来还原数组
			while (begin1 <= end1 && begin2 <= end2)
			{
				tmp[j++] = arr[begin1] < arr[begin2] ? arr[begin1++] : arr[begin2++];
			}
			while (begin1 <= end1)
			{
				tmp[j++] = arr[begin1++];
			}
			while (begin2 <= end2)
			{
				tmp[j++] = arr[begin2++];
			}
			//还原回原数组
			for (; k <= end2; ++k)
				arr[k] = tmp[k];
		}
		sep *= 2;
	}
}

//排升序，建大堆
void AdjustDown(vector<int>& arr, int n, int parent)
{
	int child = 2 * parent + 1;
	while (child < n)
	{
		if (child + 1 < n && arr[child + 1] > arr[child])
		{
			++child;
		}
		if (arr[child] > arr[parent])
		{
			swap(arr[child], arr[parent]);
			parent = child;
			child = 2 * parent + 1;
		}
		else
		{
			break; //已成堆
		}
	}
}

//堆排
void HeapSort(vector<int>& arr)
{
	//向下调整建堆
	int n = arr.size();
	for (int i = (n - 1 - 1) / 2; i >= 0; --i)
	{
		AdjustDown(arr, n, i);
	}
	//交换头尾两个节点，然后向下调整
	for (int i = 0; i < n ; ++i)
	{
		swap(arr[0], arr[n - i - 1]);
		AdjustDown(arr, n - i - 1, 0);
	}
}

//冒泡排序
void BufferSort(vector<int>& arr)
{
	int n = arr.size();
	for (int i = 0; i < n - 1; ++i)  //趟数
	{
		bool flag = false;  //用来优化，如果当前趟并没有进行处理，那么就说明已经是有序的
		for (int j = 0; j < n - i - 1; ++j)  //单趟所需要执行的次数
		{
			if (arr[j] > arr[j + 1])
			{
				swap(arr[j], arr[j + 1]);
				flag = true;
			}
		}
		if (flag == false) break;
	}
}

//选择排序
void SelectSort(vector<int>& arr)
{
	//一次选择一个最大的和一个最小的
	int n = arr.size();
	int left = 0, right = n - 1;
	while (left < right)
	{
		int maxi = left, mini = left;
		for (int i = left; i <= right; ++i)
		{
			if (arr[i] > arr[maxi]) maxi = i;
			if (arr[i] < arr[mini]) mini = i;
		}
		swap(arr[mini], arr[left]);
		//这里最大的那个数有可能在left这个位置
		if (maxi == left) maxi = mini;
		swap(arr[maxi], arr[right]);
		--right;
		++left;
	}
}

//插入排序
void InsertSort(vector<int>& arr)
{
	int n = arr.size();
	for (int i = 0; i < n - 1; ++i)
	{
		int end = i;
		int tmp = arr[end+1];
		while (end >= 0)
		{
			if (arr[end] > tmp)
			{
				arr[end + 1] = arr[end];
			}
			else
			{
				break;
			}
			--end;
		}
		arr[end + 1] = tmp;
	}
}

//希尔排序
void ShellSort(vector<int>& arr)
{
	int n = arr.size();
	int gap = n;
	while (gap > 1)
	{
		gap /= 2;
		for (int i = 0; i < n - gap; ++i)
		{
			int end = i;
			int tmp = arr[end + gap];
			while (end >= 0)
			{
				if (arr[end] > tmp)
				{
					arr[end + gap] = arr[end];
				}
				else break;
				end -= gap;
			}
			//把该数填写到对应的位置
			arr[end + gap] = tmp;
		}
	}
}

//计数排序，适用于数字差较小的场合，时间复杂度可以达到O(N)
void CountSort(vector<int>& arr)
{
	int n = arr.size();
	//遍历一次，找到最小值
	int min = INT_MAX;
	for (auto e : arr)
	{
		if (e < min) min = e;
	}
	//遍历第二次，映射到hash中
	vector<int> hash(n);
	for (auto e : arr)
	{
		hash[e - min]++;
	}
	//遍历第三次，把hash中的排序结果放回到原数组
	int j = 0;
	for (int i = 0; i < n; ++i)
	{
		while (hash[i]--)
		{
			arr[j++] = min + i;
		}
	}
}

int main()
{
	srand(time(0));
	vector<int> arr = { 5,3,6,8,7,9,0,3,2,1 };
	int n = arr.size();
	vector<int> tmp(arr);
	//Qsort(arr, 0, n - 1);
	//QsortNonR(arr);
	//MergeSort(arr, 0, n - 1, tmp);
	//MergeSortNonR(arr, tmp);
	//HeapSort(arr);
	//BufferSort(arr);
	//SelectSort(arr);
	//InsertSort(arr);
	//ShellSort(arr);
	CountSort(arr);
	for (auto e : arr)
	{
		cout << e << " ";
	}
	return 0;
}


//int main()
//{
//	srand(time(0));
//	int tmp = rand() % 74 +1;
//	cout << "请猜一个1到74之间的数字" << endl;
//	int count = 0;
//	while (1)
//	{
//		cout << "请输入你猜的数字" << endl;
//		++count;
//		int ret;
//		cin >> ret;
//		if (ret > tmp)
//		{
//			cout << "你猜大了" << endl;
//		}
//		else if (ret < tmp)
//		{
//			cout << "你猜小了" << endl;
//		}
//		else
//		{
//			cout << "恭喜你，猜对了" << endl;
//			break;
//		}
//	}
//	cout << "你猜的次数是" << count << endl;
//	return 0;
//}


