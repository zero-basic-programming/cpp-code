#include <iostream>
using namespace std;
#include <set>
//template <class T1, class T2>
//struct pair
//{
//	typedef T1 first_type;
//	typedef T2 second_type;
//	T1 first;
//	T2 second;
//	pair() : first(T1()), second(T2())
//	{}
//	pair(const T1& a, const T2& b) : first(a), second(b)
//	{}
//};

//int main()
//{
//	 用数组array中的元素构造set
//	int array[] = { 1, 3, 5, 7, 9, 2, 4, 6, 8, 0, 1, 3, 5, 7, 9, 2, 4,6, 8, 0 };
//	int sz = sizeof(array) / sizeof(array[0]);
//	//迭代器区间初始化
//	set<int> s(array,array+sz);
//	set<int> s;
//	for (int i = 0; i < sz; ++i)
//	{
//		s.insert(array[i]);
//	}
//	打印
//	for (auto& e : s)
//	{
//		cout << e << " ";
//	}
//	cout << endl;
//	反向打印
//	for (auto i = s.rbegin(); i != s.rend(); ++i)
//	{
//		cout << *i << " ";
//	}
//	cout << endl;
//	 set中值为3的元素出现了几次,因为实现了去重，所以元素都是1次
//	cout << s.count(3) << endl;
//	return 0;
//}
#include <map>
//int main()
//{
//	map<string, string> m;
//	m.insert(pair<string, string>("字符串","string"));
//	//直接使用make_pair就可以不用写那么复杂
//	m.insert(make_pair("banan", "香蕉"));
//	// 将<"apple", "">插入map中，插入成功，返回value的引用，将“苹果”赋值给该引用结果，
//	m["apple"] = "苹果";
//	for (auto& e : m)
//	{
//		cout << e.first << " " << e.second << endl;
//	}
//	cout << endl;
//	// 删除key为"apple"的元素
//	m.erase("apple");
//	if (1 == m.count("apple"))
//		cout << "apple还在" << endl;
//	else
//		cout << "apple被删除" << endl;
//	return 0;
//}

int main()
{
	int array[] = { 2, 1, 3, 9, 6, 1, 5, 8, 4, 7 };

	// 注意：multiset在底层实际存储的是<int, int>的键值对
	multiset<int> s(array, array + sizeof(array) / sizeof(array[0]));
	for (auto& e : s)
		cout << e << " ";
	cout << endl;
	return 0;
}