#include <iostream>
#include <vector>
using namespace std;

//class Solution {
//public:
//    int findLongestChain(vector<vector<int>>& pairs) {
//        int n = pairs.size();
//        sort(pairs.begin(), pairs.end());
//        vector<int> dp(n, 1);
//        int ret = 1;
//        for (int i = 1; i < n; ++i)
//        {
//            for (int j = 0; j < i; ++j)
//            {
//                if (pairs[i][0] > pairs[j][1])
//                {
//                    dp[i] = max(dp[i], dp[j] + 1);
//                }
//            }
//            ret = max(ret, dp[i]);
//        }
//        return ret;
//    }
//};


//class Solution {
//public:
//    int longestSubsequence(vector<int>& arr, int difference) {
//        int n = arr.size();
//        int ret = 1;
//        unordered_map<int, int> hash;
//        //初始化
//        hash[arr[0]] = 1;
//        for (int i = 1; i < n; ++i)
//        {
//            hash[arr[i]] = hash[arr[i] - difference] + 1;
//            ret = max(ret, hash[arr[i]]);
//        }
//        return ret;
//    }
//};


//class Solution {
//public:
//    int lenLongestFibSubseq(vector<int>& arr) {
//        unordered_map<int, int> hash;  //建立元素和下标的映射关系
//        int n = arr.size();
//        for (int i = 0; i < n; ++i) hash[arr[i]] = i;
//        vector<vector<int>> dp(n, vector<int>(n, 2));
//        int ret = 2;
//        for (int i = 2; i < n; ++i)  //固定最后一个位置
//        {
//            for (int j = 1; j < i; ++j)  //固定倒数第2个位置
//            {
//                int k = arr[i] - arr[j];
//                if (k < arr[j] && hash.count(k))
//                    dp[i][j] = dp[j][hash[k]] + 1;
//                ret = max(ret, dp[i][j]);
//            }
//        }
//        return ret < 3 ? 0 : ret;
//    }
//};


//class Solution {
//public:
//    int longestArithSeqLength(vector<int>& nums) {
//        int n = nums.size();
//        unordered_map<int, int> hash;
//        //初始化hash
//        hash[nums[0]] = 0;
//        int ret = 2;
//        vector<vector<int>> dp(n, vector<int>(n, 2));
//        for (int i = 1; i < n; ++i)   //固定倒数第2个位置
//        {
//            for (int j = i + 1; j < n; ++j)  //固定倒数第一个位置
//            {
//                int a = 2 * nums[i] - nums[j];
//                if (hash.count(a))
//                    dp[i][j] = dp[hash[a]][i] + 1;
//                ret = max(ret, dp[i][j]);
//            }
//            //更新倒数第2个元素到hash中
//            hash[nums[i]] = i;
//        }
//        return ret;
//    }
//};



//class Solution {
//public:
//    int numberOfArithmeticSlices(vector<int>& nums) {
//        int n = nums.size();
//        unordered_map<long long, vector<int>> hash;  //因为相同重复的数，并且每一个数都可能被使用 
//        for (int i = 0; i < n; ++i) hash[nums[i]].push_back(i);
//        vector<vector<int>> dp(n, vector<int>(n));
//        int ret = 0;  //统计个数
//        for (int i = 1; i < n; ++i)  //固定倒数第2个数
//        {
//            for (int j = i + 1; j < n; ++j)  //固定倒数第一个数
//            {
//                long long a = (long long)2 * nums[i] - nums[j];
//                if (hash.count(a))
//                {
//                    for (auto k : hash[a])
//                    {
//                        if (k < i) dp[i][j] += dp[k][i] + 1;
//                        else break;  //因为是按下标顺序放入
//                    }
//                }
//                ret += dp[i][j];
//            }
//        }
//        return ret;
//    }
//};


