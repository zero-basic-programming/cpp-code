#include <iostream>
#include <vector>
#include <unordered_map>
using namespace std;


//class Solution {
//public:
//    int maxSubArray(vector<int>& nums) {
//        int n = nums.size();
//        vector<int> dp(n);
//        //初始化
//        dp[0] = nums[0];
//        int ret = nums[0];
//        for (int i = 1; i < n; ++i)
//        {
//            if (dp[i - 1] <= 0)
//            {
//                dp[i] = nums[i];
//            }
//            else  dp[i] = dp[i - 1] + nums[i];
//            //每次得到结果都更新以下
//            ret = max(ret, dp[i]);
//        }
//        return ret;
//    }
//};



//class Solution {
//    unordered_map<int, int> up; //记录数字和个数的映射关系
//public:
//    int totalFruit(vector<int>& nums) {
//        int n = nums.size();
//        int left = 0, right = 0, len = 0;
//        while (right < n)
//        {
//            //进窗口
//            up[nums[right]]++;
//            //判断
//            while (up.size() > 2)
//            {
//                up[nums[left]]--;
//                if (up[nums[left]] == 0) up.erase(nums[left]);
//                ++left;
//            }
//            //收集结果
//            len = max(len, right - left + 1);
//            ++right;
//        }
//        return len;
//    }
//};
//
//
//int main()
//{
//    vector<int> v = { 1,2,1 };
//    Solution().totalFruit(v);
//    return 0;
//}


//快排


//三数取中
int get_mid_index(int* arr, int left, int right)
{
	int mid = left + (right - left) / 2;
	if (arr[mid] > arr[left])
	{
		if (arr[mid] < arr[right]) return mid;
		else if (arr[left] > arr[right]) return left;
		else return right;
	}
	else //arr[mid] <= arr[left]
	{
		if (arr[mid] > arr[right]) return mid;
		else if (arr[right] > arr[left]) return left;
		else return right;
	}
}

//单趟快排
int part_sort(int* arr, int left, int right)
{
	//三数取中
	int mid = get_mid_index(arr, left, right);
	//使用挖坑法
	swap(arr[mid], arr[left]);
	int hole = left;
	int tmp = arr[left]; //保存好我们需要的中间结果
	while (left < right)
	{
		//右边找小，左边找大
		while (left < right && arr[right] >= tmp) --right;
		//找到小
		arr[hole] = arr[right];
		hole = right;
		while (left < right && arr[left] <= tmp) ++left;
		//找到小
		arr[hole] = arr[left];
		hole = left;
	}
	arr[hole] = tmp;
	return hole;
}

void quick_sort(int* arr,int left,int right)
{
	if (left >= right)	return;
	//[begin,keyi-1] keyi [keyi+1，end]
	int keyi = part_sort(arr, left, right);
	quick_sort(arr, left, keyi - 1);
	quick_sort(arr, keyi + 1, right);
}

int main()
{
	int arr[9] = {4,2,5,6,1,3,8,9,7};
	quick_sort(arr,0,9);
	return 0;
}