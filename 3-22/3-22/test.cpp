#include <iostream>
#include <vector>
using namespace std;


//class Solution {
//public:
//    void qsort(vector<int>& nums, int l, int r)
//    {
//        if (l >= r) return;
//        int left = l - 1, right = r + 1;
//        int i = l;
//        while (i < right)
//        {
//            if (nums[i] < 1) swap(nums[i++], nums[++left]);
//            else if (nums[i] == 1) ++i;
//            else swap(nums[i], nums[--right]);
//        }
//    }
//    void sortColors(vector<int>& nums) {
//        //key为1进行快速选择排序
//        qsort(nums, 0, nums.size() - 1);
//    }
//};
//
//int main()
//{
//    vector<int> arr = { 2,0,2,1,1,0 };
//    Solution().sortColors(arr);
//    for (auto e : arr)
//    {
//        cout << e << " ";
//    }
//    return 0;
//}


//class Solution {
//public:
//    void qsort(vector<int>& nums, int l, int r)
//    {
//        if (l >= r) return;
//        //找基准值
//        int key = nums[rand() % (r - l + 1) + l];
//        int left = l - 1, right = r + 1, i = l;
//        while (i < right)
//        {
//            if (nums[i] < key) swap(nums[i++], nums[++left]);
//            else if (nums[i] == key) ++i;
//            else swap(nums[i], nums[--right]);
//        }
//        qsort(nums, l, left);
//        qsort(nums, right, r);
//    }
//    vector<int> sortArray(vector<int>& nums) {
//        qsort(nums, 0, nums.size() - 1);
//        return nums;
//    }
//};


//class Solution {
//public:
//    int findKthLargest(vector<int>& nums, int k) {
//        //建k个小堆来解决
//        priority_queue<int, vector<int>, greater<int>> q;
//        for (int i = 0; i < k; ++i)
//        {
//            q.push(nums[i]);
//        }
//        //把后面的元素对栈顶元素进行比较
//        for (int i = k; i < nums.size(); ++i)
//        {
//            if (nums[i] > q.top())
//            {
//                q.pop();
//                q.push(nums[i]);
//            }
//        }
//        //栈顶元素为所求
//        return q.top();
//    }
//};


//class Solution {
//public:
//    int qsort(vector<int>& nums, int l, int r, int k)
//    {
//        if (l >= r) return nums[l];
//        //找基准值
//        int key = nums[rand() % (r - l + 1) + l];
//        int left = l - 1, right = r + 1, i = l;
//        while (i < right)
//        {
//            if (nums[i] < key) swap(nums[i++], nums[++left]);
//            else if (nums[i] == key) ++i;
//            else swap(nums[i], nums[--right]);
//        }
//        int a = r - right + 1, b = r - left;  //a指的是最后一个区域，b是后面两个区域的和
//        if (a >= k) return qsort(nums, right, r, k);
//        else if (b >= k)  return key;  //这个是中间等于key的元素
//        else return qsort(nums, l, left, k - b);
//    }
//    int findKthLargest(vector<int>& nums, int k) {
//        //通过快速选择排序中的块数来排部分的元素即可确定第k个
//        return qsort(nums, 0, nums.size() - 1, k);
//    }
//};


//class Solution {
//public:
//    void qsort(vector<int>& nums, int l, int r, int k)
//    {
//        if (l >= r) return;
//        int key = nums[rand() % (r - l + 1) + l];
//        int left = l - 1, right = r + 1, i = l;
//        while (i < right)
//        {
//            if (nums[i] < key) swap(nums[i++], nums[++left]);
//            else if (nums[i] == key) ++i;
//            else swap(nums[i], nums[--right]);
//        }
//        //对区间进行选择
//        int a = left - l + 1, b = right - l;  //a是最左边的区域，b是左边和中间的区域
//        if (a > k) qsort(nums, l, left, k);
//        else if (b >= k) return;
//        else qsort(nums, right, r, k - b);
//    }
//    vector<int> inventoryManagement(vector<int>& stock, int cnt) {
//        qsort(stock, 0, stock.size() - 1, cnt);
//        return vector<int>(stock.begin(), stock.begin() + cnt);
//    }
//};