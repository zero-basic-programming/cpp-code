#include <iostream>
#include <vector>
#include <unordered_map>
using namespace std;

//class Solution {
//public:
//    bool lemonadeChange(vector<int>& bills) {
//        //存储5和10块的映射关系，
//        unordered_map<int, int> hash;
//        for (auto e : bills)
//        {
//            if (e == 5)
//            {
//                hash[e]++;
//            }
//            else if (e == 10)
//            {
//                //判断有没有5，没有就错误
//                if (hash.count(5) == 0) return false;
//                hash[5]--;
//                hash[10]++;
//            }
//            else {
//                if (hash[5] == 0 || hash[10] == 0) return false;
//                --hash[5];
//                --hash[10];
//            }
//        }
//        return true;
//    }
//};
//
//int main()
//{
//    vector<int> arr = { 5, 5, 10, 10, 20 };
//    std::cout<<Solution().lemonadeChange(arr);
//    return 0;
//}

//class Solution {
//public:
//    int minCut(string s) {
//        //先确定所有位置组成的字符串是否能形成回文
//        int n = s.size();
//        vector<vector<bool>> IsPal(n, vector<bool>(n));
//        for (int i = n - 1; i >= 0; --i)
//        {
//            for (int j = i; j < n; ++j)
//            {
//                if (s[i] == s[j])
//                    IsPal[i][j] == i + 1 < j ? IsPal[i + 1][j - 1] : true;
//            }
//        }
//        vector<int> dp(n, INT_MAX);
//        for (int i = 0; i < n; ++i)
//        {
//            if (IsPal[0][i])
//            {
//                dp[i] = 0;
//            }
//            else {
//                for (int j = 1; j <= i; ++j)
//                {
//                    if (IsPal[j][i])
//                        dp[i] = min(dp[i], dp[j - 1] + 1);
//                }
//            }
//
//        }
//        return dp[n - 1];
//    }
//};