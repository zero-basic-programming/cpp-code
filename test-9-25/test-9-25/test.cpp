#include <iostream>
#include <vector>
using namespace std;


void _MergeSort(vector<int>& arr, int left, int right, vector<int>& tmp)
{
	//返回
	if (left >= right) return;
	//每次找到中间的值
	int mid = left + (right - left) / 2;
	// [left mid] [mid+1,right]
	_MergeSort(arr, left, mid, tmp);
	_MergeSort(arr, mid+1, right, tmp);
	int begin1 = left; int end1 = mid;
	int begin2 = mid + 1; int end2 = right;
	int i = left;  //这里是这两个区间的最左边而不是0
	while (begin1 <= end1 && begin2 <= end2)
	{
		//比较两个区间的值，并放到另外一个
		if (arr[begin1] < arr[begin2])
		{
			tmp[i++] = arr[begin1++];
		}
		else
		{
			tmp[i++] = arr[begin2++];
		}
	}
	//可能还有剩余没有记录到的数据
	while (begin1 <= end1)
	{
		arr[i++] = arr[begin1++];
	}
	while (begin2 <= end2)
	{
		arr[i++] = arr[begin2++];
	}
	//拷贝回原数组
	for (int j = left; j <= right; ++j) arr[j] = tmp[j];
	//std::copy(tmp.begin() + left, tmp.begin() + right, arr.begin());
}

void MergeSort(vector<int>& arr)
{
	vector<int> tmp(arr.size()); //另外的空间，进行归并排序
	_MergeSort(arr, 0, arr.size() - 1, tmp);
}

int main()
{
	vector<int> arr = { 3,4,2,6,8,7,9,1,5,2 };
	MergeSort(arr);
	for (auto e : arr)
	{
		cout << e << " ";
	}
	return 0;
}