#include <iostream>
#include <vector>
using namespace std; 

//class Solution {
//public:
//    int combinationSum4(vector<int>& nums, int target) {
//        vector<int> dp(target + 1, 0);
//        //初始化
//        dp[0] = 1;
//        for (int j = 0; j <= target; ++j)
//        {
//            for (int i = 0; i < nums.size(); ++i)
//            {
//                if (j - nums[i] >= 0 && dp[j] < INT_MAX - dp[j - nums[i]]) // 这里要保证防止越界超过整数最大值
//                    dp[j] += dp[j - nums[i]];
//            }
//        }
//        return dp[target];
//    }
//};


//int main()
//{
//	int n = 3;
//	int m = 2;
//	vector<int> dp(n + 1, 0);
//	//初始化
//	dp[0] = 1;
//	for (int i = 1; i <= n; ++i) // 遍历背包
//	{
//		for (int j = 1; j <= m; ++j) // 遍历物品
//		{
//			//状态转移方程
//			if (i >= j)
//				dp[i] += dp[i - j];
//		}
//	}
//	cout << dp[n] << endl;
//	return 0;
//}


//class Solution {
//public:
//    int coinChange(vector<int>& coins, int amount) {
//        vector<int> dp(amount + 1, INT_MAX);
//        //初始化
//        dp[0] = 0;
//        for (int i = 1; i <= amount; ++i) // 遍历背包
//        {
//            for (int j = 0; j < coins.size(); ++j) // 遍历物品
//            {
//                if (i - coins[j] >= 0 && dp[i - coins[j]] != INT_MAX)
//                    dp[i] = min(dp[i], dp[i - coins[j]] + 1);
//            }
//        }
//        if (dp[amount] == INT_MAX)   return -1;
//        return dp[amount];
//    }
//};

class Solution {
public:
    int coinChange(vector<int>& coins, int amount) {
        vector<int> dp(amount + 1, INT_MAX);
        //初始化
        dp[0] = 0;
        for (int i = 0; i < coins.size(); ++i) // 遍历物品
        {
            for (int j = coins[i]; j <= amount; ++j) // 遍历背包
            {
                if (dp[j - coins[i]] != INT_MAX)
                    dp[j] = min(dp[j], dp[j - coins[i]] + 1);
            }
        }
        if (dp[amount] == INT_MAX)   return -1;
        return dp[amount];
    }
};