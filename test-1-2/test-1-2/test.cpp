#include <iostream>
#include <vector>
using namespace std;


//class Solution {
//public:
//    bool hasCycle(ListNode* head) {
//        if (head == nullptr || head->next == nullptr) return false;
//        ListNode* slow = head, * fast = head;
//        while (fast && fast->next)
//        {
//            slow = slow->next;
//            fast = fast->next->next;
//            if (slow == fast) return true;
//        }
//        return false; //这里说明快指针已经走完了，最后走到了空
//    }
//};


//class Solution {
//public:
//    ListNode* addTwoNumbers(ListNode* l1, ListNode* l2) {
//        //创建一个新的节点
//        ListNode* guard = new ListNode(0); //哨兵位头节点
//        ListNode* tail = guard;
//        ListNode* head1 = l1, * head2 = l2;
//        int step = 0; //进位
//        while (head1 && head2)
//        {
//            int tmp = head1->val + head2->val + step;
//            step = 0;
//            if (tmp >= 10)
//            {
//                step = tmp / 10;
//                tmp %= 10;
//            }
//            tail->next = new ListNode(tmp);
//            head1 = head1->next;
//            head2 = head2->next;
//            tail = tail->next;
//        }
//        ListNode* ret = head1 == nullptr ? head2 : head1;
//        while (ret)
//        {
//            int tmp = ret->val + step;
//            step = 0;
//            if (tmp >= 10)
//            {
//                step = tmp / 10;
//                tmp %= 10;
//            }
//            tail->next = new ListNode(tmp);
//            ret = ret->next;
//            tail = tail->next;
//        }
//        //这里还需要判断最后一次
//        if (step == 1) tail->next = new ListNode(1);
//        ListNode* head = guard->next;
//        delete guard;
//        return head;
//    }
//};



//class Solution {
//public:
//    ListNode* addTwoNumbers(ListNode* l1, ListNode* l2) {
//        ListNode* guard = new ListNode(0);
//        ListNode* tail = guard;
//        int carry = 0; //进位
//        while (l1 || l2)
//        {
//            int n1 = l1 ? l1->val : 0;
//            int n2 = l2 ? l2->val : 0;
//            int res = n1 + n2 + carry;
//            tail->next = new ListNode(res % 10);
//            carry = res / 10;
//            tail = tail->next;
//            if (l1) l1 = l1->next;
//            if (l2) l2 = l2->next;
//        }
//        //处理最后一个位置 
//        if (carry > 0) tail->next = new ListNode(1);
//        ListNode* head = guard->next;
//        delete guard; //防止内存泄漏
//        return head;
//    }
//};


//class Solution {
//public:
//    int maxDepth(TreeNode* root) {
//        //递归
//        if (root == nullptr) return 0;
//        return max(maxDepth(root->left), maxDepth(root->right)) + 1;
//    }
//};


//class Solution {
//public:
//    int maxDepth(TreeNode* root) {
//        //层序遍历
//        if (root == nullptr) return 0;
//        int count = 0;
//        queue<TreeNode*> q;
//        q.push(root);
//        while (q.size())
//        {
//            ++count;
//            int sz = q.size();
//            for (int i = 0; i < sz; ++i)
//            {
//                TreeNode* node = q.front();
//                q.pop();
//                if (node->left) q.push(node->left);
//                if (node->right) q.push(node->right);
//            }
//        }
//        return count;
//    }
//};