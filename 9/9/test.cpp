#include <iostream>
#include <unordered_map>
#include <vector>
using namespace std;

//int main() {
//    int n, m, t;
//    cin >> n >> m >> t;
//    //判断类型1使用hash，类型2，3使用并查集
//    vector<int> ufs(n, -1);
//    auto findRoot = [&ufs](int x) {
//        while (ufs[x] > 0)
//        {
//            x = ufs[x];
//        }
//        return x;
//    };
//    //合并两个并查集
//    auto MergeUfs = [&](int x1, int x2) {
//        int root1 = findRoot(x1);
//        int root2 = findRoot(x2);
//        if (root1 != root2)
//        {
//            ufs[root1] += ufs[root2];
//            ufs[root2] = root1;
//        }
//    };
//    //vector<vector<bool>> vetrex(n,vector<bool>(n,false));    //建立顶点与顶点之间的直接连接关系   
//    unordered_map<int, vector<int>> hash;
//    for (int i = 0; i < m; ++i)
//    {
//        int first, second, third;
//        cin >> first >> second >> third;
//        // vetrex[first][second] = true;
//        // vetrex[second][first] = true;
//        // vetrex[first][third] = true;
//        // vetrex[third][first] = true;
//        // vetrex[third][second] = true;
//        // vetrex[second][third] = true;
//        hash[first].push_back(second);
//        hash[second].push_back(first);
//        hash[first].push_back(third);
//        hash[third].push_back(first);
//        hash[second].push_back(third);
//        hash[third].push_back(second);
//        //放入并查集
//        MergeUfs(first, second);
//        MergeUfs(first, third);
//        MergeUfs(second, third);
//    }
//    //判断类型
//    for (int i = 0; i < t; ++i)
//    {
//        int num1, num2;
//        cin >> num1 >> num2;
//        bool flag = true;
//        for (auto& e : hash[num1])
//        {
//            if (e == num2)
//            {
//                cout << "1" << endl;
//                flag = false;
//                break;
//            }
//        }
//        // if(vetrex[num1][num2] == true || vetrex[num2][num1] == true) cout << "1" << endl;
//        // else
//        // {
//        //     //在并查集中找
//        //     int rootNum1 = findRoot(num1);
//        //     int rootNum2 = findRoot(num2);
//        //     if(rootNum1 == rootNum2) cout << "2" << endl;
//        //     else cout << "3" << endl;
//        // }
//        if (flag)
//        {
//            //在并查集中找
//            int rootNum1 = findRoot(num1);
//            int rootNum2 = findRoot(num2);
//            if (rootNum1 == rootNum2) cout << "2" << endl;
//            else cout << "3" << endl;
//        }
//    }
//    return 0;
//}



