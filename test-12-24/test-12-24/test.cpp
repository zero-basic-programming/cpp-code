#include <iostream>
#include <vector>
#include <thread>
using namespace std;


//class Solution {
//    int memo[201][201];
//public:
//    int getMoneyAmount(int n) {
//        //vector<vector<int>> memo(n+1,vector<int>(n+1,-1));
//        return dfs(1, n);
//    }
//    int dfs(int left, int right)
//    {
//        if (left >= right) return 0;
//        if (memo[left][right] != 0) return memo[left][right];
//        //返回
//        int ret = INT_MAX;
//        for (int i = left; i <= right; ++i)
//        {
//            //[left,i-1] [i+1,right]
//            int leftval = dfs(left, i - 1);
//            int rightval = dfs(i + 1, right);
//            ret = min(ret, max(leftval, rightval) + i);
//        }
//        memo[left][right] = ret;
//        return ret;
//    }
//};
//
//
//class Solution {
//    int m, n;
//    int memo[201][201];
//    int dx[4] = { 0,0,1,-1 };
//    int dy[4] = { 1,-1,0,0 };
//public:
//    int longestIncreasingPath(vector<vector<int>>& matrix) {
//        m = matrix.size(), n = matrix[0].size();
//        int ret = 0;
//        for (int i = 0; i < m; ++i)
//        {
//            for (int j = 0; j < n; ++j)
//            {
//                int res = dfs(i, j, matrix);
//                ret = max(ret, res);
//            }
//        }
//        return ret;
//    }
//    int dfs(int i, int j, vector<vector<int>>& matrix)
//    {
//        if (memo[i][j] != 0) return memo[i][j];
//        int ret = 1;
//        for (int k = 0; k < 4; ++k)
//        {
//            int x = i + dx[k], y = j + dy[k];
//            if (x >= 0 && x < m && y >= 0 && y<n && matrix[x][y] > matrix[i][j])
//            {
//                int res = dfs(x, y, matrix);
//                memo[x][y] = res;
//                ret = max(ret, res + 1);
//            }
//        }
//        return ret;
//    }
//};
//
//
//class Solution {
//    int m, n;
//    int memo[201][201];
//    int dx[4] = { 0,0,1,-1 };
//    int dy[4] = { 1,-1,0,0 };
//public:
//    int longestIncreasingPath(vector<vector<int>>& matrix) {
//        m = matrix.size(), n = matrix[0].size();
//        int ret = 0;
//        for (int i = 0; i < m; ++i)
//        {
//            for (int j = 0; j < n; ++j)
//            {
//                ret = max(ret, dfs(i, j, matrix));
//            }
//        }
//        return ret;
//    }
//    int dfs(int i, int j, vector<vector<int>>& matrix)
//    {
//        if (memo[i][j] != 0) return memo[i][j];
//        int ret = 1;
//        for (int k = 0; k < 4; ++k)
//        {
//            int x = i + dx[k], y = j + dy[k];
//            if (x >= 0 && x < m && y >= 0 && y<n && matrix[x][y] > matrix[i][j])
//            {
//                ret = max(ret, dfs(x, y, matrix) + 1);
//            }
//        }
//        memo[i][j] = ret;
//        return ret;
//    }
//};

//void threadFunc1(int a)
//{
//    std::cout << a << std::endl;
//}
//
//class ThreadFunc
//{
//public:
//    void operator()()
//    {
//        cout << "func2" << endl;
//    }
//};
//
//
//int main()
//{
//    //使用函数指针的方式调用线程
//    thread t1(threadFunc1, 10);
//    //使用仿函数
//    ThreadFunc tf;
//    thread t2(tf);
//    //使用lambda表达式
//    thread t3([]
//        {
//            cout << "thread3" << endl;
//        });
//    t1.join();
//    t2.join();
//    t3.join();
//    cout << "main thread" << endl;
//    return 0;
//}


