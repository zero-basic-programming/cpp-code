#include <iostream>
using namespace std;
//int main()
//{
//    int n;
//    cin >> n;
//    int i = 0;
//    int count = 0;//记录长度
//    int max = 0;//记录最长
//    while (i < 32)
//    {
//        if ((n >> i) & 1)
//        {
//            ++count;
//        }
//        else {
//            max = max > count ? max : count;
//            count = 0;
//        }
//        ++i;
//    }
//    cout << max << endl;
//    return 0;
//}

//class LCA {
//public:
//    int getLCA(int a, int b) {
//        // write code here
//        //这就是堆
//        int big = a;
//        int small = b;
//        if (big < small)
//        {
//            big = b;
//            small = a;
//        }
//        int result = 0;
//        while (big > 0)
//        {
//            //判断结果
//            if (small == big)
//            {
//                result = big;
//                break;
//            }
//            big /= 2;
//            //判断big和small大小
//            if (big < small)
//            {
//                int tmp = big;
//                big = small;
//                small = tmp;
//            }
//        }
//        return result;
//    }
//};

//#include <vector>
//class Solution {
//public:
//    int FirstNotRepeatingChar(string str) {
//        //哈希数组
//        int len = str.size();
//        vector<int> hash;
//        hash.resize(128, 0);
//        for (int i = 0; i < len; ++i)
//        {
//            hash[str[i]]++;
//        }
//        //遍历hash找第一个
//        for (int i = 0; i < hash.size(); ++i)
//        {
//            if (hash[str[i]] == 1)
//            {
//                return i;
//            }
//        }
//        return  -1;
//    }
//};

//class Solution {
//public:
//    bool isUnique(string str) {
//        //哈希数组
//        vector<int> hash;
//        hash.resize(26, 0);
//        for (int i = 0; i < str.size(); ++i)
//        {
//            hash[str[i] - 'a']++;
//        }
//        //遍历hash找是否都为1
//        for (int i = 0; i < str.size(); ++i)
//        {
//            if (hash[str[i] - 'a'] != 1)
//            {
//                return false;
//            }
//        }
//        return true;
//    }
//};

//class Solution {
//public:
//    ListNode* rotateRight(ListNode* head, int k) {
//        if (head == nullptr || k == 0)
//        {
//            return head;
//        }
//        vector<int> v;
//        int len = 0;//记录链表长度
//        ListNode* cur = head;
//        while (cur)
//        {
//            ++len;
//            v.push_back(cur->val);
//            cur = cur->next;
//        }
//        k %= len;
//        int n = v.size();
//        //转化为数组的逆序
//        reverse(v.begin(), v.begin() + n - k);
//        for (auto& e : v)
//        {
//            cout << e << " ";
//        }
//        reverse(v.begin() + n - k, v.end());
//        reverse(v.begin(), v.end());
//        cur = head;
//        for (int i = 0; i < v.size(); ++i)
//        {
//            cur->val = v[i];
//            cur = cur->next;
//        }
//        return head;
//    }
//};