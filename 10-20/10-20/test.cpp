#include <iostream>
using namespace std;

//int main() {
//    int T;
//    cin >> T;
//    while (T--)
//    {
//        int n, k;
//        cin >> n >> k;
//        int ret = 0, sum = 0;   //sum是外面的鸡的个数，ret是总叫声
//        while (n >= k)
//        {
//            sum += k;
//            ret += sum;
//            n -= k;
//        }
//        cout << ret << endl;
//    }
//}


//int main() {
//    int T;
//    cin >> T;
//    while (T--)
//    {
//        int n, k;
//        cin >> n >> k;
//        int d = n / k;  //次数
//        long long ret = d * k + d * (d - 1) / 2 * k;
//        cout << ret << endl;
//    }
//}


//int main() {
//    //等价于找所有元素和最大或者最小元素的最大公因数
//    int T;
//    cin >> T;
//    while (T--)
//    {
//        int n;
//        cin >> n;
//        vector<int> nums(n);
//        for (auto& e : nums) cin >> e;
//        sort(nums.begin(), nums.end());
//        int maxval = nums.back();
//        int gap = maxval - nums[0];
//        for (int i = 1; i < n - 1; ++i)
//        {
//            int val = maxval - nums[i];
//            if (gap % val == 0 || val % gap == 0) gap = min(val, gap);
//            else gap = 1;
//            if (gap == 1) break;
//        }
//        cout << gap << endl;
//    }
//}


//int main() {
//    int n, m, k, q;
//    cin >> n >> m >> k >> q;
//    vector<vector<int>> map(m, vector<int>(n));
//    unordered_map<int, pair<int, int>> hash;   //用来记录机器编号和位置的关系
//    int tip = 1;
//    while (k--)
//    {
//        int x, y;
//        cin >> x >> y;
//        map[x - 1][y - 1] = 1;
//        hash[tip++] = make_pair(x - 1, y - 1);
//    }
//    while (q--)
//    {
//        int num;
//        char ch;
//        cin >> num >> ch;
//        int x = hash[num].first, y = hash[num].second;
//        map[x][y] = 0;   //去掉标记
//        if (ch == 'U')
//        {
//            int i = x;
//            for (; i >= 1; --i)
//            {
//                if (map[i - 1][y] == 1) break;
//            }
//            x = i;
//            hash[num].first = x;
//        }
//        else if (ch == 'D')
//        {
//            int i = x;
//            for (; i + 1 < m; ++i)
//            {
//                if (map[i + 1][y] == 1) break;
//            }
//            x = i;
//            hash[num].first = x;
//        }
//        else if (ch == 'L')
//        {
//            int j = y;
//            for (; j >= 1; --j)
//            {
//                if (map[x][j - 1] == 1) break;
//            }
//            y = j;
//            hash[num].second = y;
//        }
//        else
//        {
//            int j = y;
//            for (; j + 1 < n; ++j)
//            {
//                if (map[x][j + 1] == 1) break;
//            }
//            y = j;
//            hash[num].second = y;
//        }
//        map[x][y] = 1;   //添加标记
//        cout << x + 1 << " " << y + 1 << endl;
//    }
//    return 0;
//}


