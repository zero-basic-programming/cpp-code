#include <iostream>
#include <vector>
using namespace std;


//class Solution {
//public:
//    vector<int> productExceptSelf(vector<int>& nums) {
//        int n = nums.size();
//        vector<int> ldp(n);
//        auto rdp = ldp;
//        //初始化
//        ldp[0] = 1;
//        rdp[n - 1] = 1;
//        for (int i = 1; i < n; ++i) ldp[i] = ldp[i - 1] * nums[i - 1];
//        for (int i = n - 2; i >= 0; --i) rdp[i] = rdp[i + 1] * nums[i + 1];
//        vector<int> ret;
//        for (int i = 0; i < n; ++i)
//        {
//            ret.push_back(ldp[i] * rdp[i]);
//        }
//        return ret;
//    }
//};

//class Solution {
//public:
//    int subarraySum(vector<int>& nums, int k) {
//        unordered_map<int, int> hash;  //用来统计和以及次数的关系
//        hash[0] = 1;  //初始化
//        int ret = 0;
//        int sum = 0;
//        for (auto e : nums)
//        {
//            sum += e;
//            if (hash.count(sum - k)) ret += hash[sum - k];
//            //统计和放入到hash中
//            hash[sum]++;
//        }
//        return ret;
//    }
//};


//class Solution {
//public:
//    int subarraysDivByK(vector<int>& nums, int k) {
//        unordered_map<int, int> hash;  //元素和 % k的余数和次数的关系
//        int sum = 0;
//        hash[0 % k] = 1;  //初始化
//        int ret = 0;
//        for (auto x : nums)
//        {
//            sum += x;
//            int res = (sum % k + k) % k;  //这里为了防止负数取模的情况
//            if (hash.count(res)) ret += hash[res];
//            hash[res]++;
//        }
//        return ret;
//    }
//};


//#include <iostream>
//#include <vector>
//#include <queue>
//using namespace std;
//
//class cmp
//{
//public:
//    bool operator()(const pair<pair<int, int>, int>& p1, const pair<pair<int, int>, int>& p2)
//    {
//        return p1.second > p2.second;
//    }
//};
//
//int main()
//{
//    int n, c;
//    cin >> n >> c;
//    vector<int> _ufs(n, -1);  //并查集
//    auto FindRoot = [&_ufs](int x) {
//        while (_ufs[x] >= 0)
//        {
//            x = _ufs[x];
//        }
//        return x;
//    };
//    auto Union = [&](int x1, int x2)
//    {
//        int root1 = FindRoot(x1);
//        int root2 = FindRoot(x2);
//        if (root1 != root2)
//        {
//            _ufs[root1] += _ufs[root2];
//            _ufs[root2] = root1; //把集合2归并到集合1中
//        }
//    };
//    //建小堆
//    priority_queue<pair<pair<int, int>, int>, vector<pair<pair<int, int>, int>>, cmp> q;
//    //无向图路的最短路径问题
//    int tmp = n;
//    while (tmp--)
//    {
//        int u, v, op;
//        cin >> u >> v >> op;
//        q.push(make_pair(make_pair(u, v), op));
//    }
//    //通过最短边开始最小路径查找
//    int count = 0; //用来统计收集的边数
//    int ret = 0;  //用来统计总的边长
//    while (true)
//    {
//        //1.取出堆顶最小元素
//        auto temp = q.top();
//        q.pop();
//        //判断该元素是否在并查集中，如果在那就放弃这条边，如果不在就添加
//        if (FindRoot(temp.first.first) == FindRoot(temp.first.second))
//        {
//            continue;
//        }
//        ++count;
//        ret += temp.second;
//        Union(temp.first.first, temp.first.second);
//        if (count == n - 1) break;  //已经满足条件
//    }
//    cout << ret * c << endl;
//    return 0;
//}


//class Solution {
//public:
//    int findMaxLength(vector<int>& nums) {
//        int n = nums.size();
//        unordered_map<int, int> hash;  //到i位置的所有元素的总和和下标i的映射关系
//        hash[0] = -1;
//        int sum = 0;
//        int ret = 0;
//        for (int i = 0; i < n; ++i)
//        {
//            sum += nums[i] == 0 ? -1 : 1;
//            if (hash.count(sum)) ret = max(ret, i - hash[sum]);
//            else hash[sum] = i;
//        }
//        return ret;
//    }
//};


//class Solution {
//public:
//    vector<vector<int>> matrixBlockSum(vector<vector<int>>& mat, int k) {
//        int m = mat.size();
//        int n = mat[0].size();
//        //前缀和数组
//        vector<vector<int>> dp(m + 1, vector<int>(n + 1));
//        for (int i = 1; i <= m; ++i)
//        {
//            for (int j = 1; j <= n; ++j)
//            {
//                dp[i][j] = dp[i - 1][j] + dp[i][j - 1] - dp[i - 1][j - 1] + mat[i - 1][j - 1];
//            }
//        }
//        vector<vector<int>> ret(m, vector<int>(n));
//        for (int i = 0; i < m; ++i)
//        {
//            for (int j = 0; j < n; ++j)
//            {
//                int x1 = max(i - k, 0) + 1;
//                int y1 = max(j - k, 0) + 1;
//                int x2 = min(i + k, m - 1) + 1;
//                int y2 = min(j + k, n - 1) + 1;
//                ret[i][j] = dp[x2][y2] - dp[x2][y1 - 1] - dp[x1 - 1][y2] + dp[x1 - 1][y1 - 1];
//            }
//        }
//        return ret;
//    }
//};


//class Solution {
//public:
//    int maxSubArray(vector<int>& nums) {
//        int n = nums.size();
//        vector<int> dp(n + 1);
//        int ret = INT_MIN; //收集结果
//        for (int i = 1; i <= n; ++i)
//        {
//            dp[i] = max(dp[i - 1] + nums[i - 1], nums[i - 1]);
//            //更新结果
//            ret = max(ret, dp[i]);
//        }
//        return ret;
//    }
//};


