#include <iostream>
#include <vector>
using namespace std;


//class Solution {
//public:
//    vector<int> countBits(int n) {
//        vector<int> ret(n + 1);
//        for (int i = 0; i <= n; ++i)
//        {
//            int count = 0;
//            int tmp = i; //为了防止i后面被改变而找不到下标
//            while (i)
//            {
//                i &= (i - 1);
//                ++count;
//            }
//            ret[tmp] = count;
//        }
//        return ret;
//    }
//};


//int main()
//{
//    Solution().countBits(2);
//    return 0;
//}
//


//class Solution {
//public:
//    int singleNumber(vector<int>& nums) {
//        int n = nums.size();
//        int ret = 0;
//        for (auto e : nums)
//        {
//            ret ^= e;
//        }
//        //在结果中找到1的那位，然后根据这两位来分成两组，然后分别异或得到结果
//        int j = 0; // 记录j下标含有1的位置
//        for (int i = 0; i < 32; ++i)
//        {
//            if ((nums[i] >> i) & 1)
//            {
//                j = i;
//                break;
//            }
//        }
//        vector<int> dp1(n); //第i为1
//        vector<int> dp2(n); //第i为0
//        for (auto e : nums)
//        {
//            if ((e >> j) & 1)
//            {
//                dp1.push_back(e);
//            }
//            else {
//                dp2.push_back(e);
//            }
//        }
//        int ret1 = 0, ret2 = 0;
//        for (auto e : dp1)
//        {
//            ret1 ^= e;
//        }
//        for (auto e : dp2)
//        {
//            ret1 ^= e;
//        }
//        //其中一个一定在原来的数组中找到
//        for (auto e : nums)
//        {
//            if (e == ret1)   return ret2;
//            else return ret1;
//        }
//        return 0;
//    }
//};


