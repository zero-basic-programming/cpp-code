#include <iostream>
#include <vector>
using namespace std;


//class Solution {
//    vector<vector<int>> ret;   //收集结果
//    vector<int> path;  //记录路径
//public:
//    void dfs(TreeNode* root, int target, int sum)
//    {
//        if (!root->left && !root->right)
//        {
//            //收集结果
//            if (sum == target)
//            {
//                ret.push_back(path);
//            }
//            return;
//        }
//        if (root->left)
//        {
//            path.push_back(root->left->val);
//            dfs(root->left, target, sum + root->left->val);
//            path.pop_back();
//        }
//        if (root->right)
//        {
//            path.push_back(root->right->val);
//            dfs(root->right, target, sum + root->right->val);
//            path.pop_back();
//        }
//    }
//    vector<vector<int>> pathTarget(TreeNode* root, int target) {
//        if (root == nullptr) return ret;
//        path.push_back(root->val);
//        dfs(root, target, root->val);
//        return ret;
//    }
//};


//class Solution {
//public:
//    int calculateDepth(TreeNode* root) {
//        if (root == nullptr) return 0;
//        int leftdeep = calculateDepth(root->left);
//        int rightdeep = calculateDepth(root->right);
//        return max(leftdeep, rightdeep) + 1;
//    }
//};


//class Solution {
//public:
//    int calculateDepth(TreeNode* root) {
//        //通过层序遍历的方式去看最大的深度
//        if (root == nullptr) return 0;
//        queue<TreeNode*> q;
//        q.push(root);
//        int deep = 0;
//        while (q.size())
//        {
//            ++deep;
//            int sz = q.size();
//            for (int i = 0; i < sz; ++i)
//            {
//                TreeNode* node = q.front();
//                q.pop();
//                if (node->left) q.push(node->left);
//                if (node->right) q.push(node->right);
//            }
//        }
//        return deep;
//    }
//};


//class Solution {
//public:
//    vector<int> sockCollocation(vector<int>& sockets) {
//        int ret = 0;
//        for (auto e : sockets) ret ^= e;
//        //找到ret中位数为1的位，然后划分成两组
//        vector<int> nums1;
//        vector<int> nums2;
//        int i = 0;
//        while (i < 32)
//        {
//            if ((ret >> i) & 1) break;
//            ++i;
//        }
//        for (auto e : sockets)
//        {
//            if ((e >> i) & 1) nums1.push_back(e);
//            else nums2.push_back(e);
//        }
//        int ret1 = 0, ret2 = 0;  //分别记录两组得到的结果
//        for (auto e : nums1) ret1 ^= e;
//        for (auto e : nums2) ret2 ^= e;
//        return { ret1, ret2 };
//    }
//};


