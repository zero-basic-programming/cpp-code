#include <iostream>
#include <vector>
using namespace std;


//class Solution {
//public:
//    ListNode* reverseList(ListNode* head)
//    {
//        if (!head || !head->next) return head;  //不需要进行反转
//        ListNode* n1 = head, * n2 = head->next, * n3 = nullptr;
//        n1->next = nullptr;
//        while (n2)
//        {
//            n3 = n2->next;
//            n2->next = n1;
//            //迭代
//            n1 = n2;
//            n2 = n3;
//        }
//        return n1;
//    }
//    void reorderList(ListNode* head) {
//        //1.统计出链表长度
//        int n = 0;
//        ListNode* cur = head;
//        while (cur)
//        {
//            ++n;
//            cur = cur->next;
//        }
//        int step = n / 2;  //需要将后面的链表反转
//        //2.把链表的后半部分进行反转
//        cur = head;
//        while (step--)
//            cur = cur->next;
//        //反转
//        ListNode* right = reverseList(cur);
//        //3.从链表左右两边开始走，走到其中一个为空的时候就结束了（这里因为链表长度的奇偶性）
//        ListNode* left = head;
//        ListNode* guard = new ListNode(0);    //用于尾插
//        ListNode* tail = guard;
//        while (left && right)
//        {
//            tail->next = left;
//            tail = tail->next;
//            left = left->next;
//            //这里如果是奇数个，会重复尾插
//            if (left)
//            {
//                tail->next = right;
//                tail = tail->next;
//                right = right->next;
//            }
//        }
//        head = guard->next;
//        delete guard;
//    }
//};



//class Solution {
//public:
//    ListNode* reverseList(ListNode* head)
//    {
//        if (!head || !head->next) return head;  //不需要进行反转
//        ListNode* n1 = head, * n2 = head->next, * n3 = nullptr;
//        n1->next = nullptr;
//        while (n2)
//        {
//            n3 = n2->next;
//            n2->next = n1;
//            //迭代
//            n1 = n2;
//            n2 = n3;
//        }
//        return n1;
//    }
//    bool isPalindrome(ListNode* head) {
//        //1.找到中间节点，进行链表反转
//        int n = 0;
//        ListNode* cur = head;
//        while (cur)
//        {
//            ++n;
//            cur = cur->next;
//        }
//        int step = n / 2;
//        cur = head;
//        while (step--) cur = cur->next;
//        ListNode* right = reverseList(cur);
//        //2.从头尾出发，判断是否为回文链表
//        ListNode* left = head;
//        while (right)
//        {
//            if (left->val != right->val) return false;
//            left = left->next;
//            right = right->next;
//        }
//        return true;
//    }
//};


//class Solution {
//public:
//    bool isValid(string s) {
//        int ptr = 0;
//        int n = s.size();
//        if (n == 0) return true;  //处理空字符串
//        else if (n % 2 != 0) return false;   //处理奇数个
//        //处理第一个字符
//        if (s[ptr] == ')' || s[ptr] == '{' || s[ptr] == ']') return false;
//        ++ptr;
//        bool flag = true;  //记录上一个是不是左括号
//        while (ptr < n)
//        {
//            if (s[ptr] == '(' || s[ptr] == '{' || s[ptr] == '[')
//            {
//                //左括号需只要保证前面那个不是左括号即可
//                if (flag) return false;  //这里说明出现两个连续的左括号
//                flag = true;
//            }
//            else
//            {
//                //右括号需要匹配对应的左括号
//                if (flag == false) return false;  //前面那个是右括号
//                flag = false;
//                if (s[ptr] == ')' && s[ptr - 1] != '(') return false;
//                else if (s[ptr] == '}' && s[ptr - 1] != '{') return false;
//                else if (s[ptr] == ']' && s[ptr - 1] != '[') return false;
//            }
//            ++ptr;
//        }
//        return true;
//    }
//};


