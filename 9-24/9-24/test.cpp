#include <iostream>
#include <vector>
using namespace std;



//class Solution {
//public:
//    int max_depth(TreeNode* root) {
//        //使用层序遍历，来确定层数
//        int level = 0;
//        if (root == nullptr) return level;
//        queue<TreeNode*> q;
//        q.push(root);
//        while (q.size())
//        {
//            int sz = q.size();
//            for (int i = 0; i < sz; ++i)
//            {
//                TreeNode* node = q.front();
//                q.pop();
//                //放入子节点
//                if (node->left) q.push(node->left);
//                if (node->right) q.push(node->right);
//            }
//            ++level;
//        }
//        return level;
//    }
//};

//class Solution {
//public:
//    double findMaxAverage(vector<int>& arr, int k) {
//        //前缀和 
//        int n = arr.size();
//        if (n < k || k <= 0) return 0.0;   //特殊情况
//        vector<int> dp(n + 1);
//        for (int i = 1; i <= n; ++i)
//        {
//            dp[i] = dp[i - 1] + arr[i - 1];
//        }
//        //收集结果
//        double ret = INT_MIN;
//        for (int i = k; i <= n; ++i)
//        {
//            double gap = dp[i] - dp[i - k];
//            ret = max(ret, gap / k);
//        }
//        return ret;
//    }
//};




//class Solution {
//public:
//    vector<vector<int> > generate_matrix(int n) {
//        int maxNum = n * n;
//        int k = 1;   //要填写的数字
//        int i = 0, j = 0, x = 0, y = 0;
//        vector<vector<int>> ret(n, vector<int>(n));
//        while (k < maxNum)
//        {
//            i = x, j = y;
//            for (; j < n - y - 1; ++j) ret[i][j] = k++;
//            for (; i < n - x - 1; ++i) ret[i][j] = k++;
//            for (; j > y; --j) ret[i][j] = k++;
//            for (; i > x; --i) ret[i][j] = k++;
//            x++;
//            y++;
//        }
//        //这里可能是单数的情况
//        if (k == maxNum) ret[x][y] = k;
//        return ret;
//    }
//};
//
//int main()
//{
//    int n = 20;
//    vector<vector<int>> ret = Solution().generate_matrix(n);
//    for (auto& a : ret)
//    {
//        for (auto e : a)
//        {
//            cout << e << " ";
//        }
//        cout << endl;
//    }
//    return 0;
//}



//#include <iostream>
//#include <vector>
//#include <queue>
//using namespace std;
//
//int dx[4] = { 0,0,1,-1 };
//int dy[4] = { 1,-1,0,0 };
//int m, n;
//
//void bfs(vector<vector<int>>& nums, vector<vector<int>>& vis, int& sum, queue<pair<int, int>>& q)
//{
//    while (q.size() && sum > 0)
//    {
//        int sz = q.size();
//        for (int i = 0; i < sz && sum>0; ++i)
//        {
//            auto p = q.front();
//            q.pop();
//            //赋值
//            nums[p.first][p.second] = 1;
//            vis[p.first][p.second] = true;
//            --sum;
//            for (int k = 0; k < 4; ++k)
//            {
//                int x = p.first + dx[k], y = p.second + dy[k];
//                if (x >= 0 && x < m && y >= 0 && y < n && !vis[x][y])
//                {
//                    q.push(make_pair(x, y));
//                }
//            }
//        }
//    }
//}
//
//int main() {
//    int k;
//    cin >> n >> m >> k;
//    int len;
//    cin >> len;
//    int sum = 0;
//    for (int i = 0; i < len; ++i)
//    {
//        int tmp;
//        cin >> tmp;
//        sum += tmp;
//    }
//    vector<vector<int>> nums(m, vector<int>(n));
//    vector<vector<int>> vis(m, vector<int>(n, false));
//    //1.找到最中间的格子，然后采用宽度优先在种
//    int i = m / 2;
//    int j = n / 2;
//    queue<pair<int, int>> q;
//    q.push(make_pair(i, j));
//    if (m % 2 == 0 && n % 2 == 0)
//    {
//        //放入4个
//        q.push(make_pair(i - 1, j));
//        q.push(make_pair(i, j - 1));
//        q.push(make_pair(i - 1, j - 1));
//    }
//    else if (m % 2 == 0 && n % 2 != 0)
//    {
//        q.push(make_pair(i + 1, j));
//    }
//    else if (m % 2 != 0 && n % 2 == 0)
//    {
//        q.push(make_pair(i, j - 1));
//    }
//    bfs(nums, vis, sum, q);
//    //2.计算出二维前缀和
//    vector<vector<int>> dp(m + 1, vector<int>(n + 1));
//    for (int i = 1; i <= m; ++i)
//    {
//        for (int j = 1; j <= n; ++j)
//        {
//            dp[i][j] = dp[i - 1][j] + dp[i][j - 1] + nums[i - 1][j - 1] - dp[i - 1][j - 1];
//        }
//    }
//    //3.统计出总和
//    int ret = 0;
//    for (int i = k; i <= m; ++i)
//    {
//        for (int j = k; j <= n; ++j)
//        {
//            ret += (dp[i][j] - dp[i - k][j] - dp[i][j - k] + dp[i - k][j - k]);
//        }
//    }
//    cout << ret << endl;
//}



