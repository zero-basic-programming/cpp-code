#include <iostream>
#include <vector>
using namespace std;


//class Solution {
//public:
//    int trainingPlan(vector<int>& actions) {
//        //和训练计划3思路一样，首先把数组中的元素全部异或，然后通过找1的位数来分成两组
//        int ret = 0;
//        for (auto e : actions) ret ^= e;
//        int i = 0;
//        while (i < 32)
//        {
//            if ((ret >> i) & 1) break;
//            ++i;
//        }
//        int group1 = 0, group2 = 0;
//        for (auto e : actions)
//        {
//            if ((e >> i) & 1) group1 ^= e;
//            else group2 ^= e;
//        }
//        return group1 == 0 ? group2 : group1;
//    }
//};


//class Solution {
//public:
//    int trainingPlan(vector<int>& actions) {
//        unordered_map<int, int> hash;   //记录元素和次数的关系
//        for (auto e : actions)   ++hash[e];
//        for (auto& p : hash)
//        {
//            if (p.second == 1) return p.first;
//        }
//        return -1;
//    }
//};


