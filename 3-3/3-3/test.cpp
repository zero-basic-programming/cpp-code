#include <iostream>
#include <vector>
using namespace std;


//class Solution {
//public:
//    bool containsNearbyDuplicate(vector<int>& nums, int k) {
//        unordered_map<int, int> hash;  //用来维护数字和下标的映射关系
//        for (int i = 0; i < nums.size(); ++i)
//        {
//            //判断前面是否有满足条件的一样的数字
//            auto it = hash.find(nums[i]);
//            if (it != hash.end())
//            {
//                if (abs(i - it->second) <= k) return true;
//            }
//            hash[nums[i]] = i;
//        }
//        return false;
//    }
//};


//class Solution {
//public:
//    ListNode* reverseK(ListNode* head, int k)
//    {
//        ListNode* n1 = head, * n2 = head->next, * n3 = nullptr;
//        while (--k)
//        {
//            n3 = n2->next;
//            n2->next = n1;
//            //迭代
//            n1 = n2;
//            n2 = n3;
//        }
//        return n1;  //返回当前的头
//    }
//    ListNode* reverseKGroup(ListNode* head, int k) {
//        //使用递归的方式
//        //返回条件
//        if (k == 1) return head;  //如果k是一个就不需要反转了
//        if (head == nullptr) return nullptr;
//        //1.找到当前k个数的头节点
//        ListNode* nexthead = head;
//        int m = k;
//        while (m--)
//        {
//            if (nexthead == nullptr)
//            {
//                //这里说明当前一组不够k个，那么就不需要反转了
//                return head;
//            }
//            nexthead = nexthead->next;
//        }
//        //2.反转k个
//        ListNode* newhead = reverseK(head, k);
//        //3.连接
//        head->next = reverseKGroup(nexthead, k);
//        return newhead;
//    }
//};



//class Solution {
//public:
//    vector<vector<int>> levelOrder(TreeNode* root) {
//        vector<vector<int>> ret;
//        if (root == nullptr) return ret;
//        queue<TreeNode*> q;
//        q.push(root);
//        while (q.size())
//        {
//            int sz = q.size();
//            vector<int> tmp;
//            //把队列中的所有元素的子节点放入到队列中
//            for (int i = 0; i < sz; ++i)
//            {
//                TreeNode* front = q.front();
//                q.pop();
//                tmp.push_back(front->val);
//                //放入其左右子树
//                if (front->left) q.push(front->left);
//                if (front->right) q.push(front->right);
//            }
//            ret.push_back(tmp);
//        }
//        return ret;
//    }
//};


//class Solution {
//public:
//    ListNode* removeNthFromEnd(ListNode* head, int n) {
//        if (head->next == nullptr)
//        {
//            delete head;
//            return nullptr;
//        }
//        ListNode* cur = head;
//        ListNode* tail = head;
//        while (n--)
//        {
//            tail = tail->next;
//        }
//        //这里有可能是删除第一个，特判一下
//        //如果tail走到最后，那么一定是头删
//        if (tail == nullptr)
//        {
//            head = head->next;
//            delete cur;
//            return head;
//        }
//        //然后两个指针同时走直到tail为空或者tail->next为空
//        while (tail && tail->next)
//        {
//            cur = cur->next;
//            tail = tail->next;
//        }
//        ListNode* next = cur->next;
//        cur->next = next->next;
//        delete next;
//        return head;
//    }
//};


//class Solution {
//public:
//    bool isSameTree(TreeNode* p, TreeNode* q) {
//        //前序遍历，看两棵树是否完全相同
//        if ((p == nullptr && q != nullptr) || (p != nullptr && q == nullptr)) return false;
//        if (p == nullptr && q == nullptr) return true;
//        if (p->val != q->val) return false;
//        return isSameTree(p->left, q->left) && isSameTree(p->right, q->right);
//    }
//};