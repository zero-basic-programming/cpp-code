#include <iostream>
#include <vector>
using namespace std;


//class Solution {
//public:
//    int numTrees(int n) {
//        //dp
//        vector<int>dp(n + 1);
//        //初始化
//        dp[0] = 1;
//        for (int i = 1; i <= n; ++i)
//        {
//            for (int j = 1; j <= i; ++j)
//            {
//                //状态转移方程
//                dp[i] += dp[j - 1] * dp[i - j];
//            }
//        }
//        return dp[n];
//    }
//};

//class Solution {
//public:
//    bool canPartition(vector<int>& nums) {
//        //求和找中位数
//        int sum = 0;
//        for (auto& e : nums)
//        {
//            sum += e;
//        }
//        //奇数一定不满足
//        if (sum % 2)
//            return false;
//        int target = sum / 2;
//        vector<int> dp(target + 1, 0);
//        //dp
//        for (int i = 0; i < nums.size(); ++i)
//        {
//            for (int j = target; j >= nums[i]; --j)
//            {
//                //状态转移方程
//                dp[j] = max(dp[j], dp[j - nums[i]] + nums[i]);
//            }
//        }
//        if (target = dp[target])
//            return true;
//        return false;
//    }
//};
//


//class Solution {
//public:
//    int lastStoneWeightII(vector<int>& stones) {
//        int sum = 0;
//        for (auto& e : stones)
//        {
//            sum += e;
//        }
//        int target = sum / 2;
//        vector<int> dp(target + 1, 0);
//        for (int i = 0; i < stones.size(); ++i)
//        {
//            for (int j = target; j >= stones[i]; --j)
//            {
//                //状态转移方程
//                dp[j] = max(dp[j], dp[j - stones[i]] + stones[i]);
//            }
//        }
//        return (sum - 2 * dp[target]);
//    }
//};


