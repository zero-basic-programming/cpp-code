#include <iostream>
#include <vector>
using namespace std;

void MergeSortNonR(vector<int>& arr, vector<int>& tmp)
{
	int n = arr.size();
	int gap = 1;
	while (gap < n)
	{
		for (int i = 0; i < n; i += 2 * gap)
		{
			int begin1 = i, end1 = i + gap - 1;
			int begin2 = i + gap, end2 = i + 2 * gap - 1;
			//判断边界条件
			if (begin2 >= n) break; //说明最后一组归并的时候没有第2组
			if (end2 >= n) end2 = n - 1;  //最后一组个数不全
			int j = i, k = i;  //j用来归并，k用来还原
			while (begin1 <= end1 && begin2 <= end2)
			{
				tmp[j++] = arr[begin1] < arr[begin2] ? arr[begin1++] : arr[begin2++];
			}
			while (begin1 <= end1)
			{
				tmp[j++] = arr[begin1++];
			}
			while (begin2 <= end2)
			{
				tmp[j++] = arr[begin2++];
			}
			//还原
			for (; k <= end2; k++)
			{
				arr[k] = tmp[k];
			}
		}
		gap *= 2;
	}
}

//冒泡排序
void BufferSort(vector<int>& arr)
{
	int n = arr.size();
	for (int i = 1; i < n; ++i)  //趟数
	{
		bool flag = true; //用于优化单趟冒泡排序
		for (int j = 0; j < n - i; ++j)
		{
			if (arr[j] > arr[j + 1])
			{
				swap(arr[j], arr[j + 1]);
				flag = false;
			}
		}
		if (flag) break; //此时已经有序
	}
}

//插入排序
void InsertSort(vector<int>& arr)
{
	int n = arr.size();
	for (int i = 0; i < n - 1; ++i)
	{
		int end = i;
		int key = arr[end + 1];
		while (end >= 0)
		{
			if (arr[end] > key)
			{
				arr[end + 1] = arr[end];  //需要把当前位置向后移动
				--end;
			}
			else break;  //这里已经找到了插入的地方
		}
		//插入
		arr[end + 1] = key;
	}
}

//选择排序
void SelectSort(vector<int>& arr)
{
	int n = arr.size();
	int left = 0, right = n - 1;
	while (left <= right)
	{
		int maxi = left, mini = left;
		for (int i = left; i <= right; ++i)
		{
			if (arr[maxi] < arr[i]) maxi = i;
			if (arr[mini] > arr[i]) mini = i;
		}
		swap(arr[left], arr[mini]);
		//这里最大的那个可能在left下标位置
		if (maxi == left) maxi = mini;
		swap(arr[maxi], arr[right]);
		--right;
		++left;
	}
}

//希尔排序
void ShellSort(vector<int>& arr)
{
	int n = arr.size();
	int gap = n;
	while (gap > 1)
	{
		gap /= 2;
		for (int i = 0; i < n - gap; ++i)
		{
			int end = i;
			int key = arr[end + gap];
			while (end >= 0)
			{
				if (arr[end] > key)
				{
					arr[end + gap] = arr[end];
					end -= gap;
				}
				else break;  //找到插入位置
			}
			//插入
			arr[end + gap] = key;
		}
	}
}

int main()
{
	vector<int> arr = { 3,5,57,8,5,6,5,3,8,9,0,3,1,2 };
	int n = arr.size();
	vector<int> tmp(n);
	//MergeSortNonR(arr, tmp);
	//BufferSort(arr);
	//InsertSort(arr);
	//SelectSort(arr);
	ShellSort(arr);
	for (auto e : arr)
	{
		cout << e << " ";
	}
	return 0;
}