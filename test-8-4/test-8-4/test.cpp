#include <iostream>
#include <vector>
using namespace std;

//int main()
//{
//	int a = 100;
//	int b = 100;
//	cout << min(a, b) << endl;
//	return 0;
//}


//class Solution {
//public:
//    int minFallingPathSum(vector<vector<int>>& matrix) {
//        int n = matrix.size();
//        vector<vector<int>> dp(n, vector<int>(n + 1, 101));
//        //初始化第一行
//        for (int i = 1; i <= n; ++i)
//        {
//            dp[0][i] = matrix[0][i - 1];
//        }
//        //填写dp表
//        for (int i = 1; i < n; ++i)
//        {
//            for (int j = 1; j <= n; ++j)
//            {
//                int minret = min(dp[i - 1][j - 1], dp[i - 1][j]);
//                if (j<n && minret > dp[i - 1][j + 1])
//                {
//                    minret = dp[i - 1][j + 1];
//                }
//                dp[i][j] = (minret + matrix[i][j - 1]);
//            }
//        }
//        //遍历最后一行，找到结果
//        int ret = INT_MAX;
//        for (int i = 1; i <= n; ++i)
//        {
//            if (ret > dp[n - 1][i])
//            {
//                ret = dp[n - 1][i];
//            }
//        }
//        return ret;
//    }
//};

//class Solution {
//public:
//    int minPathSum(vector<vector<int>>& grid) {
//        int m = grid.size();
//        int n = grid[0].size();
//        vector<int> dp(n, 0);
//        //初始化
//        dp[0] = grid[0][0];
//        for (int i = 1; i < n; ++i)
//        {
//            dp[i] += (dp[i - 1] + grid[0][i]);
//        }
//        for (int i = 1; i < m; ++i)
//        {
//            for (int j = 0; j < n; ++j)
//            {
//                if (j == 0)
//                {
//                    dp[j] += grid[i][j];
//                }
//                else dp[j] = (min(dp[j], dp[j - 1]) + grid[i][j]);
//            }
//        }
//        return dp[n - 1];
//    }
//};


