#pragma once
#include <iostream>
using namespace std;
#include <assert.h>
#include <time.h>
template <class K ,class V>
struct AVLTreeNode
{
	pair<K, V> _kv;
	AVLTreeNode* _left;
	AVLTreeNode* _right;
	AVLTreeNode* _parent;
	//平衡因子  balance factor
	int _bf;
	//构造
	AVLTreeNode(const pair<K, V>& kv)
		:_kv(kv)
		, _left(nullptr)
		, _right(nullptr)
		, _parent(nullptr)
		, _bf(0)
	{}
};

template <class K ,class V>
class AVLTree
{
	typedef AVLTreeNode<K, V> Node;
public:
	//插入，和搜索二叉树类似
	bool insert(const pair<K, V>& kv)
	{
		//第一个
		if (_root == nullptr)
		{
			_root = new Node(kv);
			_root->_parent = nullptr;
			return true;
		}
		Node* cur = _root;
		Node* parent = nullptr;
		while (cur)
		{
			if (cur->_kv.first < kv.first)
			{
				parent = cur;
				cur = cur->_right;
			}
			else if (cur->_kv.first > kv.first)
			{
				parent = cur;
				cur = cur->_left;
			}
			else
			{
				//相同的值不插入
				return false;
			}
		}
		//插入
		cur = new Node(kv);
		//链接
		if (parent->_kv.first < kv.first)
		{
			parent->_right = cur;
			cur->_parent = parent;
		}
		else
		{
			parent->_left = cur;
			cur->_parent = parent;
		}
		//判断平衡因子是否符合AVL树
		while (parent)//_root的parent是空
		{
			//插入节点在parent的左就--_bf
			//插入节点在parent的右就++_bf
			if (parent->_left == cur)
			{
				parent->_bf--;
			}
			else
			{
				parent->_bf++;
			}
			//判断平衡因子是否正确
			//如果parent的bf为0说明之前不平衡，现在平衡了
			//parent的bf为-1或者1说明parent原来是0即平衡，新增节点会改变更上面的节点的bf
			//parent的bf为-2或者2就需要赶紧调平
			if (parent->_bf == 0)
			{
				break;
			}
			else if (parent->_bf == -1 || parent->_bf == 1)
			{
				//更新上面节点的bf
				cur = parent;
				parent = parent->_parent;
			}
			else if (parent->_bf == -2 || parent->_bf == 2)
			{
				//旋转
				//parent为-2 cur为-1需要右旋
				if (parent->_bf == -2 && cur->_bf == -1)
				{
					RotateR(parent);
				}
				else if (parent->_bf == 2 && cur->_bf == 1)
				{
					RotateL(parent);
				}
				else if (parent->_bf == -2 && cur->_bf == 1)
				{
					RotateLR(parent);
				}
				else if (parent->_bf == 2 && cur->_bf == -1)
				{
					RotateRL(parent);
				}
				break;
			}
			else
			{
				//不存在，但是你有可能写错，预防错误
				assert(false);
			}
		}
		return true;
	}
	void RotateR(Node* parent)
	{
		Node* subL = parent->_left;
		Node* subLR = subL->_right;
		//建立链接关系
		parent->_left = subLR;
		if (subLR)
		{
			subLR->_parent = parent;
		}
		//更上面的父节点
		Node* ppNode = parent->_parent;
		subL->_right = parent;
		parent->_parent = subL;
		//ppNode可能为空
		if (ppNode == nullptr)
		{
			_root = subL;
			subL->_parent = nullptr;
		}
		else
		{
			if (ppNode->_left == parent)
			{
				ppNode->_left = subL;
			}
			else
			{
				ppNode->_right = subL;
			}
			subL->_parent = ppNode;
		}
		//更新平衡因子
		parent->_bf = subL->_bf = 0;
	}
	void RotateL(Node* parent)
	{
		Node* subR = parent->_right;
		Node* subRL = subR->_left;
		parent->_right = subRL;
		if (subRL)
		{
			subRL->_parent = parent;
		}
		Node* ppNode = parent->_parent;
		parent->_parent = subR;
		subR->_left = parent;
		if (_root == parent)
		{
			_root = subR;
			subR->_parent = nullptr;
		}
		else
		{
			if (ppNode->_left == parent)
			{
				ppNode->_left = subR;
			}
			else
			{
				ppNode->_right = subR;
			}
			subR->_parent = ppNode;
		}
		//更新平衡因子
		parent->_bf = subR->_bf = 0;
	}
	void RotateLR(Node* parent)
	{
		Node* subL = parent->_left;
		Node* subLR = subL->_right;
		//记录subLR的bf，在其左子树插入则parent最终bf就为1，在其右子树插入则subL最终为-1
		int bf = subLR->_bf;
		//先左旋，再右旋
		RotateL(subL);
		RotateR(parent);
		//更新平衡因子
		if (bf == 0)
		{
			//这种就是插入subLR的情况，一共3个节点，刚好平衡
			parent->_bf = 0;
			subL->_bf = 0;
			subLR->_bf = 0;
		}
		else if (bf == -1)//在subLR左子树插入
		{
			parent->_bf = 1;
			subL->_bf = 0;
			subLR->_bf = 0;
		}
		else if (bf == 1)//在subLR右子树插入
		{
			parent->_bf = 0;
			subL->_bf = -1;
			subLR->_bf = 0;
		}
		else
		{
			//不存在这种情况
			assert(false);
		}
	}
	void RotateRL(Node* parent)
	{
		Node* subR = parent->_right;
		Node* subRL = subR->_left;
		int bf = subRL->_bf;
		//在左插入subR最终bf为1，在右插入parent的bf最终为-1
		RotateR(subR);
		RotateL(parent);
		if (bf == 0)
		{
			parent->_bf = 0;
			subR->_bf = 0;
			subRL->_bf = 0;
		}
		else if (bf == 1)//在右插入
		{
			parent->_bf = -1;
			subR->_bf = 0;
			subRL->_bf = 0;
		}
		else if (bf == -1)//在左插入
		{
			parent->_bf = 0;
			subR->_bf = 1;
			subRL->_bf = 0;
		}
		else 
		{
			assert(false);
		}
	}
	void Inorder()
	{
		_Inorder(_root);
	}
	bool Isbalance()
	{
		return _Isbalance(_root);
	}
private:
	int Height(Node* _root)
	{
		if (_root == nullptr)
			return 0;
		int lh = Height(_root->_left);
		int rh = Height(_root->_right);
		return lh > rh ? lh + 1 : rh + 1;
	}
	bool _Isbalance(Node* _root)
	{
		if (_root == nullptr)
			return true;
		int lh = Height(_root->_left);
		int rh = Height(_root->_right);
		if (rh - lh != _root->_bf)
		{
			cout <<_root->_kv.first<< "平衡因子异常" << endl;
			return false;
		}
		//查看子树
		return abs(rh - lh) < 2 && _Isbalance(_root->_left) && _Isbalance(_root->_right);
	}
	void _Inorder(Node* _root)
	{
		if (_root == nullptr)
			return;
		_Inorder(_root->_left);
		cout << _root->_kv.first << endl;
		_Inorder(_root->_right);
	}
	Node* _root = nullptr;
};

void TestAVLTree1()
{
	//int a[] = { 16, 3, 7, 11, 9, 26, 18, 14, 15 };
	int a[] = { 4, 2, 6, 1, 3, 5, 15, 7, 16 , 14 };
	AVLTree<int, int> t;
	for (auto& e : a)
	{
		t.insert(make_pair(e, e));
	}
	t.Inorder();
}

void TestAVLTree2()
{
	srand(time(0));
	AVLTree<int, int> t;
	for (int i = 0; i < 100000; ++i)
	{
		int x = rand();
		t.insert(make_pair(x, x));
	}
	cout << t.Isbalance() << endl;
}