#include <iostream>
#include <vector>
using namespace std;


//class Solution {
//public:
//    int lastStoneWeight(vector<int>& stones) {
//        int n = stones.size();
//        if (n == 1) return stones[0];
//        priority_queue<int, vector<int>, less<int>> q;
//        for (auto e : stones) q.push(e);
//        while (q.size() > 1)
//        {
//            int first = q.top();
//            q.pop();
//            int second = q.top();
//            q.pop();
//            q.push(first - second);
//        }
//        return q.top();
//    }
//};


//class KthLargest {
//    //k个的小堆
//    priority_queue<int, vector<int>, greater<int>> q;
//    int _k;
//public:
//    KthLargest(int k, vector<int>& nums) {
//        _k = k;
//        for (auto e : nums)
//        {
//            q.push(e);
//            if (q.size() > _k) q.pop();
//        }
//    }
//
//    int add(int val) {
//        q.push(val);
//        if (q.size() > _k) q.pop();
//        return q.top();
//    }
//};


//class Solution {
//    class cmp
//    {
//    public:
//        bool operator()(const pair<string, int>& p1, const pair<string, int>& p2)
//        {
//            if (p1.second == p2.second)
//            {
//                //根据字典序来排
//                return p1.first > p2.first;
//            }
//            return p1.second < p2.second;
//        }
//    };
//public:
//    vector<string> topKFrequent(vector<string>& words, int k) {
//        //1.使用hash映射字符串和个数的关系
//        unordered_map<string, int> hash;
//        for (auto& str : words)
//        {
//            hash[str]++;
//        }
//        //2.把hash中的元素放入到大堆中
//        priority_queue<pair<string, int>, vector<pair<string, int>>, cmp> q;  //默认是使用大堆
//        for (auto& e : hash)
//        {
//            q.push(e);
//        }
//        //3.得到结果
//        vector<string> ret;
//        for (int i = 0; i < k; ++i)
//        {
//            ret.push_back(q.top().first);
//            q.pop();
//        }
//        return ret;
//    }
//};


//class Solution {
//    class cmp
//    {
//    public:
//        //小堆
//        bool operator()(const pair<string, int>& p1, const pair<string, int>& p2)
//        {
//            if (p1.second == p2.second)
//            {
//                //根据字典序来排
//                return p1.first < p2.first;
//            }
//            return p1.second > p2.second;
//        }
//    };
//public:
//    vector<string> topKFrequent(vector<string>& words, int k) {
//        //1.使用hash映射字符串和个数的关系
//        unordered_map<string, int> hash;
//        for (auto& str : words)
//        {
//            hash[str]++;
//        }
//        //2.把hash中的元素放入到大堆中
//        priority_queue<pair<string, int>, vector<pair<string, int>>, cmp> q;  //默认是使用大堆
//        for (auto& e : hash)
//        {
//            q.push(e);
//            if (q.size() > k) q.pop();
//        }
//        //3.得到结果
//        vector<string> ret(k);
//        for (int i = k - 1; i >= 0; --i)
//        {
//            ret[i] = q.top().first;
//            q.pop();
//        }
//        return ret;
//    }
//};


//class MedianFinder {
//    //创建一个大堆和小堆
//    //大堆存放前半部分数据，小堆存放大的数据
//    priority_queue<int> bigheap;
//    priority_queue<int, vector<int>, greater<int>> smallheap;
//public:
//    void addNum(int num) {
//        if (bigheap.size() == smallheap.size())
//        {
//            //放入大堆
//            if (bigheap.empty() || num <= bigheap.top())
//            {
//                bigheap.push(num);
//            }
//            else
//            {
//                smallheap.push(num);
//                bigheap.push(smallheap.top());
//                smallheap.pop();
//            }
//        }
//        else
//        {
//            //不相等
//            if (num <= bigheap.top())
//            {
//                bigheap.push(num);
//                smallheap.push(bigheap.top());
//                bigheap.pop();
//            }
//            else smallheap.push(num);
//        }
//    }
//
//    double findMedian() {
//        if (bigheap.size() == smallheap.size()) (bigheap.top() + smallheap.top()) / 2.0;
//        return bigheap.top();
//    }
//};


//class MedianFinder {
//    priority_queue<int> left;  //左边的数据是大堆
//    priority_queue<int, vector<int>, greater<int>> right;  //右边的数据是小堆
//public:
//    MedianFinder() {
//
//    }
//
//    void addNum(int num) {
//        if (left.size() == right.size())
//        {
//            if (left.empty() || num <= left.top())
//            {
//                //左边的数据可以比右边的数据多一个
//                left.push(num);
//            }
//            else
//            {
//                //放到右边，但是要把右边的堆顶数据放入到左边
//                right.push(num);
//                left.push(right.top());
//                right.pop();
//            }
//        }
//        else
//        {
//            if (num <= left.top())
//            {
//                //这个时候左边的数据本来就比右边的数据多一个
//                left.push(num);
//                right.push(left.top());
//                left.pop();
//            }
//            else right.push(num);
//        }
//    }
//
//    double findMedian() {
//        //相等就取平均，否则就是左边的多，就取左边堆顶元素
//        if (left.size() == right.size()) return (left.top() + right.top()) / 2.0;
//        return left.top();
//    }
//};


//class Solution {
//public:
//    int getMaxLen(vector<int>& nums) {
//        int ret = 0; //记录长度
//        int n = nums.size();
//        vector<int> f(n + 1);  //选择i下标的正数最长
//        vector<int> g(n + 1);  //选择i下标的负数最长
//        for (int i = 1; i <= n; ++i)
//        {
//            if (nums[i - 1] < 0)
//            {
//                f[i] = g[i - 1] == 0 ? 0 : g[i - 1] + 1;
//                g[i] = f[i - 1] + 1;
//            }
//            else if (nums[i - 1] > 0)
//            {
//                f[i] = f[i - 1] + 1;
//                g[i] = g[i - 1] == 0 ? 0 : g[i - 1] + 1;
//            }
//            ret = max(ret, f[i]);
//        }
//        return ret;
//    }
//};


