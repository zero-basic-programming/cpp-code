#include <iostream>
#include <vector>
using namespace std;


//class Solution {
//public:
//    vector<int> largestValues(TreeNode* root) {
//        vector<int> ret;
//        if (root == nullptr) return ret;
//        queue<TreeNode*> q;
//        q.push(root);
//        while (!q.empty())
//        {
//            int sz = q.size();
//            int maxres = INT_MIN;
//            for (int i = 0; i < sz; ++i)
//            {
//                TreeNode* node = q.front();
//                q.pop();
//                maxres = max(node->val, maxres);
//                if (node->left) q.push(node->left);
//                if (node->right) q.push(node->right);
//            }
//            ret.push_back(maxres);
//        }
//        return ret;
//    }
//};


//class Solution{
//public:
//    int lastStoneWeight(vector<int>&stones) {
//        priority_queue<int,vector<int>,less<int>> q; //建大堆
//        for (auto& e : stones)
//        {
//            q.push(e);
//        }
//        while (q.size() > 1)
//        {
//            int left = q.top();
//            q.pop();
//            int right = q.top();
//            q.pop();
//            //把结果放入到堆中
//            if (left > right) q.push(left - right);
//        }
//        //这里有可能堆中没有数据
//        return q.empty() ? 0 : q.top();
//    }
//};


//class KthLargest {
//    //建k大小的小堆
//    priority_queue<int, vector<int>, greater<int>> q;
//    int _k;
//public:
//    KthLargest(int k, vector<int>& nums) {
//        int i = 0;
//        _k = k;
//        for (auto& e : nums)
//        {
//            q.push(e);
//            if (q.size() > k) q.pop();
//        }
//    }
//
//    int add(int val) {
//        q.push(val);
//        if (q.size() > _k) q.pop();
//        return q.top();
//    }
//};


//class Solution {
//    struct cmp
//    {
//        bool operator()(const pair<string, int>& p1, const pair<string, int>& p2)
//        {
//            //根据大根堆的方式
//            if (p1.second == p2.second)
//            {
//                //次数相同，则次数大的放在前面
//                return p1.first < p2.first;
//            }
//            return p1.second > p2.second;
//        }
//    };
//public:
//    vector<string> topKFrequent(vector<string>& words, int k) {
//        //先使用hash表把数组处理一下
//        unordered_map<string, int> hash;
//        for (auto& str : words)
//        {
//            hash[str]++;
//        }
//        //使用堆进行处理
//        priority_queue<pair<string, int>, vector<pair<string, int>>, cmp> heap;
//        for (auto& e : hash)
//        {
//            heap.push(e);
//            if (heap.size() > k) heap.pop();
//        }
//        //收集结果
//        vector<string> ret(k);
//        for (int i = k - 1; i >= 0; --i)
//        {
//            ret[i] = heap.top().first;
//            heap.pop();
//        }
//        return ret;
//    }
//};