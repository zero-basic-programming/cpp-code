#include <iostream>
#include <vector>
using namespace std;


//class Solution {
//public:
//    int bestTiming(vector<int>& prices) {
//        //采用贪心的策略，对于每一个元素来说，我们都需要判定这个元素所带来的最大的利润
//        int n = prices.size();
//        if (n <= 1) return 0;   //一个也是没有利润的
//        int minprice = prices[0], profit = 0;
//        for (int i = 1; i < n; ++i)
//        {
//            if (prices[i] > minprice)
//            {
//                //存在利润
//                profit = max(profit, prices[i] - minprice);
//            }
//            else minprice = prices[i];   //为了获得最大的利润，一定要选择最小的价格开始计算
//        }
//        return profit;
//    }
//};


//void Qsort(vector<int>& arr, int l, int r)
//{
//	if (l >= r) return;
//	int key = arr[rand() % (r - l + 1) + l];
//	int left = l - 1, right = r + 1, i = l;
//	while (i < right)
//	{
//		if (arr[i] > key) swap(arr[i++], arr[++left]);
//		else if (arr[i] == key) ++i;
//		else swap(arr[i], arr[--right]);
//	}
//	Qsort(arr, l, left);
//	Qsort(arr, right, r);
//}

void mergeSort(vector<int>& arr, int left, int right, vector<int>& tmp)
{
	if (left >= right) return;
	int mid = left + (right - left) / 2;
	//[left,mid] [mid+1,right]
	mergeSort(arr, left, mid, tmp);
	mergeSort(arr, mid+1, right, tmp);
	int begin1 = left, end1 = mid;
	int begin2 = mid + 1, end2 = right;
	int i = left;
	while (begin1 <= end1 && begin2 <= end2)
	{
		tmp[i++] = arr[begin1] < arr[begin2] ? arr[begin1++] : arr[begin2++];
	}
	while (begin1 <= end1)
	{
		tmp[i++] = arr[begin1++];
	}
	while (begin2 <= end2)
	{
		tmp[i++] = arr[begin2++];
	}
	//还原会原数组
	for (int j = left; j <= right; ++j)
	{
		arr[j] = tmp[j];
	}
}


void mergeSortNonR(vector<int>& arr, vector<int>& tmp)
{
	int n = arr.size();
	int gap = 1;
	while (gap < n)
	{
		//单次归并的逻辑
		for (int i = 0; i < n; i += 2*gap)
		{
			int begin1 = i, end1 = i + gap - 1;
			int begin2 = i + gap, end2 = i + 2 * gap - 1;
			//处理边界条件
			if (begin2 >= n) break;			//这里说明最后一组比较的第2组是不存在的
			if (end2 >= n) end2 = n - 1;	//这里说明最后一组比较的第2组是不全的，不能全部比较
			int j = i, k = i;				//j 用来处理归并  k用来还原
			while (begin1 <= end1 && begin2 <= end2)
			{
				tmp[j++] = arr[begin1] < arr[begin2] ? arr[begin1++] : arr[begin2++];
			}
			while (begin1 <= end1)
			{
				tmp[j++] = arr[begin1++];
			}
			while (begin2 <= end2)
			{
				tmp[j++] = arr[begin2++];
			}
			//还原
			for (; k <= end2; ++k)
			{
				arr[k] = tmp[k];
			}
		}
		gap *= 2;
	}
}

int main()
{
	vector<int> arr = { 3,4,5,3,2,1,8,7,9,0,7,6 };
	int n = arr.size();
	vector<int> tmp(n);
	// Qsort(arr, 0, n - 1);
	// mergeSort(arr, 0, n - 1, tmp);
	mergeSortNonR(arr, tmp);
	for (auto e : arr)
	{
		cout << e << " ";
	}
	return 0;
}