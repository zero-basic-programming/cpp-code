#include <iostream>
#include <queue>
#include <vector>
using namespace std;


class TreeNode
{
public:
	int _val;
	TreeNode* _left;
	TreeNode* _right;
	TreeNode(int val) :_val(val),_left(nullptr),_right(nullptr)
	{
	}
};

class Solution
{
public:
	vector<int> Func(TreeNode* root)
	{
		queue<TreeNode*> q;
		vector<int> ret;
		if (root == nullptr) return ret;
		q.push(root);
		while (q.size())
		{
			int sz = q.size();
			for (int i = 0; i < sz; ++i)
			{
				TreeNode* node = q.front();
				q.pop();
				ret.push_back(node->_val);
				if (node->_left) q.push(node->_left);
				if (node->_right) q.push(node->_right);
			}
		}
		return ret;
	}
};

//int main()
//{
//	TreeNode* root = new TreeNode(0);
//	TreeNode* node1 = new TreeNode(1);
//	TreeNode* node2 = new TreeNode(2);
//	TreeNode* node3 = new TreeNode(3);
//	TreeNode* node4 = new TreeNode(4);
//	root->_left = node1;
//	root->_right = node2;
//	node1->_left = node3;
//	node2->_right = node4;
//	vector<int> ret = Solution().Func(root);
//	for (auto e : ret)
//	{
//		cout << e << " ";
//	}
//	return 0;
//}


//class Solution {
//public:
//	bool dfs(TreeNode* root)
//	{
//		if (root == nullptr) return true;   //当前节点可以删除
//		//查看左右节点是否可以删除
//		bool leftFlag = dfs(root->left);
//		bool rightFlag = dfs(root->right);
//		if (leftFlag) root->left = nullptr;
//		if (rightFlag) root->right = nullptr;
//		if (leftFlag && rightFlag && root->val == 0)
//		{
//			return true;    //告诉上一层已经删除了
//		}
//		return false;
//	}
//	TreeNode* pruneTree(TreeNode* root) {
//		//分成3个部分，当前节点的左右节点还有当前节点的值
//		if (dfs(root))
//			return nullptr;
//		return root;
//	}
//};


