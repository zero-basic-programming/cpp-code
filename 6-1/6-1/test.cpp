#include <iostream>
#include <vector>
using namespace std;

void Qsort(vector<int>& arr, int l, int r)
{
	if (l >= r) return;
	//选取基准值
	int key = arr[rand() % (r - l + 1) + l];
	int left = l - 1, right = r + 1, i = l;
	while (i < right)
	{
		if (arr[i] < key) swap(arr[i++], arr[++left]);
		else if (arr[i] == key) ++i;
		else swap(arr[i], arr[--right]);  //这里最后的数字交换之前，当前数字是没有进行判断的
 	}
	Qsort(arr, l, left);
	Qsort(arr, right, r);
}

void MergeSort(vector<int>& arr, int left, int right)
{
	if (left >= right) return;

}

int main()
{
	vector<int> arr = { 2,3,5,4,1,2,6,8,7,6,9,0 };
	int n = arr.size();
	Qsort(arr, 0, n - 1);
	for (auto e : arr)
	{
		cout << e << " ";
	}
	return 0;
}