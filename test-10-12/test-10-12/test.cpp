#include <iostream>
#include <vector>
using namespace std;

enum col { RED,BLACK};

template <class K,class V>
struct RBTreeNode
{
	//三叉链
	RBTreeNode<K, V>* _parent;
	RBTreeNode<K, V>* _left;
	RBTreeNode<K, V>* _right;
	pair<K, V> _kv;
	//颜色
	col _col; 
	//构造
	RBTreeNode(const pair<K, V>& kv) :_kv(kv), _col(RED),
		_parent(nullptr), _left(nullptr), _right(nullptr) {}
};

template <class K,class V>
class RBTree
{
	typedef typename RBTreeNode<K, V> Node;
public:
	RBTree():_root(nullptr){}
	pair<Node*,bool> Insert(pair<K, V>& kv)
	{
		if (_root == nullptr)
		{
			_root = new Node(kv);
			_root->_col = BLACK;
			return true;
		}
		//找到插入位置
		Node* cur = _root;
		Node* parent = nullptr;
		while (cur)
		{
			if (kv.first < cur.first)
			{
				parent = cur;
				cur = cur->_left;
			}
			else if (kv.first > cur.first)
			{
				parent = cur;
				cur = cur->_right;
			}
			else
			{
				//找到了一样的，返回错误
				return make_pair(cur, false);
			}
		}
		//插入
		cur = new Node(kv);
		if (parent->_kv.first > kv.first)
		{
			//插入到左边
			parent->_left = cur;
			cur->_parent = parent;
		}
		else
		{
			//插入到右边
			parent->_right = cur;
			cur->_parent = parent;
		}
		//对红黑树进行调整
		while (parent && parent->_col == RED)
		{
			Node* gparent = parent->_parent;
			if (gparent->_left == parent)
			{
				Node* uncle = gparent->_right;
				if (uncle && uncle == RED)  //情况1 uncle存在且为红
				{
					//更新颜色然后向上更新即可
					uncle->_col = parent->_col = BLACK;
					cur = gparent;
					parent = cur->_parent;
				}
				else //情况2+情况3，uncle不存在，或者uncle存在为黑
				{
					if (parent->_left == cur)
					{
						//右旋+变色
						RotateR(gparent);
						gparent->_col = RED;
						parent->_col = BLACK;
					}
					else
					{
						//左右双旋+变色
						RotateLR(gparent);
						cur->_col = BLACK;
						gparent->_col = RED;
					}
				}
			}
			else  //uncle是gparent的左孩子
			{
				Node* uncle = gparent->_left;
				if (parent->_left == cur)
				{
					//右左双旋+变色
					RotateRL(gparent);
					cur->_col = BLACK;
					gparent->_col = RED;
				}
				else
				{
					//左旋+变色
					RotateL(gparent);
					gparent->_col = RED;
					parent->_col = BLACK;
				}
			}
		}
		//这里可能有修改根节点的颜色
		_root->_col = BLACK;
		return make_pair(cur, true);
	}
	//左旋
	void RotateL(Node* cur)
	{
		Node* parent = cur->_parent;
		Node* subR = cur->_right;
		Node* subRL = subR->_right;
		//把RL挂到cur上
		cur->_right = subRL;
		if (subRL) subRL->_parent = cur;
		//subR和cur建立关联
		cur->_parent = subR;
		subR->_left = cur;
		//建立subR和parent的关联
		if (parent == nullptr)
		{
			//根节点
			_root = subR;
			_root->_parent = nullptr;
		}
		else
		{
			if (parent->_left == cur)
			{
				parent->_left = subR;
			}
			else
			{
				parent->_right = subR;
			}
			subR->_parent = parent;
		}
	}
	//右旋
	void RotateR(Node* cur)
	{
		Node* parent = cur->_parent;
		Node* subL = cur->_left;
		Node* subLR = subL->_right;
		//建立subLR和cur的关联
		cur->_left = subLR;
		if (subLR) subLR->_parent = cur;
		//建立cur和subL的关联
		subL->_right = cur;
		cur->_parent = subL;
		//建立parent和subL的关联
		if (parent == nullptr)
		{
			_root = subL;
			_root->_parent = nullptr;
		}
		else
		{
			if (parent->_left == cur)
			{
				parent->_left = subL;
			}
			else
			{
				parent->_right = subL;
			}
			subL->_parent = parent;
		}
	}
	//左右双旋
	void RotateLR(Node* cur)
	{
		RotateL(cur->_left);
		RotateR(cur);
	}
	//右左双旋
	void RotateRL(Node* cur)
	{
		RotateR(cur->_right);
		RotateL(cur);
	}
private:
	Node* _root; 
};