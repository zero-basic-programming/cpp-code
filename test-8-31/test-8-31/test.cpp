﻿#include <iostream>
#include <vector>
using namespace std;


//class Solution {
//public:
//    int subarraySum(vector<int>& nums, int k) {
//        int res = 0;
//        int sum = 0;
//        int n = nums.size();
//        for (int left = 0, right = 0; right < n; ++right) {
//            //进窗口
//            sum += nums[right];
//           
//            while (sum > k)
//            {
//                //出窗口
//                sum -= nums[left++];
//            }
//            if (sum == k)
//            {
//                res++;
//            }
//        }
//        return res;
//    }
//};

class Solution
{
public:
	vector<int> productExceptSelf(vector<int>& nums)
	{
		// lprod 表⽰：[0, i - 1] 区间内所有元素的乘积
		// rprod 表⽰：[i + 1, n - 1] 区间内所有元素的乘积
		int n = nums.size();
		vector<int> lprod(n + 1), rprod(n + 1);
		lprod[0] = 1, rprod[n - 1] = 1;

		// 预处理前缀积以及后缀积
		for (int i = 1; i < n; i++)
			lprod[i] = lprod[i - 1] * nums[i - 1];
		for (int i = n - 2; i >= 0; i--)
			rprod[i] = rprod[i + 1] * nums[i + 1];

		// 处理结果数组
		vector<int> ret(n);
		for (int i = 0; i < n; i++)
			ret[i] = lprod[i] * rprod[i];
		return ret;
	}
};