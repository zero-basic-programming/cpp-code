#include <iostream>
#include <vector>
using namespace std;

void Qsort(vector<int>& arr, int l, int r)
{
	if (l >= r) return;
	//找基准值
	int key = arr[rand() % (r - l + 1) + l];
	int left = l - 1, right = r + 1;
	int i = l;
	while (i < right)
	{
		if (arr[i] < key) swap(arr[i++], arr[++left]);
		else if (arr[i] == key) ++i;
		else swap(arr[i], arr[--right]);
	}
	Qsort(arr, l, left);
	Qsort(arr, right, r);
}


void MergeSortNonR(vector<int>& arr, vector<int>& tmp)
{
	int n = arr.size();
	int gap = 1;
	while (gap < n)
	{
		for (int i = 0; i < n; i += 2 * gap)
		{
			//[i,i+gap-1] [i+gap,i+2*gap-1]
			int begin1 = i, end1 = i + gap - 1;
			int begin2 = i + gap, end2 = i + 2 * gap - 1;
			int j = i, k = i; //j用于完成归并 ，k用于还原数组
			//边界条件
			if (begin2 >= n) break; //后面一组没有数组，那么这两组不需要归并
			if (end2 >= n) end2 = n - 1; //最后一组的数据不全
			//两两归并
			while (begin1 <= end1 && begin2 <= end2)
			{
				tmp[j++] = arr[begin1] < arr[begin2] ? arr[begin1++] : arr[begin2++];
			}
			//处理剩下的数据
			while (begin1 <= end1)
			{
				tmp[j++] = arr[begin1++];
			}
			while (begin2 <= end2)
			{
				tmp[j++] = arr[begin2++];
			}
			//还原回原数组 
			for (; k <= end2; ++k)
			{
				arr[k] = tmp[k];
			}
		}
		gap *= 2;
	}
}


void AdjustDown(vector<int>& arr, int parent, int n)
{
	int child = 2 * parent + 1;
	while (child < n)
	{
		if (child + 1 < n && arr[child] < arr[child + 1])
		{
			++child;
		}
		if (arr[parent] < arr[child])
		{
			swap(arr[parent], arr[child]);
			//迭代
			parent = child;
			child = 2 * parent + 1;
		}
		else break; //以成堆
	}
}

void HeapSort(vector<int>& arr)
{
	int n = arr.size();
	//排升序建大堆
	for (int i = (n - 1 - 1) / 2; i >= 0; --i)
	{
		AdjustDown(arr, i, n);
	}
	//排序，堆顶元素和最后一个元素交换，最后从堆顶开始向下调整
	for (int i = n - 1; i > 0; --i)
	{
		swap(arr[0], arr[i]);
		AdjustDown(arr, 0, i);
	}
}

int main()
{
	srand(time(nullptr));
	vector<int> arr = { 3,2,5,4,6,3,5,7,8,9,9,0,3 };
	int n = arr.size();
	vector<int> tmp(n);
	//Qsort(arr, 0, n - 1);
	//MergeSortNonR(arr, tmp);
	HeapSort(arr);
	for (auto e : arr)
	{
		cout << e << " ";
	}
	return 0;
}