#include <iostream>
#include <vector>
using namespace std;

//int main() {
//    int n;
//    cin >> n;
//    vector<int> arr(n);
//    int x, y;
//    for (auto& e : arr)
//    {
//        cin >> e;
//    }
//    cin >> x >> y;
//    //遍历数组，查看是否相邻
//    for (int i = 0; i < n; ++i)
//    {
//        if (arr[i] == x)
//        {
//            //在两侧看看有没有y
//            if ((i >= 1 && arr[i - 1] == y) || (i != n - 1 && arr[i + 1] == y))
//            {
//                cout << "Yes" << endl;
//                break;
//            }
//            cout << "No" << endl;
//            break;
//        }
//    }
//    return 0;
//}



//int main() {
//    int n;
//    cin >> n;
//    vector<pair<double, double>> price(n);
//    for (int i = 0; i < n; ++i)
//    {
//        double a, b;
//        cin >> a >> b;
//        if (b > a)
//        {
//            cout << "error" << endl;
//            return -1;
//        }
//        price.push_back(make_pair(a, b));
//    }
//    double x, y;  //x表示满多少元，y表示减去的价格
//    cin >> x >> y;
//    double discountsum = 0;  //折扣之后的总和
//    double sum = 0;  //原价总和
//    for (auto e : price)
//    {
//        discountsum += e.second;
//        sum += e.first;
//    }
//    double subprice = (sum / x) * y;  //满减的价格
//    double res = subprice < discountsum ? subprice : discountsum;
//    printf("%0.2lf", res);
//    return 0;
//}


//int main() {
//    int n;
//    cin >> n;
//    string s, t;
//    cin >> s >> t;
//    int hashs[26] = { 0 };
//    int hasht[26] = { 0 };
//    int count = 0; //用来统计最大匹配度
//    for (int i = 0; i < n; ++i)
//    {
//        if (s[i] == t[i])
//        {
//            ++count; //这里不需要进入hash中，因为这两个已经是结果了
//        }
//        else
//        {
//            hashs[s[i] - 'a']++;
//            hasht[t[i] - 'a']++;
//        }
//    }
//    //查看hash中重复的个数
//    int tmp = 0;
//    for (int i = 0; i < 26; ++i)
//    {
//        if (hashs[i] > 0 && hasht[i] > 0) ++tmp;
//        if (tmp >= 2) break;
//    }
//    cout << count + tmp << endl;
//    return 0;
//}


//int main() {
//    int n;
//    cin >> n;
//    vector<int> tree(n);
//    for (auto& e : tree)
//    {
//        cin >> e;
//    }
//    int count = 0;  //统计结果
//    int x, y;
//    while (cin >> x >> y)
//    {
//        x -= 1;
//        y -= 1;  //为了对应数组下标
//        int mul = tree[x] * tree[y];
//        int res = sqrt(mul);
//        if (res * res == mul)
//        {
//            //是完全平方数
//            count += 2;
//        }
//    }
//    cout << count << endl;
//}

