#include <iostream>
#include <vector>
using namespace std;


//class Solution{
//public:
//    ListNode * mergeKLists(vector<ListNode*>&lists) {
//        int n = lists.size();
//        return Merge(lists,0,n - 1); //利用归并排序的思想进行分治
//    }
//    ListNode * Merge(vector<ListNode*>&lists,int left,int right)
//    {
//        if (left > right) return nullptr; //区间不存在
//        if (left == right) return lists[left];
//        int mid = (left + right) >> 1;
//        //[left,mid] [mid+1,right]
//        ListNode* leftlist = Merge(lists,left,mid);
//        ListNode* rightlist = Merge(lists,mid + 1,right);
//        //合并两个有序链表
//        return MergeTwoList(leftlist,rightlist);
//    }
//    ListNode * MergeTwoList(ListNode * leftlist,ListNode * rightlist)
//    {
//        if (leftlist == nullptr) return rightlist;
//        if (rightlist == nullptr) return leftlist;
//        //合并两个有序链表
//        ListNode* guard = new ListNode(0);
//        ListNode* tail = guard;
//        while (leftlist && rightlist)
//        {
//            if (leftlist->val <= rightlist->val)
//            {
//                tail->next = leftlist;
//                tail = leftlist;
//                leftlist = leftlist->next;
//            }
//            else {
//                tail->next = rightlist;
//                tail = rightlist;
//                rightlist = rightlist->next;
//            }
//        }
//        //处理剩下没有处理完的链表
//        if (leftlist) tail->next = leftlist;
//        if (rightlist) tail->next = rightlist;
//        ListNode* head = guard->next;
//        delete guard;
//        return head;
//    }
//};


//class Solution {
//public:
//    string longestCommonPrefix(vector<string>& strs) {
//        string ret;
//        for (int j = 0; j < strs[0].size(); ++j)
//        {
//            char tmp = strs[0][j];
//            for (int i = 0; i < strs.size(); ++i)
//            {
//                if (j == strs[i].size() || tmp != strs[i][j])
//                    return ret;
//            }
//            //收集结果
//            ret += tmp;
//        }
//        return strs[0];
//    }
//};


