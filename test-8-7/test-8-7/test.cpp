#include <iostream>
#include <vector>
using namespace std;

//class Solution {
//public:
//    int minSubArrayLen(int target, vector<int>& nums) {
//        int n = nums.size();
//        if (n == 1 && nums[0] >= target) return 1;
//        if (n == 1 && nums[0] < target) return 0;
//        int left = 0;
//        int right = 1;
//        int count = INT_MAX;
//        if (nums[left] >= target || nums[right] >= target)  return 1;
//        int sum = nums[left] + nums[right];
//        while (right < n)
//        {
//            if (sum < target)
//            {
//                sum += nums[++right];
//            }
//            else
//            {
//                count = min(count, (right - left + 1));
//                if (count == 1)  break;
//                sum -= nums[left++];
//            }
//        }
//        return count;
//    }
//};
//
//
//int main()
//{
//    vector<int> tmp = { 1,4,4 };
//    Solution().minSubArrayLen(4, tmp);
//    return 0;
//}

//class Solution {
//public:
//    int minSubArrayLen(int target, vector<int>& nums) {
//        int n = nums.size();
//        int len = INT_MAX, sum = 0;
//        for (int left = 0, right = 0; right < n; ++right)
//        {
//            //进窗口
//            sum += nums[right];
//            //判断
//            while (sum >= target)
//            {
//                len = min(len, right - left + 1);
//                sum -= nums[left++];
//            }
//        }
//        //这里可能没有结果
//        return len == INT_MAX ? 0 : len;
//    }
//};


//class Solution {
//public:
//    int rob1(vector<int>& nums, int left, int right)
//    {
//        int n = nums.size();
//        //f表示偷，g表示不偷
//        vector<int> f(n);
//        auto g = f;
//        //初始化
//        f[left] = nums[left];
//        for (int i = left + 1; i <= right; ++i)
//        {
//            f[i] = g[i - 1] + nums[i];
//            g[i] = max(f[i - 1], g[i - 1]);
//        }
//        return max(f[right], g[right]);
//    }
//    int rob(vector<int>& nums) {
//        int n = nums.size();
//        if (n == 1)  return nums[0];
//        if (n == 2)  return max(nums[0], nums[1]);
//        //x表示0下标不偷，y表示偷
//        int x = rob1(nums, 1, n - 1);
//        int y = rob1(nums, 2, n - 2) + nums[0];
//        return max(x, y);
//    }
//};

//class Solution {
//public:
//    int lengthOfLongestSubstring(string s) {
//        char hash[128] = { 0 }; // 通过hash的方法来保证不重复
//        int n = s.size();
//        int len = 0;
//        for (int left = 0, right = 0; right < n; ++right)
//        {
//            //判断是否入hash
//            if (hash[s[right]] == 0)
//            {
//                ++hash[s[right]];
//                //更新len
//                len = max(len, right - left + 1);
//            }
//            else
//            {
//                //这里已经有重复的结果了
//                len = max(len, right - left);
//                //找到前面区间和right下标相同的值
//                while (s[right] != s[left])
//                {
//                    //去映射
//                    hash[s[left++]] = 0;
//                }
//                //这里是相同的值，无需去映射，直接++
//                ++left;
//            }
//        }
//        return len;
//    }
//};

