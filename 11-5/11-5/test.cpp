#include <iostream>
#include <vector>
#include <string>
using namespace std;


//命令模式
class Command
{
public:
	virtual ~Command() {}
	//需要一个执行的命令
	virtual void Execute() const = 0;
};

//具体的命令
class SimpleCommand : public Command
{
protected:
	string _str;   //对应的业务代码需求
public:
	SimpleCommand(const string& str):_str(str) {}
	void Execute() const
	{
		//简单的业务需求
		cout << "this is simple command: " << _str << endl;
	}
};

//接收者，要处理相关的业务逻辑
class Receiver
{
	//做一些共有的业务逻辑，这样复杂的请求就可以塞一个这样的指针就可以避免重复代码的编写
public:
	void DoSomething(const string& str)
	{
		cout << "now,Receiver do something for client： " << str << endl;
	}
	void DoSomethingElse(const string& str)
	{
		cout << "now,Receiver do otherthing for client： " << str << endl;
	}
};

//复杂的命令，需要通过接收者来处理部分的业务逻辑
class ComplexCommand : public Command
{
protected:
	Receiver* _receiver;
	string _a;
	string _b;
public:
	ComplexCommand(Receiver* receiver, const string& a,const string& b)
		:_receiver(receiver), _a(a), _b(b)
	{
	}
	void Execute() const
	{
		//需要根据不同的参数进行处理，同时中间可能用到接收者类的业务代码
		cout << "now solve the complexCommand: " << endl;
		_receiver->DoSomething(_a);
		_receiver->DoSomething(_b);
	}
};

//  ...  other class 保证不同的参数化下的命令不同

//一个发送者，用来处理客户的需求
//这里需要处理一个或者多个的请求
class Invoker
{
protected:
	Command* _FirstCommand;
	// ... 
	Command* _EndCommand;
public:
	~Invoker()
	{
		if (_FirstCommand) delete _FirstCommand;
		if (_EndCommand) delete _EndCommand;
	}
	//设置对应的命令
	void SetFirstCommand(Command* command)
	{
		_FirstCommand = command;
	}
	void SetEndCommand(Command* command)
	{
		_EndCommand = command;
	}
	//处理对应命令的操作
	void DoSomethingExecute()
	{
		cout << "now,Does anybody want something done before I begin?" << endl;
		if (_FirstCommand) _FirstCommand->Execute();
		//处理其他操作
		std::cout << "Invoker: doing something really important...\n";
		if (_EndCommand) _EndCommand->Execute();
		cout << "now,Does anybody want something done before I end?" << endl;
	}
};

//int main()
//{
//	Invoker* voker = new Invoker;
//	Command* simple = new SimpleCommand("hello world");
//	voker->SetFirstCommand(simple);
//	Receiver* recv = new Receiver;
//	Command* complex = new ComplexCommand(recv, "send file", "save file");
//	voker->SetEndCommand(complex);
//	voker->DoSomethingExecute();
//	return 0;
//}


