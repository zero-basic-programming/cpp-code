#include <iostream>
#include <vector>
using namespace std;


//class Solution{
//public:
//    int findSubstringInWraproundString(string s) {
//        int n = s.size();
//        vector<int> dp(n,1);
//        for (int i = 1; i < n; ++i)
//        {
//            //要么是连续的情况，要么是z到a的情况，长度为1的直接在初始化中已经处理了
//            if (s[i - 1] + 1 == s[i] || (s[i - 1] == 'z' && s[i] == 'a'))
//            {
//                dp[i] += dp[i - 1];
//            }
//        }
//        //这里是不能直接加的，因为这里有可能是有重复计数
//        int hash[26] = {0};
//        int ret = 0;
//        for (int i = 0; i < n; ++i)
//        {
//            hash[s[i] - 'a'] = max(hash[s[i] - 'a'],dp[i]);
//        }
//        for (auto e : hash) ret += e;
//        return ret;
//    }
//};



//class Solution {
//public:
//    int lengthOfLIS(vector<int>& nums) {
//        int n = nums.size();
//        vector<int> dp(n, 1);
//        int ret = 1;
//        for (int i = 1; i < n; ++i)
//        {
//            for (int j = i - 1; j >= 0; --j)
//            {
//                if (nums[j] < nums[i])
//                {
//                    dp[i] += dp[j];
//                    break;
//                }
//            }
//            ret = max(ret, dp[i]);
//        }
//        return ret;
//    }
//};


