#include <iostream>
#include <vector>
#include <stack>
using namespace std;

//快排
void Qsort(vector<int>& arr, int l, int r)
{
	if (l >= r) return;
	//找基准值
	int key = arr[rand() % (r - l + 1) + l];
	int left = l - 1, right = r + 1;
	int i = l;
	while (i < right)
	{
		if (arr[i] < key) swap(arr[i++], arr[++left]);
		else if (arr[i] == key) ++i;
		else swap(arr[i], arr[--right]);
	}
	Qsort(arr, l, left);
	Qsort(arr, right, r);
}


//快排非递归
void QsortNonR(vector<int>& arr)
{
	int n = arr.size();
	int begin = 0;
	int end = n - 1;
	stack<int> st;
	st.push(begin);
	st.push(end);
	while (st.size())
	{
		int r = st.top();
		st.pop();
		int l = st.top();
		st.pop();
		//找基准值
		int key = arr[rand() % (r - l + 1) + l];
		int left = l - 1, right = r + 1;
		int i = l;
		while (i < right)
		{
			if (arr[i] < key) swap(arr[i++], arr[++left]);
			else if (arr[i] == key) ++i;
			else swap(arr[i], arr[--right]);
		}
		//入栈
		if (l < left)
		{
			st.push(l);
			st.push(left);
		}
		if (right < r) 
		{
			st.push(right);
			st.push(r);
		}
	}
}


//归并排序
void MergeSort(vector<int>& arr, int left, int right,vector<int>& tmp)
{
	if (left >= right) return;
	//找中间值
	int mid = (left + right) >> 1;
	//[left,mid] [mid+1,right]
	MergeSort(arr, left, mid,tmp);
	MergeSort(arr, mid + 1, right, tmp);
	int begin1 = left, end1 = mid;
	int begin2 = mid + 1, end2 = right;
	int i = left;
	while (begin1 <= end1 && begin2 <= end2)
	{
		tmp[i++] = arr[begin1] < arr[begin2] ? arr[begin1++] : arr[begin2++];
	}
	while (begin1 <= end1)
	{
		tmp[i++] = arr[begin1++];
	}
	while (begin2 <= end2)
	{
		tmp[i++] = arr[begin2++];
	}
	//还原
	for (int j = left; j <= right; ++j)
		arr[j] = tmp[j];
}

//归并非递归
void MergeSortNonR(vector<int>& arr, vector<int>& tmp)
{
	int n = arr.size();
	int gap = 1;
	while (gap < n)
	{
		for (int i = 0; i < n; i += 2*gap)  //每次跳过两组，这里是两组进行归并排序
		{
			int begin1 = i, end1 = i + gap - 1;
			int begin2 = i + gap, end2 = i + 2 * gap - 1;
			//处理边界条件
			if (begin2 >= n) break;    //这里是第二组没有数据，就需要进行排序了
			if (end2 >= n) end2 = n - 1;  //最后一组数据没有满，需要把边界处理一下
			//开始归并
			int j = i, k = i;  //j用来归并，k用来还原回原数组
			while (begin1 <= end1 && begin2 <= end2)
			{
				tmp[j++] = arr[begin1] < arr[begin2] ? arr[begin1++] : arr[begin2++];
			}
			while (begin1 <= end1)
			{
				tmp[j++] = arr[begin1++];
			}
			while (begin2 <= end2)
			{
				tmp[j++] = arr[begin2++];
			}
			//还原
			for (; k <= end2; ++k)
			{
				arr[k] = tmp[k];
			}
		}
		gap *= 2;
	}
}

//排升序，建大堆
void AdjustDown(vector<int>& arr, int parent, int n)
{
	int child = 2 * parent + 1;
	while (child < n)
	{
		if (child + 1 < n && arr[child + 1] > arr[child])
		{
			++child;
		}
		if (arr[child] > arr[parent])
		{
			swap(arr[child], arr[parent]);
			//向下调整
			parent = child;
			child = 2 * parent + 1;
		}
		else break;  //已成堆
	}
}

//堆排
void HeapSort(vector<int>& arr)
{
	int n = arr.size();
	//向下调整建堆
	for (int i = (n - 1 - 1) / 2; i >= 0; --i)
	{
		AdjustDown(arr, i, n);
	}
	//交换堆顶元素和最后一个元素，然后进行向下调整
	for (int i = 1; i < n; ++i)  //这里调整n-1次
	{
		swap(arr[0], arr[n - i]);
		AdjustDown(arr, 0, n - i);
	}
}


//冒泡排序
void BufferSort(vector<int>& arr)
{
	int n = arr.size();
	for (int i = 1; i < n ; ++i)  //趟数
	{
		bool flag = true;  //判断当次冒泡是否有移动数据
		for (int j = 0; j < n - i; j++)
		{
			if (arr[j] > arr[j + 1])
			{
				flag = false;
				swap(arr[j], arr[j + 1]);
			}
		}
		if (flag) break; 
	}
}

//选择排序
void SelectSort(vector<int>& arr)
{
	int n = arr.size();
	int left = 0, right = n - 1;
	while (left < right)
	{
		int maxi = left, mini = left;
		for (int i = left; i <= right; ++i)
		{
			if (arr[i] < arr[mini]) mini = i;
			if (arr[i] > arr[maxi]) maxi = i;
		}
		swap(arr[left], arr[mini]);
		//这里maxi可能在left的下标处
		if (left == maxi) maxi = mini;
		swap(arr[right], arr[maxi]);
		--right;
		++left;
	}
}


//直接插入排序
void InsertSort(vector<int>& arr)
{
	int n = arr.size();
	for (int i = 0; i < n - 1; ++i) 
	{ 
		int end = i;
		int key = arr[end + 1];
		while (end >= 0)
		{
			if (arr[end] > key)
			{
				arr[end + 1] = arr[end];
			}
			else break;  //这里已经找到了该插入的位置
			--end;
		}
		//插入
		arr[end + 1] = key;
	}
}

//希尔排序
void ShellSort(vector<int>& arr)
{
	int n = arr.size();
	int gap = n;
	while (gap > 1)
	{
		gap /= 2;
		for (int i = 0; i < n - gap; ++i)
		{
			int end = i;
			int key = arr[end + gap];
			while (end >= 0)
			{
				if (arr[end] > key)
				{
					arr[end + gap] = arr[end];
				}
				else break;  
				end -= gap;
			}
			arr[end + gap] = key;
		}
	}
}

//计数排序
void CountSort(vector<int>& arr)
{
	int n = arr.size();
	int minnum = INT_MAX;
	int maxnum = INT_MIN;
	for (auto e : arr)
	{
		if (e < minnum) minnum = e;
		if (e > maxnum) maxnum = e;
	}
	int gap = maxnum - minnum + 1;
	vector<int> hash(gap);  //这里需要多一个空间
	for (auto e : arr)
	{
		hash[e - minnum]++;
	}
	//还原到原数组
	int j = 0;
	for (int i = 0; i < gap; ++i)
	{
		while (hash[i]--)
		{
			arr[j++] = (i + minnum);
		}
	}
}

int main()
{
	vector<int> arr = { 2,5,63,4,7,5,8,9,0,9,5,6,3,1,2 };
	int n = arr.size();
	vector<int> tmp(n);
	//Qsort(arr, 0, n - 1);
	//QsortNonR(arr);
	//MergeSort(arr, 0, n - 1, tmp);
	//MergeSortNonR(arr, tmp);
	//HeapSort(arr);
	//BufferSort(arr);
	//SelectSort(arr);
	//InsertSort(arr);
	//ShellSort(arr);
	CountSort(arr);
	for (auto e : arr)
	{
		cout << e << " ";
	}
	return 0;
}