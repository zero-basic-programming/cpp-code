#include <iostream>
#include <vector>
using namespace std;


void Qsort(vector<int>& nums, int l, int r)
{
	if (l >= r) return;
	int key = nums[(rand() % (r - l + 1)) + l];
	int left = l - 1, right = r + 1, i = l;
	while (i < right)
	{
		if (nums[i] < key) swap(nums[i++], nums[++left]);
		else if (nums[i] == key) ++i;
		else swap(nums[i], nums[--right]);
	}
	Qsort(nums, l, left);
	Qsort(nums, right, r);
}

void MergeSort(vector<int>& nums, int left, int right, vector<int>& tmp)
{
	if (left >= right) return;
	int mid = left + (right - left) / 2;
	MergeSort(nums, left, mid,tmp);
	MergeSort(nums, mid+1, right, tmp);
	//归并
	int begin1 = left, end1 = mid;
	int begin2 = mid+1, end2 = right;
	int i = left;
	while (begin1 <= end1 && begin2 <= end2)
	{
		tmp[i++] = nums[begin1] < nums[begin2] ? nums[begin1++] : nums[begin2++];
	}
	while (begin1 <= end1)
	{
		tmp[i++] = nums[begin1++];
	}
	while (begin2 <= end2)
	{
		tmp[i++] = nums[begin2++];
	}
	//还原会原数组
	for (int j = left; j <= right; ++j)
		nums[j] = tmp[j];
}


void MergeSortNonR(vector<int>& nums, vector<int>& tmp)
{
	int n = nums.size();
	int gap = 1;
	while (gap < n)
	{
		for (int i = 0; i < n; i += 2 * gap)   
		{
			int begin1 = i, end1 = i + gap - 1;
			int begin2 = i + gap, end2 = i + 2 * gap - 1;
			//处理边界条件
			if (begin2 >= n) break;    //这里是最后一组都没有，那么就无需处理
			if (end2 >= n) end2 = n - 1;   //这里是最后一组没有全，需要把边界处理了
			//单趟归并
			int j = i, k = i;   //j用来记录tmp数组下标，k用来还原数组
			while (begin1 <= end1 && begin2 <= end2)
			{
				tmp[j++] = nums[begin1] < nums[begin2] ? nums[begin1++] : nums[begin2++];
			}
			while (begin1 <= end1)
			{
				tmp[j++] = nums[begin1++];
			}
			while (begin2 <= end2)
			{
				tmp[j++] = nums[begin2++];
			}
			//还原回数组
			for (; k <= end2; ++k)
				nums[k] = tmp[k];
		}
		gap *= 2;
	}
}


//升序，建大堆
void AdjustDown(vector<int>& nums, int parent, int n)
{
	int child = 2 * parent + 1;
	while (child < n)
	{
		if (child + 1 < n && nums[child] < nums[child + 1])
		{
			++child;
		}
		if (nums[parent] < nums[child])
		{
			swap(nums[parent], nums[child]);
			//迭代
			parent = child;
			child = 2 * parent + 1;
		}
		else break;   //已成堆
	}
}


void HeapSort(vector<int>& nums)
{
	int n = nums.size();
	for (int i = (n - 1 - 1) / 2; i >= 0; --i)
	{
		AdjustDown(nums, i, n);
	}
	//交换前后
	for (int i = 1; i < n; ++i)
	{
		swap(nums[0], nums[n - i]);
		AdjustDown(nums, 0, n - i);
	}
}


void BufferSort(vector<int>& nums)
{
	int n = nums.size();
	for (int i = 1; i < n; ++i)   //趟数
	{
		bool flag = true;   //用来优化
		for (int j = 0; j < n - i; ++j)   //单趟排序
		{
			if (nums[j] > nums[j + 1])
			{
				swap(nums[j], nums[j + 1]);
				flag = false;
			}
		}
		if (flag) break;   //这里单趟没有执行过，说明这里后续不需要再排序了
	}
}


void InsertSort(vector<int>& nums)
{
	int n = nums.size();
	for (int i = 0; i < n - 1; ++i)
	{
		int end = i;
		int tmp = nums[end + 1];   //插入
		while (end >= 0)
		{
			if (nums[end] > tmp) nums[end + 1] = nums[end];
			else break;
			--end;
		}
		//插入
		nums[end + 1] = tmp;
	}
}


int main()
{
	vector<int> nums = { 1,2,6,5,4,3,8,7,9 ,12};
	int n = nums.size();
	vector<int> tmp(n);
	//Qsort(nums, 0, n - 1);
	//MergeSort(nums, 0, n - 1, tmp);
	//MergeSortNonR(nums, tmp);
	//HeapSort(nums);
	//BufferSort(nums);
	//InsertSort(nums);
	for (auto e : nums) cout << e << " ";
	return 0;
}