#pragma once
#include <iostream>
#include <vector>
using namespace std;


class UnionFindSet
{
public:
	UnionFindSet(size_t n)
	{
		_ufs.resize(n, -1);
	}
	//合并两个数
	void Union(int x1, int x2)
	{
		int root1 = FindRoot(x1);
		int root2 = FindRoot(x2);
		
		//这里为了保证树的层数没有那么多，这里采用小的联合到大的中
		if (abs(_ufs[root1]) < _ufs[root2])
			swap(root1, root2);

		_ufs[root1] += _ufs[root2];
		_ufs[root2] = root1;
	}
	//查找根
	int FindRoot(int x)
	{
		int root = x;
		while (_ufs[root] >= 0)
		{
			root = _ufs[root];
		}
		//压缩路径
		while (_ufs[x] >= 0)
		{
			int parent = _ufs[x];
			_ufs[x] = root;
			//迭代
			x = parent;
		}
		return root;
	}
	//查看两个元素是否在并查集中
	bool Inset(int x1,int x2)
	{
		//看他们的根是否相同
		return FindRoot(x1) == FindRoot(x2);
	}
	//查看当前集合的大小，也就是有多少棵树
	int SetSize()
	{
		int sz = 0;
		for (int i = 0; i < _ufs.size(); ++i)
		{
			if (_ufs[i] < 0) ++sz;
		}
		return sz;
	}

private:
	vector<int> _ufs; //使用数组来实现并查集,这里主要是树形结构，利用抽象关系
};

void testUnionFindSet()
{
	UnionFindSet ufs(10);
	ufs.Union(8, 9);
	ufs.Union(7, 8);
	ufs.Union(6, 7);
	ufs.Union(5, 6);
	ufs.Union(4, 5);

	ufs.FindRoot(6);
	ufs.FindRoot(9);
}