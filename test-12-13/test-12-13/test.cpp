#include <iostream>
#include <vector>
using namespace std;

//class Solution {
//    int m, n;
//    int ret;
//    bool check[15][15];
//public:
//    int getMaximumGold(vector<vector<int>>& grid) {
//        m = grid.size();
//        n = grid[0].size();
//        for (int i = 0; i < m; ++i)
//        {
//            for (int j = 0; j < n; ++j)
//            {
//                if (grid[i][j] != 0)
//                {
//                    check[i][j] = true;
//                    ret = max(ret, grid[i][j]);
//                    dfs(grid, i, j, grid[i][j]);
//                    check[i][j] = false;
//                }
//            }
//        }
//        return ret;
//    }
//    int dx[4] = { 0,0,1,-1 };
//    int dy[4] = { 1,-1,0,0 };
//    void dfs(vector<vector<int>>& grid, int i, int j, int count)
//    {
//        for (int k = 0; k < 4; ++k)
//        {
//            int x = i + dx[k], y = j + dy[k];
//            //判断越界，是否使用过，下一个是否为0
//            if (x >= 0 && x < m && y >= 0 && y < n && !check[x][y] && grid[x][y] != 0)
//            {
//                check[x][y] = true;
//                ret = max(ret, count + grid[x][y]);
//                dfs(grid, x, y, count + grid[x][y]);
//                check[x][y] = false;
//            }
//        }
//    }
//};


class Solution {
    int ret, count, m, n;
    bool check[21][21];
    int dx[4] = { 1,-1,0,0 };
    int dy[4] = { 0,0,-1,1 };
public:
    int uniquePathsIII(vector<vector<int>>& grid) {
        m = grid.size(), n = grid[0].size();
        //统计0的个数
        int x = 0, y = 0; //找1的下标
        for (int i = 0; i < m; ++i)
        {
            for (int j = 0; j < n; ++j)
            {
                if (grid[i][j] == 0) ++count;
                else if (grid[i][j] == 1)
                {
                    x = i; y = j;
                }
            }
        }
        check[x][y] = true;
        dfs(grid, x, y, 0);
        return ret;
    }
    void dfs(vector<vector<int>>& grid, int i, int j, int path)
    {
        //返回
        if (path == count + 1 && grid[i][j] == 2)
        {
            ++ret;
            return;
        }
        for (int k = 0; k < 4; ++k)
        {
            int x = i + dx[k], y = j + dy[k];
            //判断边界，判断是否已经找过，判断不能等于-1
            if (x >= 0 && x < m && y >= 0 && y < n && !check[x][y] && grid[x][y] != -1)
            {
                check[x][y] = true;
                dfs(grid, x, y, path + 1);
                //回溯
                check[x][y] = false;
            }
        }
    }
};