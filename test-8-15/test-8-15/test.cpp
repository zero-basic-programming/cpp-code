#include <iostream>
#include <vector>
#include <string>
using namespace std;

//class Solution {
//public:
//    vector<int> findAnagrams(string s, string p) {
//        int n = p.size();
//        vector<int> res;
//        int hash[26] = { 0 };
//        for (auto ch : p) hash[ch - 'a']++;
//        int len = 0;
//        for (int left = 0, right = 0; right < s.size(); ++right)
//        {
//            //进窗口
//            hash[s[right] - 'a']--;
//            ++len;
//            int flag = 0;
//            if (len == n)
//            {
//                for (auto e : hash)
//                {
//                    if (e != 0)
//                    {
//                        flag = 1;
//                        break;
//                    }
//                }
//                //收集结果
//                if (flag == 0)
//                {
//                    res.push_back(left);
//                }
//                hash[s[left++] - 'a']++;
//                --len;
//            }
//        }
//        return res;
//    }
//};


//class Solution {
//public:
//    int maxSubarraySumCircular(vector<int>& nums) {
//        int n = nums.size();
//        //f表示求出非环形数组中最大和，g求出最小和
//        int sum = 0;
//        if (n == 1) return nums[0];
//        for (auto e : nums) sum += e;
//        vector<int> f(n, 0);
//        auto g = f;
//        //初始化
//        f[0] = g[0] = nums[0];
//        int maxres = INT_MIN, minres = INT_MAX;
//        for (int i = 1; i < n; ++i)
//        {
//            f[i] = max(nums[i], f[i - 1] + nums[i]);
//            g[i] = min(nums[i], g[i - 1] + nums[i]);
//            maxres = max(maxres, f[i]);
//            minres = min(minres, g[i]);
//        }
//        return minres == sum ? maxres : max(sum - minres, maxres);
//    }
//};


