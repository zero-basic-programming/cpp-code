#include <iostream>
#include <vector>
using namespace std;


//class Solution {
//public:
//    vector<vector<int>> fileCombination(int target) {
//        vector<vector<int>> ret;
//        int left = 1, right = 1;
//        int sum = 0;
//        while (right < target)
//        {
//            //进窗口
//            sum += right;
//            //判断
//            while (target < sum)
//            {
//                //出窗口
//                sum -= left;
//                ++left;
//            }
//            //收集结果
//            if (target == sum)
//            {
//                vector<int> tmp;
//                for (int k = left; k <= right; ++k) tmp.push_back(k);
//                ret.push_back(tmp);
//            }
//            ++right;
//        }
//        return ret;
//    }
//};

//class Solution {
//public:
//    int crackNumber(int ciphertext) {
//        //转换成字符串进行处理
//        string str = to_string(ciphertext);
//        int n = str.size();
//        vector<int> dp(n + 1);
//        //初始化
//        dp[0] = 1;
//        //状态转移
//        for (int i = 1; i < n; ++i)
//        {
//            dp[i] = dp[i - 1];
//            //判断当前字符能否和上一个字符进行组合
//            if (i > 1 && str[i - 2] != '0')
//            {
//                int tmp = stoi(str.substr(i, 2));
//                if (10 <= tmp && tmp <= 25) dp[i] += dp[i - 2];
//            }
//        }
//        //输出结果
//        return dp[n];
//    }
//};
