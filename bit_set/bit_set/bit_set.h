#pragma once
#include <iostream>
#include <vector>
using namespace std;

namespace liang
{
	template <size_t N>
	class bit_set
	{
	public:
		bit_set()
		{
			//_bit.resize((N >> 3) + 1, 0);
			_bit.resize(N/8 + 1, 0);
		}
		void set(size_t x)
		{
			size_t i = x >> 3;//等价于N/8
			size_t j = x % 8;
			//把那个bit置为1
			_bit[i] |= (1 << j);
		}
		void reset(size_t x)
		{
			size_t i = x >> 3;//等价于N/8
			size_t j = x % 8;
			//把除了那个bit都保持不变
			_bit[i] &= (~(1 << j));
		}
		bool test(size_t x)
		{
			size_t i = x >> 3;//等价于N/8
			size_t j = x % 8;
			return _bit[i] & (1 << j);
		}
	private:
		vector<char> _bit;
	};
	void test_bit_set()
	{
		/*bit_set<100> bs1;
		bit_set<1000> bs2;
		bit_set<10000> bs3;*/
		bit_set<0xffffffff> bs;
		//bit_set<((size_t)-1)> bs;
		bs.set(1024);
		bs.set(10);
		bs.set(104);
		bs.set(102401);

		cout << bs.test(10) << endl;

		bs.reset(10);
		cout << bs.test(10) << endl;

	}
}