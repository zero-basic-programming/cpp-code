#include <iostream>
#include <vector>
#include <list>
using namespace std;


//组合模式

//基类对象，零件类
class Component
{
protected:
	Component* _parent;   //父对象
public:
	//设置父对象
	void SetParent(Component* parent)
	{
		_parent = parent;
	}
	//获取父对象
	Component* GetParent()
	{
		return _parent;
	}
	//如果是复杂对象，有加入节点和删除节点操作
	virtual void Add(Component* component) {}
	virtual void Remove(Component* component) {}
	//是否是一个复杂对象
	virtual bool IsComposite()
	{
		return false;  //一般来说都是创建叶子节点，不是复杂对象
	}
	//具体的业务操作
	virtual string Operator() const = 0;
};


//叶子节点类
class Leaf : public Component
{
public:
	string Operator() const
	{
		return "Leaf";
	}
};



//复杂类对象
class Composite : public Component
{
protected:
	list<Component*> _children;    //获取其子类的各个节点
public:
	//对子类节点的插入删除
	void Add(Component* component)
	{
		_children.push_back(component);
		component->SetParent(this);
	}
	void Remove(Component* component)
	{
		_children.remove(component);
		component->SetParent(nullptr);
	}
	//是一个复杂对象
	bool IsComposite()
	{
		return true;
	}
	//实际业务逻辑
	string Operator() const
	{
		//遍历所有的子节点，来找到不同的输出
		string ret;
		for (auto& node : _children)
		{
			if (node == _children.back())
			{
				ret += node->Operator();
			}
			else
			{
				ret += node->Operator() + "+";
			}
		}
		return "Branch(" + ret + ")";
	}
};


void ClientCode(Component* component)
{
	cout << component->Operator();
}

void ClientCode1(Component* c1, Component* c2)
{
	//约定C1是复杂对象，C2是叶子
	if (c1->IsComposite())
	{
		c1->Add(c2);
	}
	cout << c1->Operator();
}

//int main()
//{
//	Component* lf = new Leaf;
//	cout << "now test leaf only:" << endl;
//	ClientCode(lf);
//
//	cout << "\n\n";
//
//	//test composite
//	Component* tree = new Composite;
//	Component* p1 = new Composite;
//	Component* p2 = new Composite;
//	Component* l1 = new Leaf;
//	Component* l2 = new Leaf;
//	Component* l3 = new Leaf;
//	p1->Add(l1);
//	p1->Add(l2);
//	p2->Add(l3);
//	tree->Add(p1);
//	tree->Add(p2);
//	cout << "now test composite:" << endl;
//	ClientCode(tree);
//
//	cout << "\n\n"; 
//
//	//test insert new leaf
//	ClientCode1(tree, lf);
//	return 0;
//}



//观察者模式

//订阅者
class IObserver
{
public:
	//析构函数
	virtual ~IObserver(){}
	//更新数据，打印
	virtual void Update(const string& message_from_subject) = 0;
};

//发布者
class ISubject
{
public:
	//析构函数
	virtual ~ISubject() {}
	//添加订阅者
	virtual void AddObserver(IObserver* observer) = 0;
	//删除订阅者
	virtual void DelObserver(IObserver* observer) = 0;
	//通知订阅者
	virtual void Nodify() = 0;
};


//具体发布者     --该版本不能做到随机添加
class Subject : public ISubject
{
public:
	~Subject() override
	{
		//打印即可
		cout << "I am Subject,quit now!" << endl;
	}
	void AddObserver(IObserver* observer) override
	{
		_observers.push_back(observer);
	}
	void DelObserver(IObserver* observer) override
	{
		_observers.remove(observer);
	}
	void Nodify() override
	{
		//遍历链表拿到对应的订阅者，然后更新他们的数据即可
		for (auto& obs : _observers)
		{
			obs->Update(_message);
		}
	}
	//发布者添加对应的数据
	void CreateMessage(string& str)
	{
		_message = str;
		//通知对应的订阅者
		Nodify();
	}
private:
	string _message;					//发布者要发布的数据
	list<IObserver*> _observers;		//对应的订阅者链表
};

//具体订阅者
class Observer : public IObserver
{
public:
	Observer(Subject& subject) : _subject(subject)
	{
		//添加当前订阅者到发布者链表
		_subject.AddObserver(this);
		cout << "I am Observer,my number is: " << ++_static_number << endl;
		_number = _static_number;
	}
	~Observer() override
	{
		RemoveCurObsFromList();
		cout << "I am Observer,quit now! number is: " << _number << endl;
	}
	void Update(const string& message_from_subject) override
	{
		_message_from_subject = message_from_subject;
		PrintMsg();
	}
	//打印出发送过来的数据
	void PrintMsg()
	{
		cout << "subject send message: " << _message_from_subject << endl;
	}
	//删除当前的发布者
	void RemoveCurObsFromList()
	{
		_subject.DelObserver(this);
		cout << "the number: " << _number << " remove from list" << endl;
	}
private:
	Subject& _subject;				//发布者对象
	string _message_from_subject;	//发布者发送过来的数据
	int _number = 0;				//订阅者的编号
	static int _static_number;		//订阅者总数
};

int Observer::_static_number = 0;

void ClientCode()
{
	Subject* sub = new Subject();
	Observer* obs1 = new Observer(*sub);
	Observer* obs2 = new Observer(*sub);
	Observer* obs3 = new Observer(*sub);
	Observer* obs4;
	Observer* obs5;

	//发送数据
	string sendMsg = "hello world";
	sub->CreateMessage(sendMsg);

	obs3->RemoveCurObsFromList();
	sendMsg = "replace send";
	sub->CreateMessage(sendMsg);

	obs4 = new Observer(*sub);
	obs5 = new Observer(*sub);
	sendMsg = "last send";
	sub->CreateMessage(sendMsg);

	//释放空间
	delete obs1;
	delete obs2;
	delete obs3;
	delete obs4;
	delete obs5;
	delete sub;
}

//int main()
//{
//	ClientCode();
//	return 0;
//}


//外观模式

//多个子系统
class SubSystem1
{
public:
	string Operator1()
	{
		return "begin";
	}
	string Operator2()
	{
		return "end";
	}
	//......
};


class SubSystem2
{
public:
	string OperatorA()
	{
		return "first";
	}
	string OperatorZ()
	{
		return "last";
	}
};


//通过统一的外观方式对这些子系统进行封装，这样外面客户端在使用的时候时候不需要注意子系统的存在
class Facade
{
protected:
	SubSystem1* _sys1;
	SubSystem2* _sys2;
public:
	Facade(SubSystem1* sys1 = nullptr, SubSystem2* sys2 = nullptr)
	{
		_sys1 = sys1 == nullptr ? new SubSystem1 : sys1;
		_sys2 = sys2 == nullptr ? new SubSystem2 : sys2;
	}
	~Facade()
	{
		if (_sys1) delete _sys1;
		if (_sys2) delete _sys2;
	}
	string Operator()
	{
		//把多个子系统的操作放在一个统一的接口
		string result;
		result += _sys1->Operator2();
		result += " + ";
		result += _sys2->OperatorA();
		result += " + ";
		result += _sys2->OperatorZ();
		result += " + ";
		result += _sys1->Operator1();
		return result;
	}
};


void ClientCode2(Facade* fa)
{
	cout << fa->Operator();
}


int main()
{
	SubSystem1* sys1 = new SubSystem1;
	SubSystem2* sys2 = new SubSystem2;
	Facade* fa = new Facade(sys1, sys2);
	ClientCode2(fa);
	return 0;
}