#include <iostream>
#include <vector>
#include <cstring>
using namespace std;

//const int N = 1010;
//int n, V;
//int v[N],w[N];
//int dp[N][N];
//
//int main()
//{
//    cin >> n >> V;
//    for (int i = 1; i <= n; ++i)
//    {
//        cin >> v[i] >> w[i];
//    }
//    //处理第一问
//    for (int i = 1; i <= n; ++i)
//    {
//        for (int j = 0; j <= V; ++j)
//        {
//            dp[i][j] = dp[i - 1][j];  //不选
//            if (j >= v[i])
//                dp[i][j] = max(dp[i][j], dp[i][j - v[i]] + w[i]);
//        }
//    }
//    cout << dp[n][V] << endl;
//    //处理第二问
//    memset(dp, 0, sizeof(dp));
//    //初始化
//    for (int j = 1; j <= V; ++j) dp[0][j] = -1;
//    for (int i = 1; i <= n; ++i)
//    {
//        for (int j = 0; j <= V; ++j)
//        {
//            dp[i][j] = dp[i - 1][j]; // 不选
//            if (j >= v[i] && dp[i][j - v[i]] != -1)
//                dp[i][j] = max(dp[i][j], dp[i][j - v[i]] + w[i]);
//        }
//    }
//    cout << (dp[n][V] == -1 ? 0 : dp[n][V]) << endl;
//    return 0;
//}


//滚动数组优化
//const int N = 1010;
//int n, V;
//int v[N], w[N];
//int dp[N];
//
//int main()
//{
//    cin >> n >> V;
//    for (int i = 1; i <= n; ++i)
//    {
//        cin >> v[i] >> w[i];
//    }
//    //处理第一问
//    for (int i = 1; i <= n; ++i)
//    {
//        for (int j = 0; j <= V; ++j)
//        {
//            if (j >= v[i])
//                dp[j] = max(dp[j], dp[j - v[i]] + w[i]);
//        }
//    }
//    cout << dp[V] << endl;
//    //处理第二问
//    memset(dp, 0, sizeof(dp));
//    //初始化
//    for (int j = 1; j <= V; ++j) dp[j] = -1;
//    for (int i = 1; i <= n; ++i)
//    {
//        for (int j = 0; j <= V; ++j)
//        {
//            if (j >= v[i] && dp[j - v[i]] != -1)
//                dp[j] = max(dp[j], dp[j - v[i]] + w[i]);
//        }
//    }
//    cout << (dp[V] == -1 ? 0 : dp[V]) << endl;
//    return 0;
//}


