#include <iostream>
#include <vector>
using namespace std;


//class Solution {
//public:
//    //判断是否合法，如果遇到字母字符统一修改为小写
//    bool Legal(char& ch)
//    {
//        if ('a' <= ch && ch <= 'z') return true;
//        else if ('A' <= ch && ch <= 'Z')
//        {
//            //转换
//            ch = ch - 'A' + 'a';
//            return true;
//        }
//        else if ('0' <= ch && ch <= '9') return true;
//        return false;
//    }
//    bool isPalindrome(string s) {
//        //使用双指针算法
//        int n = s.size();
//        int left = 0, right = n - 1;
//        while (left < right)
//        {
//            //排除非法字符
//            while (left < right && !Legal(s[left])) ++left;
//            while (left < right && !Legal(s[right])) --right;
//            //这里可能不满足循环条件
//            if (left >= right) break;
//            if (s[left] != s[right]) return false;
//            ++left;
//            --right;
//        }
//        return true;
//    }
//};
//
//int main()
//{
//    string s = "race a car";
//    cout << Solution().isPalindrome(s);
//    return 0;
//}


