#include <iostream>
#include <vector>
#include <queue>
using namespace std;


//class Solution {
//    //使用枚举来记录颜色，通过染色的方式去区分二分性
//    typedef enum Color {
//        UNDEFEND,
//        RED,
//        BLACK
//    }color;
//    vector<int> nums;
//public:
//    bool isBipartite(vector<vector<int>>& graph) {
//        int n = graph.size();
//        nums.resize(n, UNDEFEND);
//        nums[0] = RED;   //开始的节点染成红色
//        //从0开始广度优先遍历
//        queue<int> q;
//        q.push(0);
//        while (q.size())
//        {
//            int sz = q.size();
//            for (int i = 0; i < n; ++i)
//            {
//                //得到每一个节点，然后把子节点染色，如果子节点已经有颜色并且和要染的颜色不用就不是二分
//                int node = q.front();
//                q.pop();
//                int col = nums[node] == RED ? BLACK : RED;   //子节点的颜色
//                for (int j = 0; j < graph[node].size(); ++j)
//                {
//                    //连接出去的节点
//                    if (j > 0 && graph[node][j - 1] > graph[node][j]) break;  //这里处理一些0的边界
//                    int connectNode = graph[node][j];
//                    if (nums[connectNode] == UNDEFEND)
//                    {
//                        nums[connectNode] = col;
//                        q.push(connectNode);
//                    }
//                    else {
//                        if (nums[connectNode] != col) return false;
//                    }
//                }
//            }
//        }
//        return true;
//    }
//};


//class CBTInserter {
//    //使用堆的方式进行模拟即可
//    vector<TreeNode*> heap;
//public:
//    CBTInserter(TreeNode* root) {
//        //使用queue层序遍历，把树的节点放入到堆中
//        queue<TreeNode*> q;
//        q.push(root);
//        while (q.size())
//        {
//            int sz = q.size();
//            for (int i = 0; i < sz; ++i)
//            {
//                TreeNode* node = q.front();
//                q.pop();
//                heap.push_back(node);
//                if (node->left) q.push(node->left);
//                if (node->right) q.push(node->right);
//            }
//        }
//    }
//
//    int insert(int v) {
//        //1.构造节点
//        TreeNode* node = new TreeNode(v);
//        //2.将节点放入heap中
//        heap.push_back(node);
//        //3.根据逻辑关系找到对应的父节点，然后连接子节点
//        int lastIdx = heap.size() - 1;
//        int parentIdx = (lastIdx - 1) / 2;
//        TreeNode* parent = heap[parentIdx];
//        if (parentIdx * 2 + 1 == lastIdx) parent->left = node;
//        else parent->right = node;
//        return parent->val;
//    }
//
//    TreeNode* get_root() {
//        return heap[0];
//    }
//};




