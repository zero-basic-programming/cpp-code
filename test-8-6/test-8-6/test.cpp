#include <iostream>
#include <vector>
#include <algorithm>
using namespace std;


//class Solution {
//public:
//    int massage(vector<int>& nums) {
//        int n = nums.size();
//        if (n == 0)  return 0;
//        //f表示选择，g表示不选
//        vector<int> f(n);
//        auto g = f;
//        //初始化
//        f[0] = nums[0];
//        for (int i = 1; i < n; ++i)
//        {
//            f[i] = g[i - 1] + nums[i];
//            g[i] = max(f[i - 1], g[i - 1]);
//        }
//        return max(f[n - 1], g[n - 1]);
//    }
//};


//class Solution {
//public:
//    vector<vector<int>> threeSum(vector<int>& nums) {
//        int n = nums.size();
//        sort(nums.begin(), nums.end());
//        vector<vector<int>> result;
//        for (int i = 0; i < n - 2;)
//        {
//            int left = i + 1;
//            int right = n - 1;
//            //得到它的相反数，把问题转换成两数之和
//            int target = nums[i] * (-1);
//            while (left < right)
//            {
//                int sum = nums[left] + nums[right];
//                if (sum > target) --right;
//                else if (sum < target) ++left;
//                else
//                {
//                    result.push_back({ nums[i],nums[left++],nums[right--] });
//                    //得到结果之后再对这两数进行去重
//                    while (left < n - 1 && nums[left] == nums[left - 1]) ++left;
//                    while (right > i && nums[right] == nums[right + 1]) --right;
//                }
//            }
//            ++i;
//            //对i去重
//            if (nums[i] == nums[i - 1])
//            {
//                while (i < n - 1 && nums[i] == nums[i - 1]) ++i;
//            }
//        }
//        return result;
//    }
//};


//class Solution {
//public:
//    vector<vector<int>> fourSum(vector<int>& nums, int target) {
//        vector<vector<int>> result;
//        int n = nums.size();
//        if (n < 4) return {};
//        sort(nums.begin(), nums.end());
//        for (int i = 0; i < n - 3;)
//        {
//            for (int j = i + 1; j < n - 2;)
//            {
//                int left = j + 1;
//                int right = n - 1;
//                long long res = (long long)target - nums[i] - nums[j]; //得到两数之和
//                while (left < right)
//                {
//                    int sum = nums[left] + nums[right];
//                    if (sum < res) ++left;
//                    else if (sum > res) --right;
//                    else {
//                        //得到结果
//                        result.push_back({ nums[i],nums[j],nums[left++],nums[right--] });
//                        //对left和right进行去重
//                        while (left < right && nums[left] == nums[left - 1]) ++left;
//                        while (left < right && nums[right] == nums[right + 1]) --right;
//                    }
//                }
//                //对j进行去重
//                j++;
//                if (nums[j] == nums[j - 1])
//                {
//                    while (j < n - 2 && nums[j] == nums[j - 1]) ++j;
//                }
//            }
//            //对i去重
//            i++;
//            if (nums[i] == nums[i - 1])
//            {
//                while (i < n - 3 && nums[i] == nums[i - 1]) ++i;
//            }
//        }
//        return result;
//    }
//};
//
//int main()
//{
//    vector<int> tmp = { 1,0,-1,0,-2,2 };
//    Solution().fourSum(tmp, 0);
//    return 0;
//}


