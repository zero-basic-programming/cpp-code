#include <iostream>
#include <vector>
using namespace std;


//class Solution {
//public:
//    int numSquares(int n) {
//        int m = sqrt(n);
//        vector<vector<int>> dp(m + 1, vector<int>(n + 1, 0x3f3f3f3f));
//        //初始化
//        dp[0][0] = 0;
//        for (int i = 1; i <= m; ++i)
//        {
//            for (int j = 0; j <= n; ++j)
//            {
//                dp[i][j] = dp[i - 1][j];
//                if (j >= i * i)
//                    dp[i][j] = min(dp[i][j], dp[i][j - i * i] + 1);
//            }
//        }
//        return dp[m][n];
//    }
//};


//滚动数组优化
//class Solution {
//public:
//    int numSquares(int n) {
//        int m = sqrt(n);
//        vector<int> dp(n + 1, 0x3f3f3f3f);
//        //初始化
//        dp[0] = 0;
//        for (int i = 1; i <= m; ++i)
//        {
//            for (int j = i * i; j <= n; ++j)
//                dp[j] = min(dp[j], dp[j - i * i] + 1);
//        }
//        return dp[n];
//    }
//};


//二维费用问题
//class Solution {
//public:
//    int findMaxForm(vector<string>& strs, int m, int n) {
//        int len = strs.size();
//        vector<vector<vector<int>>> dp(len + 1, vector<vector<int>>(m + 1, vector<int>(n + 1)));
//        for (int i = 1; i <= len; ++i)
//        {
//            //统计出当前string中的0和1的个数
//            int zero = 0, one = 0;
//            for (auto ch : strs[i - 1])
//            {
//                if (ch == '0') ++zero;
//                else ++one;
//            }
//            for (int j = 0; j <= m; ++j)
//            {
//                for (int k = 0; k <= n; ++k)
//                {
//                    dp[i][j][k] = dp[i - 1][j][k];
//                    if (j >= zero && k >= one)
//                    {
//                        dp[i][j][k] = max(dp[i][j][k], dp[i - 1][j - zero][k - one] + 1);
//                    }
//                }
//            }
//        }
//        return dp[len][m][n];
//    }
//};


//滚动数组优化
//class Solution{
//public:
//    int findMaxForm(vector<string>&strs, int m, int n) {
//        int len = strs.size();
//        vector<vector<int>> dp(m + 1,vector<int>(n + 1));
//        for (int i = 1; i <= len; ++i)
//        {
//            //统计出当前string中的0和1的个数
//            int zero = 0,one = 0;
//            for (auto ch : strs[i - 1])
//            {
//                if (ch == '0') ++zero;
//                else ++one;
//            }
//            for (int j = m; j >= zero; --j) //因为这里是类比01背包问题，必须是从大到小
//            {
//                for (int k = n; k >= one; --k)
//                {
//                    dp[j][k] = max(dp[j][k],dp[j - zero][k - one] + 1);
//                }
//            }
//        }
//        return dp[m][n];
//    }
//};


//class Solution {
//public:
//    int profitableSchemes(int n, int minProfit, vector<int>& group, vector<int>& profit) {
//        int m = profit.size();
//        const int MOD = 1e9 + 7;
//        vector<vector<vector<int>>> dp(m + 1, vector<vector<int>>(n + 1, vector<int>(minProfit + 1)));
//        //初始化
//        for (int j = 0; j <= n; ++j) dp[0][j][0] = 1; //没有任务的时候都是一个选法
//        for (int i = 1; i <= m; ++i)
//        {
//            for (int j = 0; j <= n; ++j)
//            {
//                for (int k = 0; k <= minProfit; ++k)
//                {
//                    dp[i][j][k] = dp[i - 1][j][k];
//                    if (j >= group[i - 1])
//                    {
//                        dp[i][j][k] += dp[i - 1][j - group[i - 1]][max(0, k - profit[i - 1])];
//                        //防止越界
//                        dp[i][j][k] %= MOD;
//                    }
//                }
//            }
//        }
//        return dp[m][n][minProfit];
//    }
//};


//利用滚动数组优化
//class Solution {
//public:
//    int profitableSchemes(int n, int minProfit, vector<int>& group, vector<int>& profit) {
//        int m = profit.size();
//        const int MOD = 1e9 + 7;
//        vector<vector<int>> dp(n + 1, vector<int>(minProfit + 1));
//        //初始化
//        for (int j = 0; j <= n; ++j) dp[j][0] = 1; //没有任务的时候都是一个选法
//        for (int i = 1; i <= m; ++i)
//        {
//            for (int j = n; j >= group[i - 1]; --j)
//            {
//                for (int k = minProfit; k >= 0; --k)
//                {
//                    dp[j][k] += dp[j - group[i - 1]][max(0, k - profit[i - 1])];
//                    //防止越界
//                    dp[j][k] %= MOD;
//                }
//            }
//        }
//        return dp[n][minProfit];
//    }
//};