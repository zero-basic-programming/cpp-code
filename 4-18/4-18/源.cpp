#include <iostream>
#include <vector>
using namespace std;


//class Solution {
//    vector<vector<bool>> vis;  //用来观察元素是否使用过
//    int m, n;
//    int dx[4] = { 0,0,1,-1 };
//    int dy[4] = { 1,-1,0,0 };
//public:
//    bool exist(vector<vector<char>>& board, string word) {
//        m = board.size(), n = board[0].size();
//        vis.resize(m, vector<bool>(n, false));
//        for (int i = 0; i < m; ++i)
//        {
//            for (int j = 0; j < n; ++j)
//            {
//                if (board[i][j] == word[0])
//                {
//                    if (dfs(i, j, board, word, 1)) return true;
//                }
//            }
//        }
//        return false;
//    }
//    bool dfs(int i, int j, vector<vector<char>>& board, string& word, int index)
//    {
//        if (index == word.size()) return true;
//        vis[i][j] = true;
//        for (int k = 0; k < 4; ++k)
//        {
//            int x = i + dx[k], y = j + dy[k];
//            if (x >= 0 && x < m && y >= 0 && y < n && !vis[x][y] && board[x][y] == word[index])
//            {
//                if (dfs(x, y, board, word, index + 1)) return true;
//            }
//        }
//        vis[i][j] = false;
//        return false;
//    }
//};


//class Solution {
//    vector<vector<bool>> vis;
//    int m, n;
//    int dx[4] = { 0,0,1,-1 };
//    int dy[4] = { 1,-1,0,0 };
//    int ret;
//public:
//    int getMaximumGold(vector<vector<int>>& grid) {
//        m = grid.size(), n = grid[0].size();
//        vis.resize(m, vector<bool>(n, false));
//        for (int i = 0; i < m; ++i)
//        {
//            for (int j = 0; j < n; ++j)
//            {
//                if (grid[i][j] != 0)
//                {
//                    dfs(grid, i, j, grid[i][j]);
//                }
//            }
//        }
//        return ret;
//    }
//    void dfs(vector<vector<int>>& grid, int i, int j, int sum)
//    {
//        vis[i][j] = true;
//        for (int k = 0; k < 4; ++k)
//        {
//            int x = i + dx[k], y = j + dy[k];
//            if (x >= 0 && x < m && y >= 0 && y < n && !vis[x][y] && grid[x][y] != 0)
//            {
//                dfs(grid, x, y, sum + grid[x][y]);
//            }
//        }
//        ret = max(ret, sum);
//        vis[i][j] = false;
//    }
//};


//class Solution {
//    vector<vector<bool>> vis;
//    int m, n, ret, num;
//    int dx[4] = { 0,0,1,-1 };
//    int dy[4] = { 1,-1,0,0 };
//public:
//    int uniquePathsIII(vector<vector<int>>& grid) {
//        m = grid.size(), n = grid[0].size();
//        vis.resize(m, vector<bool>(n, false));
//        int x, y;
//        for (int i = 0; i < m; ++i)
//        {
//            for (int j = 0; j < n; ++j)
//            {
//                if (grid[i][j] == 0) ++num;
//                if (grid[i][j] == 1) x = i, y = j;
//            }
//        }
//        cout << num;
//        dfs(grid, x, y, 0);
//        return ret;
//    }
//    void dfs(vector<vector<int>>& grid, int i, int j, int step)
//    {
//        if (step == num + 1 && grid[i][j] == 2) ++ret;
//        vis[i][j] = true;
//        for (int k = 0; k < 4; ++k)
//        {
//            int x = i + dx[k], y = j + dy[k];
//            if (x >= 0 && x < m && y >= 0 && y < n && !vis[x][y] && grid[x][y] != -1)
//            {
//                dfs(grid, x, y, step + 1);
//            }
//        }
//        vis[i][j] = false;
//    }
//};


//class Solution {
//    int dx[4] = { 0,0,1,-1 };
//    int dy[4] = { 1,-1,0,0 };
//    int m, n, oldcolor;
//public:
//    vector<vector<int>> floodFill(vector<vector<int>>& image, int sr, int sc, int color) {
//        if (image[sr][sc] == color) return image;
//        oldcolor = image[sr][sc];
//        m = image.size(), n = image[0].size();
//        dfs(image, sr, sc, color);
//        return image;
//    }
//    void dfs(vector<vector<int>>& image, int i, int j, int color)
//    {
//        image[i][j] = color;
//        for (int k = 0; k < 4; ++k)
//        {
//            int x = i + dx[k], y = j + dy[k];
//            if (x >= 0 && x < m && y >= 0 && y < n && image[x][y] == oldcolor)
//            {
//                dfs(image, x, y, color);
//            }
//        }
//    }
//};
//
//int main()
//{
//    vector<vector<int>> arr = { {1,1,1} ,{1,1,0},{1,0,1} };
//    Solution().floodFill(arr, 1, 1, 2);
//    return 0;
//}



//#include <iostream>
//#include <vector>
//#include <thread>
//#include <string>
//using namespace std;
//
//int main() {
//    char english, tmp;
//    int num;
//    vector<string> in;
//    string str("A,26,%");
//    int pos = 0;
//    int cur = str.find(',', pos);
//    while (cur != string::npos) {
//        in.push_back(str.substr(pos, cur - pos));
//        pos = cur + 1;
//        cur = str.find(',', pos);
//    }
//    in.push_back(str.substr(pos));
//    english = in[0][0], num = stoi(in[1]), tmp = in[2][0];
//    int i = 1;
//    thread t1([&]() {
//        for (int i = 1; i <= num; ++i)
//        cout << "A" << i << "#";
//        });
//    t1.join();
//    return 0;
//}