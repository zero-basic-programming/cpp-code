#include <iostream>
#include <vector>
#include <thread>
using namespace std;

//void Qsort(vector<int>& arr, int l, int r)
//{
//	if (l >= r) return;
//	//找基准点
//	int key = arr[rand() % (r - l + 1) + l];
//	int left = l - 1, right = r + 1, i = l;
//	while (i < right)
//	{
//		if (key > arr[i]) swap(arr[i++], arr[++left]);
//		else if (key == arr[i]) ++i;
//		else swap(arr[i], arr[--right]);
//	}
//	//递归左右
//	Qsort(arr, l, left);
//	Qsort(arr, right, r);
//}
//
//
//void MergeSort(vector<int>& arr, int left, int right,vector<int>& tmp)
//{
//	if (left >= right) return;
//	//找中间节点
//	int mid = (left + right) >> 1;
//	//[left,mid] [mid+1,right]
//	//归并左右
//	MergeSort(arr, left, mid,tmp);
//	MergeSort(arr, mid + 1, right,tmp);
//	//排序
//	int begin1 = left, end1 = mid;
//	int begin2 = mid + 1, end2 = right;
//	int i = left;
//	while (begin1 <= end1 && begin2 <= end2)
//	{
//		tmp[i++] = arr[begin1] < arr[begin2] ? arr[begin1++] : arr[begin2++];
//	}
//	//处理剩下的数据
//	while (begin1 <= end1)
//	{
//		tmp[i++] = arr[begin1++];
//	}
//	while (begin2 <= end2)
//	{
//		tmp[i++] = arr[begin2++];
//	}
//	//还原
//	for (int j = left; j <= right; ++j)
//		arr[j] = tmp[j];
//}
//
//
//
//int main()
//{
//	srand(time(nullptr));
//	vector<int> arr = { 1,4,3,6,2,4,7,4,7,9,8,0 };
//	int n = arr.size();
//	vector<int> tmp(n);
//	//Qsort(arr, 0, n - 1);
//	MergeSort(arr, 0, n - 1, tmp);
//	for (auto e : arr)
//	{
//		cout << e << " ";
//	}
//	return 0;
//}


//class Solution {
//	bool row[9][10];
//	bool col[9][10];
//	bool lattic[3][3][10];
//public:
//	bool isValidSudoku(vector<vector<char>>& board) {
//		for (int i = 0; i < 9; ++i)
//		{
//			for (int j = 0; j < 9; ++j)
//			{
//				if (board[i][j] != '.')
//				{
//					int num = board[i][j] - '0';
//					if (row[i][num] || col[j][num] || lattic[i / 3][j / 3][num])
//						return false;
//					//记录hash
//					row[i][num] = col[j][num] = lattic[i / 3][j / 3][num] = true;
//				}
//			}
//		}
//		return true;
//	}
//};

