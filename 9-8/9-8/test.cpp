#include <iostream>
#include <vector>
using namespace std;


//int main()
//{
//    int n;
//    cin >> n;
//    if (n % 2 == 0) cout << n / 2 << endl;
//    else cout << (n / 2) + 1 << endl;
//    return 0;
//}



//#include <iostream>
//#include <vector>
//#include <string>
//using namespace std;
//
//int n, m;
//
//bool dfs(vector<string>& nums, vector<vector<bool>>& vis, int i, int j)
//{
//    char tmp = nums[i][j];   //拿到当前格子的移动方向
//    int x = i, y = j;
//    if (tmp == 'L') --y;
//    else if (tmp == 'R') ++y;
//    else if (tmp == 'U') --x;
//    else ++x;
//    if (x < 0 || x >= n || y < 0 || y >= m)
//    {
//        vis[i][j] = false;
//        return false;  //这条路不通，凡是走到该位置都是false
//    }
//    vis[i][j] = true;
//    //如果走到了曾经走过的位置，就返回true
//    if (vis[x][y]) return true;
//    if (dfs(nums, vis, x, y) == false)
//    {
//        //回退，前面的都是错的
//        vis[i][j] = false;
//        return false;
//    }
//    return true;
//}
//
//int main()
//{
//    cin >> n >> m;
//    vector<string> nums;
//    for (int i = 0; i < n; ++i) {
//        string tmp;
//        cin >> tmp;
//        nums.push_back(tmp);
//    }
//    vector<vector<bool>> vis(n, vector<bool>(m, false));  //用来标记位置是否会出界
//    for (int i = 0; i < n; ++i)
//    {
//        for (int j = 0; j < m; ++j)
//        {
//            if (vis[i][j] == false)
//            {
//                dfs(nums, vis, i, j);
//            }
//        }
//    }
//    //查看vis中true的个数
//    int ret = 0;
//    for (auto& v : vis)
//    {
//        for (bool x : v)
//        {
//            if (x == true) ++ret;
//        }
//    }
//    cout << ret << endl;
//    return 0;
//}



#include <unordered_set>
using namespace std;

//int g(int x)
//{
//    //获取到最后一位的1的前一位是0
//    int i = 0;
//    while (i < 31)
//    {
//        if ((((x >> i) & 1) == 1) && (((x >> (i + 1)) & 1) == 0)) break;
//        ++i;
//    }
//    //把i位置的1换到i+1位置得到结果
//    x &= ~(1 << i);
//    x |= (1 << (i + 1));
//    return x;
//}
//
//int main()
//{
//    int n;
//    cin >> n;
//    unordered_set<int> hash;
//    for (int i = 0; i < n; ++i)
//    {
//        int val;
//        cin >> val;
//        hash.insert(val);
//    }
//    int ret = 0;  //获取结果
//    for (auto& x : hash)
//    {
//        int tmpVal = x;
//        int tmpRes = 0;   //记录每个数字开始的结果
//        while (tmpVal != -1)
//        {
//            int nextVal = g(tmpVal);
//            auto it = hash.find(nextVal);
//            if (it != hash.end())
//            {
//                ++tmpRes;
//                tmpVal = nextVal;
//            }
//            else tmpVal = -1;
//        }
//        ret = max(ret, tmpRes);
//    }
//    cout << ret << endl;
//    return 0;
//}

//int gtmp(int x, unordered_set<int>& hash)
//{
//    //获取到最后一位的1的前一位是0
//    int i = 0;
//    while (i < 31)
//    {
//        if ((((x >> i) & 1) == 1) && (((x >> (i + 1)) & 1) == 0))
//        {
//            //把i位置的1换到i+1位置得到结果
//            x &= ~(1 << i);
//            x |= (1 << (i + 1));
//            //在hash中找有没有这个数，如果没有就继续找，有的话
//            if (hash.find(x) != hash.end()) return x;
//            else {
//                i = 0;
//                continue;
//            }
//        }
//        ++i;
//    }
//    return -1;
//}
//
//int main()
//{
//    int n;
//    cin >> n;
//    unordered_set<int> hash;
//    for (int i = 0; i < n; ++i)
//    {
//        int val;
//        cin >> val;
//        hash.insert(val);
//    }
//    int ret = 1;  //获取结果
//    for (auto& x : hash)
//    {
//        int tmpVal = x;
//        int tmpRes = 1;   //记录每个数字开始的结果
//        while (true)
//        {
//            int nextVal = gtmp(tmpVal, hash);
//            if (nextVal == -1) break;
//            ++tmpRes;
//            tmpVal = nextVal;
//        }
//        ret = max(ret, tmpRes);
//    }
//    cout << ret << endl;
//    return 0;
//}
//



//int f(int x)
//{
//    int count = 0;
//    while (x)
//    {
//        ++count;
//        x &= (x - 1);
//    }
//    return count;
//}
//
//int main()
//{
//    int n;
//    cin >> n;
//    unordered_map<int, vector<int>> hash;
//    for (int i = 0; i < n; ++i)
//    {
//        int val;
//        cin >> val;
//        int num = f(val);
//        hash[num].push_back(val);
//    }
//    int ret = 0;
//    for (auto& p : hash)
//    {
//        int sz = p.second.size();
//        ret = max(ret, sz);
//    }
//    cout << ret << endl;
//    return 0;
//}
//
//
//

