#include <iostream>
#include <vector>
using namespace std;

//class Solution {
//public:
//    int tribonacci(int n) {
//        //判断一下边界条件
//        if (n == 0 || n == 1) return n;
//        if (n == 2) return 1;
//        vector<int> dp(n + 1);
//        //初始化
//        dp[0] = 0; dp[1] = dp[2] = 1;
//        for (int i = 3; i <= n; ++i)
//        {
//            //状态转移方程
//            dp[i] = dp[i - 1] + dp[i - 2] + dp[i - 3];
//        }
//        return dp[n];
//    }
//};

//class Solution {
//public:
//    int tribonacci(int n) {
//        //空间优化
//        //判断一下边界条件
//        if (n == 0 || n == 1) return n;
//        if (n == 2) return 1;
//        //初始化
//        int a = 0, b = 1, c = 1, d = 0;
//        for (int i = 3; i <= n; ++i)
//        {
//            //状态转移方程
//            d = a + b + c;
//            //迭代
//            a = b;
//            b = c;
//            c = d;
//        }
//        return d;
//    }
//};

//class Solution {
//public:
//    int waysToStep(int n) {
//        //处理一下边界情况
//        if (n == 1 || n == 2)   return n;
//        if (n == 3)  return 4;
//        vector<int> dp(n + 1);
//        //初始化
//        //dp[0] 没有意义
//        dp[1] = 1; dp[2] = 2; dp[3] = 4;
//        //处理结果很大的情况
//        const int Mod = 1e9 + 7;
//        for (int i = 4; i <= n; ++i)
//        {
//            //状态转移方程
//            dp[i] = ((dp[i - 1] + dp[i - 2]) % Mod + dp[i - 3]) % Mod; // 这里每次加完都要取模，防止溢出
//        }
//        return dp[n];
//    }
//};
//


