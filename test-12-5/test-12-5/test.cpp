#include <iostream>
#include <vector>
using namespace std;

////冒泡排序
//void BufferSort(vector<int>& arr)
//{
//	int n = arr.size();
//	for (int i = 0; i < n - 1; ++i)  //趟数
//	{
//		bool flag = false;
//		for (int j = 0; j < n - i - 1; ++j)
//		{
//			if (arr[j] > arr[j + 1])
//			{
//				swap(arr[j], arr[j + 1]);
//				flag = true;
//			}
//		}
//		//这里一次没有交换说明已经是有序
//		if (flag == false) break;
//	}
//}
//
////选择排序
//void SelectSort(vector<int>& arr)
//{
//	int n = arr.size();
//	int left = 0, right = n - 1;
//	while (left < right)
//	{
//		int maxi = left, mini = left;
//		for (int i = left; i <= right; ++i)
//		{
//			if (arr[i] > arr[maxi]) maxi = i;
//			if (arr[i] < arr[mini]) mini = i;
//		}
//		swap(arr[maxi], arr[right]);
//		//这里有可能mini在right的位置
//		if (mini == right) mini = maxi;
//		swap(arr[mini], arr[left]);
//		++left;
//		--right;
//	}
//}
//
////直接插入排序
//void InsertSort(vector<int>& arr)
//{
//	int n = arr.size();
//	for (int i = 0; i < n - 1; ++i)
//	{
//		int end = i;
//		int tmp = arr[end + 1];
//		while (end >= 0)
//		{
//			if (tmp < arr[end])
//			{
//				arr[end + 1] = arr[end];
//				--end;
//			}
//			else break; //这里已经找到该插入的位置
//		}
//		arr[end + 1] = tmp;
//	}
//}
//
//int main()
//{
//	vector<int> arr = { 1,5,3,4,2,8,5,9,8,0 };
//	//BufferSort(arr);
//	//SelectSort(arr);
//	InsertSort(arr);
//	for (auto e : arr)
//	{
//		cout << e << " ";
//	}
//	return 0;
//}


//class Solution {
//    vector<vector<string>> ret;
//    vector<string> path;
//public:
//    vector<vector<string>> solveNQueens(int n) {
//        path.resize(n, string(n, '.'));
//        dfs(n, 0);
//        return ret;
//    }
//    void dfs(int n, int pos)
//    {
//        //返回
//        if (pos == n)
//        {
//            ret.push_back(path);
//            return;
//        }
//        for (int i = 0; i < n; ++i)
//        {
//            //判断当前位置是否满足条件
//            if (IsTrue(pos, i, n))
//            {
//                path[pos][i] = 'Q';
//                dfs(n, pos + 1);
//                //回溯
//                path[pos][i] = '.';
//            }
//        }
//    }
//    //判断是否符合N皇后
//    bool IsTrue(int i, int j, int n)
//    {
//        //判断纵列
//        for (int x = 0; x < i; ++x)
//        {
//            if (path[x][j] == 'Q')
//                return false;
//        }
//        //判断正斜
//        for (int x = i - 1, y = j + 1; x >= 0 && y < n; --x, ++y)
//        {
//            if (path[x][y] == 'Q')
//                return false;
//        }
//        //判断反斜
//        for (int x = i - 1, y = j - 1; x >= 0, y >= 0; --x, --y)
//        {
//            if (path[x][y] == 'Q')
//                return false;
//        }
//        return true;
//    }
//};
//


//class Solution {
//    vector<vector<string>> ret;
//    vector<string> path;
//    //使用hash的方式快速找到是否能放皇后
//    bool col[9];
//    bool diff1[40];
//    bool diff2[20];
//public:
//    vector<vector<string>> solveNQueens(int n) {
//        path.resize(n, string(n, '.'));
//        dfs(n, 0);
//        return ret;
//    }
//    void dfs(int n, int pos)
//    {
//        //返回
//        if (pos == n)
//        {
//            ret.push_back(path);
//            return;
//        }
//        for (int i = 0; i < n; ++i)
//        {
//            if (col[i] || diff1[i - pos + n] || diff2[i + pos]) continue; //这里已经有皇后了
//            path[pos][i] = 'Q';
//            col[i] = true;
//            diff1[i - pos + n] = true;
//            diff2[i + pos] = true;
//            dfs(n, pos + 1);
//            //回溯
//            path[pos][i] = '.';
//            col[i] = false;
//            diff1[i - pos + n] = false;
//            diff2[i + pos] = false;
//        }
//    }
//};


