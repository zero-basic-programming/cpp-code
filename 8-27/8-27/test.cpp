#include <iostream>
#include <vector>
using namespace std;


//class Solution {
//public:
//    Node* dfs(Node* head)   // 返回子节点的最后一个节点
//    {
//        Node* cur = head;
//        Node* tail = nullptr;   //记录子节点层的最后一个节点
//        while (cur)
//        {
//            Node* next = cur->next;
//            if (cur->child)
//            {
//                //递归处理子节点，然后获取到子节点的最后一个节点改变连接
//                Node* child_last = dfs(cur->child);
//                //改变当前节点与其子节点的连接关系
//                cur->next = cur->child;
//                cur->child->prev = cur;
//                //改变子节点最后一个节点和当前节点的下一个节点的连接关系
//                //这里的next可能为空
//                if (next)
//                {
//                    child_last->next = next;
//                    next->prev = child_last;
//                }
//                //将当前节点的子节点置空
//                cur->child = nullptr;
//                tail = child_last;
//            }
//            else
//            {
//                tail = cur;
//            }
//            cur = next;   //迭代
//        }
//        return tail;
//    }
//    Node* flatten(Node* head) {
//        //通过遍历链表中的节点，如果有子节点那么就是递归处理
//        dfs(head);
//        return head;
//    }
//};


//class LRUCache {
//    typedef list<pair<int, int>>::iterator Lister;
//public:
//    LRUCache(int capacity) :_capacity(capacity)
//    {
//    }
//
//    int get(int key) {
//        //从hash中找，如果找到就更新链表节点返回，如果没有找到返回-1
//        auto iter = _hash.find(key);
//        if (iter == _hash.end()) return -1;
//        else
//        {
//            //更新链表节点
//            Lister it = iter->second;  //链表节点的迭代器
//            _list.splice(_list.begin(), _list, it);
//            return it->second;
//        }
//    }
//
//    void put(int key, int value) {
//        //如果找到就更新数据，同时放到list的头部，如果找不到就插入到头部，这里需要注意缓存大小
//        auto iter = _hash.find(key);
//        if (iter == _hash.end())
//        {
//            //没找到
//            if (_list.size() == _capacity)
//            {
//                //删除链表的最后一个数据
//                _hash.erase(_list.back().first);
//                _list.pop_back();
//            }
//            _list.push_front(make_pair(key, value));
//            _hash[key] = _list.begin();
//        }
//        else
//        {
//            //找到了需要更新
//            Lister it = iter->second;
//            it->second = value;
//            _list.splice(_list.begin(), _list, it);
//        }
//    }
//private:
//    list<pair<int, int>> _list;  //用来存放数据   k-v
//    unordered_map<int, Lister> _hash;  //存放k,和list节点的迭代器
//    int _capacity;
//};



//class RandomizedSet {
//public:
//    /** Initialize your data structure here. */
//    RandomizedSet() {
//
//    }
//
//    /** Inserts a value to the set. Returns true if the set did not already contain the specified element. */
//    bool insert(int val) {
//        auto it = hash.find(val);
//        if (it != hash.end()) return false;  //已经存在
//        else
//        {
//            //在nums后面插入，然后添加到hash中
//            nums.push_back(val);
//            hash[val] = nums.size() - 1;
//            return true;
//        }
//    }
//
//    /** Removes a value from the set. Returns true if the set contained the specified element. */
//    bool remove(int val) {
//        auto it = hash.find(val);
//        if (it == hash.end()) return false;
//        else
//        {
//            //将nums中的最后一个数据和当前数据对调，然后删除最后一个数据即可
//            int i = hash[val];
//            int lastVal = nums.back();
//            nums[i] = lastVal;
//            hash[lastVal] = i;
//            nums.pop_back();
//            hash.erase(val);
//            return true;
//        }
//    }
//
//    /** Get a random element from the set. */
//    int getRandom() {
//        return nums[rand() % nums.size()];
//    }
//private:
//    vector<int> nums;    //用来存放数据
//    unordered_map<int, int> hash;   //用来存放数据已经他在nums中的下标
//};


