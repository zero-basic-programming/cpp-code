#include <iostream>
#include <vector>
using namespace std; 

class Solution {
public:
    int change(int amount, vector<int>& coins) {
        vector<int> dp(amount + 1, 0);
        //初始化
        dp[0] = 1;
        for (int i = 0; i < coins.size(); ++i) // 遍历物品既面额
        {
            for (int j = coins[i]; j <= amount; ++j)
            {
                //状态转移方程
                dp[j] += (dp[j - coins[i]]);
            }
        }
        return dp[amount];
    }
};