#include <iostream>
#include <string>
#include <algorithm>
#include <vector>
using namespace std;


//class BugTemplate {
//public:
//    virtual void FixTest() = 0;
//};
//
////存在1个bug
//class ClassBug :public BugTemplate
//{
//private:
//    //这里的temp对象创建在栈上，当出了函数作用域的时候空间就会被释放
//    //那么再次访问当前空间的时候就是野指针访问，自然就会出现乱码问题
//    const std::string StringToUpper(const char* src)
//    {
//        std::string temp;
//        temp = src;
//        std::transform(temp.begin(), temp.end(), temp.begin(), ::toupper);
//        return temp;
//    }
//
//    const std::string StringToLower(const char* src)
//    {
//        std::string temp;
//        temp = src;
//        std::transform(temp.begin(), temp.end(), temp.begin(), ::tolower);
//        return temp;
//    }
//
//public:
//    ClassBug() {}
//    ~ClassBug() {}
//    //需要修复乱码问题
//    //tip:考察对类的作用域理解
//    virtual void FixTest()
//    {
//        std::cout << "请修复bug，使程序正确输出转大写转小写" << std::endl;
//        const char* test = "ABCddasdasdaddaweqWADfFcddasdasdaddaweqewabcddasdasdaddaweWQD";
//        const std::string test1 = StringToUpper(test);  //这里返回的时候拷贝一份就可以了
//        const std::string test2 = StringToLower(test);
//        std::cout << test << std::endl;
//        std::cout << test1 << std::endl;
//        std::cout << test2 << std::endl;
//
//    }
//
//};
//
////空间改为9个空间
//struct buffertest {
//    char strbuffer1[9];
//    char strbuffer2[9];
//    char strbuffer3[9];
//};
//
////存在3个bug
//class CopyBug :public BugTemplate
//{
//public:
//    CopyBug() {}
//    ~CopyBug() {}
//    //需要修复乱码问题
//    virtual void FixTest()
//    {
//
//        std::cout << "请让程序能够正常输出ABCDEFGHIJKLMNOP" << std::endl;
//        buffertest* buffer = (buffertest*)malloc(sizeof(buffertest));
//        //这里开辟strbuffer之后8个空间，但是后面这两个字符串有9个空间，最后跟了一个'\0'
//        strcpy(buffer->strbuffer2, "IJKLMNOP");
//        strcpy(buffer->strbuffer1, "ABCDEFGH");
//
//        std::cout << buffer->strbuffer1;
//        std::cout << buffer->strbuffer2 << std::endl;
//
//    }
//};
//
////存在一个bug
//class VectorBug :public BugTemplate
//{
//public:
//    VectorBug() {}
//    ~VectorBug() {}
//    virtual void FixTest()
//    {
//        std::vector<int> vec = { 1,2,3,4,5,6,7,8,9 };
//        std::cout << "正确使用迭代器删除程序前三个数值,使程序不崩溃" << std::endl;
//        std::vector<int>::iterator it = vec.begin();
//        for (int i = 0; i < 3; i++)
//        {
//            //这里删除会导致迭代器失效，利用删除后返回的新的迭代器赋值给当前迭代器即可
//            //因为删除是删除当前元素，同时返回下一个元素的迭代器
//            it = vec.erase(it);
//        }
//        for (it = vec.begin(); it != vec.end(); it++)
//        {
//            std::cout << *it << " ";
//        }
//
//    }
//};
//
//
////修复bug 使程序正常运作
//int main()
//{
//    {
//        ClassBug bug;
//        bug.FixTest();
//    }
//    {
//        CopyBug bug;
//        bug.FixTest();
//    }
//    {
//        VectorBug bug;
//        bug.FixTest();
//    }
//    system("pause");
//    return 0;
//}



//class Solution {
//public:
//    string removeDuplicates(string s) {
//        string ret;  //模拟栈
//        ret += s[0];
//        for (int i = 1; i < s.size(); ++i)
//        {
//            if (ret.size() && s[i] == ret.back()) ret.pop_back();
//            else ret += s[i];
//        }
//        return ret;
//    }
//};


//class Solution {
//public:
//    bool backspaceCompare(string s, string t) {
//        string str, str1;  //用来模拟栈
//        for (auto ch : s)
//        {
//            if (ch == '#' && str.size()) str.pop_back();
//            else str += ch;
//        }
//        for (auto ch : t)
//        {
//            if (ch == '#')
//            {
//                if(str1.size())
//                    str1.pop_back();
//            }
//            else str1 += ch;
//        }
//        return str == str1;
//    }
//};
//
//int main()
//{
//    Solution().backspaceCompare("y#fo##f", "y#f#o##f");
//    return 0;
//}


//class Solution {
//public:
//    int calculate(string s) {
//        vector<int> num;  //数字栈
//        char op = '+';   //保存当前的操作
//        int n = s.size();
//        for (int i = 0; i < s.size(); ++i)
//        {
//            if (s[i] == ' ') continue;
//            else if ('0' <= s[i] && s[i] <= '9')
//            {
//                int number = 0;
//                //把后面的数字全面取出来
//                while (i < n && '0' <= s[i] && s[i] <= '9')
//                {
//                    number = number * 10 + (s[i++] - '0');
//                }
//                --i;
//                //进行当前次的操作符运算
//                if (op == '+') num.push_back(number);
//                else if (op == '-') num.push_back(-number);
//                else if (op == '*') num.back() *= number;
//                else num.back() /= number;
//            }
//            else
//            {
//                op = s[i];
//            }
//        }
//        //把栈中的元素全部加起来
//        int ret = 0;
//        for (auto e : num) ret += e;
//        return ret;
//    }
//};


//class Solution {
//public:
//    string decodeString(string s) {
//        stack<int> num;  //数字栈
//        stack<string> st;   //字符串栈
//        int n = s.size();
//        st.push("");
//        int i = 0;
//        while (i < n)
//        {
//            if ('0' <= s[i] && s[i] <= '9')
//            {
//                //记录该数字并放入到数字栈
//                int tmp = 0;
//                while (i < n && '0' <= s[i] && s[i] <= '9')
//                {
//                    tmp = tmp * 10 + (s[i++] - '0');
//                }
//                num.push(tmp);
//            }
//            else if (s[i] == '[')
//            {
//                //把后面的字符串放入到字符串栈中
//                ++i;  //跳过[
//                string tmp;
//                while (i < n && 'a' <= s[i] && s[i] <= 'z')
//                {
//                    tmp += s[i++];
//                }
//                st.push(tmp);
//            }
//            else if (s[i] == ']')
//            {
//                //把栈顶的字符串以及数字栈顶取出来进行拼接
//                int number = num.top();
//                num.pop();
//                string tmp = st.top();
//                st.pop();
//                while (number--)
//                {
//                    st.top() += tmp;
//                }
//                ++i;  //跳过]
//            }
//            else
//            {
//                string tmp;
//                while (i < n && 'a' <= s[i] && s[i] <= 'z')
//                {
//                    tmp += s[i++];
//                }
//                st.top() += tmp;
//            }
//        }
//        return st.top();
//    }
//};


//class Solution {
//public:
//    bool validateStackSequences(vector<int>& pushed, vector<int>& popped) {
//        int i = 0;
//        vector<int> st;
//        for (auto e : pushed)
//        {
//            //进栈
//            st.push_back(e);
//            //出栈
//            while (st.size() && popped[i] == st.back())
//            {
//                st.pop_back();
//                ++i;
//            }
//        }
//        return i == popped.size();
//    }
//};


//class Solution {
//public:
//    vector<vector<int>> levelOrder(Node* root) {
//        vector<vector<int>> ret;
//        if (root == nullptr) return ret;
//        queue<Node*> q;
//        q.push(root);
//        while (q.size())
//        {
//            vector<int> tmp;
//            int sz = q.size();
//            for (int i = 0; i < sz; ++i)
//            {
//                Node* front = q.front();
//                q.pop();
//                tmp.push_back(front->val);
//                //放入其孩子
//                for (auto& node : front->children)
//                {
//                    if (node) q.push(node);
//                }
//            }
//            ret.push_back(tmp);
//        }
//        return ret;
//    }
//};


//class Solution {
//public:
//    vector<vector<int>> zigzagLevelOrder(TreeNode* root) {
//        vector<vector<int>> ret;
//        if (root == nullptr) return ret;
//        queue<TreeNode*> q;
//        q.push(root);
//        int level = 0;  // 用来记录层数，如果是奇数层就需要对结果进行反转
//        while (q.size())
//        {
//            int sz = q.size();
//            vector<int> tmp;
//            for (int i = 0; i < sz; ++i)
//            {
//                TreeNode* node = q.front();
//                q.pop();
//                tmp.push_back(node->val);
//                //放入左右子节点
//                if (node->left) q.push(node->left);
//                if (node->right) q.push(node->right);
//            }
//            //奇数层进行反转
//            if (level % 2 == 1) reverse(tmp.begin(), tmp.end());
//            ret.push_back(tmp);
//            ++level;
//        }
//        return ret;
//    }
//};


//class Solution {
//public:
//    vector<int> largestValues(TreeNode* root) {
//        vector<int> ret;
//        if (root == nullptr) return ret;
//        queue<TreeNode*> q;
//        q.push(root);
//        while (!q.empty())
//        {
//            int sz = q.size();
//            int maxres = INT_MIN;
//            for (int i = 0; i < sz; ++i)
//            {
//                TreeNode* node = q.front();
//                q.pop();
//                maxres = max(node->val, maxres);
//                if (node->left) q.push(node->left);
//                if (node->right) q.push(node->right);
//            }
//            ret.push_back(maxres);
//        }
//        return ret;
//    }
//};

//
//class Solution {
//public:
//    int widthOfBinaryTree(TreeNode* root) {
//        int ret = 0;  //收集结果
//        vector<pair<TreeNode*, int>> q;  //第2个是下标
//        q.push_back(make_pair(root, 1));
//        while (q.size())
//        {
//            auto& [left, x1] = q[0];
//            auto& [right, x2] = q.back();
//            ret = max(ret, x2 - x1 + 1);
//            vector<pair<TreeNode*, int>> tmp;
//            for (auto& [x, y] : q)
//            {
//                if (x->left) tmp.push_back(make_pair(x->left, 2 * y));
//                if (x->right) tmp.push_back(make_pair(x->left, 2 * y + 1));
//            }
//            q = tmp;
//        }
//        return ret;
//    }
//};


