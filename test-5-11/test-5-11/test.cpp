#include <iostream>
using namespace std;

//class Solution {
//private:
//    int maxcount = INT_MIN;//记录放入的最大count
//    int count = 1;// 用来记录次数
//    TreeNode* pre = nullptr; // 记录前一个节点
//public:
//    void traversal(TreeNode* cur, vector<int>& v)
//    {
//        if (cur == nullptr) return;
//        traversal(cur->left, v);
//        if (pre == nullptr)
//        {
//            count = 1;
//        }
//        else if (pre->val == cur->val)
//        {
//            ++count;
//        }
//        else {
//            count = 1; // 不相同了更新count
//        }
//        pre = cur;
//        //如果相同加入结果集
//        if (count == maxcount)
//        {
//            v.push_back(cur->val);
//        }
//        if (count > maxcount)
//        {
//            maxcount = count;
//            v.clear();//清除前面的数据
//            v.push_back(cur->val);
//        }
//        traversal(cur->right, v);
//        return;
//    }
//    vector<int> findMode(TreeNode* root) {
//        vector<int> result;
//        traversal(root, result);
//        return result;
//    }
//};



//class Solution {
//public:
//    vector<int> findMode(TreeNode* root) {
//        //使用栈模拟中序过程
//        stack<TreeNode*> st;
//        TreeNode* cur = root;
//        vector<int> result;
//        int count = 0;
//        int maxcount = 0; // 记录最大出现的次数
//        TreeNode* pre = nullptr;
//        while (cur || !st.empty())
//        {
//            if (cur)
//            {
//                st.push(cur);
//                cur = cur->left; // 往左子树走
//            }
//            else
//            {
//                cur = st.top();
//                st.pop();
//                //中序过程
//                if (pre == nullptr)
//                {
//                    count = 1;
//                }
//                else if (pre->val == cur->val)
//                {
//                    ++count;
//                }
//                else {
//                    count = 1; //不相等
//                }
//                if (count == maxcount)
//                {
//                    result.push_back(cur->val);
//                }
//                //大于
//                if (count > maxcount)
//                {
//                    maxcount = count;
//                    result.clear();
//                    result.push_back(cur->val);
//                }
//                //更新pre
//                pre = cur;
//                //往右子树走
//                cur = cur->right;
//            }
//        }
//        return result;
//    }
//};


