#include <iostream>
#include <vector>
using namespace std;

//class Solution {
//public:
//    int maxSubarraySumCircular(vector<int>& nums) {
//        int sum = 0;
//        int n = nums.size();
//        //f找非环形数组中最大的
//        //g找非环形数组中最小的
//        vector<int> f(n + 1);
//        auto g = f;
//        int fmax = INT_MIN, gmin = INT_MAX;
//        for (int i = 1; i <= n; ++i)
//        {
//            int x = nums[i - 1];
//            f[i] = max(x, x + f[i - 1]);
//            fmax = max(fmax, f[i]);
//            g[i] = min(x, x + g[i - 1]);
//            gmin = min(gmin, g[i]);
//            sum += x;
//        }
//        //数组中有可能是全负数
//        return sum == gmin ? fmax : max(fmax, sum - gmin);
//    }
//};


class Solution {
public:
    vector<int> searchRange(vector<int>& nums, int target) {
        if (nums.size() == 0)    return { -1,-1 };
        //分别求出其最左或者最右的区间即可
        int left = 0, right = nums.size() - 1;
        //找最左端点
        while (left < right)
        {
            int mid = left + (right - left) / 2;
            if (nums[mid] < target)  left = mid + 1;
            else right = mid;
        }
        int begin = left; //这里一定是左右两个边界相等才会出循环
        //有可能没有结果
        if (nums[left] != target) return { -1,-1 };
        left = 0;
        right = nums.size() - 1;
        //找右边界
        while (left < right)
        {
            int mid = left + (right - left + 1) / 2;
            if (nums[mid] <= target) left = mid;
            else right = mid - 1;
        }
        return { begin,right };
    }
};


class Solution {
public:
    int mySqrt(int x) {
        int left = 0, right = x;
        while (left < right)
        {
            int mid = left + (right - left + 1) / 2;
            if (mid * mid > right) right = mid - 1;
            else    left = mid;
        }
        return left;
    }
};