#include <iostream>
#include <vector>
using namespace std;


//class Solution {
//    vector<vector<int>> ret;
//    vector<int> path;
//    int sum;
//public:
//    void dfs(vector<int>& candidates, int target, int start)
//    {
//        if (sum == target)
//        {
//            ret.push_back(path);
//            return;
//        }
//        if (sum > target) return;
//        for (int i = start; i < candidates.size(); ++i)
//        {
//            sum += candidates[i];
//            path.push_back(candidates[i]);
//            dfs(candidates, target, i);
//            //回溯
//            path.pop_back();
//            sum -= candidates[i];
//        }
//    }
//    vector<vector<int>> combinationSum(vector<int>& candidates, int target) {
//        sort(candidates.begin(), candidates.end());
//        dfs(candidates, target, 0);
//        return ret;
//    }
//};



//class Solution {
//    vector<string> ret;
//    string path;
//    int n;
//public:
//    void dfs(string& s, int i)
//    {
//        //返回条件
//        if (n == path.size())
//        {
//            ret.push_back(path);
//            return;
//        }
//        //每一个元素都有转换或者不转换的可能
//        //不转换
//        path += s[i];
//        dfs(s, i + 1);
//        path.pop_back();
//        //转换
//        if ('a' <= s[i] && s[i] <= 'z')
//        {
//            path += s[i] - 'a' + 'A';
//            dfs(s, i + 1);
//            path.pop_back();
//        }
//        if ('A' <= s[i] && s[i] <= 'Z')
//        {
//            path += s[i] - 'A' + 'a';
//            dfs(s, i + 1);
//            path.pop_back();
//        }
//
//    }
//    vector<string> letterCasePermutation(string s) {
//        n = s.size();
//        dfs(s, 0);
//        return ret;
//    }
//};



//class Solution {
//    bool vis[16];
//    int ret = 0;
//public:
//    void dfs(int n, int count)
//    {
//        if (count == n)
//        {
//            ++ret;
//            return;
//        }
//        for (int i = 1; i <= n; ++i)
//        {
//            if (vis[i]) continue;
//            //判断是否是合法的，剪枝
//            if (i % (count + 1) == 0 || (count + 1) % i == 0)
//            {
//                vis[i] = true;
//                dfs(n, count + 1);
//                vis[i] = false;
//            }
//        }
//    }
//    int countArrangement(int n) {
//        dfs(n, 0);
//        return ret;
//    }
//};


//class Solution {
//    vector<vector<string>> ret;
//    vector<string> board;  //棋盘
//    int _n;
//public:
//    void dfs(int i)
//    {
//        if (i == _n)
//        {
//            ret.push_back(board);
//            return;
//        }
//        for (int j = 0; j < _n; ++j)
//        {
//            //判断合法性
//            if (IsTrue(i, j))
//            {
//                board[i][j] = 'Q';
//                dfs(i + 1);
//                board[i][j] = '.';   //回溯
//            }
//        }
//    }
//    bool IsTrue(int i, int j)
//    {
//        //判断行
//        for (int x = j - 1; x >= 0; --x)
//        {
//            if (board[i][x] == 'Q') return false;
//        }
//        //判断列
//        for (int x = i - 1; x >= 0; --x)
//        {
//            if (board[x][j] == 'Q') return false;
//        }
//        //判断正斜
//        for (int x = i - 1, y = j + 1; x >= 0 && y < _n; --x, ++y)
//        {
//            if (board[x][y] == 'Q') return false;
//        }
//        //判断反斜
//        for (int x = i - 1, y = j - 1; x >= 0 && y >= 0; --x, --y)
//        {
//            if (board[x][y] == 'Q') return false;
//        }
//        return true;
//    }
//    vector<vector<string>> solveNQueens(int n) {
//        _n = n;
//        board.resize(n, string(n, '.'));
//        dfs(0);
//        return ret;
//    }
//};


//class Solution {
//    //利用hash来进行优化
//    bool hashCol[10] = { false };
//    bool hashdiff1[20] = { false };
//    bool hashdiff2[20] = { false };
//    vector<vector<string>> ret;
//    vector<string> board;  //棋盘
//    int _n;
//public:
//    void dfs(int i)
//    {
//        if (i == _n)
//        {
//            ret.push_back(board);
//            return;
//        }
//        for (int j = 0; j < _n; ++j)
//        {
//            //判断合法性
//            if (hashCol[j] || hashdiff1[i + j] || hashdiff2[i - j + _n]) continue;
//            {
//                board[i][j] = 'Q';
//                hashCol[j] = hashdiff1[i + j] = hashdiff2[i - j + _n] = true;
//                dfs(i + 1);
//                board[i][j] = '.';   //回溯
//                hashCol[j] = hashdiff1[i + j] = hashdiff2[i - j + _n] = false;
//            }
//        }
//    }
//    vector<vector<string>> solveNQueens(int n) {
//        _n = n;
//        board.resize(n, string(n, '.'));
//        dfs(0);
//        return ret;
//    }
//};


//class Solution {
//    //使用hash来记录行列，方格
//    bool hashRow[9][10];
//    bool hashCol[9][10];
//    bool hashlattic[3][3][10];
//public:
//    bool isValidSudoku(vector<vector<char>>& board) {
//        for (int i = 0; i < board.size(); ++i)
//        {
//            for (int j = 0; j < board[0].size(); ++j)
//            {
//                if (board[i][j] != '.')
//                {
//                    int num = board[i][j] - '0';
//                    if (hashRow[i][num] || hashCol[j][num] || hashlattic[i / 3][j / 3][num])
//                        return false;
//                    hashRow[i][num] = hashCol[j][num] = hashlattic[i / 3][j / 3][num] = true;
//                }
//            }
//        }
//        return true;
//    }
//};


//class Solution {
//    bool hashRow[9][10];
//    bool hashCol[9][10];
//    bool hashlattic[3][3][10];
//public:
//    void solveSudoku(vector<vector<char>>& board) {
//        //初始化
//        for (int i = 0; i < 9; ++i)
//        {
//            for (int j = 0; j < 9; ++j)
//            {
//                if (board[i][j] != '.')
//                {
//                    int num = board[i][j] - '0';
//                    hashRow[i][num] = hashCol[j][num] = hashlattic[i / 3][j / 3][num] = true;
//                }
//            }
//        }
//        dfs(board);
//    }
//    bool dfs(vector<vector<char>>& board)
//    {
//        for (int i = 0; i < 9; ++i)
//        {
//            for (int j = 0; j < 9; ++j)
//            {
//                if (board[i][j] == '.')
//                {
//                    for (int num = 1; num < 10; ++num)
//                    {
//                        if (hashRow[i][num] || hashCol[j][num] || hashlattic[i / 3][j / 3][num])
//                            continue;
//                        board[i][j] = num + '0';
//                        hashRow[i][num] = hashCol[j][num] = hashlattic[i / 3][j / 3][num] = true;
//                        if (dfs(board)) return true;
//                        board[i][j] = '.';
//                        hashRow[i][num] = hashCol[j][num] = hashlattic[i / 3][j / 3][num] = false;
//                    }
//                    return false;
//                }
//            }
//        }
//        return true;
//    }
//};



