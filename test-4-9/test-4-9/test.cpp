#include <iostream>
using namespace std;


//class Solution {
//public:
//    bool wordBreak(string s, unordered_set<string>& dict) {
//        //dp
//        int n = s.size();
//        vector<bool> result(n + 1, false);
//        //初始化
//        result[0] = true;
//        for (int i = 1; i <= n; ++i)
//        {
//            for (int j = i - 1; j >= 0; --j)
//            {
//                //状态转移方程
//                if (result[j] && dict.find(s.substr(j, i - j)) != dict.end())
//                {
//                    //给当前状态赋值为true
//                    result[i] = true;
//                    break;
//                }
//            }
//        }
//        return result[n];
//    }
//};

//class Solution {
//public:
//    int minimumTotal(vector<vector<int> >& triangle) {
//        //dp
//        int row = triangle.size();
//        vector<vector<int>> result(triangle);
//        //初始值 triangle[0][0] ,这里不需要定义了
//        for (int i = 1; i < row; ++i)
//        {
//            for (int j = 0; j <= i; ++j)
//            {
//                //判断左右边界
//                if (j == 0)
//                {
//                    result[i][j] = result[i - 1][j];
//                }
//                else if (i == j)
//                {
//                    result[i][j] = result[i - 1][j - 1];
//                }
//                else
//                {
//                    result[i][j] = min(result[i - 1][j - 1], result[i - 1][j]);
//                }
//                //最后加上当前值
//                result[i][j] += triangle[i][j];
//            }
//        }
//        // for(auto& v:result)
//        // {
//        //     for(auto& e:v)
//        //     {
//        //         cout<<e<<" ";
//        //     }
//        //     cout<<endl;
//        // }
//        //遍历最后一行，找最小
//        int minresult = result[row - 1][0];
//        for (int i = 1; i < row; ++i)
//        {
//            // if(minresult > result[row-1][0])
//            // {
//            //     minresult = result[row-1][0];
//            // }
//            minresult = min(result[row - 1][i], minresult);
//        }
//        return minresult;
//    }
//};


//class Solution {
//public:
//    int minimumTotal(vector<vector<int> >& triangle) {
//        //dp
//        if (triangle.size() == 1)
//        {
//            return triangle[0][0];
//        }
//        int row = triangle.size();
//        vector<vector<int>> vv(triangle);
//        for (int i = row - 2; i >= 0; --i)
//        {
//            for (int j = 0; j <= i; ++j)
//            {
//                //状态转移方程
//                vv[i][j] = min(vv[i + 1][j], vv[i + 1][j + 1]) + triangle[i][j];
//            }
//        }
//        return vv[0][0];
//    }
//};


//class Solution {
//public:
//    int uniquePaths(int m, int n) {
//        // write code here
//        if (m == 1 || n == 1)
//        {
//            return 1;
//        }
//        //dp    
//        vector<vector<int>> vv(m, vector<int>(n));
//        //第一行和第一列初始化为1
//        for (int i = 0; i < n; ++i)
//        {
//            vv[0][i] = 1;
//        }
//        for (int i = 0; i < m; ++i)
//        {
//            vv[i][0] = 1;
//        }
//        //遍历vv
//        for (int i = 1; i < m; ++i)
//        {
//            for (int j = 1; j < n; ++j)
//            {
//                //状态转移方程
//                vv[i][j] = vv[i - 1][j] + vv[i][j - 1];
//            }
//        }
//        // for(auto& v :vv)
//        // {
//        //     for(auto& e:v)
//        //     {
//        //         cout<<e<<" ";
//        //     }
//        //     cout<<endl;
//        // }
//        return vv[m - 1][n - 1];
//    }
//};

//#include <vector>
//int main()
//{
//	int m = 2;
//	int n = 2;
//	vector<vector<int> > ret(m, vector<int>(n, 1));
//    for (auto& v : ret)
//         {
//             for(auto& e:v)
//             {
//                 cout<<e<<" ";
//             }
//             cout<<endl;
//         }
//	return 0;
//}

//class Solution {
//public:
//    int minPathSum(vector<vector<int> >& grid) {
//        // write code here
//        //dp
//        int row = grid.size();
//        int col = grid[0].size();
//        //左边和上面需要单独处理,即初始化
//        for (int i = 1; i < row; ++i)
//        {
//            grid[i][0] = grid[i - 1][0] + grid[i][0];
//        }
//        for (int i = 1; i < col; ++i)
//        {
//            grid[0][i] = grid[0][i - 1] + grid[0][i];
//        }
//        for (int i = 1; i < row; ++i)
//        {
//            for (int j = 1; j < col; ++j)
//            {
//                //状态转移方程
//                grid[i][j] = min(grid[i - 1][j], grid[i][j - 1]) + grid[i][j];
//            }
//        }
//        return grid[row - 1][col - 1];
//    }
//};


