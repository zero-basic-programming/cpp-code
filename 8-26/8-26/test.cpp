#include <iostream>
#include <vector>
using namespace std;


//class Solution {
//    Node* guard;
//    Node* tail;
//    stack<Node*> st;   //通过记录next节点来防止在child递归的时候找不到之前节点的next
//public:
//    void dfs(Node* cur)
//    {
//        //返回条件
//        //这里如果next为空，但是st中有节点是不能直接返回的
//        if (cur == nullptr && st.empty()) return;
//        else if (cur == nullptr && st.size() > 0)
//        {
//            //把st中指针赋值给cur，然后往下执行
//            cur = st.top();
//            st.pop();
//        }
//        //插入当前值
//        tail->next = cur;
//        tail = tail->next;
//        //查看当前节点是否有子节点
//        if (cur->child)
//        {
//            st.push(cur->next);
//            dfs(cur->child);
//            return;  //这里不需要递归下一个节点，下一个节点有st记录了
//        }
//        //递归当前节点的下一个节点 
//        dfs(cur->next);
//    }
//    Node* flatten(Node* head) {
//        guard = new Node(0);   //哨兵位的头节点
//        tail = guard;
//        dfs(head);
//        Node* newHead = guard->next;
//        delete guard;
//        return newHead;
//    }
//};


//class Solution {
//    int n;
//public:
//    int Rob(vector<int>& nums, int left, int right)
//    {
//        vector<int> f(n), g(n);
//        //初始化
//        f[left] = nums[left];
//        for (int i = left + 1; i <= right; ++i)
//        {
//            f[i] = g[i - 1] + nums[i];
//            g[i] = max(f[i - 1], g[i - 1]);
//        }
//        return max(f[right], g[right]);
//    }
//    int rob(vector<int>& nums) {
//        //分成是否偷第一家两种情况,坐标使用闭区间
//        n = nums.size();
//        if (n == 1) return nums[0];   //处理特殊情况
//        int tmp1 = Rob(nums, 0, n - 2);
//        int tmp2 = Rob(nums, 1, n - 1);
//        return max(tmp1, tmp2);
//    }
//};

//
//class Solution {
//public:
//    bool isAnagram(string s, string t) {
//        int ret = 0;
//        for (auto ch : s) ret ^= ch;
//        for (auto ch : t) ret ^= ch;
//        return ret == 0 ? true : false;
//    }
//};


//class Solution {
//public:
//    bool isAnagram(string s, string t) {
//        //先用hash来处理次数相同的条件，再遍历一次来处理顺序不完全相同
//        if (s.size() != t.size()) return false;
//        int n = s.size();
//        int hash[26] = { 0 };
//        for (auto ch : s) ++hash[ch - 'a'];
//        for (auto ch : t)
//        {
//            if (--hash[ch - 'a'] < 0) return false;
//        }
//        for (int i = 0; i < n; ++i)
//        {
//            if (s[i] != t[i]) return true;
//        }
//        return false;
//    }
//};


//class Solution {
//public:
//    vector<vector<int>> threeSum(vector<int>& nums) {
//        vector<vector<int>> ret;
//        int n = nums.size();
//        int i = 0;
//        sort(nums.begin(), nums.end());
//        while (i < n - 2)
//        {
//            int left = i + 1, right = n - 1;
//            int target = -nums[i];
//            while (left < right)
//            {
//                if (nums[left] + nums[right] > target) --right;
//                else if (nums[left] + nums[right] < target) ++left;
//                else
//                {
//                    //收集结果 + 去重
//                    ret.push_back({ nums[i],nums[left++],nums[right--] });
//                    while (left < right && nums[left] == nums[left - 1]) ++left;
//                    while (left < right && nums[right] == nums[right + 1]) --right;
//                }
//            }
//            ++i;
//            //对i进行去重
//            while (i < n - 2 && nums[i] == nums[i - 1]) ++i;
//        }
//        return ret;
//    }
//};


