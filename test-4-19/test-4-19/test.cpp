#include <iostream>
#include <vector>
using namespace std;

//class Solution {
//public:
//    bool is_sqrt(int n)
//    {
//        for (int i = 1; n > 0; i += 2)
//        {
//            n -= i;
//        }
//        return 0 == n;
//    }
//    void find_sqrt(vector<int>& nums, int n)
//    {
//        for (int i = 1; i <= n; ++i)
//        {
//            if (is_sqrt(i))
//            {
//                nums.push_back(i);
//            }
//        }
//    }
//    int numSquares(int n) {
//        vector<int> dp(n + 1, INT_MAX);
//        //初始化
//        dp[0] = 0;
//        //把1到n的完全平方数找出放入数组中
//        vector<int> nums;
//        find_sqrt(nums, n);
//        for (int i = 0; i < nums.size(); ++i) // 先遍历物品
//        {
//            for (int j = nums[i]; j <= n; ++j) //遍历背包
//            {
//                //状态转移方程
//                dp[j] = min(dp[j], dp[j - nums[i]] + 1);
//            }
//        }
//        if (dp[n] == INT_MAX)    return -1;
//        return dp[n];
//    }
//};


class Solution {
public:
    int numSquares(int n) {
        vector<int> dp(n + 1, INT_MAX);
        //初始化
        dp[0] = 0;
        for (int i = 1; i * i <= n; ++i) // 遍历物品
        {
            for (int j = i * i; j <= n; ++j) //遍历背包
            {
                //状态转移方程
                dp[j] = min(dp[j - i * i] + 1, dp[j]);
            }
        }
        return dp[n];
    }
};