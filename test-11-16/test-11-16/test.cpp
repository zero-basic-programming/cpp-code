#include <iostream>
#include <vector>
using namespace std;

 
//class Solution {
//public:
//    ListNode* reverseList(ListNode* head) {
//        //返回条件
//        if (head == nullptr || head->next == nullptr)
//            return head;
//        //先遍历到最后一个链表，同时记录一下返回的头节点
//        ListNode* newhead = reverseList(head->next);
//        //单次递归
//        head->next->next = head;
//        head->next = nullptr;
//        return newhead;
//    }
//};


//class Solution {
//public:
//    ListNode* swapPairs(ListNode* head) {
//        //返回
//        if (head == nullptr || head->next == nullptr)
//            return head; //处理一个或者没有的情况
//        //单次递归
//        ListNode* newhead = swapPairs(head->next->next);
//        ListNode* l1 = head;
//        ListNode* l2 = head->next;
//        //l2连接l1 ，l1连接后面的节点，返回l2
//        l2->next = l1;
//        l1->next = newhead;
//        return l2;
//    }
//};


//class Solution {
//public:
//    int maxProfit(vector<int>& prices) {
//        int ret = 0;
//        int minprice = prices[0];
//        for (int i = 1; i < prices.size(); ++i)
//        {
//            if (prices[i] < prices[i - 1])
//            {
//                //收集结果了,同时准备下一次购买
//                if (minprice != prices[i - 1])
//                {
//                    //这里才需要计算利润
//                    ret += (prices[i - 1] - minprice);
//                }
//                //有可能一直处于下降趋势
//                minprice = prices[i];
//            }
//        }
//        //最后可能一直处于上升趋势
//        if (minprice != prices.back()) ret += (prices.back() - minprice);
//        return ret;
//    }
//};



//class Solution {
//public:
//    int maxProfit(vector<int>& prices) {
//        //通过一天一天的方式，就是把每一份都细分都最小
//        int ret = 0;
//        for (int i = 1; i < prices.size(); ++i)
//        {
//            if (prices[i] > prices[i - 1])
//            {
//                ret += (prices[i] - prices[i - 1]);
//            }
//        }
//        return ret;
//    }
//};


//class Solution {
//public:
//    int largestSumAfterKNegations(vector<int>& nums, int k) {
//        int m = 0;//记录负数的个数
//        int minabs = INT_MAX; //绝对值最小的数
//        int ret = 0;
//        for (auto e : nums) {
//            if (e < 0) ++m;
//            minabs = min(abs(e), minabs);
//        }
//        //根据m和k的大小进行讨论
//        if (m >= k)
//        {
//            sort(nums.begin(), nums.end());
//            //全部转换
//            int i = 0;
//            for (; i < k; ++i)
//            {
//                ret += -nums[i];
//            }
//            //剩下的都是直接加
//            for (; i < nums.size(); ++i)
//            {
//                ret += nums[i];
//            }
//        }
//        else
//        {
//            for (auto e : nums) ret += abs(e);
//            cout << ret << " " << minabs << endl;
//            if ((k - m) % 2)
//            {
//                //奇数情况
//                ret -= 2 * minabs;
//            }
//        }
//        return ret;
//    }
//};