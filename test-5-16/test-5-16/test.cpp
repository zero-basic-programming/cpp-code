#include <iostream>
using namespace std;

//class Solution {
//public:
//    TreeNode* insertIntoBST(TreeNode* root, int val) {
//        //利用二叉搜索树的性质，一直找到空，那就是我们需要插入的位置
//        if (root == nullptr)
//        {
//            TreeNode* node = new TreeNode(val);
//            return node;
//        }
//        if (root->val > val) root->left = insertIntoBST(root->left, val);
//        if (root->val < val) root->right = insertIntoBST(root->right, val);
//        return root;
//    }
//};



//class Solution {
//public:
//    TreeNode* insertIntoBST(TreeNode* root, int val) {
//        //使用迭代方式
//        if (root == nullptr)
//        {
//            root = new TreeNode(val);
//            return root;
//        }
//        TreeNode* node = root;
//        TreeNode* parent = nullptr; // 记录一下当前节点的父节点
//        while (node)
//        {
//            parent = node; //更新父节点
//            if (node->val > val)
//            {
//                node = node->left;
//            }
//            else if (node->val < val)
//            {
//                node = node->right;
//            }
//        }
//        //这里node走到空的时候就是结果
//        if (parent->val > val)
//        {
//            //往左边插入
//            parent->left = new TreeNode(val);
//        }
//        else
//        {
//            parent->right = new TreeNode(val);
//        }
//        return root;
//    }
//};


//class Solution {
//public:
//    TreeNode* deleteNode(TreeNode* root, int key) {
//        //使用迭代法，找到该节点，然后找到该节点的右子树的最小节点来替代删除
//        TreeNode* node = root;
//        if (root == nullptr) return nullptr;
//        TreeNode* parent = nullptr; //记录祖先节点
//        while (node)
//        {
//            parent = node;
//            if (node->val > key)
//            {
//                node = node->left;
//            }
//            else if (node->val < key)
//            {
//                node = node->right;
//            }
//            else {
//                //找到了
//                break;
//            }
//        }
//        if (node == nullptr)
//        {
//            //整棵树都没有要找的节点
//            return root;
//        }
//        //找该节点的右数的最小节点
//        TreeNode* tmp = node->right;
//        if (tmp == nullptr)
//        {
//            //直接删除该节点，然后把该节点的左子树挂给祖父节点
//            TreeNode* left = node->left;
//            delete node;
//            parent->left = left;
//            return root;
//        }
//        //找到使用伪删除
//        if (tmp->left == nullptr)
//        {
//            node->val = tmp->val;
//            node->right = tmp->right;
//            delete tmp;
//            return root;
//        }
//        TreeNode* pre = tmp;
//        while (tmp->left)
//        {
//            pre = tmp;
//            tmp = tmp->left;
//        }
//        //伪删除
//        node->val = tmp->val;
//        pre->left = nullptr;
//        delete tmp;
//        return root;
//    }
//};


