#include <iostream>
using namespace std; 

//class Solution {
//public:
//    int totalFruit(vector<int>& fruits) {
//        //使用hash来记录已经放入的元素的个数
//        unordered_map<int, int> m;
//        //使用滑动窗口
//        int l = 0;
//        int len = 0;//记录结果
//        for (int r = 0; r < fruits.size(); ++r)
//        {
//            //无论什么情况都是直接插入hash中
//            m[fruits[r]]++;
//            //如果超过2个就要出来
//            while (m.size() > 2)
//            {
//                --m[fruits[l]];
//                if (m[fruits[l]] == 0)
//                {
//                    m.erase(fruits[l]);
//                }
//                ++l;
//            }
//            //更新一下len
//            len = max(len, r - l + 1);
//        }
//        return len;
//    }
//};

//class Solution {
//public:
//    unordered_map <char, int> ori, cnt;
//
//    bool check() {
//        for (const auto& p : ori) {
//            if (cnt[p.first] < p.second) {
//                return false;
//            }
//        }
//        return true;
//    }
//
//    string minWindow(string s, string t) {
//        for (const auto& c : t) {
//            ++ori[c];
//        }
//
//        int l = 0, r = -1;
//        int len = INT_MAX, ansL = -1, ansR = -1;
//
//        while (r < int(s.size())) {
//            if (ori.find(s[++r]) != ori.end()) {
//                ++cnt[s[r]];
//            }
//            while (check() && l <= r) {
//                if (r - l + 1 < len) {
//                    len = r - l + 1;
//                    ansL = l;
//                }
//                if (ori.find(s[l]) != ori.end()) {
//                    --cnt[s[l]];
//                }
//                ++l;
//            }
//        }
//
//        return ansL == -1 ? string() : s.substr(ansL, len);
//    }
//};

//class Solution {
//public:
//    ListNode* removeElements(ListNode* head, int val) {
//        ListNode* guard = new ListNode(0);
//        guard->next = head;
//        ListNode* cur = head;
//        ListNode* prev = guard;
//        while (cur)
//        {
//            if (cur->val == val)
//            {
//                prev->next = cur->next;
//                ListNode* tmp = cur;//使用记录要释放的节点
//                cur = cur->next;
//                delete tmp;
//            }
//            else
//            {
//                prev = cur;
//                cur = cur->next;
//            }
//        }
//        ListNode* newhead = guard->next;
//        delete guard;
//        return newhead;
//    }
//};

//class Solution {
//public:
//    ListNode* listreverse(ListNode* pre, ListNode* cur)
//    {
//        if (cur == nullptr)
//        {
//            return pre;
//        }
//        //先记录第三个节点
//        ListNode* tmp = cur->next;
//        //改变链接
//        cur->next = pre;
//        return listreverse(cur, tmp);
//    }
//    ListNode* reverseList(ListNode* head) {
//        //使用递归法求解
//        return listreverse(nullptr, head);
//    }
//};

//class Solution {
//public:
//    ListNode* swapPairs(ListNode* head) {
//        if (head == nullptr || head->next == nullptr)
//        {
//            return head;
//        }
//        //使用三个指针解决
//        ListNode* cur = head;
//        ListNode* prev = head->next;
//        //这里要顺便更新一下head
//        head = prev;
//        ListNode* next = prev->next;
//        while (next && next->next)
//        {
//            prev->next = cur;
//            if (next->next)
//            {
//                cur->next = next->next;
//                //这里的关系是反过来的
//                prev = next->next;
//                cur = next;
//                next = prev->next;
//            }
//        }
//        //这里还有最后一次没有处理
//        if (next)
//        {
//            //奇数个节点的情况
//            prev->next = cur;
//            cur->next = next;
//            next->next = nullptr;
//        }
//        else
//        {
//            //偶数个节点的情况
//            prev->next = cur;
//            cur->next = nullptr;
//        }
//        return head;
//    }
//};

