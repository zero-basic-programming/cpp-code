#include <iostream>
#include <vector>
using namespace std;


//class Solution {
//public:
//    int longestSubsequence(vector<int>& arr, int difference) {
//        int n = arr.size();
//        vector<int> dp(n, 1);
//        int ret = 0;
//        for (int i = 1; i < n; ++i)
//        {
//            for (int j = i - 1; j >= 0; --j)
//            {
//                if (arr[i] - arr[j] == difference)
//                {
//                    //在前面找结果
//                    dp[i] = dp[j] + 1;
//                    ret = max(ret, dp[i]);
//                    break;
//                }
//            }
//        }
//        return dp[n - 1];
//    }
//};
//
//int main()
//{
//    vector<int> arr = { 3,4,-3,-2,-4 };
//    Solution().longestSubsequence(arr, -5);
//    return 0;
//}


//class Solution {
//public:
//    int longestSubsequence(vector<int>& arr, int difference) {
//        int n = arr.size();
//        unordered_map<int, int> hash;
//        //初始化
//        hash[arr[0]] = 1;
//        int ret = 1;
//        for (int i = 1; i < n; ++i)
//        {
//            //如果没有找到，那么这里的值就是0，找到就是这里的值+1
//            hash[arr[i]] = hash[arr[i] - difference] + 1;
//            ret = max(hash[arr[i]], ret);
//        }
//        return ret;
//    }
//};


//class Solution {
//public:
//    int findPoisonedDuration(vector<int>& timeSeries, int duration) {
//        int ret = duration;
//        //记录第一次中毒的时间
//        int begin = timeSeries[0];
//        int end = timeSeries[0] + duration - 1;
//        for (int i = 1; i < timeSeries.size(); ++i)
//        {
//            ret += duration;
//            //算上一次开始和这一次结束差值
//            begin = timeSeries[i];
//            if (end >= begin)
//            {
//                ret -= (end - begin + 1);
//            }
//            end = timeSeries[i]+ duration - 1;
//            cout << ret << " ";
//        }
//        return ret;
//    }
//};

//int main()
//{
//    vector<int> arr = { 1,2,3,4,5 };
//    int ret = Solution().findPoisonedDuration(arr, 5);
//    cout << ret << endl;
//}
//class Solution {
//public:
//    int findPoisonedDuration(vector<int>& timeSeries, int duration) {
//        int ret = duration;
//        //这里先计算最后一个，因为最后一个中毒之后是不会重置的
//        for (int i = 0; i < timeSeries.size() - 1; ++i)
//        {
//            int x = timeSeries[i + 1] - timeSeries[i];
//            if (x >= duration) ret += duration;
//            else ret += x;
//        }
//        return ret;
//    }
//};


