#include <iostream>
using namespace std;

#include <vector>
//class A
//{
//public:
//	void test(float a)
//	{
//		cout << a;
//	}
//};
//
//class B : public A
//{
//public:
//	void test(int b)
//	{
//		cout << b;
//	}
//};


//class A
//{
//public:
//	virtual void f()
//	{
//		cout << "A::f()" << endl;
//	}
//};
//
//class B : public A
//{
//private:
//	virtual void f()
//	{
//		cout << "B::f()" << endl;
//	}
//};
//int main()
//{
//	A* pa = (A*)new B;
//	pa->f();
//	return 0;
//}
//
////int main()
////{
////	A* a = new A;
////	B* b = new B;
////	a = b;
////	a->test(1.1);
////	return 0;
////}


//class Solution {
//public:
//    vector<vector<int>> levelOrderBottom(TreeNode* root) {
//        vector<vector<int>> result;
//        if (root == nullptr)
//            return result;
//        queue<TreeNode*> q;
//        q.push(root);
//        while (!q.empty())
//        {
//            vector<int> tmp;
//            int size = q.size();
//            while (size--)
//            {
//                TreeNode* frontnode = q.front();
//                q.pop();
//                tmp.push_back(frontnode->val);
//                //放入左右节点
//                if (frontnode->left)
//                {
//                    q.push(frontnode->left);
//                }
//                if (frontnode->right)
//                {
//                    q.push(frontnode->right);
//                }
//            }
//            result.push_back(tmp);
//        }
//        reverse(result.begin(), result.end());
//        return result;
//    }
//};

//class Solution {
//public:
//    vector<vector<int>> levelOrder(TreeNode* root) {
//        vector<vector<int>> result;
//        if (root == nullptr)
//            return result;
//        queue<TreeNode*> q;
//        q.push(root);
//        while (!q.empty())
//        {
//            vector<int> tmp;
//            int size = q.size();
//            while (size--)
//            {
//                TreeNode* frontnode = q.front();
//                q.pop();
//                tmp.push_back(frontnode->val);
//                //放入左右节点
//                if (frontnode->left)
//                {
//                    q.push(frontnode->left);
//                }
//                if (frontnode->right)
//                {
//                    q.push(frontnode->right);
//                }
//            }
//            result.push_back(tmp);
//        }
//        return result;
//    }
//};

//class Solution {
//public:
//    void _tree2str(TreeNode* root, string& result)
//    {
//        if (root == nullptr)
//        {
//            result += "";
//            return;
//        }
//        result += to_string(root->val);
//        if (root->left || root->right)
//        {
//            result += "(";
//            _tree2str(root->left, result);
//            result += ")";
//        }
//        if (root->right)
//        {
//            result += "(";
//            _tree2str(root->right, result);
//            result += ")";
//        }
//    }
//    string tree2str(TreeNode* root) {
//        string result;
//        _tree2str(root, result);
//        return result;
//    }
//};

