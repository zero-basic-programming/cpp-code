#include <iostream>
#include <vector>
using namespace std;

//class Solution {
//public:
//    vector<int> diStringMatch(string s) {
//        int left = 0, right = s.size();
//        vector<int> ret;
//        for (auto ch : s)
//        {
//            if (ch == 'I') ret.push_back(left++);
//            else ret.push_back(right--);
//        }
//        ret.push_back(left);
//        return ret;
//    }
//};


//class Solution {
//public:
//    int findContentChildren(vector<int>& g, vector<int>& s) {
//        int count = 0;
//        sort(g.begin(), g.end());
//        sort(s.begin(), s.end());
//        int i = 0, j = 0;
//        while (i < g.size() && j < s.size())
//        {
//            if (s[j] >= g[i])
//            {
//                ++count;
//                ++j;
//                ++i;
//            }
//            else
//            {
//                //不够吃，用下一个饼干来满足当前孩子
//                ++j;
//            }
//        }
//        return count;
//    }
//};


void Qsort(vector<int>& arr, int l, int r)
{
	//返回
	if (l >= r) return;
	//找基准点
	int key = arr[(rand() % (r - l + 1) + l)];
	int i = l, left = l - 1, right = r + 1;
	while (i < right)
	{
		if (arr[i] < key) swap(arr[i++], arr[++left]);
		else if (arr[i] == key) ++i;
		else swap(arr[i], arr[--right]);
	}
	//[l,left] [left+1，right-1] [right,r]
	Qsort(arr, l, left);
	Qsort(arr, right, r);
}

void MergeSort(vector<int>& arr, int left, int right, vector<int>& tmp)
{
	if (left >= right) return;
	//找中间点
	int mid = (left + right) >> 1;
	//先归并
	//[left,mid] [mid+1,right]
	MergeSort(arr, left, mid,tmp);
	MergeSort(arr, mid + 1, right,tmp);
	int begin1 = left, begin2 = mid + 1;
	int i = left;
	while (begin1 <= mid && begin2 <= right)
	{
		tmp[i++] = arr[begin1] < arr[begin2] ? arr[begin1++] : arr[begin2++];
	}
	//排序剩下的数据
	while (begin1 <= mid)
	{
		tmp[i++] = arr[begin1++];
	}
	while (begin2 <= right)
	{
		tmp[i++] = arr[begin2++];
	}
	//还原回原来的数组
	for (int j = left; j <= right; ++j)
		arr[j] = tmp[j];
}

int main()
{
	srand(time(nullptr));
	vector<int> arr = { 1,2,5,7,4,7,8,5,8,9,0,4 };
	vector<int> tmp(arr.size());
	//Qsort(arr, 0, arr.size() - 1);
	MergeSort(arr, 0, arr.size() - 1, tmp);
	for (auto e : arr)
	{
		cout << e << " ";
	}
	return 0;
}