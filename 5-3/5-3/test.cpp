#include <iostream>
#include <vector>
using namespace std;


//class Solution {
//public:
//    bool dfs(TreeNode* a, TreeNode* b)
//    {
//        if (a == nullptr && b == nullptr) return true;
//        else if (a == nullptr && b) return false;
//        else if (a && b == nullptr) return true;
//        if (a->val != b->val) return false;
//        if (dfs(a->left, b->left) == false) return false;
//        if (dfs(a->right, b->right) == false) return false;
//        return true;
//    }
//    bool isSubStructure(TreeNode* A, TreeNode* B) {
//        //比较每一个A的节点和B的根节点是否相同，相同则递归其左右子树
//        //返回条件
//        if (B == nullptr && A == nullptr) return false;
//        else if (A == nullptr && B) return false;
//        else if (A && B == nullptr) return false;
//        if (A->val == B->val)
//        {
//            if (dfs(A, B)) return true;
//        }
//        if (isSubStructure(A->left, B)) return true;
//        if (isSubStructure(A->right, B)) return true;
//        return false;
//    }
//};


//class Solution {
//public:
//    bool isSubStructure(TreeNode* A, TreeNode* B) {
//        return (A && B) &&
//            (dfs(A, B) || isSubStructure(A->left, B) || isSubStructure(A->right, B));
//    }
//    bool dfs(TreeNode* a, TreeNode* b)
//    {
//        if (b == nullptr) return true;  //这里B该分支是没有节点的，满足要求
//        if (a == nullptr || a->val != b->val) return false;
//        return dfs(a->left, b->left) && dfs(a->right, b->right);
//    }
//};


//class Solution {
//public:
//    TreeNode* mirrorTree(TreeNode* root) {
//        //通过后序遍历进行反转
//        if (root == nullptr) return root;
//        mirrorTree(root->left);
//        mirrorTree(root->right);
//        TreeNode* tmp = root->left;
//        root->left = root->right;
//        root->right = tmp;
//        return root;
//    }
//};


//class Solution {
//    vector<vector<int>> ret;
//    vector<int> path;
//public:
//    void dfs(vector<int>& candidates, int target, int sum, int index)
//    {
//        if (sum > target) return;
//        if (sum == target)
//        {
//            ret.push_back(path);
//            return;
//        }
//        for (int i = index; i < candidates.size(); ++i)
//        {
//            path.push_back(candidates[i]);
//            dfs(candidates, target, sum + candidates[i], i);
//            //回溯
//            path.pop_back();
//        }
//    }
//    vector<vector<int>> combinationSum(vector<int>& candidates, int target) {
//        dfs(candidates, target, 0, 0);
//        return ret;
//    }
//};


//class Solution {
//public:
//    string addBinary(string a, string b) {
//        string ret;
//        int i = a.size() - 1, j = b.size() - 1;
//        int carry = 0;  //计算进制
//        while (i >= 0 || j >= 0 || carry)
//        {
//            if (i >= 0) carry += a[i--] - '0';
//            if (j >= 0) carry += b[j--] - '0';
//            ret += carry % 2 + '0';
//            carry /= 2;
//        }
//        //反转ret
//        reverse(ret.begin(), ret.end());
//        //处理前导零
//        int k = 0;
//        while (k < ret.size())
//        {
//            if (ret[k] != '0') break;
//            ++k;
//        }
//        return (k != 0 && ret[0] == '0') ? ret : ret.substr(k);
//    }
//};


