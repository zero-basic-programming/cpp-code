#include "RBTree.h"

//class Solution {
//    vector<vector<int>> ret;
//    vector<bool> vis;
//    vector<int> path;
//public:
//    void dfs(vector<int>& candidates, int target, int sum, int pos)
//    {
//        //返回
//        if (sum > target) return;
//        else if (sum == target)
//        {
//            //收集结果，然后进行返回
//            ret.push_back(path);
//            return;
//        }
//        for (int i = pos; i < candidates.size(); ++i)
//        {
//            //查看当前元素是否使用过，不同层不能重复，同层可以
//            if (i > 0 && candidates[i] == candidates[i - 1] && !vis[i - 1]) continue;
//            path.push_back(candidates[i]);
//            vis[i] = true;
//            dfs(candidates, target, sum + candidates[i], i + 1);
//            vis[i] = false;
//            path.pop_back();  //回溯
//        }
//    }
//    vector<vector<int>> combinationSum2(vector<int>& candidates, int target) {
//        int n = candidates.size();
//        vis.resize(n, false);
//        sort(candidates.begin(), candidates.end());
//        dfs(candidates, target, 0, 0);
//        return ret;
//    }
//};


//class Solution {
//public:
//    int singleNumber(vector<int>& nums) {
//        //核心思路：遍历每一个比特位，找出特殊数字的有1的位数进行操作
//        int ret = 0;
//        for (int i = 0; i < 32; ++i) //遍历每一位
//        {
//            //遍历每一个数字
//            int sum = 0;  //记录该位含1的个数
//            for (auto e : nums)
//            {
//                if ((e >> i) & 1) ++sum;
//            }
//            sum %= 3;  //这里把其他位数含1的全部舍弃
//            if (sum == 1) ret |= (1 << i);
//        }
//        return ret;
//    }
//};


//class Solution {
//    vector<vector<int>> ret;    //存放最终结果
//    vector<int> path;           //存放临时结果
//    vector<bool> vis;           //存放该元素是否使用过的信息
//    int n;                      //存放nums元素个数
//public:
//    void dfs(vector<int>& nums)
//    {
//        if (path.size() == n)
//        {
//            //收集结果
//            ret.push_back(path);
//            return;
//        }
//        for (int i = 0; i < n; ++i)
//        {
//            if (vis[i]) continue;   //已经使用过了
//            path.push_back(nums[i]);
//            vis[i] = true;
//            dfs(nums);
//            vis[i] = false;
//            path.pop_back();  //回溯
//        }
//    }
//    vector<vector<int>> permute(vector<int>& nums) {
//        n = nums.size();
//        vis.resize(n, false);
//        dfs(nums);
//        return ret;
//    }
//};

//class Solution {
//    vector<vector<int>> ret;
//    vector<int> path;
//    vector<bool> vis;
//    int n;
//public:
//    void dfs(vector<int>& nums)
//    {
//        if (path.size() == n)
//        {
//            ret.push_back(path);
//            return;
//        }
//        for (int i = 0; i < n; ++i)
//        {
//            //前面有使用过，或者和上一个相等的前面那个没使用过的需要跳过
//            if (vis[i]) continue;
//            if (i > 0 && nums[i - 1] == nums[i] && !vis[i - 1]) continue;
//            path.push_back(nums[i]);
//            vis[i] = true;
//            dfs(nums);
//            vis[i] = false;
//            path.pop_back();
//        }
//    }
//    vector<vector<int>> permuteUnique(vector<int>& nums) {
//        sort(nums.begin(), nums.end());
//        n = nums.size();
//        vis.resize(n, false);
//        dfs(nums);
//        return ret;
//    }
//};

int main()
{
	liang::TestRBTree();
	return 0;
}


