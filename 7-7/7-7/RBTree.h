#pragma once
#include <iostream>

namespace liang
{
	//枚举颜色
	enum Color
	{
		RED,
		BLACK
	};
	//节点类
	template <class T>
	class RBTreeNode
	{
	public:
		RBTreeNode(T val)
			:_val(val), _parent(nullptr),_left(nullptr), _right(nullptr),_col(RED)
		{}
		T _val;					//节点中的值
		RBTreeNode<T>* _parent;    //父节点
		RBTreeNode<T>* _left;		//左孩子
		RBTreeNode<T>* _right;		//右孩子
		Color _col;				//默认都是红色，因为红色调整的少
	};
	//红黑树类
	template <class T>
	class RBTree
	{
		typedef RBTreeNode<T> Node;
	public:
		RBTree()
			:_root(nullptr)
		{}
		bool insert(const T& data)
		{
			//空树的情况
			if (_root == nullptr)
			{
				_root = new Node(data);
				_root->_col = BLACK;   //跟永远是黑色的
				return true;
			}
			//正常的二叉树的插入
			Node* cur = _root;
			Node* prev = nullptr;
			while (cur)
			{
				if (cur->_val > data)
				{
					//往左边插入
					prev = cur;
					cur = cur->_left;
				}
				else if (cur->_val < data)
				{
					//往右边插入
					prev = cur;
					cur = cur->_right;
				}
				else
				{
					//如果是等于的情况直接返回，因为这里插入的数据是不重复的
					return false;
				}
			}
			//此处cur为空，可以直接插入
			cur = new Node(data);
			//平衡调整，向上调平
			while (cur != _root)
			{
				//当前节点和父亲节点颜色一样且都为红需要进行调整
				if (cur->_col == RED && prev->_col == RED)
				{
					Node* gparent = prev->_parent;
					if (gparent->_left == prev)
					{
						//uncle在右
						Node* uncle = gparent->_right;
						//情况1：uncle存在,且为红，直接把uncle和prev颜色变成黑，然后向上迭代更新
						if (uncle && uncle->_col == RED)
						{
							uncle->_col = prev->_col = BLACK;
							gparent->_col = RED;
							//向上迭代
							cur = gparent;
							prev = gparent->_parent;
						}
						else
						{
							//情况2，3：需要进行旋转+变色进行处理
							//判断cur是prev的左孩子还是右孩子
							if (prev->_left == cur)	  //右旋转
							{
								RolateR(gparent);
								prev->_col = BLACK;
								gparent->_col = RED;
							}
							else   //左右双旋
							{
								RolateLR(gparent);
								gparent->_col = RED;
								cur->_col = BLACK;
							}
						}
					}
					else
					{
						//uncle在左
						Node* uncle = gparent->_left;
						//1.uncle存在,且为红，直接把uncle和prev颜色变成黑，然后向上迭代更新
						if (uncle && uncle->_col == RED)
						{
							uncle->_col = prev->_col = BLACK;
							gparent->_col = RED;
							//向上迭代更新
							cur = gparent;
							prev = gparent->_parent;
						}
						else
						{
							//2.3uncle不存在，或者uncle存在且为黑：旋转加变色
							if (prev->_right == cur)
							{
								//右旋
								RolateR(gparent);
								gparent->_col = RED;
								prev->_col = BLACK;
							}
							else
							{
								//右左双旋
								RolateRL(gparent);
								gparent->_col = RED;
								cur->_col = BLACK;
							}
						}
					}
				}
				else break;  //已经调整平衡
			}
			//这里最后把根节点变成黑
			_root->_col = BLACK;
			return true;
		}
		void RolateL(Node* cur)
		{
			Node* parent = cur->_parent;
			Node* subR = cur->_right;
			Node* subRL = subR->_left;
			//链接cur和subR
			cur->_parent = subR;
			subR->_left = cur;
			//链接cur和subRL
			cur->_right = subRL;
			if (subRL) subRL->_parent = cur;
			//链接subR和parent
			subR->_parent = parent;
			if (parent->_left == cur)
				parent->_left = subR;
			else parent->_right = subR;
		}
		void RolateR(Node* cur)
		{
			Node* subL = cur->_left;
			Node* parent = cur->_parent;
			Node* subLR = subL->_right;
			//cur和subL互相建立链接
			cur->_parent = subL;
			subL->_right = cur;
			//链接cur和subLR
			cur->_left = subLR;
			//这里subLR可能是空
			if (subLR) subLR->_parent = cur;
			//链接subL和parent
			subL->_parent = parent;
			if (parent->_left == cur)	parent->_left = subL;
			else	parent->_right = subL;
		}
		void RolateLR(Node* cur)
		{
			RolateL(cur->_left);
			RolateR(cur);
		}
		void RolateRL(Node* cur)
		{
			RolateR(cur->_right);
			RolateL(cur);
		}
		void dfs(Node* cur)
		{
			if (cur == nullptr) return;
			dfs(cur->_left);
			std::cout << cur->_val << " : " << cur->_col << std::endl;
			dfs(cur->_right);
		}
		void PrintTree()
		{
			dfs(_root);
		}
	private:
		Node* _root;
	};
	void TestRBTree()
	{
		RBTree<int> tree;
		tree.insert(1);
		tree.insert(2);
		tree.insert(3);
		tree.insert(4);
		tree.PrintTree();
	}
}   //namespace liang

