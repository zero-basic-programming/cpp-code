#include <iostream>
#include <vector>
using namespace std;


//class Solution {
//    int m, n;
//    typedef pair<int, int> PII;
//    int dx[4] = { 1,-1,0,0 };
//    int dy[4] = { 0,0,1,-1 };
//public:
//    vector<vector<int>> floodFill(vector<vector<int>>& image, int sr, int sc, int color) {
//        int prev = image[sr][sc]; //记录一下原来的数字
//        if (prev == color) return image;  //处理边界条件
//        m = image.size(), n = image[0].size();
//        queue<PII> q;
//        q.push({ sr,sc });
//        while (q.size())
//        {
//            auto [a, b] = q.front();
//            q.pop();
//            image[a][b] = color;
//            //遍历四个方向
//            for (int i = 0; i < 4; ++i)
//            {
//                int x = a + dx[i], y = b + dy[i];
//                if (x >= 0 && x < m && y >= 0 && y < n && image[x][y] == prev)
//                {
//                    q.push({ x,y });
//                }
//            }
//        }
//        return image;
//    }
//};


//class Solution {
//    int m, n;
//    int dx[4] = { 0,0,1,-1 };
//    int dy[4] = { 1,-1,0,0 };
//    typedef pair<int, int> PII;
//    bool vis[301][301];  //用来标记当前元素是否已经记录过
//public:
//    int numIslands(vector<vector<char>>& grid) {
//        m = grid.size(), n = grid[0].size();
//        int ret = 0;
//        for (int i = 0; i < m; ++i)
//        {
//            for (int j = 0; j < n; ++j)
//            {
//                if (!vis[i][j] && grid[i][j] == '1')
//                {
//                    bfs(grid, i, j);
//                    ++ret;
//                }
//            }
//        }
//        return ret;
//    }
//    void bfs(vector<vector<char>>& grid, int i, int j)
//    {
//        queue<PII> q;
//        q.push({ i,j });
//        vis[i][j] = true;
//        while (q.size())
//        {
//            auto [a, b] = q.front();
//            q.pop();
//            //从4个方向迭代
//            for (int k = 0; k < 4; ++k)
//            {
//                int x = a + dx[k], y = b + dy[k];
//                if (x >= 0 && x < m && y >= 0 && y < n && grid[x][y] == '1' && !vis[x][y])
//                {
//                    q.push({ x,y });
//                    vis[x][y] = true;
//                }
//            }
//        }
//    }
//};


//int main()
//{
//	char* str1 = (char*)"abc";
//	char* str2 = (char*)"abc";
//	printf("%p\n", str1);
//	printf("%p\n", str2);
//	*str1 = (char*)"cbf";
//	printf("%p\n", str1);
//	return 0;
//}

#include <cstdio>
int main() {
	long long a = 1, b = 2, c = 3;
	printf("%d %d %d\n", a, b, c);
	return 0;
}


//int main()
//{
//	int a = 1;
//	char b = (char)a;
//	if (b == 0)
//	{
//		cout << "大端" << endl;
//	}
//	else cout << "小端" << endl;
//	return 0;
//}