#include <iostream>
#include <vector>
using namespace std;

//class Solution {
//public:
//    void reorderList(ListNode* head) {
//        if (!head || !head->next) return;  //保证有两个节点以上
//        //找中间节点+反转链表+合并链表
//        ListNode* slow = head;
//        ListNode* fast = head;
//        while (!fast && !fast->next)
//        {
//            slow = slow->next;
//            fast = fast->next->next;
//        }
//        //这里slow就是中间节点，把链表分成两部分，后半部分进行反转
//        ListNode* n1 = slow;
//        ListNode* n2 = slow->next;
//        ListNode* n3 = nullptr;
//        //修改尾
//        slow->next = nullptr;
//        while (n2)
//        {
//            n3 = n2->next;
//            n2->next = n1;
//            //迭代
//            n1 = n2;
//            n2 = n3;
//        }
//        //合并链表
//        ListNode* tail = head;
//        n1 = head->next;
//        while (n1 != nullptr && n2 != nullptr)
//        {
//            tail->next = n2;
//            n2 = n2->next;
//            tail = tail->next;
//            if (n2)
//            {
//                tail->next = n1;
//                n1 = n1->next;
//                tail = tail->next;
//            }
//        }
//    }
//};

void qsort(vector<int>& arr, int left, int right)
{
	//返回
	if (left >= right) return;
	//生成随机数
	int key = arr[rand() % (right - left + 1) + left];
	int l = left - 1, r = right + 1, i = left;
	while (i < r)
	{
		if (arr[i] < key) swap(arr[++l], arr[i++]);
		else if (arr[i] == key) ++i;
		else swap(arr[--r], arr[i]);
	}
	//向剩下两部分递归
	//[left,l] [r,right]
	qsort(arr, left, l);
	qsort(arr, r, right);
}

int GetKey(vector<int>& nums, int left, int right)
{
    //生成随机下标
    int r = rand() % (right - left + 1) + left;
    return nums[r];
}
void QuickSort(vector<int>& nums, int left, int right)
{
    if (left >= right) return;
    int key = nums[rand() % (right - left + 1) + left];
    //使用三指针
    int pre = left - 1;
    int end = right + 1;
    int i = left;
    while (i < end)
    {
        //分三步讨论
        if (nums[i] < key) {
            swap(nums[++pre], nums[i++]);
        }
        else if (nums[i] == key) ++i;
        else {
            swap(nums[--end], nums[i]);
        }
    }
    //[left,pre] [pre+1,end-1] [end,right]
    QuickSort(nums, left, pre);
    QuickSort(nums, end, right);
}
vector<int> sortArray(vector<int>& nums) {
    //使用随机数来进行快排
    srand(time(nullptr));
    QuickSort(nums, 0, nums.size() - 1);
    return nums;
}

void _MergeSort(vector<int>& nums, int left, int right, vector<int>& tmp)
{
    if (left >= right) return;
    int mid = (left + right) >> 1;
    // [left,mid] [mid+1,right]
    _MergeSort(nums, left, mid,tmp);
    _MergeSort(nums, mid+1, right,tmp);
    //合并有序数组
    int cur1 = left, cur2 = mid + 1, i = left;
    while (cur1 <= mid && cur2 <= right)
    {
        tmp[i++] = nums[cur1] < nums[cur2] ? nums[cur1++] : nums[cur2++];
    }
    //处理剩下数组中的数据
    while (cur1 <= mid) {
        tmp[i++] = nums[cur1++];
    }
    while (cur2 <= right) {
        tmp[i++] = nums[cur2++];
    }
    //还原
    for (int j = left; j <= right; ++j)
        nums[j] = tmp[j];
}

void MergeSort(vector<int>& nums)
{
    vector<int> tmp(nums.size());
    _MergeSort(nums, 0, nums.size() - 1,tmp);
}

int main()
{
	srand(time(nullptr));
	vector<int> arr = { 2,3,6,4,5,7,9,8,1,3,5 };
    //sortArray(arr);
	//qsort(arr, 0, arr.size() - 1);
    MergeSort(arr);
	for (auto e : arr)
	{
		cout << e << " ";
	}
	return 0;
}