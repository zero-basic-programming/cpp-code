#include <iostream>
#include <vector>
#include <string>
#include <unordered_map>
using namespace std;

class Solution {
public:
    string minWindow(string s, string t) {
        int hash[128] = { 0 };
        int count = t.size();
        int n = s.size();
        string res;
        //把t中的元素出现的次数存放到hash中
        for (auto& ch : t)
        {
            hash[ch]++;
        }
        for (int left = 0, right = 0; right < n; ++right)
        {
            //进窗口
            if (--hash[s[right]] >= 0) --count;
            while (count == 0)
            {
                string tmp = s.substr(left, right - left + 1);
                if (res.size() > tmp.size()) res = tmp;
                if (++hash[s[left++]] > 0) ++count;
            }
        }
        return res;
    }
};


class Solution {
public:
    unordered_map <char, int> ori, cnt;

    bool check() {
        for (const auto& p : ori) {
            if (cnt[p.first] < p.second) {
                return false;
            }
        }
        return true;
    }

    string minWindow(string s, string t) {
        for (const auto& c : t) {
            ++ori[c];
        }

        int l = 0, r = -1;
        int len = INT_MAX, ansL = -1, ansR = -1;

        while (r < int(s.size())) {
            if (ori.find(s[++r]) != ori.end()) {
                ++cnt[s[r]];
            }
            while (check() && l <= r) {
                if (r - l + 1 < len) {
                    len = r - l + 1;
                    ansL = l;
                }
                if (ori.find(s[l]) != ori.end()) {
                    --cnt[s[l]];
                }
                ++l;
            }
        }

        return ansL == -1 ? string() : s.substr(ansL, len);
    }
};