#include <iostream>
#include <vector>
using namespace std;


//struct ListNode
//{
//    int val;
//    ListNode* prev;
//    ListNode* next;
//    ListNode(int v) :val(v), prev(nullptr), next(nullptr) {}
//};
//
//
//class MyLinkedList
//{
//public:
//    MyLinkedList()
//    {
//        head = new ListNode(0);  //哨兵位头节点
//    }
//    ~MyLinkedList()
//    {
//        //从头节点开始不断向后释放空间
//        ListNode* tail = head;
//        while (tail)
//        {
//            ListNode* next = tail->next;
//            delete tail;
//            tail = next;
//        }
//    }
//    MyLinkedList(const MyLinkedList& list)
//    {
//        ListNode* tail = head;
//        ListNode* copyhead = list.head;
//        while (copyhead)
//        {
//            tail = new ListNode(copyhead->val);
//            copyhead = copyhead->next;
//            tail = tail->next;
//        }
//    }
//    MyLinkedList& operator=(const MyLinkedList& list)
//    {
//        ListNode* tail = head;
//        while (tail)
//        {
//            ListNode* next = tail->next;
//            delete tail;
//            tail = next;
//        }
//        head = nullptr;
//        tail = head;
//        ListNode* copyhead = list.head;
//        while (copyhead)
//        {
//            tail = new ListNode(copyhead->val);
//            copyhead = copyhead->next;
//            tail = tail->next;
//        }
//        return *this;
//    }
//    void push_back(int val)
//    {
//        ListNode* tail = head->prev;
//        ListNode* newnode = new ListNode(val);
//        //连接
//        tail->next = newnode;
//        newnode->prev = tail;
//        head->prev = newnode;
//        newnode->next = head;
//    }
//    void insert(int val, int index)
//    {
//        //找到index位置，然后插入
//        ListNode* pos = head;
//        while (index--)
//        {
//            pos = pos->next;
//        }
//        ListNode* next = pos->next;
//        ListNode* newnode = new ListNode(val);
//        //连接
//        pos->next = newnode;
//        newnode->prev = pos;
//        newnode->next = next;
//        next->prev = newnode;
//    }
//    void erase(int index)
//    {
//        //找到index位置
//        ListNode* pos = head;
//        while (index--)
//        {
//            pos = pos->next;
//        }
//        ListNode* prev = pos->prev;
//        ListNode* next = pos->next;
//        //删除然后连接前后
//        delete pos;
//        prev->next = next;
//        next->prev = prev;
//    }
//private:
//    ListNode* head;
//};
//
//
//int main()
//{
//    MyLinkedList list;
//
//    return 0;
//}



//class Solution
//{
//    void rotateImage(vector<vector<int>>& image)
//    {
//        //通过找规律我们可以知道在一轮旋转之后有以下规律
//        //逆向旋转90度的规律如下：当前的i = n-y-1 ,当前的j = x
//        int n = image.size();
//        //每个元素都只遍历了一次，时间复杂度为O(N)
//        for (int j = 0; j < n - 1; ++j)   //只关注第一类
//        {
//            int i = 0;   //这里只有第一行需要处理
//            int tmp = image[i][j];
//            int x = n - j - 1, y = i;
//            for (int k = 0; k < n - 1; ++k)   //次数
//            {
//                image[i][j] = image[x][y];
//                i = x;
//                j = y;
//            }
//            image[i][j] = tmp;  //将第一行的值赋值给最后一个
//        }
//    }
//};


