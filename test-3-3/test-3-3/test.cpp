//class Solution {
//public:
//    bool isAnagram(string s, string t) {
//        //使用hash，把其中一个字符串中的内容放入
//        unordered_map<char, int> m;
//        for (auto& ch : s)
//        {
//            m[ch]++;
//        }
//        for (auto& ch : t)
//        {
//            auto it = m.find(ch);
//            if (it == m.end())
//            {
//                return false;
//            }
//            --m[ch];
//            if (m[ch] == 0)
//            {
//                m.erase(it);
//            }
//        }
//        if (!m.empty())
//        {
//            return false;
//        }
//        return true;
//    }
//};

//class Solution {
//public:
//    vector<int> intersection(vector<int>& nums1, vector<int>& nums2) {
//        //我们可以采用unordered_set 对其进行去重，然后在其中一个集合找另一个集合的元素即可
//        vector<int> result;
//        unordered_set<int> s1;
//        unordered_set<int> s2;
//        for (auto& e : nums1)
//        {
//            s1.insert(e);
//        }
//        for (auto& e : nums2)
//        {
//            s2.insert(e);
//        }
//        //在遍历s1的时候找s2
//        for (auto& e : s1)
//        {
//            if (s2.find(e) != s2.end())
//            {
//                result.push_back(e);
//            }
//        }
//        return result;
//    }
//};


//class Solution {
//public:
//    int getn(int n)
//    {
//        int ret = 0;
//        while (n)
//        {
//            int tmp = (n % 10) * (n % 10);
//            ret += tmp;
//            n /= 10;
//        }
//        return ret;
//    }
//    bool isHappy(int n) {
//        //使用hash存放结果，如果在表中出现过了，那么就不是快乐树
//        unordered_set<int> us;
//        us.insert(n);
//        while (n != 1)
//        {
//            n = getn(n);//这里把平方和赋值
//            if (us.find(n) != us.end())
//            {
//                return false;
//            }
//            us.insert(n);
//        }
//        return true;
//    }
//};


//class Solution{
//public:
//    vector<int> twoSum(vector<int>&nums, int target) {
//        unordered_map<int,int> um;
//        vector<int> result;
//        for (int i = 0; i < nums.size(); ++i)
//        {
//            //先去hash中找，如果没有找到就说明要插入
//            auto it = um.find(target - nums[i]);
//            if (it != um.end())
//            {
//                //找到了，直接返回结果
//                result.push_back(it->second);
//                result.push_back(i);
//                return result;
//            }
//            //没找到就插入
//            um[nums[i]] = i;
//        }
//        return result;
//    }
//};

