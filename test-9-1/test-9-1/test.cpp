#include <iostream>
#include <vector>
using namespace std;

//class Solution {
//public:
//    int pivotIndex(vector<int>& nums) {
//        int n = nums.size();
//        vector<int> f(n);
//        auto g = f;
//        //预处理好前缀和数组和后缀和数组
//        for (int i = 1; i < n; ++i)
//        {
//            f[i] = f[i - 1] + nums[i - 1];
//        }
//        for (int i = n - 2; i >= 0; --i)
//        {
//            g[i] = g[i + 1] + nums[i + 1];
//        }
//        for (int i = 0; i < n; ++i)
//        {
//            if (f[i] == g[i]) return i;
//        }
//        return -1;
//    }
//};


//class Solution {
//public:
//    vector<int> productExceptSelf(vector<int>& nums) {
//        int n = nums.size();
//        vector<int> f(n), g(n);
//        vector<int> res;
//        //初始化
//        f[0] = 1;
//        g[n - 1] = 1;
//        //处理前后缀乘积
//        for (int i = 1; i < n; ++i)
//        {
//            f[i] = f[i - 1] * nums[i - 1];
//        }
//        for (int i = n - 2; i >= 0; --i)
//        {
//            g[i] = g[i + 1] * nums[i + 1];
//        }
//        for (int i = 0; i < n; ++i)
//        {
//            res.push_back(f[i] * g[i]);
//        }
//        return res;
//    }
//};


//class Solution {
//public:
//    int getMaxLen(vector<int>& nums) {
//        int n = nums.size();
//        vector<int> g(n + 1), f(n + 1);
//        int res = INT_MIN;
//        for (int i = 1; i <= n; ++i)
//        {
//            if (nums[i - 1] > 0)
//            {
//                f[i] = f[i - 1] + 1;
//                g[i] = g[i - 1] == 0 ? 0 : g[i - 1] + 1;
//            }
//            else if (nums[i - 1] < 0)
//            {
//                f[i] = g[i - 1] == 0 ? 0 : g[i - 1] + 1;
//                g[i] = f[i - 1] + 1;
//            }
//            res = max(res, f[i]);
//        }
//        return res;
//    }
//};


//class Solution {
//public:
//    int numberOfArithmeticSlices(vector<int>& nums) {
//        int n = nums.size();
//        if (n < 3) return 0;
//        int gap1 = nums[1] - nums[0];
//        vector<int> f(n);
//        for (int i = 2; i < n; ++i)
//        {
//            int gap2 = nums[i] - nums[i - 1];
//            if (gap2 == gap1)
//            {
//                f[i] = f[i - 1] + 1;
//            }
//            else {
//                f[i] = f[i - 1];
//
//            }
//            gap1 = gap2;
//        }
//        return f[n - 1];
//    }
//};


//class Solution {
//public:
//    int numberOfArithmeticSlices(vector<int>& nums) {
//        int n = nums.size();
//        if (n < 3) return 0;
//        int gap1 = nums[1] - nums[0];
//        vector<int> f(n);
//        int res = 0;
//        for (int i = 2; i < n; ++i)
//        {
//            int gap2 = nums[i] - nums[i - 1];
//            if (gap2 == gap1)
//            {
//                f[i] = f[i - 1] + 1;
//            }
//            else {
//                f[i] = 0;
//
//            }
//            gap1 = gap2;
//            res += f[i];
//        }
//        return res;
//    }
//};


//class Solution {
//public:
//    int maxTurbulenceSize(vector<int>& arr) {
//        int n = arr.size();
//        if (n < 3) return n;
//        vector<int> dp(n, 1);
//        int gap1 = arr[1] - arr[0];
//        dp[0] = 1; dp[1] = 2;
//        int res = 2;
//        for (int i = 2; i < n; ++i)
//        {
//            int gap2 = arr[i] - arr[i - 1];
//            if ((gap1 > 0 && gap2 > 0) || (gap1 < 0 && gap2 < 0))
//            {
//                dp[i] = dp[i - 1] + 1;
//            }
//            else dp[i] = 2;
//            gap1 = gap2;
//            res = max(res, dp[i]);
//        }
//        return res;
//    }
//};

