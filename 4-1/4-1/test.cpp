#include <iostream>
#include <vector>
using namespace std;


//class Solution {
//public:
//    int findSubstringInWraproundString(string s) {
//        int n = s.size();
//        vector<int> dp(n, 1);
//        //1.使用动态规划来统计出每个字符结尾的时候长度
//        for (int i = 1; i < n; ++i)
//        {
//            //如果当前的字符上一个字符+1，或者上一个是z，下一个是a
//            if (s[i - 1] + 1 == s[i] || (s[i - 1] == 'z' && s[i] == 'a'))
//                dp[i] = dp[i - 1] + 1;
//        }
//        //通过hash表来统计以某一个位置为结尾的最大的个数
//        int hash[26] = { 0 };
//        for (int i = 0; i < n; ++i)
//        {
//            hash[s[i] - 'a'] = max(hash[s[i] - 'a'], dp[i]);
//        }
//        //收集结果
//        int sum = 0;
//        for (auto e : hash) sum += e;
//        return sum;
//    }
//};



//class Solution {
//public:
//    int lengthOfLIS(vector<int>& nums) {
//        int n = nums.size();
//        vector<int> dp(n, 1);
//        int ret = 1;
//        for (int i = 1; i < n; ++i)
//        {
//            for (int j = 0; j < i; ++j)
//            {
//                if (nums[i] > nums[j])
//                    dp[i] = max(dp[i], dp[j] + 1);
//            }
//            ret = max(ret, dp[i]);
//        }
//        return ret;
//    }
//};


//class Solution {
//public:
//    int wiggleMaxLength(vector<int>& nums) {
//        int n = nums.size();
//        vector<int> f(n, 1);
//        vector<int> g(n, 1);
//        int ret = 1;
//        for (int i = 1; i < n; ++i)
//        {
//            for (int j = 0; j < i; ++j)
//            {
//                if (nums[i] > nums[j])
//                    f[i] = max(g[j] + 1, f[i]);
//                else if (nums[i] < nums[j])
//                    g[i] = max(f[j] + 1, g[i]);
//            }
//            ret = max(ret, max(f[i], g[i]));
//        }
//        return ret;
//    }
//};



//class Solution {
//public:
//    int findNumberOfLIS(vector<int>& nums) {
//        int n = nums.size();
//        vector<int> len(n, 1), count(n, 1);
//        int retlen = 1;
//        int retcount = 1;  //个数
//        for (int i = 1; i < n; ++i)
//        {
//            for (int j = 0; j < i; ++j)
//            {
//                if (nums[i] > nums[j])
//                {
//                    if (len[j] + 1 == len[i])
//                    {
//                        count[i] += count[j];
//                    }
//                    else if (len[j] + 1 > len[i]) {
//                        //重新计算
//                        len[i] = len[j] + 1;
//                        count[i] = count[j];
//                    }
//                }
//            }
//            if (retlen < len[i])
//            {
//                retlen = len[i];
//                retcount = count[i];
//            }
//            else if (len[i] == retlen) retcount += count[i];
//        }
//        return retcount;
//    }
//};



//class Solution {
//public:
//    bool isPalStr(string chars) {
//        //1.把全部的大写转换成小写
//        int n = chars.size();
//        if (n == 0 || n == 1) return true;
//        string str;
//        for (auto& ch : chars)
//        {
//            if (('A' <= ch && ch <= 'Z') || ('a' <= ch && ch <= 'z')
//                || ('0' <= ch && ch <= '9'))
//            {
//                if ('A' <= ch && ch <= 'Z')
//                {
//                    ch -= 'A';
//                    ch += 'a';
//                }
//                str += ch;
//            }
//        }
//        //2.判断回文
//        int left = 0, right = str.size() - 1;
//        while (left < right)
//        {
//            if (str[left] != str[right]) return false;
//            ++left;
//            --right;
//        }
//        return true;
//    }
//};



//class Solution {
//public:
//    ListNode* ListReverse(ListNode* head)
//    {
//        if (head == nullptr || head->next == nullptr) return head;
//        ListNode* n1 = head, * n2 = head->next, * n3 = nullptr;
//        while (n2)
//        {
//            n3 = n2->next;
//            n2->next = n1;
//            //迭代
//            n1 = n2;
//            n2 = n3;
//        }
//        return n1;
//    }
//    bool isReverseChild(ListNode* a, ListNode* b) {
//        //先将a链表进行反转，然后再从头开始和b进行比较即可
//        ListNode* head = ListReverse(a);
//        ListNode* cur = head;
//        while (cur)
//        {
//            if (cur->val == b->val) break;  //找到相同的节点
//            cur = cur->next;
//        }
//        while (cur)
//        {
//            if (b == nullptr) return true;  //这里b比较小肯定小走完
//            if (cur->val != b->val) return false;
//            cur = cur->next;
//            b = b->next;
//        }
//        //最后有可能同时走到空
//        if (!cur && !b) return true;
//        return false;
//    }
//};

//#include <vector>
//#include <queue>
//#include <iostream>
//using namespace std;
//
//
//struct TreeNode
//{
//    TreeNode(int i) :val(i), left(nullptr), right(nullptr) {}
//    int val;
//    TreeNode* left;
//    TreeNode* right;
//};
//
//class Solution {
//    int i = 0;
//public:
//    TreeNode* BuildTree(vector<int>& pre, vector<int>& mid, int left, int right)
//    {
//        //1.通过前序找中序的根
//        //2.通过下标完成子树的构建
//        if (left > right) return nullptr;
//        TreeNode* root = new TreeNode(pre[i]);
//        int j = left;
//        for (; j <= right; ++j)
//        {
//            if (mid[j] == pre[i]) break;
//        }
//        ++i;  //到下一轮
//        //[left,j-1] [j+1,right]
//        root->left = BuildTree(pre, mid, left, j - 1);
//        root->right = BuildTree(pre, mid, j + 1, right);
//        return root;
//    }
//    vector<int> levelPrintTree(vector<int>& pre, vector<int>& mid) {
//        //1.通过前中来构建树
//        TreeNode* root = BuildTree(pre, mid, 0, mid.size() - 1);
//        //2.利用队列进行层序遍历
//        queue<TreeNode*> q;
//        q.push(root);
//        vector<int> ret;
//        while (q.size())
//        {
//            int sz = q.size();
//            for (int i = 0; i < sz; ++i)
//            {
//                TreeNode* node = q.front();
//                q.pop();
//                ret.push_back(node->val);
//                //入左右子节点
//                if (node->left) q.push(node->left);
//                if (node->right) q.push(node->right);
//            }
//        }
//        return ret;
//    }
//};
//
//
//int main()
//{
//    vector<int> pre = { 1,2,3,4,5 };
//    vector<int> mid = { 2,1,4,3,5 };
//    vector<int> tmp = Solution().levelPrintTree(pre, mid);
//    for (auto e : tmp) cout << e << " ";
//    return 0;
//}


#include <iostream>
#include <vector>
#include <unordered_map>
#include <string>
using namespace std;


//int main()
//{
//    int n;
//    cin >> n;
//    vector<string> vs;
//    while (n--)
//    {
//        string str;
//        cin >> str;
//        vs.push_back(str);
//    }
//    unordered_map<string, int> hash;  //名字与次数的关系
//    for (auto& str : vs)
//    {
//        if (hash.count(str) > 0) continue;
//        cout << str << endl;
//        hash[str]++;
//    }
//    return 0;
//}


#include <iostream>
#include <vector>
#include <string>
#include <unordered_set>
#include <algorithm>
#include <queue>
using namespace std;

//class cmp
//{
//public:
//    bool operator()(const pair<string, int>& p1, const pair<string, int>& p2)
//    {
//        //排降序，但是相同的次数的按字典序排序
//        if (p1.second == p2.second)
//        {
//            if (p1.first > p2.first) return true;
//            else return false;
//        }
//        return p1.second < p2.second;
//    }
//};
//
//int main()
//{
//    int n, q;
//    cin >> n >> q;
//    unordered_set<string> hash;  //保存关键词
//    while (q--)
//    {
//        string msg;
//        cin >> msg;
//        hash.insert(msg);
//    }
//    priority_queue<pair<string, int>, vector<pair<string, int>>, cmp> nums;  //商品和关键词匹配的数量
//    while (n--)
//    {
//        string goods;  //商品名称
//        cin >> goods;
//        int m;   //关键词数量
//        cin >> m;
//        int count = 0;  //该商品匹配的关键词数量
//        while (m--)
//        {
//            string tmp;
//            cin >> tmp;
//            if (hash.count(tmp)) ++count;
//        }
//        //放入到数组中
//        nums.push(make_pair(goods, count));
//    }
//    while (nums.size())
//    {
//        auto p = nums.top();
//        nums.pop();
//        cout << p.first << endl;
//    }
//    return 0;
//}


//int main()
//{
//    int n;
//    cin >> n;
//    vector<long long> arr(n);
//    long long maxnum = 0;  //记录全部数据的最大值
//    for (auto& e : arr)
//    {
//        cin >> e;
//        maxnum = max(maxnum, e);
//    }
//    //判断1个和2个的特殊情况
//    if (n == 1)
//    {
//        cout << arr[0] << endl;
//        return 0;
//    }
//    if (n == 2)
//    {
//        if (arr[0] > arr[1])
//        {
//            cout << (long long)arr[0] + arr[1] << endl;
//            cout << -1 << endl;
//        }
//        else if (arr[0] == arr[1])
//        {
//            cout << (long long)arr[0] + arr[1] + 1 << endl;
//            cout << (long long)arr[0] + arr[1] + 1 << endl;
//        }
//        else {
//            cout << -1 << endl;
//            cout << (long long)arr[0] + arr[1] << endl;
//        }
//        return 0;
//    }
//    n = arr.size();
//    priority_queue<long long, vector<long long>, greater<long long>> q;  //建立小堆
//    for (int i = 0; i < n; ++i)
//    {
//        //把其他元素入堆
//        for (int j = 0; j < n; ++j)
//        {
//            if (j == i) continue;  //不放入当前值
//            q.push(arr[j]);
//        }
//        long long tmpmax = maxnum;  //这个最大值可能随时有变化
//        long long tmpi = arr[i];  //为了不改变数组原来的元素
//        while (tmpi < tmpmax)
//        {
//            ++tmpi;
//            if (tmpi >= tmpmax) break;  //符合条件
//            auto mintop = q.top();
//            ++mintop;
//            //这里可能最小的都超过了最大
//            if (tmpmax < mintop) tmpmax = mintop;
//            q.pop();
//            q.push(mintop);
//        }
//        //把堆中的数据全部加起来
//        long long sum = tmpi;
//        while (q.size())
//        {
//            sum += q.top();
//            q.pop();
//        }
//        cout << sum << endl;
//    }
//    return 0;
//}
//


