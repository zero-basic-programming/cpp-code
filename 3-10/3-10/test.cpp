#include <iostream>
#include <vector>
#include <unordered_map>
using namespace std;

//int main()
//{
//	int n = 5, m = 3, q = 5;
//	vector<int> ufs(n + 1,-1);  //第一个元素是不使用的
//	auto findRoot = [&ufs](int x) {
//		while (ufs[x] >= 0)
//		{
//			x = ufs[x];
//		}
//		return x;
//	};
//	auto Union = [&ufs,&findRoot](int x1, int x2){
//		int root1 = findRoot(x1);
//		int root2 = findRoot(x2);
//		if (root1 != root2)  //两颗树如果相等，那就不用联合
//		{
//			ufs[root1] += ufs[root2];
//			ufs[root2] = root1;
//		}
//	};
//	unordered_map<pair<int, int>, int> hash;  //用来存储查询或者删除的关系 
//	vector<pair<int, int>> edge;  //用来存放初始边的关系
//	edge.push_back(make_pair(1, 2));
//	edge.push_back(make_pair(2, 3));
//	edge.push_back(make_pair(4, 5));

	/*auto it = make_pair(1, 5);
	hash[it] = 1;
	it = make_pair(1, 3);
	hash[it] = 2;
	it = make_pair(1, 4);
	hash[it] = 2;
	it = make_pair(1, 2);
	hash[it] = 1;
	it = make_pair(1, 3);
	hash[it] = 2;*/
	//把没有删除的边先放入并查集
	//for (auto& e : edge)
	//{
	//	int x = e.first;
	//	int y = e.second;
	//	auto xy = make_pair(x, y);
	//	auto yx = make_pair(y, x);
	//	if (hash.count(xy) > 0 && hash[xy] == 2)
	//	{
	//		//可以放入并查集中
	//		Union(x, y);
	//	}
	//	if (hash.count(yx) > 0 && hash[yx] == 2)
	//	{
	//		//可以放入并查集中
	//		Union(y, x);
	//	}
	//}
	//逆序遍历hash
	//vector<string> ret;  //用来保存结果,后序要逆序输出
	//auto iter = hash.end();
	//while (iter != hash.begin())
	//{
	//	int x = iter->first.first;
	//	int y = iter->first.second;
	//	if (iter->second == 1)
	//	{
	//		//插入新的边
	//		Union(x,y);
	//	}
	//	else
	//	{
	//		//查询
	//		int root1 = findRoot(x);
	//		int root2 = findRoot(y);
	//		if (root1 != root2)
	//			ret.push_back("No");
	//		else
	//			ret.push_back("Yes");
	//	}
	//	--iter;
	//}
	////反转结果集
	//reverse(ret.begin(), ret.end());
	//for (auto& str : ret)
	//	cout << str << endl;
	/*return 0;
}*/


//class Solution {
//public:
//	int maxSubArray(vector<int>& nums) {
//		int n = nums.size();
//		vector<int> dp(n);
//		dp[0] = nums[0];
//		int ret = dp[0];
//		for (int i = 1; i < n; ++i)
//		{
//			if (dp[i - 1] <= 0) dp[i] = nums[i];
//			else
//			{
//				dp[i] = dp[i - 1] + nums[i];
//			}
//			//更新ret
//			ret = max(ret, dp[i]);
//		}
//		return ret;
//	}
//};


