#include <iostream>
#include <vector>
using namespace std;


//class Solution{
//public:
//    int longestArithSeqLength(vector<int>&nums) {
//        int n = nums.size();
//        int ret = 2;
//        vector<vector<int>> dp(n,vector<int>(n,2));
//        unordered_map<int,int> hash; //用来记录前面已经出现过的数字和其下标的映射关系
//        hash[nums[0]] = 0;
//        for (int i = 1; i < n; ++i)
//        {
//            for (int j = i - 1; j > 0; --j)
//            {
//                int a = 2 * nums[j] - nums[i];
//                if (hash.count(a))
//                {
//                    //前面那个下标可以比j大，要舍弃
//                    int k = hash[a];
//                    if (k < j) dp[j][i] = dp[k][j] + 1;
//                }
//                ret = max(ret,dp[j][i]);
//            }
//            hash[nums[i]] = i;
//        }
//        return ret;
//    }
//};


//class Solution {
//public:
//    int longestArithSeqLength(vector<int>& nums) {
//        int n = nums.size();
//        vector<vector<int>> dp(n, vector<int>(n, 2)); //创建并初始化
//        unordered_map<int, int> hash; //元素和下标的映射关系
//        hash[nums[0]] = 0;
//        int ret = 2;
//        for (int i = 1; i < n; ++i) //固定倒数第二个数
//        {
//            for (int j = i + 1; j < n; ++j) //固定最后一个数
//            {
//                int a = 2 * nums[i] - nums[j];
//                if (hash.count(a))
//                {
//                    int k = hash[a];
//                    dp[i][j] = dp[k][i] + 1;
//                }
//                ret = max(ret, dp[i][j]);
//            }
//            hash[nums[i]] = i;
//        }
//        return ret;
//    }
//};


//class Solution{
//public:
//    void sortColors(vector<int>&nums) {
//        int n = nums.size();
//        int left = -1;
//        int right = n;
//        int i = 0;
//        //分成4个区间
//        while (i < right)
//        {
//            if (nums[i] == 0)
//            {
//                //和left后面的数字交换，更新0的值
//                swap(nums[++left],nums[i++]);
//            }
//            else if (nums[i] == 1)
//            {
//                //这里就是当前区间 [left,i]
//                ++i;
//            }
//            else
//            {
//                //和有区域交换，但是i不能+，因为右边区域是还没有处理过的
//                swap(nums[i],nums[--right]);
//            }
//        }
//    }
//};


class Solution {
public:
    int ThreeOfMid(vector<int>& nums, int left, int right)
    {
        int mid = left + (right - left) / 2;
        if (nums[mid] > nums[left])
        {
            if (nums[right] > nums[mid]) return mid;
            else if (nums[right] < nums[left]) return left;
            else return right;
        }
        else //nums[mid] <= nums[left]
        {
            if (nums[right] > nums[left]) return left;
            else if (nums[right] < nums[mid]) return mid;
            else return right;
        }
    }
    int _QuickSort(vector<int>& nums, int left, int right)
    {
        //三数取中
        int mid = ThreeOfMid(nums, left, right);
        //交换中间和左边,左边就是key
        swap(nums[left], nums[mid]);
        int keyi = left;
        int pre = left; //找大
        int cur = pre + 1; //找小
        while (cur <= right)
        {
            if (nums[cur] < nums[keyi] && ++pre != cur) {
                swap(nums[pre], nums[cur]);
            }
            ++cur;
        }
        swap(nums[keyi], nums[pre]);
        return pre;
    }
    void QuickSort(vector<int>& nums, int left, int right)
    {
        if (left >= right) return;
        int keyi = _QuickSort(nums, left, right);
        //[left,keyi-1] keyi [keyi+1,right]
        QuickSort(nums, left, keyi - 1);
        QuickSort(nums, keyi + 1, right);
    }
    vector<int> sortArray(vector<int>& nums) {
        QuickSort(nums, 0, nums.size() - 1);
        return nums;
    }
};