#include <iostream>
#include <vector>
using namespace std;


//class Solution {
//public:
//    double myPow(double x, int n) {
//        return n < 0 ? 1 / pow(x, -(long long)n) : pow(x, n);
//    }
//    double pow(double x, long long n) //这里的n的类型是为了防止其是负无穷转正无穷而溢出
//    {
//        if (n == 0) return 1;
//        double ret = pow(x, n / 2);
//        return n % 2 == 0 ? ret * ret : x * ret * ret;
//    }
//};


//class Solution {
//public:
//    vector<string> sortPeople(vector<string>& names, vector<int>& heights) {
//        //通过hash映射的方式把下标和身高映射起来
//        int n = heights.size();
//        vector<int> hash(n);
//        for (int i = 0; i < n; ++i) hash[i] = i;
//        sort(hash.begin(), hash.end(), [&](const int i, const int j) {
//            return heights[i] > heights[j];
//            });
//        vector<string> ret(names.size());
//        for (int i = 0; i < n; ++i)
//        {
//            ret[i] = names[hash[i]];
//        }
//        return ret;
//    }
//};


//class Solution {
//public:
//    bool evaluateTree(TreeNode* root) {
//        if (root->left == nullptr) return root->val == 0 ? false : true;
//        //得到左右子树的值
//        bool leftval, rightval;
//        if (root->left && root->right)
//        {
//            leftval = evaluateTree(root->left);
//            rightval = evaluateTree(root->right);
//        }
//        //计算结果
//        return root->val == 2 ? leftval | rightval : leftval & rightval;
//    }
//};