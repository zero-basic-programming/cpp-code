#include <iostream>
#include <vector>
using namespace std;


//class Solution{
//public:
//    int missingNumber(vector<int>&nums) {
//        int ret = 0;
//        int n = nums.size() + 1;
//        for (int i = 0; i < n; ++i) ret ^= i;
//        for (auto e : nums)   ret ^= e;
//        return ret;
//    }
//};


//class Solution {
//public:
//    int hammingWeight(uint32_t n) {
//        int count = 0;
//        while (n)
//        {
//            //相当于每次去掉最后一个1
//            n &= (n - 1);
//            ++count;
//        }
//        return count;
//    }
//};


//class Solution {
//public:
//    vector<int> countBits(int n) {
//        vector<int> ret(n + 1);
//        for (int i = 0; i <= n; ++i)
//        {
//            int count = 0;
//            while (i)
//            {
//                i &= (i - 1);
//                ++count;
//            }
//            ret.push_back(count);
//        }
//        return ret;
//    }
//};