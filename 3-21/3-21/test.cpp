#include <iostream>
#include <vector>
using namespace std;

//class Solution {
//public:
//    int findPoisonedDuration(vector<int>& timeSeries, int duration) {
//        int ret = 0;  //总的中毒时间
//        int n = timeSeries.size();
//        for (int i = 0; i < n; ++i)
//        {
//            int last = timeSeries[i] + duration - 1; //当前攻击后中毒持续时间
//            if (i + 1 < n && last >= timeSeries[i + 1]) ret += timeSeries[i + 1] - timeSeries[i];
//            else ret += duration;
//        }
//        return ret;
//    }
//};


//class Solution {
//public:
//    string convert(string s, int numRows) {
//        string ret;
//        int n = s.size();
//        if (numRows == 1) return s;
//        int d = 2 * numRows - 2;  //每一行的规律差值
//        //选取第一行的数
//        for (int i = 0; i < n; i += d) ret += s[i];
//        //选取中间行
//        for (int k = 1; k < numRows - 1; ++k)
//        {
//            //每一行
//            for (int i = k, j = d - k; i < n || j < n; i += d, j += d)
//            {
//                if (i < n) ret += s[i];
//                if (j < n) ret += s[j];
//            }
//        }
//        //处理最后一行
//        for (int i = numRows - 1; i < n; i += d) ret += s[i];
//        return ret;
//    }
//};


//class Solution {
//public:
//    string countAndSay(int n) {
//        string ret = "1";
//        for (int k = 2; k <= n; ++k)
//        {
//            int i = 0;
//            string tmp;
//            int n = ret.size();
//            while (i < n)
//            {
//                int j = i;
//                while (j + 1 < n && ret[j] == ret[j + 1]) ++j;
//                tmp += to_string(j - i + 1) + ret[i];
//                i = j + 1;
//            }
//            ret = tmp;
//        }
//        return ret;
//    }
//};


//class Solution {
//public:
//    int minNumberOfFrogs(string croakOfFrogs) {
//        string tmp = "croak";
//        int hashi[26] = { 0 };  //统计字符与下标的映射关系
//        int hash[5] = { 0 };  //记录当前字符出现的次数
//        //把tmp中的字符以及下标映射到hashi中
//        int i = 0;
//        for (auto ch : tmp) hashi[ch - 'a'] = i, ++i;
//        for (auto ch : croakOfFrogs)
//        {
//            if (ch == 'c')
//            {
//                //判断最后一个k出现的次数是否>0
//                //1.>0,说明有一个青蛙叫过一次了，下一次可能还是这个青蛙叫的
//                if (hash[4] > 0)
//                    --hash[4];
//                ++hash[0];
//            }
//            else
//            {
//                //其他字符，如果前一个字符没有出现过，那么就是错误的
//                //如果出现过，那么就可以把前面出现的次数-1，当前次数+1，表示同一个青蛙叫的
//                if (hash[hashi[ch - 'a'] - 1]-- == 0) return -1;
//                hash[hashi[ch - 'a']]++;
//            }
//        }
//        //最后遍历hash，除了k字符以外，如果某个字符出现的次数是>0，那么结果一定是错的
//        for (int i = 0; i < 4; ++i)
//        {
//            if (hash[i] > 0) return -1;
//        }
//        return hash[4];  //返回最后一个k字符出现的次数，就是青蛙的最小次数
//    }
//};



//int dx[4] = { 0,0,1,-1 };
//int dy[4] = { 1,-1,0,0 };
//int n, m;
//int ret = INT_MAX;
//
//bool IsTrue(vector<vector<char>>& arr, int i, int j, int x, int y)
//{
//    if (arr[i][j] == 'r' && arr[x][y] == 'd') return false;
//    if (arr[i][j] == 'e' && arr[x][y] == 'r') return false;
//    if (arr[i][j] == 'd' && arr[x][y] == 'e') return false;
//    return true;
//}
//
//void dfs(vector<vector<char>>& arr, vector<vector<bool>>& vis, int i, int j, int count)
//{
//    //返回条件
//    if (i == m - 1 && j == n - 1)
//    {
//        ret = min(ret, count);
//        return;
//    }
//    vis[i][j] = true;
//    //遍历当前位置的4个方向
//    for (int k = 0; k < 4; ++k)
//    {
//        int x = i + dx[k], y = j + dy[k];
//        //判断是否符合条件
//        if (x >= 0 && x < m && y >= 0 && y < n && !vis[x][y] && IsTrue(arr, i, j, x, y))
//        {
//            dfs(arr, vis, x, y, count + 1);
//        }
//    }
//}
//
//int main() {
//    cin >> n >> m;
//    vector<vector<char>> arr(m, vector<char>(n));
//    for (auto& a : arr)
//    {
//        for (auto& e : a)
//        {
//            cin >> e;
//        }
//    }
//    vector<vector<bool>> vis(m, vector<bool>(n, false));   //标记数组，判断当前的字符是否已经使用过
//    dfs(arr, vis, 0, 0, 0);
//    if (ret == INT_MAX) ret = -1;
//    cout << ret << endl;
//}


//int main() {
//    int q;
//    cin >> q;
//    while (q--)
//    {
//        string tmp;
//        cin >> tmp;
//        //统计字符串中的0的个数和1的个数
//        int one = 0, zero = 0;
//        for (auto ch : tmp)
//        {
//            if (ch == '1') ++one;
//            else ++zero;
//        }
//        //判断1的个数和0的个数是否都为奇数次
//        if (one % 2 == 1 && zero % 2 == 1) cout << "No" << endl;
//        else cout << "Yes" << endl;
//    }
//    return 0;
//}


//bool Isvowel(char ch) //判断一个字符是否是元音
//{
//    if (ch == 'a' || ch == 'e' || ch == 'i' || ch == 'o' || ch == 'u') return true;
//    return false;
//}
//
//bool IsTrue(string& str, int left, int right)
//{
//    int hash[26] = { 0 };
//    for (int i = left; i <= right; ++i)
//    {
//        if (i == left || i == left + 3)
//        {
//            if (Isvowel(str[i])) return false;
//        }
//        else
//        {
//            if (!Isvowel(str[i])) return false;  //其他位置不能是元音字符 
//        }
//        if (hash[str[i] - 'a'] > 0) return false;  //排除一样的字符
//        ++hash[str[i] - 'a'];
//    }
//    return true;
//}
//
//int main() {
//    string str;
//    cin >> str;
//    int ret = 0; //收集结果
//    int n = str.size();
//    if (n < 5)
//    {
//        cout << 0;
//        return 0;
//    }
//    for (int i = 0; i < 5; ++i)  //从长度的某一个位置出发
//    {
//        for (int j = i; j < n; j += 5)  //一次判断一个字串是否满足
//        {
//            if (IsTrue(str, j, j + 4)) ++ret;
//        }
//    }
//    cout << ret << endl;
//    return 0;
//}


