#include <iostream>
#include <vector>
using namespace std;


//int main()
//{
//    int a, x;
//    for (a = 0, x = 0; a <= 1 && !x++; a++)
//    {
//        a++;
//    }
//    cout << a << x << endl;
//    return 0;
//}


//void main()
//{
//	char ch1, ch2;
//	ch1 = 'D' + '8'-'3';
//	ch2 = '9'-'1';
//	printf("%c %d\n", ch1, ch2);
//}

//class Solution {
//public:
//    vector<int> reverseBookList(ListNode* head) {
//        //链表逆置
//        vector<int> ret;
//        //处理边界条件
//        if (head == nullptr) return ret;
//        if (head->next == nullptr)
//        {
//            ret.push_back(head->val);
//            return ret;
//        }
//        //1.把链表进行反转
//        ListNode* n1 = head, * n2 = head->next, * n3 = nullptr;
//        n1->next = nullptr;  //处理第一个节点
//        while (n2)
//        {
//            n3 = n2->next;
//            n2->next = n1;
//            n1 = n2;
//            n2 = n3;
//        }
//        //2.遍历链表，收集结果
//        ListNode* cur = n1;
//        while (cur)
//        {
//            ret.push_back(cur->val);
//            cur = cur->next;
//        }
//        return ret;
//    }
//};


//class Solution {
//    vector<int> ret;
//public:
//    void dfs(ListNode* head)
//    {
//        if (head == nullptr) return;
//        dfs(head->next);
//        ret.push_back(head->val);
//    }
//    vector<int> reverseBookList(ListNode* head) {
//        //利用递归
//        dfs(head);
//        return ret;
//    }
//};


//class Solution {
//public:
//    vector<int> reverseBookList(ListNode* head) {
//        //通过栈来模拟递归
//        vector<int> ret;
//        vector<int> st;
//        while (head)
//        {
//            st.push_back(head->val);
//            head = head->next;
//        }
//        while (st.size())
//        {
//            ret.push_back(st.back());
//            st.pop_back();
//        }
//        return ret;
//    }
//};


//class Solution {
//public:
//    string pathEncryption(string path) {
//        //首先把'.'的个数统计出来，然后再扩容，最后进行尾部开始维护
//        int count = 0;
//        for (auto ch : path)
//        {
//            if (ch == '.') ++count;
//        }
//        int n = path.size();
//        path.resize(n + 2 * count);
//        int i = n - 1, j = n - 1 + 2 * count;
//        while (i >= 0)
//        {
//            if (path[i] != '.')
//            {
//                path[j--] = path[i--];
//            }
//            else
//            {
//                //替换成%20
//                path[j--] = '0';
//                path[j--] = '2';
//                path[j--] = '%';
//                --i;
//            }
//        }
//        return path;
//    }
//};


//class Solution {
//public:
//    string pathEncryption(string path) {
//        for (auto& ch : path)
//        {
//            if (ch == '.') ch = ' ';
//        }
//        return path;
//    }
//};


