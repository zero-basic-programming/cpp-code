#pragma once
#include "UnionFindSet.h"
#include <iostream>
#include <map>
#include <vector>
#include <queue>
using namespace std;


namespace matrix
{
	//默认是无向图
	template<class V,class W,W W_MAX = INT_MAX,bool Direction = false>
	class Graph
	{
		typedef Graph<V, W,W_MAX, Direction> Self;
	public:
		Graph() = default;
		Graph(const V* a, size_t n)
		{
			_vertex.reserve(n);
			for (size_t i = 0; i < n; ++i)
			{
				_vertex.push_back(a[i]);
				_indexMap[a[i]] = i;
			}
			_matrix.resize(n);
			for (size_t i = 0; i < n; ++i)
			{
				_matrix[i].resize(n,W_MAX);
			}
		}
		//通过顶点获取下标
		size_t GetVertexIndex(const V& v)
		{
			auto it = _indexMap.find(v);
			if (it != _indexMap.end())
			{
				return it->second;
			}
			//顶点不存在
			throw invalid_argument("顶点不存在");
			return -1;
		}
		void _AddEdge(size_t srci, size_t dsti,const W& w)
		{
			//在矩阵中添加他们的关系
			_matrix[srci][dsti] = w;
			if (Direction == false)
			{
				_matrix[dsti][srci] = w;
			}
		}
		//通过两个顶点以及权值来添加边
		void AddEdge(const V& src, const V& dst, const W& w)
		{
			size_t srci = GetVertexIndex(src);
			size_t dsti = GetVertexIndex(dst);
			_AddEdge(srci, dsti, w);
		}
		//打印出图
		void Print()
		{
			//打印顶点和坐标的对应关系
			for (size_t i = 0; i < _vertex.size(); ++i)
			{
				cout << "[" << i << "]" << " -> " << _vertex[i] << endl;
			}
			cout << endl;
			int n = _matrix.size();
			cout << "  ";
			//打印显示行
			for (int i = 0; i < n; ++i)
			{
				printf("%4d", i);
			}
			cout << endl;
			//打印权值
			for (int i = 0; i < n; ++i)
			{
				cout << i << " ";
				for (int j = 0; j < n; ++j)
				{
					if (_matrix[i][j] == W_MAX)
					{
						printf("%4c", '*');
					}
					else
					{
						printf("%4d", _matrix[i][j]);
					}
				}
				cout << endl;
			}
			cout << endl;
		}
		//给出一个顶点，使用广度优先遍历
		void BFS(const V& src)
		{
			queue<int> q;
			vector<bool> vis(_vertex.size(),false); //用来标记
			int srci = GetVertexIndex(src);
			int n = _matrix.size();
			q.push(srci);
			vis[srci] = true;
			while (q.size())
			{
				int sz = q.size();
				while (sz--)
				{
					int front = q.front();
					q.pop();
					cout << front << " -> " << _vertex[front] << endl;
					for (int i = 0; i < n; ++i)
					{
						if (_matrix[front][i] != '*')
						{
							if (!vis[i])
							{
								q.push(i);
								vis[i] = true;
							}
						}
					}
				}
			}
		}
		void _DFS(int srci, vector<bool>& vis)
		{
			vis[srci] = true;
			cout << srci << "->" << _vertex[srci] << endl;
			//往深处找
			for (int i = 0; i < _matrix.size(); ++i)
			{
				if (!vis[i] && _matrix[srci][i] != '*')
				{
					_DFS(i, vis);
				}
			}
		}
		//给你一个顶点进行深度优先遍历
		void DFS(const V& src)
		{
			int srci = GetVertexIndex(src);
			vector<bool> vis(_vertex.size(), false);
			_DFS(srci, vis);
		}
		//最小生成树算法
		struct Edge
		{
			size_t _srci;
			size_t _dsti;
			W _w; //权值
			Edge(size_t srci, size_t dsti, const W& w)
				:_srci(srci),_dsti(dsti),_w(w)
			{}
			bool operator>(const Edge& e) const
			{
				return _w > e._w;
			}
		};
		W Kruskal(Self& minTree)
		{
			//初始化最小生成树
			int n = _vertex.size();
			minTree._vertex = _vertex;
			minTree._indexMap = _indexMap;
			minTree._matrix.resize(n);
			for (int i = 0; i < n; ++i)
			{
				minTree._matrix[i].resize(n, W_MAX);
			}
			//使用堆来进行小边的筛选
			priority_queue<Edge,vector<Edge>,greater<Edge>> pq;
			for (int i = 0; i < n; ++i)
			{
				for (int j = 0; j < n; ++j)
				{
					if (i < j && _matrix[i][j] != W_MAX)
					{
						pq.push(Edge(i, j, _matrix[i][j]));
					}
				}
			}
			//选取n-1条最短的边，但是这里需要不能使他们构成环，这里使用并查集来校验
			int size = 0;
			W totalW = W();
			UnionFindSet ufs(n);
			while (pq.size())
			{
				//每次选一个最短的边
				Edge min = pq.top();
				pq.pop();
				//查看这条边是否会形成环
				if (!ufs.Inset(min._srci, min._dsti))
				{
					cout << _vertex[min._srci] << "->" << _vertex[min._dsti] << ":" << min._w << endl;
					++size;
					ufs.Union(min._srci, min._dsti);
					minTree._AddEdge(min._srci, min._dsti, min._w);
					totalW += min._w;
				}
				else
				{
					cout << "构成环: ";
					cout << _vertex[min._srci] << "->" << _vertex[min._dsti] << ":" << min._w << endl;
				}
			}
			//到这里如果凑齐了n-1条边才有结果
			if (size == n - 1)
			{
				return totalW;
			}
			return W();
		}
		W Prim(Self& minTree, const V& src)
		{
			int srci = GetVertexIndex(src);
			int n = _vertex.size();
			//完成最小生成树的初始化工作
			minTree._vertex = _vertex;
			minTree._indexMap = _indexMap;
			minTree._matrix.resize(n);
			for (int i = 0; i < n; ++i)
			{
				minTree._matrix[i].resize(n,W_MAX);
			}
			//把与开始的顶点相连的边全部放入到堆中
			vector<bool> X(n, false);  //形成边的顶点
			vector<bool> Y(n, true);  //未放入边的顶点
			X[srci] = true;
			Y[srci] = false;
			priority_queue<Edge, vector<Edge>, greater<Edge>> minq;
			for (int i = 0; i < n; ++i)
			{
				if (_matrix[srci][i] != W_MAX)
				{
					minq.push(Edge(srci, i, _matrix[srci][i]));
				}
			}
			//开始选边
			W totalW = W();
			size_t sz = 0; //记录一共记录的边的个数
			while (minq.size())
			{
				Edge min = minq.top();
				minq.pop();
				if (X[min._dsti])
				{
					//这里说明已经成环
					cout << "构成环:";
					cout << _vertex[min._srci] << "->" << _vertex[min._dsti] << " : " << min._w << endl;
				}
				else
				{
					minTree._AddEdge(min._srci, min._dsti, min._w);
					cout << _vertex[min._srci] << "->" << _vertex[min._dsti] << " : " << min._w << endl;
					++sz;
					totalW += min._w;
					X[min._dsti] = true;
					Y[min._dsti] = false;
					if (sz == n - 1) break;
					//把目标顶点继续入队
					for (int i = 0; i < n; ++i)
					{
						if (_matrix[min._dsti][i] != W_MAX && Y[i])  //这里后面的目标的顶点必须是还没有选过的
						{
							minq.push(Edge(min._dsti, i, _matrix[min._dsti][i]));
						}
					}
				}
			}
			if(sz == n-1)	return totalW;
			return W();
		}
		//打印最短路径
		void PrintShortPath(const V& src, const vector<W>& dist, const vector<int>& pPath)
		{
			//根据源顶点进行打印，利用其父路径
			int srci = GetVertexIndex(src);
			int n = _vertex.size();
			for (int i = 0; i < n; ++i)
			{
				if (i != srci)
				{
					vector<int> path;
					int parenti = i;
					while (parenti != srci)
					{
						path.push_back(parenti);
						parenti = pPath[parenti];
					}
					//这里打印的是相反的，需要逆置
					path.push_back(parenti);
					reverse(path.begin(), path.end());
					for (auto index : path)
					{
						cout << _vertex[index] << "->";
					}
					cout << " W:" << dist[i] << endl;
				}
			}
		}
		//最短路径算法
		void Dijkstra(const V& src, vector<W>& dist, vector<int>& pPath)
		{
			int srci = GetVertexIndex(src);
			int n = _vertex.size();
			dist.resize(n, W_MAX);
			pPath.resize(n, -1);
			//初始化
			dist[srci] = W();
			pPath[srci] = srci;
			vector<bool> S(n, false); //记录顶点是否确定为最小路径
			for (int i = 0; i < n; ++i)
			{
				//选择还没有确定顶点，到达该顶点的是最小的边
				int u = 0;
				int minW = W_MAX;
				for (int j = 0; j < n; ++j)
				{
					if (S[j] == false && dist[j] < minW)
					{
						u = j;
						minW = dist[j];
					}
				}
				S[u] = true;
				//开始根据该顶点来松弛的修改到达其他顶点的边
				for (int v = 0; v < n; ++v)
				{
					if (S[v] == false && _matrix[u][v] != W_MAX && dist[u] + _matrix[u][v] < dist[v])
					{
						dist[v] = dist[u] + _matrix[u][v];
						pPath[v] = u;
					}
				}
			}
		}
		bool BellmanFord(const V& src, vector<W>& dist, vector<int>& pPath)
		{
			int srci = GetVertexIndex(src);
			int n = _vertex.size();
			dist.resize(n, W_MAX);
			pPath.resize(n, -1);
			dist[srci] = W();
			//总共最多更新n次
			for (int k = 0; k < n; ++k)
			{
				//更新一次
				cout << "更新第" << k << "次" << endl;
				bool flag = true;  //为了优化，走一次如果没有更新就说明已经完成了全部更新
				for (int i = 0; i < n; ++i)
				{
					for (int j = 0; j < n; ++j)
					{
						if (_matrix[i][j] != W_MAX && dist[i] + _matrix[i][j] < dist[j])
						{
							flag = false;
							dist[j] = dist[i] + _matrix[i][j];
							pPath[j] = i;
							cout << _vertex[i] << "->" << _vertex[j] << ":" << _matrix[i][j] << endl;
						}
					}
				}
				if (flag) break;  //这里如果当次没有更新说明已经是最短路径
			}
			//这里如果已经更新了n次，但是还会更新，那么一定是出现了负权回路，无法处理
			for (int i = 0; i < n; ++i)
			{
				for (int j = 0; j < n; ++j)
				{
					if (_matrix[i][j] != W_MAX && dist[i] + _matrix[i][j] < dist[j])
						return false;
				}
			}
			return true;
		}
		void FloydWarshall(vector<vector<W>>& vvDist, vector<vector<int>>& vvpPath)
		{
			int n = _vertex.size();
			//进行初始化扩容
			vvDist.resize(n);
			vvpPath.resize(n);
			for (int i = 0; i < n; ++i)
			{
				vvDist[i].resize(n, W_MAX);
				vvpPath[i].resize(n, -1);
			}
			//把他们之间相连的边先记录下来
			for (int i = 0; i < n; ++i)
			{
				for (int j = 0; j < n; ++j)
				{
					if (_matrix[i][j] != W_MAX)
					{
						vvDist[i][j] = _matrix[i][j];
						vvpPath[i][j] = i;
					}
					if (i == j)
					{
						vvDist[i][j] = W();
					}
				}
			}
			//从i -> j中找到一个k，可以更新i -> j的距离
			for (int k = 0; k < n; ++k)
			{
				for (int i = 0; i < n; ++i)
				{
					for (int j = 0; j < n; ++j)
					{
						if (vvDist[i][k] != W_MAX && vvDist[k][j] != W_MAX
							&& vvDist[i][k] + vvDist[k][j] < vvDist[i][j])
						{
							vvDist[i][j] = vvDist[i][k] + vvDist[k][j];
							//更新父路径
							//i .. k .. x j
							//这里k到j可能不是直接相连
							vvpPath[i][j] = vvpPath[k][j];
						}
					}
				}
			}
			//打印权值
			for (int i = 0; i < n; ++i)
			{
				for (int j = 0; j < n; ++j)
				{
					if (vvDist[i][j] != W_MAX)
					{
						printf("%3d", vvDist[i][j]);	
					}
					else
					{
						printf("%3c", '*');
					}
				}
				cout << endl;
			}
			cout << endl;
			//打印父路径
			for (int i = 0; i < n; ++i)
			{
				for (int j = 0; j < n; ++j)
				{
					printf("%3d", vvpPath[i][j]);
				}
				cout << endl;
			}
		}
	private:
		vector<V> _vertex;			//存放顶点
		map<V, int> _indexMap;		//存放顶点和下标的映射关系
		vector<vector<W>> _matrix;	//临街矩阵
	};
	void TestGraph1()
	{
		Graph<char, int, INT_MAX, true> g("0123", 4);
		g.AddEdge('0', '1', 1);
		g.AddEdge('0', '3', 4);
		g.AddEdge('1', '3', 2);
		g.AddEdge('1', '2', 9);
		g.AddEdge('2', '3', 8);
		g.AddEdge('2', '1', 5);
		g.AddEdge('2', '0', 3);
		g.AddEdge('3', '2', 6);

		g.Print();
	}

	void TestBDFS()
	{
		string a[] = { "张三", "李四", "王五", "赵六", "周七" };
		Graph<string, int> g1(a, sizeof(a) / sizeof(string));
		g1.AddEdge("张三", "李四", 100);
		g1.AddEdge("张三", "王五", 200);
		g1.AddEdge("王五", "赵六", 30);
		g1.AddEdge("王五", "周七", 30);
		g1.Print();

		g1.BFS("张三");
		g1.DFS("张三");
	}
	void TestGraphMinTree()
	{
		const char* str = "abcdefghi";
		Graph<char, int> g(str, strlen(str));
		g.AddEdge('a', 'b', 4);
		g.AddEdge('a', 'h', 8);
		//g.AddEdge('a', 'h', 9);
		g.AddEdge('b', 'c', 8);
		g.AddEdge('b', 'h', 11);
		g.AddEdge('c', 'i', 2);
		g.AddEdge('c', 'f', 4);
		g.AddEdge('c', 'd', 7);
		g.AddEdge('d', 'f', 14);
		g.AddEdge('d', 'e', 9);
		g.AddEdge('e', 'f', 10);
		g.AddEdge('f', 'g', 2);
		g.AddEdge('g', 'h', 1);
		g.AddEdge('g', 'i', 6);
		g.AddEdge('h', 'i', 7);

		Graph<char, int> kminTree;
		cout << "Kruskal:" << g.Kruskal(kminTree) << endl;
		kminTree.Print();
		Graph<char, int> pminTree;
		cout << "Prim:" << g.Prim(pminTree, 'a') << endl;
		pminTree.Print();
		cout << endl;

		for (size_t i = 0; i < strlen(str); ++i)
		{
			cout << "Prim:" << g.Prim(pminTree, str[i]) << endl;
		}
	}
	void TestGraphDijkstra()
	{
		/*const char* str = "syztx";
		Graph<char, int, INT_MAX, true> g(str, strlen(str));
		g.AddEdge('s', 't', 10);
		g.AddEdge('s', 'y', 5);
		g.AddEdge('y', 't', 3);
		g.AddEdge('y', 'x', 9);
		g.AddEdge('y', 'z', 2);
		g.AddEdge('z', 's', 7);
		g.AddEdge('z', 'x', 6);
		g.AddEdge('t', 'y', 2);
		g.AddEdge('t', 'x', 1);
		g.AddEdge('x', 'z', 4);

		vector<int> dist;
		vector<int> parentPath;
		g.Dijkstra('s', dist, parentPath);
		g.PrintShortPath('s', dist, parentPath);*/

		// 图中带有负权路径时，贪心策略则失效了。
		// 测试结果可以看到s->t->y之间的最短路径没更新出来
		const char* str = "sytx";
		Graph<char, int, INT_MAX, true> g(str, strlen(str));
		g.AddEdge('s', 't', 10);
		g.AddEdge('s', 'y', 5);
		g.AddEdge('t', 'y', -7);
		g.AddEdge('y', 'x', 3);
		vector<int> dist;
		vector<int> parentPath;
		g.Dijkstra('s', dist, parentPath);
		g.PrintShortPath('s', dist, parentPath);
	}
	void TestGraphBellmanFord()
	{
		/*	const char* str = "syztx";
			Graph<char, int, INT_MAX, true> g(str, strlen(str));
			g.AddEdge('s', 't', 6);
			g.AddEdge('s', 'y', 7);
			g.AddEdge('y', 'z', 9);
			g.AddEdge('y', 'x', -3);
			g.AddEdge('z', 's', 2);
			g.AddEdge('z', 'x', 7);
			g.AddEdge('t', 'x', 5);
			g.AddEdge('t', 'y', 8);
			g.AddEdge('t', 'z', -4);
			g.AddEdge('x', 't', -2);
			vector<int> dist;
			vector<int> parentPath;
			g.BellmanFord('s', dist, parentPath);
			g.PrintShortPath('s', dist, parentPath);*/

		const char* str = "syztx";
		Graph<char, int, INT_MAX, true> g(str, strlen(str));
		g.AddEdge('s', 't', 6);
		g.AddEdge('s', 'y', 7);
		g.AddEdge('y', 'z', 9);
		g.AddEdge('y', 'x', -3);
		g.AddEdge('y', 's', 1); // 新增
		g.AddEdge('z', 's', 2);
		g.AddEdge('z', 'x', 7);
		g.AddEdge('t', 'x', 5);
		g.AddEdge('t', 'y', -8); //更改
		//g.AddEdge('t', 'y', 8);

		g.AddEdge('t', 'z', -4);
		g.AddEdge('x', 't', -2);
		vector<int> dist;
		vector<int> parentPath;
		if (g.BellmanFord('s', dist, parentPath))
			g.PrintShortPath('s', dist, parentPath);
		else
			cout << "带负权回路" << endl;
	}

	void TestFloydWarShall()
	{
		const char* str = "12345";
		Graph<char, int, INT_MAX, true> g(str, strlen(str));
		g.AddEdge('1', '2', 3);
		g.AddEdge('1', '3', 8);
		g.AddEdge('1', '5', -4);
		g.AddEdge('2', '4', 1);
		g.AddEdge('2', '5', 7);
		g.AddEdge('3', '2', 4);
		g.AddEdge('4', '1', 2);
		g.AddEdge('4', '3', -5);
		g.AddEdge('5', '4', 6);
		vector<vector<int>> vvDist;
		vector<vector<int>> vvParentPath;
		g.FloydWarshall(vvDist, vvParentPath);

		 //打印任意两点之间的最短路径
		for (size_t i = 0; i < strlen(str); ++i)
		{
			g.PrintShortPath(str[i], vvDist[i], vvParentPath[i]);
			cout << endl;
		}
	}
}
