#include <iostream>
#include <vector>
using namespace std;


//class Solution {
//public:
//    vector<vector<int>> threeSum(vector<int>& nums) {
//        vector<vector<int>> ret;
//        sort(nums.begin(), nums.end());
//        int n = nums.size();
//        for (int i = 0; i < n - 2;)
//        {
//            int left = i + 1, right = n - 1;
//            int target = -nums[i];
//            while (left < right)
//            {
//                if (nums[left] + nums[right] < target) ++left;
//                else if (nums[left] + nums[right] > target) --right;
//                else {
//                    //这里已经有结果，同时需要向后排除重复的结果
//                    ret.push_back({ nums[i],nums[left++],nums[right--] });
//                    //排除left和right的重复的数字
//                    while (left < right && nums[left - 1] == nums[left]) ++left;
//                    while (left < right && nums[right + 1] == nums[right]) --right;
//                }
//            }
//            //这里说明i位置的已经全部选择完成，接下来要去重i位置
//            ++i;
//            while (i < n - 2 && nums[i - 1] == nums[i]) ++i;
//        }
//        return ret;
//    }
//};

void BufferSort(vector<int>& arr)
{
	int n = arr.size();
	for (int i = 1; i < n; ++i)
	{
		bool flag = true;
		for (int j = 0; j < n-i; ++j)  //一轮冒泡排序
		{
			if (arr[j] > arr[j + 1]) {
				flag = false;
				swap(arr[j], arr[j + 1]);
			}
		}
		if (flag) break;  //这里说明这一趟都是没有排序过的，那么这里一定整体有序了
	}
}


int main()
{
	vector<int> arr = { 3,4,2,5,1,6,7,0,9,8 ,1 };
	BufferSort(arr);
	for (auto e : arr)
	{
		cout << e << " ";
	}
	return 0;
}