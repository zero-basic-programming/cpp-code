#pragma once
#include "RBTree.h"

namespace lra
{
	template <class K, class V>
	class map
	{
		struct MapKeyOfT
		{
			const K& operator()(const pair<K, V>& kv)
			{
				return kv.first;
			}
		};
	public:
		typedef typename RBTree<K, pair<const K, V>, MapKeyOfT>::iterator iterator;
		typedef typename RBTree<K, pair<const K, V>, MapKeyOfT>::const_iterator const_iterator;
		iterator begin()
		{
			return _t.begin();
		}
		const_iterator begin() const
		{
			return _t.begin();
		}
		iterator end()
		{
			return _t.end();
		}
		const_iterator end()const
		{
			return _t.end();
		}
		pair<iterator, bool> insert(pair<K, V> kv)
		{
			return _t.insert(kv);
		}
		V& operator[](const K& key)
		{
			pair<iterator, bool> ret = insert(make_pair(key, V()));
			return ret.first->second;
		}
	private:
		RBTree<K, pair<const K,V>, MapKeyOfT> _t;
	};
	void testmap()
	{
		int a[] = { 16, 3, 7, 11, 9, 26, 18, 14, 15 };
		map<int,int> m;
		for (auto e : a)
		{
			m[e] = e;
		}
		auto it = m.begin();
		while (it != m.end())
		{
			cout << it->first << " ";
			++it;
		}
		cout << endl;
	}
}