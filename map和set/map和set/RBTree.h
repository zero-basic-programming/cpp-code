#pragma once
//定义红黑
enum Colour
{
	RED,
	BLACK,
};

//红黑树节点结构
template<class T>
struct RBTreeNode
{
	T _data;//这里可能是键值对，或者是key，根据需求来传递
	RBTreeNode<T>* _left;
	RBTreeNode<T>* _right;
	RBTreeNode<T>* _parent;
	Colour _col;

	RBTreeNode(const T& data)
		:_data(data)
		, _left(nullptr)
		, _right(nullptr)
		, _parent(nullptr)
		, _col(RED)
	{}
};

//红黑树迭代器类
template <class T,class Ref,class Ptr>
struct RBTree_iterator
{
	typedef RBTreeNode<T> Node;
	typedef RBTree_iterator<T,Ref,Ptr> Self;
	typedef RBTree_iterator<T, T&, T*> iterator;
	Node* _node;//记录迭代器的节点
	RBTree_iterator(Node* node)
		:_node(node)
	{}
	// 普通迭代器的时候，他是拷贝构造
	// const迭代器的时候，他是构造，支持用普通迭代器构造const迭代器
	RBTree_iterator(const iterator& s)
		: _node(s._node)
	{}

	T& operator*()
	{
		return _node->_data;
	}
	T* operator->()
	{
		return &_node->_data;
	}
	Self& operator++()
	{
		if (_node->_right)
		{
			//找右子树的最左
			Node* tmp = _node->_right;
			while (tmp->_left)
			{
				tmp = tmp->_left;
			}
			_node = tmp;
		}
		else
		{
			//看看node的parent有没有被遍历过，没有就更新，有就向上更新
			Node* cur = _node;
			Node* parent = _node->_parent;
			while (parent && parent->_right == cur)
			{
				//向上更新
				cur = cur->_parent;
				parent = parent->_parent;
			}
			_node = parent;
		}
		return *this;
	}
	Self operator--()
	{
		if (_node->_left)
		{
			//找左子树的最右节点
			Node* tmp = _node->_left;
			while (tmp->_right)
			{
				tmp = tmp->_right;
			}
			_node = tmp;
		}
		else
		{
			//向上不断更新，知道有左
			Node* cur = _node;
			Node* parent = _node->_parent;
			while (parent && parent->_right == cur)
			{
				cur = cur->_parent;
				parent = parent->_parent;
			}
			_node = parent;
		}
		return *this;
	}
	bool operator==(const Self& r)
	{
		return _node == r._node;
	}
	bool operator!=(const Self& r)
	{
		return _node != r._node;
	}
};

//红黑树实现
template <class K,class T,class KeyOfT>
class RBTree
{
	typedef RBTreeNode<T> Node;
public:
	typedef RBTree_iterator<T, T&, T*> iterator;
	typedef RBTree_iterator<T,const T&,const T*> const_iterator;
	iterator begin()
	{
		//找最左节点
		Node* left = _root;
		while (left->_left)
		{
			left = left->_left;
		}
		return iterator(left);
	}
	const_iterator begin()const 
	{
		//找最左节点
		Node* left = _root;
		while (left->_left)
		{
			left = left->_left;
		}
		return const_iterator(left);
	}
	iterator end()
	{
		//最后一个是_root的parent，这里设计成空
		return iterator(nullptr);
	}
	const_iterator end() const
	{
		//最后一个是_root的parent，这里设计成空
		return const_iterator(nullptr);
	}
	pair<iterator,bool> insert(const T& data)
	{
		//第一个直接插入，并改为黑
		if (_root == nullptr)
		{
			_root = new Node(data);
			_root->_col = BLACK;
			return make_pair(iterator(_root),true);
		}
		KeyOfT kot;
		Node* cur = _root;
		Node* parent = nullptr;
		//找到空，插入节点
		while (cur)
		{
			if (kot(cur->_data) < kot(data))
			{
				parent = cur;
				cur = cur->_right;
			}
			else if (kot(cur->_data) > kot(data))
			{
				parent = cur;
				cur = cur->_left;
			}
			else
			{
				return make_pair(iterator(cur),false);
			}
		}
		cur = new Node(data);
		cur->_parent = parent;
		Node* newnode = cur;//记录当前节点，因为后面插入可能会更新迭代cur
		if (kot(parent->_data) > kot(data))
		{
			parent->_left = cur;
		}
		else
		{
			parent->_right = cur;
		}
		//判断是否满足红黑树的规则
		while (parent && parent->_col == RED)
		{
			Node* grandfather = parent->_parent;
			//parent在grandfather的左
			if (grandfather->_left == parent)
			{
				Node* uncle = grandfather->_right;
				//分3种情况
				//1.uncle存在&&uncle和parent都是红，直接把他们改成黑，然后grandfather改成红迭代上去即可
				if (uncle && uncle->_col == RED)
				{
					uncle->_col = parent->_col = BLACK;
					grandfather->_col = RED;
					//迭代向上更新，有可能grandfather上面是红
					cur = grandfather;
					parent = cur->_parent;
				}
				else
				{
					//情况2或者3
					//情况2：cur在parent左边，形成直线型，直接右旋变色即可，parent变成黑，grandfather变成红
					if (parent->_left == cur)
					{
						RotateR(grandfather);
						parent->_col = BLACK;
						grandfather->_col = RED;
					}
					else
					{
						//情况3：形成折线型，先左旋parent再右旋grandfather然后变色，cur最终变黑，grandfather变红
						RotateL(parent);
						RotateR(grandfather);
						cur->_col = BLACK;
						grandfather->_col = RED;
					}
					//无论是情况2还是3，最终上面的节点都是黑，就不需要更新了，直接跳出
					break;
				}
			}
			else
			{
				//parent在grandfather的右
				Node* uncle = grandfather->_left;
				//情况1
				if (uncle && uncle->_col == RED)
				{
					uncle->_col = parent->_col = BLACK;
					grandfather->_col = RED;
					//迭代
					cur = grandfather;
					parent = cur->_parent;
				}
				else
				{
					//情况2
					if (parent->_right == cur)
					{
						//左旋加变色，parent变黑，grandfather变红
						RotateL(grandfather);
						parent->_col = BLACK;
						grandfather->_col = RED;
					}
					else
					{
						//情况3：先右旋再左旋，cur变黑，grandfather变红
						RotateR(parent);
						RotateL(grandfather);
						cur->_col = BLACK;
						grandfather->_col = RED;
					}
				}
			}
		}
		//最终根一定是黑的
		_root->_col = BLACK;
		return make_pair(iterator(newnode), true);
	}
	void Inorder()
	{
		_Inorder(_root);
	}
	bool IsvalidRBTree()
	{
		//空树是红黑树
		if (_root == nullptr)
			return true;
		//根是黑色
		if (_root->_col != BLACK)
		{
			cout << "不满足规则2：根节点必须为黑色" << endl;
			return false;
		}
		//每个路径，黑色节点数量相同
		Node* cur = _root;
		int blackcount = 0;//记录黑色节点数量
		while (cur)
		{
			if (cur->_col == BLACK)
				++blackcount;
			cur = cur->_left;
		}
		int k = 0;//用来记录每个路径的黑色节点个数
		return _IsvaildRBTree(_root, k, blackcount);
	}
private:
	bool _IsvaildRBTree(Node* root, int k, const int blackcount)
	{
		if (root == nullptr)
		{
			if (k != blackcount)
			{
				cout << "不满足规则4，每个路径的黑色节点数相同" << endl;
				return false;
			}
			return true;
		}
		Node* parent = root->_parent;
		//往上找是否存在相连的红色节点
		if (parent && parent->_col == root->_col && root->_col == RED)
		{
			cout << "存在相连的红节点，不满足规则3" << endl;
			return false;
		}
		if (root->_col == BLACK)
			++k;
		return _IsvaildRBTree(root->_left, k, blackcount) && _IsvaildRBTree(root->_right, k, blackcount);
	}

	void _Inorder(Node* root)
	{
		if (root == nullptr)
			return;
		_Inorder(root->_left);
		cout << root->_kv.first << ":" << root->_kv.second << endl;
		_Inorder(root->_right);
	}
	void RotateR(Node* parent)
	{
		Node* subL = parent->_left;
		Node* subLR = subL->_right;
		parent->_left = subLR;
		if (subLR)
		{
			subLR->_parent = parent;
		}
		Node* ppNode = parent->_parent;
		parent->_parent = subL;
		subL->_right = parent;
		if (ppNode == nullptr)//等价于_root == parent
		{
			_root = subL;
			_root->_parent = nullptr;
		}
		else
		{
			if (ppNode->_left == parent)
			{
				ppNode->_left = subL;
			}
			else
			{
				ppNode->_right = subL;
			}
			subL->_parent = ppNode;
		}
	}
	void RotateL(Node* parent)
	{
		Node* subR = parent->_right;
		Node* subRL = subR->_left;
		parent->_right = subRL;
		if (subRL)
		{
			subRL->_parent = parent;
		}
		Node* ppNode = parent->_parent;
		parent->_parent = subR;
		subR->_left = parent;
		//ppNode可能为空
		if (ppNode == nullptr)
		{
			_root = subR;
			_root->_parent = nullptr;
		}
		else
		{
			//parent可能是ppNode的左右孩子
			if (ppNode->_left == parent)
			{
				ppNode->_left = subR;
			}
			else
			{
				ppNode->_right = subR;
			}
			subR->_parent = ppNode;
		}
	}
	Node* _root = nullptr;
};