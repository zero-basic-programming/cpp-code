#include <iostream>
#include <vector>
using namespace std;

//class Solution {
//public:
//    int search(vector<int>& nums, int target) {
//        int left = 0, right = nums.size() - 1;
//        int mid = left + (right - left) / 2;
//        while (left <= right)
//        {
//            mid = left + (right - left) / 2;
//            cout << mid << ":" << nums[mid] << endl;
//            if (nums[mid] < target)
//                left = mid + 1;
//            else if (nums[mid] > target)
//                right = mid - 1;
//            else return mid;
//        }
//        return -1;
//    }
//};


//class Solution {
//public:
//    vector<int> searchRange(vector<int>& nums, int target) {
//        if (nums.size() == 0)    return { -1,-1 };
//        //分别求出其最左或者最右的区间即可
//        int left = 0, right = nums.size() - 1;
//        //找最左端点
//        while (left < right)
//        {
//            int mid = left + (right - left) / 2;
//            if (nums[mid] < target)  left = mid + 1;
//            else right = mid;
//        }
//        int begin = left; //这里一定是左右两个边界相等才会出循环
//        //有可能没有结果
//        if (nums[left] != target) return { -1,-1 };
//        left = 0;
//        right = nums.size() - 1;
//        //找右边界
//        while (left < right)
//        {
//            int mid = left + (right - left) / 2;
//            if (nums[mid] <= target) left = mid;
//            else right = mid - 1;
//        }
//        return { begin,right };
//    }
//};


//class Solution {
//public:
//    int searchInsert(vector<int>& nums, int target) {
//        int left = 0; int right = nums.size() - 1;
//        while (left <= right)
//        {
//            int mid = left + (right - left) / 2;
//            if (nums[mid] < target) left = mid + 1;
//            else if (nums[mid] > target)   right = mid - 1;
//            else return mid;
//        }
//        return left;
//    }
//};



