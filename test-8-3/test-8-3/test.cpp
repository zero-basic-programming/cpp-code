#include <iostream>
#include <vector>
using namespace std;

//class Solution {
//public:
//    int getn(int n)
//    {
//        int ret = 0;
//        while (n)
//        {
//            int tmp = n % 10;
//            ret += (tmp * tmp);
//            n /= 10;
//        }
//        return ret;
//    }
//    bool isHappy(int n) {
//        //使用快慢指针的方法解决循环问题
//        int slow = n;
//        int fast = getn(n);
//        while (fast != slow)
//        {
//            slow = getn(slow);
//            fast = getn(getn(fast));
//            if (slow == fast)    break;
//        }
//        if (slow == 1)   return true;
//        return false;
//    }
//};


//class Solution {
//public:
//    int maxArea(vector<int>& height) {
//        int front = 0;
//        int tail = height.size() - 1;
//        int maxresult = 0;
//        while (front < tail)
//        {
//            int tmp = (tail - front);
//            int minval = height[front];
//            if (height[tail] < minval)   minval = height[tail];
//            tmp *= minval;
//            if (maxresult < tmp) maxresult = tmp;
//            //看哪个是短板就更新哪个
//            if (height[front] < height[tail])    ++front;
//            else  --tail;
//        }
//        return maxresult;
//    }
//};


//class Solution {
//public:
//    int numDecodings(string s) {
//        int n = s.size();
//        vector<int> dp(n);
//        //初始化
//        dp[0] = s[0] == '0' ? 0 : 1;
//        if (n == 1)
//            return dp[0];
//        //判断单数情况
//        if ('1' <= s[1] && s[1] <= '9') dp[1] = dp[0];
//        //判断双数情况
//        int tmp = (s[0] - '0') * 10 + (s[1] - '0');
//        if (10 <= tmp && tmp <= 26) dp[1] += 1;
//        for (int i = 2; i < n; ++i)
//        {
//            if (s[i] != '0')   dp[i] = dp[i - 1];
//            int tmp = (s[i - 1] - '0') * 10 + (s[i] - '0');
//            if (10 <= tmp && tmp <= 26) dp[i] += dp[i - 2];
//        }
//        return dp[n - 1];
//    }
//};


//class Solution {
//public:
//    int numDecodings(string s) {
//        int n = s.size();
//        vector<int> dp(n + 1);
//
//        //初始化
//        dp[0] = 1;
//        dp[1] = s[0] == '0' ? 0 : 1;
//        for (int i = 2; i <= n; ++i)
//        {
//            if (s[i - 1] != '0')   dp[i] = dp[i - 1];
//            int tmp = (s[i - 2] - '0') * 10 + (s[i - 1] - '0');
//            if (10 <= tmp && tmp <= 26) dp[i] += dp[i - 2];
//        }
//        return dp[n];
//    }
//};


//class Solution {
//public:
//    int uniquePaths(int m, int n) {
//        vector<int> dp(n, 1); //同步完成初始化，因为这里第一行都只有一种方式
//        for (int i = 0; i < m - 1; ++i)
//        {
//            //这里不能越界，同时第一列也是之后一种方式
//            for (int j = 1; j < n; ++j)
//            {
//                //状态转移方程
//                dp[j] += dp[j - 1];
//            }
//        }
//        return dp[n - 1];
//    }
//};


//class Solution {
//public:
//    int uniquePathsWithObstacles(vector<vector<int>>& obstacleGrid) {
//        int m = obstacleGrid.size();
//        int n = obstacleGrid[0].size();
//        vector<int> dp(n, 0);
//        //初始化
//        for (int i = 0; i < n; ++i)
//        {
//            //有障碍
//            if (obstacleGrid[0][i] == 1)
//            {
//                break;
//            }
//            dp[i] = 1;
//        }
//        for (int i = 1; i < m; ++i)
//        {
//            for (int j = 0; j < n; ++j)
//            {
//                if (obstacleGrid[i][j] == 1) dp[j] = 0;
//                else
//                {
//                    if (j != 0)
//                        dp[j] += dp[j - 1];
//                }
//            }
//        }
//        return dp[n - 1];
//    }
//};


//class Solution {
//public:
//    int maxValue(vector<vector<int>>& grid) {
//        int m = grid.size();
//        int n = grid[0].size();
//        vector<int> dp(n, 0);
//        //初始化
//        dp[0] = grid[0][0];
//        for (int i = 1; i < n; ++i)
//        {
//            dp[i] = (dp[i - 1] + grid[0][i]);
//        }
//        for (int i = 1; i < m; ++i)
//        {
//            for (int j = 0; j < n; ++j)
//            {
//                if (j == 0)
//                {
//                    dp[j] += grid[i][j];
//                }
//                else
//                {
//                    dp[j] = max(dp[j], dp[j - 1]) + grid[i][j];
//                }
//            }
//        }
//        return dp[n - 1];
//    }
//};


class Solution {
public:
    int maxValue(vector<vector<int>>& grid) {
        int m = grid.size();
        int n = grid[0].size();
        vector<int> dp(n, 0);
        for (int i = 0; i < m; ++i)
        {
            for (int j = 0; j < n; ++j)
            {
                if (j == 0)
                {
                    dp[j] += grid[i][j];
                }
                else
                {
                    dp[j] = max(dp[j], dp[j - 1]) + grid[i][j];
                }
            }
        }
        return dp[n - 1];
    }
};
