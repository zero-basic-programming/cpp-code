#include <iostream>
#include <vector>
#include <string>
using namespace std;


//class Solution {
//public:
//    string multiply(string num1, string num2) {
//        int m = num1.size();
//        int n = num2.size();
//        //反转原来的字符串，为了更好的利用下标
//        reverse(num1.begin(), num1.end());
//        reverse(num2.begin(), num2.end());
//        int sz = m + n - 1;  //这个是新数组应该开辟的大小，也就是相乘的结果
//        vector<int> arr(sz);
//        //先处理无进位相乘
//        for (int i = 0; i < m; ++i) //遍历第一个string
//        {
//            for (int j = 0; j < n; ++j)
//            {
//                int v1 = num1[i] - '0';
//                int v2 = num2[j] - '0';
//                arr[i + j] += v1 * v2;
//            }
//        }
//        //处理进位
//        string ret;
//        int t = 0;
//        int i = 0;
//        while (i < sz || t)
//        {
//            if (i < sz) t += arr[i++];
//            ret += t % 10 + '0';
//            t /= 10;
//        }
//        //处理前导零
//        while (ret.size() > 1 && ret.back() == '0') ret.pop_back();
//        reverse(ret.begin(), ret.end());
//        return ret;
//    }
//};


//class Solution {
//public:
//    string removeDuplicates(string s) {
//        string st; //模拟栈
//        for (auto ch : s)
//        {
//            if (!st.empty() && st.back() == ch) st.pop_back();
//            else st.push_back(ch);
//        }
//        return st;
//    }
//};


//class Solution {
//public:
//    bool backspaceCompare(string s, string t) {
//        return SameString(s) == SameString(t);
//    }
//    string SameString(string str)
//    {
//        string st; //模拟栈
//        for (auto ch : str)
//        {
//            if (ch == '#') {
//                if (!st.empty()) st.pop_back();
//            }
//            else st.push_back(ch);
//        }
//        return st;
//    }
//};


//class Solution{
//public:
//    bool validateStackSequences(vector<int>&pushed, vector<int>&popped) {
//        stack<int> st;
//        int i = 0;
//        for (auto e : pushed)
//        {
//            st.push(e);
//            while (!st.empty() && st.top() == popped[i])
//            {
//                ++i;
//                st.pop();
//            }
//        }
//        return st.empty();
//    }
//};


class Solution{
public:
    int calculate(string s) {
        vector<int> st;
        int i = 0;
        while (i < s.size())
        {
            if (s[i] == ' ') ++i; //特殊处理一下空格，直接跳过
            else if (s[i] == '+') {
                st.push_back(s[i + 1] - '0'); //无需处理，直接把后一个数字放入栈中
                i += 2;
            }
            else if (s[i] == '-')
            {
                int val = -(s[i + 1] - '0'); //把后面那个数字变为相反数,放入栈中
                st.push_back(val);
                i += 2;
            }
            else if (s[i] == '*') {
                //把栈顶元素取出来，和s[i+1]相乘，放入栈中
                int left = st.back();
                st.pop_back(); //记得把当前数字取出
                int right = s[i + 1] - '0';
                st.push_back(left * right);
                i += 2; //跳过后面那个字符
            }
            else if (s[i] == '/')
            {
                //把栈顶元素取出来，和s[i+1]相乘，放入栈中
                int left = st.back();
                st.pop_back(); //记得把当前数字取出
                int right = s[i + 1] - '0';
                st.push_back(left / right);
                i += 2; //跳过后面那个字符
            }
            else {
                //数字字符，放入栈中
                st.push_back(s[i++] - '0');
            }
        }
        //直接让栈中元素全部相加即可
        int ret = 0;
        for (auto e : st)
        {
            ret += e;
        }
        return ret;
    }
};