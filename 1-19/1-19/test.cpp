#include <iostream>
#include <vector>
#include <list>
using namespace std;


//class LRUCache {
//    typedef list<pair<int, int>>::iterator LtIter;
//public:
//    LRUCache(int capacity)
//        :_capacity(capacity)
//    {
//    }
//
//    int get(int key) {
//        //如果找到就更新位置，同时返回找不到返回-1
//        auto it = _hash.find(key);
//        if (it != _hash.end())
//        {
//            //找到了
//            LtIter iter = it->second;
//            _List.splice(_List.begin(), _List, iter); // 这里可以保证迭代器不失效
//            return iter->second;
//        }
//        else return -1;
//    }
//
//    void put(int key, int value) {
//        //如果找到就更新
//        auto it = _hash.find(key);
//        if (it != _hash.end())
//        {
//            //更新
//            auto iter = _hash[key];
//            iter->second = value;
//            _List.splice(_List.begin(), _List, iter);
//        }
//        else {
//            //没找到,把最久的元素删去，插入最新元素
//            if (_capacity == _List.size())
//            {
//                pair<int, int> back = _List.back();
//                _hash.erase(back.first);
//                _List.pop_back();
//            }
//            //这里list没满，可以直接插入
//            _List.push_front(make_pair(key, value));
//            _hash[key] = _List.begin();
//        }
//    }
//private:
//    unordered_map<int, LtIter> _hash;
//    list<pair<int, int>> _List;
//    int _capacity;
//};


void Qsort(vector<int>& arr, int l, int r)
{
	if (l >= r) return;
	int key = arr[rand() % (r - l + 1) + l];
	int left = l - 1, right = r + 1;
	int i = l;
	while (i < right)
	{
		if (arr[i] < key) swap(arr[i++], arr[++left]);
		else if (arr[i] == key) ++i;
		else swap(arr[i], arr[--right]);
	}
	Qsort(arr, l, left);
	Qsort(arr, right, r);
}

int main()
{
	vector<int> arr = { 1,2,5,3,6,8,9,8,0,6 };
	int n = arr.size();
	Qsort(arr, 0, n - 1);
	for (auto e : arr)
	{
		cout << e << " ";
	}

	return 0;
}