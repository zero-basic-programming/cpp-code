#include <iostream>
#include <vector>
using namespace std;


//class Solution {
//public:
//    int minCostClimbingStairs(vector<int>& cost) {
//        int n = cost.size();
//        vector<int> dp(n + 1);
//        for (int i = 2; i <= n; ++i)
//        {
//            dp[i] = min(dp[i - 1] + cost[i - 1], dp[i - 2] + cost[i - 2]);
//        }
//        return dp[n];
//    }
//};


//class Solution {
//public:
//    int minCostClimbingStairs(vector<int>& cost) {
//        int n = cost.size();
//        vector<int> dp(n + 1);
//        int first = 0, second = 0, third = 0;
//        for (int i = 2; i <= n; ++i)
//        {
//            third = min(first + cost[i - 2], second + cost[i - 1]);
//            // 迭代更新
//            first = second;
//            second = third;
//        }
//        return third;
//    }
//};


//class Solution {
//public:
//    int numSubarrayProductLessThanK(vector<int>& nums, int k) {
//        int n = nums.size();
//        int ret = 0;
//        for (int i = 0; i < n; ++i)
//        {
//            //判断当前位置是否<k
//            if (nums[i] >= k) continue;   //以这个位置为起始没有结果
//            else
//            {
//                ++ret;
//                int mul = nums[i];   //记录乘积
//                for (int j = i + 1; j < n; ++j)
//                {
//                    if (nums[j] * mul >= k) break;
//                    else
//                    {
//                        mul *= nums[j];
//                        ++ret;
//                    }
//                }
//            }
//        }
//        return ret;
//    }
//};


//class Solution {
//public:
//    int numSubarrayProductLessThanK(vector<int>& nums, int k) {
//        int n = nums.size();
//        int ret = 0;
//        for (int i = 0; i < n; ++i)
//        {
//            int mul = 1;   //记录乘积
//            for (int j = i; j < n; ++j)
//            {
//                if (nums[j] * mul >= k) break;
//                else
//                {
//                    mul *= nums[j];
//                    ++ret;
//                }
//
//            }
//        }
//        return ret;
//    }
//};


//class Solution {
//public:
//    int numSubarrayProductLessThanK(vector<int>& nums, int k) {
//        //使用滑动窗口
//        int n = nums.size();
//        int left = 0, right = 0, ret = 0, mul = 1;
//        if (k == 0) return 0;   //特殊情况，理论上这里的数字都是>0的，怎么给个0?
//        while (right < n)
//        {
//            //进窗口
//            mul *= nums[right];
//            //判断窗口的合法性
//            while (left <= right && mul >= k)
//            {
//                //出窗口
//                mul /= nums[left++];
//            }
//            //收集结果，最小也是0，都可以收集
//            ret += (right - left + 1);
//            ++right;
//        }
//        return ret;
//    }
//};



//
//class Solution {
//public:
//    int rob(vector<int>& nums) {
//        int n = nums.size();
//        vector<int> f(n), g(n);   //f表示在该位置偷，g表示在该位置不偷
//        //初始化
//        f[0] = nums[0];
//        for (int i = 1; i < n; ++i)
//        {
//            f[i] = g[i - 1] + nums[i];
//            g[i] = max(f[i - 1], g[i - 1]);
//        }
//        return max(f[n - 1], g[n - 1]);
//    }
//};


