#pragma once
#include <iostream>
#include <cassert>
using namespace std;


namespace liang
{
	//节点类
	template <class T>
	class ListNode
	{
	public:
		ListNode(const T& val = T())
			:_val(val), _prev(nullptr), _next(nullptr)
		{}
		//成员变量
		T _val;
		ListNode* _prev;   //前驱指针
		ListNode* _next;   //后继指针
	};
	//迭代器类
	template <class T, class Ref, class Ptr>
	class ListIterator
	{
	public:
		typedef ListNode<T> Node;
		typedef ListIterator<T, Ref, Ptr> self;
		//构造函数
		ListIterator(Node* node)
			:_node(node)
		{}
		//运算符重载
		//前置
		self operator++()
		{
			_node = _node->_next;
			return *this;
		}
		self operator--()
		{
			_node = _node->_prev;
			return *this;
		}
		//后置
		self operator++(int)
		{
			self tmp(*this);
			_node = _node->next;
			return tmp;
		}
		self operator--(int)
		{
			self tmp(*this);
			_node = _node->_prev;
			return tmp;
		}
		bool operator==(const self& s) const
		{
			return _node == s._node;
		}
		bool operator!=(const self& s) const
		{
			return _node != s._node;
		}
		Ref operator*()
		{
			return _node->_val;
		}
		Ptr operator->()
		{
			return &_node->_val;
		}
		//成员变量
		Node* _node;
	};
	//双向循环链表
	template <class T>
	class List
	{
	public:
		typedef ListNode<T> Node;
		//这两个就是取迭代器的时候是否为const
		typedef ListIterator<T, T&, T*> iterator;
		typedef ListIterator<T, const T&, const T*> const_iterator;
		//默认成员函数
		List()
		{
			_head = new Node(0);
			_head->_next = _head;
			_head->_prev = _head;
		}
		List(const List<T>& lt)
		{
			//构造完成之后，然后不断插入即可
			_head = new Node(0);  //头节点的值设为0
			_head->_next = _head;
			_head->_prev = _head;
			//把lt中的节点拷贝插入到新链表中
			for (const auto& e : lt)
			{
				push_back(e);
			}
		}
		//现代写法，更加方便
		List<T>& operator=(List<T> lt)
		{
			swap(lt);
			return *this;
		}
		~List()
		{
			clear();
			delete _head;
			_head = nullptr;
		}

		//迭代器相关函数
		iterator begin()
		{
			return iterator(_head->_next);
		}
		iterator end()
		{
			return iterator(_head);
		}
		const_iterator begin() const
		{
			return const_iterator(_head->_next);
		}
		const_iterator end() const
		{
			return const_iterator(_head);
		}

		//访问容器相关函数
		T& front()
		{
			return *begin();
		}
		T& back()
		{
			return *(--end());  //最后一个节点的下一个位置
		}
		const T& front() const
		{
			return *begin();
		}
		const T& back() const
		{
			return *(--end());
		}

		//插入、删除函数
		void insert(iterator pos, const T& val)
		{
			//在pos位置的前一个位置进行插入,这里是为了方便后面的实现
			assert(pos._node);
			Node* cur = pos._node;
			Node* prev = cur->_prev;
			//创建节点改变连接
			Node* newnode = new Node(val);
			//建立连接关系
			prev->_next = newnode;
			newnode->_prev = prev;
			newnode->_next = cur;
			cur->_prev = newnode;
		}
		iterator erase(iterator pos)
		{
			assert(pos._node);
			assert(pos != end());   //不能删除头节点
			Node* cur = pos._node;
			Node* prev = cur->_prev;
			Node* next = cur->_next;
			//改变连接
			prev->_next = next;
			next->_prev = prev;
			delete cur;
			return iterator(next);
		}
		void push_back(const T& val)
		{
			insert(end(), val);
		}
		void pop_back()
		{
			erase(--end());
		}
		void push_front(const T& val)
		{
			insert(begin(), val);
		}
		void pop_front()
		{
			erase(begin());
		}

		//其他函数
		size_t size() const
		{
			//遍历一次大小
			size_t sz = 0;
			const_iterator it = begin();
			while (it != end())
			{
				++sz;
				++it;
			}
			return sz;
		}
		void resize(size_t n, const T& val = T())
		{
			iterator it = begin();
			size_t len = 0;
			while (len < n && it != end())
			{
				++len;
				++it;
			}
			if (len == n)
			{
				//说明需要删除
				while (it != end())
				{
					it = erase(it);
				}
			}
			else
			{
				while (len < n)
				{
					push_back(val);
					++len;
				}
			}
		}
		void clear()
		{
			//遍历一次，然后删除每一个节点,这里只是清空，并不删除头节点
			iterator it = begin();
			while (it != end())
			{
				it = erase(it);
			}
		}
		bool empty() const
		{
			return _head->_next == _head;
		}
		void swap(List<T>& lt)
		{
			::swap(_head, lt.head);  //这里只交换头节点即可
		}

	private:
		Node* _head; //头节点指针
	};
	void TestList()
	{
		List<int> lt;
		lt.push_back(1);
		lt.push_back(2);
		lt.push_back(3);
		lt.push_back(4);
		lt.push_front(6);
		lt.pop_back();
		auto it = lt.begin();
		while (it != lt.end())
		{
			cout << *it << " ";
			++it;
		}
	}
}
