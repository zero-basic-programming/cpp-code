#pragma once

template <class K>
class BSTreeNode
{
public:
	K _key;
	BSTreeNode<K>* _left;
	BSTreeNode<K>* _right;
	BSTreeNode(const K& key = K())
		:_key(key),_left(nullptr),_right(nullptr)
	{}
};

template <class K> 
class BSTree
{
	typedef BSTreeNode<K> Node;
public:
	//构造函数
	BSTree():_root(nullptr)
	{}
	Node* Copy(Node* root)
	{
		//构建树的过程,使用先序遍历进行构建
		if (root == nullptr) return nullptr;
		Node* node = new Node(root->_key);
		node->_left = Copy(root->_left);
		node->_right = Copy(root->_right);
		return node;
	}
	//拷贝构造函数
	BSTree(const BSTree<K>& t)
	{
		_root = Copy(t._root);
	}
	//赋值运算符重载
	BSTree<K>& operator=(BSTree<K> t)
	{
		::swap(_root, t._root);
		return *this;
	}
	void Distroy(Node* root)
	{
		if (root == nullptr) return;
		Distroy(root->_left);
		Distroy(root->_right);
		delete root;
	}
	//析构函数
	~BSTree()
	{
		//采用后序遍历的方式对节点进行释放
		Distroy(_root);
		_root = nullptr;
	}
	//插入
	bool Insert(const K& key)
	{
		//空树直接插入即可
		if (_root == nullptr)
		{
			_root = new Node(key);
			return true;
		}
		Node* cur = _root;
		Node* parent = nullptr;  //用来记录当前节点的上一个节点
		while (cur)
		{
			if (cur->_key == key)
			{
				//不重复插入
				return false;
			}
			else if (cur->_key < key)
			{
				parent = cur;
				cur = cur->_right;
			}
			else
			{
				parent = cur;
				cur = cur->_left;
			}
		}
		if (parent->_key > key)
		{
			cur = new Node(key);
			parent->_left = cur;
		}
		else
		{
			cur = new Node(key);
			parent->_right = cur;
		}
		return true;
	}
	//删除
	bool Erase(const K& key)
	{
		//这里主要看的是待删除节点的左右子树的问题
		if (_root == nullptr) return false;  
		Node* cur = _root;
		Node* parent = nullptr;
		while (cur)
		{
			if (cur->_key == key)
			{
				if (cur->_left == nullptr)
				{
					//这里需要考虑头节点的清空
					if (cur == _root)
					{
						_root = _root->_right;
					}
					else
					{
						//1.如果其左子树为空，那么直接将parent连接其右，然后删除即可
						Node* Right = cur->_right;
						if (parent->_left == cur)
						{
							parent->_left = Right;
						}
						else
						{
							parent->_right = Right;
						}
					}
					delete cur;
					return true;
				}
				else if (cur->_right == nullptr)
				{
					if (cur == _root)
					{
						_root = _root->_left;
					}
					else
					{
						//2.如果右子树为空，那么就将parent连接其左，然后删除
						Node* Left = cur->_left;
						if (parent->_left == cur)
						{
							parent->_left = Left;
						}
						else
						{
							parent->_right = Left;
						}
					}
					delete cur;
					return true;
				}
				else
				{
					//3.如果左右都不为空，那么就找其右子树的最左节点进行替换删除
					Node* minRight = cur->_right;
					Node* minParent = cur;
					while (minRight->_left)
					{
						minParent = minRight;
						minRight = minRight->_left;
					}
					cur->_key = minRight->_key;  //替换删除
					//这里minRight可能有右孩子
					if (minRight->_right)
					{
						minParent->_left = minRight->_right;
					}
					else
					{
						//这里可能是单支情况
						minParent->_right = minRight->_right;
					}
					delete minRight;
					return true;
				}
			}
			else if (cur->_key < key)
			{
				parent = cur;
				cur = cur->_right;
			}
			else
			{
				parent = cur;
				cur = cur->_left;
			}
		}
		//到这里说明没有找到
		return false;
	}
	//查找
	Node* Find(const K& key)
	{
		//因为其还是符合二叉树的性质，应该采用前序遍历的方式进行插入，效率最高
		Node* cur = _root;
		while (cur)
		{
			if (cur->_key == key)
			{
				return cur;
			}
			else if (cur->_key < key)
			{
				cur = cur->_right;
			}
			else
			{
				cur = cur->_left;
			}
		}
		//这里是遍历完没有结果
		return nullptr;
	}
	//中序遍历
	void _InOrder(Node* root)
	{
		if (root == nullptr) return;
		_InOrder(root->_left);
		cout << root->_key << " ";
		_InOrder(root->_right);
	}
	void InOrder()
	{
		_InOrder(_root);
	}
private:
	Node* _root;  //根节点
};


void TestBSTree()
{
	BSTree<int> t;
	t.Insert(1);
	t.Insert(2);
	t.Insert(3);
	t.Insert(4);
	t.Erase(2);
	t.InOrder(); 
	/*BSTree<int> t1(t);
	cout << endl;
	t1.InOrder();*/
}
