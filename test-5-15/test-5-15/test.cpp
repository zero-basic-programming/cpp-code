#include <iostream>
using namespace std;

//class Solution {
//public:
//    TreeNode* lowestCommonAncestor(TreeNode* root, TreeNode* p, TreeNode* q) {
//        //使用后序遍历模拟回溯过程
//        if (root == nullptr || root == p || root == q)   return root;
//        TreeNode* left = lowestCommonAncestor(root->left, p, q);
//        TreeNode* right = lowestCommonAncestor(root->right, p, q);
//        //从下一层中拿到左右两个结果值，如果左右两个都有结果，就说明节点是他们的祖先
//        if (left && right)   return root;
//        //如果其中一个为空，说明只有一个节点在其分支上,就返回分支节点
//        if (left && !right)  return left;
//        if (!left && right)  return right;
//        return nullptr; // 两边都为空证明左右子树没有要找的结果
//    }
//};


//class Solution {
//public:
//    TreeNode* lowestCommonAncestor(TreeNode* root, TreeNode* p, TreeNode* q) {
//        //利用二叉搜索树的性质，根大于左小于右,这里只需要找一条路径即可
//        if (root->val > p->val && root->val > q->val)
//        {
//            //这里说明这两个节点一定在当前节点的左边
//            //往左递归
//            return lowestCommonAncestor(root->left, p, q);
//        }
//        else if (root->val < p->val && root->val < q->val)
//        {
//            //这里说明这两个节点一定在当前节点的右边
//            //往右递归
//            return lowestCommonAncestor(root->right, p, q);
//        }
//        else
//            //最后一定就找到了
//            return root;
//    }
//};


//class Solution {
//public:
//    TreeNode* lowestCommonAncestor(TreeNode* root, TreeNode* p, TreeNode* q) {
//        //使用非递归，也是利用二叉搜索树的性质
//        while (root)
//        {
//            if (root->val > p->val && root->val > q->val)
//            {
//                root = root->left;
//            }
//            else if (root->val < p->val && root->val < q->val)
//            {
//                root = root->right;
//            }
//            else
//                return root;
//        }
//        return nullptr;
//    }
//};


