#include <iostream>
using namespace std;


//class Solution {
//public:
//    bool _lowestCommonAncestor(TreeNode* root, TreeNode* p, stack<TreeNode*>& st)
//    {
//        if (root == nullptr)
//        {
//            return false;
//        }
//        st.push(root);
//        if (root == p)
//        {
//            return true;
//        }
//        if (_lowestCommonAncestor(root->left, p, st))
//        {
//            return true;
//        }
//        if (_lowestCommonAncestor(root->right, p, st))
//        {
//            return true;
//        }
//        //回退
//        st.pop();
//        return false;
//    }
//    TreeNode* lowestCommonAncestor(TreeNode* root, TreeNode* p, TreeNode* q) {
//        //回溯法
//        stack<TreeNode*> pv;
//        stack<TreeNode*> qv;
//        _lowestCommonAncestor(root, p, pv);
//        _lowestCommonAncestor(root, q, qv);
//        while (pv.size() != qv.size())
//        {
//            if (pv.size() > qv.size())
//            {
//                pv.pop();
//            }
//            else
//                qv.pop();
//        }
//        while (pv.top() != qv.top())
//        {
//            pv.pop();
//            qv.pop();
//        }
//        return pv.top();
//    }
//};


//class Solution {
//public:
//	void _Convert(TreeNode* cur, TreeNode*& prev)
//	{
//		if (cur == nullptr)
//		{
//			return;
//		}
//		//中序走到最小
//		_Convert(cur->left, prev);
//		//建立链接关系
//		if (prev)
//		{
//			prev->right = cur;
//		}
//		cur->left = prev;
//		prev = cur;
//		_Convert(cur->right, prev);
//	}
//	TreeNode* Convert(TreeNode* root) {
//		if (root == nullptr)
//			return nullptr;
//		TreeNode* prev = nullptr;
//		_Convert(root, prev);
//		//返回根
//		while (root->left)
//		{
//			root = root->left;
//		}
//		return root;
//	}
//};


//class Solution {
//public:
//    TreeNode* _buildTree(vector<int>& preorder, vector<int>& inorder, int begin, int end, int& i)
//    {
//        if (begin > end)
//            return nullptr;
//        //从中序中找子树区间
//        int j = begin;
//        for (; j <= end; ++j)
//        {
//            if (inorder[j] == preorder[i])
//                break;
//        }
//        TreeNode* root = new TreeNode(preorder[i++]);
//        //[begin,j-1] j [j+1,end]
//        root->left = _buildTree(preorder, inorder, begin, j - 1, i);
//        root->right = _buildTree(preorder, inorder, j + 1, end, i);
//        return root;
//    }
//    TreeNode* buildTree(vector<int>& preorder, vector<int>& inorder) {
//        //先序找根，中序找子树
//        //采用闭区间
//        int i = 0;
//        return  _buildTree(preorder, inorder, 0, inorder.size() - 1, i);
//    }
//};

//class Solution {
//public:
//    TreeNode* _buildTree(vector<int>& inorder, vector<int>& postorder, int begin, int end, int& i)
//    {
//        if (begin > end)
//            return nullptr;
//        int j = begin;
//        for (; j <= end; ++j)
//        {
//            if (postorder[i] == inorder[j])
//                break;
//        }
//        TreeNode* root = new TreeNode(postorder[i--]);
//        root->right = _buildTree(inorder, postorder, j + 1, end, i);
//        root->left = _buildTree(inorder, postorder, begin, j - 1, i);
//        return root;
//    }
//    TreeNode* buildTree(vector<int>& inorder, vector<int>& postorder) {
//        int i = postorder.size() - 1;
//        return _buildTree(inorder, postorder, 0, inorder.size() - 1, i);
//    }
//};

//class Solution {
//public:
//    vector<int> preorderTraversal(TreeNode* root) {
//        //分成两部分，左路节点和右子树
//        TreeNode* cur = root;
//        stack<TreeNode*> st;
//        vector<int> v;
//        while (!st.empty() || cur)
//        {
//            while (cur)
//            {
//                v.push_back(cur->val);
//                st.push(cur);
//                cur = cur->left;
//            }
//            //走右子树
//            TreeNode* tmp = st.top();
//            st.pop();
//
//            cur = tmp->right;
//        }
//        return v;
//    }
//};
//int main()
//{
//
//	return 0;
//}

//class Solution {
//public:
//    vector<int> inorderTraversal(TreeNode* root) {
//        //前序遍历栈弹出的元素顺序就是中序
    //    vector<int> v;
    //    stack<TreeNode*> st;
    //    TreeNode* cur = root;
    //    while (!st.empty() || cur)
    //    {
    //        while (cur)
    //        {
    //            st.push(cur);
    //            cur = cur->left;
    //        }
    //        //弹出，并存放结果
    //        TreeNode* tmp = st.top();
    //        st.pop();
    //        v.push_back(tmp->val);

    //        cur = tmp->right;
    //    }
    //    return v;
    //}
//};

//class Solution {
//public:
//    vector<int> postorderTraversal(TreeNode* root) {
//        //右路节点和左子树
//        vector<int> v;
//        stack<TreeNode*> st;
//        TreeNode* cur = root;
//        while (!st.empty() || cur)
//        {
//            while (cur)
//            {
//                st.push(cur);
//                v.push_back(cur->val);
//                cur = cur->right;
//            }
//            //弹出，并存放结果
//            TreeNode* tmp = st.top();
//            st.pop();
//            cur = tmp->left;
//        }
//        reverse(v.begin(), v.end());
//        return v;
//    }
//
//};


//class Solution {
//public:
//    vector<int> postorderTraversal(TreeNode* root) {
//        vector<int> v;
//        stack<TreeNode*> st;
//        TreeNode* cur = root;
//        TreeNode* prev = nullptr;//用来记录根的右子树是否被访问
//        while (!st.empty() || cur)
//        {
//            while (cur)
//            {
//                st.push(cur);
//                cur = cur->left;
//            }
//            TreeNode* top = st.top();
//            //如果右子树为空或者到最右端返回的时候就回收结果
//            if (top->right == nullptr || top->right == prev)
//            {
//                st.pop();
//                v.push_back(top->val);
//                prev = top;//从最右端回来的时候起重要作用
//            }
//            else
//            {
//                //这时候要往右迭代
//                cur = top->right;
//            }
//        }
//        return v;
//    }
//};