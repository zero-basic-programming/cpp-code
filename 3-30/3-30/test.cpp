//#include <iostream>
//#include <vector>
//#include <cmath>
//using namespace std;
//
//int main() {
//    vector<int> dp(16385);
//    dp[2] = 3;
//    for (int i = 3; i < 16385; ++i)
//    {
//        dp[i] = dp[i - 1] + 1;  //上一次操作的情况下再复制一次
//        for (int j = 2; j <= sqrt(i); ++j)
//        {
//            if (i % j == 0)
//            {
//                int count = (i / j - 1) + 2;
//                dp[i] = min(dp[i], dp[j] + count);
//            }
//        }
//    }
//    for (int i = 0; i <= 8; ++i)
//    {
//        cout << dp[i] << " ";
//    }
//    cout << endl;
//    int T;
//    cin >> T;
//    while (T--)
//    {
//        int n;
//        cin >> n;
//        cout << dp[n] << endl;
//    }
//    return 0;
//}


//#include <iostream>
//#include <vector>
//#include <map>
//using namespace std;
////模拟快速排序
//void qsort(vector<int>& nums, vector<int>& index, int l, int r)
//{
//    if (l >= r) return;
//    int key = nums[rand() % (r - l + 1) + l];
//    int left = l - 1, right = r + 1, i = l;
//    while (i < right)
//    {
//        if (nums[i] < key)
//        {
//            swap(nums[i], nums[++left]);
//            swap(index[i], index[left]);
//            ++i;
//        }
//        else if (nums[i] == key) ++i;
//        else
//        {
//            swap(nums[i], nums[--right]);
//            swap(index[i], index[right]);
//        }
//    }
//    qsort(nums, index, l, left);
//    qsort(nums, index, right, r);
//}
//
//int main() {
//    int p = 20, n = 7, m = 1;
//    vector<int> nums(n,0);  //表示怪物的战斗力
//    for (int i = 0; i < 7; ++i) nums[i] = 10;
//    vector<int> boss(m);  //表示boss的战力
//    boss[0] = 130;
//    vector<int> index(m);  //建立boss和下标的映射关系
//    for (int i = 0; i < m; ++i) index[i] = i;
//    map<int, int> map;  //boss下标和次数的映射关系
//    qsort(boss, index, 0, m - 1);
//    //初始值，有可能不需要打怪就能打boss
//    int i = 0;
//    for (; i < m; ++i)
//    {
//        if (boss[i] < p) map[index[i]] = 0;
//        else break;
//    }
//    //每次挑和自己当前战斗力最接近的怪去打  
//    //二分查找
//    int count = 0;  //打怪次数
//    while (p <= boss[m - 1])  //战斗力大过最大的那个
//    {
//        int left = 0, right = n - 1;  //打怪
//        while (left < right)
//        {
//            int mid = left + (right - left + 1) / 2;
//            if (nums[mid] >= p) right = mid - 1;
//            else left = mid;
//        }
//        //这里有可能没有怪物,或者打怪物加的战斗值不如奖励房间就进入奖励房间,或者怪物房间中一个都打不过
//        if (n == 0 || nums[left] > p || p / 10 > nums[left])
//        {
//            p += (p / 10);
//            ++count;
//        }
//        else
//        {
//            ++count;
//            //nums[left] 为当前能打的最大的怪
//            p += nums[left];
//        }
//        //看看能打哪个boss
//        for (; i < m; ++i)
//        {
//            if (boss[i] <= p) map[index[i]] = count;
//            else break;
//        }
//    }
//    //输出结果
//    for (auto& it : map)
//    {
//        cout << it.second << endl;
//    }
//    return 0;
//}
//
//
//int main() {
//    int n;
//    cin >> n;
//    vector<int> hash(21, 0);  //记录元素和个数的映射关系
//    vector<int> vis(21, false);   //记录当前元素下标是否为所求年数
//    vis[1] = vis[3] = vis[6] = vis[10] = true; //初始化
//    while (n--)
//    {
//        int num;
//        cin >> num;
//        if (vis[num])
//        {
//            ++hash[num];    //送当前的礼物
//        }
//        //判断前面的礼物是否需要送
//        num -= 2;
//        for (int i = 1; i < num; ++i)
//        {
//            if (vis[i]) ++hash[i];
//        }
//    }
//    //收集结果
//    for (int i = 0; i < 11; ++i)
//    {
//        if (vis[i]) cout << hash[i] << " ";
//    }
//    return 0;
//}


