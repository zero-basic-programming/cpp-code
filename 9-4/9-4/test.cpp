#include <iostream>
#include <vector>
#include <unordered_map>
using namespace std;


//class Solution {
//public:
//    int lenLongestFibSubseq(vector<int>& arr) {
//        int n = arr.size();
//        vector<vector<int>> dp(n, vector<int>(n, 2));
//        unordered_map<int, int> hash;   //建立元素和下标的映射关系
//        hash[arr[0]] = 0;   //这里没有遍历到
//        int ret = 2;
//        for (int i = 1; i < n - 1; i++)
//        {
//            for (int j = i + 1; j < n; ++j)
//            {
//                int val = arr[j] - arr[i];
//                if (hash.count(val))
//                {
//                    dp[i][j] = dp[hash[val]][i] + 1;
//                    ret = max(ret, dp[i][j]);
//                }
//            }
//            hash[arr[i]] = i;
//        }
//        return ret == 2 ? 0 : ret;
//    }
//};


//class Solution {
//public:
//    int minCut(string s) {
//        int n = s.size();
//        vector<vector<bool>> check(n, vector<bool>(n, false));
//        //创建出判断是否回文
//        for (int i = n - 1; i >= 0; --i)
//        {
//            for (int j = i; j < n; ++j)
//            {
//                if (s[i] == s[j])
//                {
//                    check[i][j] = i + 1 < j ? check[i + 1][j - 1] : true;
//                }
//            }
//        }
//        vector<int> dp(n, INT_MAX);
//        for (int i = 0; i < n; ++i)
//        {
//            if (check[0][i]) dp[i] = 0;
//            else
//            {
//                for (int j = 1; j <= i; ++j)
//                {
//                    if (check[j][i]) dp[i] = min(dp[i], dp[j - 1] + 1);
//                }
//            }
//        }
//        return dp[n - 1];
//    }
//};



//class Solution {
//public:
//    int longestCommonSubsequence(string text1, string text2) {
//        int m = text1.size(), n = text2.size();
//        vector<vector<int>> dp(m + 1, vector<int>(n + 1));
//        //为了保证下标的一致性
//        text1 = " " + text1;
//        text2 = " " + text2;
//        for (int i = 1; i <= m; ++i)
//        {
//            for (int j = 1; j <= n; ++j)
//            {
//                if (text1[i] == text2[j]) dp[i][j] = dp[i - 1][j - 1] + 1;
//                else dp[i][j] = max(dp[i - 1][j], dp[i][j - 1]);
//            }
//        }
//        return dp[m][n];
//    }
//};



//class Solution {
//public:
//    vector<vector<string>> groupAnagrams(vector<string>& strs) {
//        //利用hash的方法，把排序之后字符串作为key，排序前的字符串为val
//        unordered_map<string, vector<string>> hash;
//        for (auto& str : strs)
//        {
//            string tmp = str;
//            sort(tmp.begin(), tmp.end());
//            hash[tmp].push_back(str);
//        }
//        //从hash中得到结果
//        vector<vector<string>> ret;
//        for (auto& pair : hash)
//        {
//            ret.push_back(pair.second);
//        }
//        return ret;
//    }
//};



////单例模式
//template <class T> 
//class singleton
//{
//public:
//	static T& getInstance()
//	{
//		if (_state == nullptr)
//		{
//			_state = new T();
//		}
//		return *_state;
//	}
//private:
//	class GC
//	{
//		~GC()
//		{
//			if (singleton::_state)
//			{
//				delete singleton::_state;     //防止程序在执行完之后没有回收单例
//			}
//		}
//	};
//private:
//	singleton();
//	~singleton();
//	//因为是单例，所以需要禁用拷贝构造和赋值重载
//	singleton(const singleton&);
//	singleton& operator=(const singleton&);
//	static GC _gc;        //在类执行完成之后需要释放对应new开辟的资源
//	static T* _state;     //单例
//};



//template <class T>
//class singleTon 
//{
//public:
//	static T& getInstance()
//	{
//		if (!_state)
//		{
//			_state = new T();
//		}
//		return *_state;
//	}
//	singleTon(const singleTon&) = delete;
//	singleTon& singleTon = (const singleTon&) = delete;
//private:
//	class GC    //用来回收单例
//	{
//		~GC()
//		{
//			if (singleTon::_gc) delete singleTon::_gc;
//		}
//	};
//private:
//	//构造、析构、拷贝、赋值都禁用
//	singleTon();
//	~singleTon();
//	static GC _gc;
//	static T* _state;   //单例
//};
//
////初始化单例，置为空
//template <class T>
//T* singleTon<T>::_state = nullptr;



