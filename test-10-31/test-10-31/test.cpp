#include <iostream>
#include <vector>
using namespace std;


//class Solution {
//public:
//    int longestPalindromeSubseq(string s) {
//        int n = s.size();
//        vector<vector<int>> dp(n, vector<int>(n));
//        for (int i = n - 1; i >= 0; --i)
//        {
//            for (int j = i; j < n; ++j)
//            {
//                if (s[i] == s[j])
//                {
//                    if (i == j) dp[i][j] = 1;
//                    else if (i + 1 == j) dp[i][j] = 2;
//                    else dp[i][j] = dp[i + 1][j - 1] + 2;
//                }
//                else dp[i][j] = max(dp[i][j - 1], dp[i + 1][j]);
//            }
//        }
//        return dp[0][n - 1];
//    }
//};


//class Solution {
//public:
//    int longestPalindromeSubseq(string s) {
//        int n = s.size();
//        vector<vector<int>> dp(n, vector<int>(n));
//        for (int i = n - 1; i >= 0; --i)
//        {
//            //优化
//            dp[i][i] = 1;
//            for (int j = i + 1; j < n; ++j)
//            {
//                if (s[i] == s[j])  dp[i][j] = dp[i + 1][j - 1] + 2;
//                else dp[i][j] = max(dp[i][j - 1], dp[i + 1][j]);
//            }
//        }
//        return dp[0][n - 1];
//    }
//};


//class Solution {
//public:
//    int minInsertions(string s) {
//        int n = s.size();
//        vector<vector<int>> dp(n, vector<int>(n));
//        for (int i = n - 1; i >= 0; --i)
//        {
//            dp[i][i] = 0;
//            for (int j = i + 1; j < n; ++j)
//            {
//                if (s[i] == s[j]) dp[i][j] = dp[i + 1][j - 1];
//                else dp[i][j] = min(dp[i + 1][j], dp[i][j - 1]) + 1;
//            }
//        }
//        return dp[0][n - 1];
//    }
//};


//class Solution {
//public:
//    int longestCommonSubsequence(string s1, string s2) {
//        int m = s1.size(), n = s2.size();
//        vector<vector<int>> dp(m + 1, vector<int>(n + 1));
//        for (int i = 1; i <= m; ++i)
//        {
//            for (int j = 1; j <= n; ++j)
//            {
//                if (s1[i - 1] == s2[j - 1]) dp[i][j] = dp[i - 1][j - 1] + 1;
//                else dp[i][j] = max(dp[i][j - 1], dp[i - 1][j]);
//            }
//        }
//        return dp[m][n];
//    }
//};

//
//class Solution {
//public:
//    int longestCommonSubsequence(string s1, string s2) {
//        int m = s1.size(), n = s2.size();
//        vector<vector<int>> dp(m + 1, vector<int>(n + 1));
//
//        s1 = " " + s1, s2 = " " + s2; //这样可以不用考虑下标的映射关系
//        for (int i = 1; i <= m; ++i)
//        {
//            for (int j = 1; j <= n; ++j)
//            {
//                if (s1[i] == s2[j]) dp[i][j] = dp[i - 1][j - 1] + 1;
//                else dp[i][j] = max(dp[i][j - 1], dp[i - 1][j]);
//            }
//        }
//        return dp[m][n];
//    }
//};


//class Solution {
//public:
//    int maxUncrossedLines(vector<int>& nums1, vector<int>& nums2) {
//        int m = nums1.size(), n = nums2.size();
//        vector<vector<int>> dp(m + 1, vector<int>(n + 1));
//        for (int i = 1; i <= m; ++i)
//        {
//            for (int j = 1; j <= n; ++j)
//            {
//                if (nums1[i - 1] == nums2[j - 1]) dp[i][j] = dp[i - 1][j - 1] + 1;
//                else dp[i][j] = max(dp[i - 1][j], dp[i][j - 1]);
//            }
//        }
//        return dp[m][n];
//    }
//};



//class Solution {
//public:
//    int wiggleMaxLength(vector<int>& nums) {
//        int count = 0;
//        int left = 0;
//        for (int i = 0; i < nums.size() - 1; i++)
//        {
//            int right = nums[i + 1] - nums[i];
//            if (right == 0) continue;
//            //如果左右两边互为相反数，就收集结果
//            if (left * right <= 0)
//            {
//                ++count;
//                left = right;
//            }
//        }
//        //把最后一个位置算
//        ++count;
//        return count;
//    }
//};


//class Solution {
//public:
//    int numDistinct(string s, string t) {
//        int m = t.size(), n = s.size();
//        vector<vector<double>> dp(m + 1, vector<double>(n + 1));
//        //初始化
//        for (int j = 0; j <= n; ++j) dp[0][j] = 1;
//        for (int i = 1; i <= m; ++i)
//        {
//            for (int j = 1; j <= n; ++j)
//            {
//                //一定存在不相等的点
//                dp[i][j] = dp[i][j - 1];
//                //最后两个相等
//                if (t[i - 1] == s[j - 1]) dp[i][j] += dp[i - 1][j - 1];
//            }
//        }
//        return dp[m][n];
//    }
//};


//class Solution {
//public:
//    bool isMatch(string s, string p) {
//        int m = s.size(), n = p.size();
//        vector<vector<bool>> dp(m + 1, vector<bool>(n + 1, false));
//        s = " " + s;
//        p = " " + p;
//        //初始化
//        dp[0][0] = true;
//        //初始化第一行，如果有通配符才能匹配，否则不行
//        for (int j = 1; j <= n; ++j)
//        {
//            if (p[j] == '*') dp[0][j] = true;
//            else break;
//        }
//        for (int i = 1; i <= m; ++i)
//        {
//            for (int j = 1; j <= n; ++j)
//            {
//                if (p[j] == '*') dp[i][j] = dp[i - 1][j] || dp[i][j - 1];
//                else dp[i][j] = (s[i] == p[j] || p[j] == '?') && dp[i - 1][j - 1];
//            }
//        }
//        return dp[m][n];
//    }
//};


