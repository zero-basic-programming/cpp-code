#include <iostream>
#include <vector>
using namespace std;

//class Solution {
//public:
//    int calculate(string s) {
//        int i = 0; int n = s.size();
//        char op = '+';
//        vector<int> st; //使用数组来模拟栈
//        while (i < n)
//        {
//            if (s[i] == ' ') ++i; //处理空格字符
//            else if ('0' <= s[i] && s[i] <= '9')
//            {
//                //数字字符，根据上一次的op来判断出这一次的操作
//                //先把这个数字取出来
//                int tmp = 0;
//                while (i < n && '0' <= s[i] && s[i] <= '9')
//                {
//                    tmp = tmp * 10 + (s[i++] - '0');
//                }
//                if (op == '+') st.push_back(tmp);
//                else if (op == '-') st.push_back(-tmp);
//                else if (op == '*') st.back() *= tmp;
//                else st.back() /= tmp;
//            }
//            else {
//                //操作符
//                op = s[i++];
//            }
//        }
//        //把剩下的字符全部加起来即可
//        int ret = 0;
//        for (auto e : st)
//        {
//            ret += e;
//        }
//        return ret;
//    }
//};


//class Solution {
//public:
//    string decodeString(string s) {
//        stack<int> num; //数字栈
//        stack<string> st; //结果栈
//        st.push(""); //方便后面处理
//        int i = 0; int n = s.size();
//        while (i < n)
//        {
//            if (s[i] >= '0' && s[i] <= '9')
//            {
//                //处理数字
//                int tmp = 0;
//                while (s[i] >= '0' && s[i] <= '9')
//                {
//                    tmp = tmp * 10 + (s[i++] - '0');
//                }
//                std::cout << tmp << std::endl;
//                num.push(tmp);
//            }
//            else if (s[i] == '[')
//            {
//                ++i; // 跳过这个字符
//                //把后面的字符串放入栈中
//                string tmp;
//                while (s[i] >= 'a' && s[i] <= 'z')
//                {
//                    tmp += s[i++];
//                }
//                st.push(tmp);
//            }
//            else if (s[i] == ']')
//            {
//                //把两个栈元素取出整合  
//                int k = num.top();
//                num.pop();
//                string tmp = st.top();
//                st.pop();
//                while (k--)
//                {
//                    st.top() += tmp;
//                }
//                ++i; //跳过这个字符    
//            }
//            else {
//                //处理之后字符串的情况
//                string tmp;
//                while (i < n && s[i] >= 'a' && s[i] <= 'z')
//                {
//                    tmp += s[i++];
//                }
//                st.top() += tmp;
//            }
//        }
//        return st.top();
//    }
//};



//class Solution {
//public:
//    vector<vector<int>> levelOrder(Node* root) {
//        vector<vector<int>> result;
//        if (root == nullptr) return result;
//        queue<Node*> q;
//        vector<int> tmp; //每一层的结果
//        q.push(root);
//        while (!q.empty())
//        {
//            //记录大小，并取出，然后放入子孩子
//            int sz = q.size();
//            for (int i = 0; i < sz; ++i)
//            {
//                Node* front = q.front();
//                tmp.push_back(front->val);
//                q.pop();
//                //记录结果，并放入子孩子
//                for (auto e : front->children)
//                {
//                    if (e) q.push(e);
//                }
//            }
//            result.push_back(tmp);
//            tmp.clear();
//        }
//        return result;
//    }
//};


//class Solution {
//public:
//    vector<vector<int>> zigzagLevelOrder(TreeNode* root) {
//        vector<vector<int>> result;
//        queue<TreeNode*> q;
//        if (root == nullptr) return result;
//        q.push(root);
//        int level = 0; //记录层数，从而判断是否需要逆序
//        while (!q.empty())
//        {
//            int sz = q.size();
//            vector<int> tmp; //用来逆序的数组   
//            for (int i = 0; i < sz; ++i)
//            {
//                TreeNode* node = q.front();
//                q.pop();
//                tmp.push_back(node->val);
//                if (node->left) q.push(node->left);
//                if (node->right) q.push(node->right);
//            }
//            if (level % 2 == 1) reverse(tmp.begin(), tmp.end());
//            result.push_back(tmp);
//            ++level;
//        }
//        return result;
//    }
//};


