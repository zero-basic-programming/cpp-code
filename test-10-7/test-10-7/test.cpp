#include <iostream>
#include <vector>
using namespace std;

//class Solution {
//    class cmp
//    {
//    public:
//        bool operator()(const ListNode* l1, const ListNode* l2)
//        {
//            return l1->val > l2->val;
//        }
//    };
//public:
//    ListNode* mergeKLists(vector<ListNode*>& lists) {
//        //创建堆来完成排序
//        priority_queue<ListNode*, vector<ListNode*>, cmp> heap;
//        //把第一个元素放入到堆中 
//        for (auto l : lists)
//        {
//            if (l)  heap.push(l);
//        }
//        //每次取出最小的值，然后放到新的链表中
//        ListNode* guard = new ListNode(0);
//        ListNode* tail = guard;
//        while (!heap.empty())
//        {
//            ListNode* t = heap.top();
//            heap.pop();
//            tail->next = t;
//            tail = t;
//            //放入下个元素
//            if (t->next)  heap.push(t->next);
//        }
//        //处理后序工作
//        tail->next = nullptr;
//        ListNode* head = guard->next;
//        delete guard;
//        return head;
//    }
//};

//struct ListNode{
//     int val;
//     ListNode* next;
//     ListNode() : val(0), next(nullptr) {}
//     ListNode(int x) : val(x), next(nullptr) {}
//     ListNode(int x, ListNode* next) : val(x), next(next) {}
// };
//
//class Solution {
//public:
//    ListNode* reverseList(ListNode* cur, int k)
//    {
//        ListNode* n1 = cur;
//        ListNode* n2 = cur->next;
//        ListNode* n3 = nullptr;
//        while (--k)
//        {
//            n3 = n2->next;
//            n2->next = n1;
//            //迭代
//            n1 = n2;
//            n2 = n3;
//        }
//        return n1;
//    }
//    ListNode* reverseKGroup(ListNode* head, int k) {
//        if (k == 1) return head;
//        //每次遍历k个单位
//        if (head == nullptr) return head;
//        ListNode* tmp = head;
//        int n = k;
//        while (n--)
//        {
//            if (tmp == nullptr) return head;
//            tmp = tmp->next;
//        }
//        //找到下一次的头,然后进行当次的反转链表
//        ListNode* newhead = reverseList(head, k);
//        head->next = reverseKGroup(tmp, k);
//        return newhead;
//    }
//};
//
//
//
//int main()
//{
//    ListNode* n1 = new ListNode(1);
//    ListNode* n2 = new ListNode(2);
//    ListNode* n3 = new ListNode(3);
//    ListNode* n4 = new ListNode(4);
//    ListNode* n5 = new ListNode(5);
//    n1->next = n2;
//    n2->next = n3;
//    n3->next = n4;
//    n4->next = n5;
//    n5->next = nullptr;
//    ListNode* head = Solution().reverseKGroup(n1, 2);
//    while (head)
//    {
//        cout << head->val << " ";
//        head = head->next;
//    }
//    return 0;
//}

