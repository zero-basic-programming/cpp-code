#include <iostream>
#include <vector>
using namespace std;


//class Solution {
//    vector<vector<bool>> vis;
//    int m, n;  //记录行列
//    int dx[8] = { -1,-1,-1,0,0,1,1,1 };
//    int dy[8] = { -1,0,1,-1,1,-1,0,1 };
//    int count = 0;
//public:
//    void dfs(vector<vector<int>>& map, int i, int j)
//    {
//        vis[i][j] = true;
//        for (int k = 0; k < 8; ++k)
//        {
//            int x = i + dx[k], y = j + dy[k];
//            //判断合法性
//            if (x >= 0 && x < m && y >= 0 && y < n && map[x][y] == 1 && !vis[x][y])
//            {
//                dfs(map, x, y);
//            }
//        }
//    }
//    int GetMinClickCount(vector<vector<int>>& map) {
//        m = map.size(), n = map[0].size();
//        vis.resize(m, vector<bool>(n, false));
//        //从中间的格子开始
//        for (int i = 1; i < m - 1; ++i)
//        {
//            for (int j = 1; j < n - 1; ++j)
//            {
//                if (map[i][j] == 1 && !vis[i][j])
//                {
//                    dfs(map, i, j);
//                    ++count;   //因为这里是连锁式爆炸
//                }
//            }
//        }
//        //把周围有1但是vis中没有记录到的元素
//        for (int j = 0; j < n; ++j)
//        {
//            if (map[0][j] == 1 && !vis[0][j]) ++count;
//            if (map[m - 1][j] == 1 && !vis[m - 1][j]) ++count;
//        }
//        for (int i = 1; i < m - 1; ++i)
//        {
//            if (map[i][0] == 1 && !vis[i][0]) ++count;
//            if (map[i][n - 1] == 1 && !vis[i][n - 1]) ++count;
//        }
//        return count;
//    }
//};


//class Solution {
//    int ret = 0;
//public:
//    bool IsTrue(int hash[7])
//    {
//        for (int i = 0; i < 7; ++i)
//        {
//            if (hash[i] % 2 != 0) return false;
//        }
//        return true;
//    }
//    int calculate_count(string str) {
//        int n = str.size();
//        for (int i = 0; i < n - 1; ++i)
//        {
//            int hash[7] = { 0 };
//            hash[str[i] - 'a']++;
//            for (int j = i + 1; j < n; ++j)
//            {
//                hash[str[j] - 'a']++;
//                //每次都判断一下
//                if ((j - i + 1) % 2 == 0 && IsTrue(hash))
//                {
//                    ret = max(ret, j - i + 1);
//                }
//            }
//        }
//        return ret;
//    }
//};
//
//
//int main()
//{
//    Solution().calculate_count("abaa");
//    return 0;
//}


//class Solution {
//public:
//    int Calc(int nValue) {
//        int ret = 0;  //收集结果
//        vector<vector<int>> dp(nValue + 1, vector<int>(nValue + 1));
//        for (int k = 2; k < nValue; ++k) dp[1][k] = 1 + k;
//        for (int i = 2; i < nValue - 1; ++i)
//        {
//            for (int j = i + 1; j < nValue; ++j)
//            {
//                int k = i * i - j;  //前面需要找的元素，必须是等差
//                if (k > 0 && k < i)
//                {
//                    dp[i][j] = dp[k][i] + j;
//                    if (dp[i][j] == nValue) ++ret;
//                }
//            }
//        }
//        return ret;
//    }
//};
//
//
//int main()
//{
//    cout << Solution().Calc(6);
//    return 0;
//}



//class Solution {
//    int m, n;  //记录行列
//    int dx[8] = { -1,-1,-1,0,0,1,1,1 };
//    int dy[8] = { -1,0,1,-1,1,-1,0,1 };
//    int count = 0;
//public:
//    void dfs(vector<vector<int>>& map, int i, int j)
//    {
//        map[i][j] = 0;
//        for (int k = 0; k < 8; ++k)
//        {
//            int x = i + dx[k], y = j + dy[k];
//            //判断合法性
//            if (x >= 0 && x < m && y >= 0 && y < n && map[x][y] == 1)
//            {
//                dfs(map, x, y);
//            }
//        }
//    }
//    int GetMinClickCount(vector<vector<int>>& map) {
//        m = map.size(), n = map[0].size();
//        //从中间的格子开始
//        for (int i = 0; i < m; ++i)
//        {
//            for (int j = 0; j < n; ++j)
//            {
//                if (map[i][j] == 1)
//                {
//                    dfs(map, i, j);
//                    ++count;   //因为这里是连锁式爆炸
//                }
//            }
//        }
//        return count;
//    }
//};


