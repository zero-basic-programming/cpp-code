#include <iostream>
#include <vector>
using namespace std;


class Solution {
    int dx[8] = { -1,-1,-1,0,0,1,1,1 };
    int dy[8] = { -1,0,1,-1,1,-1,0,1 };
    int m, n;
public:
    vector<vector<char>> updateBoard(vector<vector<char>>& board, vector<int>& click) {
        //特殊处理一下
        m = board.size(), n = board[0].size();
        int i = click[0], j = click[1];
        if (board[i][j] == 'M')
        {
            board[i][j] = 'X';
            return board;
        }
        dfs(board, i, j);
        return board;
    }
    void dfs(vector<vector<char>>& board, int i, int j)
    {
        int count = 0; //这个是计算当前的格子周围雷的个数
        for (int k = 0; k < 8; ++k)
        {
            int x = i + dx[k], y = j + dy[k];
            if (x >= 0 && x < m && y >= 0 && y < n && board[x][y] == 'M')
            {
                ++count;
                //这里不能递归到下一层去处理，因为当前格子是没有填完的，如果递归就会死递归了
            }
        }
        //如果count为0就需要为B，数字的话就填写
        if (count > 0)
        {
            board[i][j] = count + '0';
            return;
        }
        else
        {
            board[i][j] = 'B';
            //递归处理周围的格子
            for (int k = 0; k < 8; ++k)
            {
                int x = i + dx[k], y = j + dy[k];
                if (x >= 0 && x < m && y >= 0 && y < n && board[x][y] == 'E')
                {
                    dfs(board, x, y);
                }
            }
        }
    }
};


class Solution {
public:
    int digit(int n)
    {
        int ret = 0;
        while (n)
        {
            ret += n % 10;
            n /= 10;
        }
        return ret;
    }
    int wardrobeFinishing(int m, int n, int cnt) {
        int count = 0;
        for (int i = 0; i < m; ++i)
        {
            for (int j = 0; j < n; ++j)
            {
                if (digit(i) + digit(j) > cnt) break;
                ++count;
            }
        }
        return count;
    }
};


class Solution {
public:
    int digit(int n)
    {
        int ret = 0;
        while (n)
        {
            ret += n % 10;
            n /= 10;
        }
        return ret;
    }
    int wardrobeFinishing(int m, int n, int cnt) {
        vector<vector<int> > vis(m, vector<int>(n, 0));
        int ans = 1;
        vis[0][0] = 1;
        for (int i = 0; i < m; ++i)
        {
            for (int j = 0; j < n; ++j)
            {
                if ((i == 0 && j == 0) || digit(i) + digit(j) > cnt) continue;
                // 边界判断
                if (i - 1 >= 0) vis[i][j] |= vis[i - 1][j];
                if (j - 1 >= 0) vis[i][j] |= vis[i][j - 1];
                ans += vis[i][j];
            }
        }
        return ans;
    }
};


class Solution {
public:
    int digit(int n)
    {
        int ret = 0;
        while (n)
        {
            ret += n % 10;
            n /= 10;
        }
        return ret;
    }
    int ret, m, n;
    int dx[2] = { 0,1 };
    int dy[2] = { 1,0 };
    int wardrobeFinishing(int _m, int _n, int cnt) {
        m = _m, n = _n;
        if (!cnt) return 1;
        vector<vector<bool>> vis(m, vector<bool>(n, false));
        dfs(vis, 0, 0, cnt);
        return ret;
    }
    void dfs(vector<vector<bool>>& vis, int i, int j, int cnt)
    {
        ++ret;
        vis[i][j] = true;
        for (int k = 0; k < 2; ++k)
        {
            int x = i + dx[k], y = j + dy[k];
            if (x >= 0 && x < m && y >= 0 && y < n && !vis[x][y] && digit(x) + digit(y) <= cnt)
            {
                dfs(vis, x, y, cnt);
            }
        }
    }
};