#pragma once
#include <iostream>
using namespace std;

//节点类
template <class K>
struct BSTreeNode
{
	BSTreeNode<K>* _left;
	BSTreeNode<K>* _right;
	K _key;

	BSTreeNode(const K& key)
		:_key(key)
		,_left(nullptr)
		,_right(nullptr)
	{}
};
//二叉搜索树
template <class K>
class BSTree
{
	typedef BSTreeNode<K> Node;
public:
	BSTree()
		:_root(nullptr)
	{}
	//为了不破坏封装性，采用子函数的形式不暴露根
	BSTree(const BSTree<K>& t)
	{
		_root = CopyTree(t._root);
	}
	BSTree<K>& operator=(BSTree<K> t)
	{
		swap(_root,t._root);
	    return *this;
	}
	~BSTree()
	{
		Destroy(_root);
		_root = nullptr;
	}
	//插入
	bool Insert(const K& key)
	{
		//开始插入第一个的情况
		if (_root == nullptr)
		{
			_root = new Node(key);
			return true;
		}
		Node* cur = _root;
		Node* parent = nullptr;
		while (cur)
		{
			if (cur->_key < key)
			{
				parent = cur;
				cur = cur->_right;
			}
			else if (cur->_key > key)
			{
				parent = cur;
				cur = cur->_left;
			}
			else
			{
				//不允许插入相同的值
				return false;
			}
		}
		cur = new Node(key);
		//判断链接的左右
		if (parent->_key < key)
		{
			parent->_right = cur;
		}
		else
		{
			parent->_left = cur;
		}
		return true;
	}
	//查找
	bool Find(const K& key)
	{
		if (_root == nullptr)
			return false;
		Node* cur = _root;
		while (cur)
		{
			if (cur->_key < key)
			{
				cur = cur->_right;
			}
			else if (cur->_key > key)
			{
				cur = cur->_left;
			}
			else
			{
				return true;
			}
		}
		return false;
	}
	//删除
	bool Erase(const K& key)
	{
		Node* cur = _root;
		Node* parent = nullptr;
		while (cur)
		{
			if (cur->_key < key)
			{
				parent = cur;
				cur = cur->_right;
			}
			else if (cur->_key > key)
			{
				parent = cur;
				cur = cur->_left;
			}
			else
			{
				//找到了
				//1.可能是左为空
				//2.右为空
				//两边都不为空
				if(cur->_left == nullptr)
				{
					//有可能删除根节点
					if (cur == _root)
					{
						_root = _root->_right;
					}
					else
					{
						if (parent->_left == cur)
						{
							parent->_left = cur->_right;
						}
						else
						{
							parent->_right = cur->_right;
						}
					}
					delete cur;
				
				}
				else if (cur->_right == nullptr)
				{
					if (cur == _root)
					{
						_root = _root->_right;
					}
					else
					{
						if (parent->_left == cur)
						{
							parent->_left = cur->_left;
						}
						else
						{
							parent->_right = cur->_left;
						}
					}
					delete cur;
				}
				else
				{
					//都不为空
					//从当前节点的右子树开始找最小的值
					Node* minright = cur->_right;
					Node* parent = cur;
					while (minright->_left)
					{
						parent = minright;
						minright = minright->_left;
					}
					cur->_key = minright->_key;

					//将最小的值的节点剩下的节点链接给parent
					if (parent->_left == minright)
					{
						parent->_left = minright->_right;
					}
					else
					{
						parent->_right = minright->_right;
					}
					delete minright;
				}
				return true;
			}
		}
		return false;
	}

	//打印,为了保证其封装性，可以使用子函数,采用中序遍历
	void Print()
	{
		PrintHelper(_root);
	}

	//递归版本的插入
	bool InsertR(const K& key)
	{
		return _InsertR(_root, key);
	}
	//递归版本的查找
	bool FindR(const K& key)
	{
		return _FindR(_root, key);
	}
	//递归版本的删除
	bool EraseR(const K& key)
	{
		return _EraseR(_root, key);
	}
private:
	void Destroy(Node* root)
	{
		if (root == nullptr)
		{
			return;
		}
		Destroy(root->_left);
		Destroy(root->_right);
		delete root;
	}
	Node* CopyTree(Node* root)
	{
		//前序建树即可
		if (root == nullptr)
		{
			return true;
		}
		Node* newRoot = new Node(root->_key);
		newRoot->_left = CopyTree(root->_left);
		newRoot->_right = CopyTree(root->_right);
		return newRoot;
	}

	bool _EraseR(Node*& root, const K& key)
	{
		if (root == nullptr)
		{
			return false;
		}
		if (root->_key < key)
		{
			return _EraseR(root->_right, key);
		}
		else if (root->_key > key)
		{
			return _EraseR(root->_left, key);
		}
		else
		{
			//找到，删除
			Node* del = root;
			//还是分3种情况
			if (root->_left == nullptr)
			{
				root = root->_right;
			}
			else if (root->_right == nullptr)
			{
				root = root->_left;
			}
			else
			{
				//在当前节点的右子树找到最小值，然后交换
				Node* minright = root->_right;
				while (minright->_left)
				{
					minright = minright->_left;
				}
				//交换
				swap(minright->_key, root->_key);
				//在右子树中找到要删除的值
				return _EraseR(root->_right, key);
			}
			delete del;
			return true;
		}
	}

	bool _FindR(Node* root, const K& key)
	{
		if (root == nullptr)
		{
			return false;
		}
		if (root->_key > key)
		{
			return _FindR(root->_left, key);
		}
		else if (root->_key < key)
		{
			return _FindR(root->_right, key);
		}
		else
		{
			return true;
		}
	}

	bool _InsertR(Node*& root,const K& key)
	{
		if (root == nullptr)
		{
			//插入,因为这里是引用，所以直接赋值即可
			root = new Node(key);
			return true;
		}
		if (root->_key < key)
		{
			return  _InsertR(root->_right, key);
		}
		else if (root->_key > key)
		{
			return  _InsertR(root->_left, key);
		}
		else
		{
			//相同
			return false;
		}
	}
	void PrintHelper(const Node* _root)
	{
		//中序遍历
		if (_root == nullptr)
			return;
		PrintHelper(_root->_left);
		cout << _root->_key << " ";
		PrintHelper(_root->_right);
	}
	Node* _root = nullptr;
};

void BSTreeTest1()
{
	int a[] = { 8, 3, 1, 10, 6, 4, 7, 14, 13 };
	BSTree<int> b;
	for (auto& e : a)
	{
		b.InsertR(e);
	}
	//bool flag = b.FindR(3);
	b.Print();
	/*b.EraseR(3);
	cout << endl;
	b.Print();*/

	for (auto e : a)
	{
		b.EraseR(e);
	}
	cout << endl;
	b.Print();
}

//改造二叉搜索树变为KV模型
namespace KV
{
	template <class K, class V>
	struct BSTreeNode
	{
		BSTreeNode<K,V>* _left;
		BSTreeNode<K,V>* _right;
		K _key;
		V _val;

		BSTreeNode(const K& key, const V& val)
			:_key(key)
			, _val(val)
			, _left(nullptr)
			, _right(nullptr)
		{}
	};

	template <class K, class V>
	class BSTree
	{
		typedef BSTreeNode<K, V> Node;
	public:
		//插入
		bool Insert(const K& key, const V& val)
		{
			//开始插入第一个的情况
			if (_root == nullptr)
			{
				_root = new Node(key,val);
				return true;
			}
			Node* cur = _root;
			Node* parent = nullptr;
			while (cur)
			{
				if (cur->_key < key)
				{
					parent = cur;
					cur = cur->_right;
				}
				else if (cur->_key > key)
				{
					parent = cur;
					cur = cur->_left;
				}
				else
				{
					//不允许插入相同的值
					return false;
				}
			}
			cur = new Node(key,val);
			//判断链接的左右
			if (parent->_key < key)
			{
				parent->_right = cur;
			}
			else
			{
				parent->_left = cur;
			}
			return true;
		}
		//查找
		Node* Find(const K& key)
		{
			if (_root == nullptr)
				return nullptr;
			Node* cur = _root;
			while (cur)
			{
				if (cur->_key < key)
				{
					cur = cur->_right;
				}
				else if (cur->_key > key)
				{
					cur = cur->_left;
				}
				else
					return cur;
				{
				}
			}
			return nullptr;
		}
		void Print()
		{
			PrintHelper(_root);
		}
	private:
		void PrintHelper(const Node* _root)
		{
			//中序遍历
			if (_root == nullptr)
				return;
			PrintHelper(_root->_left);
			cout << _root->_key << " "<< _root->_val<<endl;
			PrintHelper(_root->_right);
		}
		Node* _root = nullptr;
	};
	void TestBSTree1()
	{
		BSTree<string, string> dict;
		dict.Insert("string", "字符串");
		dict.Insert("tree", "树");
		dict.Insert("left", "左边、剩余");
		dict.Insert("right", "右边");
		dict.Insert("sort", "排序");
		string str;
		while (cin >> str)
		{
			//在字典中查找
			BSTreeNode<string,string>* ret = dict.Find(str);
			if (ret)
			{
				cout << ret->_val << endl;
			}
			else
			{
				cout << "不存在" << endl;
			}
		}
	}

	void TestBSTree2()
	{
		// 统计水果出现的次数
		string arr[] = { "苹果", "西瓜", "苹果", "西瓜", "苹果", "苹果", "西瓜",
	   "苹果", "香蕉", "苹果", "香蕉" };
		BSTree<string, int> countTree;
		for (const auto& e : arr)
		{
			//将数据插入到二叉搜索树中
			auto ret = countTree.Find(e);
			if (ret == nullptr)
			{
				//树中没有该水果
				countTree.Insert(e, 1);
			}
			else
			{
				ret->_val++;
			}
		}
		countTree.Print();
	}
}

