#include <iostream>
using namespace std;

//int main() {
//    char ch;
//    while (cin >> ch) {
//        if ('a' <= ch && ch <= 'z') ch += 'A' - 'a';
//        else if ('A' <= ch && ch <= 'Z') ch += 'a' - 'A';
//        cout << ch << endl;
//    }
//}


//class Solution {
//public:
//    int search(vector<int>& nums, int target) {
//        int n = nums.size();
//        int left = 0, right = n - 1;
//        while (left <= right)
//        {
//            int mid = left + (right - left) / 2;
//            if (nums[mid] < target)
//            {
//                left = mid + 1;
//            }
//            else if (nums[mid] > target) right = mid - 1;
//            else return mid;
//        }
//        return -1;
//    }
//};


//class Solution {
//public:
//    string compressString(string param) {
//        int n = param.size();
//        string ret;
//        if (n == 0) return ret;
//        //记录当前字符以及出现的次数
//        char curCh = param[0];
//        int count = 1;
//        for (int i = 1; i < n; ++i)
//        {
//            if (param[i] == curCh) ++count;
//            else
//            {
//                //收集结果写入到结果集，同时更新当前字符和count
//                ret += curCh;
//                if (count > 1) ret += to_string(count);
//                curCh = param[i];
//                count = 1;
//            }
//        }
//        //最后这里还有尾部数据需要处理
//        ret += curCh;
//        if (count > 1) ret += to_string(count);
//        return ret;
//    }
//};



//int main() {
//    double walk = 1;   //统计走需要的时间
//    double car = 0;
//    double total = 0;  //总路程
//    cin >> total;
//    walk = total;
//    car = 10 + total / 10;
//    if (walk < car) cout << 'w' << endl;
//    else cout << 'v' << endl;
//    return 0;
//}


//class Solution {
//    typedef enum Color
//    {
//        UNDEF,
//        RED,
//        BLACK
//    }color;
//public:
//    bool isBipartite(vector<vector<int>>& graph) {
//        int n = graph.size();
//        vector<int> nums(n, UNDEF);
//        //从每一个顶点出发，因为这里不是连通的
//        for (int i = 0; i < n; ++i)
//        {
//            queue<int> q;
//            if (nums[i] == UNDEF)
//            {
//                q.push(i);
//                nums[i] = RED;
//            }
//            //宽度优先遍历
//            while (q.size())
//            {
//                int sz = q.size();
//                for (int j = 0; j < sz; ++j)
//                {
//                    int curIdx = q.front();
//                    q.pop();
//                    int col = nums[curIdx] == RED ? BLACK : RED;   //确定下一个节点的颜色
//                    //遍历当前下标的所有子节点
//                    //1.如果未定义就染成col并添加到q中
//                    //2.如果定义了颜色和col不同就是错误的
//                    for (int idx : graph[curIdx])
//                    {
//                        if (nums[idx] == UNDEF)
//                        {
//                            nums[idx] = col;
//                            q.push(idx);
//                        }
//                        else if (nums[idx] != col) return false;
//                    }
//                }
//            }
//        }
//        return true;
//    }
//};


//class Solution {
//    int m, n;
//    int dx[4] = { 0,0,1,-1 };
//    int dy[4] = { 1,-1,0,0 };
//public:
//    vector<vector<int>> updateMatrix(vector<vector<int>>& mat) {
//        //如果当前位置是0，直接填0，如果是1就宽度优先遍历找到0为止
//        m = mat.size(), n = mat[0].size();
//        vector<vector<int>> ret(m, vector<int>(n));
//        for (int i = 0; i < m; ++i)
//        {
//            for (int j = 0; j < n; ++j)
//            {
//                if (mat[i][j] == 0) ret[i][j] = 0;
//                else
//                {
//                    int num = 0;   //统计最近拿到的0的步数
//                    bfs(mat, i, j, num);
//                    ret[i][j] = num;
//                }
//            }
//        }
//        return ret;
//    }
//    void bfs(vector<vector<int>>& mat, int i, int j, int& count)
//    {
//        queue<pair<int, int>> q;
//        q.push(make_pair(i, j));
//        while (q.size())
//        {
//            int sz = q.size();
//            ++count;
//            for (int i = 0; i < sz; ++i)
//            {
//                auto p = q.front();
//                q.pop();
//                int a = p.first, b = p.second;
//                for (int k = 0; k < 4; ++k)
//                {
//                    int x = a + dx[k], y = b + dy[k];
//                    if (x >= 0 && x < m && y >= 0 && y < n)
//                    {
//                        if (mat[x][y] == 0) return;
//                        q.push(make_pair(x, y));
//                    }
//                }
//            }
//        }
//    }
//};


//class Solution {
//    int m, n;
//    int dx[4] = { 0,0,1,-1 };
//    int dy[4] = { 1,-1,0,0 };
//public:
//    vector<vector<int>> updateMatrix(vector<vector<int>>& mat) {
//        //如果当前位置是0，直接填0，如果是1就宽度优先遍历找到0为止
//        m = mat.size(), n = mat[0].size();
//        vector<vector<int>> ret(m, vector<int>(n));
//        vector<vector<bool>> vis(m, vector<bool>(n, false));
//        queue<pair<int, int>> q;
//        for (int i = 0; i < m; ++i)
//        {
//            for (int j = 0; j < n; ++j)
//            {
//                if (mat[i][j] == 0)
//                {
//                    //放入队列中
//                    q.push(make_pair(i, j));
//                    vis[i][j] = true;
//                }
//            }
//        }
//        //遍历队列中的0，然后向外扩展
//        int num = 1;   //下一个位置开始+1
//        while (q.size())
//        {
//            int sz = q.size();
//            for (int i = 0; i < sz; ++i)
//            {
//                auto [a, b] = q.front();
//                q.pop();
//                for (int k = 0; k < 4; ++k)
//                {
//                    int x = a + dx[k], y = b + dy[k];
//                    if (x >= 0 && x < m && y >= 0 && y < n && !vis[x][y])
//                    {
//                        vis[x][y] = true;
//                        ret[x][y] = num;
//                        q.push(make_pair(x, y));
//                    }
//                }
//            }
//            ++num;
//        }
//        return ret;
//    }
//};


