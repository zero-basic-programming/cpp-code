#include <iostream>
#include <vector>
using namespace std;

//class Solution {
//public:
//    int wiggleMaxLength(vector<int>& nums) {
//        int n = nums.size();
//        //f表示差值为正，g表示差值为负
//        vector<int> f(n, 1), g(n, 1);
//        int ret = 1;
//        for (int i = 1; i < n; ++i)
//        {
//            for (int j = 0; j < i; ++j)
//            {
//                if (nums[i] > nums[j])
//                {
//                    f[i] = max(g[j] + 1, f[i]);
//                }
//                else if (nums[i] < nums[j])
//                {
//                    g[i] = max(f[j] + 1, g[i]);
//                }
//                ret = max(ret, max(f[i], g[i]));
//            }
//        }
//        return ret;
//    }
//};


//class Solution {
//public:
//    int singleNumber(vector<int>& nums) {
//        int ret = 0;
//        for (int i = 0; i < 32; i++)
//        {
//            int sum = 0;
//            for (auto x : nums) //统计该比特位下的所以比特位之和
//            {
//                if ((x >> i) & 1 == 1) ++sum;
//            }
//            sum %= 3;
//            if (sum == 1) ret |= (1 << i);
//        }
//        return ret;
//    }
//};


//class Solution {
//public:
//    vector<int> countBits(int n) {
//        vector<int> ret(n + 1);
//        for (int i = 0; i <= n; ++i)
//        {
//            int j = i;
//            int count = 0;
//            int index = i; //用来记录下标，防止在改变之后出现问题
//            while (j)
//            {
//                j &= (j - 1);
//                ++count;
//            }
//            ret[i] = count;
//        }
//        return ret;
//    }
//};

