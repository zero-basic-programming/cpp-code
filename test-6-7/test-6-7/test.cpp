#include <iostream>
#include <vector>

//class Solution {
//private:
//    vector<vector<int>> result;
//    vector<int> path;
//    void backtracking(vector<int>& nums, int startindex)
//    {
//        这里每次都要收集结果，因为这里相当于收集树的每个节点
//        result.push_back(path);
//        终止条件，这里可以不写，因为循环结束自己会跳出递归
//        if (startindex >= nums.size())
//        {
//            return;
//        }
//        for (int i = startindex; i < nums.size(); ++i)
//        {
//            path.push_back(nums[i]);
//            因为是组合问题，所以这里i+1才对
//            backtracking(nums, i + 1);
//            path.pop_back(); //回溯
//        }
//        return;
//    }
//public:
//    vector<vector<int>> subsets(vector<int>& nums) {
//        backtracking(nums, 0);
//        return result;
//    }
//};


//class Solution {
//private:
//    vector<vector<int>> result;
//    vector<int> path;
//    void backtracking(vector<int>& nums, int startindex, vector<bool>& used)
//    {
//        result.push_back(path);
//        for (int i = startindex; i < nums.size(); ++i)
//        {
//            //这里要判断去重
//            if (i > 0 && nums[i - 1] == nums[i] && used[i - 1] == false)
//            {
//                continue; // 如果这两个相等就需要去重
//            }
//            used[i] = true;
//            path.push_back(nums[i]);
//            backtracking(nums, i + 1, used);
//            path.pop_back();
//            used[i] = false; //保证同一层不可以重复，但是不同层可以重复
//        }
//        return;
//    }
//public:
//    vector<vector<int>> subsetsWithDup(vector<int>& nums) {
//        vector<bool> used(nums.size(), false);//用来去重
//        sort(nums.begin(), nums.end()); //需要排序去重
//        backtracking(nums, 0, used);
//        return result;
//    }
//};

