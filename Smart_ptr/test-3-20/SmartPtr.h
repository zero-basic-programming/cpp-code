#pragma once
#include <thread>
#include <mutex>
namespace liang
{
	//auto_ptr不推荐使用
	template <class T>
	class auto_ptr
	{
	public:
		auto_ptr(T* ptr)
			:_ptr(ptr)
		{}
		auto_ptr(auto_ptr<T>& pt)
			:_ptr(pt._ptr)
		{
			//这里的拷贝将其置为空，然后用新的指针替代它
			pt._ptr = nullptr;//
		}
		~auto_ptr()
		{
			if (_ptr)
			{
				delete _ptr;
				_ptr = nullptr;
			}
		}
		//使用赋值运算符重载，使其行为像指针一样
		auto_ptr<T> operator=(const auto_ptr<T>& pt)
		{
			//不能自己给自己赋值
			if (_ptr != pt._ptr)
			{
				//先释放原来的资源，然后再转给新的对象
				if (_ptr)
				{
					delete _ptr;
				}
				_ptr = pt._ptr;
				pt._ptr = nullptr;
			}
			return *this;
		}
		T& operator*()
		{
			return *_ptr;
		}
		T* operator->()
		{
			return &_ptr;
		}
	private:
		T* _ptr;
	};

	template <class T>
	class unique_ptr
	{
	public:
		unique_ptr(T* ptr)
			:_ptr(ptr)
		{}
		unique_ptr(const unique_ptr<T>& pt) = delete; // 不允许拷贝
		unique_ptr<T> operator=(const unique_ptr<T>& pt) = delete; // 不允许赋值
		~unique_ptr()
		{
			if (_ptr)
			{
				delete _ptr;
				_ptr = nullptr;
			}
		}
		//使用赋值运算符重载，使其行为像指针一样
		T& operator*()
		{
			return *_ptr;
		}
		T* operator->()
		{
			return &_ptr;
		}
	private:
		T* _ptr;
	};

	//默认释放方式
	template <class T>
	class default_delete
	{
	public:
		void operator()(T* t)
		{
			delete t;
		}
	};
	//使用引用计数保证资源的合理分配
	template <class T ,class D = default_delete<T>>
	class shared_ptr
	{
	public:
		shared_ptr(T* ptr = nullptr)
			:_ptr(ptr)
			,_count(new int(1))
			,_mtx(new mutex)
		{}
		shared_ptr(const shared_ptr<T>& pt)
			:_ptr(pt._ptr)
			,_count(pt._count)
			,_mtx(pt._mtx)
		{
			_mtx->lock();
			++(*_count);
			_mtx->unlock();
		}
		shared_ptr<T>& operator=(const shared_ptr<T>& pt)
		{
			//不能自己给自己赋值
			if (_ptr != pt._ptr)
			{
				Release();
				_ptr = pt._ptr;
				_count = pt._count;
				_mtx = pt._mtx;

				_mtx->lock();
				++(*_count);
				_mtx->unlock();
			}
			return *this;
		}
		~shared_ptr()
		{
			Release();
		}
		T* get_ptr() const 
		{
			return _ptr;
		}
		int get_count() const
		{
			return *_count;
		}
		//使用赋值运算符重载，使其行为像指针一样
		T& operator*()
		{
			return *_ptr;
		}
		T* operator->()
		{
			return _ptr;
		}
	private:
		void Release()
		{
			//因为这里只有最后一次才要把锁释放
			int flag = false;
			_mtx->lock();
			if (--(*_count) == 0)
			{
				delete _ptr;
				_ptr = nullptr;
				flag = true;
				_del(_ptr);
			}
			_mtx->unlock();
			if(flag)
				delete _mtx;
		}
		T* _ptr;
		int* _count;//引用计数
		mutex* _mtx;//锁的指针，因为对象是不允许拷贝构造的，只能通过靠拷贝指针来进行拷贝构造
		D _del;
	};
	//weak_ptr解决shared_ptr中循环引用的问题
	template <class T>
	class weak_ptr
	{
	public:
		weak_ptr()
			:_ptr(nullptr)
		{}
		weak_ptr(const shared_ptr<T>& pt)
			:_ptr(pt.get_ptr())
		{}
		weak_ptr<T>& operator=(const shared_ptr<T>& pt)
		{
			_ptr = pt.get_ptr();
			return *this;
		}
	
		//使用赋值运算符重载，使其行为像指针一样
		T& operator*()
		{
			return *_ptr;
		}
		T* operator->()
		{
			return _ptr;
		}
	private:
		T* _ptr;
	};
	struct Data
	{
		int _year =1;
		int _month = 1;
		int _day = 1 ;
	};
	//这里测试一下shared_ptr的线程安全
	void test_shared_ptr()
	{
		int n = 100000;
		//加个锁保证资源是线程安全的
		mutex mtx;
		shared_ptr<Data> sp(new Data);
		//多个线程在不断的拷贝构造
		//这里会出现线程安全问题
		//就算shard_ptr能保证count的安全，但是不能保证智能指针中的内容是安全的
		thread t1([&]() {
			for (int i = 0; i < n; ++i)
			{
				shared_ptr<Data> sp1(sp);
				mtx.lock();
				++sp->_day;
				++sp->_month;
				++sp->_year;
				mtx.unlock();
			}
			});
		thread t2([&]() {
			for (int i = 0; i < n; ++i)
			{
				shared_ptr<Data> sp1(sp);
				mtx.lock();
				++sp->_day;
				++sp->_month;
				++sp->_year;
				mtx.unlock();
			}
			});
		//线程等待
		t1.join();
		t2.join();

		cout << sp.get_count() << endl;
		cout << sp.get_ptr() << endl;
		cout << sp->_day << endl;
		cout << sp->_month << endl;
		cout << sp->_year << endl;
	}
	//测试shared_ptr会出现循环引用的问题
	struct ListNode
	{
		/*shared_ptr<ListNode> _next;
		shared_ptr<ListNode> _prev;*/
		weak_ptr<ListNode> _next;
		weak_ptr<ListNode> _prev;
		int val = 0;
		//写个析构方便观察
		~ListNode()
		{
			cout << "~ListNode()" << endl;
		}
	};
	//不使用智能指针是没有问题的
	/*void test_shared_ptr1()
	{
		ListNode* n1 = new ListNode;
		ListNode* n2 = new ListNode;
		n1->_next = n2;
		n2->_prev = n1;
		delete n1;
		delete n2;
	}*/
	void test_shared_ptr2()
	{
		shared_ptr<ListNode> n1 = new ListNode;
		shared_ptr<ListNode> n2 = new ListNode;
		n1->_next = n2;
		n2->_prev = n1;
	}
	template <class T>
	class delete_arr
	{
	public:
		void operator()(T* t)
		{
			delete[] t;
			//cout << "delete_arr" << endl;
		}
	};
	class delete_file
	{
	public:
		void operator()(FILE* t)
		{
			fclose(t);
			t = nullptr;
			//cout << "delete_arr" << endl;
		}
	};
	//为了解决可能new的时候不止开辟一份空间，为了保证释放，使用定制删除器
	void test_shared_ptr3()
	{
		//需要第二个模板参数，使用仿函数进行释放
		shared_ptr<int,delete_arr<int>> sp1 = new int[10];
		//使用文件
		shared_ptr<FILE, delete_file> sp2(fopen("test.cpp", "r"));

	}


}