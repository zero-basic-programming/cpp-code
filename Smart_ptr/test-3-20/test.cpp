#define _CRT_SECURE_NO_WARNINGS 1
#include <iostream>
using namespace std;
#include "SmartPtr.h"
//
//int main()
//{
//    int n = 0;
//    while (true)
//    {
//        cin >> n;
//        if (n == 0)
//        {
//            break;
//        }
//        int result = 0;//记录结果
//        int tmp = 0;//记录空瓶
//        while (n > 2)
//        {
//            result += n / 3;
//            tmp = n / 3;//喝完之后的空瓶
//            n %= 3;//没有换的空瓶
//            tmp += n;
//            n = tmp;
//        }
//        if (n == 2)
//        {
//            cout << result + 1 << endl;
//        }
//        else
//        {
//            cout << result << endl;
//        }
//    }
//    return 0;
//}

//int main()
//{
//	//liang::auto_ptr<int> p1(new int(0));
//	//liang::auto_ptr<int> p2(p1);
//	////这里没有办法对p1进行操作了，这就是auto_ptr的危害
//	////cout << *p1 << endl;
//	//cout << *p2 << endl;
//	//liang::unique_ptr<int> p3(new int(0));
//	//////不被允许
//	////liang::unique_ptr<int> p4(p3);
//	////p4 = p3;
//
//	liang::shared_ptr<int> p5(new int(9));
//	cout << *p5 << endl;
//	liang::shared_ptr<int> p6(p5);
//	cout << *p6 << endl;
//	liang::shared_ptr<int> p7(new int(4));
//	cout << *p7 << endl;
//	p7 = p5;
//	cout << *p7 << endl;
//	return 0;
//}


int main()
{
	liang::test_shared_ptr2();
	return 0;
}
