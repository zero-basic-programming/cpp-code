#include <iostream>
#include <vector>
using namespace std;


//class Solution {
//public:
//    int coinChange(vector<int>& coins, int amount) {
//        int n = coins.size();
//        const int INF = 0x3f3f3f3f;
//        vector<vector<int>> dp(n + 1, vector<int>(amount + 1));
//        //��ʼ��
//        for (int j = 1; j <= amount; ++j) dp[0][j] = INF;
//        for (int i = 1; i <= n; ++i)
//        {
//            for (int j = 1; j <= amount; ++j)
//            {
//                dp[i][j] = dp[i - 1][j];
//                if (j >= coins[i - 1])
//                    dp[i][j] = min(dp[i][j], dp[i][j - coins[i - 1]] + 1);
//            }
//        }
//        return dp[n][amount] >= INF ? -1 : dp[n][amount];
//    }
//};


//class Solution {
//public:
//    int combinationSum4(vector<int>& nums, int target) {
//        int n = nums.size();
//        vector<vector<int>> dp(n + 1, vector<int>(target + 1));
//        //��ʼ��
//        for (int i = 0; i <= n; ++i) dp[i][0] = 1;
//        for (int i = 1; i <= n; ++i)
//        {
//            for (int j = 1; j <= target; ++j)
//            {
//                dp[i][j] = dp[i - 1][j];
//                if (j >= nums[i - 1])
//                    dp[i][j] += dp[i][j - nums[i - 1]];
//            }
//        }
//        return dp[n][target];
//    }
//};


//class Solution {
//    int m, n;
//    vector<vector<bool>> vis;
//    int dx[4] = { 0,0,1,-1 };
//    int dy[4] = { 1,-1,0,0 };
//    int ret = 0;
//    int count = 0;
//public:
//    void dfs(vector<vector<int>>& grid, int i, int j, int& count)
//    {
//        vis[i][j] = true;
//        ++count;
//        for (int k = 0; k < 4; ++k)
//        {
//            int x = i + dx[k], y = j + dy[k];
//            if (x >= 0 && x < m && y >= 0 && y < n && !vis[x][y] && grid[x][y] == 1)
//            {
//                dfs(grid, x, y, count);
//            }
//        }
//    }
//    int maxAreaOfIsland(vector<vector<int>>& grid) {
//        m = grid.size(), n = grid[0].size();
//        vis.resize(m, vector<bool>(n, false));
//        for (int i = 0; i < m; ++i)
//        {
//            for (int j = 0; j < n; ++j)
//            {
//                if (grid[i][j] == 1 && !vis[i][j])
//                {
//                    int count = 0;
//                    dfs(grid, i, j, count);
//                    ret = max(ret, count);
//                }
//            }
//        }
//        return ret;
//    }
//};


