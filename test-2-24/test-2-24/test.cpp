//class Solution {
//public:
//    vector<int> intersect(vector<int>& nums1, vector<int>& nums2) {
//        unordered_map<int, int> m1;
//        unordered_map<int, int> m2;
//        for (auto& e : nums1)
//        {
//            m1[e]++;
//        }
//        for (auto& e : nums2)
//        {
//            m2[e]++;
//        }
//        //在1中找2是否出现过以及次数问题
//        vector<int> result;
//        for (auto& e : m1)
//        {
//            //记录出现次数最少的那个
//            int min = 0;
//            auto it = m2.find(e.first);
//            if (it != m2.end())
//            {
//                int m2second = it->second;
//                int m1second = e.second;
//                min = m2second > m1second ? m1second : m2second;
//            }
//            if (min != 0)
//            {
//                for (int i = 0; i < min; ++i)
//                {
//                    result.push_back(e.first);
//                }
//            }
//        }
//        return result;
//    }
//};

//class Solution {
//public:
//    vector<string> uncommonFromSentences(string s1, string s2) {
//        //把两个字符串中的元素放入哈希表中，最后统计哈希表中出现次数等于1的元素
//        unordered_map<string, int> hs;
//        stringstream ss1(s1);//利用字符串输入的时候自动以空格断开
//        stringstream ss2(s2);
//        string tmp;
//        while (ss1 >> tmp)
//        {
//            hs[tmp]++;
//        }
//        while (ss2 >> tmp)
//        {
//            hs[tmp]++;
//        }
//        vector<string> result;
//        for (auto& e : hs)
//        {
//            if (e.second == 1)
//            {
//                result.push_back(e.first);
//            }
//        }
//        return result;
//    }
//};

