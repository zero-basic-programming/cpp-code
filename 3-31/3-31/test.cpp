#include <iostream>
#include <vector>
#include <unordered_map>
using namespace std;


//class Solution {
//public:
//
//    int countSubstrings(string s) {
//        //中心扩展算法
//        int n = s.size();
//        int ret = 0;  //收集结果
//        for (int i = 0; i < n; ++i)
//        {
//            //奇数次的回文
//            int left = i, right = i;
//            while (left >= 0 && right < n)
//            {
//                if (s[left] == s[right])
//                {
//                    ++ret;
//                    --left;
//                    ++right;
//                }
//                else  break;
//            }
//            //统计偶数次回文
//            left = i, right = i + 1;
//            while (left >= 0 && right < n)
//            {
//                if (s[left] == s[right])
//                {
//                    ++ret;
//                    --left;
//                    ++right;
//                }
//                else  break;
//            }
//        }
//        return ret;
//    }
//};
//
//
//class Solution {
//public:
//    vector<vector<int> > threeSum(vector<int>& nums) {
//        //双指针
//        vector<vector<int>> ret;
//        int n = nums.size();
//        sort(nums.begin(), nums.end());
//        for (int i = 0; i < n - 2;)
//        {
//            int left = i + 1, right = n - 1;
//            int target = -nums[i];  //左右两个数相加等于目标
//            while (left < right)
//            {
//                if (nums[left] + nums[right] < target) ++left;
//                else if (nums[left] + nums[right] > target) --right;
//                else
//                {
//                    ret.push_back({ nums[i],nums[left],nums[right] });
//                    ++left;
//                    --right;
//                    //去重
//                    while (left < right && nums[left] == nums[left - 1]) ++left;
//                    while (left < right && nums[right] == nums[right + 1]) --right;
//                }
//            }
//            ++i;
//            //去重i
//            while (i < n - 2 && nums[i] == nums[i - 1]) ++i;
//        }
//        return ret;
//    }
//};



//class Solution {
//public:
//    int numberOfArithmeticSlices(vector<int>& nums) {
//        int n = nums.size();
//        vector<int> dp(n);
//        int ret = 0;  //收集结果
//        for (int i = 2; i < n; ++i)
//        {
//            if (nums[i] + nums[i - 2] == nums[i - 1] * 2) dp[i] = dp[i - 1] + 1;
//            else dp[i] = 0;
//            //更新结果
//            ret += dp[i];
//        }
//        return ret;
//    }
//};


//class Solution {
//public:
//    int maxTurbulenceSize(vector<int>& arr) {
//        int n = arr.size();
//        vector<int> f(n, 1);   //以i位置为结尾的数组成上升趋势
//        vector<int> g(n, 1);   //以i位置为结尾的数组成下降趋势
//        int ret = 1;
//        for (int i = 1; i < n; ++i)
//        {
//            if (arr[i] - arr[i - 1] > 0) f[i] = g[i - 1] + 1;
//            else if (arr[i] - arr[i - 1] < 0) g[i] = f[i - 1] + 1;
//            ret = max(ret, max(f[i], g[i]));
//        }
//        return ret;
//    }
//};


//class Solution {
//public:
//    bool wordBreak(string s, vector<string>& wordDict) {
//        //使用hash记录wordDict
//        unordered_map<string, int> hash;
//        for (auto& str : wordDict) hash[str]++;
//        string tmp;  //用来记录当前的字符串
//        int i = 0;
//        while (i < s.size())
//        {
//            tmp += s[i++];
//            if (hash.count(tmp) > 0)
//            {
//                hash.erase(tmp);
//                tmp.clear();      
//            }
//        }
//        //看最后一个是否满足
//        if (tmp.size()) return false;
//        return true;
//    }
//};
//
//int main()
//{
//    vector<string> vs = { "aaaa","aaa" };
//    cout << Solution().wordBreak("aaaaaaa", vs);
//    return 0;
//}


//class Solution {
//public:
//    bool wordBreak(string s, vector<string>& wordDict) {
//        int n = s.size();
//        unordered_set<string> hash;
//        for (auto& str : wordDict) hash.insert(str);
//        //方便下标的映射
//        s = ' ' + s;
//        vector<bool> dp(n + 1, false);
//        dp[0] = true;
//        for (int i = 1; i <= n; ++i)
//        {
//            for (int j = i; j >= 1; --j)
//            {
//                if (dp[j - 1] == true && hash.count(s.substr(j, i - j + 1)))
//                {
//                    dp[i] = true;
//                    break;
//                }
//            }
//        }
//        return dp[n];
//    }
//};
//#include <iostream>
//#include <vector>
//using namespace std;
//
//int n, m;
//string str = "tencent";
//int ret = 0;
//vector<vector<bool>> vis;
//int dx[4] = { 0, 0, 1, -1 };
//int dy[4] = { 1, -1, 0, 0 };
//
//void dfs(vector<vector<char>>& arr, int i, int j, int a) {
//    if (a == str.size()) ++ret;
//    vis[i][j] = true;
//    for (int k = 0; k < 4; ++k) {
//        int x = i + dx[k], y = j + dy[k];
//        while (x >= 0 && x < n && y >= 0 && y < m && arr[x][y] == str[a] &&
//            !vis[x][y]) {
//            dfs(arr, x, y, a + 1);
//        }
//    }
//    vis[i][j] = false;
//}
//
//void FindCount(vector<vector<char>>& arr) {
//    for (int i = 0; i < n; ++i) {
//        for (int j = 0; j < m; ++j) {
//            if (arr[i][j] == 't') {
//                dfs(arr, i, j, 1);
//            }
//        }
//    }
//}
//
//int main() {
//    cin >> n >> m;
//    vector<vector<char>> arr(n, vector<char>(m));
//    vis.resize(n, vector<bool>(m, false));
//    int c = n;
//    int i = 0;
//    while (c--) {
//        string tmp;
//        cin >> tmp;
//        vector<char> tmparr;
//        for (auto ch : tmp) tmparr.push_back(ch);
//        arr[i++] = tmparr;
//    }
//    FindCount(arr);
//    cout << ret << endl;
//}