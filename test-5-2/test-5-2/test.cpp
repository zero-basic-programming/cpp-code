#include <iostream>
using namespace std;

//class Solution {
//public:
//    int sumOfLeftLeaves(TreeNode* root) {
//        if (root == nullptr) return 0;
//        if (root->left == nullptr && root->right == nullptr)    return 0;
//        //后续遍历的方式
//        int leftval = sumOfLeftLeaves(root->left);
//        //到达左叶子节点
//        if (root->left && !root->left->right && !root->left->left)
//        {
//            leftval = root->left->val;
//        }
//        int rightval = sumOfLeftLeaves(root->right);
//        return leftval + rightval;
//    }
//};


//class Solution {
//public:
//    int sumOfLeftLeaves(TreeNode* root) {
//        //使用非递归
//        stack<TreeNode*> st;
//        int result = 0; // 记录结果
//        if (root != nullptr) st.push(root);
//        while (!st.empty())
//        {
//            TreeNode* node = st.top();
//            st.pop();
//            //前序遍历，判断左叶子节点
//            if (node->left && !node->left->left && !node->left->right)
//            {
//                result += node->left->val;
//            }
//            if (node->right)  st.push(node->right);
//            if (node->left)  st.push(node->left);
//        }
//        return result;
//    }
//};


//class Solution {
//public:
//    int findBottomLeftValue(TreeNode* root) {
//        queue<TreeNode*> q;
//        if (root)    q.push(root);
//        int result = 0;
//        while (!q.empty())
//        {
//            int size = q.size();
//            bool flag = true; // 用来记录当层第一个元素
//            while (size--)
//            {
//                TreeNode* node = q.front();
//                q.pop();
//                if (flag) {
//                    result = node->val;
//                    flag = false;
//                }
//                if (node->left)  q.push(node->left);
//                if (node->right)  q.push(node->right);
//            }
//        }
//        return result;
//    }
//};


//class Solution {
//public:
//    int maxdepth = INT_MIN;
//    int number;
//    void travelsal(TreeNode* root, int depth)
//    {
//        //到叶子节点就收集结果
//        if (root->left == nullptr && root->right == nullptr)
//        {
//            if (depth > maxdepth)
//            {
//                maxdepth = depth;
//                number = root->val;
//            }
//            return;
//        }
//        //左右
//        if (root->left)
//        {
//            depth++;
//            travelsal(root->left, depth);
//            depth--; // 回溯过程
//        }
//        if (root->right)
//        {
//            ++depth;
//            travelsal(root->right, depth);
//            --depth;
//        }
//        return;
//    }
//    int findBottomLeftValue(TreeNode* root) {
//        travelsal(root, 0);
//        return number;
//    }
//};

//class Solution {
//public:
//    int maxdepth = INT_MIN;
//    int number;
//    void travelsal(TreeNode* root, int depth)
//    {
//        //到叶子节点就收集结果
//        if (root->left == nullptr && root->right == nullptr)
//        {
//            if (depth > maxdepth)
//            {
//                maxdepth = depth;
//                number = root->val;
//            }
//            return;
//        }
//        //左右
//        if (root->left)
//        {
//            travelsal(root->left, depth + 1);
//        }
//        if (root->right)
//        {
//            travelsal(root->right, depth + 1);
//        }
//        return;
//    }
//    int findBottomLeftValue(TreeNode* root) {
//        travelsal(root, 0);
//        return number;
//    }
//};

