#include <iostream>
#include <vector>
using namespace std;


class Solution {
    int m, n;
    int dx[4] = { 0,0,1,-1 };
    int dy[4] = { 1,-1,0,0 };
public:
    void solve(vector<vector<char>>& board) {
        m = board.size(); n = board[0].size();
        //处理边界情况
        //处理第一列
        for (int i = 0; i < m; ++i)
        {
            if (board[i][0] == 'O')  dfs(board, i, 0);
        }
        //处理第一行
        for (int j = 1; j < n; ++j)
        {
            if (board[0][j] == 'O')  dfs(board, 0, j);
        }
        //处理最后一列
        for (int i = 1; i < m; ++i)
        {
            if (board[i][n - 1] == 'O') dfs(board, i, n - 1);
        }
        //处理最后一行
        for (int j = 1; j < n - 1; ++j)
        {
            if (board[m - 1][j] == 'O') dfs(board, m - 1, j);
        }
        //整体进行处理，如果遇到.就修改为O，如果遇到O就修改为X
        for (auto& v : board)
        {
            for (auto& ch : v)
            {
                if (ch == '.') ch = 'O';
                else if (ch == 'O') ch = 'X';
            }
        }
    }
    void dfs(vector<vector<char>>& board, int i, int j)
    {
        board[i][j] = '.';
        for (int k = 0; k < 4; ++k)
        {
            int x = i + dx[k], y = j + dy[k];
            if (x >= 0 && x < m && y >= 0 && y < n && board[x][y] == 'O')
            {
                dfs(board, x, y);
            }
        }
    }
};


class Solution {
    int m, n;
    int dx[4] = { 0,0,1,-1 };
    int dy[4] = { 1,-1,0,0 };
public:
    void solve(vector<vector<char>>& board) {
        m = board.size(); n = board[0].size();
        //处理边界情况
        //处理第一列,和最后一列
        for (int i = 0; i < m; ++i)
        {
            if (board[i][0] == 'O')  dfs(board, i, 0);
            if (board[i][n - 1] == 'O') dfs(board, i, n - 1);
        }
        //处理第一行,和最后一行
        for (int j = 1; j < n - 1; ++j)
        {
            if (board[0][j] == 'O')  dfs(board, 0, j);
            if (board[m - 1][j] == 'O') dfs(board, m - 1, j);
        }
        //整体进行处理，如果遇到.就修改为O，如果遇到O就修改为X
        for (auto& v : board)
        {
            for (auto& ch : v)
            {
                if (ch == '.') ch = 'O';
                else if (ch == 'O') ch = 'X';
            }
        }
    }
    void dfs(vector<vector<char>>& board, int i, int j)
    {
        board[i][j] = '.';
        for (int k = 0; k < 4; ++k)
        {
            int x = i + dx[k], y = j + dy[k];
            if (x >= 0 && x < m && y >= 0 && y < n && board[x][y] == 'O')
            {
                dfs(board, x, y);
            }
        }
    }
};


class Solution {
    int m, n;
    int dx[4] = { 0,0,1,-1 };
    int dy[4] = { 1,-1,0,0 };
public:
    vector<vector<int>> pacificAtlantic(vector<vector<int>>& heights) {
        vector<vector<int>> ret;
        m = heights.size(), n = heights[0].size();
        vector<vector<bool>> pcheck(m, vector<bool>(n, false));
        vector<vector<bool>> acheck(m, vector<bool>(n, false));
        //处理边界
        //处理第一列和最后一列
        for (int i = 0; i < m; ++i)
        {
            dfs(heights, i, 0, pcheck);
            dfs(heights, i, n - 1, acheck);
        }
        //处理第一行和最后一行
        for (int j = 0; j < n; ++j)
        {
            dfs(heights, 0, j, pcheck);
            dfs(heights, m - 1, j, acheck);
        }
        //收集结果
        for (int i = 0; i < m; ++i)
        {
            for (int j = 0; j < n; ++j)
            {
                if (pcheck[i][j] && acheck[i][j])
                    ret.push_back({ i,j });
            }
        }
        return ret;
    }
    void dfs(vector<vector<int>>& heights, int i, int j, vector<vector<bool>>& check)
    {
        check[i][j] = true;
        for (int k = 0; k < 4; ++k)
        {
            int x = i + dx[k], y = j + dy[k];
            if (x >= 0 && x < m && y >= 0 && y < n
                && !check[x][y] && heights[x][y] >= heights[i][j])
                dfs(heights, x, y, check);
        }
    }
};