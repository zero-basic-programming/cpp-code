#include <iostream>
#include <unordered_map>
#include <unordered_set>
#include <vector>
using namespace std;


//int main()
//{
//    unordered_map<double, int> fly;       //用来记录起飞的飞机的机场编号和起飞次数的映射关系
//    unordered_map<double, int> down;       //用来记录降落的飞机的机场编号和起飞次数的映射关系
//    unordered_set<double> set;           //用来记录机场编号
//    vector<int> ret(15, 0);               //用来记录结果
//    int m, n;
//    cin >> m;
//    vector<double> inputFly(m);
//    for (auto& e : inputFly)
//    {
//        cin >> e;
//        set.insert(e);
//    }
//    cin >> n;
//    vector<double> inputDown(n);
//    for (auto& e : inputDown)
//    {
//        cin >> e;
//        set.insert(e);
//    }
//    for (auto e : inputFly)    ++fly[e];
//    for (auto e : inputDown)    ++down[e];
//    //将起飞的和降落的飞机做差，得到绝对值
//    for (auto e : set)
//    {
//        int gap = fly[e] - down[e];
//        ++ret[gap - 1];
//    }
//    for (auto e : ret) cout << e << " ";
//    return 0;
//}




//int main()
//{
//    string str;
//    cin >> str;
//    //通过遍历到最后都为0的时候就可以结束遍历了
//    int n = str.size();
//    int ret = 0;
//    while (true)
//    {
//        bool flag = false;  //这里用来标记是否符合条件，也就是当一次循环中没有做出修改字符串的行为的时候
//        for (int i = 0; i < n - 1;)
//        {
//            if (str[i] == '0' && str[i + 1] == '1')
//            {
//                //这里说明对字符串进行了修改，需要更新标志
//                flag = true;
//                str[i] = '1';
//                str[i + 1] = '0';
//                i += 2;
//            }
//            else ++i;  //其他情况都是直接往后判断
//        }
//        if (!flag) break;
//        ++ret;   //最后一次是跳出循环
//    }
//    cout << ret << endl;
//    return 0;
//}



int main()
{
    string str;
    cin >> str;
    //通过遍历到最后都为0的时候就可以结束遍历了
    int n = str.size();
    int ret = 0;
    int zeroIdx = 0;
    int oneIdx = 0;
    for (int i = 0; i < n; ++i)
    {
        if (str[i] == '0') zeroIdx = i; break;
    }
    for (int i = zeroIdx + 1; i < n; ++i)
    {
        if (str[i] == '1') ++ret;
    }
    cout << ret << endl;
    return 0;
}