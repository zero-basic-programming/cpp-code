#include <iostream>
#include <vector>
using namespace std;


//class Solution {
//public:
//    vector<int> asteroidCollision(vector<int>& asteroids) {
//        vector<int> ret;
//        int n = asteroids.size();
//        ret.push_back(asteroids[0]);
//        //从第2个开始，如果当前的符合和上一个的符号相同就不用处理
//        //如果不同，当前为负就碰撞，为正就无事
//        for (int i = 1; i < n; ++i)
//        {
//            if (asteroids[i] < 0 && ret.back() > 0)
//            {
//                //发生碰撞
//                while (ret.size() > 0 && asteroids[i] < 0 && ret.back() > 0 && abs(asteroids[i]) > ret.back())
//                {
//                    //需要往前更新
//                    ret.pop_back();
//                }
//                if (abs(asteroids[i]) == ret.back())   //这里两个块抵消，不能影响前面
//                {
//                    ret.pop_back();
//                    continue;
//                }
//                //最后除非ret的最后一个是正数比它大，否则都需要放入ret中
//                if (ret.back() > 0 && ret.back() > abs(asteroids[i]))
//                {
//
//                }
//                else ret.push_back(asteroids[i]);
//            }
//            else
//            {
//                //都不会发生碰撞
//                ret.push_back(asteroids[i]);
//            }
//        }
//        return ret;
//    }
//};



//class Solution {
//public:
//    vector<int> asteroidCollision(vector<int>& asteroids) {
//        vector<int> st;    //用来存放结果
//        for (auto aster : asteroids)
//        {
//            bool flag = true;  // 用来记录小行星是否存在
//            while (st.size() > 0 && st.back() > 0 && aster < 0 && flag)
//            {
//                flag = st.back() < -aster;
//                //前面的元素是否被该小行星撞击
//                if (st.back() <= -aster) st.pop_back();
//            }
//            if (flag) st.push_back(aster);
//        }
//        return st;
//    }
//};


//class Solution {
//public:
//    vector<int> dailyTemperatures(vector<int>& temperatures) {
//        int n = temperatures.size();
//        vector<int> ret(n);
//        ret[n - 1] = 0;
//        for (int i = 0; i < n - 1; ++i)
//        {
//            //这里可以通过往后找第一个比自己大的数来计算他们之间的差值得到结果
//            for (int j = i + 1; j < n; ++j)
//            {
//                if (temperatures[j] > temperatures[i])
//                {
//                    ret[i] = j - i;
//                    break;
//                }
//            }
//        }
//        return ret;
//    }
//};


//class Solution {
//public:
//    vector<int> dailyTemperatures(vector<int>& temperatures) {
//        vector<int> st;  //使用数组来模拟栈
//        int n = temperatures.size();
//        vector<int> ret(n);
//        for (int i = 0; i < n; ++i)
//        {
//            //如果如的这个元素比栈顶元素大，那么就需要更新对应栈中的下标对应的元素
//            while (st.size() && temperatures[i] > temperatures[st.back()])
//            {
//                int prevIdx = st.back();
//                st.pop_back();
//                ret[prevIdx] = i - prevIdx;
//            }
//            //入栈
//            st.push_back(i);
//        }
//        return ret;
//    }
//};


