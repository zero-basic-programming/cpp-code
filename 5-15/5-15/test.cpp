#include <iostream>
#include <vector>
using namespace std;

//class Solution {
//public:
//    vector<vector<int>> decorateRecord(TreeNode* root) {
//        vector<vector<int>> ret;
//        if (root == nullptr) return ret;
//        queue<TreeNode*> q;
//        q.push(root);
//        int level = 0;  //层数
//        while (q.size())
//        {
//            int sz = q.size();
//            vector<int> tmp;  //收集当层的结果
//            for (int i = 0; i < sz; ++i)
//            {
//                TreeNode* node = q.front();
//                q.pop();
//                tmp.push_back(node->val);
//                if (node->left) q.push(node->left);
//                if (node->right) q.push(node->right);
//            }
//            if (level % 2 == 1) reverse(tmp.begin(), tmp.end());
//            ret.push_back(tmp);
//            ++level;
//        }
//        return ret;
//    }
//};


//class Solution {
//public:
//    bool verifyTreeOrder(vector<int>& postorder) {
//        int n = postorder.size();
//        if (n == 0 || n == 1) return true;
//        else if (n == 2) return postorder[0] < postorder[1];
//        int left = 0, right = 1, mid = 2;
//        while (mid < n)
//        {
//            if (postorder[mid] < postorder[left] || postorder[mid] > postorder[right])
//                return false;
//            //迭代
//            left = mid;
//            right = mid + 1;
//            mid = right + 1;
//        }
//        return true;
//    }
//};

//
//class Solution {
//public:
//    bool verifyTreeOrder(vector<int>& postorder) {
//        return dfs(postorder, 0, postorder.size() - 1);
//    }
//    bool dfs(vector<int>& postorder, int l, int r)
//    {
//        if (l >= r) return true;
//        int p = l;  //通过p找第一个大于根节点的
//        if (postorder[p] < postorder[r]) ++p;
//        int m = p;
//        //[l,m-1] [m,r-1]
//        while (postorder[p] > postorder[r]) ++p;
//        return p == r && dfs(postorder, l, m - 1) && dfs(postorder, m, r - 1);
//    }
//};