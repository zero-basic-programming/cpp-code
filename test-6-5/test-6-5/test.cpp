#include <iostream>

//class Solution {
//private:
//    vector<int> path;
//    vector<vector<int>> result;
//    int sum = 0; // 保存临时的结果
//public:
//    void backtracking(vector<int>& v, int target, int first)
//    {
//        if (sum > target)
//        {
//            return;
//        }
//        if (sum == target)
//        {
//            保存结果
//            result.push_back(path);
//            return;
//        }
//        for (int i = first; i < v.size(); ++i)
//        {
//            sum += v[i];
//            path.push_back(v[i]);
//            backtracking(v, target, i);
//            sum -= v[i]; // 回溯
//            path.pop_back();
//        }
//        return;
//    }
//    vector<vector<int>> combinationSum(vector<int>& candidates, int target) {
//        backtracking(candidates, target, 0);
//        return result;
//    }
//};

//class Solution {
//private:
//    vector<int> path;
//    vector<vector<int>> result;
//    int sum = 0; // 保存临时的结果
//public:
//    void backtracking(vector<int>& v, int target, int first)
//    {
//        if (sum == target)
//        {
//            //保存结果
//            result.push_back(path);
//            return;
//        }
//        for (int i = first; i < v.size() && sum + v[i] <= target; ++i) // 优化
//        {
//            sum += v[i];
//            path.push_back(v[i]);
//            backtracking(v, target, i);
//            sum -= v[i]; // 回溯
//            path.pop_back();
//        }
//        return;
//    }
//    vector<vector<int>> combinationSum(vector<int>& candidates, int target) {
//        sort(candidates.begin(), candidates.end());
//        backtracking(candidates, target, 0);
//        return result;
//    }
//};


//class Solution {
//public:
//    vector<vector<int>> result;
//    vector<int> path;
//    void backtracking(vector<int>& v, vector<bool>& used, int first, int sum, int target)
//    {
//        if (sum > target)   return;
//        if (sum == target)
//        {
//            //收集结果
//            result.push_back(path);
//            return;
//        }
//        for (int i = first; i < v.size() && sum + v[i] <= target; ++i)
//        {
//            if (i > 0 && v[i] == v[i - 1] && used[i - 1] == false)
//            {
//                continue;
//            }
//            used[i] = true;
//            path.push_back(v[i]);
//            sum += v[i];
//            backtracking(v, used, i + 1, sum, target);
//            sum -= v[i]; //回溯
//            path.pop_back(); // 回溯
//            used[i] = false; // 回溯
//        }
//        return;
//    }
//    vector<vector<int>> combinationSum2(vector<int>& candidates, int target) {
//        vector<bool> used(candidates.size(), false); //用来标记已经使用过的candidates中的值
//        sort(candidates.begin(), candidates.end());
//        backtracking(candidates, used, 0, 0, target);
//        return result;
//
//    }
//};