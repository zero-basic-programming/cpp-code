#include <iostream>
#include <vector>
using namespace std;

//class Solution {
//public:
//    int findLongestChain(vector<vector<int>>& pairs) {
//        //先排序保证顺序的正确性
//        sort(pairs.begin(), pairs.end());
//        int n = pairs.size();
//        vector<int> dp(n, 1);
//        for (int i = 1; i < n; ++i)
//        {
//            for (int j = i - 1; j >= 0; --j)
//            {
//                if (pairs[i][0] > pairs[j][1])
//                {
//                    dp[i] = max(dp[j] + 1, dp[i]);
//                    break;
//                }
//            }
//        }
//        return dp[n - 1];
//    }
//};



//class Solution {
//public:
//    char change_ch(string& s, int front, int back, string& ret)
//    {
//        for (int i = 'a'; i <= 'z'; ++i)
//        {
//            if (front == -1 && back > s.size()) return i;
//            if (front == -1 && i != s[back])
//            {
//                //处理？字符在第一个的情况
//                return i;
//            }
//            else if (back > s.size() && i != s[front])
//            {
//                return i;
//            }
//            else if (ret[front] != i && s[back] != i)
//            {
//                return i;
//            }
//        }
//        return 'z';
//    }
//    string modifyString(string s) {
//        string ret;
//        for (int i = 0; i < s.size(); ++i)
//        {
//            if (s[i] == '?')
//            {
//                ret += change_ch(s, i - 1, i + 1, ret);
//            }
//            else {
//                ret += s[i];
//            }
//        }
//        return ret;
//    }
//};


//class Solution {
//public:
//    string modifyString(string s) {
//        //直接在原地改变字符串即可
//        int n = s.size();
//        for (int i = 0; i < n; ++i)
//        {
//            if (s[i] == '?')
//            {
//                //判断前面和后面的两个字符是否和当前字符相同
//                for (char ch = 'a'; ch <= 'z'; ++ch)
//                {
//                    if ((i == 0 || ch != s[i - 1]) && (i == n - 1 || ch != s[i + 1]))
//                        s[i] = ch;
//                }
//            }
//        }
//        return s;
//    }
//};

