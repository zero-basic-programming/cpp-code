#include <iostream>
#include <vector>
using namespace std;

//int main()
//{
//	cout << "hello world" << endl;
//	return 0;
//}


//class Solution {
//    const int INF = 1e9 + 7;
//    vector<int> demo;
//public:
//    int dfs(int n)
//    {
//        //如果当前已经计算过，那么我们直接在demo中查找即可
//        if (demo[n] != 0) return demo[n];
//        if (n == 0 || n == 1)
//        {
//            demo[n] = 1;
//            return 1;
//        }
//        demo[n] = (dfs(n - 2) + dfs(n - 1)) % INF;   //把计算得到的结果放入到数组中
//        return demo[n];
//    }
//    int trainWays(int num) {
//        //记忆化搜索
//        demo.resize(num + 1);
//        dfs(num);
//        return demo[num];
//    }
//};


//class Solution {
//    const int INF = 1e9 + 7;
//public:
//    int trainWays(int num) {
//        //动态规划
//        if (num == 0) return 1;
//        if (num == 1) return 1;
//        int first = 1, second = 1;
//        int third = 2;
//        for (int i = 2; i <= num; ++i)
//        {
//            third = (first + second) % INF;
//            //迭代
//            first = second;
//            second = third;
//        }
//        return third;
//    }
//};


//class Solution {
//public:
//    int stockManagement(vector<int>& stock) {
//        int minnum = INT_MAX;
//        for (auto e : stock)
//        {
//            if (e < minnum) minnum = e;
//        }
//        return minnum;
//    }
//};


//class Solution {
//public:
//    int stockManagement(vector<int>& stock) {
//        int target = stock[0];  //最左边的元素
//        int left = 0, right = stock.size() - 1;
//        while (left < right)
//        {
//            int mid = left + (right - left) / 2;
//            if (stock[mid] > target) left = mid;
//            else right = mid - 1;
//        }
//        return stock[right] > target ? target : stock[right];
//    }
//};


//class Solution {
//public:
//    int findMin(vector<int>& nums) {
//        //利用2分算法，中间值一定是比右边的值小的
//        int left = 0, right = nums.size() - 1;
//        while (left < right)
//        {
//            int mid = left + (right - left) / 2;
//            if (nums[mid] > nums[right])
//            {
//                //大于右边界，说明其左边的值都不符合要求
//                left = mid + 1;
//            }
//            else if (nums[mid] < nums[right]) right = mid;
//            else right--;  //这里因为存在重复元素，所以只能排除右边一个元素
//        }
//        return nums[left];
//    }
//};


//class Solution {
//    vector<vector<bool>> vis;  //用来标记已经使用过的元素
//    int m, n;
//    int dx[4] = { 0,0,1,-1 };
//    int dy[4] = { 1,-1,0,0 };
//public:
//    bool dfs(vector<vector<char>>& grid, int i, int j, string& target, int index)
//    {
//        if (index == target.size()) return true;
//        vis[i][j] = true;
//        for (int k = 0; k < 4; ++k)
//        {
//            int x = dx[k] + i, y = dy[k] + j;
//            if (x >= 0 && x < m && y >= 0 && y < n && !vis[x][y] && grid[x][y] == target[index])
//            {
//                if (dfs(grid, x, y, target, index + 1)) return true;
//            }
//        }
//        vis[i][j] = false;
//        return false;
//    }
//    bool wordPuzzle(vector<vector<char>>& grid, string target) {
//        m = grid.size(), n = grid[0].size();
//        vis.resize(m, vector<bool>(n, false));
//        for (int i = 0; i < m; ++i)
//        {
//            for (int j = 0; j < n; ++j)
//            {
//                if (grid[i][j] == target[0])
//                {
//                    if (dfs(grid, i, j, target, 1)) return true;
//                }
//            }
//        }
//        return false;
//    }
//};


