#include <iostream>
#include <vector>
using namespace std;

//堆排
void AdjustDown(vector<int>& arr, int parent, int n)
{
	int child = 2 * parent + 1;
	while (child < n)
	{
		//判断右孩子是否比左孩子大
		if (child + 1 < n && arr[child] < arr[child + 1])
		{
			++child;
		}
		if (arr[child] > arr[parent])
		{
			swap(arr[child], arr[parent]);
			//迭代
			parent = child;
			child = 2 * parent + 1;
		}
		else
		{
			//这里已经成堆
			break;
		}
	}
}

void HeapSort(vector<int>& arr)
{
	//向下调整算法建堆
	int n = arr.size();
	for (int i = (n - 1 - 1) / 2; i >= 0; --i)
	{
		AdjustDown(arr, i, n);
	}
	for (int i = 1; i < n; ++i)
	{
		swap(arr[0], arr[n - i]);
		//从堆顶调整
		AdjustDown(arr, 0, n - i);
	}
}


//选择排序
void SelectSort(vector<int>& arr)
{
	int n = arr.size();
	int left = 0, right = n - 1;
	while (left < right)
	{
		int mini = left, maxi = left;
		for (int i = left; i <= right; ++i)
		{
			if (arr[i] < arr[mini]) mini = i;
			if (arr[i] > arr[maxi]) maxi = i;
		}
		swap(arr[maxi], arr[right]);
		//判定一下特殊情况，这里可能mini在right的位置
		if (mini == right) mini = maxi;
		swap(arr[mini], arr[left]);
		--right;
		++left;
	}
}

//冒泡
void BufferSort(vector<int>& arr)
{
	int n = arr.size();
	for (int i = 0; i < n; ++i) //趟数
	{
		bool flag = true;
		for (int j = 0; j < n - i -1; ++j)
		{
			if (arr[j] > arr[j + 1])
			{
				swap(arr[j], arr[j + 1]);
				flag = false;
			}
		}
		if (flag) break; //说明这一趟没有排过元素，已经是有序
	}
}


//插入排序
void InsertSort(vector<int>& arr)
{
	int n = arr.size();
	for (int i = 1; i < n; ++i)
	{
		int end = i - 1;
		int target = arr[i];
		while (end >= 0)
		{
			if (arr[end] > target)
			{
				arr[end + 1] = arr[end];
			}
			else break; //这里说明找到了一个正确的位置
			--end;
		}
		arr[end + 1] = target;
	}
}

//希尔排序
void ShellSort(vector<int>& arr)
{
	int n = arr.size();
	int gap = n;
	while (gap > 1)
	{
		gap /= 2;
		for (int i = 0; i + gap < n; ++i)
		{
			int end = i;
			int target = arr[end + gap];
			while (end >= 0)
			{
				if (arr[end] > target)
				{
					arr[end + gap] = arr[end];
				}
				else break;
				end -= gap;
			}
			arr[end + gap] = target;
		}
	}
}


//计数排序 -- 适用于范围相对差较小的数，时间复杂度为O(N)
void CountSort(vector<int>& arr)
{
	int n = arr.size();
	int maxn = INT_MIN; //最大
	int minn = INT_MAX; //最小
	for (auto e : arr)
	{
		if (e > maxn) maxn = e;
		if (e < minn) minn = e;
	}
	int gap = maxn - minn; 
	//开辟hash数组
	vector<int> hash(gap+1);
	for (auto e : arr)
	{
		hash[e - minn]++;
	}
	//还原回原数组
	int j = 0;
	for (int i = 0; i < gap; ++i)
	{
		while (hash[i]--)
		{
			arr[j++] = i + minn;
		}
	}
}


int main()
{
	vector<int> arr = { 1,4,3,5,2,3,7,5,6,8,9,0 };
	//HeapSort(arr);
	//SelectSort(arr);
	//BufferSort(arr);
	//InsertSort(arr);
	//ShellSort(arr);
	CountSort(arr);
	for (auto e : arr)
	{
		cout << e << " ";
	}
	return 0;
}