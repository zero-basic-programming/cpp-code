#include <iostream>
#include <vector>
using namespace std;

//bool IsVal(long long num, int k)
//{
//    int count = 0;
//    while (num)
//    {
//        if (num % 10 == 0) ++count;
//        num /= 10;
//    }
//    return count >= k ? true : false;
//}
//
//int main() {
//    int n, k;
//    cin >> n >> k;
//    vector<int> arr(n);
//    for (auto& e : arr)
//    {
//        cin >> e;
//    }
//    //1.先把全部的乘积统计出来
//    long long sum = 1;
//    for (auto e : arr)
//    {
//        sum *= e;
//    }
//    int left = 0, right = 0;
//    int sz = arr.size();
//    int ret = 0;  //统计结果
//    long long tmp = 1;  //用来统计区间的数
//    while (right < sz)
//    {
//        //进窗口
//        tmp *= arr[right++];
//        while (IsVal(sum / tmp, k))
//        {
//            ++ret;
//            tmp *= arr[right++];
//        }
//        //出窗口.除了第一个都需要进行每个的判断
//        bool flag = true;
//        while (left < right)
//        {
//            if (flag)
//            {
//                tmp /= arr[left++];
//                if (IsVal(sum / tmp, k)) ++ret;
//                flag = false;
//                continue;
//            }
//            if (IsVal(sum / arr[left], k)) ++ret;
//            tmp /= arr[left++];
//            if (IsVal(sum / tmp, k)) ++ret;
//        }
//        ++right;
//    }
//    cout << ret << endl;
//}


//int main()
//{
//	int n = 4;
//	vector<vector<int>> arr(n, vector<int>(n));  
//	//初始化
//	arr[0] = { 1,0,1,0 };
//	arr[1] = { 0 ,1,0,1 };
//	arr[2] = { 1,1,0,0 };
//	arr[3] = { 0,0,1,1 };
//	//创建前缀和数组
//	vector<vector<int>> dp(n+1, vector<int>(n+1));
//	for (int i = 1; i <= n; ++i)
//	{
//		for (int j = 1; j <= n; ++j)
//		{
//			//这里要注意原数组和dp数组的下标的关系
//			dp[i][j] = dp[i - 1][j] + dp[i][j - 1] - dp[i - 1][j - 1] + arr[i-1][j-1];
//		}
//	}
//	//开始收集结果
//	cout << 0 << endl;  //第一次结果一定是0
//	for (int k = 1; k < n; ++k)
//	{
//		//一次i*i的结果
//		int count = 0;
//		for (int i = 1; i + k <= n; ++i)
//		{
//			for (int j = 1; j + k <= n; ++j)
//			{
//				int x1 = i - 1, y1 = j - 1;
//				int x2 = i + k, y2 = j + k;
//				int tmp = (k+1) * (k+1); 
//				int sum = dp[x2][y2] - dp[x1][y2] - dp[x2][y1] + dp[x1][y1];
//				//cout << "sum: " << sum << "tmp: " << tmp << endl;
//				if (tmp / sum == 2 && tmp % sum == 0) ++count;
//			}
//		}
//		cout << count << endl;
//	}
//	return 0;
//}

