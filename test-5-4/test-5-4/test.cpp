#include <iostream>
using namespace std;

//class Solution {
//public:
//    TreeNode* mergeTrees(TreeNode* root1, TreeNode* root2) {
//        if (root1 == nullptr)    return root2;
//        if (root2 == nullptr)    return root1;
//        //这里就把值加起来建一棵新的树
//        int val = root1->val + root2->val;
//        TreeNode* root = new TreeNode(val);
//        //构建左右子树
//        root->left = mergeTrees(root1->left, root2->left);
//        root->right = mergeTrees(root1->right, root2->right);
//        return root;
//    }
//};

//class Solution {
//public:
//    TreeNode* mergeTrees(TreeNode* root1, TreeNode* root2) {
//        if (root1 == nullptr)    return root2;
//        if (root2 == nullptr)    return root1;
//        使用非递归，队列来模拟
//        queue<TreeNode*> q;
//        q.push(root1);
//        q.push(root2);
//        这里我们不需要构建新的树，在root1的基础下改造即可
//        while (!q.empty())
//        {
//            TreeNode* node1 = q.front();  q.pop();
//            TreeNode* node2 = q.front();  q.pop();
//            把值加到root1中
//            node1->val += node2->val;
//            放入左右子树
//            if (node1->left && node2->left)
//            {
//                q.push(node1->left);
//                q.push(node2->left);
//            }
//            if (node1->right && node2->right)
//            {
//                q.push(node1->right);
//                q.push(node2->right);
//            }
//            如果root1中左右子树其中一个为空那么直接返回右树的节点即可
//            if (node1->left == nullptr)
//            {
//                node1->left = node2->left;
//            }
//            if (node1->right == nullptr)
//            {
//                node1->right = node2->right;
//            }
//        }
//        return root1;
//    }
//};


//class Solution {
//public:
//    TreeNode* searchBST(TreeNode* root, int val) {
//        //利用前序去二分查找
//        if (root == nullptr || root->val == val) return root;
//        TreeNode* result = nullptr;
//        if (root->val < val)
//        {
//            //往右树找
//            result = searchBST(root->right, val);
//        }
//        if (root->val > val)
//        {
//            result = searchBST(root->left, val);
//        }
//        return result;
//    }
//};


//class Solution {
//public:
//    TreeNode* searchBST(TreeNode* root, int val) {
//        //使用迭代
//        TreeNode* node = root;
//        while (node)
//        {
//            if (node->val == val)    return node;
//            else if (node->val > val)    node = node->left;
//            else    node = node->right;
//        }
//        return nullptr;
//    }
//};


//class Solution {
//public:
//    TreeNode* pre = nullptr; // 用来记录前一个节点
//    bool isValidBST(TreeNode* root) {
//        if (root == nullptr) return true;
//        bool left = isValidBST(root->left);
//        //中间判断大小
//        if (pre && pre->val >= root->val)    return false;
//        //更新pre
//        pre = root;
//        bool right = isValidBST(root->right);
//        return left && right;
//    }
//};


//class Solution {
//public:
//    bool isValidBST(TreeNode* root) {
//        //使用栈模拟非递归过程
//        if (root == nullptr) return true;
//        stack<TreeNode*> st;
//        TreeNode* cur = root;
//        TreeNode* pre = nullptr; // 用来记录前一个
//        while (cur || !st.empty())
//        {
//            if (cur)
//            {
//                //入栈
//                st.push(cur);
//                cur = cur->left;
//            }
//            else
//            {
//                cur = st.top();
//                st.pop();
//                if (pre && pre->val >= cur->val) return false;
//                //更新pre
//                pre = cur;
//                cur = cur->right;
//            }
//        }
//        return true;
//    }
//};


