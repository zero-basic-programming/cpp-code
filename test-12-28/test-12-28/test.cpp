#include <iostream>
#include <vector>
using namespace std;


//struct ListNode* ReverseList(struct ListNode* pHead) {
//    // write code here
//    if (pHead == NULL || pHead->next == NULL) return pHead;
//    struct ListNode* l1 = pHead;
//    struct ListNode* l2 = pHead->next;
//    struct ListNode* l3 = NULL;
//    l1->next = NULL;
//    while (l2)
//    {
//        l3 = l2->next;
//        l2->next = l1;
//        //迭代
//        l1 = l2;
//        l2 = l3;
//    }
//    return l1;
//}


//class Solution {
//public:
//    void merge(vector<int>& nums1, int m, vector<int>& nums2, int n) {
//        int i = m - 1, j = n - 1, k = m + n - 1;
//        while (i >= 0 && j >= 0)
//        {
//            nums1[k--] = nums1[i] >= nums2[j] ? nums1[i--] : nums2[j--];
//        }
//        //这里如果是j为0就不需要处理了，但是如果j不为0就需要处理
//        while (j >= 0)
//        {
//            nums1[k--] = nums2[j--];
//        }
//    }
//};


//class Solution {
//public:
//    int removeElement(vector<int>& nums, int val) {
//        int n = nums.size();
//        int left = 0, right = n - 1;
//        while (left <= right)
//        {
//            //右指针找值为val
//            while (left <= right && nums[right] == val) --right;
//            //左指针找不等于val的元素
//            while (left <= right && nums[left] != val) ++left;
//            if (left > right) break;
//            //这里一定满足条件
//            swap(nums[left++], nums[right--]);
//        }
//        return left;
//    }
//};


//class Solution {
//public:
//    int removeDuplicates(vector<int>& nums) {
//        int n = nums.size();
//        int i = 0, j = 0;
//        while (j < n)
//        {
//            //j找和i不同的元素
//            while (j < n && nums[j] == nums[i]) ++j;
//            if (j == n) break;
//            nums[++i] = nums[j++];
//        }
//        return i + 1;
//    }
//};


