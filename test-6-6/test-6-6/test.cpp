#include <iostream>
#include <vector>
#include <string>

//class Solution {
//public:
//    vector<vector<string>> result;
//    vector<string> path;
//public:
//    bool is_palindrome(const string& s, int left, int right)
//    {
//        while (left < right)
//        {
//            if (s[left] != s[right]) return false;
//            ++left;
//            --right;
//        }
//        return true;
//    }
//    void backtracking(const string& s, int startindex)
//    {
//        //终止条件，当startindex大于字符串大小的时候
//        if (startindex >= s.size())
//        {
//            result.push_back(path);
//            return;
//        }
//        //遍历一行
//        for (int i = startindex; i < s.size(); ++i)
//        {
//            //判断startindex到i范围内是否有回文串
//            if (!is_palindrome(s, startindex, i))
//            {
//                //不是回文串，那就剪枝
//                continue;
//            }
//            //保存结果，然后递归加回溯
//            path.push_back(s.substr(startindex, i - startindex + 1));
//            backtracking(s, i + 1);
//            path.pop_back(); //回溯
//        }
//        return;
//    }
//    vector<vector<string>> partition(string s) {
//        backtracking(s, 0); // 放开始的位置
//        return result;
//    }
//};

//class Solution {
//private:
//    vector<string> result;
//    string path;
//    int n = 0; // 表示分隔了多少个整数
//public:
//    bool is_effect(const string& s, int start, int end) {
//        if (start > end) {
//            return false;
//        }
//        if (s[start] == '0' && start != end) { // 0开头的数字不合法
//            return false;
//        }
//        int num = 0;
//        for (int i = start; i <= end; i++) {
//            if (s[i] > '9' || s[i] < '0') { // 遇到非数字字符不合法
//                return false;
//            }
//            num = num * 10 + (s[i] - '0');
//            if (num > 255) { // 如果大于255了不合法
//                return false;
//            }
//        }
//        return true;
//    }
//    void backtracking(string& s, int startIndex) {
//        if (n == 3) { // 逗点数量为3时，分隔结束
//            // 判断第四段子字符串是否合法，如果合法就放进result中
//            if (is_effect(s, startIndex, s.size() - 1)) {
//                result.push_back(s);
//            }
//            return;
//        }
//        for (int i = startIndex; i < s.size(); i++) {
//            if (is_effect(s, startIndex, i)) { // 判断 [startIndex,i] 这个区间的子串是否合法
//                s.insert(s.begin() + i + 1, '.');  // 在i的后面插入一个逗点
//                n++;
//                backtracking(s, i + 2);   // 插入逗点之后下一个子串的起始位置为i+2
//                n--;                         // 回溯
//                s.erase(s.begin() + i + 1);         // 回溯删掉逗点
//            }
//            else break; // 不合法，直接结束本层循环
//        }
//    }
//    vector<string> restoreIpAddresses(string s) {
//        if (s.size() < 4 || s.size() > 12)   return result;
//        backtracking(s, 0);
//        return result;
//    }
//};

//class Solution {
//private:
//    vector<string> result;
//    string path;
//    int n = 0; // 表示分隔了多少个整数
//public:
//    bool is_effect(const string& s, int start, int end) {
//        if (start > end) {
//            return false;
//        }
//        if (s[start] == '0' && start != end) { // 0开头的数字不合法
//            return false;
//        }
//        int num = 0;
//        for (int i = start; i <= end; i++) {
//            if (s[i] > '9' || s[i] < '0') { // 遇到非数字字符不合法
//                return false;
//            }
//            num = num * 10 + (s[i] - '0');
//            if (num > 255) { // 如果大于255了不合法
//                return false;
//            }
//        }
//        return true;
//    }
//    void backtrackding(const string& s, int startindex)
//    {
//        if (n == 3)
//        {
//            if (is_effect(s, startindex, s.size() - 1)) {
//
//                path += s.substr(startindex, s.size() - startindex);
//                result.push_back(path);
//            }
//            return;
//        }
//        for (int i = startindex; i < s.size(); ++i)
//        {
//            if (!is_effect(s, startindex, i))
//            {
//                continue;
//            }
//            auto size = path.size();
//            path += s.substr(startindex, i - startindex + 1);
//            path += ".";
//            ++n;
//            backtrackding(s, i + 1);
//            --n;
//            path.erase(size); //这里把.也删了
//        }
//        return;
//    }
//    vector<string> restoreIpAddresses(string s) {
//        if (s.size() < 4 || s.size() > 12)   return result;
//        backtrackding(s, 0);
//        return result;
//    }
//};

using namespace std;

//int main()
//{
//	string s = "1245";
//	int tmp = stoi(s);
//	cout << tmp << endl;
//	return 0;
//}