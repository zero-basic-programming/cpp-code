#include <iostream>
#include <vector>
using namespace std;

//class Solution {
//public:
//    ListNode* addTwoNumbers(ListNode* l1, ListNode* l2) {
//        ListNode* guard = new ListNode(0);
//        ListNode* tail = guard;
//        int carry = 0;  //处理进位
//        while (l1 || l2)
//        {
//            int sum = carry;  //当前的余数加上一次的进位
//            if (l1) sum += l1->val;
//            if (l2) sum += l2->val;
//            tail->next = new ListNode(sum % 10);
//            carry = sum / 10;
//            tail = tail->next;
//            if (l1) l1 = l1->next;
//            if (l2) l2 = l2->next;
//        }
//        //最后可能还有一个进位
//        if (carry == 1) tail->next = new ListNode(1);
//        ListNode* newhead = guard->next;
//        delete guard;  //释放头节点
//        return newhead;
//    }
//};


//class Solution {
//public:
//    ListNode* swapPairs(ListNode* head) {
//        if (head == nullptr || head->next == nullptr) return head; //特殊情况
//        ListNode* n1 = head;
//        ListNode* n2 = head->next;
//        ListNode* tmp = n2->next;  //记录后一个位置
//        ListNode* newhead = n2;  //记录新的头
//        while (tmp && tmp->next)
//        {
//            n2->next = n1;
//            n1->next = tmp->next;
//            //迭代
//            n1 = tmp;
//            n2 = tmp->next;
//            tmp = n2->next;
//        }
//        //这里处理最后一组
//        n2->next = n1;
//        n1->next = tmp;
//        return newhead;
//    }
//};


//class Solution {
//public:
//    ListNode* swapPairs(ListNode* head) {
//        //使用递归方法
//        //返回
//        if (head == nullptr || head->next == nullptr) return head;
//        ListNode* n1 = head, * n2 = head->next;
//        n1->next = swapPairs(n2->next);
//        n2->next = n1;
//        return n2;
//    }
//};


//class Solution {
//public:
//    ListNode* ReverseList(ListNode* head)
//    {
//        if (head == nullptr || head->next == nullptr) return head;
//        ListNode* n1 = head, * n2 = head->next, * n3 = nullptr;
//        n1->next = nullptr;
//        while (n2)
//        {
//            n3 = n2->next;
//            n2->next = n1;
//            //迭代
//            n1 = n2;
//            n2 = n3;
//        }
//        return n1;
//    }
//    void reorderList(ListNode* head) {
//        if (head == nullptr || head->next == nullptr) return;
//        //1.利用快慢双指针找到中间节点
//        ListNode* slow = head, * fast = head;
//        while (fast && fast->next)
//        {
//            slow = slow->next;
//            fast = fast->next->next;
//        }
//        //2.把中间节点后面的节点进行反转
//        ListNode* right = ReverseList(slow->next);  //记录右边的头
//        slow->next = nullptr;
//        //3.合并两个链表
//        ListNode* guard = new ListNode(0);
//        ListNode* left = head, * tail = guard;
//        while (left)
//        {
//            tail->next = left;
//            left = left->next;
//            tail = tail->next;
//            if (right)
//            {
//                tail->next = right;
//                right = right->next;
//                tail = tail->next;
//            }
//        }
//        head = guard->next;
//        delete guard;
//    }
//};


//class Solution {
//    struct cmp
//    {
//        bool operator()(const ListNode* l1, const ListNode* l2)
//        {
//            return l1->val > l2->val;
//        }
//    };
//public:
//    ListNode* MergeTowList(ListNode* l1, ListNode* l2)
//    {
//        ListNode* guard = new ListNode(0);
//        ListNode* tail = guard;
//        while (l1 && l2)
//        {
//            if (l1->val < l2->val)
//            {
//                tail->next = l1;
//                l1 = l1->next;
//            }
//            else
//            {
//                tail->next = l2;
//                l2 = l2->next;
//            }
//            tail = tail->next;
//        }
//        if (l1) tail->next = l1;
//        if (l2) tail->next = l2;
//        return guard->next;
//    }
//    ListNode* mergeKLists(vector<ListNode*>& lists) {
//        if (lists.size() == 0) return nullptr;
//        //利用堆
//        priority_queue<ListNode*, vector<ListNode*>, cmp> q;
//        for (auto& lt : lists)
//            if (lt) q.push(lt);
//        while (q.size() > 1)  //最终合并成一个链表
//        {
//            ListNode* l1 = q.top();
//            q.pop();
//            ListNode* l2 = q.top();
//            q.pop();
//            ListNode* newlist = MergeTowList(l1, l2);
//            q.push(newlist);
//        }
//        return q.size() == 0 ? nullptr : q.top();
//    }
//};


//class Solution {
//    struct cmp
//    {
//        bool operator()(const ListNode* l1, const ListNode* l2)
//        {
//            return l1->val > l2->val;
//        }
//    };
//public:
//    ListNode* mergeKLists(vector<ListNode*>& lists) {
//        if (lists.size() == 0) return nullptr;
//        //利用堆
//        priority_queue<ListNode*, vector<ListNode*>, cmp> q;
//        for (auto& lt : lists)
//            if (lt) q.push(lt);
//        ListNode* guard = new ListNode(0);
//        ListNode* tail = guard;
//        while (q.size())
//        {
//            ListNode* t = q.top();
//            q.pop();
//            tail->next = t;
//            tail = tail->next;
//            if (t->next) q.push(t->next);
//        }
//        tail = guard->next;
//        delete guard;
//        return tail;
//    }
//};


//class Solution {
//public:
//    ListNode* mergeKLists(vector<ListNode*>& lists) {
//        //利用归并思想
//        return Merge(lists, 0, lists.size() - 1);
//    }
//    ListNode* Merge(vector<ListNode*>& lists, int left, int right)
//    {
//        if (left > right) return nullptr;
//        if (left == right) return lists[left];
//        int mid = (left + right) >> 1;
//        ListNode* l1 = Merge(lists, left, mid);
//        ListNode* l2 = Merge(lists, mid + 1, right);
//        //合并两个有序链表
//        return MergeTowList(l1, l2);
//    }
//    ListNode* MergeTowList(ListNode* l1, ListNode* l2)
//    {
//        ListNode* guard = new ListNode(0);
//        ListNode* tail = guard;
//        while (l1 && l2)
//        {
//            if (l1->val < l2->val)
//            {
//                tail->next = l1;
//                l1 = l1->next;
//            }
//            else
//            {
//                tail->next = l2;
//                l2 = l2->next;
//            }
//            tail = tail->next;
//        }
//        if (l1) tail->next = l1;
//        if (l2) tail->next = l2;
//        return guard->next;
//    }
//};


