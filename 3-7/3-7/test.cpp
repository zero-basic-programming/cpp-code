#include <iostream>
#include <vector>
using namespace std;


//class Solution {
//    int ret = 0;
//public:
//    void dfs(TreeNode* root, int sum)
//    {
//        if (root == nullptr) return;
//        if (root->val == sum) ++ret;
//        //遍历左右子树
//        dfs(root->left, sum - root->val);
//        dfs(root->right, sum - root->val);
//    }
//    int FindPath(TreeNode* root, int sum) {
//        if (root == nullptr) return ret;
//        //每个节点都需要遍历，那就暴力的以每一个节点为根节点进行查找
//        dfs(root, sum);
//        FindPath(root->left, sum);
//        FindPath(root->right, sum);
//        return ret;
//    }
//};


//class Solution {
//    unordered_map<int, int> _hash;  //用来记录到当前节点的和值，以及条数
//public:
//    int dfs(TreeNode* root, int sum, int last)
//    {
//        if (root == nullptr) return 0;
//        int res = 0;
//        int tmp = root->val + last;
//        //在hash中查找是否有这个值
//        if (_hash.find(tmp - sum) != _hash.end())
//        {
//            res += _hash[tmp - sum];
//        }
//        //记录当前tmp的路径
//        _hash[tmp]++;
//        //递归左右子树
//        res += dfs(root->left, sum, tmp);
//        res += dfs(root->right, sum, tmp);
//        //回溯，其他的分枝是不需要使用到当前分枝的路径
//        _hash[tmp]--;
//        return res;
//    }
//    int FindPath(TreeNode* root, int sum) {
//        _hash[0] = 1;  //和为0的路径有一条
//        return dfs(root, sum, 0);
//    }
//};


//class Solution {
//public:
//    bool isValid(string s) {
//        //如果是左括号，那么直接入栈，如果是右括号，那么就需要找栈顶元素来匹配
//        int n = s.size();
//        if (n % 2 == 1) return false;  //奇数一定不匹配
//        stack<char> st;
//        for (auto ch : s)
//        {
//            if (ch == '(' || ch == '[' || ch == '{')
//            {
//                st.push(ch);
//            }
//            else
//            {
//                if (st.empty()) return false;
//                char top = st.top();
//                st.pop();
//                if ((ch == ')' && top != '(') ||
//                    (ch == ']' && top != '[') ||
//                    (ch == '}' && top != '{'))
//                    return false;
//            }
//        }
//        return st.empty();
//    }
//};


//class Solution {
//public:
//    TreeNode* invertTree(TreeNode* root) {
//        //把左右子树进行反转即可
//        if (root == nullptr) return nullptr;
//        invertTree(root->left);
//        invertTree(root->right);
//        TreeNode* tmp = root->left;
//        root->left = root->right;
//        root->right = tmp;
//        return root;
//    }
//};


//class Solution {
//    vector<int> ret;
//public:
//    vector<int> rightSideView(TreeNode* root) {
//        //利用层序遍历的思想，每次把队列中的最后一个元素放入到结果集中即可
//        vector<int> ret;
//        if (root == nullptr) return ret;
//        queue<TreeNode*> q;
//        q.push(root);
//        while (q.size())
//        {
//            int sz = q.size();
//            for (int i = 0; i < sz; ++i)
//            {
//                TreeNode* front = q.front();
//                q.pop();
//                if (i == sz - 1)
//                {
//                    ret.push_back(front->val);
//                }
//                //放入其左右子树
//                if (front->left) q.push(front->left);
//                if (front->right) q.push(front->right);
//            }
//        }
//        return ret;
//    }
//};

