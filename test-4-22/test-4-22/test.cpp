#include <iostream>
#include <vector>
using namespace std;

//class Solution {
//public:
//    int rob(TreeNode* root) {
//        vector<int> dp = rob_tree(root);
//        return max(dp[0], dp[1]);
//    }
//    vector<int> rob_tree(TreeNode* root)
//    {
//        //0表示的不偷当前节点，1表示偷当前节点
//        //后序遍历的方式
//        if (root == nullptr) return { 0,0 };
//        vector<int> left = rob_tree(root->left);
//        vector<int> right = rob_tree(root->right);
//        //偷当前节点
//        int val1 = root->val + left[0] + right[0];
//        //不偷当前节点
//        int val2 = max(left[0], left[1]) + max(right[0], right[1]);
//        //放回结果集
//        return { val2,val1 };
//    }
//};


//class Solution {
//public:
//    unordered_map<TreeNode*, int> um;
//    int rob(TreeNode* root) {
//        //使用um来去重
//        if (root == nullptr) return 0;
//        //偷当前节点
//        int val1 = root->val;
//        //如果已经计算过，就不需要重复计算了
//        if (um[root]) return um[root];
//        if (root->left)  val1 += rob(root->left->left) + rob(root->left->right);
//        if (root->right)  val1 += rob(root->right->left) + rob(root->right->right);
//        //不偷当前节点
//        int val2 = rob(root->left) + rob(root->right);
//        //保存一下可能会重复计算的结果
//        um[root] = max(val1, val2);
//        return max(val1, val2);
//    }
//};

//class Solution {
//public:
//    string removeDuplicates(string s) {
//        stack<char> st;
//        for (auto& ch : s)
//        {
//            if (st.empty())
//            {
//                st.push(ch);
//                continue;
//            }
//            //当前元素和栈顶元素相同
//            if (st.top() == ch)   st.pop();
//            else st.push(ch);
//        }
//        string result;
//        while (!st.empty())
//        {
//            result.push_back(st.top());
//            st.pop();
//        }
//        reverse(result.begin(), result.end());
//        return result;
//    }
//};


//class Solution {
//    //比较大小的仿函数
//    class cmp
//    {
//    public:
//        bool operator()(const pair<int, int>& l, const pair<int, int>& r)
//        {
//            return l.second < r.second;
//        }
//    };
//public:
//    vector<int> topKFrequent(vector<int>& nums, int k) {
//        priority_queue<pair<int, int>, vector<pair<int, int>>, cmp> _pq;
//        unordered_map<int, int> _umap;
//        for (auto& e : nums)
//        {
//            _umap[e]++;
//        }
//        //把hash中的数据放入pq中比较大小
//        for (auto& e : _umap)
//        {
//            _pq.push(e);
//        }
//        //放入结果集中
//        vector<int> result(k);
//        for (int i = k - 1; i >= 0; i--) {
//            result[i] = _pq.top().first;
//            _pq.pop();
//        }
//        return result;
//    }
//};


