#include <iostream>
#include <string>
using namespace std;

//int main() {
//    string a, b;
//    while (cin >> a >> b)
//    {
//        bool flag = true;
//        //使用hash方法
//        char hash[26] = { 0 };
//        for (auto& ch : a)
//        {
//            hash[ch - 'A']++;
//        }
//        //再从b中去减hash中的字符，如果小于0，就说明是错误的
//        for (auto& ch : b)
//        {
//            if (hash[ch - 'A'] == 0)
//            {
//                cout << "No" << endl;
//                flag = false;
//                break;
//            }
//            hash[ch - 'A']--;
//        }
//        if (flag)
//            cout << "Yes" << endl;
//    }
//    return 0;
//}


#include <iostream>
#include <vector>
#include <string>
#include <algorithm>
using namespace std;

//这里应该用unorder_map才能解决问题
int main() {
    int n, k;
    vector<string> vs;
    cin >> n;
    while (n--)
    {
        string tmp;
        cin >> tmp;
        vs.push_back(tmp);
    }
    sort(vs.begin(), vs.end());
    string x;
    cin >> x >> k;
    //使用hash方法来统计x的个数
    char hash[128] = { 0 };
    for (auto& ch : x)
    {
        hash[ch]++;
    }
    //在vs中找兄弟字符，并找到第k个兄弟字符
    int count = 0;
    for (auto& str : vs) {
        string tmp(hash);
        for (auto& ch : str)
        {
            if (tmp[ch] == 0)
            {
                //说明当前单词不满足条件
                continue;
            }
        }
        if (str != x) //相同的单词不满足
            ++count;
    }
    //排查第k是否满足
    for (auto& ch : vs[k])
    {
        if (hash[ch] == 0)
        {
            //这里说明第k个不满足
            cout << count << endl;
            return 0;
        }
        hash[ch]--;
    }
    cout << count << endl;
    cout << vs[k] << endl;
    return 0;
}



