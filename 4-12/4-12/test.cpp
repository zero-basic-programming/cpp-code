#include <iostream>
#include <vector>
#include <stack>
using namespace std;

void Qsort(vector<int>& arr, int l, int r)
{
	if (l >= r) return;
	int key = arr[rand() % (r - l + 1) + l];
	int left = l - 1, right = r + 1;
	int i = l;
	while (i < right)
	{
		if (arr[i] < key) swap(arr[i++], arr[++left]);
		else if (arr[i] == key) ++i;
		else swap(arr[i], arr[--right]);
	}
	Qsort(arr, l, left);
	Qsort(arr, right, r);
}

void QsortNonR(vector<int>& arr, int l, int r)
{
	stack<int> st;
	st.push(r);
	st.push(l);
	while (st.size())
	{
		int ll = st.top(); st.pop();
		int rr = st.top(); st.pop();
		int key = arr[rand() % (rr - ll + 1) + ll];
		int left = ll - 1, right = rr + 1;
		int i = ll;
		while (i < right)
		{
			if (arr[i] < key) swap(arr[i++], arr[++left]);
			else if (arr[i] == key) ++i;
			else swap(arr[i], arr[--right]);
		}
		if (left > ll)
		{
			st.push(left);
			st.push(ll);
		}
		if (right < rr)
		{
			st.push(rr);
			st.push(right);
		}

	}
}


void MergeSort(vector<int>& arr, int left, int right, vector<int>& tmp)
{
	if (left >= right) return;
	int mid = left + (right - left) / 2;
	MergeSort(arr, left, mid, tmp);
	MergeSort(arr, mid+1, right, tmp);
	int begin1 = left, end1 = mid;
	int begin2 = mid + 1, end2 = right;
	int i = left;
	while (begin1 <= end1 && begin2 <= end2)
	{
		tmp[i++] = arr[begin1] < arr[begin2] ? arr[begin1++] : arr[begin2++];
	}
	while (begin1 <= end1) tmp[i++] = arr[begin1++];
	while (begin2 <= end2) tmp[i++] = arr[begin2++];
	for (int j = left; j <= right; ++j)
		arr[j] = tmp[j];
}

void MergeSortNonR(vector<int>& arr, vector<int>& tmp)
{
	int n = arr.size();
	int gap = 1;
	while (gap < n)
	{
		for (int i = 0; i < n; i += 2 * gap)
		{
			int begin1 = i, end1 = i + gap - 1;
			int begin2 = i + gap, end2 = i + 2 * gap - 1;
			//判断边界情况
			if (begin2 >= n) break;  //第二组没有元素，此时这两组无需比较
			if (end2 >= n) end2 = n - 1;  //最后一组元素不全
			//单次的归并排序
			int j = i, k = i;  //j用来操作临时数组，k用来还原数组
			while (begin1 <= end1 && begin2 <= end2)
			{
				tmp[j++] = arr[begin1] < arr[begin2] ? arr[begin1++] : arr[begin2++];
			}
			while (begin1 <= end1) tmp[j++] = arr[begin1++];
			while (begin2 <= end2) tmp[j++] = arr[begin2++];
			//还原
			for (; k <= end2; ++k) arr[k] = tmp[k];
		}
		gap *= 2;
	}
}

void AdjustDown(vector<int>& arr, int parent, int n)
{
	int child = 2 * parent + 1;
	while (child < n)
	{
		if (child + 1 < n && arr[child] < arr[child + 1])
		{
			++child;
		}
		if (arr[child] > arr[parent])
		{
			swap(arr[child], arr[parent]);
			parent = child;
			child = 2 * parent + 1;
		}
		else break;  //已成堆
	}
}

//排升序，建大堆
void HeapSort(vector<int>& arr)
{
	int n = arr.size();
	//向下调整建堆
	for (int i = (n - 1 - 1) / 2; i >= 0; --i)
	{
		AdjustDown(arr, i, n);
	}
	for (int i = 1; i < n; ++i)
	{
		swap(arr[0], arr[n - i]);
		AdjustDown(arr, 0, n - i);
	}
}


void BufferSort(vector<int>& arr)
{
	int n = arr.size();
	for (int i = 0; i < n - 1; ++i)  //趟数
	{
		bool flag = true;  //用于冒泡排序优化
		for (int j = 1; j < n - i; ++j)
		{
			if (arr[j] < arr[j - 1])
			{
				swap(arr[j], arr[j - 1]);
				flag = false;
			}
		}
		if (flag) break;
	}
}


void SelectSort(vector<int>& arr)
{
	int n = arr.size();
	int left = 0, right = n - 1;
	while (left < right)
	{
		int maxi = left, mini = left;
		for (int i = left; i <= right; ++i)
		{
			if (arr[maxi] < arr[i]) maxi = i;
			if (arr[mini] > arr[i]) mini = i;
		}
		swap(arr[maxi], arr[right]);
		//这里有可能最小值在right位置
		if (mini == right) mini = maxi;
		swap(arr[mini], arr[left]);
		++left;
		--right;
	}
}


void InsertSort(vector<int>& arr)
{
	int n = arr.size();
	for (int i = 0; i < n - 1; ++i)
	{
		int end = i;
		int key = arr[end+1];
		while (end >= 0)
		{
			if (key < arr[end])
			{
				arr[end+1] = arr[end];
				--end;
			}
			else break;
		}
		arr[end+1] = key;
	}
}

void ShellSort(vector<int>& arr)
{
	int n = arr.size();
	int gap = n;
	while (gap > 1)
	{
		gap /= 2;
		for (int i = 0; i < n - gap; i++)
		{
			int end = i;
			int key = arr[end + gap];
			while (end >= 0)
			{
				if (key < arr[end])
				{
					arr[end + gap] = arr[end];
					end -= gap;
				}
				else break;
			}
			arr[end + gap] = key;
		}
	}
}

int main()
{
	vector<int> arr = { 2,3,5,3,4,7,6,45,6,0,9,8,7 };
	int n = arr.size();
	vector<int> tmp(n);
	//Qsort(arr, 0, n - 1);
	//MergeSort(arr, 0, n - 1, tmp);
	//MergeSortNonR(arr, tmp);
	//QsortNonR(arr, 0, n - 1);
	//HeapSort(arr);
	//BufferSort(arr);
	//SelectSort(arr);
	//InsertSort(arr);
	ShellSort(arr);
	for (auto e : arr)
	{
		cout << e << " ";
	}
	return 0;
}