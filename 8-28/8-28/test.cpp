#include <iostream>
#include <vector>
using namespace std;


//class Solution {
//public:
//    Node* insert(Node* head, int insertVal) {
//        //如果头是空直接插入即可，如果不是空，就从头节点开始找到小于他的数，同时后面是大于他的数
//        if (head == nullptr)
//        {
//            head = new Node(insertVal);
//            head->next = head;
//            return head;
//        }
//        else if (head->next == head)
//        {
//            //只有一个元素的情况
//            Node* tmp = new Node(insertVal);
//            head->next = tmp;
//            tmp->next = head;
//            return head;
//        }
//        Node* prev = head, * cur = head->next;
//        while (cur != head)
//        {
//            if (prev->val <= insertVal && insertVal <= cur->val) break;
//            if (prev->val > cur->val)
//            {
//                //折线
//                if (insertVal >= prev->val || insertVal <= cur->val) break;
//            }
//            prev = cur;
//            cur = cur->next;
//        }
//        Node* tmp = new Node(insertVal);
//        tmp->next = cur;
//        prev->next = tmp;
//        return head;
//    }
//};


//class Solution {
//public:
//    int minFlipsMonoIncr(string s) {
//        //dp[i][0] :以i位置为结尾并且最终修改为0的最小修改次数
//        //dp[i][1] :以i位置为结尾并且最终修改为1的最小修改次数
//        int n = s.size();
//        vector<int> zero(n), one(n);
//        //初始化
//        zero[0] = s[0] == '0' ? 0 : 1;
//        one[0] = s[0] == '1' ? 0 : 1;
//        for (int i = 1; i < n; ++i)
//        {
//            zero[i] = zero[i - 1] + (s[i] == '0' ? 0 : 1);
//            one[i] = min(zero[i - 1], one[i - 1]) + (s[i] == '1' ? 0 : 1);   //因为这里可以是升序
//        }
//        return min(zero[n - 1], one[n - 1]);
//    }
//};


