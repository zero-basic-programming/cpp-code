#include <iostream>
#include <vector>
using namespace std;

//class Solution {
//public:
//    vector<int> missingTwo(vector<int>& nums) {
//        int n = nums.size() + 2;
//        int ret = 0;
//        for (auto e : nums) ret ^= e;
//        for (int i = 1; i <= n; ++i) ret ^= i;
//        //通过获取ret中1的位置的，讲他们分成两组
//        int i = 0;
//        for (; i < 32; ++i)
//        {
//            if ((ret >> i) & 1 == 1) break;
//        }
//        vector<int> v1, v2;
//        for (auto e : nums)
//        {
//            if ((e >> i) & 1 == 1)
//                v1.push_back(e);
//            else v2.push_back(e);
//        }
//        for (int j = 1; j <= n; ++j)
//        {
//            if ((j >> i) & 1 == 1)
//                v1.push_back(j);
//            else v2.push_back(j);
//        }
//        //收集结果
//        int ret1 = 0, ret2 = 0;
//        for (auto e : v1) ret1 ^= e;
//        for (auto e : v2) ret2 ^= e;
//        return { ret1,ret2 };
//    }
//};


class Solution {
public:
    int findNumberOfLIS(vector<int>& nums) {
        int n = nums.size();
        //分别用来记录最长长度，和最长长度的个数
        vector<int> len(n, 1), count(n, 1);
        int sum = 1, countlen = 1;
        for (int i = 1; i < n; ++i)
        {
            for (int j = 0; j < i; ++j)
            {
                if (nums[j] < nums[i])
                {
                    if (len[j] + 1 == len[i]) count[i] += count[j];
                    else if (len[j] + 1 > len[i])
                    {
                        len[i] = len[j] + 1;
                        count[i] = count[j];
                    }
                }
            }
            //每次填完一次表就确定一次结果
            if (sum == len[i]) countlen += count[i];
            else if (sum < len[i]) {
                sum = len[i];
                countlen = count[i];
            }
        }
        return countlen;
    }
};