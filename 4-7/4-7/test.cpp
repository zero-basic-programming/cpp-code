#include <iostream>
#include <vector>
using namespace std;

//class Solution {
//public:
//    int countSubstrings(string s) {
//        int n = s.size();
//        vector<vector<bool>> dp(n, vector<bool>(n, false));
//        int ret = 0;
//        for (int i = n - 1; i >= 0; --i)
//        {
//            for (int j = i; j < n; ++j)
//            {
//                if (s[i] == s[j])
//                {
//                    dp[i][j] = i + 1 < j ? dp[i + 1][j - 1] : true;
//                }
//                if (dp[i][j]) ++ret;
//            }
//        }
//        return ret;
//    }
//};


//class Solution {
//public:
//    string longestPalindrome(string s) {
//        int n = s.size();
//        vector<vector<bool>> dp(n, vector<bool>(n, false));
//        int left = 0, len = 0;
//        for (int i = n; i >= 0; --i)
//        {
//            for (int j = i; j < n; ++j)
//            {
//                if (s[i] == s[j])
//                    dp[i][j] = i + 1 < j ? dp[i + 1][j - 1] : true;
//                if (dp[i][j] && j - i + 1 > len)
//                {
//                    left = i;
//                    len = j - i + 1;
//                }
//            }
//        }
//        return s.substr(left, len);
//    }
//};


//class Solution {
//public:
//    string longestPalindrome(string s) {
//        //中心扩展算法
//        int n = s.size();
//        if (n == 1) return s;
//        int begin = 0, len = 0;
//        for (int i = 0; i < n; ++i)
//        {
//            //奇数位统计
//            int left = i, right = i;
//            while (left >= 0 && right < n && s[left] == s[right])
//            {
//                ++right;
//                --left;
//            }
//            if (right - left - 1 > len)
//            {
//                begin = left + 1;
//                len = right - left - 1;
//            }
//            //偶数位统计
//            left = i, right = i + 1;
//            while (left >= 0 && right < n && s[left] == s[right])
//            {
//                ++right;
//                --left;
//            }
//            if (right - left - 1 > len)
//            {
//                begin = left + 1;
//                len = right - left - 1;
//            }
//        }
//        return s.substr(begin, len);
//    }
//};



//class Solution {
//public:
//    bool checkPartitioning(string s) {
//        int n = s.size();
//        vector<vector<bool>> dp(n, vector<bool>(n, false));
//        //使用dp来统计出以i为起始，j为末尾的下标的字串是否是回文
//        for (int i = n - 1; i >= 0; --i)
//        {
//            for (int j = i; j < n; ++j)
//            {
//                if (s[i] == s[j]) dp[i][j] = i + 1 < j ? dp[i + 1][j - 1] : true;
//            }
//        }
//        //将数组分成3段，来找时候满足条件
//        for (int i = 1; i < n - 1; ++i)
//        {
//            for (int j = i; j < n - 1; ++j)
//            {
//                if (dp[0][i - 1] && dp[i][j] && dp[j + 1][n - 1]) return true;
//            }
//        }
//        return false;
//    }
//};


//class Solution {
//public:
//    int minCut(string s) {
//        const int INF = 0x3f3f3f3f;
//        int n = s.size();
//        vector<vector<bool>> vis(n, vector<bool>(n, false));
//        for (int i = n - 1; i >= 0; --i)
//        {
//            for (int j = i; j < n; ++j)
//            {
//                if (s[i] == s[j]) vis[i][j] = i + 1 < j ? vis[i + 1][j - 1] : true;
//            }
//        }
//        vector<int> dp(n, INF);
//        for (int i = 0; i < n; ++i)
//        {
//            if (vis[0][i]) dp[i] = 0;
//            else
//            {
//                for (int j = 1; j <= i; ++j)
//                {
//                    if (vis[j][i]) dp[i] = min(dp[i], dp[j - 1] + 1);
//                }
//            }
//        }
//        return dp[n - 1];
//    }
//};


//class Solution {
//public:
//    int longestPalindromeSubseq(string s) {
//        int n = s.size();
//        vector<vector<int>> dp(n, vector<int>(n, 1));
//        for (int i = n - 1; i >= 0; --i)
//        {
//            for (int j = i; j < n; ++j)
//            {
//                if (s[i] == s[j])
//                {
//                    if (i + 1 >= j) dp[i][j] = j - i + 1;
//                    else dp[i][j] = dp[i + 1][j - 1] + 2;
//                }
//                else dp[i][j] = max(dp[i + 1][j], dp[i][j - 1]);
//            }
//        }
//        return dp[0][n - 1];
//    }
//};


//class Solution {
//public:
//    int minInsertions(string s) {
//        int n = s.size();
//        vector<vector<int>> dp(n, vector<int>(n));
//        for (int i = n - 1; i >= 0; --i)
//        {
//            for (int j = i + 1; j < n; ++j)
//            {
//                if (s[i] == s[j]) dp[i][j] = dp[i + 1][j - 1];
//                else dp[i][j] = min(dp[i + 1][j], dp[i][j - 1]) + 1;
//            }
//        }
//        return dp[0][n - 1];
//    }
//};


vector<int> GetNext(string& t)
{
	vector<int> next = { 0 };
	int prefix_len = 0, i = 1;
	while (i < t.size())
	{
		if (t[prefix_len] == t[i])
		{
			++prefix_len;
			next.push_back(prefix_len);
			++i;
		}
		else
		{
			if (prefix_len == 0)
			{
				//此时说明已经没有前缀了，这时候直接收集结果
				next.push_back(0);
				++i;
			}
			else
			{
				//通过前缀来获取到其有可能有的更优的结果
				prefix_len = next[prefix_len - 1];
			}
		}
	}
	for (auto e : next) cout << e << " ";
	return next;
}

int main()
{
	string s = "abababcaa";
	string t = "ababc";
	vector<int> next = GetNext(t);
	//找出相同的数组的起始下标，如果没有相同就是-1
	int i = 0, j = 0;
	while (i < s.size())
	{
		if (s[i] == t[j])
		{
			++i;
			++j;
		}
		else
		{
			j = next[j - 1];
		}
		if (j == t.size()-1) break;
	}
	cout << i << endl;
	return 0;
}