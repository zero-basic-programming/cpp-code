#include <iostream>
#include <vector>
#include <string>
using namespace std;


//class Solution {
//    vector<string> ret;
//    string path;
//    string hash[10] = { "","","abc","def","ghi","jkl","mno","pqrs","tuv","wxyz" };
//public:
//    void dfs(string& digits, int m)
//    {
//        //返回
//        if (path.size() == digits.size())
//        {
//            ret.push_back(path);
//            return;
//        }
//        for (auto s : hash[digits[m] - '0'])
//        {
//            path += s;
//            dfs(digits, m + 1);
//            //回溯
//            path.pop_back();
//        }
//
//    }
//    vector<string> letterCombinations(string digits) {
//        if (digits.size() == 0) return ret;
//        dfs(digits, 0);
//        return ret;
//    }
//};

void Qsort(vector<int>& arr, int l, int r)
{
	if (l >= r) return;
	//选取基准点
	int key = arr[(rand() % (r - l + 1) + l)];
	int left = l - 1, right = r + 1, i = l;
	while (i < right)
	{
		if (key > arr[i]) swap(arr[++left], arr[i++]);
		else if (key == arr[i]) ++i;
		else swap(arr[--right], arr[i]);
	}
	Qsort(arr, l, left);
	Qsort(arr, right, r);
}

int main()
{
	srand(time(nullptr));
	vector<int> arr = { 1,3,2,5,6,3,7,8,9,5 };
	int n = arr.size();
	Qsort(arr, 0, n - 1);
	for (auto e : arr)
	{
		cout << e << " ";
	}
	return 0;
}