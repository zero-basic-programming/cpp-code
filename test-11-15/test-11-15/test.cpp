#include <iostream>
#include <vector>
using namespace std;

//class Solution{
//public:
//    void hanota(vector<int>&a, vector<int>&b, vector<int>&c) {
//        dfs(a,b,c,a.size());
//    }
//    void dfs(vector<int>&a, vector<int>&b, vector<int>&c,int n)  //把a通过b放入到c中
//    {
//        //返回条件
//        if (n == 1)
//        {
//            c.push_back(a.back());
//            a.pop_back();
//            return;
//        }
//        //先把n-1个上面的通过c放入到b
//        dfs(a,c,b,n - 1);
//        //把a中最后那个放入到c中
//        c.push_back(a.back());
//        a.pop_back();
//        //把b中的n-1个借助a放入到c即可
//        dfs(b,a,c,n - 1);
//    }
//};



//class Solution {
//public:
//    ListNode* mergeTwoLists(ListNode* list1, ListNode* list2) {
//        if (list1 == nullptr) return list2;
//        if (list2 == nullptr) return list1;
//        //比较两个值
//        if (list1->val < list2->val)
//        {
//            list1->next = mergeTwoLists(list1->next, list2);
//            return list1;
//        }
//        else
//        {
//            list2->next = mergeTwoLists(list1, list2->next);
//            return list2;
//        }
//    }
//};


//贪心
//class Solution {
//public:
//    int findLengthOfLCIS(vector<int>& nums) {
//        int count = 0;
//        int tmp = INT_MAX; //上一个的递增序列的最后一个
//        int ret = 1;
//        for (auto e : nums)
//        {
//            if (e > tmp)
//            {
//                ++count;
//                tmp = e; //每次更新最后一个值
//                ret = max(ret, count);
//            }
//            else {
//                tmp = e;
//                count = 1;
//            }
//        }
//        return ret;
//    }
//};


//class Solution {
//public:
//    int maxProfit(vector<int>& prices) {
//        int minprice = INT_MAX; //全局区域的最小值
//        int ret = 0; //利润
//        for (auto e : prices)
//        {
//            if (e < minprice) minprice = e;
//            else {
//                ret = max(ret, e - minprice);
//            }
//        }
//        return ret;
//    }
//};


