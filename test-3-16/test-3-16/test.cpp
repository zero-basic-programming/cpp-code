#include <iostream>
using namespace std;
//
//int main()
//{
//    int n = 0;
//    while (cin >> n)
//    {
//        int count = 0;
//        while (n)
//        {
//            ++count;
//            n = n & (n - 1);
//        }
//        cout << count << endl;
//    }
//    return 0;
//}

//#include <string>
//class Person
//{
//public:
//	Person(const char* name = "", int age = 0)
//		:_name(name)
//		, _age(age)
//	{}
//	Person(const Person& p) = delete;
//private:
//	string _name;
//	int _age;
//};
//int main()
//{
//	Person s1;
//	Person s2 = s1;//这里是不能使用拷贝构造的，因为这里已经delete
//	Person s3 = move(s1);
//	return 0;
//}


//// 递归终止函数
//template <class T>
//void ShowList(const T& t)
//{
//	cout << t << endl;
//}
//// 展开函数
//template <class T, class ...Args>
//void ShowList(T value, Args... args)
//{
//	cout << value << " ";
//	ShowList(args...);
//}
//int main()
//{
//	ShowList(1);
//	ShowList(1, 'A');
//	ShowList(1, 'A', std::string("sort"));
//	return 0;
//}

//int main()
//{
//	// 最简单的lambda表达式, 该lambda表达式没有任何意义
//	[] {};
//
//	// 省略参数列表和返回值类型，返回值类型由编译器推导为int
//	int a = 3, b = 4;
//	[=] {return a + 3; };
//
//	// 省略了返回值类型，无返回值类型
//	auto fun1 = [&](int c) {b = a + c; };
//	fun1(10);
//	cout << a << " " << b << endl;
//
//	// 各部分都很完善的lambda函数
//	auto fun2 = [=, &b](int c)->int {return b += a + c; };
//	cout << fun2(10) << endl;
//
//	// 复制捕捉x
//	int x = 10;
//	auto add_x = [x](int a) mutable { x *= 2; return a + x; };
//	cout << add_x(10) << endl;
//	return 0;
//}


int main()
{
	int a = 1;
	int b = 2;
	auto add = [&](int c) {return b = a + c; };
	add(2);
	return 0;
}