#include <iostream>
#include <vector>
using namespace std;


//class Solution {
//public:
//    vector<int> advantageCount(vector<int>& nums1, vector<int>& nums2) {
//        int n = nums1.size();
//        //开两个下标数组
//        vector<int> index1(n);
//        vector<int> index2(n);
//        for (int i = 0; i < n; ++i)
//        {
//            index1[i] = i;
//            index2[i] = i;
//        }
//        //将index的两个数组通过num1和2进行排序
//        sort(index1.begin(), index1.end(), [&](const int i, const int j) {
//            return nums1[i] < nums1[j];
//            });
//        sort(index2.begin(), index2.end(), [&](const int i, const int j) {
//            return nums2[i] < nums2[j];
//            });
//        //进行比较
//        int i = 0, left = 0, right = n - 1; //left和right表示num2的前后
//        vector<int> ret(n);
//        while (i < n)
//        {
//            if (nums1[index1[i]] > nums2[index2[left]])
//            {
//                //已经是优势
//                ret[index2[left]] = nums1[index1[i]];
//                ++i;
//                ++left;
//            }
//            else
//            {
//                //1中最小的元素去消耗2中最大的元素
//                ret[index2[right]] = nums1[index1[i]];
//                ++i;
//                --right;
//            }
//        }
//        return ret;
//    }
//};


//class Solution {
//public:
//    vector<int> advantageCount(vector<int>& nums1, vector<int>& nums2) {
//        int n = nums1.size();
//        vector<int> index2(n);
//        for (int i = 0; i < n; ++i)  index2[i] = i;
//        sort(nums1.begin(), nums1.end());
//        sort(index2.begin(), index2.end(), [&](const int i, const int j) {
//            return nums2[i] < nums2[j];
//            });
//        //进行比较
//        int i = 0, left = 0, right = n - 1; //left和right表示num2的前后
//        vector<int> ret(n);
//        while (i < n)
//        {
//            if (nums1[i] > nums2[index2[left]])
//            {
//                //已经是优势
//                ret[index2[left++]] = nums1[i++];
//            }
//            else
//            {
//                //1中最小的元素去消耗2中最大的元素
//                ret[index2[right--]] = nums1[i++];
//            }
//        }
//        return ret;
//    }
//};


//class Solution {
//public:
//    int longestPalindrome(string s) {
//        int n = s.size();
//        if (n == 1) return 1; //边界情况
//        bool flag = false; //用来记录单个的字符
//        sort(s.begin(), s.end());
//        int count = 0;
//        int i = 0;
//        while (i < n - 1)
//        {
//            if (s[i] != s[i + 1])
//            {
//                flag = true;
//                ++i;
//            }
//            else {
//                count += 2;
//                i += 2;
//            }
//        }
//        return flag ? count + 1 : count;
//    }
//};



//class Solution {
//public:
//    int longestPalindrome(string s) {
//        int hash[128] = { 0 };
//        for (auto ch : s) hash[ch]++;
//        int ret = 0;
//        for (auto x : hash)
//        {
//            ret += x / 2 * 2;
//        }
//        return ret < s.size() ? ret + 1 : ret;
//    }
//};


//class Solution {
//    int sum = 0;
//public:
//    void dfs(TreeNode* root, int tmp)
//    {
//        //返回
//        if (root->left == nullptr && root->right == nullptr)
//        {
//            //叶子节点
//            tmp *= 10;
//            tmp += root->val;
//            sum += tmp;
//            return;
//        }
//        tmp *= 10;
//        tmp += root->val;
//        if (root->left) dfs(root->left, tmp);
//        if (root->right) dfs(root->right, tmp);
//    }
//    int sumNumbers(TreeNode* root) {
//        dfs(root, 0);
//        return sum;
//    }
//};


