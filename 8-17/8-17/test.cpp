#include <iostream>
#include <vector>
using namespace std;


//class Solution {
//public:
//    int countSubstrings(string s) {
//        //中心扩展算法
//        int n = s.size();
//        int ret = 0;
//        for (int i = 0; i < n; ++i)
//        {
//            int left = i, right = i;   //这里是判断基数个
//            while (left >= 0 && right < n)
//            {
//                if (s[left] == s[right]) ++ret;
//                else break;
//                --left;
//                ++right;
//            }
//            left = i, right = i + 1;
//            while (left >= 0 && right < n)
//            {
//                if (s[left] == s[right]) ++ret;
//                else break;
//                --left;
//                ++right;
//            }
//        }
//        return ret;
//    }
//};



//class Solution {
//public:
//    int countSubstrings(string s) {
//        //使用动态规划
//        int n = s.size();
//        vector<vector<bool>> dp(n, vector<bool>(n, false));
//        int ret = 0;
//        for (int i = n - 1; i >= 0; --i)
//        {
//            for (int j = i; j < n; ++j)
//            {
//                if (s[i] == s[j]) dp[i][j] = i + 1 < j ? dp[i + 1][j - 1] : true;
//                if (dp[i][j]) ++ret;
//            }
//        }
//        return ret;
//    }
//};


