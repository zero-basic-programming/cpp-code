#pragma once
#include <iostream>
#include <bitset>
#include <string>
#include <time.h>
#include <vector>
using namespace std;


namespace liang
{
	//多个哈希函数来确定每个元素有多个不同的位置
	struct BKDRHash
	{
		size_t operator()(const string& key)
		{
			size_t hash = 0;
			for (auto ch : key)
			{
				hash *= 131;
				hash += ch;
			}
			return hash;
		}
	};

	struct APHash
	{
		size_t operator()(const string& key)
		{
			unsigned int hash = 0;
			int i = 0;

			for (auto ch : key)
			{
				if ((i & 1) == 0)
				{
					hash ^= ((hash << 7) ^ (ch) ^ (hash >> 3));
				}
				else
				{
					hash ^= (~((hash << 11) ^ (ch) ^ (hash >> 5)));
				}

				++i;
			}

			return hash;
		}
	};

	struct DJBHash
	{
		size_t operator()(const string& key)
		{
			unsigned int hash = 5381;

			for (auto ch : key)
			{
				hash += (hash << 5) + ch;
			}

			return hash;
		}
	};

	struct JSHash
	{
		size_t operator()(const string& s)
		{
			size_t hash = 1315423911;
			for (auto ch : s)
			{
				hash ^= ((hash << 5) + ch + (hash >> 2));
			}
			return hash;
		}
	};
	//布隆实现
	template<size_t N,size_t X = 6,class K = string,class HashFunc1 = BKDRHash,
		class HashFunc2 = APHash,class HashFunc3 = DJBHash, class HashFunc4 = JSHash>
	class BloomFilter
	{
	public:
		void set(const K& key)
		{
			//通过hash函数映射找到不同的位置
			size_t hash1 = HashFunc1()(key) % (N * X);
			size_t hash2 = HashFunc2()(key) % (N * X);
			size_t hash3 = HashFunc3()(key) % (N * X);
			size_t hash4 = HashFunc4()(key) % (N * X);
			//改变位图
			_bf.set(hash1);
			_bf.set(hash2);
			_bf.set(hash3);
			_bf.set(hash4);
		}
		//测试是否存在
		bool test(const K& key)
		{
			//每次都test，如果没找到就一定不存在，如果找到了可能是冲突
			size_t hash1 = HashFunc1()(key) % (N * X);
			if (!_bf.test(hash1))
			{
				return false;
			}
			size_t hash2 = HashFunc2()(key) % (N * X);
			if (!_bf.test(hash2))
			{
				return false;
			}
			size_t hash3 = HashFunc3()(key) % (N * X);
			if (!_bf.test(hash3))
			{
				return false;
			}
			size_t hash4 = HashFunc4()(key) % (N * X);
			if (!_bf.test(hash4))
			{
				return false;
			}
			return true;
		}
	private:
		bitset<N* X> _bf;
	};

	void testbloomfilter1()
	{
		string arr[] = { "string","sort","but","however","1but","but1","b1ut","bu1t" };
		BloomFilter<10> bf;
		for (auto& str : arr)
		{
			bf.set(str);
		}
		for (auto& str : arr)
		{
			cout << bf.test(str) << endl;
		}
		srand(time(0));
		for (const auto& s : arr)
		{
			//都是必定不存在的
			cout << bf.test(s + to_string(rand())) << endl;
		}
	}
	void testbloomfilter2()
	{
		srand(time(0));
		const size_t N = 100000;
		BloomFilter<N> bf;

		vector<string> v1;
		string url = "https://www.cnblogs.com/-clq/archive/2012/05/31/2528153.html";

		for (size_t i = 0; i < N; ++i)
		{
			v1.push_back(url + to_string(i));
		}

		for (auto& str : v1)
		{
			bf.set(str);
		}

		// v2跟v1是相似字符串集，但是不一样
		vector<string> v2;
		for (size_t i = 0; i < N; ++i)
		{
			std::string url = "https://www.cnblogs.com/-clq/archive/2012/05/31/2528153.html";
			url += to_string(999999 + i);
			v2.push_back(url);
		}

		size_t n2 = 0;
		for (auto& str : v2)
		{
			if (bf.test(str))
			{
				++n2;
			}
		}
		cout << "相似字符串误判率:" << (double)n2 / (double)N << endl;

		// 不相似字符串集
		std::vector<std::string> v3;
		for (size_t i = 0; i < N; ++i)
		{
			string url = "zhihu.com";
			url += std::to_string(i + rand());
			v3.push_back(url);
		}

		size_t n3 = 0;
		for (auto& str : v3)
		{
			if (bf.test(str))
			{
				++n3;
			}
		}
		cout << "不相似字符串误判率:" << (double)n3 / (double)N << endl;
	}
}
	
