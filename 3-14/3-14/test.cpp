#include <iostream>
#include <vector>
using namespace std;


//class Solution {
//public:
//    int minCost(vector<vector<int>>& costs) {
//        int n = costs.size();
//        vector<vector<int>> dp(n, vector<int>(3));
//        //初始化
//        dp[0][0] = costs[0][0];
//        dp[0][1] = costs[0][1];
//        dp[0][2] = costs[0][2];
//        for (int i = 1; i < n; ++i)
//        {
//            dp[i][0] = min(dp[i - 1][1], dp[i - 1][2]) + costs[i][0];
//            dp[i][1] = min(dp[i - 1][0], dp[i - 1][2]) + costs[i][1];
//            dp[i][2] = min(dp[i - 1][0], dp[i - 1][1]) + costs[i][2];
//        }
//        return min(dp[n - 1][0], min(dp[n - 1][1], dp[n - 1][2]));
//    }
//};


//class Solution {
//public:
//    int maxProfit(vector<int>& prices) {
//        int n = prices.size();
//        vector<vector<int>> dp(n, vector<int>(3));
//        //初始化
//        dp[0][0] = -prices[0];
//        for (int i = 1; i < n; ++i)
//        {
//            dp[i][0] = max(dp[i - 1][0], dp[i - 1][2] - prices[i]);
//            dp[i][1] = dp[i - 1][0] + prices[i];
//            dp[i][2] = max(dp[i - 1][1], dp[i - 1][2]);
//        }
//        return max(dp[n - 1][0], max(dp[n - 1][1], dp[n - 1][2]));
//    }
//};


//class Solution {
//public:
//    int maxProfit(vector<int>& prices, int fee) {
//        int n = prices.size();
//        vector<int> f(n);
//        auto g = f;
//        //初始化
//        f[0] = -prices[0];
//        for (int i = 1; i < n; ++i)
//        {
//            f[i] = max(f[i - 1], g[i - 1] - prices[i]);
//            g[i] = max(g[i - 1], f[i - 1] + prices[i] - fee);
//        }
//        return max(f[n - 1], g[n - 1]);
//    }
//};


//class Solution {
//public:
//    int maxProfit(vector<int>& prices) {
//        int n = prices.size();
//        if (n == 1) return 0;
//        int i = 0;
//        int maxp = 0, secondp = 0;
//        int minprice = prices[0];
//        while (i < n - 1)
//        {
//            if (prices[i] > prices[i + 1])
//            {
//                //收集结果
//                int profit = prices[i] - minprice;
//                if (profit > maxp)
//                {
//                    secondp = maxp;
//                    maxp = profit;
//                }
//                else if (profit > secondp) secondp = profit;
//                //更新minp
//                minprice = prices[i + 1];
//            }
//            ++i;
//        }
//        //最后一段可能是上升的没有收集到结果
//        if (prices[n - 1] > prices[n - 2])
//        {
//            int profit = prices[n - 1] - minprice;
//            if (profit > maxp)
//            {
//                secondp = maxp;
//                maxp = profit;
//            }
//            else if (profit > secondp) secondp = profit;
//        }
//        return maxp + secondp;
//    }
//};


//class Solution {
//public:
//    int maxProfit(vector<int>& prices) {
//        int n = prices.size();
//        const int INF = -0x3f3f3f3f;  //后续需要比较最大值，需要提前初始化
//        vector<vector<int>> f(n, vector<int>(3, INF));
//        auto g = f;
//        //初始化
//        f[0][0] = -prices[0];
//        g[0][0] = 0;
//        for (int i = 1; i < n; ++i)
//        {
//            for (int j = 0; j < 3; ++j)  //因为最多只能有两笔交易
//            {
//                f[i][j] = max(f[i - 1][j], g[i - 1][j] - prices[i]);
//                g[i][j] = g[i - 1][j];
//                //考虑到这里j必须大于1，也就是说至少完成一笔交易才能填写这个位置
//                if (j > 0)
//                    g[i][j] = max(g[i][j], f[i - 1][j - 1] + prices[i]);
//            }
//        }
//        //最后收集第0到2列的数据,对应的是交易多少次
//        int ret = INF;
//        for (int i = 0; i < 3; ++i)
//        {
//            ret = max(ret, g[n - 1][i]);
//        }
//        return ret;
//    }
//};


//class Solution {
//    const int INF = 0x3f3f3f3f;
//public:
//    int maxProfit(int k, vector<int>& prices) {
//        int n = prices.size();
//        vector<vector<int>> f(n, vector<int>(k + 1, -INF));
//        auto g = f;
//        //初始化
//        f[0][0] = -prices[0];
//        g[0][0] = 0;
//        for (int i = 1; i < n; ++i)
//        {
//            for (int j = 0; j < k + 1; ++j)
//            {
//                f[i][j] = max(f[i - 1][j], g[i - 1][j] - prices[i]);
//                g[i][j] = g[i - 1][j];
//                //交易次数最少要1次才能用f来更行g
//                if (j > 0)
//                    g[i][j] = max(g[i][j], f[i - 1][j - 1] + prices[i]);
//            }
//        }
//        //收集0到k次的最大值
//        int ret = -INF;
//        for (int j = 0; j < k + 1; ++j)
//        {
//            ret = max(ret, g[n - 1][j]);
//        }
//        return ret;
//    }
//};

#include <unordered_map>

//class Solution {
//public:
//    vector<int> findSubstring(string s, vector<string>& words) {
//        vector<int> ret;
//        int n = words.size();
//        int m = s.size();
//        unordered_map<string, int> hash;  //key是字符串 val是次数
//        for (auto& str : words) hash[str]++;
//        int sz = words[0].size();
//        int left = 0, right = 0;
//        string tmp;  //用来记录当前的字符串
//        //窗口内的结果一定是满足条件的
//        while (right < m)
//        {
//            //进窗口 
//            tmp += s[right];
//            //判断
//            if (tmp.size() == sz)
//            {
//                //判断hash中是否有tmp
//                if (hash[tmp] == 0)
//                {
//                    //出窗口 , 这里有两种情况
//                    //1.有tmp，但是已经出现过了
//                    //2.没有tmp，那我们就可以把前面的都出窗口
//                    while (hash[tmp] == 0 && left < right)
//                    {
//                        string lefttmp = s.substr(left, sz);
//                        ++hash[lefttmp];
//                        left += sz;
//                    }
//                }
//                else    hash[tmp]--;
//                tmp.clear();  //满足一次长度就需要清空一次
//            }
//            //收集结果
//            if (right - left + 1 == sz * n)
//                ret.push_back(left);
//            ++right;
//        }
//        return ret;
//    }
//};
//
//
//int main()
//{
//    vector<string> vs = { "word","good","best","word" };
//    Solution().findSubstring("wordgoodgoodgoodbestword", vs);
//    return 0;
//}

//class Solution {
//public:
//    vector<int> findSubstring(string s, vector<string>& words) {
//        int n = words.size();
//        int len = words[0].size();
//        unordered_map<string, int> hash1; //用来记录words中的字符串的映射关系
//        vector<int> ret;
//        for (auto& str : words) hash1[str]++;
//        //从某个位置开始
//        for (int i = 0; i < len; ++i)
//        {
//            unordered_map<string, int> hash2; //用来记录s中有的字符串的个数
//            for (int left = i, right = i, count = 0; right + len <= s.size(); right += len)
//            {
//                //进窗口
//                string in = s.substr(right, len);
//                ++hash2[in];
//                if (hash1.count(in) && hash2[in] <= hash1[in]) ++count;
//                //判断
//                if (right - left + 1 > len * n)
//                {
//                    //出窗口
//                    string out = s.substr(left, len);
//                    if (hash1.count(out) && hash2[out] <= hash1[out]) --count;
//                    --hash2[out];
//                    left += len;   //更新len
//                }
//                //收集结果
//                if (count == n) ret.push_back(left);
//            }
//        }
//        return ret;
//    }
//};


