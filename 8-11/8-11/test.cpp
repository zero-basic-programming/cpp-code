#include <iostream>
#include <vector>
#include <unordered_map>
using namespace std;


class Solution {
public:
    int subarraySum(vector<int>& nums, int k) {
        //前缀和 + hash
        int n = nums.size();
        vector<int> dp(n + 1);   //前缀和数组,这里空一个位置，是为了后面在处理前缀和的时候相等的情况
        for (int i = 1; i <= n; ++i)  dp[i] = dp[i - 1] + nums[i - 1];
        //在遍历的过程中加入hash
        unordered_map<int, int> hash;   // 映射前缀和与次数的关系
        hash[0] = 1;   //当和为0的时候，不选就是一个结果
        int ret = 0;
        for (int i = 1; i <= n; ++i)
        {
            int gap = dp[i] - k;   //在hash中需要找的值
            if (hash.find(gap) != hash.end())
            {
                //可以找到，就收集结果
                ret += hash[gap];
            }
            //将当前的前缀和放入到hash中
            ++hash[dp[i]];
        }
        return ret;
    }
};


int main()
{
    vector<int> nums = { 1,1,1 };
    Solution().subarraySum(nums, 2);
    return 0;
}