#include <iostream>
#include <vector>
using namespace std;

//class Solution {
//public:
//    void moveZeroes(vector<int>& nums) {
//        //前后指针法，模拟快排思想
//        int head = 0;
//        int tail = 0;
//        while (tail < nums.size())
//        {
//            //tail往后找非0元素，head找0元素，交换即可
//            if (nums[tail] != 0)
//                swap(nums[head++], nums[tail]);
//            ++tail;
//        }
//    }
//};

//class Solution {
//public:
//    void moveZeroes(vector<int>& nums) {
//        //前后指针思想
//        for (int dest = -1, cur = 0; cur < nums.size(); ++cur)
//        {
//            if (nums[cur])
//            {
//                //处理非0元素
//                swap(nums[++dest], nums[cur]);
//            }
//        }
//    }
//};


//class Solution {
//public:
//    void duplicateZeros(vector<int>& arr) {
//        for (int cur = 0; cur < arr.size(); ++cur)
//        {
//            //遇到0才进行处理
//            if (arr[cur] == 0)
//            {
//                //后面的数向右平移
//                int end = arr.size() - 1;
//                for (int i = end - 1; i > cur; --i)
//                {
//                    arr[i + 1] = arr[i];
//                }
//                //在cur+1的位置补上0,要防止越界
//                if (cur + 1 < arr.size())
//                    arr[cur + 1] = 0;
//                ++cur; // 这里是为了跳跃两次
//            }
//        }
//    }
//};

class Solution{
public:
    void duplicateZeros(vector<int>&arr) {
        //找到最后一个复写的位置
        int cur = 0; int dest = -1; int n = arr.size();
        while (cur < n)
        {
            if (arr[cur]) ++dest; //非0情况
            else dest += 2;
            //这里可能已经结束
            if (dest >= n - 1)
                break;
            ++cur;
        }
        //这里有可能最后一个复写的是0而导致越界
        if (dest == n)
        {
            arr[n - 1] = 0;
            dest -= 2;
            cur--;
        }
        //从后向前复写
        while (cur >= 0)
        {
            if (arr[cur])
            {
                arr[dest--] = arr[cur--];
            }
            else {
                arr[dest--] = 0;
                arr[dest--] = 0;
                --cur;
            }
        }
    }
};

