#include <iostream>
#include <vector>
#include <string>
using namespace std;

//int main() {
//    int n;
//    while (scanf("%d\n", &n) != EOF)
//    {
//        vector<string> vs;
//        while (n--)
//        {
//            string tmp;
//            getline(cin, tmp);
//            vs.push_back(tmp);
//        }
//        //在vs中查看，或者空格
//        string result;
//        for (auto& str : vs)
//        {
//            if (str.find(',') != string::npos)
//            {
//                //处理，的情况
//                result += '"';
//                result += str;
//                result += '"';
//                result += ", ";
//            }
//            else if (str.find(' ') != string::npos)
//            {
//                //处理空格情况
//                result += '"';
//                result += str;
//                result += '"';
//                result += ", ";
//            }
//            else {
//                //这里直接+=即可
//                result += str;
//                result += ", ";
//            }
//        }
//        //处理最后一个多出来的， 
//        result.pop_back();
//        result.pop_back();
//        cout << result << endl;
//        //每次使用完vs,和result都要清空
//        vs.clear();
//        result.clear();
//    }
//}


//int main() {
//    vector<long long> v(90);
//    v[0] = 1;
//    v[1] = 2;
//    //dp求出每天的兔子的个数
//    for (int i = 2; i < 90; ++i)
//    {
//        v[i] = v[i - 1] + v[i - 2];
//    }
//    int m;
//    while (cin >> m)
//    {
//        if (m < 3) {
//            cout << m << endl;
//            continue;
//        }
//        //这里就是m大于等于3的情况
//        cout << v[m - 1] << endl;
//    }
//    return 0;
//}


