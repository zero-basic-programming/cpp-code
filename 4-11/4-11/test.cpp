#include <iostream>
#include <vector>
using namespace std;

//int main() {
//    int n, V;
//    cin >> n >> V;
//    vector<int> v(n);
//    vector<int> w(n);
//    for (int i = 0; i < n; ++i) cin >> v[i] >> w[i];
//    vector<vector<int>> dp(n + 1, vector<int>(V + 1));
//    for (int i = 1; i <= n; ++i)
//    {
//        for (int j = 0; j <= V; ++j)
//        {
//            dp[i][j] = dp[i - 1][j];
//            if (j >= v[i - 1])
//                dp[i][j] = max(dp[i][j], dp[i][j - v[i - 1]] + w[i - 1]);
//        }
//    }
//    cout << dp[n][V] << endl;
//    //第2问
//    //第一行要初始化为-1，因为在没有商品的时候不可能装满背包
//    for (int j = 1; j <= V; ++j) dp[0][j] = -1;
//    for (int i = 1; i <= n; ++i)
//    {
//        for (int j = 0; j <= V; ++j)
//        {
//            dp[i][j] = dp[i - 1][j];
//            if (j >= v[i - 1] && dp[i][j - v[i - 1]] != -1)
//                dp[i][j] = max(dp[i][j], dp[i][j - v[i - 1]] + w[i - 1]);
//        }
//    }
//    cout << (dp[n][V] == -1 ? 0 : dp[n][V]) << endl;
//    return 0;
//}



//int main() {
//    int n, V;
//    cin >> n >> V;
//    vector<int> v(n);
//    vector<int> w(n);
//    for (int i = 0; i < n; ++i) cin >> v[i] >> w[i];
//    vector<int> dp(V + 1);
//    for (int i = 1; i <= n; ++i)
//    {
//        for (int j = v[i - 1]; j <= V; ++j)
//        {
//            dp[j] = max(dp[j], dp[j - v[i - 1]] + w[i - 1]);
//        }
//    }
//    cout << dp[V] << endl;
//    //第2问
//    //第一行要初始化为-1，因为在没有商品的时候不可能装满背包
//    for (int j = 1; j <= V; ++j) dp[j] = -1;
//    for (int i = 1; i <= n; ++i)
//    {
//        for (int j = v[i - 1]; j <= V; ++j)
//        {
//            if (dp[j - v[i - 1]] != -1)
//                dp[j] = max(dp[j], dp[j - v[i - 1]] + w[i - 1]);
//        }
//    }
//    cout << (dp[V] == -1 ? 0 : dp[V]) << endl;
//    return 0;
//}


//class Solution {
//public:
//    int coinChange(vector<int>& coins, int amount) {
//        int n = coins.size();
//        const int INF = 0x3f3f3f3f;
//        vector<vector<int>> dp(n + 1, vector<int>(amount + 1));
//        //初始化
//        for (int j = 1; j <= amount; ++j) dp[0][j] = INF;
//        for (int i = 1; i <= n; ++i)
//        {
//            for (int j = 0; j <= amount; ++j)
//            {
//                dp[i][j] = dp[i - 1][j];
//                if (j >= coins[i - 1])
//                    dp[i][j] = min(dp[i][j], dp[i][j - coins[i - 1]] + 1);
//            }
//        }
//        return dp[n][amount] >= INF ? -1 : dp[n][amount];
//    }
//};



//class Solution {
//public:
//    int coinChange(vector<int>& coins, int amount) {
//        int n = coins.size();
//        const int INF = 0x3f3f3f3f;
//        vector<int> dp(amount + 1);
//        //初始化
//        for (int j = 1; j <= amount; ++j) dp[j] = INF;
//        for (int i = 1; i <= n; ++i)
//        {
//            for (int j = coins[i - 1]; j <= amount; ++j)
//            {
//                dp[j] = min(dp[j], dp[j - coins[i - 1]] + 1);
//            }
//        }
//        return dp[amount] >= INF ? -1 : dp[amount];
//    }
//};



//class Solution {
//public:
//    int change(int amount, vector<int>& coins) {
//        int n = coins.size();
//        vector<vector<int>> dp(n + 1, vector<int>(amount + 1));
//        //初始化
//        dp[0][0] = 1;
//        for (int i = 1; i <= n; ++i)
//        {
//            for (int j = 0; j <= amount; ++j)
//            {
//                dp[i][j] = dp[i - 1][j];
//                if (j >= coins[i - 1])
//                    dp[i][j] += dp[i][j - coins[i - 1]];
//            }
//        }
//        return dp[n][amount];
//    }
//};


//class Solution {
//public:
//    int change(int amount, vector<int>& coins) {
//        int n = coins.size();
//        vector<int> dp(amount + 1);
//        //初始化
//        dp[0] = 1;
//        for (int i = 1; i <= n; ++i)
//        {
//            for (int j = coins[i - 1]; j <= amount; ++j)
//            {
//                dp[j] += dp[j - coins[i - 1]];
//            }
//        }
//        return dp[amount];
//    }
//};


//class Solution {
//public:
//    int numSquares(int n) {
//        vector<int> dp(n + 1);
//        dp[1] = 1;
//        for (int i = 2; i <= n; ++i)
//        {
//            dp[i] = 1 + dp[i - 1];  //至少有一个这样的结果
//            for (int j = 2; j * j <= i; ++j)
//            {
//                dp[i] = min(dp[i], dp[i - j * j] + 1);
//            }
//        }
//        return dp[n];
//    }
//};


//class Solution {
//public:
//    int findMaxForm(vector<string>& strs, int m, int n) {
//        int sz = strs.size();
//        vector<vector<vector<int>>> dp(sz + 1, vector<vector<int>>(m + 1, vector<int>(n + 1)));
//        for (int i = 1; i <= sz; ++i)
//        {
//            int a = 0;  //统计0的个数
//            int b = 0;  //统计1的个数
//            for (auto ch : strs[i - 1])
//            {
//                if (ch == '0') ++a;
//                else ++b;
//            }
//            for (int j = 0; j <= m; ++j)
//            {
//                for (int k = 0; k <= n; ++k)
//                {
//                    dp[i][j][k] = dp[i - 1][j][k];
//                    if (j >= a && k >= b)
//                        dp[i][j][k] = max(dp[i][j][k], dp[i - 1][j - a][k - b] + 1);
//                }
//            }
//        }
//        return dp[sz][m][n];
//    }
//};


//class Solution {
//public:
//    int findMaxForm(vector<string>& strs, int m, int n) {
//        int sz = strs.size();
//        vector<vector<int>> dp(m + 1, vector<int>(n + 1));
//        for (int i = 1; i <= sz; ++i)
//        {
//            int a = 0;  //统计0的个数
//            int b = 0;  //统计1的个数
//            for (auto ch : strs[i - 1])
//            {
//                if (ch == '0') ++a;
//                else ++b;
//            }
//            for (int j = m; j >= a; --j)
//            {
//                for (int k = n; k >= b; --k)
//                {
//                    dp[j][k] = max(dp[j][k], dp[j - a][k - b] + 1);
//                }
//            }
//        }
//        return dp[m][n];
//    }
//};


//class Solution {
//public:
//    int profitableSchemes(int n, int minProfit, vector<int>& group, vector<int>& profit) {
//        const int MOD = 1e9 + 7;
//        int m = group.size(), l = profit.size();
//        vector<vector<vector<int>>> dp(l + 1, vector<vector<int>>(n + 1, vector<int>(minProfit + 1)));
//        //初始化 -- 当项目和利润都为0的时候，无论人数是多少，都有1种解决方案
//        for (int j = 0; j <= n; ++j) dp[0][j][0] = 1;
//        for (int i = 1; i <= l; ++i)
//        {
//            for (int j = 0; j <= n; ++j)
//            {
//                for (int k = 0; k <= minProfit; ++k)
//                {
//                    dp[i][j][k] = dp[i - 1][j][k];
//                    if (j >= group[i - 1])
//                    {
//                        dp[i][j][k] += dp[i - 1][j - group[i - 1]][max(0, k - profit[i - 1])];
//                    }
//                    dp[i][j][k] %= MOD;
//                }
//            }
//        }
//        return dp[l][n][minProfit];
//    }
//};


//class Solution {
//public:
//    int profitableSchemes(int n, int minProfit, vector<int>& group, vector<int>& profit) {
//        const int MOD = 1e9 + 7;
//        int l = profit.size();
//        vector<vector<int>> dp(n + 1, vector<int>(minProfit + 1));
//        //初始化 -- 当项目和利润都为0的时候，无论人数是多少，都有1种解决方案
//        for (int j = 0; j <= n; ++j) dp[j][0] = 1;
//        for (int i = 1; i <= l; ++i)
//        {
//            for (int j = n; j >= group[i - 1]; --j)
//            {
//                for (int k = 0; k <= minProfit; ++k)
//                {
//                    dp[j][k] += dp[j - group[i - 1]][max(0, k - profit[i - 1])];
//                    dp[j][k] %= MOD;
//                }
//            }
//        }
//        return dp[n][minProfit];
//    }
//};


//class Solution {
//public:
//    int combinationSum4(vector<int>& nums, int target) {
//        int n = nums.size();
//        vector<vector<int>> dp(n + 1, vector<int>(target + 1));
//        //初始化
//        for (int i = 0; i <= n; ++i) dp[i][0] = 1;
//        for (int i = 1; i <= n; ++i)
//        {
//            for (int j = 0; j <= target; ++j)
//            {
//                dp[i][j] = dp[i - 1][j];
//                if (j >= nums[i - 1])
//                {
//                    dp[i][j] += dp[i - 1][j - nums[i - 1]];
//                }
//            }
//        }
//        return dp[n][target];
//    }
//};


//class Solution {
//public:
//    int combinationSum4(vector<int>& nums, int target) {
//        int n = nums.size();
//        vector<double> dp(target + 1);
//        //初始化
//        dp[0] = 1;
//        for (int j = 1; j <= target; ++j)
//        {
//            for (int i = 1; i <= n; ++i)
//            {
//                if (j >= nums[i - 1])
//                {
//                    dp[j] += dp[j - nums[i - 1]];
//                }
//            }
//        }
//        return dp[target];
//    }
//};



//class Solution {
//public:
//    int numTrees(int n) {
//        vector<int> dp(n + 1);
//        //初始化
//        dp[0] = 1;
//        for (int i = 1; i <= n; ++i)
//        {
//            for (int j = 1; j <= i; ++j)
//            {
//                dp[i] += dp[i - j] * dp[j - 1];
//            }
//        }
//        return dp[n];
//    }
//};


