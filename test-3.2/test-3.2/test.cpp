#include <iostream>
using namespace std;

struct ListNode {
	int val;
	ListNode* next;
	ListNode() : val(0), next(nullptr) {}
	ListNode(int x) : val(x), next(nullptr) {}
	ListNode(int x, ListNode* next) : val(x), next(next) {}
};

class Solution {
public:
    ListNode* removeNthFromEnd(ListNode* head, int n) {
        //创建头节点
        ListNode* guard = new ListNode(0);
        guard->next = head;
        //前后指针法，前指针走n-1步，然后迭代走删除即可
        ListNode* front = head;
        ListNode* tail = guard;
        while (--n)
        {
            front = front->next;
        }
        while (front->next)
        {
            front = front->next;
            tail = tail->next;
        }
        //删除
        ListNode* tmp = tail->next;
        //tail->next = tail->next->next;
        tail->next = front;
        ListNode* newhead = guard->next;
        delete tmp;
        delete guard;
        return newhead;
    }
};


int main()
{
    ListNode* p1 = new ListNode(1);
    ListNode* p2 = new ListNode(2);
    ListNode* p3 = new ListNode(3);
    ListNode* p4 = new ListNode(4);
    ListNode* p5 = new ListNode(5);
    p1->next = p2;
    p2->next = p3;
    p3->next = p4;
    p4->next = p5;
    p5->next = nullptr;
    ListNode* ret = Solution().removeNthFromEnd(p1, 2);

    return 0;
}