#include <iostream>
#include <vector>
#include <cstdlib>
#include <algorithm>
using namespace std;


//int main() {
//    int n;
//    cin >> n;
//    vector<long long> arr(n);
//    long long sum = 0;
//    long long ret = 0;  //防止数据溢出
//    for (auto& e : arr)
//    {
//        cin >> e;
//        sum += e;
//    }
//    for (int i = 1; i < n; ++i)
//    {
//        ret = max(ret, sum - (arr[i] + arr[i - 1]) + ((long long)arr[i] * arr[i - 1]));
//    }
//    cout << ret << endl;
//}


//int main() {
//    int n;
//    cin >> n;
//    vector<long long> arr(n);
//    long long sum = 0;
//    for (auto& e : arr)
//    {
//        cin >> e;
//        sum += e;
//    }
//    //如果sum%n==0说明可以整除，那么操作数就是所有数到平均数距离之和的一半
//    if (sum % n == 0)
//    {
//        long long avg = sum / n;
//        long long ret = 0;  //操作数
//        for (auto e : arr)
//        {
//            if (e > avg) ret += (e - avg);
//            else ret += (avg - e);
//        }
//        cout << ret / 2 << endl;
//        return 0;
//    }
//    //这里就是不能整除，那么一定能保证n-1个数是可以整除，这时候我们的策略就是放弃最大或者最小的那个数
//    sort(arr.begin(), arr.end());
//    auto FindMinRet = [&](int left, int right)
//    {
//        long long sum = 0;
//        for (int i = left; i <= right; ++i)
//        {
//            sum += arr[i];
//        }
//        long long avg = sum / (right - left + 1);
//        long long avg1 = avg + 1;  //中间可能因为奇数的情况而没有办法操作全部，所以可能有两个结果
//        long long a = 0, b = 0, c = 0, d = 0;  //a，b用来计算avg c，d用来计算avg2
//        for (int i = left; i <= right; ++i)
//        {
//            if (arr[i] > avg) a += (arr[i] - avg);
//            else b += (avg - arr[i]);
//            if (arr[i] > avg1) c += (arr[i] - avg1);
//            else d += (avg1 - arr[i]);
//        }
//        return min(max(a, b), max(c, d));
//    };
//    long long ret1 = FindMinRet(0, n - 2);  //放弃最大的那个数
//    long long ret2 = FindMinRet(1, n - 1);  //放弃最小的那个数
//    cout << min(ret1, ret2) << endl;
//}


//#include <map>
//#include <set>
//using namespace std;
//
//bool IsRegular(string& str) {
//    for (auto ch : str) {
//        if ('a' > ch || 'z' < ch) return false;
//    }
//    return true;
//}
//
//int main() {
//    int n;
//    cin >> n;
//    map<string, pair<string, set<string>>> hash; //key存商家 val中的key是主店地址,地址集合
//    while (n--) {
//        string bus, addr; //商家和地址
//        cin >> bus >> addr;
//        //判断合法性
//        if (IsRegular(bus) && IsRegular(addr))
//        {
//            if (hash[bus].first == "") hash[bus].first = addr;
//            hash[bus].second.insert(addr);
//        }
//    }
//    //输出
//    for (auto& iter : hash) {
//        cout << iter.first << " " << iter.second.first << " " << iter.second.second.size() - 1 << endl;
//    }
//    return 0;
//}


//#include <iostream>
//#include <vector>
//#include <string>
//using namespace std;
//
//int main() {
//    int n;
//    cin >> n;
//    vector<vector<int>> arr(n, vector<int>(n));
//    vector<vector<int>> dp(n + 1, vector<int>(n + 1));  //前缀和数组
//    for (auto& a : arr)
//    {
//        string str;
//        cin >> str;
//        int i = 0;
//        for (auto& e : a)
//        {
//            e = (str[i++] - '0');
//        }
//    }
//    //计算前缀和数组
//    for (int i = 1; i <= n; ++i)
//    {
//        for (int j = 1; j <= n; ++j)
//        {
//            dp[i][j] = dp[i - 1][j] + dp[i][j - 1] - dp[i - 1][j - 1] + arr[i - 1][j - 1];  //这里需要注意下标的映射关系
//        }
//    }
//    //统计结果
//    for (int k = 0; k < n; ++k)
//    {
//        if (k % 2 == 0)
//        {
//            cout << 0 << endl;  //奇数的情况下一定不存在完美区域  
//            continue;
//        }
//        int ret = (k + 1) * (k + 1) / 2; //前缀和的结果必须满足ret
//        int count = 0;
//        for (int i = 1; i + k <= n; ++i)
//        {
//            for (int j = 1; j + k <= n; ++j)
//            {
//                int x1 = i - 1, y1 = j - 1;
//                int x2 = i + k, y2 = j + k;
//                int res = dp[x2][y2] - dp[x2][y1] - dp[x1][y2] + dp[x1][y1];
//                if (ret == res) ++count;
//            }
//        }
//        cout << count << endl;
//    }
//    return 0;
//}


//class Solution {
//public:
//    int numDecodings(string s) {
//        int n = s.size();
//        vector<int> dp(n + 1);
//        //初始化
//        dp[0] = 1;
//        dp[1] = s[0] != '0';
//        for (int i = 2; i <= n; ++i)
//        {
//            //判断当前位置是否可以独自解码
//            if (s[i - 1] != '0') dp[i] += dp[i - 1];
//            //判断当前位置是否能和前面的位置构成解码关系
//            int tmp = (s[i - 2] - '0') * 10 + (s[i - 1] - '0');
//            if (10 <= tmp && tmp <= 26) dp[i] += dp[i - 2];
//        }
//        return dp[n];
//    }
//};


//class Solution {
//public:
//    int minFallingPathSum(vector<vector<int>>& matrix) {
//        int n = matrix.size();
//        vector<vector<int>> dp(n + 1, vector<int>(n + 2, INT_MAX));
//        //对dp第一行进行初始化
//        for (int i = 0; i < n + 2; ++i)
//        {
//            dp[0][i] = 0;
//        }
//        for (int i = 1; i < n + 1; ++i)
//        {
//            for (int j = 1; j < n + 1; ++j)
//            {
//                dp[i][j] = min(dp[i - 1][j - 1], min(dp[i - 1][j], dp[i - 1][j + 1])) + matrix[i - 1][j - 1];
//            }
//        }
//        //收集最后一行结果的最小值
//        int ret = INT_MAX;
//        for (int i = 1; i < n + 1; ++i)
//        {
//            if (dp[n][i] < ret) ret = dp[n][i];
//        }
//        return ret;
//    }
//};


//int main() {
//    int n, m, q;
//    cin >> n >> m >> q;
//    vector<int> ufs(n + 1, -1);  //并查集
//    auto FindRoot = [&](int x)
//    {
//        while (ufs[x] >= 0)
//        {
//            x = ufs[x];
//        }
//        return x;
//    };
//    auto Union = [&](int x1, int x2)
//    {
//        int root1 = FindRoot(x1);
//        int root2 = FindRoot(x2);
//        if (root1 != root2)
//        {
//            ufs[root1] += ufs[root2];
//            ufs[root2] = root1;
//        }
//    };
//    unordered_map<int, int> hash;  //用来保存边之间的关系
//    unordered_map<int, int> del_hash;  //用来保存删除之间的关系
//    vector<vector<int>> Input; //用来输入
//    int u, v;
//    while (m--)
//    {
//        cin >> u >> v;
//        hash[u] = v;
//    }
//    int op;
//    while (q--)
//    {
//        cin >> op >> u >> v;
//        Input.push_back({ op,u,v });
//        if (op == 1)
//        {
//            if (hash.count(u))
//            {
//                del_hash[u] = v;
//            }
//            if (hash.count(v))
//            {
//                del_hash[v] = u;
//            }
//        }
//    }
//    //往并查集中插入后序没有删除的元素
//    for (auto& iter : hash)
//    {
//        int key = iter.first, val = iter.second;
//        if (del_hash[key] == val || del_hash[val] == key) continue;  //不需要插入，因为后序需要删除
//        Union(key, val);  //放入并查集中
//    }
//    //打印并查集调试
//    //查询结果，如果遇到1就需要往并查集中插入
//    vector<string> ret;  //用于保存结果
//    //这里需要逆序查找
//    for (int i = n - 1; i >= 0; --i)
//    {
//        if (Input[i][0] == 1)
//        {
//            //需要放入并查集中
//            Union(Input[i][1], Input[i][2]);
//        }
//        else
//        {
//            int root1 = FindRoot(Input[i][1]);
//            int root2 = FindRoot(Input[i][2]);
//            if (root1 == root2)
//            {
//                ret.push_back("Yes");
//            }
//            else ret.push_back("No");
//        }
//    }
//    //反转结果
//    reverse(ret.begin(), ret.end());
//    for (auto& str : ret) cout << str << endl;
//    return 0;
//}


//int main() {
//    int n, k;
//    cin >> n >> k;
//    int x = 0, y = 0;  //找出2和5的因子
//    vector<int> nums(n);
//    for (auto& e : nums)
//    {
//        cin >> e;
//        if (e % 2 == 0) ++x;
//        if (e % 5 == 0) ++y;
//    }
//    //处理前缀和数组
//    vector<int> two(n), five(n);
//    //初始化
//    if (nums[0] % 2 == 0) two[0] = 1;
//    if (nums[0] % 5 == 0) two[0] = 1;
//    for (int i = 1; i < n; ++i)
//    {
//        if (nums[i] % 2 == 0) two[i] = two[i - 1] + 1;
//        if (nums[i] % 5 == 0) five[i] = five[i - 1] + 1;
//    }
//    int ret = 0;
//    int twoCount = 0, fiveCount = 0;
//    for (int left = 0, right = 0; right < n; ++right)
//    {
//        //进窗口
//        twoCount += two[right];
//        fiveCount += five[right];
//        //判断
//        while (twoCount > x - k || fiveCount > y - k)
//        {
//            //出窗口
//            twoCount -= two[left];
//            fiveCount -= five[left];
//            --left;
//        }
//        //收集结果
//        if (twoCount <= x - k && fiveCount <= y - k) ++ret;
//    }
//    cout << ret << endl;
//}


