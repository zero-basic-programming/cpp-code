#include <iostream>
#include <vector>
using namespace std;


//这里统一排升序
//插入排序
void InsertSort(vector<int>& arr)
{
	//只能确定前面的是有序的
	int n = arr.size();
	for (int i = 0; i < n - 1; ++i) //趟数
	{
		int end = i;
		int tmp = arr[i+1]; // 保存要确定的值
		while (end >= 0)
		{
			if (arr[end] > tmp)
			{
				arr[end + 1] = arr[end];
				--end;
			}
			else break;
		}
		//把要确定的值填到该位置
		arr[end + 1] = tmp;
	}
}

//希尔排序
void ShellSort(vector<int>& arr)
{
	int n = arr.size();
	int gap = n; //gap不断变小，意味着顺序逐渐变得有序
	while (gap > 1) //这里不能等于，不然会导致死循环
	{
		gap /= 2;
		//一趟
		for (int i = 0; i < n - gap; ++i)
		{
			int end = i;
			int tmp = arr[end + gap];
			while (end >= 0)
			{
				if (arr[end] > tmp)
				{
					arr[end + gap] = arr[end];
					end -= gap;
				}
				else break;
			}
			//把目标值放到该位置
			arr[end + gap] = tmp;
		}
	}
}


//选择排序 -- 遍历一次选一个最大和最小
void SelectSort(vector<int>& arr)
{
	int n = arr.size();
	int left = 0;
	int right = n - 1; //使用闭区间
	while (left < right)
	{
		int maxi = left;
		int mini = left;
		for (int i = left; i <= right; ++i)
		{
			//在这个区间内找到最大或者最小
			if (arr[i] > arr[maxi]) maxi = i;
			else if (arr[i] < arr[mini]) mini = i;
		}
		//交换
		swap(arr[mini], arr[left]);  //最小值和最左边的数字进行交换
		//这里有可能最左边的值是最大值，会影响到最大值的交换问题
		if (maxi == left)
		{
			maxi = mini; 
		}
		swap(arr[maxi], arr[right]);
		++left;
		--right;
	}
}

//向下调整算法 大堆
void AdjustDown(vector<int>& arr,int parent,int n)
{
	//在当前节点中找左右较大的孩子来和根比较，如果大于根就交换
	int child = 2 * parent + 1; //左孩子
	while (child < n)
	{
		//找左右孩子中较大的,有可能没有右孩子（越界了）
		if (child + 1 < n && arr[child] < arr[child + 1])
		{
			child += 1;
		}
		if (arr[parent] < arr[child])
		{
			swap(arr[parent], arr[child]);
			//向下调整
			parent = child;
			child = 2 * parent + 1;
		}
	}
}

//堆排序
void HeapSort(vector<int>& arr)
{
	int n = arr.size();
	//利用向下调整建堆
	for (int i = (n - 1 - 1) / 2; i >= 0; --i)
	{
		AdjustDown(arr, i,n);
	}
	//每次交换堆顶数据和最后一个数据
	for (int i = 1; i < n; ++i) //进行n-1次即可
	{
		swap(arr[0], arr[n - i]);
		//向下调整
		AdjustDown(arr, 0, n - i);
	}
}


//冒泡排序
void BubbleSort(vector<int>& arr)
{
	int n = arr.size();
	//一次排一个最大的数
	for (int i = 0; i < n - 1; ++i) //趟数
	{
		int exchange = 1;//用来记录该趟是否改变过
		for (int j = 0; j < n - i - 1; ++j)
		{
			if (arr[j] > arr[j + 1])
			{
				swap(arr[j], arr[j + 1]);
				exchange = 0; //这里说明已经改变过了
			}
		}
		if (exchange == 1) break; //没有改变过，说明整体有序，不需要再进行下去
	}
}

int NumOfMid(vector<int>& arr,int left,int right)
{
	int mid = left + (right - left) / 2; //防止数据溢出
	if (arr[left] > arr[mid])
	{
		if (arr[mid] > arr[right]) return mid;
		else if (arr[right] > arr[left]) return left;
		else return right;
	}
	else  //arr[left] < arr[mid]
	{
		if (arr[left] > arr[right]) return left;
		else if (arr[right] > arr[mid]) return mid;
		else return right;
	}
}

//一次快排 -- 挖坑法
int _QuickSort1(vector<int>& arr, int left, int right)
{
	//三数取中，为了防止有序的情况下效率低
	int mid = NumOfMid(arr, left, right);
	swap(arr[mid], arr[left]);
	//最左边的是目标值
	int key = arr[left];
	int holei = left;
	while (left < right)
	{
		//左边找大，右边找小 ,这里必须是等于，否则有重复元素就是死循环，坑位会一直在同一个元素交换
		while (left < right && arr[right] >= key) --right;
		arr[holei] = arr[right];
		holei = right;
		while (left < right && arr[left] <= key) ++left;
		arr[holei] = arr[left];
		holei = left;
	}
	arr[holei] = key;
	return holei;
}

//快排 -- 前后指针法
int _QuickSort2(vector<int>& arr, int left, int right)
{
	//三数取中，为了防止有序的情况下效率低
	int mid = NumOfMid(arr, left, right);
	swap(arr[mid], arr[left]);
	//最左边的是目标值
	int keyi = left;
	int cur = left; //找小
	int pre = left; //找大
	while (cur <= right)
	{
		if (arr[cur] < arr[keyi] && ++pre != cur)
		{
			swap(arr[pre], arr[cur]);
		}
		++cur;
	}
	swap(arr[keyi], arr[pre]);
	return pre;
}

//快排 -- 递归版本
void QuickSort(vector<int>& arr,int left,int right)
{
	if (left >= right) return;
	//int keyi = _QuickSort1(arr, left, right);
	int keyi = _QuickSort2(arr, left, right);
	// [left,keyi-1] [keyi+1,right]
	QuickSort(arr, left, keyi - 1);
	QuickSort(arr, keyi+1, right);
}


int main()
{
	vector<int> arr = { 1,4,2,5,6,3,7,9,8,2,3};
	//InsertSort(arr);
	//ShellSort(arr);
	//SelectSort(arr);
	//HeapSort(arr);
	//BubbleSort(arr);
	QuickSort(arr, 0, arr.size()-1);
	for (auto e : arr)
	{
		cout << e << " ";
	}
	cout << endl;
	return 0;
}