#include <iostream>
#include <vector>
using namespace std;

//class Solution {
//public:
//    int minimumDeleteSum(string s1, string s2) {
//        int m = s1.size();
//        int n = s2.size();
//        vector<vector<int>> dp(m + 1, vector<int>(n + 1));
//        for (int i = 1; i <= m; ++i)
//        {
//            for (int j = 1; j <= n; ++j)
//            {
//                dp[i][j] = max(dp[i - 1][j], dp[i][j - 1]);
//                if (s1[i - 1] == s2[j - 1]) dp[i][j] = max(dp[i][j], dp[i - 1][j - 1] + s1[i - 1]);
//            }
//        }
//        //统计全部字符的和，然后得出结果
//        int sum = 0;
//        for (auto ch : s1) sum += ch;
//        for (auto ch : s2) sum += ch;
//        return sum - 2 * dp[m][n];
//    }
//};


//class Solution {
//public:
//    int findLength(vector<int>& nums1, vector<int>& nums2) {
//        int m = nums1.size();
//        int n = nums2.size();
//        vector<vector<int>> dp(m + 1, vector<int>(n + 1));
//        int ret = 0;
//        for (int i = 1; i <= m; ++i)
//        {
//            for (int j = 1; j <= n; ++j)
//            {
//                if (nums1[i - 1] == nums2[j - 1])
//                    dp[i][j] = dp[i - 1][j - 1] + 1;
//                ret = max(ret, dp[i][j]);
//            }
//        }
//        return ret;
//    }
//};