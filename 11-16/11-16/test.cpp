//#include <iostream>
//#include <vector>
//#include <map>
//#include <fstream>
//#include <string>
//#include <sstream>
//#include <cassert>
//using namespace std;
//
//class cmp
//{
//public:
//	bool operator()(const pair<string,string>& p1, const pair<string, string>& p2)
//	{
//		return p1.first > p2.first;
//	}
//};
//
//
//class Util 
//{
//public:
//	static bool ReadFile(map<string,string,cmp>& map)
//	{
//		ifstream file;
//		file.open("test.txt");
//		if (file.is_open() == false) return false;
//		//读取文件放入到map中
//		string line;
//		while (getline(file, line))   //读取文件中的每一行
//		{
//			//按照空格进行分割
//			stringstream ss(line);
//			vector<string> tokens;   //放入切割好的字符串
//			string token;			//每一个切割的字符串
//			while (ss >> token)     //按照空格进行分割
//			{
//				tokens.emplace_back(token);
//			}
//			//把tokens中的内容放入到map中,这里通过协议的方式保证了每次一定是两个
//			int sz = tokens.size();
//			assert(sz / 2 == 0 && sz > 0);
//			for (int i = 0; i < sz; i+=2)
//			{
//				map[tokens[i]] = tokens[i + 1];
//			}
//		}
//		//关闭文件
//		file.close();
//		return true;
//	}
//	static bool WriteFile(map<string,string,cmp>& map)
//	{
//		//读取map，然后通过行的方式写入即可
//		ofstream file("result.txt");
//		if (file.is_open() == false) return false;
//		for (auto& p : map)
//		{
//			file << p.first << "    " << p.second << endl;
//		}
//		//关闭文件
//		file.close();
//	}
//};
//
//
//int main()
//{
//	map<string, string,cmp> map;   //记录英文和中文之间的映射关系
//	Util::ReadFile(map);
//	Util::WriteFile(map);
//	return 0;
//}



#include <iostream>
#include <vector>
#include <map>
#include <fstream>
#include <string>
#include <sstream>
#include <cassert>
#include <functional> // 包含functional以继承binary_function

using namespace std;

//class cmp : public binary_function<pair<string, string>, pair<string, string>, bool> 
//{
//public:
//	bool operator()(const pair<string, string>& p1, const pair<string, string>& p2) const noexcept
//	{
//		return p1.first > p2.first;
//	}
//};
//
//class Util {
//public:
//	static bool ReadFile(map<string, string, cmp>& map) {
//		ifstream file;
//		file.open("test.txt");
//		if (!file.is_open()) return false;
//		string line;
//		while (getline(file, line)) {
//			stringstream ss(line);
//			vector<string> tokens;
//			string token;
//			while (ss >> token) {
//				tokens.emplace_back(token);
//			}
//			assert(tokens.size() % 2 == 0 && tokens.size() > 0);
//			for (size_t i = 0; i < tokens.size(); i += 2) {
//				map[tokens[i]] = tokens[i + 1];
//			}
//		}
//		file.close();
//		return true;
//	}
//	static bool WriteFile(map<string, string, cmp>& map) {
//		ofstream file("result.txt");
//		if (!file.is_open()) return false;
//		for (auto& p : map) {
//			file << p.first << "    " << p.second << endl;
//		}
//		file.close();
//		return true;
//	}
//};

//int main() {
//	map<string, string, cmp> map;
//	if (Util::ReadFile(map) && Util::WriteFile(map)) {
//		cout << "文件读写成功。" << endl;
//	}
//	else {
//		cout << "文件读写失败。" << endl;
//	}
//	return 0;
//}



