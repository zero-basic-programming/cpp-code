#include <iostream>
#include <vector>
#include <queue>
using namespace std;

//class Solution {
//public:
//    int maxPrice(vector<vector<int> >& grid) {
//        int m = grid.size(), n = grid[0].size();
//        vector<vector<int>> dp(m + 1, vector<int>(n + 1));
//        for (int i = 1; i <= m; ++i)
//        {
//            for (int j = 1; j <= n; ++j)
//            {
//                dp[i][j] = max(dp[i - 1][j], dp[i][j - 1]) + grid[i - 1][j - 1];
//            }
//        }
//        return dp[m][n];
//    }
//};


//struct TreeNode {
//    int val;
//    struct TreeNode* left;
//    struct TreeNode* right;
//    TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
//};
//
//
//class Solution {
//public:
//    vector<vector<int>> sortOutput(TreeNode* root) {
//        // write code here
//        queue<TreeNode*> q;
//        q.push(root);
//        vector<vector<int>> ret;
//        if (root == nullptr) return ret;  //特殊节点处理
//        while (q.size())
//        {
//            vector<int> tmp;   //用来保存当前层的节点值
//            int sz = q.size();
//            for (int i = 0; i < sz; ++i)
//            {
//                TreeNode* node = q.front();
//                q.pop();
//                tmp.push_back(node->val);
//                //把两个子节点入队列
//                if (node->left) q.push(node->left);
//                if (node->right) q.push(node->right);
//            }
//            //收集结果
//            ret.push_back(tmp);
//        }
//        return ret;
//    }
//};



//class Solution {
//public:
//    ListNode* mergeProductList(ListNode* list1, ListNode* list2) {
//        ListNode* guard = new ListNode(0);  //创建一个头节点
//        ListNode* tail = guard;
//        while (list1 && list2)
//        {
//            if (list1->val > list2->val)
//            {
//                tail->next = list1;
//                list1 = list1->next;
//            }
//            else
//            {
//                tail->next = list2;
//                list2 = list2->next;
//            }
//            tail = tail->next;
//        }
//        //这里还有一个没有连接的链表
//        if (list1) tail->next = list1;
//        if (list2) tail->next = list2;
//        //释放内存
//        ListNode* newhead = guard->next;
//        delete guard;
//        return newhead;
//    }
//};



