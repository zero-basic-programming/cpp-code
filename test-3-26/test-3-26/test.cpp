#include <iostream>
#include <map>
#include <string>
#include <mutex>

using namespace std;


//class copy_ban
//{
//	//禁止拷贝构造和赋值重载
//	copy_ban(const copy_ban& cb) = delete;
//	copy_ban& operator=(const copy_ban& cb) = delete;
//};

////一个只能在堆上开辟空间的类
//class heap_only
//{
//public:
//	static heap_only* create()
//	{
//		return new heap_only;
//	}
//private:
//	heap_only();
//	heap_only(const heap_only&) = delete;//禁止拷贝构造，防止在其他地方开辟空间
//};
//int main()
//{
//	heap_only* hy1 = heap_only::create();
//	//heap_only* hy2(hy1);
//	//heap_only* hy3 = hy1;
//
//	return 0;
//}

//int main()
//{
//	//int a = 1;
//	////相似类型安全类型转换
//	//double d = static_cast<double>(a);
//
//	////不相似的安全类型转换
//	//int* p = reinterpret_cast<int*>(a);
//
//	////为了保证内存的可见性，增加关键字volatile
//	//const int b = 2;
//	//int* p = const_cast<int*>(&b);
//	//*p = 3;
//	////这里打印出来的结果是有问题的，是因为编译器的优化，直接从寄存器中拿到数据，所以最后的结果是2，3
//	//cout << b << endl;
//	//cout << *p << endl;
//
//
//	//为了保证内存的可见性，增加关键字volatile
//	volatile const int b = 2;
//	int* p = const_cast<int*>(&b);
//	*p = 3;
//	cout << b << endl;
//	cout << *p << endl;
//	return 0;
//}


//class A
//{
//public:
//	virtual void fun() {};
//	int _a;
//};
//
//class B :public A
//{
//public:
//	int _b;
//};
//
////void func(A* p)
////{
////	B* bptr = dynamic_cast<B*>(p);
////	//这里是b的就没问题，如果是a就会出错
////
////	bptr->_a = 1;
////	bptr->_a = 2;
////	cout << bptr->_a << endl;
////	cout << bptr->_b << endl;
////}
//
//void func(A* p)
//{
//	B* bptr = dynamic_cast<B*>(p);
//	//这里因为dynamic_cast的保护，如果超出访问就会赋值为空
//	if (bptr)
//	{
//		bptr->_a = 1;
//		bptr->_b = 2;
//		cout << bptr->_a << endl;
//		cout << bptr->_b << endl;
//	}
//}
//int main()
//{
//	A a;
//	B b;
//	func(&a);
//	func(&b);
//	return 0;
//}


//单例模式
////饿汉模式
//class InfoSingleton
//{
//public:
//	static InfoSingleton& create()
//	{
//		return _sins;
//	}
//	//假设这是一个存储工资的程序
//	void insert(string s,int salary)
//	{
//		_mp[s] = salary;
//	}
//	void print()
//	{
//		for (auto& kv : _mp)
//		{
//			cout << kv.first << " : " << kv.second << endl;
//		}
//	}
//private:
//	map<string, int> _mp;
//	InfoSingleton()
//	{}
//	InfoSingleton(const InfoSingleton&) = delete;
//	InfoSingleton& operator=(const InfoSingleton&) = delete;
//private:
//	static InfoSingleton _sins;
//};
// 
//InfoSingleton InfoSingleton::_sins;
//
//
//int main()
//{
//	InfoSingleton& infos = InfoSingleton::create();
//	infos.insert("李四", 15000);
//	infos.insert("张三", 16000);
//	infos.insert("赵六", 12000);
//	infos.insert("王五", 8000);
//	infos.print();
//	return 0;
//}



////懒汉模式
////这种写法到了C++11之后才支持
//class InfoSingleton
//{
//public:
//	static InfoSingleton& create()
//	{
//		static InfoSingleton sinst;
//		return sinst;
//	}
//	//假设这是一个存储工资的程序
//	void insert(string s, int salary)
//	{
//		_mp[s] = salary;
//	}
//	void print()
//	{
//		for (auto& kv : _mp)
//		{
//			cout << kv.first << " : " << kv.second << endl;
//		}
//	}
//private:
//	map<string, int> _mp;
//	InfoSingleton()
//	{}
//	InfoSingleton(const InfoSingleton&) = delete;
//	InfoSingleton& operator=(const InfoSingleton&) = delete;
//
//};
//
//
//int main()
//{
//	InfoSingleton& infos = InfoSingleton::create();
//	infos.insert("李四", 15000);
//	infos.insert("张三", 16000);
//	infos.insert("赵六", 12000);
//	infos.insert("王五", 8000);
//	infos.print();
//	return 0;
//}

////使用RAII的方式对锁进行管理
//template <class Lock>
//class Lock_Guard
//{
//public:
//	Lock_Guard(Lock& lk)
//		:_lk(lk)
//	{
//		_lk.lock();
//	}
//	~Lock_Guard()
//	{
//		_lk.unlock();
//	}
//private:
//	Lock& _lk;//这里引用保证只创建一把锁
//};
//
////懒汉模式C++11之前的写法
////这时存在线程安全的问题
//class InfoSingleton
//{
//public:
//	static InfoSingleton& create()
//	{
//		//双检查模式
//		if (_sins == nullptr)
//		{
//			Lock_Guard<mutex> lock(_mtx);
//			if (_sins == nullptr)
//			{
//				_sins = new InfoSingleton;
//			}
//		}
//		return *_sins;
//	}
//	//假设这是一个存储工资的程序
//	void insert(string s,int salary)
//	{
//		_mp[s] = salary;
//	}
//	void print()
//	{
//		for (auto& kv : _mp)
//		{
//			cout << kv.first << " : " << kv.second << endl;
//		}
//	}
//	//可以选择手动回收资源
//	static void DelInstance()
//	{
//		//保存文件
//		//... 
//
//		Lock_Guard<mutex> lock(_mtx);
//		if (_sins)
//		{
//			delete _sins;
//			_sins = nullptr;
//		}
//	}
//	//使用GC的方式在进程结束的时候自动回收
//	class GC
//	{
//	public:
//		~GC()
//		{
//			if (_sins)
//			{
//				cout << "~GC" << endl;
//				DelInstance();
//			}
//		}
//	};
//private:
//	map<string, int> _mp;
//	InfoSingleton()
//	{}
//	InfoSingleton(const InfoSingleton&) = delete;
//	InfoSingleton& operator=(const InfoSingleton&) = delete;
//private:
//	static InfoSingleton* _sins;
//	static mutex _mtx;
//	static GC _gc;
//};
// 
//InfoSingleton* InfoSingleton::_sins = nullptr;
//mutex InfoSingleton::_mtx;
//InfoSingleton::GC InfoSingleton::_gc;
//
//int main()
//{
//	InfoSingleton& infos = InfoSingleton::create();
//	infos.insert("李四", 15000);
//	infos.insert("张三", 16000);
//	infos.insert("赵六", 12000);
//	infos.insert("王五", 8000);
//	infos.print();
//	//如果需要手动释放，就可以调用这个函数
//	InfoSingleton::DelInstance();
//	return 0;
//}
//


//int main()
//{
//    int n;
//    while (cin >> n)
//    {
//        while (n > 10)
//        {
//            //把每个位都得到
//            int tmp = n;//存储一下n
//            int sum = 0;
//            while (tmp)
//            {
//                sum += tmp % 10;
//                tmp /= 10;
//            }
//            n = sum;
//        }
//        cout << n << endl;
//    }
//    return 0;
//}

