#include <iostream>
#include <vector>
using namespace std;


//class Solution {
//public:
//    int evalRPN(vector<string>& tokens) {
//        stack<int> num;  //数字栈，用来存储数字
//        for (auto& str : tokens)
//        {
//            //如果是表达式，那么需要将数字栈中栈顶两位数字拿出来
//            if (str == "+" || str == "-" || str == "*" || str == "/")
//            {
//                int right = num.top();
//                num.pop();
//                int left = num.top();
//                num.pop();
//                if (str == "+") num.push(left + right);
//                else if (str == "-") num.push(left - right);
//                else if (str == "*") num.push(left * right);
//                else num.push(left / right);
//            }
//            else
//            {
//                //入栈
//                num.push(stoi(str));
//            }
//        }
//        return num.top();
//    }
//};



//class Solution {
//public:
//    int getMin(string& str)
//    {
//        return stoi(str.substr(0, 2)) * 60 + stoi(str.substr(3, 2));
//    }
//    int findMinDifference(vector<string>& timePoints) {
//        //排序加鸽槽原理
//        //通过统计每一个时间的分钟数，然后做差来得出最小分钟数
//        sort(timePoints.begin(), timePoints.end());
//        int ret = INT_MAX;
//        int firstMin = getMin(timePoints[0]);
//        int preMin = firstMin;
//        int n = timePoints.size();
//        for (int i = 1; i < n; ++i)
//        {
//            int curMin = getMin(timePoints[i]);
//            ret = min(ret, curMin - preMin);
//            preMin = curMin;
//        }
//        //处理第一个和最后一个
//        ret = min(ret, firstMin + 1440 - preMin);
//        return ret;
//    }
//};



//class Solution {
//public:
//    int getMin(string& str)
//    {
//        return (int(str[0] - '0') * 10 + (str[1] - '0')) * 60 + int(str[3] - '0') * 10 + (str[4] - '0');
//    }
//    int findMinDifference(vector<string>& timePoints) {
//        //排序加鸽槽原理
//        //通过统计每一个时间的分钟数，然后做差来得出最小分钟数
//        sort(timePoints.begin(), timePoints.end());
//        int ret = INT_MAX;
//        int firstMin = getMin(timePoints[0]);
//        int preMin = firstMin;
//        int n = timePoints.size();
//        for (int i = 1; i < n; ++i)
//        {
//            int curMin = getMin(timePoints[i]);
//            ret = min(ret, curMin - preMin);
//            preMin = curMin;
//        }
//        //处理第一个和最后一个
//        ret = min(ret, firstMin + 1440 - preMin);
//        return ret;
//    }
//};


//class Solution {
//public:
//    int numDistinct(string s, string t) {
//        int m = s.size(), n = t.size();
//        s = ' ' + s;
//        t = ' ' + t;
//        vector<vector<double>> dp(m + 1, vector<double>(n + 1));
//        //初始化
//        for (int i = 0; i < m; ++i) dp[i][0] = 1;
//        for (int i = 1; i <= m; ++i)
//        {
//            for (int j = 1; j <= n; ++j)
//            {
//                dp[i][j] = dp[i - 1][j];
//                if (s[i] == t[j]) dp[i][j] += dp[i - 1][j - 1];
//            }
//        }
//        return dp[m][n];
//    }
//};


