#include <iostream>
#include <vector>
using namespace std;


//class Solution {
//public:
//    ListNode* ReverseList(ListNode* head)
//    {
//        if (head->next == nullptr) return head;  //一个的情况特殊处理
//        ListNode* n1 = head, * n2 = head, * n3 = nullptr;
//        while (n2)
//        {
//            n3 = n2->next;
//            n2->next = n1;
//            //迭代
//            n1 = n2;
//            n2 = n3;
//        }
//        return n1;
//    }
//    bool isPail(ListNode* head) {
//        //没有节点或者一个节点都是回文
//        if (head == nullptr || head->next == nullptr) return true;
//        //1.通过快慢指针的方法找到中间节点
//        ListNode* fast = head, * slow = head;
//        while (fast && fast->next)
//        {
//            fast = fast->next->next;
//            slow = slow->next;
//        }
//        //2.把后部分的节点进行反转
//        ListNode* mid = slow;
//        ListNode* righthead = ReverseList(mid);
//        //从左右同时出发比较
//        while (righthead != mid)
//        {
//            if (righthead->val != head->val) return false;
//            righthead = righthead->next;
//            head = head->next;
//        }
//        return true;
//    }
//};


//class Solution {
//    vector<int> path;
//    int ret = 0;  //用来收集结果
//public:
//    void dfs(TreeNode* root, int count, int sum)
//    {
//        if (root == nullptr) return;
//        count += root->val;
//        path.push_back(root->val);
//        int tmp = count;
//        if (count == sum) ++ret;
//        else if (count > sum)
//        {
//            //查看当前路径下是否有满足条件
//            for (auto e : path)
//            {
//                tmp -= e;
//                if (tmp == sum)
//                {
//                    ++ret;
//                    break;
//                }
//            }
//        }
//        //递归左右子树
//        dfs(root->left, count, sum);
//        dfs(root->right, count, sum);
//        //回溯
//        path.pop_back();
//        count -= root->val;
//    }
//    int FindPath(TreeNode* root, int sum) {
//        if (root == nullptr) return 0;
//        dfs(root, 0, sum);
//        return ret;
//    }
//};


//class Solution {
//    //用于遍历4个方向
//    int dx[4] = { 0,0,1,-1 };
//    int dy[4] = { 1,-1,0,0 };
//public:
//    int rotApple(vector<vector<int> >& grid) {
//        int count = -1;  //用于统计结果
//        int m = grid.size();
//        int n = grid[0].size();
//        queue<pair<int, int>> q;  //用来存放坐标
//        //1.使用队列把烂苹果放入到队列中
//        for (int i = 0; i < m; ++i)
//        {
//            for (int j = 0; j < n; ++j)
//            {
//                if (grid[i][j] == 2)
//                {
//                    q.push(make_pair(i, j));
//                }
//            }
//        }
//        //2.利用广度优先遍历来统计出结果
//        while (q.size())
//        {
//            int sz = q.size();
//            for (int i = 0; i < sz; ++i)
//            {
//                auto it = q.front();
//                q.pop();
//                //对当前周围苹果吞噬
//                for (int k = 0; k < 4; ++k)
//                {
//                    int x = it.first + dx[k], y = it.second + dy[k];
//                    //判断有效性
//                    if (x >= 0 && x < m && y >= 0 && y < n && grid[x][y] == 1)
//                    {
//                        cout << "x: " << x << "  y : " << y << endl;
//                        grid[x][y] = 2;  //这里吞噬完变成烂苹果了
//                        q.push(make_pair(x, y));
//                    }
//                }
//            }
//            ++count;
//        }
//        //3.最后遍历一次数组，如果有1，结果就是-1
//        bool flag = true;
//        for (int i = 0; i < m; ++i)
//        {
//            for (int j = 0; j < n; ++j)
//            {
//                if (grid[i][j] == 1)
//                {
//                    flag = false;
//                    break;
//                }
//            }
//            if (flag == false) break;
//        }
//        return flag == false ? -1 : count;
//    }
//};


