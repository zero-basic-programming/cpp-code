#pragma once
#include <iostream>
#include <vector>
#include <string>
using namespace std;

template <class K>
struct HashFunc
{
	size_t operator()(const K& key)
	{
		//有可能key是负数
		return (size_t)key;
	}
};

//特化
template <>
struct HashFunc<string>
{
	size_t operator()(const string& s)
	{
		size_t hash = 0;
		for (auto ch : s)
		{
			hash *= 131;//效果好
			hash += ch;
		}
		return hash;
	}
};
namespace closeHash
{
	enum State
	{
		EMPTY,
		EXIST,
		DELETE
	};
	template<class K,class V>
	struct HashNode
	{
		pair<K, V> _kv;
		State _s = EMPTY;
	};
	template<class K, class V,class Hash = HashFunc<K>>
	class HashTable
	{
		typedef HashNode<K, V> Node;
	public:
		HashTable()
			:_n(0)
		{
			_table.resize(10);//这里对扩容有很大的帮助
		}
		bool insert(const pair<K, V> kv)
		{
			//不能重复
			if (find(kv.first))
			{
				return false;
			}
			//扩容，使其效率提高
			if (_n * 10 / _table.size() >= 7)
			{
				HashTable<K, V> newtable;
				newtable._table.resize(2 * _table.size());
				//把旧表的数据放入新表
				for (auto& e : _table)
				{
					if(e._s == EXIST)
						newtable.insert(e._kv);
				}
				//交换即可，局部对象自动会销毁的
				_table.swap(newtable._table);
			}
			Hash hs;
			int hashi = hs(kv.first) % _table.size();
			//查看冲突,只要状态不是存在都是可以插入的
			while (_table[hashi]._s == EXIST)
			{
				++hashi;
				//这里可能回到最开始
				hashi %= _table.size();
			}
			//插入
			_table[hashi]._kv = kv;
			_table[hashi]._s = EXIST;
			++_n;
		}
		Node* find(const K& key)
		{
			Hash hs;
			int hashi = hs(key) % _table.size();
			while (_table[hashi]._s != EMPTY)
			{
				//存在且相同才是需要的
				if (_table[hashi]._s == EXIST && _table[hashi]._kv.first == key)
				{
					return &_table[hashi];
				}
				++hashi;
				hashi %= _table.size();
			}
			return nullptr;
		}
		bool erase(const K& key)
		{
			//找到然后改变状态即可
			Node* ret = find(key);
			if (ret)
			{
				ret->_s = DELETE;
				return true;
			}
			return false;
		}
	private:
		vector<Node> _table;
		int _n = 0;//用来记录个数
	};
	void testhashtable1()
	{
		int a[] = { 18, 8, 7, 27, 57, 3, 38, 18 };
		HashTable<int, int> ht;
		for (auto e : a)
		{
			ht.insert(make_pair(e, e));
		}
		ht.insert(make_pair(17, 17));
		ht.insert(make_pair(5, 5));

		cout << ht.find(7) << endl;
		cout << ht.find(8) << endl;
	}
	void testhashtable2()
	{
		string arr[] = { "苹果", "西瓜", "香蕉", "草莓", "苹果", "西瓜", "苹果", "苹果", "西瓜", "苹果", "香蕉", "苹果", "香蕉" };
		HashTable<string, int> ht;
		for (auto& e : arr)
		{
			auto ret = ht.find(e);
			if (ret)
			{
				ret->_kv.second++;
			}
			else
			{
				ht.insert(make_pair(e, 1));
			}
		}
	}
}