#include <iostream>
#include <vector>
using namespace std;

//class Solution {
//public:
//    string optimalDivision(vector<int>& nums) {
//        int n = nums.size();
//        if (n == 1)
//        {
//            return to_string(nums[0]);
//        }
//        if (n == 2) {
//            return to_string(nums[0]) + "/" + to_string(nums[1]);
//        }
//        //其他的情况,贪心，全部出了第二个全部放到分子上
//        string ret = to_string(nums[0]) + "/(" + to_string(nums[1]);
//        for (int i = 2; i < n; ++i)
//        {
//            ret += "/" + to_string(nums[i]);
//        }
//        ret += ")";
//        return ret;
//    }
//};


//class Solution {
//public:
//    TreeNode* pruneTree(TreeNode* root) {
//        if (root == nullptr) return nullptr;
//        //看root的左右子树是否为空
//        root->left = pruneTree(root->left);
//        root->right = pruneTree(root->right);
//        if (!root->left && !root->right && root->val == 0)
//        {
//            //删除当前节点
//            delete root;
//            root = nullptr;
//        }
//        return root;
//    }
//};


//class Solution {
//    long prev = LONG_MIN; //前面的那个节点的值
//public:
//    bool isValidBST(TreeNode* root) {
//        if (root == nullptr) return true; //空树也是一个二叉搜索树
//        bool retleft = isValidBST(root->left);
//        if (retleft == false) return false;
//        if (root->val <= prev)
//        {
//            //这里已经不是二叉搜索树了，剪枝
//            return false;
//        }
//        prev = root->val;
//        bool retright = isValidBST(root->right);
//        return retleft && retright;
//    }
//};


