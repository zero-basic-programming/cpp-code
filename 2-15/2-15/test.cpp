#include <iostream>
#include <vector>
#include <stack>
using namespace std;


//归并排序非递归版本
void MergeSortNonR(vector<int>& arr, vector<int>& tmp)
{
	int n = arr.size();
	int gap = 1;
	while (gap < n)
	{
		for (int i = 0; i < n; i += 2 * gap)
		{
			//单次归并的逻辑
			int begin1 = i, end1 = i + gap - 1;
			int begin2 = i + gap, end2 = i + 2 * gap - 1;
			//判断边界条件
			if (begin2 >= n) break;  //这里最后一组就空的，不需要排序
			if (end2 >= n) end2 = n - 1;
			//单次归并
			int j = i, k = i;  //j用于记录tmp数组，k用于后序还原
			while (begin1 <= end1 && begin2 <= end2)
			{
				tmp[j++] = arr[begin1] < arr[begin2] ? arr[begin1++] : arr[begin2++];
			}
			while (begin1 <= end1)
			{
				tmp[j++] = arr[begin1++];
			}
			while (begin2 <= end2)
			{
				tmp[j++] = arr[begin2++];
			}
			//还原
			for (; k <= end2; ++k)
				arr[k] = tmp[k];
		}
		gap *= 2;
	}
}

//升序排大堆
void AdjustDown(vector<int>& arr, int parent, int n)
{
	int child = 2 * parent + 1;
	while (child < n)
	{
		if (child + 1 < n && arr[child] < arr[child + 1])
		{
			++child;
		}
		if (arr[child] > arr[parent])
		{
			swap(arr[child], arr[parent]);
			//向下迭代
			parent = child;
			child = 2 * parent + 1;
		}
		else break;
	}
}

//堆排序
void HeapSort(vector<int>& arr)
{
	int n = arr.size();
	//向下调整建堆
	for (int i = (n - 1 - 1) / 2; i >= 0; --i)
	{
		AdjustDown(arr, i, n);
	}
	//交换堆顶数据和最后一个数据，然后向下调整
	for (int i = 1; i < n; ++i)
	{
		swap(arr[0], arr[n - i]);
		AdjustDown(arr, 0, n - i);
	}
}

//冒泡排序
void BufferSort(vector<int>& arr)
{
	int n = arr.size();
	for (int i = 1; i < n; ++i)  //趟数
	{
		bool flag = true;  //这里用来标记单趟排序是否有交换过数据
		for (int j = 0; j < n - i; ++j)
		{
			if (arr[j] > arr[j + 1])
			{
				swap(arr[j], arr[j + 1]);
				flag = false;
			}
		}
		if (flag) break;  //这里说明单趟排序没有移动过数据，说明已经有序
	}
}

//快排非递归版本
void QsortNonR(vector<int>& arr)
{
	int n = arr.size();
	int left = 0, right = n - 1;
	stack<int> st;
	st.push(left);
	st.push(right);
	while (st.size())
	{
		int r = st.top();
		st.pop();
		int l = st.top();
		st.pop();
		//选基准值
		int key = arr[rand() % (r - l + 1) + l];
		int left1 = l - 1, right1 = r + 1;
		int i = l;
		while (i < right1)
		{
			if (arr[i] < key) swap(arr[i++], arr[++left1]);
			else if (arr[i] == key) ++i;
			else swap(arr[i], arr[--right1]);
		}
		//[l,left1]  [right1,r]
		if (l < left1)
		{
			st.push(l);
			st.push(left1);
		}
		if (right1 < r)
		{
			st.push(right1);
			st.push(r);
		}
	}
}

//插入排序
void InsertSort(vector<int>& arr)
{
	int n = arr.size();
	for (int i = 0; i < n - 1; ++i)
	{
		int end = i;  
		int key = arr[end+1];
		while (end >= 0)
		{
			if (arr[end] > key)
			{
				arr[end + 1] = arr[end];
			}
			else break; //这里已经找到合适的位置进行插入
			--end;
		}
		arr[end + 1] = key;
	}
}

//希尔排序
void ShellSort(vector<int>& arr)
{
	int n = arr.size();
	int gap = n;
	while (gap > 1)
	{
		gap /= 2;
		for (int i = 0; i < n - gap; ++i)  //这里是每一组同时进行排序
		{
			int end = i;
			int key = arr[end + gap];
			while (end >= 0)
			{
				if (arr[end] > key)
				{
					arr[end + gap] = arr[end];
				}
				else break;
				end -= gap;
			}
			//插入
			arr[end + gap] = key;
		}
	}
}

//选择排序
void SelectSort(vector<int>& arr)
{
	int n = arr.size();
	int left = 0, right = n - 1;
	while (left < right)
	{
		int maxi = left, mini = left;
		for (int i = left; i <= right; ++i)
		{
			if (arr[i] > arr[maxi]) maxi = i;
			if (arr[i] < arr[mini]) mini = i;
		}
		//把这两个数分别放入到左右两个位置
		swap(arr[left], arr[mini]);
		//这里有可能maxi在left的位置
		if (maxi == left) maxi = mini;  //这里因为交换了，所以最大值在mini的位置
		swap(arr[maxi], arr[right]);
		++left; 
		--right;
	}
}


int main()
{
	vector<int> arr = { 2,5,3,7,8,5,6,9,9,0,2,5 };
	vector<int> tmp(arr);
	//MergeSortNonR(arr, tmp);
	//HeapSort(arr);
	//BufferSort(arr);
	//QsortNonR(arr);
	//InsertSort(arr);
	//ShellSort(arr);
	SelectSort(arr);
	for (auto e : arr)
	{
		cout << e << " ";
	}
	return 0;
}