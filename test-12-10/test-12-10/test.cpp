#include <iostream>
#include <vector>
using namespace std;


//class Solution {
//    bool row[9][10];
//    bool col[9][10];
//    bool lattic[3][3][10];
//public:
//    void solveSudoku(vector<vector<char>>& board) {
//        //初始化
//        for (int i = 0; i < 9; ++i)
//        {
//            for (int j = 0; j < 9; ++j)
//            {
//                if (board[i][j] != '.')
//                {
//                    int num = board[i][j] - '0';
//                    row[i][num] = col[j][num] = lattic[i / 3][j / 3][num] = true;
//                }
//            }
//        }
//        dfs(board);
//    }
//    bool dfs(vector<vector<char>>& board)
//    {
//        for (int i = 0; i < 9; ++i)
//        {
//            for (int j = 0; j < 9; ++j)
//            {
//                if (board[i][j] == '.')
//                {
//                    //从1到9中填写
//                    for (int num = 1; num <= 9; ++num)
//                    {
//                        if (row[i][num] || col[j][num] || lattic[i / 3][j / 3][num]) continue;
//                        board[i][j] = num + '0';
//                        row[i][num] = col[j][num] = lattic[i / 3][j / 3][num] = true;
//                        //这里说明返回的时候是正确的，就不需要继续遍历了，可以直接返回
//                        if (dfs(board)) return true;
//                        //回溯
//                        board[i][j] = '.';
//                        row[i][num] = col[j][num] = lattic[i / 3][j / 3][num] = false;
//                    }
//                    //这里全部数字尝试过也没有合适的直接返回false
//                    return false;
//                }
//            }
//        }
//        return true;
//    }
//};


//class Solution {
//    bool vis[7][7];
//    int m, n; //原数组的大小
//public:
//    bool exist(vector<vector<char>>& board, string word) {
//        m = board.size(), n = board[0].size();
//        //遍历完所有位置找第一个
//        for (int i = 0; i < m; ++i)
//        {
//            for (int j = 0; j < n; ++j)
//            {
//                if (word[0] == board[i][j])
//                {
//                    vis[i][j] = true;
//                    if (dfs(board, i, j, word, 1)) return true;
//                    vis[i][j] = false; //回溯
//                }
//            }
//        }
//        return false;
//    }
//    int dx[4] = { 0,0,-1,1 };
//    int dy[4] = { -1,1,0,0 };
//    bool dfs(vector<vector<char>>& board, int i, int j, string& word, int pos)
//    {
//        //返回值
//        if (pos == word.size())
//        {
//            return true;
//        }
//        //上下左右查找
//        for (int k = 0; k < 4; ++k)
//        {
//            int x = i + dx[k], y = j + dy[k];
//            //注意防止越界
//            if (x >= 0 && x < m && y >= 0 && y < n && !vis[x][y] && board[x][y] == word[pos])
//            {
//                vis[x][y] = true;
//                if (dfs(board, x, y, word, pos + 1)) return true;
//                //恢复现场
//                vis[x][y] = false;
//            }
//        }
//        return false;
//    }
//};


