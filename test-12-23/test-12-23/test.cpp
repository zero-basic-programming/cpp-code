#include <iostream>
#include <vector>
using namespace std;


class Solution {
public:
    int uniquePaths(int m, int n) {
        //到达m，n位置的递归
        vector<vector<int>> meno(m + 1, vector<int>(n + 1, 0));
        return dfs(m, n, meno);
    }
    int dfs(int i, int j, vector<vector<int>>& memo)
    {
        if (memo[i][j] != 0) return memo[i][j];
        //处理返回
        if (i == 0 || j == 0) return 0;
        if (i == 1 && j == 1)
        {
            memo[i][j] = 1;
            return 1;
        }
        memo[i][j] = dfs(i, j - 1, memo) + dfs(i - 1, j, memo);
        return memo[i][j];
    }
};


class Solution {
public:
    int lengthOfLIS(vector<int>& nums) {
        int n = nums.size();
        vector<int> memo(n, 0);
        int ret = 0;
        for (int i = 0; i < n; ++i)
        {
            ret = max(ret, dfs(i, memo, nums));
        }
        return ret;
    }
    int dfs(int pos, vector<int>& memo, vector<int>& nums)
    {
        if (memo[pos] != 0) return memo[pos];
        int ret = 1;
        for (int i = pos + 1; i < nums.size(); ++i)
        {
            if (nums[i] > nums[pos])
            {
                ret = max(ret, dfs(i, memo, nums) + 1);
            }
        }
        memo[pos] = ret;
        return ret;
    }
};


class Solution {
public:
    int lengthOfLIS(vector<int>& nums) {
        //动态规划
        int n = nums.size();
        vector<int> dp(n, 1);
        int ret = 1;
        for (int i = n - 1; i >= 0; --i)
        {
            for (int j = i + 1; j < n; ++j)
            {
                if (nums[j] > nums[i])
                    dp[i] = max(dp[i], dp[j] + 1);
            }
            ret = max(ret, dp[i]);
        }
        return ret;
    }
};




