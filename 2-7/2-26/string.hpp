#pragma once
#include <iostream>
#include <cassert>
using namespace std;

namespace liang
{
	class string
	{
	public:
		typedef char* iterator;
		typedef const char* const_iterator;
		string(const char* str = "")
		{
			_size = strlen(str);
			_capacity = _size;
			_str = new char[_capacity + 1];
			strcpy(_str, str);
		}
		string(const string& s)
		{
			string tmp(s._str);
			swap(tmp);
		}
		//赋值运算符重载
		string& operator=(string s)
		{
			swap(s);
			return *this;
		}
		~string()
		{
			delete[] _str;
			_str = nullptr;
			_size = _capacity = 0;
		}

		//迭代器相关函数
		iterator begin()
		{
			return _str;
		}
		iterator end()
		{
			return _str + _size;
		}
		const_iterator begin() const
		{
			return _str;
		}
		const_iterator end() const
		{
			return _str + _size;
		}

		//容器和大小相关函数
		size_t size()
		{
			return _size;
		}
		size_t capacity()
		{
			return _capacity;
		}
		//扩容
		void reserve(size_t n)
		{
			//这里我们不采用缩容的方式，效率低
			if (n > _capacity)
			{
				//这里可能还是出现了空的情况
				if (_str == nullptr)
				{
					_str = new char[n + 1];
				}
				else
				{
					char* str = new char[n + 1];  //这里需要存放'\0'
					strncpy(str, _str, _size + 1);
					delete[] _str;  //销毁原来的空间
					_str = str;
				}
				_capacity = n;
			}
		}
		void resize(size_t n, char ch = '\0')
		{
			if (n < _size)
			{
				_size = n;
				_str[n] = '\0';
			}
			else
			{
				if (n > _capacity)
				{
					//先扩容
					reserve(n);
				}
				//把后面全都改为ch
				for (int i = _size; i < n; ++i)
				{
					_str[i] = ch;
				}
				_str[n] = '\0';
			}
		}
		bool empty()const
		{
			return _size == 0;
		}

		//修改字符串相关函数
		void push_back(char ch)
		{
			//先查看是否需要扩容，然后尾插即可
			if (_size + 1 > _capacity)
			{
				//这里可能是第一次插入，那么就是需要初始化
				int capacity = _capacity == 0 ? 4 : 2 * _capacity;
				reserve(capacity);
				_capacity = capacity;
			}
			_str[_size] = ch;
			++_size;
			_str[_size] = '\0';
		}
		string& operator+=(char ch)
		{
			push_back(ch);
			return *this;
		}
		string& operator+=(const char* str)
		{
			int len = strlen(str);
			if (len + _size > _capacity)
			{
				//扩容
				reserve(len + _size);
			}
			int sz = _size;
			for (int i = 0; i < len; ++i)
			{
				_str[i + sz] = str[i];
				++_size;
			}
			_str[_size] = '\0';
			return *this;
		}
		//这个接口的效率不高，不建议使用
		string& insert(size_t pos, char ch)
		{
			//将pos后面的字符全部都转移到后一个位置
			//判断扩容
			if (_size + 1 > _capacity)
			{
				int capacity = _capacity == 0 ? 4 : 2 * _capacity;
				reserve(capacity);
			}
			int end = _size;
			while (end >= pos)
			{
				_str[end + 1] = _str[end];
				--end;
			}
			_str[pos] = ch;
			++_size;
			return *this;
		}
		string& insert(size_t pos, const char* str)
		{
			int len = strlen(str);
			if (_size + len > _capacity)
			{
				reserve(_size + len);
			}
			int end = _size;
			while (end >= pos)
			{
				_str[end + len] = _str[end];
				--end;
			}
			//把字符移动到字符串中
			for (int i = 0; i < len; ++i)
			{
				_str[pos + i] = str[i];
			}
			_size += len;
			return *this;
		}
		//覆盖式删除效率低，不建议使用
		string& erase(size_t pos, size_t len)
		{
			assert(len < _size);
			if (pos < _size)
			{
				int end = pos;
				while (end < _size)
				{
					_str[end] = _str[end + len];
					++end;
				}
			}
			--_size;
			return *this;
		}
		void clear()
		{
			delete[] _str;
			_str = nullptr;
			_size = _capacity = 0;
		}
		void swap(string& s)
		{
			_str = s._str;
			_size = s._size;
			_capacity = s._capacity;
		}
		const char* c_str() const
		{
			return _str;
		}

		//访问字符串函数
		char& operator[](size_t i)
		{
			return _str[i];
		}
		const char& operator[](size_t i) const
		{
			return _str[i];
		}
		size_t find(char ch, size_t pos = 0) const
		{
			for (int i = pos; i < _size; ++i)
			{
				if (_str[i] == ch) return i;
			}
			//没找到返回npos
			return npos;
		}

		//关系运算符重载
		bool operator>(const string& s)const
		{
			//按ASCLL来比较
			return strcmp(_str, s._str);
		}
	private:
		char* _str;   //存储字符串
		size_t _size;  //记录当前字符串的长度
		size_t _capacity;   //记录字符串当前的容量
		static const size_t npos = -1;  //整形最大值
	};
	istream& operator>>(istream& in, string& s)
	{
		s.clear();
		char ch = in.get();
		while (ch != ' ' && ch != '\n')
		{
			s += ch;
			ch = in.get();
		}
		return in;
	}
	ostream& operator<<(ostream& out, string& s)
	{
		out << s.c_str();
		return out;
	}
	istream& getline(istream& in, string& s)
	{
		s.clear();
		char ch;
		cin >> ch;
		while (ch != '\n')
		{
			s += ch;
			ch = in.get();
		}
		return in;
	}
	void teststring()
	{
		//string str("hello");
		//str.reserve(10);
		/*str.resize(10,'1');
		cout << str;*/

		/*string::iterator it = str.begin();
		cout << it;*/
		/*str.push_back('1');
		cout << str;*/

		//str += 'a';
		/*str += " world";
		cout << str;*/
		//str.insert(3, 'a');
		//str.insert(3, "nihao");
		//str.erase(1, 6);
		//cout << str;
		
		/*string str;
		cin >> str;
		cout << str;*/
	}
}