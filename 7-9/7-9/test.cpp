#include <iostream>
#include <vector>
using namespace std;

//class Solution {
//public:
//    bool Comp(vector<int>& hash1, vector<int>& hash2)
//    {
//        for (int i = 0; i < 26; ++i)
//        {
//            if (hash1[i] > 0 && hash2[i] > 0) return false;  //同一个字符出现
//        }
//        return true;
//    }
//    int maxProduct(vector<string>& words) {
//        int n = words.size();
//        int ret = 0;  //收集结果
//        vector<vector<int>> hash(n, vector<int>(26, 0));
//        //把每个单词的字符映射到hash中
//        for (int i = 0; i < n; ++i)
//        {
//            for (auto ch : words[i])
//            {
//                ++hash[i][ch - 'a'];
//            }
//        }
//        //开始两两比较
//        for (int i = 0; i < n; ++i)
//        {
//            for (int j = i + 1; j < n; ++j)
//            {
//                //比较hash中是否有重复的值
//                if (Comp(hash[i], hash[j]))
//                {
//                    int sz1 = words[i].size();
//                    int sz2 = words[j].size();
//                    ret = max(ret, sz1 * sz2);
//                }
//            }
//        }
//        return ret;
//    }
//};


//class Solution {
//public:
//    int maxProduct(vector<string>& words) {
//        int n = words.size();
//        int ret = 0;  //收集结果
//        //采用位图的方式，降低复杂度
//        vector<int> map(n);
//        //把每个单词的字符映射到hash中
//        for (int i = 0; i < n; ++i)
//        {
//            for (auto ch : words[i])
//            {
//                map[i] |= 1 << (ch - 'a');
//            }
//        }
//        //开始两两比较
//        for (int i = 0; i < n; ++i)
//        {
//            for (int j = i + 1; j < n; ++j)
//            {
//                if ((map[i] & map[j]) == 0)
//                {
//                    //这里说明这个字符是没有两边都同时出现过的
//                    int sz1 = words[i].size();
//                    int sz2 = words[j].size();
//                    ret = max(ret, sz1 * sz2);
//                }
//            }
//        }
//        return ret;
//    }
//};


//class Solution {
//public:
//    vector<int> twoSum(vector<int>& numbers, int target) {
//        //使用双向指针的思路，如果相加大就移动右指针减小，如果小了就移动左指针增大
//        int n = numbers.size();
//        int left = 0, right = n - 1;
//        while (left < right)
//        {
//            if (numbers[left] + numbers[right] < target) ++left;
//            else if (numbers[left] + numbers[right] > target) --right;
//            else return { left,right };
//        }
//        return { -1,-1 };
//    }
//};


//class Solution {
//    vector<string> ret;     //结果
//    string path;            //路径
//public:
//    void dfs(int n, int left, int right)
//    {
//        if (path.size() == 2 * n)
//        {
//            ret.push_back(path);
//            return;
//        }
//        //选择左括号
//        if (left < n)
//        {
//            path += '(';
//            dfs(n, left + 1, right);
//            path.pop_back();
//        }
//        //选择右括号，这里有前提就是左括号数量必须>右括号
//        if (left > right)
//        {
//            path += ')';
//            dfs(n, left, right + 1);
//            path.pop_back();
//        }
//    }
//    vector<string> generateParenthesis(int n) {
//        //采用回溯算法，对于满足括号生成的条件就是左括号必须>右括号的数量
//        //在一次选择中，我们可以选择左括号，也可以选择右括号
//        dfs(n, 0, 0);
//        return ret;
//    }
//};


