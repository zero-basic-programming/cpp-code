#include <iostream>
using namespace std;

struct ListNode {
	int val;
	ListNode* next;
	ListNode() : val(0), next(nullptr) {}
	ListNode(int x) : val(x), next(nullptr) {}
	ListNode(int x, ListNode* next) : val(x), next(next) {}
};

//class Solution {
//public:
//    void reorderList(ListNode* head) {
//        if (!head || !head->next) return;  //保证有两个节点以上
//        //找中间节点+反转链表+合并链表
//        ListNode* slow = head;
//        ListNode* fast = head;
//        while (fast && fast->next)
//        {
//            slow = slow->next;
//            fast = fast->next->next;
//        }
//        //这里slow就是中间节点，把链表分成两部分，后半部分进行反转
//        ListNode* n1 = slow;
//        ListNode* n2 = slow->next;
//        ListNode* n3 = nullptr;
//        //修改尾
//        slow->next = nullptr;
//        while (n2)
//        {
//            n3 = n2->next;
//            n2->next = n1;
//            //迭代
//            n1 = n2;
//            n2 = n3;
//        }
//        //合并链表
//        ListNode* tail = head;
//        n2 = n1;
//        n1 = head->next;
//        while (n1 && n2)
//        {
//            tail->next = n2;
//            n2 = n2->next;
//            tail = tail->next;
//            if (n2)
//            {
//                tail->next = n1;
//                n1 = n1->next;
//                tail = tail->next;
//            }
//        }
//        tail->next = nullptr;
//    }
//};
//
//int main()
//{
//    ListNode* n1 = new ListNode(1);
//    ListNode* n2 = new ListNode(2);
//    ListNode* n3 = new ListNode(3);
//    ListNode* n4 = new ListNode(4);
//    n1->next = n2;
//    n2->next = n3;
//    n3->next = n4;
//    n4->next = nullptr;
//    Solution().reorderList(n1);
//	return 0;
//}


//class Solution {
//    class cmp
//    {
//    public:
//        bool operator()(const ListNode* l1, const ListNode* l2)
//        {
//            return l1->val > l2->val;
//        }
//    };
//public:
//    ListNode* mergeKLists(vector<ListNode*>& lists) {
//        //创建堆来完成排序
//        priority_queue<ListNode*, vector<ListNode*>, cmp> heap;
//        //把第一个元素放入到堆中 
//        for (auto l : lists) heap.push(l);
//        //每次取出最小的值，然后放到新的链表中
//        ListNode* guard = new ListNode();
//        ListNode* tail = guard;
//        while (!heap.empty())
//        {
//            ListNode* top = heap.top();
//            heap.pop();
//            tail->next = top;
//            tail = tail->next;
//            //放入下个元素
//            if (top->next)  heap.push(top->next);
//        }
//        //处理后序工作
//        tail->next = nullptr;
//        ListNode* head = guard->next;
//        delete guard;
//        return head;
//    }
//};

