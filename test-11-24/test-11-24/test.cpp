#include <iostream>
#include <vector>
using namespace std;


//class Solution {
//public:
//    int jump(vector<int>& nums) {
//        int n = nums.size(), left = 0, right = 0, maxpos = 0, ret = 0;
//        while (left <= right)
//        {
//            if (maxpos >= n - 1)
//            {
//                return ret;
//            }
//            for (int i = left; i <= right; ++i)
//            {
//                maxpos = max(maxpos, i + nums[i]);
//            }
//            ++ret;
//            //迭代
//            left = right + 1;
//            right = maxpos;
//        }
//        return -1;
//    }
//};


//class Solution {
//public:
//    bool canJump(vector<int>& nums) {
//        int n = nums.size(), left = 0, right = 0, maxpos = 0;
//        while (left <= right)
//        {
//            if (maxpos >= n - 1) return true;
//            for (int i = left; i <= right; ++i)
//            {
//                maxpos = max(maxpos, i + nums[i]);
//            }
//            left = right + 1;
//            right = maxpos;
//        }
//        return false;
//    }
//};


void MergeSort(vector<int>& arr, vector<int>& tmp)
{
	int n = arr.size();
	int gap = 1;
	while (gap < n)
	{
		for (int i = 0; i < n; i += 2 * gap)
		{
			//[i,i+gap-1] [i+gap,i+2*gap-1]
			int begin1 = i, end1 = i + gap - 1;
			int begin2 = i + gap, end2 = i + 2 * gap - 1;
			int j = begin1,k = begin1;
			//处理边界条件
			if (begin2 >= n) break;
			if (end2 >= n) end2 = n - 1;
			//单次归并
			while (begin1 <= end1 && begin2 <= end2)
			{
				tmp[k++] = arr[begin1] < arr[begin2] ? arr[begin1++] : arr[begin2++];
			}
			while (begin1 <= end1)
			{
				tmp[k++] = arr[begin1++];
			}
			while (begin2 <= end2)
			{
				tmp[k++] = arr[begin2++];
			}
			//还原回原数组
			for (; j <= end2; ++j)
				arr[j] = tmp[j];
		}
		gap *= 2;
	}
}

int main()
{
	vector<int> arr = { 1,2,4,5,6,4,3,2,8,9,0 };
	vector<int> tmp(arr.size());
	MergeSort(arr, tmp);
	for (auto e : arr)
	{
		cout << e << " ";
	}
	return 0;
}