#include <iostream>
#include <vector>
using namespace std;

//class Solution {
//public:
//    bool isUnique(string astr) {
//        int n = astr.size();
//        if (n > 26) return false;
//        int ret = 0; //通过该变量来记录元素是否已经存在
//        for (auto ch : astr)
//        {
//            int i = ch - 'a';  //获取偏移量
//            //判断当前偏移量的坐标是否已经有字符
//            if ((ret >> i) & 1) return false;
//            //放入到位图中
//            ret |= (1 << i);
//        }
//        return true;
//    }
//};


//class Solution {
//public:
//    int missingNumber(vector<int>& nums) {
//        //利用两次异或解决
//        int n = nums.size();
//        int ret = 0;
//        for (int i = 0; i <= n; ++i) ret ^= i;
//        for (auto e : nums) ret ^= e;
//        return ret;
//    }
//};


//class Solution {
//public:
//    int getSum(int a, int b) {
//        while (b != 0)  //b为进位
//        {
//            int ret = a ^ b;  //无进位相加
//            int carry = a & b;  //相加的进位
//            a = ret;
//            b = (carry << 1);  //移动进位
//        }
//        return a;
//    }
//};


//class Solution {
//public:
//    int singleNumber(vector<int>& nums) {
//        //遍历每一位
//        int ret = 0;
//        for (int i = 0; i < 32; ++i)
//        {
//            int sum = 0;
//            for (auto e : nums)
//            {
//                //遍历每一个元素，把当前位相加
//                if ((e >> i) & 1) sum += 1;
//            }
//            sum %= 3; //这里是将除了目标元素以外的元素都排除
//            if (sum == 1) ret |= (1 << i);
//        }
//        return ret;
//    }
//};


//class Solution {
//public:
//    vector<int> missingTwo(vector<int>& nums) {
//        int n = nums.size() + 2; //获得其中的总个数
//        int res = 0;
//        for (int i = 1; i <= n; ++i) res ^= i;
//        for (auto e : nums) res ^= e;
//        //找res的位数为1的数
//        int diff = 0;
//        for (int i = 0; i < 32; ++i)
//        {
//            if ((res >> i) & 1)
//            {
//                diff = i;
//                break;
//            }
//        }
//        //把当前位置不同的两个数分成两组，然后进行异或就可以得出结果
//        int a = 0, b = 0;  //a统计不同位的1
//        for (int i = 1; i <= n; ++i)
//        {
//            if ((i >> diff) & 1) a ^= i;
//            else b ^= i;
//        }
//        for (auto e : nums)
//        {
//            if ((e >> diff) & 1) a ^= e;
//            else b ^= e;
//        }
//        return { a,b };
//    }
//};
//
//
//int main()
//{
//    vector<int> arr = { 2 };
//    Solution().missingTwo(arr);
//    return 0;
//}


//class Solution {
//public:
//    int maxSubarraySumCircular(vector<int>& nums) {
//        int n = nums.size();
//        vector<int> f(n + 1);  //用来统计最大的连续子数组和
//        vector<int> g(n + 1);  //用来统计最小的连续子数组和
//        int sum = 0; //用来统计数组的总和
//        int fmax = INT_MIN;  //用来统计最大
//        int gmin = INT_MAX;  //用来统计最小
//        for (int i = 1; i <= n; ++i)
//        {
//            sum += nums[i - 1];
//            f[i] = max(f[i - 1] + nums[i - 1], nums[i - 1]);
//            g[i] = min(g[i - 1] + nums[i - 1], nums[i - 1]);
//            fmax = max(fmax, f[i]);
//            gmin = min(gmin, g[i]);
//        }
//        //特判数组全是负数的情况
//        if (sum == gmin) return fmax;
//        return fmax > (sum - gmin) ? fmax : sum - gmin;
//    }
//};


//class Solution {
//public:
//    int maxProduct(vector<int>& nums) {
//        int n = nums.size();
//        vector<int> f(n + 1, 1);  //表示到i位置为结尾的最大子数组
//        vector<int> g(n + 1, 1);  //最小子数组
//        int ret = INT_MIN;
//        for (int i = 1; i <= n; ++i)
//        {
//            int x = nums[i - 1];
//            int y = f[i - 1] * nums[i - 1];
//            int z = g[i - 1] * nums[i - 1];
//            f[i] = max(x, max(y, z));
//            g[i] = min(x, min(y, z));
//            ret = max(ret, f[i]);
//        }
//        return ret;
//    }
//};


