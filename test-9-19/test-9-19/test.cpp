#include <iostream>
#include <vector>
#include <string>
using namespace std;


//class Solution {
//public:
//    string convert(string s, int numRows) {
//        int n = s.size();
//        vector<vector<char>> vv(numRows, vector<char>(n, ' '));
//        int i = 0, j = 0, tmp = n, k = 0;
//        while (k < n)
//        {
//            //列
//            for (; i < n; ++i)
//            {
//                vv[i][j] = s[k++];
//                if (k == n) break;
//            }
//            if (k == n) break;
//            //斜对角
//            ++j;
//            for (; i > 0; --i, ++j)
//            {
//                if (k == n) break;
//                vv[i][j] = s[k++];
//            }
//        }
//        string ret;
//        for (auto& v : vv)
//        {
//            for (auto ch : v)
//            {
//                if (ch != ' ')
//                    ret += ch;
//            }
//        }
//        return ret;
//    }
//};


//class Solution {
//public:
//    string countAndSay(int n) {
//        string tmp = "1";
//        string ret = "1";
//        for (int i = 0; i < n; ++i)
//        {
//            char num = tmp[0];
//            int count = 1;
//            for (int i = 0; i < tmp.size(); ++i)
//            {
//                if (tmp[i] != num)
//                {
//                    ret += to_string(count) + to_string(num);
//                    num = tmp[i];
//                    count = 1;
//                    tmp = ret;
//                    ret.clear();
//                }
//            }
//            if (i != n - 1)
//            {
//                //每次到最后也需要更新结果
//                ret += to_string(count) + num;
//                tmp = ret;
//                ret.clear();
//            }
//        }
//        return ret;
//    }
//};
//
//
//int main()
//{
//    string ret = Solution().countAndSay(4);
//    cout << ret << endl;
//    return 0;
//}



//class Solution{
//public:
//    int minNumberOfFrogs(string croakOfFrogs) {
//        vector<int> hashi(26);
//        string tmp = "croak";
//        int i = 0;
//        //记录字符和下标的映射关系
//        for (auto ch : tmp)   hashi[ch - 'a'] = i,++i;
//        //记录字符和次数的映射关系
//        vector<int> hash(5);
//        for (auto ch : tmp) hash[ch - 'a'] = 0;
//        //通过两个hash表统计出结果
//        for (auto ch : croakOfFrogs)
//        {
//            if (ch == 'c') {
//                //这里需要看后面的k字符的个数
//                if (hash[hashi['k' - 'a']] != 0)
//                {
//                    --hash[hashi['k' - 'a']];
//                }
//                ++hash[0];
//            }
//            else { //其他的几种情况
//                hash[hashi[ch - 'a'] - 1]--;
//                hash[hashi[ch - 'a']]++;
//            }
//        }
//        int ret = 0;
//        for (int i = 0; i < 4; ++i)
//        {
//            if (hash[i] != 0) return -1;
//        }
//        return hash[4];
//    }
//};



//class Solution {
//public:
//    int lenLongestFibSubseq(vector<int>& arr) {
//        int n = arr.size();
//        vector<vector<int>> dp(n, vector<int>(n, 2));
//        unordered_map<int, int> hash; // 用来记录数字和下标的映射，降低时间复杂度
//        int ret = 2;
//        for (int i = 0; i < n; ++i)
//        {
//            for (int j = i - 1; j > 0; --j)
//            {
//                int c = arr[i] - arr[j];
//                if (hash.find(c) == hash.end())
//                {
//                    //没找到直接在该位置填写2，
//                    dp[j][i] = 2;
//                }
//                else
//                {
//                    //找到了，但是可能找到的是j之后的结果
//                    if (hash[c] > arr[j])
//                    {
//                        dp[j][i] = 2;
//                    }
//                    else {
//                        int k = hash[c];
//                        dp[j][i] = dp[k][j] + 1;
//                    }
//                }
//                ret = max(ret, dp[j][i]);
//            }
//            //每次得到结果都需要放入hash中
//            hash[arr[i]] = i;
//        }
//        return ret > 3 ? ret : 0;
//    }
//};