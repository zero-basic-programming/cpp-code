#include <iostream>
#include <vector>
using namespace std;


//class MedianFinder{
//    priority_queue<int> leftheap; //左边的建大堆
//    priority_queue<int,vector<int>,greater<int>> rightheap; //右边建小堆
//public:
//    MedianFinder() {
//    }
//
//    void addNum(int num) {
//        //判断当前左右堆的大小来分情况,但是始终保证左边大于等于右边
//        if (leftheap.size() == rightheap.size())
//        {
//            //num小于左边直接放入左边
//            if (leftheap.empty() || num <= leftheap.top())
//            {
//                leftheap.push(num);
//            }
//            else
//            {
//                //大于左边就把元素插入到右边，并把右边的堆顶元素放入到左边
//                rightheap.push(num);
//                leftheap.push(rightheap.top());
//                rightheap.pop();
//            }
//        }
//        else { //左边大于右边
//            if (num <= leftheap.top())
//            {
//                //插入左边，并把左边堆顶元素放入到右边
//                leftheap.push(num);
//                rightheap.push(leftheap.top());
//                leftheap.pop();
//            }
//            else
//            {
//                //直接插入右边
//                rightheap.push(num);
//            }
//        }
//    }
//
//    double findMedian() {
//        if (leftheap.size() == rightheap.size())
//            return (leftheap.top() + rightheap.top()) / 2.0;
//        return leftheap.top();
//    }
//};


//class Solution {
//public:
//    int widthOfBinaryTree(TreeNode* root) {
//        unsigned int ret = 0;
//        vector<pair<unsigned int, TreeNode*>> q;  //下标和节点的映射
//        q.push_back(make_pair(1, root));
//        while (!q.empty())
//        {
//            auto& [x1, y1] = q[0];
//            auto& [x2, y2] = q.back();
//            //比较得出结果
//            ret = max(ret, x2 - x1 + 1);
//            vector<pair<unsigned int, TreeNode*>> tmp;
//            for (auto& [x, y] : q)
//            {
//                if (y->left) tmp.push_back(make_pair(2 * x, y->left));
//                if (y->right) tmp.push_back(make_pair(2 * x + 1, y->right));
//            }
//            q = tmp;
//        }
//        return ret;
//    }
//};


