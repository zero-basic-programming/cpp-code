#include <cmath>
#include <iostream>
#include <ostream>
#include <regex>
#include <math.h>
using namespace std;

int main() {
    int n;
    cin >> n;
    int count = 0;//统计个数
    for (int i = 0; i <= n; ++i)
    {
        int sum = 0;//统计i的位数
        int tmp = i;
        while (tmp)
        {
            tmp /= 10;
            ++sum;
        }
        int id = 10 * tmp;
        if (id == 0  || (i*i) % id == i)
        {
            ++count;
        }
    }
    cout << count << endl;
    return 0;
}
