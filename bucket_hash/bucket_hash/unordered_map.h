﻿#pragma once
#include "bucket_hash.h"

namespace um
{
	template <class K,class V,class Hash = HashFunc<K>>
	class unordered_map
	{
		struct MapKeyOfT
		{
			const K& operator()(const pair<const K, V>& kv)
			{
				return kv.first;
			}
		};
	public:
		typedef typename liang::HashTable<K, pair<const K, V>, Hash, MapKeyOfT>::iterator iterator;
		pair<iterator, bool> insert(const pair<K,V>& kv)
		{
			return _ht.insert(kv);
		}
		iterator find(const K& key)
		{
			return _ht.find(key);
		}
		bool erase(const K& key)
		{
			return _ht.erase(key);
		}
		V& operator[](const K& key)
		{
			pair<iterator, bool> it = _ht.insert(make_pair(key,V()));
			return it.first->second;
		}
		iterator begin()
		{
			return _ht.begin();
		}
		iterator end()
		{
			return _ht.end();
		}
	private:
		liang::HashTable<K,pair<const K,V>, Hash, MapKeyOfT> _ht;
	};
	void test_unordered_map()
	{
		string arr[] = { "苹果", "西瓜", "香蕉", "草莓", "苹果", "西瓜", "苹果", "苹果", "西瓜", "苹果", "香蕉", "苹果", "香蕉" };

		unordered_map<string, int> countMap;
		for (auto& e : arr)
		{
			countMap[e]++;
		}
		unordered_map<string, int>::iterator it = countMap.begin();
		/*while (it != countMap.end())
		{
			cout << (*it).first << (*it).second << endl;
			++it;
		}*/
		for (const auto& kv : countMap)
		{
			cout << kv.first << ":" << kv.second << endl;
		}
	}
}