#pragma once
#include <iostream>
using namespace std;
#include <vector>


template <class K>
struct HashFunc
{
	size_t operator()(const K& key)
	{
		return (size_t)key;
	}
};

//针对字符串类的特化
template<>
struct HashFunc<string>
{
	size_t operator()(const string& s)
	{
		size_t tmp = 1;
		for (auto ch : s)
		{
			tmp *= 131;
			tmp += ch;
		}
		return tmp;
	}
};

namespace liang
{
	//节点类
	template <class T>
	struct HashNode
	{
		T _data;
		HashNode<T>* _next;
		HashNode(const T& data)
			:_data(data)
			,_next(nullptr)
		{}
	};
	//先声明，迭代器类要使用
	template <class K, class T, class Hash,class KeyOfT>
	class HashTable;

	//迭代器类
	template <class K,class T,class Hash,class KeyOfT>
	struct HTiterator
	{
		typedef HashNode<T> Node;
		typedef HashTable<K, T, Hash,KeyOfT> HT;
		typedef HTiterator<K, T,Hash,KeyOfT> Self;
		Node* _node;
		HT* _ht;
		HTiterator(Node* node,HT* ht)
			:_node(node)
			,_ht(ht)
		{}
		T& operator*()
		{
			return _node->_data;
		}
		T* operator->()
		{
			return &_node->_data;
		}
		bool operator!=(const Self& s) const
		{
			return _node != s._node;
		}
		Self& operator++()
		{
			KeyOfT kot;
			if (_node->_next)
			{
				_node = _node->_next;
			}
			else
			{
				size_t hashi = Hash()(kot(_node->_data)) % _ht->_table.size();
				++hashi;
				while (hashi < _ht->_table.size())
				{
					if (_ht->_table[hashi])
					{
						_node = _ht->_table[hashi];
						break;
					}
					++hashi;
				}
				if (hashi == _ht->_table.size())
					_node = nullptr;

				return *this;
			}
		}
	};
	//哈希表
	template <class K,class T,class Hash,class KeyOfT>
	class HashTable
	{
		typedef HashNode<T>  Node;
		//因为迭代器类可能会访问HashTable的私有成员，这里可以设为友元
		template <class K, class T, class Hash, class KeyOfT>
		friend struct HTiterator;
	public:
		typedef HTiterator<K, T, Hash, KeyOfT> iterator;
		HashTable()
		{
			//_table.resize(10,nullptr);
			_table.resize(__stl_next_prime(0), nullptr);
		}
		~HashTable()
		{
			for (int i = 0; i < _table.size(); ++i)
			{
				Node* cur = _table[i];
				while (cur)
				{
					Node* next = cur->_next;
					delete cur;
					cur = next;
				}
				_table[i] = nullptr;
			}
		}
		iterator begin()
		{
			for (int hashi = 0; hashi < _table.size(); ++hashi)
			{
				if (_table[hashi])
				{
					return iterator(_table[hashi], this);
				}
			}
			//表为空
			return iterator(nullptr, this);
		}

		iterator end()
		{
			return iterator(nullptr, this);
		}

		pair<iterator,bool> insert(const T& data)
		{
			//如果找到了就不插入了
			KeyOfT kot;
			auto it = find(kot(data));
			if (it != end())
			{
				return make_pair(it, false);
			}
			//判断是否需要扩容
			if (_n == _table.size())
			{
				//遍历旧表，把旧表中的数据全部拖到新表中
				vector<Node*> newtable;
				//newtable.resize(2 * _table.size(), nullptr);
				newtable.resize(__stl_next_prime(_table.size()), nullptr);
				for (int i = 0; i < _table.size(); ++i)
				{
					Node* cur = _table[i];
					while (cur)
					{
						Node* next = cur->_next;
						//把该节点插入新表中
						int hashi = Hash()(kot(cur->_data)) % newtable.size();
						//链接起来
						cur->_next = newtable[hashi];
						newtable[hashi] = cur;
						//迭代更新
						cur = next;
					}
					_table[i] = nullptr;
				}
				//交换两个表
				_table.swap(newtable);
			}
			//插入数据
			int hashi = Hash()(kot(data)) % _table.size();
			Node* newnode = new Node(data);
			newnode->_next = _table[hashi];
			_table[hashi] = newnode;
			++_n;
			return make_pair(iterator(newnode, this), true);
		}
		iterator find(const K& key)
		{
			int hashi = Hash()(key) % _table.size();
			Node* cur = _table[hashi];
			while (cur)
			{
				if (KeyOfT()((cur->_data)) == key)
				{
					return iterator(cur,this);
				}
				cur = cur->_next;
			}
			return end();
		}
		bool erase(const K& key)
		{
			int hashi = Hash()(key) % _table.size();
			Node* cur = _table[hashi];
			Node* prev = nullptr;
			while (cur)
			{
				if (cur->_kv.first == key)
				{
					//删除
					if (prev == nullptr)
					{
						//头删
						_table[hashi] = nullptr;
					}
					else
					{
						prev->_next = cur->_next;
					}
					delete cur;
					--_n;
					return true;
				}
				else
				{
					prev = cur;
					cur = cur->_next;
				}
			}
			return false;
		}
		//使用质数进行扩容可以有效的防止hash冲突
		inline unsigned long __stl_next_prime(unsigned long n)
		{
			static const int __stl_num_primes = 28;
			static const unsigned long __stl_prime_list[__stl_num_primes] =
			{
				53, 97, 193, 389, 769,
				1543, 3079, 6151, 12289, 24593,
				49157, 98317, 196613, 393241, 786433,
				1572869, 3145739, 6291469, 12582917, 25165843,
				50331653, 100663319, 201326611, 402653189, 805306457,
				1610612741, 3221225473, 4294967291
			};

			for (int i = 0; i < __stl_num_primes; ++i)
			{
				if (__stl_prime_list[i] > n)
				{
					return __stl_prime_list[i];
				}
			}

			return __stl_prime_list[__stl_num_primes - 1];
		}

	private:
		vector<Node*> _table;
		size_t _n = 0; //元素个数
	};
}