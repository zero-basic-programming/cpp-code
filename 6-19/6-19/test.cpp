#include <iostream>
#include <vector>
using namespace std;

//class Solution {
//public:
//    bool isBalanced(TreeNode* root) {
//        //使用层序遍历得方式去获取其最大深度和最小深度
//        queue<TreeNode*> q;
//        int minlevel = 10001;
//        int level = 0;  //记录最大层数
//        q.push(root);
//        while (q.size())
//        {
//            int sz = q.size();
//            for (int i = 0; i < sz; ++i)
//            {
//                TreeNode* node = q.front();
//                q.pop();
//                //判断当前节点的左右子节点是否都为空，如果都为空就有最小层数
//                if (!node->left && !node->right) minlevel = min(minlevel, level);
//                if (node->left) q.push(node->left);
//                if (node->right) q.push(node->right);
//            }
//            ++level;
//        }
//        return level - minlevel <= 1;
//    }
//};

//class Solution {
//public:
//    bool isBalanced(TreeNode* root) {
//        //使用层序遍历得方式去获取其最大深度和最小深度
//        if (root == nullptr) return true;
//        queue<TreeNode*> q;
//        int minlevel = 10001;
//        int level = 0;  //记录最大层数
//        q.push(root);
//        while (q.size())
//        {
//            int sz = q.size();
//            for (int i = 0; i < sz; ++i)
//            {
//                TreeNode* node = q.front();
//                q.pop();
//                //判断当前节点的左右子节点是否都为空，如果都为空就有最小层数
//                if (!node->left && !node->right) minlevel = min(minlevel, level);
//                if (node->left) q.push(node->left);
//                if (node->right) q.push(node->right);
//            }
//            ++level;
//        }
//        cout << level << minlevel << endl;
//        return level - minlevel <= 2;
//    }
//};


