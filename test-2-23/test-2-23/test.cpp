#include <iostream>
using namespace std;

//class Solution {
//public:
//    int firstUniqChar(string s) {
//        //使用哈希映射思想
//        int hash[27] = { 0 };
//        //遍历统计次数
//        for (int i = 0; i < s.size(); ++i)
//        {
//            hash[s[i] - 'a']++;
//        }
//        //查找
//        for (int i = 0; i < s.size(); ++i)
//        {
//            if (hash[s[i] - 'a'] == 1)
//            {
//                return i;
//            }
//        }
//        return -1;
//    }
//};

//class Solution {
//public:
//    int repeatedNTimes(vector<int>& nums) {
//        //使用unordered map 把元素放入其中，然后再看哪个元素出现的次数等于N
//        unordered_map<int, int> m;
//        for (auto& e : nums)
//        {
//            m[e]++;
//        }
//        //查找出现n次的
//        int n = nums.size() / 2;
//        for (auto& e : m)
//        {
//            if (e.second == n)
//            {
//                return e.first;
//            }
//        }
//        return 0;
//    }
//};
//int main()
//{
//
//	return 0;
//}

//class Solution {
//public:
//    vector<int> intersection(vector<int>& nums1, vector<int>& nums2) {
//        //我们可以采用unordered_set 对其进行去重，然后在其中一个集合找另一个集合的元素即可
//        vector<int> result;
//        unordered_set<int> s1;
//        unordered_set<int> s2;
//        for (auto& e : nums1)
//        {
//            s1.insert(e);
//        }
//        for (auto& e : nums2)
//        {
//            s2.insert(e);
//        }
//        //在遍历s1的时候找s2
//        for (auto& e : s1)
//        {
//            if (s2.find(e) != s2.end())
//            {
//                result.push_back(e);
//            }
//        }
//        return result;
//    }
//};

