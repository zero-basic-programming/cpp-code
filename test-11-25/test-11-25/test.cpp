#include <iostream>
#include <vector>
using namespace std;

//class Solution {
//    vector<string> ret;
//public:
//    void dfs(TreeNode* root, string path)
//    {
//        //返回值
//        if (root->left == nullptr && root->right == nullptr)
//        {
//            //叶子节点
//            path += to_string(root->val);
//            ret.push_back(path);
//            return;
//        }
//        path += to_string(root->val) + "->";
//        //简单的剪枝
//        if (root->left) dfs(root->left, path);
//        if (root->right) dfs(root->right, path);
//    }
//    vector<string> binaryTreePaths(TreeNode* root) {
//        string path;
//        dfs(root, path);
//        return ret;
//    }
//};


//class Solution {
//    vector<string> ret;
//public:
//    void dfs(TreeNode* root, string path)
//    {
//        path += to_string(root->val);
//        //返回值
//        if (root->left == nullptr && root->right == nullptr)
//        {
//            //叶子节点
//            ret.push_back(path);
//            return;
//        }
//        path += "->";
//        //简单的剪枝
//        if (root->left) dfs(root->left, path);
//        if (root->right) dfs(root->right, path);
//    }
//    vector<string> binaryTreePaths(TreeNode* root) {
//        string path;
//        if (root == nullptr) return ret;
//        dfs(root, path);
//        return ret;
//    }
//};


//class Solution {
//public:
//    int canCompleteCircuit(vector<int>& gas, vector<int>& cost) {
//        int n = gas.size();
//        int total = 0; //总差值
//        int tmp = 0;//当前差值
//        int idx = -1; //结果
//        for (int i = 0; i < n; ++i)
//        {
//            int gap = gap[i] - cost[i];
//            total += gap;
//            if (tmp > 0 && tmp + gap >= 0) continue;
//            else
//            {
//                tmp = 0;
//                idx = -1;
//            }
//        }
//        return  total < 0 ? -1 : idx;
//    }
//};


//class Solution {
//public:
//    int canCompleteCircuit(vector<int>& gas, vector<int>& cost) {
//        int n = gas.size();
//        for (int i = 0; i < n; ++i)
//        {
//            int sum = 0;
//            for (int sep = 0; sep < n; ++sep)
//            {
//                int idx = (i + sep) % n;
//                sum += (gas[idx] - cost[idx]);
//                if (sum < 0) break;
//            }
//            if (sum >= 0) return i;
//        }
//        return -1;
//    }
//};


//class Solution {
//public:
//    int canCompleteCircuit(vector<int>& gas, vector<int>& cost) {
//        int n = gas.size();
//        for (int i = 0; i < n; ++i)
//        {
//            int sum = 0;
//            int sep = 0;
//            for (; sep < n; ++sep)
//            {
//                int idx = (i + sep) % n;
//                sum += (gas[idx] - cost[idx]);
//                if (sum < 0) break;
//            }
//            if (sum >= 0) return i;
//            i += sep;
//        }
//        return -1;
//    }
//};


//class Solution {
//public:
//    bool IsTrue(int n)
//    {
//        //后面的位数要大于等于前面的位数
//        int tail = INT_MAX;
//        while (n > 0)
//        {
//            int tmp = n % 10;
//            if (tmp > tail) return false;
//            n /= 10;
//            tail = tmp;
//        }
//        return true;
//    }
//    int monotoneIncreasingDigits(int n) {
//        for (int i = n; i >= 0; --i)
//        {
//            if (IsTrue(i)) return i;
//        }
//        return -1;
//    }
//};


