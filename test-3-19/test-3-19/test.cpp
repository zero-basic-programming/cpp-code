#include <iostream>
using namespace std;

//class Solution {
//public:
//    vector<vector<int>> threeSum(vector<int>& nums) {
//        //使用双指针的思路，控制左右边界，让第三个指针处理即可
//        vector<vector<int>> result;
//        sort(nums.begin(), nums.end());
//        for (int i = 0; i < nums.size(); ++i)
//        {
//            //因为排序过，所以如果第i个大于0就不符合
//            if (nums[i] > 0)
//            {
//                return result;
//            }
//            //对a去重
//            if (i > 0 && nums[i] == nums[i - 1])
//            {
//                continue;
//            }
//            //找第二，三个数
//            int left = i + 1;
//            int right = nums.size() - 1;
//            while (left < right)
//            {
//                //保存结果
//                if (nums[i] + nums[left] + nums[right] == 0)
//                {
//                    result.push_back({ nums[i],nums[left],nums[right] });
//                    //去重b和c
//                    while (left < right && nums[left] == nums[left + 1]) ++left;
//                    while (left < right && nums[right] == nums[right - 1]) --right;
//                    //更新left right
//                    ++left;
//                    --right;
//                }
//                else if (nums[i] + nums[left] + nums[right] > 0)
//                {
//                    --right;
//                }
//                else
//                {
//                    ++left;
//                }
//            }
//        }
//        return result;
//    }
//};


