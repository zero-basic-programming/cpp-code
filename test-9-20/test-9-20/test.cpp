#include <iostream>
#include <vector>
#include <unordered_map>

using namespace std;

//class Solution {
//public:
//    int lenLongestFibSubseq(vector<int>& arr) {
//        int n = arr.size();
//        vector<vector<int>> dp(n, vector<int>(n, 2));
//        unordered_map<int, int> hash; // 用来记录数字和下标的映射，降低时间复杂度
//        int ret = 2;
//        for (int i = 0; i < n; ++i)
//        {
//            for (int j = i - 1; j > 0; --j)
//            {
//                int c = arr[i] - arr[j];
//                if (hash.find(c) == hash.end())
//                {
//                    //没找到直接在该位置填写2
//                    dp[j][i] = 2;
//                }
//                else
//                {
//                    //找到了，但是可能找到的是j以及之后的结果
//                    if (c >= arr[j])
//                    {
//                        dp[j][i] = 2;
//                    }
//                    else {
//                        int k = hash[c];
//                        dp[j][i] = dp[k][j] + 1;
//                        cout << arr[k] << " " << arr[j] << " " << arr[i] << endl;
//                    }
//                }
//                ret = max(ret, dp[j][i]);
//            }
//            //每次得到结果都需要放入hash中
//            hash[arr[i]] = i;
//        }
//        return ret >= 3 ? ret : 0;
//    }
//};


//class Solution {
//public:
//    int minNumberOfFrogs(string croakOfFrogs) {
//        vector<int> hashi(26);
//        string tmp = "croak";
//        int i = 0;
//        //记录字符和下标的映射关系
//        for (auto ch : tmp)   hashi[ch - 'a'] = i, ++i;
//        //记录字符和次数的映射关系
//        vector<int> hash(5);
//        for (auto ch : tmp) hash[hashi[ch - 'a']] = 0;
//        //通过两个hash表统计出结果
//        for (auto ch : croakOfFrogs)
//        {
//            if (ch == 'c') {
//                //这里需要看后面的k字符的个数
//                if (hash[hashi['k' - 'a']] != 0)
//                {
//                    --hash[hashi['k' - 'a']];
//                }
//                ++hash[0];
//            }
//            else { //其他的几种情况
//                //如果前面没有字符，说明有问题，直接返回-1
//                if (hash[hashi[ch - 'a'] - 1]-- == 0) return -1;
//                hash[hashi[ch - 'a']]++;
//            }
//        }
//        int ret = 0;
//        for (int i = 0; i < 4; ++i)
//        {
//            if (hash[i] != 0) return -1;
//        }
//        return hash[4];
//    }
//};

//class Solution {
//public:
//    string countAndSay(int n) {
//        string ret = "1";
//        for (int i = 1; i < n; ++i)
//        {
//            string tmp;
//            int len = ret.size();
//            for (int left = 0, right = 0; right < len;)
//            {
//                while (right < len && ret[left] == ret[right]) ++right;
//                tmp += to_string(right - left) + ret[left];
//                left = right;
//            }
//            ret = tmp;
//        }
//        return ret;
//    }
//};