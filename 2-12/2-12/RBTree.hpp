#pragma once
#include <iostream>
using namespace std;


enum Color
{
	RED,
	BLACK
};

//节点类
template <class K,class V>
struct RBTreeNode
{
	//指针
	RBTreeNode<K, V>* _parent;
	RBTreeNode<K, V>* _left;
	RBTreeNode<K, V>* _right;
	//存储数据
	pair<K, V> _kv;
	//颜色
	Color _col;
	//构造函数
	RBTreeNode(const pair<K,V>& kv,Color col = RED)  //默认插入的节点是红色的，这样可以减少调整次数
		:_parent(nullptr),_left(nullptr),_right(nullptr)
		,_kv(kv),_col(col)
	{}
};

//红黑树
template <class K,class V>
class RBTree
{
	typedef RBTreeNode<K, V> Node;
public:
	RBTree()
		:_root(nullptr)
	{}
	//插入
	pair<Node*, bool> Insert(const pair<K, V>& kv)
	{
		//这里都是正常的二叉树的插入
		if (_root == nullptr)
		{
			_root = new Node(kv);
			return make_pair(_root, true);
		}
		Node* parent = nullptr;
		Node* cur = _root;
		while (cur)
		{
			if (cur->_kv.first == kv.first)
			{
				//这里不接受重复的值
				return make_pair(cur, false);
			}
			else if (cur->_kv.first > kv.first)
			{
				parent = cur;
				cur = cur->_left;
			}
			else
			{
				parent = cur;
				cur = cur->_right;
			}
		}
		//这里找到插入的位置就可以插入了
		cur = new Node(kv);
		Node* newnode = cur;  //后面需要返回，提前记录
		if (parent->_kv.first > kv.first)
		{
			parent->_left = cur;
		}
		else
		{
			parent->_right = cur;
		}
		cur->_parent = parent;
		//对红黑树进行调整
		while (parent && parent->_parent)
		{
			//分parent在gparent的左右两种情况
			Node* gparent = parent->_parent;
			if (parent == gparent->_left)
			{
				//分成3种情况,看叔叔
				Node* uncle = gparent->_right;
				if (uncle && uncle->_col == RED)
				{
					//情况一，只用变色即可
					uncle->_col = parent->_col = BLACK;
					gparent->_col = RED;
					//向上更新
					cur = gparent;
					parent = cur->_parent;
				}
				else
				{
					//情况2和3
					//这里cur分在parent的左右两种情况来进行旋转，然后变色即可
					if (cur == parent->_left)
					{
						//右单旋
						RotateR(gparent);
						parent->_col = BLACK;
						gparent->_col = RED;
					}
					else
					{
						//左右双旋
						RotateLR(gparent);
						gparent->_col = RED;
						cur->_col = BLACK;
					}
					break;  //情况2和情况3旋转之后就完成了红黑树的调整
				}
			}
			else
			{
				//parent = gparnet->right
				Node* uncle = gparent->_left;
				if (uncle && uncle->_col == RED)
				{
					//情况1，变色然后更新
					uncle->_col = parent->_col = BLACK;
					gparent->_col = RED;
					//向上更新
					cur = gparent;
					parent = cur->_parent;
				}
				else
				{
					//情况2和3
					//旋转加变色
					if (cur == parent->_right)
					{
						//左单旋
						RotateL(gparent);
						gparent->_col = RED;
						parent->_col = BLACK;
					}
					else
					{
						//右左双旋
						RotateRL(gparent);
						cur->_col = BLACK;
						gparent->_col = RED;
					}
					break;
				}
			}
		}
		//前面向上更新，可能更新了根节点
		_root->_col = BLACK;
		return make_pair(newnode, true);
	}
	void RotateL(Node* parent)
	{
		Node* gparent = parent->_parent;
		Node* subR = parent->_right;
		Node* subRL = subR->_left;
		//连接subRL和parent
		parent->_right = subRL;
		if (subRL) subRL->_parent = parent;
		//parent 和subR的连接
		parent->_parent = subR;
		subR->_left = parent;
		//连接gparent和subR
		if (gparent == nullptr)
		{
			_root = subR;
			subR->_parent = nullptr;
		}
		else
		{
			if (gparent->_left == parent)
			{
				gparent->_left = subR;
			}
			else
			{
				gparent->_right = subR;
			}
			subR->_parent = gparent;
		}
	}
	void RotateR(Node* parent)
	{
		Node* gparent = parent->_parent;
		Node* subL = parent->_left;
		Node* subLR = subL->_right;
		//建立subLR与parent之间的关联
		parent->_left = subLR;
		if (subLR) subLR->_parent = parent;
		subL->_right = parent;
		parent->_parent = subL;
		//建立subL与gparent之间的联系
		if (gparent == nullptr)
		{
			_root = subL;
			_root->_parent = nullptr;
		}
		else
		{
			if (gparent->_left == parent)
			{
				//改变gparent和parent之间的连接关系
				gparent->_left = subL;
			}
			else
			{
				gparent->_right = subL;
			}
			subL->_parent = gparent;
		}
	}
	void RotateLR(Node* parent)
	{
		RotateL(parent->_left);
		RotateR(parent);
	}
	void RotateRL(Node* parent)
	{
		RotateR(parent->_right);
		RotateL(parent);
	}
	//测试红黑树
	bool IsValidRBTree()
	{
		if (_root == nullptr) return true; //空树也是红黑树
		//判断根节点是否为黑色
		if (_root->_col != BLACK)
		{
			cout << "违反红黑树规则，根节点一定是黑色" << endl;
			return false;
		}
		size_t blackcount = 0;  //统计其中一条路径的黑色的节点的个数
		Node* cur = _root;
		while (cur)
		{
			if (cur->_col == BLACK) ++blackcount;
			cur = cur->_left;
		}
		size_t k = 0; //用于后序所有路径的黑色节点个数的统计
		return _IsValidRBTree(_root, blackcount, k);
	}
	bool _IsValidRBTree(Node* root, size_t blackcount, size_t k)
	{
		//返回
		if (root == nullptr)
		{
			if (k != blackcount)
			{
				cout << "违反红黑树规则，每条路径的黑色节点个数不相同" << endl;
				return false;
			}
			return true;
		}
		//统计黑色节点的个数
		if (root->_col == BLACK) ++k;
		//计算当前节点和父节点是否都是红色
		if (root && root->_col == RED && root->_parent->_col == RED)
		{
			cout << "违反红黑树规则，不能有两个相邻的红色节点" << endl;
			return false;
		}
		return _IsValidRBTree(root->_left, blackcount, k) && _IsValidRBTree(root->_right, blackcount, k);
	}
private:
	Node* _root;
};


void TestRBTree()
{
	RBTree<int, int> t;
	t.Insert(make_pair(1, 1));
	t.Insert(make_pair(2, 2));
	t.Insert(make_pair(3, 3));
	t.Insert(make_pair(4, 4));
	t.Insert(make_pair(5, 5));
	if (t.IsValidRBTree()) cout << "满足红黑树规则，是一个红黑树" << endl;
}
