#include "RBTree.hpp"
#include <vector>


void Qsort(vector<int>& arr, int l, int r)
{
	if (l >= r) return;
	//找基准值
	int key = arr[rand() % (r - l + 1) + l];
	int left = l - 1, right = r + 1;
	int i = l;
	while (i < right)
	{
		if (arr[i] < key) swap(arr[i++], arr[++left]);
		else if (arr[i] == key) ++i;
		else swap(arr[i], arr[--right]);
	}
	Qsort(arr, l, left);
	Qsort(arr, right, r);
}


void MergeSort(vector<int>& arr, int left, int right, vector<int>& tmp)
{
	if (left >= right) return;
	//找中间值
	int mid = (left + right) >> 1;
	//[left,mid] [mid+1,right]
	MergeSort(arr, left, mid, tmp);
	MergeSort(arr, mid + 1, right,tmp);
	int begin1 = left, end1 = mid;
	int begin2 = mid + 1, end2 = right;
	int i = left;
	while (begin1 <= end1 && begin2 <= end2)
	{
		tmp[i++] = arr[begin1] < arr[begin2] ? arr[begin1++] : arr[begin2++];
	}
	while (begin1 <= end1)
	{
		tmp[i++] = arr[begin1++];
	}
	while (begin2 <= end2)
	{
		tmp[i++] = arr[begin2++];
	}
	//还原
	for (int j = left; j <= right; ++j)
		arr[j] = tmp[j];
}

//排升序，建大堆
void AdjustDown(vector<int>& arr, int parent, int n)
{
	int child = 2 * parent + 1;
	while (child < n)
	{
		if (child + 1 < n && arr[child + 1] > arr[child])
			++child;
		if (arr[child] > arr[parent])
		{
			swap(arr[child], arr[parent]);
			//向下更新
			parent = child;
			child = 2 * parent + 1;
		}
		else break; //这里已成堆
	}
}

//堆排
void HeapSort(vector<int>& arr)
{
	int n = arr.size();
	//向下调整建堆
	for (int i = (n - 1 - 1) / 2; i >= 0; --i)
	{
		AdjustDown(arr, i, n);
	}
	//排序
	for (int i = 1; i < n; ++i)
	{
		swap(arr[0], arr[n - i]);
		AdjustDown(arr, 0, n - i);
	}
}

int main()
{
	//TestRBTree();
	vector<int> arr = { 3,5,6,4,7,8,0,9,8,1,2,4 };
	int n = arr.size();
	vector<int> tmp(n);
	//Qsort(arr, 0, n - 1);
	//MergeSort(arr, 0, n - 1, tmp);
	HeapSort(arr);
	for (auto e : arr)
	{
		cout << e << " ";
	}
	return 0;
}