#include <iostream>
#include <vector>
using namespace std;

//class Solution {
//public:
//    int ThreeOfMid(vector<int>& nums, int left, int right)
//    {
//        int mid = left + (right - left) / 2;
//        if (nums[mid] > nums[left])
//        {
//            if (nums[right] > nums[mid]) return mid;
//            else if (nums[right] < nums[left]) return left;
//            else return right;
//        }
//        else //nums[mid] <= nums[left]
//        {
//            if (nums[right] > nums[left]) return left;
//            else if (nums[right] < nums[mid]) return mid;
//            else return right;
//        }
//    }
//    void QuickSort(vector<int>& nums, int left, int right)
//    {
//        if (left >= right) return;
//        //三数取中选key值
//        int mid = ThreeOfMid(nums, left, right);
//        int key = nums[mid];
//        //使用三指针
//        int pre = left - 1;
//        int end = right + 1;
//        int i = left;
//        while (i < end)
//        {
//            //分三步讨论
//            if (nums[i] < key) {
//                swap(nums[++pre], nums[i++]);
//            }
//            else if (nums[i] == key) ++i;
//            else {
//                swap(nums[--end], nums[i]);
//            }
//        }
//        //[left,pre] [pre+1,end-1] [end,right]
//        QuickSort(nums, left, pre);
//        QuickSort(nums, end, right);
//    }
//    vector<int> sortArray(vector<int>& nums) {
//        QuickSort(nums, 0, nums.size() - 1);
//        return nums;
//    }
//};


//class Solution {
//public:
//    int GetKey(vector<int>& nums, int left, int right)
//    {
//        //生成随机下标
//        int r = rand() % (right - left + 1) + left;
//        return nums[r];
//    }
//    void QuickSort(vector<int>& nums, int left, int right)
//    {
//        if (left >= right) return;
//        int key = GetKey(nums, left, right);
//        //使用三指针
//        int pre = left - 1;
//        int end = right + 1;
//        int i = left;
//        while (i < end)
//        {
//            //分三步讨论
//            if (nums[i] < key) {
//                swap(nums[++pre], nums[i++]);
//            }
//            else if (nums[i] == key) ++i;
//            else {
//                swap(nums[--end], nums[i]);
//            }
//        }
//        //[left,pre] [pre+1,end-1] [end,right]
//        QuickSort(nums, left, pre);
//        QuickSort(nums, end, right);
//    }
//    vector<int> sortArray(vector<int>& nums) {
//        //使用随机数来进行快排
//        srand(time(nullptr));
//        QuickSort(nums, 0, nums.size() - 1);
//        return nums;
//    }
//};


//class Solution {
//public:
//    int qsort(vector<int>& nums, int l, int r, int k)
//    {
//        if (l == r) return nums[l];
//        int key = GetKey(nums, l, r);
//        //将当前数组排好序
//        int left = l - 1;
//        int right = r + 1;
//        int i = l;
//        while (i < right)
//        {
//            if (nums[i] < key) swap(nums[++left], nums[i++]);
//            else if (nums[i] == key) ++i;
//            else swap(nums[--right], nums[i]);
//        }
//        //根据每个区间的长度确定后序递归的方向
//        int c = r - right + 1;
//        int b = right - left - 1;
//        if (c >= k) return qsort(nums, right, r, k);
//        else if (c + b >= k) return key;
//        else return qsort(nums, l, left, k - c - b);
//    }
//    int findKthLargest(vector<int>& nums, int k) {
//        //使用随机数进行快排
//        srand(time(nullptr));
//        return qsort(nums, 0, nums.size() - 1, k);
//    }
//    int GetKey(vector<int>& nums, int l, int r)
//    {
//        return nums[rand() % (r - l + 1) + l];
//    }
//};


//class Solution {
//public:
//    int numberOfArithmeticSlices(vector<int>& nums) {
//        int n = nums.size();
//        vector<vector<int>> dp(n, vector<int>(n));
//        unordered_map<long long, vector<int>> hash; //元素和下标数组的映射关系
//        int ret = 0;
//        hash[nums[0]].push_back(0);
//        for (int i = 1; i < n; ++i)
//        {
//            for (int j = i + 1; j < n; ++j)
//            {
//                long long a = (long long)2 * nums[i] - nums[j];
//                //去hash中找下标
//                if (hash.count(a))
//                {
//                    for (auto kx : hash[a])
//                    {
//                        dp[i][j] += (dp[kx][i] + 1);
//                    }
//                }
//                ret += dp[i][j];
//            }
//            //维护映射关系
//            hash[nums[i]].push_back(i);
//        }
//        return ret;
//    }
//};


//class Solution {
//public:
//    void qsort(vector<int>& nums, int l, int r, int k)
//    {
//        if (l == r) return;
//        //选一个随机数
//        int key = nums[rand() % (r - l + 1) + l];
//        int i = l;
//        int left = l - 1;
//        int right = r + 1;
//        while (i < right)
//        {
//            if (nums[i] < key) swap(nums[++left], nums[i++]);
//            else if (nums[i] == key) ++i;
//            else swap(nums[--right], nums[i]);
//        }
//        //根据区间来选择递归的方向
//        int a = left - l + 1; //左边小于key
//        int b = right - left - 1; //等于key
//        if (a > k) return qsort(nums, l, left, k);
//        else if (a + b >= k) return;
//        else return qsort(nums, right, r, k - a - b);
//    }
//    vector<int> inventoryManagement(vector<int>& stock, int cnt) {
//        srand(time(nullptr));
//        qsort(stock, 0, stock.size() - 1, cnt);
//        vector<int> res(cnt);
//        for (int i = 0; i < cnt; ++i)
//        {
//            res[i] = stock[i];
//        }
//        return res;
//    }
//};