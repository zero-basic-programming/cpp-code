#include <iostream>
#include <vector>
using namespace std;


void Qsort(vector<int>& arr, int l, int r)
{
	if (l >= r) return;
	int key = arr[rand() % (r - l + 1) + l];
	int left = l - 1, right = r + 1, i = l;
	while (i < right)
	{
		if (arr[i] < key) swap(arr[i++], arr[++left]);
		else if (arr[i] == key) ++i;
		else swap(arr[i], arr[--right]);
	}
	Qsort(arr, l, left);
	Qsort(arr, right, r);
}

void MergeSort(vector<int>& arr, int left, int right, vector<int>& tmp)
{
	if (left >= right) return;
	int mid = left + (right - left) / 2;
	MergeSort(arr, left, mid, tmp);
	MergeSort(arr, mid + 1, right, tmp);
	int begin1 = left, end1 = mid;
	int begin2 = mid + 1, end2 = right;
	int i = left;
	while (begin1 <= end1 && begin2 <= end2)
	{
		tmp[i++] = arr[begin1] < arr[begin2] ? arr[begin1++] : arr[begin2++];
	}
	while (begin1 <= end1)
	{
		tmp[i++] = arr[begin1++];
	}
	while (begin2 <= end2)
	{
		tmp[i++] = arr[begin2++];
	}
	//��ԭ
	for (int j = left; j <= right; ++j)
		arr[j] = tmp[j];
}


void AdjustDown(vector<int>& arr,int parent,int n)
{
	int child = 2 * parent + 1;
	while (child < n)
	{
		if (child + 1 < n && arr[child] < arr[child + 1])
		{
			++child;
		}
		if (arr[parent] < arr[child])
		{
			swap(arr[parent], arr[child]);
			parent = child;
			child = 2 * parent + 1;
		}
		else break;  //�Գɶ�
	}
}

void HeapSort(vector<int>& arr)
{
	int n = arr.size();
	for (int i = (n - 1 - 1) / 2; i >= 0; --i)
	{
		AdjustDown(arr, i, n);
	}
	for (int i = 1; i < n; ++i)
	{
		swap(arr[0], arr[n - i]);
		AdjustDown(arr, 0, n - i);
	}
}


int main()
{
	vector<int> arr = { 2,4,5,3,3,7,8,0,9,7,6,1 };	
	int n = arr.size();
	vector<int> tmp(n);
	//Qsort(arr, 0, n - 1);
	//MergeSort(arr, 0, n - 1, tmp);
	HeapSort(arr);
	for (auto e : arr)
		cout << e << " ";
	return 0;
}