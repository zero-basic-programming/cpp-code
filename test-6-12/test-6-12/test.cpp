#include <iostream>

//class Solution {
//private:
//    vector<vector<int>> result;
//    vector<int> path;
//public:
//    void backtracking(vector<int>& nums, int startindex)
//    {
//        //返回条件
//        if (path.size() > 1)  result.push_back(path);
//        //对于每一层来说，如果当前的值比前面的值小，就直接跳出循环即可
//        unordered_set<int> uset; // 使用set对本层元素进行去重
//        for (int i = startindex; i < nums.size(); ++i)
//        {
//            if ((!path.empty() && nums[i] < path.back()) || uset.find(nums[i]) != uset.end())
//            {
//                //这里说明条件不符合，或者是当前层已经出现过了
//                continue;
//            }
//            //记录当前的数据，防止重复使用
//            uset.insert(nums[i]);
//            path.push_back(nums[i]);
//            backtracking(nums, i + 1);
//            path.pop_back();
//        }
//    }
//    vector<vector<int>> findSubsequences(vector<int>& nums) {
//        backtracking(nums, 0);
//        return result;
//    }
//};


//class Solution {
//private:
//    vector<vector<int>> result;
//    vector<int> path;
//    vector<bool> used; //用来判断哪些元素已经使用过
//public:
//    void backtracking(vector<int>& nums)
//    {
//        //返回条件
//        if (nums.size() == path.size())
//        {
//            result.push_back(path);
//            return;
//        }
//        //单层循环
//        for (int i = 0; i < nums.size(); ++i)
//        {
//            //判断这个元素是否已经使用过
//            if (used[i] == true) continue;
//            used[i] = true;
//            path.push_back(nums[i]);
//            backtracking(nums);
//            path.pop_back();
//            used[i] = false; // 同一层的要回退
//        }
//    }
//    vector<vector<int>> permute(vector<int>& nums) {
//        used.resize(nums.size(), false);
//        backtracking(nums);
//        return result;
//    }
//};

//class Solution {
//private:
//    vector<vector<int>> result;
//    vector<int> path;
//public:
//    void backtracking(vector<int>& nums, vector<bool>& used)
//    {
//        if (nums.size() == path.size())
//        {
//            result.push_back(path);
//            return;
//        }
//        //遍历单层
//        for (int i = 0; i < nums.size(); ++i)
//        {
//            if (i > 0 && nums[i] == nums[i - 1] && used[i - 1] == false)
//            {
//                continue; // 这里对于当层来说是不可以有元素重复的
//            }
//            if (used[i] == true) continue; //这里说明已经使用过了
//            used[i] = true;
//            path.push_back(nums[i]);
//            backtracking(nums, used);
//            //回溯
//            path.pop_back();
//            used[i] = false;
//        }
//    }
//    vector<vector<int>> permuteUnique(vector<int>& nums) {
//        vector<bool> used(nums.size(), false);
//        //这里我们提前将数组排好序，这样就可以通过相邻元素进行去重
//        sort(nums.begin(), nums.end());
//        backtracking(nums, used);
//        return result;
//    }
//};



