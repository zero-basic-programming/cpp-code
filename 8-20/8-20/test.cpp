#include <iostream>
#include <vector>
using namespace std;


//class Solution {
//public:
//    ListNode* reverseList(ListNode* head) {
//        if (!head || !head->next) return head;  //处理空或者一个的情况
//        ListNode* n1 = head, * n2 = head->next, * n3 = nullptr;
//        n1->next = nullptr;
//        while (n2)
//        {
//            n3 = n2->next;
//            n2->next = n1;
//            //迭代
//            n1 = n2;
//            n2 = n3;
//        }
//        return n1;
//    }
//    ListNode* addTwoNumbers(ListNode* l1, ListNode* l2) {
//        //先将两个链表进行反转，然后从头开始计算，最后再把链表反转回来
//        ListNode* list1 = reverseList(l1);
//        ListNode* list2 = reverseList(l2);
//        ListNode* guard = new ListNode(0);
//        ListNode* tail = guard;   //向后尾插
//        int step = 0;  //处理进位
//        while (list1 || list2)
//        {
//            int val1 = list1 == nullptr ? 0 : list1->val;
//            int val2 = list2 == nullptr ? 0 : list2->val;
//            int sum = val1 + val2 + step;
//            step = sum / 10;
//            sum %= 10;
//            tail->next = new ListNode(sum);
//            tail = tail->next;
//            if (list1) list1 = list1->next;
//            if (list2) list2 = list2->next;
//        }
//        //最后这里可能还有进位
//        if (step) tail->next = new ListNode(1);
//        //反转
//        ListNode* head = guard->next;
//        delete guard;
//        ListNode* newhead = reverseList(head);
//        return newhead;
//    }
//};


//class Solution {
//    stack<int> s1, s2;  //使用栈来模拟递归的方式进行尾部运算
//public:
//    ListNode* addTwoNumbers(ListNode* l1, ListNode* l2) {
//        ListNode* head = nullptr;
//        //入栈
//        while (l1)
//        {
//            s1.push(l1->val);
//            l1 = l1->next;
//        }
//        while (l2)
//        {
//            s2.push(l2->val);
//            l2 = l2->next;
//        }
//        int step = 0;
//        while (s1.size() || s2.size() || step != 0)
//        {
//            int val1 = s1.size() > 0 ? s1.top() : 0;
//            int val2 = s2.size() > 0 ? s2.top() : 0;
//            if (s1.size() > 0) s1.pop();
//            if (s2.size() > 0) s2.pop();
//            int sum = val1 + val2 + step;
//            step = sum / 10;
//            sum %= 10;
//            ListNode* tmp = new ListNode(sum);
//            tmp->next = head;
//            head = tmp;
//        }
//        return head;
//    }
//};
//


