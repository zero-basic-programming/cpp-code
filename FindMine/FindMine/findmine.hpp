#pragma once
#include <iostream>
#include <vector>
using namespace std;

#define ROW 9
#define COL 9

#define MINECOUNT 10

class FindMine
{
public:
	FindMine()
		:_row(ROW+2), _col(COL+2)
	{
		_show.resize(_row, vector<char>(_col, '*'));
		_mine.resize(_row, vector<char>(_col, '0'));
		_vis.resize(_row, vector<bool>(_col, false));
	}
	//对棋盘的展示
	void Show(vector<vector<char>>& broad)
	{
		//要打印行列，这样方便观察
		cout << "--------------扫雷------------------" << endl;
		for (int i = 0; i <= ROW; ++i)
		{
			cout << i << " ";
		}
		cout << endl;
		for (int i = 1; i <= ROW; ++i)
		{
			cout << i << " ";
			for (int j = 1; j <= COL; ++j)
			{
				cout << broad[i][j] << " ";
			}
			cout << endl;
		}
		cout << "--------------扫雷------------------" << endl;
	}
	//设置雷的个数
	void SetMine()
	{
		int count = MINECOUNT;
		while (count)
		{
			int row = rand() % ROW + 1;
			int col = rand() % COL + 1;
			if (_mine[row][col] != '1')
			{
				_mine[row][col] = '1';
				--count;
			}
		}
	}
	//开始游戏
	void game()
	{
		//先对雷进行设置，然后展示棋盘，最后开始扫雷
		SetMine();
		Show(_show);
		//Show(_mine);
		FindMineGame();
	}
	//查找当前位置的9宫格内有多少个雷
	int FindNumber(int row,int col)
	{
		//因为之后周围的8个位置，可以直接穷举即可
		//这里因为多开了两行两列，所以不需要考虑越界问题
		char tmp = (_mine[row - 1][col - 1] + _mine[row - 1][col] + _mine[row - 1][col + 1] +
			_mine[row][col - 1] + _mine[row][col + 1] + _mine[row + 1][col - 1] +
			_mine[row + 1][col] + _mine[row + 1][col + 1]) - 8 * '0';
		return tmp;
	}
	//实现扫雷展开一片
	void ShowAround(int i,int j)
	{
		_vis[i][j] = true;
		for (int k = 0; k < 8; ++k)
		{
			int x = i + dx[k], y = j + dy[k];
			int count = 0;
			//判断合法性
			if (x >= 1 && x <= ROW && y >= 1 && y <= COL)
			{
				count = FindNumber(x, y);
				if (count > 0)
				{
					//说明这里是不能再延展下去了
					_show[x][y] = count + '0';
					return;
				}
				else
				{
					//当前位置是0，所以还需要延展
					_show[x][y] = '0';
					//这里如果已经计算过了就不能再计算了
					if(!_vis[x][y])	ShowAround(x, y);
				}
			}
		}
	}
	//扫雷游戏的主逻辑
	void FindMineGame()
	{
		int win = ROW * COL - MINECOUNT; // 当雷的个数减少到0的时候就胜利了
		int x, y; //用来输入用户输入的坐标
		while (win)
		{
			cin >> x >> y;
			//判断坐标的合法性
			if (x >= 1 && x <= ROW && y >= 1 && y <= COL)
			{
				//判定有没有雷
				if (_mine[x][y] == '1')
				{
					//输了
					cout << "你踩雷了，下机" << endl;
					Show(_mine);
					break;
				}
				else
				{
					--win;
					_show[x][y] = FindNumber(x, y) + '0';
					//这里如果当前位置的四周是没有雷的，则进行展开一片
					if(_show[x][y] == '0') ShowAround(x,y);
					Show(_show);
				}
			}
			else
			{
				cout << "坐标不合法,请重新输入" << endl;
			}
		}
		if (win == 0)
		{
			cout << "恭喜你，扫雷成功" << endl;
		}
	}
private:
	vector<vector<char>> _show;   //给用户展示的
	vector<vector<char>> _mine;   //实际的雷的数组，其中1代表雷，0代表正常
	int _row;  //这里要多开两行两列，这样方便实现
	int _col;
	//用来实现展开一片
	int dx[8] = { -1,-1,-1,0,0,1,1,1 };
	int dy[8] = { -1,0,1,-1,1,-1,0,1 };
	vector<vector<bool>> _vis;
};