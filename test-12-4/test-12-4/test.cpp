#include <iostream>
#include <vector>
using namespace std;

//class Solution {
//    vector<vector<int>> ret;
//    vector<int> path;
//public:
//    vector<vector<int>> combinationSum(vector<int>& candidates, int target) {
//        dfs(candidates, target, 0, 0);
//        return ret;
//    }
//    void dfs(vector<int>& candidates, int target, int k, int sum)
//    {
//        //收集结果
//        if (sum == target)
//        {
//            ret.push_back(path);
//            return;
//        }
//        if (sum > target || k == candidates.size()) return;
//        for (int i = 0; sum + candidates[k] * i <= target; ++i)
//        {
//            if (i) path.push_back(candidates[k]);
//            dfs(candidates, target, k + 1, sum + i * candidates[k]);
//        }
//        //回溯
//        for (int i = 1; sum + candidates[k] * i <= target; ++i)
//        {
//            path.pop_back();
//        }
//    }
//};


//void AdjustDown(vector<int>& arr, int parent,int n)
//{
//	int child = 2 * parent + 1;
//	while (child < n)
//	{
//		//取左右孩子中较大那个
//		if (child + 1 < n && arr[child] < arr[child + 1])
//		{
//			++child;
//		}
//		if (arr[child] > arr[parent])
//		{
//			swap(arr[child], arr[parent]);
//			parent = child;
//			child = 2 * parent + 1;
//		}
//		else
//		{
//			break; //已成堆
//		}
//	}
//}
//
//void HeapSort(vector<int>& arr)
//{
//	int n = arr.size();
//	//向下调整建堆
//	for (int i = (n - 1 - 1) / 2; i >= 0; --i)
//	{
//		AdjustDown(arr, i, n);
//	}
//	//排序
//	for (int i = 0; i < n; ++i)
//	{
//		swap(arr[0], arr[n - i - 1]);
//		AdjustDown(arr, 0, n - i -1);
//	}
//}
//
//void ShellSort(vector<int>& arr)
//{
//	int n = arr.size();
//	int gap = n;
//	while (gap > 1)
//	{
//		gap /= 2;
//		for (int i = 0; i+gap < n; i++)
//		{
//			int end = i;
//			int tmp = arr[end + gap];
//			while (end >= 0)
//			{
//				if (arr[end] > tmp)
//				{
//					arr[end + gap] = arr[end];
//					end -= gap;
//				}
//				else break;
//				arr[end + gap] = tmp;
//			}
//		}
//	}
//}
//
//int main()
//{
//	vector<int> arr = { 5,3,6,26,4,7,7,344,7,8,9,8,9,1,0 };
//	//HeapSort(arr);
//	ShellSort(arr);
//	for (auto e : arr)
//	{
//		cout << e << " ";
//	}
//	return 0;
//}


//class Solution {
//    int count = 0;
//    bool check[16];
//public:
//    int countArrangement(int n) {
//        dfs(n, 1);
//        return count;
//    }
//    void dfs(int n, int pos)
//    {
//        if (pos == n + 1)
//        {
//            ++count;
//            return;
//        }
//        for (int i = 1; i <= n; ++i)
//        {
//            //剪枝
//            if (check[i] || (i % pos != 0 && pos % i != 0)) continue;
//            check[i] = true;
//            dfs(n, pos + 1);
//            //回溯
//            check[i] = false;
//        }
//    }
//};


