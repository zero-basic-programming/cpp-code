//class Solution {
//private:
//    unordered_map<string, map<string, int>> umap; //用来记录第一个元素和其映射元素的的数量关系
//    bool backtracking(int num, vector<string>& result)
//    {
//        if (result.size() == num + 1)
//        {
//            //因为机场个数+1就是刚好全部都能走过
//            return true; //走到这里说明是一定收集到了结果，否则是不能走到这里的
//        }
//        //因为使用的是map，第一次找的结果就是最优解
//        for (pair<const string, int>& p : umap[result[result.size() - 1]])
//        {
//            //从上次最后拿到的结果中在映射表中找到这次要找的字符串
//            if (p.second > 0)
//            {
//                //这里之后>0才有查找的意义
//                result.push_back(p.first);
//                p.second--;
//                //这里如果收集到结果就不需要再往下找了
//                if (backtracking(num, result) == true)  return true;
//                //回溯
//                result.pop_back();
//                p.second++;
//            }
//        }
//        return false;
//    }
//public:
//    vector<string> findItinerary(vector<vector<string>>& tickets) {
//        vector<string> result; //用来收集结果
//        //初始化umap
//        for (vector<string>& vs : tickets)
//        {
//            //这里把第一个元素与其映射元素的数量关系记录清除，这样可以不用删除，防止迭代器失效
//            umap[vs[0]][vs[1]]++; //这里非常巧妙的使用了operator[]重载
//        }
//        //添加起始机场
//        result.push_back("JFK");
//        backtracking(tickets.size(), result); //这里把大小传进去是为了终止条件
//        return result;
//    }
//};

