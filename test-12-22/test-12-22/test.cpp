#include <iostream>
#include <vector>
using namespace std;


//class Solution {
//    int x, y;
//public:
//    int uniquePaths(int m, int n) {
//        x = m, y = n;
//        vector<vector<int>> check(m, vector<int>(n, 0));
//        //初始化
//        dfs(check, 0, 0);
//        return check[m - 1][n - 1];
//    }
//    void dfs(vector<vector<int>>& check, int m, int n)
//    {
//        check[m][n]++;
//        if (m == x - 1 && n == y - 1)
//        {
//            return;
//        }
//        if (m < x)
//        {
//            dfs(check, m + 1, n);
//        }
//        if (n < y)
//        {
//            dfs(check, m, n + 1);
//        }
//    }
//};


//void Qsort(vector<int>& arr, int l, int r)
//{
//	if (l >= r) return;
//	//选取基准值
//	int key = arr[rand() % (r - l + 1) + l];
//	int left = l - 1, right = r + 1, i = l;
//	while (i < right)
//	{
//		if (arr[i] < key) swap(arr[i++], arr[++left]);
//		else if (arr[i] == key) ++i;
//		else swap(arr[i], arr[--right]);
//	}
//	//排序左右区间
//	Qsort(arr, l, left);
//	Qsort(arr, right, r);
//}

void MergeSort(vector<int>& arr, int left, int right, vector<int>& tmp)
{
	if (left >= right) return;
	int mid = (left + right) >> 1;
	//[left,mid] [mid+1,right]
	MergeSort(arr, left, mid, tmp);
	MergeSort(arr, mid + 1, right,tmp);
	//进行一次归并
	int begin1 = left, end1 = mid;
	int begin2 = mid + 1, end2 = right;
	int i = left;
	while (begin1 <= end1 && begin2 <= end2)
	{
		tmp[i++] = arr[begin1] < arr[begin2] ? arr[begin1++] : arr[begin2++];
	}
	while (begin1 <= end1)
	{
		tmp[i++] = arr[begin1++];
	}
	while (begin2 <= end2)
	{
		tmp[i++] = arr[begin2++];
	}
	//还原回原来数组
	for (int j = left; j <= right; ++j)
		arr[j] = tmp[j];
}

int main()
{
	srand(time(nullptr));
	vector<int> arr = { 4,2,3,5,2,6,4,7,5,7 };
	int n = arr.size();
	vector<int> tmp(n);
	//Qsort(arr, 0, n-1);
	MergeSort(arr, 0, n - 1, tmp);
	for (auto e : arr)
	{
		cout << e << " ";
	}
	return 0;
}