#include <iostream>
using namespace std;

//class Solution {
//public:
//    TreeNode* buildTree(vector<int>& nums, int left, int right)
//    {
//        //终止条件
//        if (left > right)
//        {
//            return nullptr;
//        }
//        //找到中间节点
//        int mid = left + (right - left) / 2;
//        TreeNode* node = new TreeNode(nums[mid]);
//        //左右递归建立子树
//        //[left,mid-1] [mid+1,right]
//        node->left = buildTree(nums, left, mid - 1);
//        node->right = buildTree(nums, mid + 1, right);
//        return node;
//    }
//    TreeNode* sortedArrayToBST(vector<int>& nums) {
//        //每次找到中间那个节点，然后划分子树
//        //采用左闭右闭
//        TreeNode* root = buildTree(nums, 0, nums.size() - 1);
//        return root;
//    }
//};


//class Solution {
//public:
//    TreeNode* sortedArrayToBST(vector<int>& nums) {
//        //使用非递归
//        //通过使用3个队列来模拟实现
//        queue<TreeNode*> nodeq; // 用来存放中间节点
//        queue<int> left; //存放左边界下标
//        left.push(0);
//        queue<int> right; // 存放右边界下标
//        right.push(nums.size() - 1); // 使用左闭右闭的方式
//        TreeNode* root = new TreeNode(0); // 先创建先，方便写
//        nodeq.push(root);
//        while (!nodeq.empty())
//        {
//            TreeNode* node = nodeq.front();
//            nodeq.pop();
//            int lefti = left.front();
//            left.pop();
//            int righti = right.front();
//            right.pop();
//            int mid = lefti + (righti - lefti) / 2;
//            node->val = nums[mid];
//            //往左右子树创建
//            if (lefti <= mid - 1)
//            {
//                node->left = new TreeNode(0);
//                left.push(lefti);
//                right.push(mid - 1);
//                nodeq.push(node->left);
//            }
//            if (righti >= mid + 1)
//            {
//                node->right = new TreeNode(0);
//                left.push(mid + 1);
//                right.push(righti);
//                nodeq.push(node->right);
//            }
//        }
//        return root;
//    }
//};


//class Solution {
//public:
//    int preval = 0;
//    TreeNode* convertBST(TreeNode* root) {
//        //直接使用右根左即可，保存上一次记录过的结果，然后加上当前值
//        if (root == nullptr) return nullptr;
//        convertBST(root->right); // 右
//        root->val = preval + root->val;
//        //更新preval
//        preval = root->val;
//        convertBST(root->left);
//        return root;
//    }
//};


//class Solution {
//public:
//    TreeNode* convertBST(TreeNode* root) {
//        //使用非递归，这里当然使用的是中序遍历了，不过是反过来的
//        if (root == nullptr) return root;
//        stack<TreeNode*> st;
//        TreeNode* cur = root;
//        int preval = 0;
//        while (!st.empty() || cur)
//        {
//            //先往右
//            if (cur)
//            {
//                st.push(cur);
//                cur = cur->right;
//            }
//            else
//            {
//                //中左
//                cur = st.top();
//                st.pop();
//                cur->val += preval;
//                //更新preval
//                preval = cur->val;
//                cur = cur->left; //左
//            }
//        }
//        return root;
//    }
//};

