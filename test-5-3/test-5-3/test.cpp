#include <iostream>
using namespace std;

//class Solution {
//public:
//    bool travelsal(TreeNode* root, int sum, int& targetSum)
//    {
//        //叶子节点
//        if (root && !root->left && !root->right)
//        {
//            sum += root->val;
//            if (sum == targetSum)
//            {
//                return true;
//            }
//            return false;
//        }
//        bool leftflag = false;
//        bool rightflag = false;
//        if (root->left)  leftflag = travelsal(root->left, sum + root->val, targetSum);
//        if (root->right) rightflag = travelsal(root->right, sum + root->val, targetSum);
//        return leftflag || rightflag;
//    }
//    bool hasPathSum(TreeNode* root, int targetSum) {
//        if (root == nullptr) return false;
//        return travelsal(root, 0, targetSum);
//    }
//};

//class Solution {
//public:
//    bool hasPathSum(TreeNode* root, int targetSum) {
//        if (root == nullptr) return false;
//        //叶子节点
//        if (!root->left && !root->right && root->val == targetSum)   return true;
//        //递归左右子树
//        return hasPathSum(root->left, targetSum - root->val) || hasPathSum(root->right, targetSum - root->val);
//    }
//};


//class Solution {
//public:
//    bool hasPathSum(TreeNode* root, int targetSum) {
//        //使用迭代方式模拟回溯过程
//        stack<pair<TreeNode*, int>> st;
//        if (root == nullptr)    return false;
//        st.push(make_pair(root, root->val));
//        while (!st.empty())
//        {
//            //判断叶子节点
//            pair<TreeNode*, int> p = st.top();
//            st.pop();
//            if (!p.first->left && !p.first->right && targetSum == p.second)  return true;
//            //左右
//            if (p.first->right)
//            {
//                st.push(make_pair(p.first->right, p.second + p.first->right->val));
//            }
//            if (p.first->left)
//            {
//                st.push(make_pair(p.first->left, p.second + p.first->left->val));
//            }
//        }
//        return false;
//    }
//};

//class Solution {
//public:
//    void travelsal(TreeNode* root, int targetSum, vector<vector<int>>& result, vector<int>& path)
//    {
//        if (!root->left && !root->right)
//        {
//            if (targetSum == 0)
//            {
//                result.push_back(path);
//            }
//            return;
//        }
//        if (root->left)
//        {
//            path.push_back(root->left->val);
//            travelsal(root->left, targetSum - root->left->val, result, path);
//            //回溯
//            path.pop_back();
//        }
//        if (root->right)
//        {
//            path.push_back(root->right->val);
//            travelsal(root->right, targetSum - root->right->val, result, path);
//            //回溯
//            path.pop_back();
//        }
//        return;
//    }
//    vector<vector<int>> pathSum(TreeNode* root, int targetSum) {
//        vector<vector<int>> result;
//        if (root == nullptr) return result;
//        vector<int> path; // 用来记录当前路径
//        path.push_back(root->val);
//        travelsal(root, targetSum - root->val, result, path);
//        return result;
//    }
//};


//class Solution {
//public:
//    TreeNode* _buildTree(vector<int>& inorder, int inorderleft, int inorderright, vector<int>& postorder, int postorderleft, int postorderright)
//    {
//        if (inorderleft > inorderright)  return nullptr;
//        //找到中序中的根
//        int i = 0;
//        for (; inorder[i] != postorder[postorderright]; ++i)
//        {
//        }
//        //构建根节点
//        TreeNode* root = new TreeNode(inorder[i]);
//        //递归构建其左右子树
//        //[inorderleft,i-1] i [i+1,inorderright] 
//        //后序:  [postleft,i+postorderleft-inorderleft-1] [i+postorderleft-inorderleft,postorderright-1]
//        root->left = _buildTree(inorder, inorderleft, i - 1, postorder, postorderleft, i + postorderleft - inorderleft - 1);
//        root->right = _buildTree(inorder, i + 1, inorderright, postorder, i + postorderleft - inorderleft, postorderright - 1);
//        return root;
//    }
//    TreeNode* buildTree(vector<int>& inorder, vector<int>& postorder) {
//        //使用闭区间
//        return _buildTree(inorder, 0, inorder.size() - 1, postorder, 0, postorder.size() - 1);
//    }
//};


//class Solution {
//public:
//    TreeNode* _buildTree(vector<int>& preorder, vector<int>& inorde, int begin, int end, int& i)
//    {
//        if (begin > end)
//        {
//            return nullptr;
//        }
//        int j = begin;
//        for (; j <= end; ++j)
//        {
//            if (inorde[j] == preorder[i])    break;
//        }
//        //构建根
//        TreeNode* root = new TreeNode(preorder[i++]);
//        //构建左右子树
//        //[begin,j-1] j [j+1,end]
//        root->left = _buildTree(preorder, inorde, begin, j - 1, i);
//        root->right = _buildTree(preorder, inorde, j + 1, end, i);
//        return root;
//    }
//    TreeNode* buildTree(vector<int>& preorder, vector<int>& inorder) {
//        int i = 0; //使用闭区间
//        return _buildTree(preorder, inorder, 0, inorder.size() - 1, i);
//    }
//};


//class Solution {
//public:
//    TreeNode* _constructMaximumBinaryTree(vector<int>& nums, int begin, int end)
//    {
//        if (begin > end)    return nullptr;
//        //找到该区间中最大的那个并划分左右子区间
//        int j = begin;
//        for (int i = begin + 1; i <= end; ++i)
//        {
//            if (nums[i] > nums[j])
//            {
//                j = i; // 记录索引
//            }
//        }
//        //到这里已经分出区间： [begin,j-1] j [j+1 ,end]
//        TreeNode* root = new TreeNode(nums[j]);
//        root->left = _constructMaximumBinaryTree(nums, begin, j - 1);
//        root->right = _constructMaximumBinaryTree(nums, j + 1, end);
//        return root;
//    }
//    TreeNode* constructMaximumBinaryTree(vector<int>& nums) {
//        return _constructMaximumBinaryTree(nums, 0, nums.size() - 1);
//    }
//};


