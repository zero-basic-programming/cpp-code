#include <iostream>
#include <vector>
#include <string>
using namespace std;

//int main() {
//    int n = 4;
//    vector<int> nums = { 1,3,2,4 };
//    vector<char> col = { 'W','R','R','W' };
//    int ret = 0;
//    for (int i = 0; i < n; ++i)
//    {
//        if (col[i] == 'W') continue;
//        int j = i;
//        while (j < n && col[j] == 'W') ++j;
//        if (j == n) continue;  //当前轮是没有比i小的
//        //往后找最小的
//        int mini = j;
//        while (j < n)
//        {
//            if (col[j] == 'W')
//            {
//                ++j;
//                continue;
//            }
//            if (nums[j] < nums[mini]) mini = j;
//            ++j;
//        }
//        //交换i，j下标的值
//        swap(nums[i], nums[mini]);
//        ++ret;
//    }
//    cout << ret << endl;
//    return 0;
//}
//
//int main() {
//    int n, m;
//    cin >> n >> m;
//    vector<vector<int>> arr(n, vector<int>(m));
//    string tmp;
//    for (auto& a : arr)
//    {
//        cin >> tmp;
//        int i = 0;
//        for (auto& e : a)
//        {
//            e = tmp[i++] - '0';
//        }
//    }
//    //创建前缀和数组
//    vector<vector<int>> dp(n + 1, vector<int>(m + 1, 0));
//    for (int i = 1; i <= n; ++i)
//    {
//        for (int j = 1; j <= m; ++j)
//        {
//            dp[i][j] = dp[i - 1][j] + dp[i][j - 1] - dp[i - 1][j - 1] + arr[i - 1][j - 1];
//        }
//    }
//    // for(int i = 1;i<=n;++i)
//    // {
//    //     for(int j = 1;j<=m;++j)
//    //     {
//    //         cout << dp[i][j] << " ";
//    //     }
//    //     cout << endl;
//    // }
//    //输出结果
//    int ret = 0;
//    for (int i = 1; i + 1 <= n; ++i)
//    {
//        for (int j = 1; j + 1 <= m; ++j)
//        {
//            int x1 = i, y1 = j;
//            int x2 = i + 1, y2 = j + 1;
//            int sum = dp[x2][y2] - dp[x2][y1 - 1] - dp[x1 - 1][y2] + dp[x1 - 1][y1 - 1];
//            if (sum * 2 == 4) ++ret;
//        }
//    }
//    cout << ret << endl;
//    return 0;
//}

//int main() {
//    int n, k;
//    cin >> n >> k;
//    string str;
//    cin >> str;
//    int left = 0, right = 0;
//    int hash[26] = { 0 };  //统计字符和数量的关系
//    //使用滑动窗口
//    int style = 0;  //统计类型
//    int count = 0;  //统计数量
//    int ret = 0;   //收集结果 
//    int tmp = k;  //用来记录当前出窗口需要的数量
//    while (right < n)
//    {
//        //进窗口
//        //找到后面括号的数字
//        int end = right + 1;
//        while (str[end] != ')') ++end;
//        string num = str.substr(right + 2, end - right + 2);
//        int number = stoi(num);
//        if (hash[str[right]] == 0) ++style;
//        hash[str[right]] += number;
//        count += number;
//        //判断
//        while (style * count >= k)
//        {
//            //收集结果
//            int out = hash[str[left]];
//            ret += out / k;
//            tmp -= out % k;
//            //出窗口
//            ++left;
//            count -= out;
//            style--;
//        }
//        ++right;
//    }
//    //最后一组可能不满
//    if (style * count < k) --ret;
//    cout << ret << endl;
//    return 0;
//}
//
//int main() {
//    int n;
//    cin >> n;
//    string str;
//    cin >> str;
//    int left = 0, right = str.size() - 1;
//    int ret = 0;
//    while (left < right)
//    {
//        while (left < right && str[left] == str[left + 1]) ++left, ++ret;
//        while (left < right && str[right] == str[right - 1]) --right, ++ret;
//        //在中间找
//        int mid = left;
//        while (mid < right)
//        {
//            if (str[mid] == str[mid + 1])
//            {
//                if (mid - left > right - mid - 1)
//                {
//                    ret += right - mid;
//                    right = mid;
//                }
//                else {
//                    ret += mid - left + 1;
//                    left = mid;
//                }
//            }
//            ++mid;
//        }
//        if (mid == right) break;
//    }
//    cout << ret << endl;
//    return 0;
//}


//class Solution {
//public:
//    int search(vector<int>& nums, int target) {
//        //二分查找
//        int left = 0, right = nums.size() - 1;
//        while (left <= right)
//        {
//            int mid = left + (right - left) / 2; 
//            if (nums[mid] > target) right = mid - 1;
//            else if (nums[mid] < target) left = mid + 1;
//            else return mid;
//        }
//        //这里没有找到
//        return -1;
//    }
//};


//class Solution {
//public:
//    void reverseString(vector<char>& s) {
//        //双指针
//        int left = 0, right = s.size() - 1;
//        while (left < right)
//        {
//            char tmp = s[right];
//            s[right] = s[left];
//            s[left] = tmp;
//            ++left;
//            --right;
//        }
//    }
//};


//树的节点类
//struct TreeNode
//{
//	int val;  
//	TreeNode* left;
//	TreeNode* right;
//};
//
//class Solution
//{
//public:
//	void TreeInvert(TreeNode* root)
//	{
//		//返回条件
//		if (root->left == nullptr && root->right == nullptr) return;
//		//利用后序遍历
//		TreeInvert(root->left);
//		TreeInvert(root->right);
//		//交换左右子节点
//		TreeNode* tmp = root->left;
//		root->left = root->right;
//		root->right = tmp;
//	}
//};
//


//class Solution {
//    vector<vector<string>> ret;  //用来收集结果
//    vector<string> path;  //用于路径记录
//    int n;  //s的大小
//public:
//    //判断回文
//    bool IsPalindrome(string& str)
//    {
//        int left = 0, right = str.size() - 1;
//        while (left < right)
//        {
//            if (str[left] != str[right]) return false;
//            ++left;
//            --right;
//        }
//        return true;
//    }
//    //回溯算法过程
//    void dfs(string& s, int index)
//    {
//        //收集结果
//        if (index == n)
//        {
//            ret.push_back(path);
//            return;
//        }
//        for (int i = index; i < n; ++i)
//        {
//            string tmp = s.substr(index, i - index + 1);
//            //查看是否满足回文
//            if (!IsPalindrome(tmp)) continue;  //不满足
//            path.push_back(tmp);
//            dfs(s, i + 1);  //在后面查找后面的字符是否满足
//            path.pop_back(); //回溯
//        }
//    }
//    vector<vector<string>> partition(string s) {
//        n = s.size();
//        dfs(s, 0); 
//        return ret;
//    }
//};
//
//
//int main()
//{
//    string str = "aab";
//    Solution().partition(str);
//    return 0;
//}


//class Solution {
//public:
//    void MergeSort(vector<int>& nums, int left, int right, vector<int>& tmp)
//    {
//        if (left >= right) return;
//        int mid = (left + right) >> 1;
//        MergeSort(nums, left, mid, tmp);
//        MergeSort(nums, mid + 1, right, tmp);
//        int begin1 = left, end1 = mid;
//        int begin2 = mid + 1, end2 = right;
//        int i = left;
//        while (begin1 <= end1 && begin2 <= end2)
//        {
//            tmp[i++] = nums[begin1] < nums[begin2] ? nums[begin1++] : nums[begin2++];
//        }
//        while (begin1 <= end1) tmp[i++] = nums[begin1++];
//        while (begin2 <= end2) tmp[i++] = nums[begin2++];
//        //还原
//        for (int j = left; j <= right; ++j) nums[j] = tmp[j];
//    }
//    vector<int> sortArray(vector<int>& nums) {
//        int n = nums.size();
//        vector<int> tmp(n);  //用来辅助数组
//        MergeSort(nums, 0, nums.size() - 1, tmp);
//        return nums;
//    }
//};


