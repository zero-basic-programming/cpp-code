#include <iostream>
#include <vector>
using namespace std;


//class Solution {
//public:
//    TreeNode* lowestCommonAncestor(TreeNode* root, TreeNode* p, TreeNode* q) {
//        if (root == nullptr) return nullptr;
//        //获得到其左右节点
//        TreeNode* left = lowestCommonAncestor(root->left, p, q);
//        TreeNode* right = lowestCommonAncestor(root->right, p, q);
//        //如果root是两个中其中一个节点
//        if (root == p && left == q) return root;
//        if (root == q && left == p) return root;
//        if (root == p && right == q) return root;
//        if (root == q && right == p) return root;
//        //返回左右节点中不为空的那一个
//        return (left == p || left == q) ? left : right;
//    }
//};


//class Solution {
//public:
//    TreeNode* lowestCommonAncestor(TreeNode* root, TreeNode* p, TreeNode* q) {
//        //优化处理
//        if (p->val > q->val) swap(p, q);   //这里只是交换了指向，并没有交换实际的数
//        while (root)
//        {
//            if (root->val < p->val)
//            {
//                //这里说明两个数都是在右子树
//                root = root->right;
//            }
//            else if (root->val > q->val)
//            {
//                //说明这两个数都在左边
//                root = root->left;
//            }
//            else break;  //这里要么是p，q在两边，要么是root为p或者为q，因为遍历是从上面开始遍历的
//        }
//        return root;
//    }
//};


//class Solution {
//public:
//    TreeNode* lowestCommonAncestor(TreeNode* root, TreeNode* p, TreeNode* q) {
//        //递归其实也是一样的，都是从根开始向下遍历，这里更加推荐使用迭代，场景更适合
//        if (root->val < p->val && root->val < q->val)
//            return lowestCommonAncestor(root->right, p, q);
//        else if (root->val > p->val && root->val > q->val)
//            return lowestCommonAncestor(root->left, p, q);
//        else return root;
//    }
//};

void AdjustDown(vector<int>& arr, int parent, int n)
{
	int child = 2 * parent + 1;
	while (child < n)
	{
		//右边可能存在一个更大的
		if (child + 1 < n && arr[child] < arr[child + 1])
			++child;
		if (arr[child] > arr[parent])
		{
			swap(arr[child], arr[parent]);
			//迭代
			parent = child;
			child = 2 * parent + 1;
		}
		else break;  //已成堆
	}
}

//升序排大堆
void heapSort(vector<int>& arr)
{
	int n = arr.size(); 
	//向下调整建堆
	for (int i = (n - 1 - 1) / 2; i >= 0; --i)
	{
		AdjustDown(arr, i, n);
	}
	//进行堆排序
	for (int i = 1; i < n; ++i)
	{
		swap(arr[0], arr[n - i]);
		AdjustDown(arr, 0, n - i);
	}
}


int main()
{
	vector<int> arr = { 5,4,6,3,2,1,2,0,9,8,7 };
	heapSort(arr);
	for (auto e : arr)
	{
		cout << e << " ";
	}
	return 0;
}