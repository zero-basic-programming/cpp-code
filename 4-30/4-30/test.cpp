#include <iostream>
#include <vector>
using namespace std;


//class Solution {
//    int pos = 0;
//public:
//    TreeNode* dfs(vector<int>& preorder, vector<int>& inorder, int left, int right)
//    {
//        if (left > right) return nullptr;
//        if (pos == preorder.size()) return nullptr;
//        //中序找根
//        int i = left;
//        for (; i <= right; ++i)
//        {
//            if (inorder[i] == preorder[pos]) break;
//        }
//        TreeNode* node = new TreeNode(preorder[pos++]);
//        node->left = dfs(preorder, inorder, left, i - 1);
//        node->right = dfs(preorder, inorder, i + 1, right);
//        return node;
//    }
//    TreeNode* deduceTree(vector<int>& preorder, vector<int>& inorder) {
//        TreeNode* root = dfs(preorder, inorder, 0, inorder.size() - 1);
//        return root;
//    }
//};


//class Solution {
//    int pos = 0;
//    unordered_map<int, int> hash;
//public:
//    TreeNode* dfs(vector<int>& preorder, vector<int>& inorder, int left, int right)
//    {
//        if (left > right) return nullptr;
//        //中序找根
//        int i = hash[preorder[pos]];
//        TreeNode* node = new TreeNode(preorder[pos++]);
//        node->left = dfs(preorder, inorder, left, i - 1);
//        node->right = dfs(preorder, inorder, i + 1, right);
//        return node;
//    }
//    TreeNode* deduceTree(vector<int>& preorder, vector<int>& inorder) {
//        //利用hash记录中序中节点和下标的关系
//        for (int i = 0; i < inorder.size(); ++i)
//            hash[inorder[i]] = i;
//        TreeNode* root = dfs(preorder, inorder, 0, inorder.size() - 1);
//        return root;
//    }
//};


//class Solution {
//public:
//    int cuttingBamboo(int bamboo_len) {
//        vector<int> dp(bamboo_len + 1);
//        //初始化
//        dp[0] = 1;  //这里需要进行初始化
//        dp[1] = 1;
//        for (int i = 3; i <= bamboo_len; ++i)
//        {
//            //前面任意一段都有可能和当前段进行组合
//            for (int j = 1; j <= i / 2; ++j)
//            {
//                dp[i] = max(dp[i], max(dp[i - j] * j, (i - j) * j));
//            }
//        }
//        return dp[bamboo_len];
//    }
//};


//class Solution {
//public:
//    int cuttingBamboo(int bamboo_len) {
//        vector<int> dp(bamboo_len + 1);
//        //初始化
//        dp[0] = 1;  //这里需要进行初始化
//        dp[1] = 1;
//        for (int i = 2; i <= bamboo_len; ++i)
//        {
//            //前面任意一段都有可能和当前段进行组合
//            for (int j = 0; j <= i; ++j)
//            {
//                dp[i] = max(dp[i], dp[j] * (i - j));
//            }
//        }
//        return dp[bamboo_len];
//    }
//};


