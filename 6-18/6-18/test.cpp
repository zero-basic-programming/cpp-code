#include <iostream>
#include <vector>
using namespace std;


//class Solution {
//public:
//    bool checkDynasty(vector<int>& places) {
//        sort(places.begin(), places.end());
//        int zero = 0;   //统计0的个数
//        int prev = -1;  //记录上一个数
//        for (auto e : places)
//        {
//            if (e == 0)
//            {
//                ++zero;
//                continue;
//            }
//            if (prev == -1)
//            {
//                prev = e;
//                continue;
//            }
//            else if (prev == e) return false;
//            if (e - prev - 1 > zero) return false;
//            zero -= (e - prev - 1);
//            prev = e;
//        }
//        return true;
//    }
//};



//class Solution {
//public:
//    bool checkDynasty(vector<int>& places) {
//        //这题的本质就是找出最大和最小的朝代之间的差是小于5的就行
//        unordered_set<int> hash;  //用来统计放入的朝代
//        int maxnum = 0, minnum = 14;
//        for (auto e : places)
//        {
//            if (e == 0) continue;
//            //重复直接返回错误
//            auto it = hash.find(e);
//            if (it != hash.end()) return false;   //有重复
//            //更新最大和最小的朝代
//            maxnum = max(e, maxnum);
//            minnum = min(e, minnum);
//            //添加当前元素到hash中
//            hash.insert(e);
//        }
//        return maxnum - minnum < 5;
//    }
//};


