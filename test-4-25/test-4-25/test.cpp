#include <iostream>
#include <vector>
using namespace std;

//class Solution {
//public:
//    vector<int> inorderTraversal(TreeNode* root) {
//        //非递归
//        stack<TreeNode*> st;
//        vector<int> result;
//        if (root == nullptr)     return result;
//        st.push(root);
//        while (!st.empty())
//        {
//            TreeNode* node = st.top();
//            //如果不是空就放入，是空就放入结果集
//            if (node)
//            {
//                st.pop();
//                //左根右
//                if (node->right)     st.push(node->right);
//                //放入节点，并完成标记
//                st.push(node);
//                st.push(nullptr);
//                if (node->left)     st.push(node->left);
//            }
//            else
//            {
//                st.pop(); // 弹出空
//                node = st.top();
//                st.pop();
//                result.push_back(node->val);
//            }
//        }
//        return result;
//    }
//};


//class Solution {
//public:
//    vector<vector<int>> levelOrder(TreeNode* root) {
//        queue<TreeNode*> q;
//        vector<vector<int>> result;
//        if (root == nullptr) return result;
//        q.push(root);
//        while (!q.empty())
//        {
//            int size = q.size();
//            vector<int> v;//保存临时结果
//            //把数据放入临时结果中，并放入其子节点
//            while (size--)
//            {
//                TreeNode* node = q.front();
//                q.pop();
//                v.push_back(node->val);
//                if (node->left) q.push(node->left);
//                if (node->right) q.push(node->right);
//            }
//            result.push_back(v);
//        }
//        return result;
//    }
//};

//class Solution {
//public:
//    TreeNode* invertTree(TreeNode* root) {
//        //交换左右子树
//        if (root == nullptr) return root;
//        TreeNode* tmp = root->right;
//        root->right = root->left;
//        root->left = tmp;
//        //向下递归
//        invertTree(root->left);
//        invertTree(root->right);
//        return root;
//    }
//};


