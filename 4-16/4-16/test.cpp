#include <iostream>
#include <vector>
using namespace std;


//class Solution {
//    vector<vector<int>> ret;  //用来保存结果
//    vector<int> path;  //用来保存路径上的值
//    vector<bool> vis;   //用来保存数组中的元素是否使用过
//    int n;  //保存数组的大小
//public:
//    void dfs(vector<int>& nums)
//    {
//        //返回结果
//        if (path.size() == n)
//        {
//            ret.push_back(path);
//            return;
//        }
//        for (int i = 0; i < n; ++i)
//        {
//            if (vis[i]) continue;  //当前元素在之前的路径已经使用过
//            vis[i] = true;
//            path.push_back(nums[i]);
//            dfs(nums);
//            //回溯
//            vis[i] = false;
//            path.pop_back();
//        }
//    }
//    vector<vector<int>> permute(vector<int>& nums) {
//        n = nums.size();
//        vis.resize(n, false);
//        dfs(nums);
//        return ret;
//    }
//};


//class Solution {
//    vector<vector<int>> ret;
//    vector<int> path;
//    int n;
//public:
//    void dfs(vector<int>& nums, int start)
//    {
//        ret.push_back(path);
//        for (int i = start; i < n; ++i)
//        {
//            path.push_back(nums[i]);
//            dfs(nums, i + 1);
//            //回溯
//            path.pop_back();
//        }
//    }
//    vector<vector<int>> subsets(vector<int>& nums) {
//        n = nums.size();
//        dfs(nums, 0);
//        return ret;
//    }
//};


//class Solution {
//    int ret, sum, n;
//public:
//    void dfs(vector<int>& nums, int start)
//    {
//        ret += sum;
//        for (int i = start; i < n; ++i)
//        {
//            sum ^= nums[i];
//            dfs(nums, i + 1);
//            sum ^= nums[i];   //回退
//        }
//    }
//    int subsetXORSum(vector<int>& nums) {
//        n = nums.size();
//        dfs(nums, 0);
//        return ret;
//    }
//};



//class Solution {
//    vector<vector<int>> ret;
//    vector<int> path;
//    vector<bool> vis;  //记录当前元素是否使用过
//    int n;
//public:
//    void dfs(vector<int>& nums)
//    {
//        if (path.size() == n)
//        {
//            ret.push_back(path);
//            return;
//        }
//        for (int i = 0; i < n; ++i)
//        {
//            //这里如果同一层相同的元素是不能使用的，但是不同层是可以的
//            if (i > 0 && nums[i - 1] == nums[i] && !vis[i - 1]) continue;
//            if (vis[i]) continue;
//            vis[i] = true;
//            path.push_back(nums[i]);
//            dfs(nums);
//            vis[i] = false;
//            path.pop_back();
//        }
//    }
//    vector<vector<int>> permuteUnique(vector<int>& nums) {
//        sort(nums.begin(), nums.end());
//        n = nums.size();
//        vis.resize(n, false);
//        dfs(nums);
//        return ret;
//    }
//};



//class Solution {
//    string hash[10] = { "","","abc","def","ghi","jkl","mno","pqrs","tuv","wxyz" };
//    vector<string> ret;
//    string path;
//    int n;
//public:
//    void dfs(string& str, int j)
//    {
//        //返回结果
//        if (path.size() == n)
//        {
//            ret.push_back(path);
//            return;
//        }
//        for (auto ch : hash[str[j] - '0'])
//        {
//            path += ch;
//            dfs(str, j + 1);
//            path.pop_back();
//        }
//
//    }
//    vector<string> letterCombinations(string digits) {
//        n = digits.size();
//        if (n == 0) return ret;
//        dfs(digits, 0);
//        return ret;
//    }
//};


//class Solution {
//    vector<string> ret;
//    string path;
//    int _n;
//    int left, right;  //左括号以及右括号的数量
//public:
//    vector<string> generateParenthesis(int n) {
//        _n = n;
//        dfs();
//        return ret;
//    }
//    void dfs()
//    {
//        if (right == _n)
//        {
//            //收集结果
//            ret.push_back(path);
//            return;
//        }
//        //放入左括号
//        if (left < _n)
//        {
//            path.push_back('(');
//            ++left;
//            dfs();
//            path.pop_back();
//            --left;
//        }
//        //放入右括号,必须要保证右括号少于左括号
//        if (right < left)
//        {
//            path.push_back(')');
//            ++right;
//            dfs();
//            path.pop_back();
//            --right;
//        }
//    }
//};

//class Solution {
//    vector<vector<int>> ret;
//    vector<int> path;
//    int _n, _k;
//public:
//    void dfs(int start)
//    {
//        if (path.size() == _k)
//        {
//            ret.push_back(path);
//            return;
//        }
//        for (int i = start; i <= _n; ++i)
//        {
//            path.push_back(i);
//            dfs(i + 1);
//            path.pop_back();
//        }
//    }
//    vector<vector<int>> combine(int n, int k) {
//        _n = n;
//        _k = k;
//        dfs(1);
//        return ret;
//    }
//};



//class Solution {
//    int ret = 0;
//public:
//    void dfs(vector<int>& nums, int target, int m, int sum)
//    {
//        if (m == nums.size())
//        {
//            if (sum == target)   ++ret;
//            return;
//        }
//        //和
//        dfs(nums, target, m + 1, sum + nums[m]);
//        //减
//        dfs(nums, target, m + 1, sum - nums[m]);
//    }
//    int findTargetSumWays(vector<int>& nums, int target) {
//        dfs(nums, target, 0, 0);
//        return ret;
//    }
//};