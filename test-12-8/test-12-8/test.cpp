#include <iostream>
#include <vector>
using namespace std;

//void MergeSortNonR(vector<int>& arr, vector<int>& tmp)
//{
//	int n = arr.size();
//	int gap = 1;
//	while (gap < n)
//	{
//		for (int i = 0; i < n; i += 2*gap)
//		{
//			//[i,i+gap-1] [i+gap,i+2*gap-1]
//			int begin1 = i, end1 = i + gap - 1;
//			int begin2 = i + gap, end2 = i + 2 * gap - 1;
//			//处理边界情况
//			if (begin2 >= n) break; //这里最后的一组的第二组没有数据，不用排序
//			if (end2 >= n) end2 = n - 1;
//			//这里进行一次排序
//			int j = i, k = i;  //提前赋值，防止begin1改变
//			while (begin1 <= end1 && begin2 <= end2)
//			{
//				tmp[j++] = arr[begin1] < arr[begin2] ? arr[begin1++] : arr[begin2++];
//			}
//			while (begin1 <= end1)
//			{
//				tmp[j++] = arr[begin1++];
//			}
//			while (begin2 <= end2)
//			{
//				tmp[j++] = arr[begin2++];
//			}
//			//还原回原数组
//			for (; k <= end2; ++k)
//				arr[k] = tmp[k];
//		}
//		gap *= 2;
//	}
//}
//

//快速选择排序
void Qsort(vector<int>& arr, int l, int r)
{
	if (l >= r) return;
	int key = arr[rand() % (r - l + 1) + l];
	int left = l - 1, right = r + 1;
	int i = l;
	while (i < right)
	{
		if (key > arr[i]) swap(arr[++left], arr[i++]);
		else if (key == arr[i]) ++i;
		else swap(arr[--right], arr[i]);
	}
	Qsort(arr, l, left);
	Qsort(arr, right, r);
}

int main()
{
	srand(time(nullptr));
	vector<int> arr = { 2,4,5,2,3,6,7,5,7,9,9,8,0 };
	int n = arr.size();
	vector<int> tmp(n);
	//MergeSortNonR(arr, tmp);
	Qsort(arr, 0, n - 1);
	for (auto e : arr)
	{
		cout << e << " ";
	}
	return 0;
}


