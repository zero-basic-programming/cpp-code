#include <iostream>
#include <string>
#include <winsock.h>
#include <cstring>
using namespace std;

////链接静态库
//#pragma comment (lib,"ws2_32.lib")
//
//static string serverip = "43.139.37.242";
//static uint16_t serverport = 3389;
//
//int main()
//{
//	WSADATA wsaData;
//	//启动Winsock
//	if (WSAStartup(MAKEWORD(2, 2), &wsaData) != 0)           // 初始化Winsock库
//	{
//		cerr << "WSAStartup() error!" << endl;
//	}
//	else
//	{
//		cout << "WSAStartup() success" << endl;
//	}
//	SOCKET sock;
//	sock = socket(AF_INET, SOCK_DGRAM, 0); //UDP
//	if (sock == -1)
//	{
//		cerr << "socket fail" << endl;
//	}
//	else cout << "socket success" << endl;
//	//填写server信息
//	struct sockaddr_in server;
//	memset(&server, 0, sizeof(server));
//	server.sin_addr.s_addr = inet_addr(serverip.c_str());
//	server.sin_family = AF_INET;
//	server.sin_port = htons(serverport);
//	while (true)
//	{
//		//开始向服务器发送信息
//		cout << "Enter# " << endl;
//		string message;
//		getline(cin, message);
//		int s = sendto(sock, message.c_str(),message.size(), 0, (struct sockaddr*)&server, sizeof(server));
//		if (s < 0)
//		{
//			cerr << "send to error" << endl;
//			break;
//		}
//		//接受服务器送来的信息
//		char buffer[1024];
//		buffer[0] = 0; // C式清空
//		struct sockaddr_in peer;
//		int len = sizeof(peer);
//		int n = recvfrom(sock, buffer, len, 0, (struct sockaddr*)&peer, &len);
//		if (n > 0)
//		{
//			buffer[n] = 0;
//			cout << "server[echo] # " << buffer << endl;
//		}
//		else break;
//	}
//	return 0;
//}


//class Solution {
//public:
//    bool traversal(TreeNode* root, TreeNode* node, vector<TreeNode*>& path)
//    {
//        if (root == nullptr)
//        {
//            return false;
//        }
//        //放入path中
//        if (root == node)
//        {
//            path.push_back(node);
//            return true;
//        }
//        //遍历左右子树
//        if (traversal(root->left, node, path))
//        {
//            return true;
//        }
//        path.push_back(node); // 中
//        if (traversal(root->right, node, path))
//        {
//            return true;
//        }
//        //每次都要回退
//        path.pop_back();
//        return false;
//    }
//    TreeNode* lowestCommonAncestor(TreeNode* root, TreeNode* p, TreeNode* q) {
//        //分别找到两个节点的路径，然后从前往后去遍历找到他们不同节点的前一个节点
//        vector<TreeNode*> ppath;
//        vector<TreeNode*> qpath;
//        //得到p，q的路径
//        traversal(root, p, ppath);
//        traversal(root, q, qpath);
//        //从前面开始遍历
//        int i = 0;
//        while (true)
//        {
//            if (ppath[i] != qpath[i])
//            {
//                --i;
//                break;
//            }
//            ++i;
//        }
//        return ppath[i];
//    }
//};


//class Solution {
//public:
//    TreeNode* lowestCommonAncestor(TreeNode* root, TreeNode* p, TreeNode* q) {
//        if (root == nullptr || root == p || root == q)
//        {
//            return root;
//        }
//        //后序遍历模拟回溯过程
//        TreeNode* left = lowestCommonAncestor(root->left, p, q);
//        TreeNode* right = lowestCommonAncestor(root->right, p, q);
//        if (left != nullptr && right != nullptr) return root;
//        //判断左右逻辑
//        if (left && !right)
//        {
//            return left;
//        }
//        else if (!left && right)
//        {
//            return right;
//        }
//        else return nullptr;
//    }
//};

