#include <iostream>
#include <vector>
using namespace std;

//int main()
//{
//	vector<int> weight = { 1, 3, 4 };
//	vector<int> value = { 15, 20, 30 };
//	int bagweight = 4;
//
//	int row = weight.size();
//	//dp ,全部初始化为0
//	vector<vector<int>> vv(row, vector<int>(bagweight+1, 0));
//	//因为要使用到上一行的数据，对第一行进行初始化
//	for (int i = 1; i <= bagweight; ++i)
//	{
//		if (weight[0] <= i)
//			vv[0][i] = value[0];
//	}
//	/*for (auto& v : vv)
//	{
//		for (auto& e : v)
//		{
//			cout << e << " ";
//		}
//		cout << endl;
//	}*/
//	for (int i = 1; i < row; ++i) //遍历物品
//	{
//		for (int j = 1; j <= bagweight; ++j) //遍历背包
//		{
//			//状态转移方程
//			if (weight[i] > j)
//				vv[i][j] = vv[i - 1][j];
//			else
//				vv[i][j] = max(vv[i - 1][j], vv[i - 1][j - weight[i]] + value[i]);
//		}
//	}
//	for (auto& v : vv)
//	{
//		for (auto& e : v)
//		{
//			cout << e << " ";
//		}
//		cout << endl;
//	}
//	//cout << vv[row - 1][bagweight - 1];
//	return 0;
//}



//int main()
//{
//	vector<int> weight = { 1, 3, 4 };
//	vector<int> value = { 15, 20, 30 };
//	int bagweight = 4;
//
//	int row = weight.size();
//	int col = bagweight;
//	vector<int> v(col + 1, 0);
//	for (int i = 0; i < row; ++i) // 先遍历物品
//	{
//		for (int j = col; j >= weight[i] ; --j) //这里如果j小于weight[i]就没有必要更新了
//		{
//			//状态转移方程
//			v[j] = max(v[j], v[j - weight[i]] + value[i]);
//		}
//	}
//	for (auto& e : v)
//	{
//		cout << e << " ";
//	}
//	cout << v[col] << endl;
//	return 0;
//}


//void test_1_wei_bag_problem() {
//	vector<int> weight = { 1, 3, 4 };
//	vector<int> value = { 15, 20, 30 };
//	int bagWeight = 4;
//
//	// 初始化
//	vector<int> dp(bagWeight + 1, 0);
//	for (int i = 0; i < weight.size(); i++) { // 遍历物品
//		for (int j = bagWeight; j >= weight[i]; j--) { // 遍历背包容量
//			dp[j] = max(dp[j], dp[j - weight[i]] + value[i]);
//		}
//	}
//	cout << dp[bagWeight] << endl;
//}
//
//int main() {
//	test_1_wei_bag_problem();
//}


//class Solution {
//public:
//    int fib(int n) {
//        //dp
//        if (n == 0)
//        {
//            return 0;
//        }
//        if (n == 1 || n == 2)
//            return 1;
//        int first = 1;
//        int second = 1;
//        int third = 2;
//        for (int i = 2; i < n; ++i)
//        {
//            third = first + second;
//            //更新
//            first = second;
//            second = third;
//        }
//        return third;
//    }
//};

//class Solution {
//public:
//    int minCostClimbingStairs(vector<int>& cost) {
//        //dp
//        int size = cost.size();
//        vector<int> dp(size + 1, 0);
//        cost.resize(size + 1, 0);
//        //初始化
//        dp[0] = cost[0];
//        dp[1] = cost[1];
//        for (int i = 2; i <= size; ++i)
//        {
//            //状态转移方程
//            dp[i] = min(dp[i - 1], dp[i - 2]) + cost[i];
//        }
//
//        return dp[size];
//    }
//};


//class Solution {
//public:
//    int minCostClimbingStairs(vector<int>& cost) {
//        //dp
//        int dp1 = 0;
//        int dp2 = 0;
//        for (int i = 2; i <= cost.size(); ++i)
//        {
//            int dpi = min(dp2 + cost[i - 1], dp1 + cost[i - 2]);
//            //更新
//            dp1 = dp2;
//            dp2 = dpi;
//        }
//        return dp2;
//    }
//};

//class Solution {
//public:
//    int uniquePaths(int m, int n) {
//        //dp
//        vector<vector<int>> dp(m, vector<int>(n, 1));
//        for (int i = 1; i < m; ++i)
//        {
//            for (int j = 1; j < n; ++j)
//            {
//                //状态转移方程
//                dp[i][j] = (dp[i - 1][j] + dp[i][j - 1]);
//            }
//        }
//        return dp[m - 1][n - 1];
//    }
//};

class Solution {
public:
    int uniquePaths(int m, int n) {
        // //dp
        // vector<vector<int>> dp(m,vector<int>(n,1));
        // for(int i = 1;i<m;++i)
        // {
        //     for(int j = 1;j<n;++j)
        //     {
        //         //状态转移方程
        //         dp[i][j] = (dp[i-1][j] + dp[i][j-1]);
        //     }
        // }
        // return dp[m-1][n-1];

        //使用一维数组
        vector<int> dp(n, 1);
        for (int i = 1; i < m; ++i)
        {
            for (int j = 1; j < n; ++j)
            {
                dp[j] += dp[j - 1];
            }
        }
        return dp[n - 1];
    }
};

