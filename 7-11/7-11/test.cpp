#include <iostream>
#include <vector>
using namespace std;


//class Solution {
//    vector<vector<string>> ret;
//    vector<string> path;
//    int n;
//public:
//    bool is_true(string& str, int start, int end)
//    {
//        while (start < end)
//        {
//            if (str[start] != str[end])  return false;
//            ++start;
//            --end;
//        }
//        return true;
//    }
//    void dfs(string& str, int pos)
//    {
//        if (pos >= n)
//        {
//            ret.push_back(path);
//            return;
//        }
//        for (int i = pos; i < n; ++i)
//        {
//            //如果满足回文条件,就需要收集数据
//            if (is_true(str, pos, i))
//            {
//                path.push_back(str.substr(pos, i - pos + 1));
//                dfs(str, i + 1);
//                path.pop_back();
//            }
//        }
//    }
//    vector<vector<string>> partition(string s) {
//        n = s.size();
//        dfs(s, 0);
//        return ret;
//    }
//};

void AdjustDown(vector<int>& arr, int parent, int n)
{
	int child = 2 * parent + 1;
	while (child < n)
	{
		if (child + 1 < n && arr[child] < arr[child + 1]) ++child;
		if (arr[parent] < arr[child])
		{
			swap(arr[parent], arr[child]);
			parent = child;
			child = 2 * parent + 1;
		}
		else break;
	}
}


void HeapSort(vector<int>& arr)
{
	int n = arr.size();
	for (int i = (n - 1 - 1) / 2; i >= 0; --i)
	{
		AdjustDown(arr, i, n);
	}
	//开始排序
	for (int i = 1; i < n; ++i)
	{
		swap(arr[n - i], arr[0]);
		AdjustDown(arr, 0, n - i);
	}
}

int main()
{
	vector<int> arr = { 4,2,3,1,15,7,6,4,7,9,0,8,0 };
	HeapSort(arr);
	for (auto e : arr)
	{
		cout << e << " ";
	}
	return 0;
}