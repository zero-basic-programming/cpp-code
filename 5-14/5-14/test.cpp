#include <iostream>
#include <vector>
using namespace std;


//class Solution {
//public:
//    int digitOneInNumber(int num) {
//        int res = 0;
//        long digit = 1;
//        int high = num / 10, low = 0, cur = num % 10;
//        while (cur != 0 || high != 0)
//        {
//            if (cur == 0) res += high * digit;
//            else if (cur == 1) res += high * digit + low + 1;
//            else res += (high + 1) * digit;
//            //迭代cur
//            low += cur * digit;
//            cur = high % 10;
//            digit *= 10;
//            high /= 10;
//        }
//        return res;
//    }
//};


//class Solution {
//public:
//    int inventoryManagement(vector<int>& stock) {
//        int cur = 0, count = 0;
//        for (auto e : stock)
//        {
//            if (count == 0)
//            {
//                count = 1;
//                cur = e;
//                continue;
//            }
//            if (e == cur) ++count;
//            else --count;
//        }
//        return cur;
//    }
//};


//class Solution {
//public:
//    vector<int> decorateRecord(TreeNode* root) {
//        //层序遍历
//        vector<int> ret;
//        if (root == nullptr) return ret;
//        queue<TreeNode*> q;
//        q.push(root);
//        while (q.size())
//        {
//            int sz = q.size();
//            for (int i = 0; i < sz; ++i)
//            {
//                TreeNode* node = q.front();
//                q.pop();
//                ret.push_back(node->val);
//                //放入左右子节点
//                if (node->left) q.push(node->left);
//                if (node->right) q.push(node->right);
//            }
//        }
//        return ret;
//    }
//};


//class Solution {
//public:
//    vector<vector<int>> decorateRecord(TreeNode* root) {
//        vector<vector<int>> ret;
//        if (root == nullptr) return ret;
//        queue<TreeNode*> q;
//        q.push(root);
//        while (q.size())
//        {
//            int sz = q.size();
//            vector<int> tmp;
//            for (int i = 0; i < sz; ++i)
//            {
//                TreeNode* node = q.front();
//                q.pop();
//                tmp.push_back(node->val);
//                if (node->left) q.push(node->left);
//                if (node->right) q.push(node->right);
//            }
//            ret.push_back(tmp);
//        }
//        return ret;
//    }
//};


