#include <iostream>
#include <vector>
using namespace std;


//class Solution {
//public:
//    int pivotIndex(vector<int>& nums) {
//        //使用前后缀和的思想，分别计算得出前后缀和，最后从左往右确定结果
//        int n = nums.size();
//        vector<int> frontSum(n), backSum(n);
//        for (int i = 1; i < n; ++i) frontSum[i] = frontSum[i - 1] + nums[i - 1];
//        for (int i = n - 2; i >= 0; --i) backSum[i] = backSum[i + 1] + nums[i + 1];
//        //从左往右找结果
//        for (int i = 0; i < n; ++i)
//        {
//            if (frontSum[i] == backSum[i]) return i;
//        }
//        return -1;
//    }
//};


//class Solution {
//public:
//    int findMaxLength(vector<int>& nums) {
//        int n = nums.size();
//        unordered_map<int, int> hash;  //到i位置的所有元素的总和和下标i的映射关系
//        hash[0] = -1;
//        int sum = 0;
//        int ret = 0;
//        for (int i = 0; i < n; ++i)
//        {
//            sum += nums[i] == 0 ? -1 : 1;
//            if (hash.count(sum)) ret = max(ret, i - hash[sum]);
//            else hash[sum] = i;
//        }
//        return ret;
//    }
//};


