#include <iostream>
#include <vector> 
using namespace std;

////用来记录周围炸开范围的偏移量
//int dx[4] = { 0,0,1,-1 };
//int dy[4] = { 1,-1,0,0 };
//int n;   //有多少行列
//
//int getCurVal(vector<vector<int>>& nums, int i, int j)
//{
//    int val = nums[i][j];
//    for (int k = 0; k < 4; ++k)
//    {
//        //判断是否越界
//        int x = i + dx[k], y = j + dy[k];
//        if (x >= 0 && x <n && y>= 0 && y < n) val += nums[x][y];
//    }
//    return val;
//}
//
//int main()
//{
//    n = 5;
//    vector<vector<int>> nums(n, vector<int>(n));
//    nums[0] = { 1,2,3,3,4 };
//    nums[1] = { 0,0,0,0,0 };
//    nums[2] = { 0,0,1,0,0 };
//    nums[3] = { 0,0,0,0,0 };
//    nums[4] = { 9,0,9,9,9 };
//    //遍历每一个位置，得出最大值
//    int ret = 0;
//    for (int i = 0; i < n; ++i)
//    {
//        for (int j = 0; j < n; ++j)
//        {
//            int tmp = getCurVal(nums, i, j);
//            ret = max(ret, tmp);
//        }
//    }
//    cout << ret << endl;
//    return 0;
//}



int main()
{
    int n = 3, m = 2;
    vector<int> beauty = {1,2,3};
    vector<int> sprise = {5,1};
    vector<vector<int>> dp(n + 1, vector<int>(m + 1));
    //使用动态规划
    for (int i = 1; i <= n; ++i)
    {
        for (int j = 1; j <= m; ++j)
        {
            //这里不能出现j后面没有对应的i
            if (i - j <= n-m) continue;
            dp[i][j] = max(dp[i - 1][j], dp[i][j - 1]);   //这里就是不选择当前i，j对应的数据
            dp[i][j] = max(dp[i][j], dp[i - 1][j - 1] + beauty[i - 1] * sprise[j - 1]);
        }
    }
    cout << dp[n][m] << endl;
    return 0;
}