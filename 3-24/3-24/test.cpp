#include <iostream>
#include <vector>
using namespace std;


//class Solution {
//    int ret = 0;  //收集结果
//    int n;  //记录record的大小
//    vector<int> tmp;
//public:
//    void MergeSort(vector<int>& arr, int left, int right)
//    {
//        if (left >= right) return;
//        int mid = (left + right) >> 1;
//        MergeSort(arr, left, mid);
//        MergeSort(arr, mid + 1, right);
//        //一边归并一边收集结果
//        int begin1 = left, end1 = mid;
//        int begin2 = mid + 1, end2 = right;
//        int i = left;
//        while (begin1 <= end1 && begin2 <= end2)
//        {
//            //收集结果
//            if (arr[begin1] > arr[begin2]) ret += (end2 - begin2 + 1);  //后面的元素也满足逆序对
//            tmp[i++] = arr[begin1] > arr[begin2] ? arr[begin1++] : arr[begin2++];
//        }
//        while (begin1 <= end1)   tmp[i++] = arr[begin1++];
//        while (begin2 <= end2)   tmp[i++] = arr[begin2++];
//        //还原回原数组
//        for (int j = left; j <= right; ++j) arr[j] = tmp[j];
//    }
//    int reversePairs(vector<int>& record) {
//        n = record.size();
//        tmp.resize(n);
//        MergeSort(record, 0, record.size() - 1);
//        return ret;
//    }
//};


//class Solution {
//    vector<int> ret;  //返回结果
//    vector<int> index;  //用来记录当前元素和原来的下标的映射关系
//    vector<int> tmp;  //用于归并
//    vector<int> tmpindex;  //用于保存每一次数据和下标的关系
//    int n;  //数组大小
//public:
//    void MergeSort(vector<int>& arr, int left, int right)
//    {
//        if (left >= right) return;
//        int mid = (left + right) >> 1;
//        MergeSort(arr, left, mid);
//        MergeSort(arr, mid + 1, right);
//        int begin1 = left, end1 = mid;
//        int begin2 = mid + 1, end2 = right;
//        int i = left;
//        while (begin1 <= end1 && begin2 <= end2)
//        {
//            if (arr[begin1] > arr[begin2])
//            {
//                ret[index[begin1]] += (end2 - begin2 + 1);  //当前位置比后面的元素都大
//                tmpindex[i] = index[begin1];
//                tmp[i++] = arr[begin1++];
//            }
//            else
//            {
//                tmpindex[i] = index[begin2];
//                tmp[i++] = arr[begin2++];
//            }
//        }
//        while (begin1 <= end1)
//        {
//            tmpindex[i] = index[begin1];
//            tmp[i++] = arr[begin1++];
//        }
//        while (begin2 <= end2)
//        {
//            tmpindex[i] = index[begin2];
//            tmp[i++] = arr[begin2++];
//        }
//        //还原
//        for (int j = left; j <= right; ++j)
//        {
//            arr[j] = tmp[j];
//            index[j] = tmpindex[j];
//        }
//    }
//    vector<int> countSmaller(vector<int>& nums) {
//        n = nums.size();
//        tmp.resize(n);
//        index.resize(n);
//        tmpindex.resize(n);
//        ret.resize(n);
//        //记录原数组坐标
//        for (int i = 0; i < n; ++i) index[i] = i;
//        MergeSort(nums, 0, n - 1);
//        return ret;
//    }
//};
//
//
//int main()
//{
//    vector<int> arr = { 5,2,6,1 };
//    Solution().countSmaller(arr);
//    return 0;
//}


class Solution {
    vector<int> tmp;
    int n;
    int ret = 0;
public:
    void MergeSort(vector<int>& nums, int left, int right)
    {
        if (left >= right) return;
        int mid = (left + right) >> 1;
        MergeSort(nums, left, mid);
        MergeSort(nums, mid + 1, right);
        int begin1 = left;
        int begin2 = mid + 1;
        int i = left;
        //找到合适的条件
        while (begin2 <= right)
        {
            //找到第一个<=的值
            while (begin1 <= mid && nums[begin1] / 2.0 <= nums[begin2]) ++begin1;
            if (begin1 > mid) break;
            ret += (mid - begin1 + 1);
            ++begin2;
        }
        //开始排序
        begin1 = left, begin2 = mid + 1;
        while (begin1 <= mid && begin2 <= right)
        {
            tmp[i++] = nums[begin1] < nums[begin2] ? nums[begin1++] : nums[begin2++];
        }
        while (begin1 <= mid) tmp[i++] = nums[begin1++];
        while (begin2 <= right) tmp[i++] = nums[begin2++];
        for (int j = left; j <= right; ++j) nums[j] = tmp[j];
    }
    int reversePairs(vector<int>& nums) {
        n = nums.size();
        tmp.resize(n);
        MergeSort(nums, 0, n - 1);
        return ret;
    }
};