#include <iostream>
#include <vector>
using namespace std;


//class Solution {
//public:
//    int jump(vector<int>& nums) {
//        int n = nums.size();
//        int i = 0;
//        int ret = 1;
//        while (i < n)
//        {
//            //每次往后找最长的位置
//            int len = nums[i];
//            int sep = 0;
//            for (int j = i + 1; j <= len && j < n; ++j)
//            {
//                sep = max(sep, j + nums[j]);
//            }
//            //往后移动
//            i += sep;
//            ++ret;
//        }
//        return ret;
//    }
//};
//
//
//int main()
//{
//    vector<int> arr = { 2,3,1,1,4 };
//    int ret = Solution().jump(arr);
//    cout << ret << endl;
//    return 0;
//}
//


//class Solution {
//    int count = 0;
//    int ret = 0;
//public:
//    int kthSmallest(TreeNode* root, int k) {
//        dfs(root, k);
//        return ret;
//    }
//    void dfs(TreeNode* root, int k)
//    {
//        if (root == nullptr) return;
//        dfs(root->left, k);
//        ++count;
//        if (count == k)
//        {
//            ret = root->val;
//            return;
//        }
//        dfs(root->right, k);
//    }
//};



//class Solution {
//public:
//    int jump(vector<int>& nums) {
//        int n = nums.size();
//        vector<int> dp(n);
//        for (int i = 0; i < n; ++i)
//        {
//            for (int j = 0; j < i; ++j)
//            {
//                if (j + nums[j] >= i)
//                    dp[i] = max(dp[i], dp[j] + 1);
//            }
//        }
//        return dp[n - 1];
//    }
//};


void MergeSort(vector<int>& arr,vector<int>& tmp)
{
	int n = arr.size();
	int gap = 1;
	while (gap < n)
	{
		for (int i = 0; i < n; i += 2 * gap)
		{
			//[i,i+gap-1] [i+gap,i+2*gap-1]
			int begin1 = i, end1 = i + gap - 1;
			int begin2 = i + gap, end2 = i + 2 * gap - 1;
			//处理边界条件
			if (begin2 >= n) break;
			if (end2 >= n) end2 = n - 1;
			int j = i;
			while (begin1 <= end1 && begin2 <= end2)
			{
				tmp[j++] = arr[begin1] < arr[begin2] ? arr[begin1++] : arr[begin2++];
			}
			while (begin1 <= end1)
			{
				tmp[i++] = arr[begin1++];
			}
			while (begin2 <= end2)
			{
				tmp[i++] = arr[begin2++];
			}
			//还原
			for (int i = begin1; i <= end2; ++i)
				arr[i] = tmp[i];
		}
		gap *= 2;
	}
}

int main()
{
	vector<int> arr = { 0,2,4,5,24,6,7,7,7 };
	int n = arr.size();
	vector<int> tmp(n);
	MergeSort(arr,tmp);
	for (auto e : arr)
	{
		cout << e << endl;
	}
	return 0;
}