#include <iostream>
#include <vector>
#include <cassert>
using namespace std;


namespace liang
{
	class string
	{
	public:
		string() :_str(nullptr), _size(0) {};
		~string()
		{
			// free memory
			if (_str)
			{
				delete _str;
				_str = nullptr;
				_size = 0;
			}
		}
		//拷贝构造
		string(const string& s)
		{
			//1.拿到空间，申请内存
			int len = s._size;
			_str = new char[len+1];
			//2.进行深拷贝
			for (int i = 0; i < len; ++i)
			{
				_str[i] = s._str[i];
			}
			_size = len;
			_str[_size] = '\0';
		}
		string(const char* str)
		{
			//1.拿到空间，申请内存
			int len = sizeof(str) - 1;
			_str = new char[len + 1];
			//2.进行深拷贝
			for (int i = 0; i < len; ++i)
			{
				_str[i] = str[i];
			}
			_size = len;
			_str[_size] = '\0';
		}
		//赋值重载
		string& operator=(string s)
		{
			swap(s);
			return *this;
		}
		char operator[](int pos)
		{
			assert(pos < _size);
			return _str[pos];
		}
		//交换函数，可以用于赋值重载，或者外部使用
		void swap(string& s)
		{
			::swap(_str, s._str);
			::swap(_size, s._size);
		}
		//常用赋值
		string& operator+=(const string& s)
		{
			//1.计算s中的空间大小
			int len = s._size;
			//2.开辟新的空间
			char* newStr = new char[len + _size + 1];   //最后需要添加'\0'
			//3.将当前的字符串和s中的字符串拼接到一起
			for (int i = 0; i < _size; ++i) newStr[i] = _str[i];
			for (int i = _size; i < len + _size; ++i) newStr[i] = s._str[i];
			newStr[len + _size] = '\0';
			delete _str;  //释放原来的内存
			_str = newStr;
			_size += len;
			return *this;
		}
		int size()
		{
			return _size;
		}
	private:
		char* _str;
		int _size;
	};
	void testString()
	{
		liang::string str("abcd");
		liang::string str2("defg");
		str += str2;
		for (int i = 0; i < str.size(); ++i) 
			cout << str[i] << endl;
	}

	//颜色
	enum color
	{
		RED,
		BLACK
	};

	//红黑树节点类
	typedef class RBTreeNode
	{
	public:
		RBTreeNode(int val, color col = RED)
			:_parent(nullptr),_left(nullptr),_right(nullptr),_val(val)
		{}
		int _val;
		RBTreeNode* _parent;
		RBTreeNode* _left;
		RBTreeNode* _right;
		color _col;
	}Node;
	//红黑树类
	class RBTree
	{
	public:
		RBTree()
			:_root(nullptr)
		{}
		bool insert(int val)
		{
			//1.根节点是否为空
			if (_root == nullptr)
			{
				_root = new Node(val);
				_root->_col = BLACK;
				return true;
			}
			//2.根据二叉树的插入逻辑来进行插入
			Node* cur = _root;
			Node* prev = nullptr;
			while (cur)
			{
				if (cur->_val < val)
				{
					prev = cur;
					cur = cur->_right;
				}
				else if (cur->_val > val)
				{
					prev = cur;
					cur = cur->_left;
				}
				else
				{
					//找到一样的，红黑树中是不能出现重复的数据
					return false;
				}
			}
			cur = new Node(val);
			cur->_parent = prev;
			//建立连接关系
			if (prev->_val < val)
				prev->_right = cur;
			else 
				prev->_left = cur;
			//3.调平衡
			while (prev && prev->_col == RED)
			{
				Node* gparent = prev->_parent;
				//3.1 gparent->_left == prev  || gparent->_right == prev
				if (gparent->_left == prev)
				{
					Node* uncle = gparent->_right;
					//3.1.2 看uncle
					if (uncle && uncle->_col == RED)
					{
						//情况1：uncle 和 prev 变成黑色，gparent变红色，向上迭代更新
						uncle->_col = prev->_col = BLACK;
						gparent->_col = RED;
						cur = gparent;
						prev = cur->_parent;
						continue;
					}
					else
					{
						//情况2，3：uncle不存在或者uncle存在为黑
						//情况2：gparent and prev and cur 3点同线 -> 右旋加变色
						//情况3：3点为折线 -> 先左旋后右旋最后变色
						if (cur == prev->_left)
						{
							RotateR(gparent);
							prev->_col = BLACK;
							gparent->_col = RED;
						}
						else
						{
							RotateLR(gparent);
							cur->_col = BLACK;
							gparent->_col = RED;
						}
						break;   //这里已经更新完成，不需要再迭代
					}
				}
				else
				{
					Node* uncle = gparent->_left;
					//跟上面情况类似
					if (uncle && uncle->_col == RED)
					{
						//变色加迭代
						uncle->_col = prev->_col = BLACK;
						gparent->_col = RED;
						cur = gparent;
						prev = cur->_parent;
						continue;
					}
					else
					{
						//旋转加变色
						if (cur == prev->_right)
						{
							//左旋加变色
							RotateL(gparent);
							prev->_col = BLACK;
							gparent->_col = RED;
						}
						else
						{
							RotateRL(gparent);
							cur->_col = BLACK;
							gparent->_col = RED;
						}
					}
					break;   //这里已经更新完成，不需要再迭代
				}
			}
			//4.把根节点的颜色置为黑色
			_root->_col = BLACK;
			return true;
		}
		void RotateL(Node* cur)
		{
			Node* subR = cur->_right;
			Node* subRL = subR->_left;
			Node* parent = cur->_parent;
			//建立subRL 和 cur 的连接
			cur->_right = subRL;
			if (subRL) subRL->_parent = cur;
			//更改subR 和 cur的连接
			subR->_left = cur;
			cur->_parent = subR;
			if (parent == nullptr)
			{
				_root = subR;
				subR->_parent = nullptr;
			}
			else
			{
				//更改parent 和 subR的连接
				if (parent->_left == cur)
				{
					parent->_left = subR;
				}
				else
				{
					parent->_right= subR;
				}
				subR->_parent = parent;
			}
		}
		void RotateR(Node* cur)
		{
			Node* parent = cur->_parent;
			Node* subL = cur->_left;
			Node* subLR = subL->_right;
			//建立subLR 和 cur的连接
			cur->_left = subLR;
			if (subLR) subLR->_parent = cur;
			//连接subL和cur
			cur->_parent = subL;
			subL->_right = cur;
			if (parent == nullptr)
			{
				_root = subL;
			}
			else
			{
				if (parent->_left == cur) parent->_left = subL;
				else parent->_right = subL;
			}
			subL->_parent = parent;
		}
		void RotateLR(Node* cur)
		{
			RotateL(cur->_left);
			RotateR(cur);
		}
		void RotateRL(Node* cur)
		{
			RotateR(cur->_right);
			RotateL(cur);
		}
	private:
		Node* _root;
	};
	void TestRBTree()
	{
		RBTree t;
		t.insert(3);
		t.insert(2);
		t.insert(1);
		cout << endl;
	}
}




int main()
{
	//liang::testString();
	liang::TestRBTree();
	return 0;
}