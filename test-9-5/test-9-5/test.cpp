#include <iostream>
#include <vector>
using namespace std;

//class Solution {
//public:
//    int subarraySum(vector<int>& nums, int k) {
//        unordered_map<int, int> hash;
//        hash[0] = 1; //处理边界情况
//        int n = nums.size();
//        int sum = 0;
//        int ret = 0;
//        for (int i = 0; i < n; ++i)
//        {
//            sum += nums[i];
//            if (hash[sum - k] > 0)  ret += hash[sum - k];
//            hash[sum]++; //把当前以i位置为结尾的和统计到hash中
//        }
//        return ret;
//    }
//};


//class Solution {
//public:
//    int subarraysDivByK(vector<int>& nums, int k) {
//        unordered_map<int, int> hash;
//        int sum = 0;
//        hash[0] = 1;
//        int ret = 0;
//        for (auto e : nums)
//        {
//            sum += e;
//            int r = (sum % k + k) % k;  //保证最后是正数
//            if (hash[r] > 0) ret += hash[r];
//            hash[r]++;
//        }
//        return ret;
//    }
//};


//class Solution {
//public:
//    int maxTurbulenceSize(vector<int>& arr) {
//        int n = arr.size();
//        //f(i) 表示以i位置为结尾上升，g(i)表示以i位置为结尾下降
//        vector<int> f(n, 1), g(n, 1);
//        int fmax = 1;
//        int gmax = 1;
//        for (int i = 1; i < n; ++i)
//        {
//            if (arr[i] > arr[i - 1])
//                f[i] = g[i - 1] + 1;
//            else if (arr[i] < arr[i - 1])
//                g[i] = f[i - 1] + 1;
//            fmax = max(fmax, f[i]);
//            gmax = max(gmax, g[i]);
//        }
//        return max(fmax, gmax);
//    }
//};


