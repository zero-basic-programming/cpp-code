#include <iostream>
#include <vector>
using namespace std;


//class Solution {
//public:
//    int lengthOfLongestSubstring(string s) {
//        int hash[128] = { 0 };
//        int n = s.size();
//        int left = 0; int right = 0; int len = 0;
//        while (right < n)
//        {
//            //进窗口
//            ++hash[s[right]];
//            //判断
//            while (hash[s[right]] > 1)
//            {
//                //出窗口
//                hash[s[left++]]--;
//            }
//            //收集结果
//            len = max(len, right - left + 1);
//            ++right;
//        }
//        return len;
//    }
//};


//class Solution {
//public:
//    int deleteAndEarn(vector<int>& nums) {
//        sort(nums.begin(), nums.end());
//        //找到最大的元素，然后开辟arr数组，用来记录当前值的总和
//        int n = nums.size();
//        int maxval = nums[n - 1];
//        vector<int> arr(maxval + 1, 0);
//        for (int i = 0; i < n; ++i)
//        {
//            arr[nums[i]] += nums[i];
//        }
//        //相当于打家劫舍，选和不选的方式
//        vector<int> f(maxval + 1, 0);
//        auto g = f;
//        //初始化
//        f[0] = arr[0];
//        for (int i = 1; i < maxval + 1; ++i)
//        {
//            f[i] = g[i - 1] + arr[i];
//            g[i] = max(f[i - 1], g[i - 1]);
//        }
//        return max(f[maxval], g[maxval]);
//    }
//};

