#include <iostream>
#include <vector>
using namespace std;


//class Solution {
//    vector<vector<int>> ret;
//    vector<int> path;
//    int sum = 0;
//public:
//    vector<vector<int>> combinationSum(vector<int>& candidates, int target) {
//        dfs(candidates, target, 0);
//        return ret;
//    }
//    void dfs(vector<int>& candidates, int target, int k)
//    {
//        if (sum > target) return;
//        //收集结果
//        if (sum == target)
//        {
//            ret.push_back(path);
//            return;
//        }
//        for (int i = k; i < candidates.size(); i++)
//        {
//            sum += candidates[i];
//            path.push_back(candidates[i]);
//            dfs(candidates, target, i);
//            //回溯
//            path.pop_back();
//            sum -= candidates[i];
//        }
//    }
//};


//class Solution {
//    vector<string> ret;
//    string path;
//public:
//    vector<string> letterCasePermutation(string s) {
//        dfs(s, 0);
//        return ret;
//    }
//    void dfs(string& s, int pos)
//    {
//        if (pos == s.size())
//        {
//            ret.push_back(path);
//            return;
//        }
//        //递归变化和不变
//        //不变
//        path += s[pos];
//        dfs(s, pos + 1);
//        path.pop_back();
//        //变,数字字符不变
//        if ('0' > s[pos] || '9' < s[pos])
//        {
//            path += change(s[pos]);
//            dfs(s, pos + 1);
//            path.pop_back();
//        }
//    }
//    char change(char ch)
//    {
//        if ('a' <= ch && ch <= 'z')
//        {
//            return ch - 'a' + 'A';
//        }
//        return ch - 'A' + 'a';
//    }
//};



void MergeSortNonR(vector<int>& arr, vector<int>& tmp)
{
	int n = arr.size();
	int gap = 1;
	while (gap < n)
	{
		for (int i = 0; i < arr.size(); i += 2 * gap)
		{
			//[i,i+gap-1] [i+gap,i+2*gap-1]
			int begin1 = i, end1 = i + gap - 1;
			int begin2 = i + gap, end2 = i + 2 * gap - 1;
			//处理边界情况
			//最后面一组没有数据，不需要处理
			if (begin2 >= n) break;
			//最后一组有数据，但是不全,需要处理越界情况
			if (end2 >= n) end2 = n - 1;
			//单层归并逻辑
			int k = i, j = i;
			while (begin1 <= end1 && begin2 <= end2)
			{
				tmp[j++] = arr[begin1] < arr[begin2] ? arr[begin1++] : arr[begin2++];
			}
			//处理剩下的数据
			while (begin1 <= end1)
			{
				tmp[j++] = arr[begin1++];
			}
			while (begin2 <= end2)
			{
				tmp[j++] = arr[begin2++];
			}
			//还原回原数组
			for (; k <= end2; ++k)
			{
				arr[k] = tmp[k];
			}
		}
		gap *= 2;
	}
}

//快速选择排序
void Qsort(vector<int>& arr, int l, int r)
{
	if (l >= r) return;
	//选基准值
	int key = arr[rand() % (r - l + 1) + l];
	int left = l - 1, right = r + 1, i = l;
	while (i < right)
	{
		if (arr[i] < key) swap(arr[i++], arr[++left]);
		else if (arr[i] == key) ++i;
		else swap(arr[i], arr[--right]);
	}
	//递归左右
	Qsort(arr, l, left);
	Qsort(arr, right, r);
}



int main()
{
	srand(time(nullptr));
	vector<int> arr = { 1,4,2,6,3,4,6,7,84,5,6,8 };
	vector<int> tmp(arr.size());
	//MergeSortNonR(arr, tmp);
	Qsort(arr, 0, arr.size() - 1);
	for (auto e : arr)
	{
		cout << e << " ";
	}
	return 0;
}