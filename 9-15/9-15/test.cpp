#include <iostream>
#include <vector>
using namespace std;


////节点类
//struct TreeNode
//{
//    TreeNode(int val) :_val(val), _left(nullptr), _right(nullptr) {}
//    int _val;
//    TreeNode* _left;
//    TreeNode* _right;
//};
//
//class Solution
//{
//private:
//    vector<TreeNode*> path;
//    vector<TreeNode*> ret;
//public:
//    TreeNode* dfs(TreeNode* root, TreeNode* start, TreeNode* end)
//    {
//        //通过后序遍历来找到他们的公共祖先节点，如果该节点是左右两个节点都不为空就是祖先节点
//        if (root == nullptr) return nullptr;
//        TreeNode* left = dfs(root->_left, start, end);
//        TreeNode* right = dfs(root->_right, start, end);
//        if (left && right) return root;   //找到节点
//        if (root == start || root == end) return root;
//        else return nullptr;
//    }
//    void findNode(TreeNode* root, TreeNode* target)
//    {
//        if (root == nullptr) return;
//        path.push_back(root);
//        if (root == target)
//        {
//            for (auto& node : path)
//                ret.push_back(node);
//        }
//        findNode(root->_left, target);
//        findNode(root->_right, target);
//    }
//    vector<TreeNode*> FindShortestDistance(TreeNode* root, TreeNode* start, TreeNode* end)
//    {
//        //1.找到他们的公共祖先节点
//        TreeNode* parent = dfs(root, start, end);
//        //2.通过他们公共祖先节点再回溯找两个节点，中间通过数组来记录中间节点
//        findNode(parent, start);
//        //这里可能end就是parent,就不需要处理了
//        if (end != parent)
//            findNode(parent, end);
//        return ret;
//    }
//};


//int main()
//{
//    int a = 1, b = 4, c = 2;
//    a = (b = c) + 8;
//    cout << a << b << c << endl;
//    (a = b) = c = 9;
//    cout << a << b << c << endl;
//    return 0;
//}


