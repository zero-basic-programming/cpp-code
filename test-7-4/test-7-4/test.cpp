#include <iostream>
#include <string>
#include <sstream>
#include <cmath>
using namespace std;

//bool is_prime(int month) {
//    if (month == 1)  return false;
//    for (int i = 2; i <= sqrt(month); ++i) {
//        if (month % i == 0) {
//            return false;
//        }
//    }
//    return true;
//}
//static int arr[13] = { 0, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };
//int GetMonthDay(int year, int month) {
//    if (month == 2 && ((year % 4 == 0 && year % 100 != 0) || year % 400 == 0)) {
//        return 29;
//    }
//    return arr[month];
//}
//
//int main() {
//    string s;
//    int startyear, startmonth, startday, endyear, endmonth, endday;
//    while (getline(cin, s)) {
//        stringstream ss(s);
//        ss >> startyear >> startmonth >> startday >> endyear >> endmonth >> endday;
//        int sum = 0;
//        while (startyear != endyear || startmonth != endmonth) {
//            if (is_prime(startmonth)) {
//                sum += GetMonthDay(startyear, startmonth);
//            }
//            else
//                sum += 2 * GetMonthDay(startyear, startmonth);
//            ++startmonth;
//            if (startmonth == 13) {
//                startmonth = 1;
//                ++startyear;
//            }
//        }
//        //这里还有最后一次没有加
//        if (is_prime(startmonth)) {
//            sum += endday;
//        }
//        else sum += 2 * endday;
//        cout << sum << endl;
//    }
//    return 0;
//}

#include <iostream>
using namespace std;

int main() {
    int n = 0;
    while (cin >> n)
    {
        if (n == 1 || n == 2) {
            cout << n << endl;
            continue;
        }
        int first = 1;
        int second = 2;
        int third = 3;
        for (int i = 3; i <= n; ++i)
        {
            third = first + second;
            //迭代
            first = second;
            second = third;
        }
        if (third > 1000000)  cout << third % 1000000 << endl;
        else cout << third << endl;
    }
    return 0;
}