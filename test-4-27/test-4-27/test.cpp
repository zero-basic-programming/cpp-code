#include <iostream>
using namespace std;

//class Solution {
//public:
//    bool sametree(TreeNode* left, TreeNode* right)
//    {
//        if (left == nullptr && right != nullptr) return false;
//        else if (left != nullptr && right == nullptr)    return false;
//        else if (left == nullptr && right == nullptr)    return true;
//        else if (left->val != right->val)    return false;
//        //左树的左和右树的右比较
//        return sametree(left->left, right->right) && sametree(left->right, right->left);
//    }
//    bool isSymmetric(TreeNode* root) {
//        return sametree(root->left, root->right);
//    }
//};

//class Solution {
//public:
//    bool isSymmetric(TreeNode* root) {
//        if (root == NULL) return true;
//        stack<TreeNode*> st; // 这里改成了栈
//        st.push(root->left);
//        st.push(root->right);
//        while (!st.empty()) {
//            TreeNode* leftNode = st.top(); st.pop();
//            TreeNode* rightNode = st.top(); st.pop();
//            if (!leftNode && !rightNode) {
//                continue;
//            }
//            if ((!leftNode || !rightNode || (leftNode->val != rightNode->val))) {
//                return false;
//            }
//            st.push(leftNode->left);
//            st.push(rightNode->right);
//            st.push(leftNode->right);
//            st.push(rightNode->left);
//        }
//        return true;
//    }
//};