#pragma once
#include <iostream>
#include <cassert>
using namespace std;

namespace liang
{
	template <class T>
	class vector
	{
	public:
		typedef T* iterator;
		typedef const T* const_iterator;
		//默认成员函数
		vector():_start(nullptr),_finish(nullptr),_endofstorage(nullptr)
		{}
		vector(size_t n, const T& val)
		{
			reserve(n);
			for (int i = 0; i < n; ++i)
				push_back(val);
		}

		//通过任意迭代器来构造
		template <class InPutIterator>
		vector(InPutIterator first, InPutIterator last)
		{
			while (first != last)
			{
				push_back(*first);
				++first;
			}
		}
		vector(const vector<T>& v)
		{
			//通过构造出一个新的vector，然后再交换即可
			reserve(v.capacity());
			for (auto e : v)
			{
				push_back(e);
			}
		}
		vector<T>& operator=(vector<T> v)
		{
			swap(v);
			return *this;
		}
		~vector()
		{
			if (_start)
			{
				delete[] _start;
				_start = nullptr;
				_finish = nullptr;
				_endofstorage = nullptr;
			}
		}

		//迭代器相关函数
		iterator begin()
		{
			return _start;
		}
		iterator end()
		{
			return _finish;
		}
		const_iterator begin() const 
		{
			return _start;
		}
		const_iterator end() const
		{
			return _finish;
		}

		//容量和大小相关函数
		size_t size() const
		{
			return _finish - _start;
		}
		size_t capacity() const
		{
			return _endofstorage - _start;
		}
		void reserve(size_t n)
		{
			//不采取缩容的机制
			if (n > capacity())
			{
				size_t sz = size(); //这里记录好原来的空间大小，因为后续的操作可能影响到sz
				T* tmp = new T[n];
				if (_start)
				{
					//把数据拷贝到新数组中
					for (int i = 0; i < sz; ++i)
					{
						tmp[i] = _start[i];
					}
					delete[] _start;
				}
				_start = tmp;
				_finish = _start + sz;
				_endofstorage = _start + n;
			}
		}
		void resize(size_t n, const T& val = T())
		{
			if (n > size())
			{
				if (n > capacity())
				{
					//扩容
					reserve(n);
				}
				for (size_t i = size(); i < n; ++i)
				{
					push_back(val);
				}
			}
			else
			{
				_finish = _start + n;
			}
		}
		bool empty() const
		{
			return _start == _finish;
		}

		//修改容器内容相关函数
		void push_back(const T& val)
		{
			//检查扩容
			if (_finish == _endofstorage)
			{
				size_t newcapacity = capacity() == 0 ? 4 : 2 * capacity();
				reserve(newcapacity);
			}
			*_finish = val;
			++_finish;
		}
		void pop_back()
		{
			//删除不能删空
			assert(!empty());
			--_finish;
		}
		//效率低，不建议使用
		void insert(iterator pos, const T& val)
		{
			//这里扩容需要注意迭代器失效的问题
			if (_finish == _endofstorage)
			{
				size_t len = pos - _start;
				size_t newcapacity = _finish == _start ? 4 : 2 * capacity();
				reserve(newcapacity);
				pos = _start + len;
			}
			//移动数据
			iterator end = _finish - 1;
			while (end >= pos)
			{
				*(end + 1) = *end;
				--end;
			}
			*pos = val;
			++_finish;
		}
		//这里的迭代器也会失效，所以使用完之后需要更新
		iterator erase(iterator pos)
		{
			iterator end = pos + 1;
			while (end != _finish)
			{
				*(end - 1) = *end;
				++end;
			}
			_finish--;
			return pos;
		}
		void swap(vector<T>& v)
		{
			//这里的::是声名使用std的swap
			::swap(_start,v._start);
			::swap(_finish,v._finish);
			::swap(_endofstorage ,v._endofstorage);
		}

		//访问容器相关函数
		T& operator[](size_t i)
		{
			return _start[i];
		}
		const T& operator[](size_t i) const
		{
			return _start[i];
		}

		//查找函数
		iterator find(const T& val)
		{
			iterator pos = _start;
			while (pos != _finish)
			{
				if (*pos == val) return pos;
				++pos;
			}
			return nullptr; //找不到
		}
	private:
		iterator _start;
		iterator _finish;
		iterator _endofstorage;  //存储的空间
	};
	void testvector()
	{
		/*vector<std::string> vs;
		vs.push_back("nihoa");
		vs.push_back("chilema");
		vector<std::string> test(vs);
		for (auto& str : test)
		{
			cout << str << endl;
		}*/

		/*vector<int> v;
		v.push_back(1);
		v.push_back(1);
		v.push_back(1);
		v.push_back(1);
		v.push_back(1);
		v.resize(9, 0);
		v.push_back(1);
		for (auto e : v)
		{
			cout << e << " ";
		}
		cout << v.capacity() << endl;*/


		vector<int> v;
		v.push_back(1);
		v.push_back(1);
		v.push_back(5);
		v.push_back(1);
		v.push_back(2);
		vector<int>::iterator it = v.find(5);
		v.insert(it, 6);
		v.erase(it);
		for (auto& e : v)
		{
			cout << e << " ";
		}
		//全部删除
		it = v.begin();
		while (it != v.end())
		{
			it = v.erase(it);
		}
		cout << "start";
		for (auto& e : v)
		{
			cout << e << " ";
		}
		cout << "endl";
	}
}