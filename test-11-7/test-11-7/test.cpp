#include <iostream>
#include <cstring>
#include <vector>
using namespace std;

//const int N = 1010;
//int v[N], w[N];
//int dp[N][N];
//
//int main()
//{
//    int n, V;
//    cin >> n >> V;
//    for (int i = 1; i <= n; ++i)
//    {
//        cin >> v[i] >> w[i];
//    }
//    //动态规划 
//    //第一题
//    for (int i = 1; i <= n; ++i)
//    {
//        for (int j = 0; j <= V; ++j)
//        {
//            dp[i][j] = dp[i - 1][j];
//            if (j >= v[i])
//                dp[i][j] = max(dp[i][j], dp[i - 1][j - v[i]] + w[i]);
//        }
//    }
//    cout << dp[n][V] << std::endl;
//    //第二题
//    memset(dp, 0, sizeof dp);
//    //初始化
//    for (int j = 1; j <= V; ++j)
//    {
//        dp[0][j] = -1; //这里没有装满背包的话统一记录为-1
//    }
//    for (int i = 1; i <= n; ++i)
//    {
//        for (int j = 0; j <= V; ++j)
//        {
//            dp[i][j] = dp[i - 1][j];
//            if (j >= v[i] && dp[i - 1][j - v[i]] != -1)
//                dp[i][j] = max(dp[i][j], dp[i - 1][j - v[i]] + w[i]);
//        }
//    }
//    int ret = dp[n][V] == -1 ? 0 : dp[n][V];
//    cout << ret << std::endl;
//    return 0;
//}


//滚动数组做优化
//int main()
//{
//    int n, V;
//    cin >> n >> V;
//    for (int i = 1; i <= n; ++i)
//    {
//        cin >> v[i] >> w[i];
//    }
//    //动态规划 
//    //第一题
//    for (int i = 1; i <= n; ++i)
//    {
//        for (int j = V; j >= 0; --j)
//        {
//            if (j >= v[i])
//                dp[j] = max(dp[j], dp[j - v[i]] + w[i]);
//        }
//    }
//    cout << dp[V] << std::endl;
//    //第二题
//    memset(dp, 0, sizeof dp);
//    //初始化
//    for (int j = 1; j <= V; ++j)
//    {
//        dp[j] = -1; //这里没有装满背包的话统一记录为-1
//    }
//    for (int i = 1; i <= n; ++i)
//    {
//        for (int j = V; j >= 0; --j)
//        {
//            if (j >= v[i] && dp[j - v[i]] != -1)
//                dp[j] = max(dp[j], dp[j - v[i]] + w[i]);
//        }
//    }
//    int ret = dp[V] == -1 ? 0 : dp[V];
//    cout << ret << std::endl;
//    return 0;
//}


//class Solution {
//public:
//    bool canPartition(vector<int>& nums) {
//        int n = nums.size();
//        int sum = 0;
//        for (auto e : nums) sum += e;
//        //如果总和不为偶数就没有结果
//        if (sum % 2 != 0) return false;
//        int target = sum / 2;
//        vector<vector<bool>> dp(n + 1, vector<bool>(target + 1, false));
//        //初始化
//        dp[0][0] = true;
//        for (int i = 1; i <= n; ++i)
//        {
//            for (int j = 1; j <= target; ++j)
//            {
//                dp[i][j] = dp[i - 1][j];
//                if (j >= nums[i - 1])
//                    dp[i][j] = (dp[i][j] || dp[i - 1][j - nums[i - 1]]);
//            }
//        }
//        return dp[n][target];
//    }
//};


//滚动数组进行空间优化
//class Solution {
//public:
//    bool canPartition(vector<int>& nums) {
//        int n = nums.size();
//        int sum = 0;
//        for (auto e : nums) sum += e;
//        //如果总和不为偶数就没有结果
//        if (sum % 2 != 0) return false;
//        int target = sum / 2;
//        vector<bool> dp(target + 1, false);
//        //初始化
//        dp[0] = true;
//        for (int i = 1; i <= n; ++i)
//        {
//            for (int j = target; j >= 1; --j)
//            {
//                if (j >= nums[i - 1])
//                    dp[j] = (dp[j] || dp[j - nums[i - 1]]);
//            }
//        }
//        return dp[target];
//    }
//};


//class Solution{
//public:
//    int findTargetSumWays(vector<int>&nums, int target) {
//        int n = nums.size();
//        int sum = 0;
//        for (auto e : nums) sum += e;
//        int aim = (target + sum) / 2;
//        //处理边界条件
//        if (aim < 0 || (target + sum) % 2) return false;
//        vector<vector<int>> dp(n + 1,vector<int>(aim + 1));
//        //初始化
//        dp[0][0] = 1;
//        for (int i = 1; i <= n; ++i)
//        {
//            for (int j = 0; j <= aim; ++j)
//            {
//                dp[i][j] = dp[i - 1][j];  //不选当前元素
//                if (j >= nums[i - 1])
//                    dp[i][j] += dp[i - 1][j - nums[i - 1]];
//            }
//        }
//        return dp[n][aim];
//    }
//};


//滚动数组优化
//class Solution {
//public:
//    int findTargetSumWays(vector<int>& nums, int target) {
//        int n = nums.size();
//        int sum = 0;
//        for (auto e : nums) sum += e;
//        int aim = (target + sum) / 2;
//        //处理边界条件
//        if (aim < 0 || (target + sum) % 2) return false;
//        vector<int> dp(aim + 1);
//        //初始化
//        dp[0] = 1;
//        for (int i = 1; i <= n; ++i)
//        {
//            for (int j = aim; j >= 0; --j)
//            {
//                if (j >= nums[i - 1])
//                    dp[j] += dp[j - nums[i - 1]];
//            }
//        }
//        return dp[aim];
//    }
//};

//class Solution {
//public:
//    int lastStoneWeightII(vector<int>& stones) {
//        int n = stones.size();
//        int sum = 0;
//        for (auto e : stones) sum += e;
//        int aim = sum / 2;
//        vector<vector<int>> dp(n + 1, vector<int>(aim + 1));
//        for (int i = 1; i <= n; ++i)
//        {
//            for (int j = 0; j <= aim; ++j)
//            {
//                dp[i][j] = dp[i - 1][j];
//                if (j >= stones[i - 1])
//                    dp[i][j] = max(dp[i][j], dp[i - 1][j - stones[i - 1]] + stones[i - 1]);
//            }
//        }
//        return sum - 2 * dp[n][aim];
//    }
//};


//滚动数组优化
//class Solution {
//public:
//    int lastStoneWeightII(vector<int>& stones) {
//        int n = stones.size();
//        int sum = 0;
//        for (auto e : stones) sum += e;
//        int aim = sum / 2;
//        vector<int> dp(aim + 1);
//        for (int i = 1; i <= n; ++i)
//        {
//            for (int j = aim; j >= 0; --j)
//            {
//                if (j >= stones[i - 1])
//                    dp[j] = max(dp[j], dp[j - stones[i - 1]] + stones[i - 1]);
//            }
//        }
//        return sum - 2 * dp[aim];
//    }
//};