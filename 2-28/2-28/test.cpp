#include <iostream>
#include <vector>
using namespace std;

//class Solution {
//public:
//    void my_reverse(vector<int>& nums, int left, int right)
//    {
//        while (left < right)
//        {
//            int tmp = nums[left];
//            nums[left] = nums[right];
//            nums[right] = tmp;
//            ++left;
//            --right;
//        }
//    }
//    void rotate(vector<int>& nums, int k) {
//        int n = nums.size();
//        k %= n;   //这里k如果大于n那后面的计算就是没有意义的
//        my_reverse(nums, 0, n - k - 1);
//        my_reverse(nums, n - k, n - 1);
//        //整体逆置
//        my_reverse(nums, 0, n - 1);
//    }
//};


//class Solution {
//public:
//    int maxProfit(vector<int>& prices) {
//        //使用贪心的思想，每次找最低的价格买入，最高的价格卖出
//        int minprice = INT_MAX;
//        int profit = 0;
//        for (int i = 0; i < prices.size(); ++i)
//        {
//            //如果比最小的小那么就更替一下，后序可能有更大的利润
//            if (prices[i] < minprice) minprice = prices[i];
//            else profit = max(profit, prices[i] - minprice);
//        }
//        return profit;
//    }
//};


//class Solution {
//public:
//    int maxProfit(vector<int>& prices) {
//        int ret = 0; //最终的结果
//        //把每天都细分，把每一份都分到最小
//        for (int i = 1; i < prices.size(); ++i)
//        {
//            if (prices[i] > prices[i - 1])
//            {
//                ret += (prices[i] - prices[i - 1]);
//            }
//        }
//        return ret;
//    }
//};



//class Solution {
//public:
//    int maxProfit(vector<int>& prices) {
//        int minprice = prices[0];
//        int ret = 0;
//        for (int i = 1; i < prices.size(); ++i)
//        {
//            if (prices[i] < prices[i - 1])
//            {
//                //这里说明前面可能是有结果的
//                if (prices[i - 1] != minprice)
//                {
//                    ret += (prices[i - 1] - minprice);
//                }
//                //需要更新minprice
//                minprice = prices[i];
//            }
//        }
//        //这里最后可能是一直上升的
//        if (prices.back() != minprice) ret += (prices.back() - minprice);
//        return ret;
//    }
//};


//class Solution {
//public:
//    string minWindow(string s, string t) {
//        //通过使用两个hash来记录s和t中的字符
//        int hash1[128] = { 0 };
//        int start = 0;   //最短字串的起始下标
//        int minlen = INT_MAX;  //最短字串的长度
//        int kinds = 0;  //用来记录t中的字符的个数
//        //把t中的字符映射到hash中
//        for (auto ch : t)
//        {
//            if (hash1[ch]++ == 0) ++kinds;
//        }
//        int hash2[128] = { 0 };  //用来后序记录s
//        //这里的count是记录字符的种类
//        for (int left = 0, right = 0, count = 0; right < s.size(); ++right)
//        {
//            //进窗口
//            char in = s[right];
//            if (hash1[in] == ++hash2[in]) ++count;
//            //判断
//            while (kinds == count)
//            {
//                //收集结果
//                if (minlen > (right - left + 1))
//                {
//                    minlen = right - left + 1;
//                    start = left;
//                }
//                //出窗口
//                char out = s[left++];
//                if (hash2[out]-- == hash1[out]) --count;
//            }
//        }
//        if (minlen == INT_MAX) return "";
//        return s.substr(start, minlen);
//    }
//};


//class Solution {
//public:
//    vector<int> twoSum(vector<int>& nums, int target) {
//        //这里使用hash的思想，两数相加 -> 减法
//        unordered_map<int, int> hash;   //数和下标的映射关系
//        for (int i = 0; i < nums.size(); ++i)
//        {
//            if (hash.find(target - nums[i]) != hash.end())
//            {
//                return { i,hash[target - nums[i]] };
//            }
//            //这里没有找到可能是还没有进入到hash中，下一次就是反过来的结果了
//            hash[nums[i]] = i;
//        }
//        return { -1,-1 };  //这里一定没有结果了
//    }
//};


