#include <iostream>
using namespace std;


//class Person {
//public:
//	virtual void BuyTicket() { cout << "买票-全价" << endl; }
//};
//class Student : public Person {
//public:
//	virtual void BuyTicket() { cout << "买票-半价" << endl; }
//};
//void Func(Person& p)
//{
//	p.BuyTicket();
//}
//int main()
//{
//	Person p;
//	Func(p);
//	Student s;
//	s.BuyTicket();
//	return 0;
//}


//class Base {
//public:
//	virtual void func1() { cout << "Base::func1" << endl; }
//	virtual void func2() { cout << "Base::func2" << endl; }
//private:
//	int a;
//};
//class Derive :public Base {
//public:
//	virtual void func1() { cout << "Derive::func1" << endl; }
//	virtual void func3() { cout << "Derive::func3" << endl; }
//	virtual void func4() { cout << "Derive::func4" << endl; }
//private:
//	int b;
//};
//typedef void(*VFptr)();
//void PrintVTable(VFptr vf[])
//{
//	//利用虚表中的地址以空为结束
//	for (int i = 0; vf[i] != nullptr; ++i)
//	{
//		printf("第%d个地址为： %x  ", i, vf[i]);
//		vf[i]();
//	}
//	cout << endl;
//}
//int main()
//{
//	Base b;
//	Derive d;
//	VFptr* bf = (VFptr*)*((void**)&b);
//	PrintVTable(bf);
//	VFptr* df = (VFptr*)*((void**)&d);
//	PrintVTable(df);
//	return 0;
//}

//int main()
//{
//	int a = 0;
//	cout << "栈:" << &a << endl;
//
//	int* p1 = new int;
//	cout << "堆:" << p1 << endl;
//
//	const char* str = "hello world";
//	cout << "代码段/常量区:" << (void*)str << endl;
//
//	static int b = 0;
//	cout << "静态区/数据段:" << &b << endl;
//
//	Base be;
//	cout << "虚表:" << (void*)*((void**)&be) << endl;
//
//	Derive de;
//	cout << "虚表:" << (void*)*((void**)&de) << endl;
//
//	return 0;
//}


//class Base1 {
//public:
//	virtual void func1() { cout << "Base1::func1" << endl; }
//	virtual void func2() { cout << "Base1::func2" << endl; }
//private:
//	int b1;
//};
//class Base2 {
//public:
//	virtual void func1() { cout << "Base2::func1" << endl; }
//	virtual void func2() { cout << "Base2::func2" << endl; }
//private:
//	int b2;
//};
//class Derive : public Base1, public Base2 {
//public:
//	virtual void func1() { cout << "Derive::func1" << endl; }
//	virtual void func3() { cout << "Derive::func3" << endl; }
//private:
//	int d1;
//};
//typedef void(*VFPTR) ();
//void PrintVTable(VFPTR vTable[])
//{
//	cout << " 虚表地址>" << vTable << endl;
//	for (int i = 0; vTable[i] != nullptr; ++i)
//	{
//		printf(" 第%d个虚函数地址 :0X%x,->", i, vTable[i]);
//		vTable[i]();
//	}
//	cout << endl;
//}
//int main()
//{
//	Derive d;
//	VFPTR* vTableb1 = (VFPTR*)(*(void**)&d);
//	PrintVTable(vTableb1);
//	//因为这里要跳过一个base的大小才能得到虚表的指针
//	VFPTR* vTableb2 = (VFPTR*)(*(void**)((char*)&d + sizeof(Base1)));
//	PrintVTable(vTableb2);
//	return 0;
//}

//class A
//{
//public:
//	virtual void func(int val = 1) { std::cout << "A->" << val << std::endl; }
//	virtual void test() { func(); }
//};
//
//class B : public A
//{
//public:
//	void func(int val = 0) { std::cout << "B->" << val << std::endl; }
//};
//
//int main(int argc, char* argv[])
//{
//	B* p = new B;
//	p->test();
//	return 0;
//}

//class Car
//{
//public:
//	virtual void Drive() = 0;
//};
//int main()
//{
//	Car* a;
//	return 0;
//}


class A
{
public:
    A() :m_iVal(0) { test(); }
    virtual void func() { std::cout << m_iVal <<" "; }
    void test() { func(); }
public:
    int m_iVal;
};

class B : public A
{
public:
    B() { test(); }
    virtual void func()
    {
        ++m_iVal;
        std::cout << m_iVal << " ";
    }
};
int main()
{
    A* p = new B;
    p->test();
    return 0;
}