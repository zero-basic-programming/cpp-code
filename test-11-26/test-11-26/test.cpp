#include <iostream>
#include <vector>
using namespace std;


//class Solution {
//    vector<vector<int>> ret; //结果
//    vector<int> path; //路径
//    vector<bool> check; //检查元素是否有使用过
//public:
//    void dfs(vector<int>& nums)
//    {
//        //返回条件
//        if (nums.size() == path.size())
//        {
//            ret.push_back(path);
//            return;
//        }
//        //单层
//        for (int i = 0; i < nums.size(); ++i)
//        {
//            //剪枝
//            if (check[i]) continue;
//            path.push_back(nums[i]);
//            check[i] = true;
//            //深度递归
//            dfs(nums);
//            //回溯
//            path.pop_back();
//            check[i] = false;
//        }
//    }
//    vector<vector<int>> permute(vector<int>& nums) {
//        int n = nums.size();
//        check.resize(n);
//        dfs(nums);
//        return ret;
//    }
//};


//class Solution {
//public:
//    int monotoneIncreasingDigits(int n) {
//        string s = to_string(n);
//        int i = 0, m = s.size();
//        while (i + 1 < m && s[i] <= s[i + 1]) ++i;  //符合条件跳过
//        //这里可能本身就是有序的，特判一下
//        if (i + 1 == m) return n;
//        //往回找第一个
//        while (i - 1 >= 0 && s[i - 1] == s[i]) --i;
//        //把当前数字-1，并把后面的数字置为9
//        s[i]--;
//        for (int j = i + 1; j < m; ++j) s[j] = '9';
//        return stoi(s);
//    }
//};


//class Solution {
//    vector<vector<int>> ret; //收集结果
//    vector<int> path; //记录路径
//public:
//    vector<vector<int>> subsets(vector<int>& nums) {
//        int n = nums.size();
//        dfs(nums, 0);
//        return ret;
//    }
//    void dfs(vector<int>& nums, int m)
//    {
//        //每次进入都需要收集结果
//        ret.push_back(path);
//        //单层循环
//        for (int i = m; i < nums.size(); ++i)
//        {
//            path.push_back(nums[i]);
//            dfs(nums, i + 1);
//            //回溯
//            path.pop_back();
//        }
//    }
//};


//class Solution {
//    vector<vector<int>> ret; //收集结果
//    vector<int> path; //记录路径
//public:
//    vector<vector<int>> subsets(vector<int>& nums) {
//        dfs(nums, 0);
//        return ret;
//    }
//    void dfs(vector<int>& nums, int m)
//    {
//        //收集结果
//        if (m == nums.size()) {
//            ret.push_back(path);
//            return;
//        }
//        //选
//        path.push_back(nums[m]);
//        dfs(nums, m + 1);
//        path.pop_back();
//        //不选
//        dfs(nums, m + 1);
//    }
//};


