#include <iostream>
#include <vector>
using namespace std;

//class Solution {
//public:
//    vector<int> twoSum(vector<int>& nums, int target) {
//        unordered_map<int, int> hash; //元素和下标的映射关系
//        for (int i = 0; i < nums.size(); ++i)
//        {
//            //在hash中找到结果
//            auto it = hash.find(target - nums[i]);
//            if (it != hash.end())
//            {
//                //找到直接返回
//                return { hash[target - nums[i]],i };
//            }
//            //没找到，把当前元素放入hash中
//            hash[nums[i]] = i;
//        }
//        return {};
//    }
//};

//class Solution {
//public:
//    bool CheckPermutation(string s1, string s2) {
//        //创建hash数组，通过抵消的操作
//        char buf[26] = { 0 };
//        for (auto ch : s1) buf[ch - 'a']++;
//        for (auto ch : s2)
//        {
//            if (buf[ch - 'a'] == 0) return false;
//            --buf[ch - 'a'];
//        }
//        //遍历一次hash数组保证全部都是0
//        for (int i = 0; i < 26; ++i)
//        {
//            if (buf[i] != 0) return false;
//        }
//        return true;
//    }
//};

//class Solution {
//public:
//    bool CheckPermutation(string s1, string s2) {
//        if (s1.size() != s2.size()) return false;
//        //创建hash数组，通过抵消的操作
//        int buf[26] = { 0 };
//        for (auto ch : s1) buf[ch - 'a']++;
//        for (auto ch : s2)
//        {
//            --buf[ch - 'a'];
//            if (buf[ch - 'a'] < 0) return false;
//        }
//        return true;
//    }
//};


//class Solution {
//public:
//    bool containsDuplicate(vector<int>& nums) {
//        unordered_map<int, int> hash; //元素和次数映射关系
//        for (auto e : nums)
//        {
//            ++hash[e];
//            if (hash[e] >= 2) return true;
//        }
//        return false;
//    }
//};

//class Solution {
//public:
//    bool containsNearbyDuplicate(vector<int>& nums, int k) {
//        unordered_map<int, int> hash; //记录元素和下标的映射关系
//        for (int i = 0; i < nums.size(); ++i)
//        {
//            auto it = hash.find(nums[i]);
//            if (it != hash.end())
//            {
//                //判断条件是否满足
//                if (abs(hash[nums[i]] - i) <= k) return true;
//            }
//            //插入或者更新hash
//            hash[nums[i]] = i;
//        }
//        return false;
//    }
//};


