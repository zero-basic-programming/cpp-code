#include <iostream>
#include <vector>
using namespace std;

//class Solution {
//public:
//    int rob(vector<int>& nums) {
//        if (nums.size() == 1)    return nums[0];
//        if (nums.size() == 2)    return max(nums[0], nums[1]);
//        vector<int> dp(nums.size() + 1, 0);
//        //初始化
//        dp[0] = nums[0];
//        dp[1] = max(nums[0], nums[1]);
//        for (int i = 2; i < nums.size(); ++i) // 遍历现金
//        {
//            //状态转移方程
//            dp[i] = max(dp[i - 1], dp[i - 2] + nums[i]);
//        }
//        return dp[nums.size() - 1];
//    }
//};

//class Solution {
//public:
//    int find_rob(vector<int>& nums, int i, int j)
//    {
//        vector<int> dp(nums.size(), 0);
//        //初始化
//        dp[0] = nums[i];
//        dp[1] = max(nums[i], nums[i + 1]);
//        i += 2;
//        for (int k = 2; k < nums.size() -1; ++k)
//        {
//            dp[k] = max(dp[k - 1], dp[k - 2] + nums[i++]);
//        }
//        return dp[nums.size() - 2];
//    }
//    int rob(vector<int>& nums) {
//        if (nums.size() == 1)    return nums[0];
//        if (nums.size() == 2)    return max(nums[0], nums[1]);
//        //使用闭区间
//        int result1 = find_rob(nums, 0, nums.size() - 2);
//        int result2 = find_rob(nums, 1, nums.size() - 1);
//        return max(result1, result2);
//    }
//};
//
//int main()
//{
//    vector<int> nums = { 1,2,3,1 };
//    int x = Solution().rob(nums);
//    cout << x << endl;
//    return 0;
//}

//class Solution {
//public:
//    int find_rob(vector<int>& nums, int i, int j)
//    {
//        vector<int> dp(nums.size(), 0);
//        //初始化
//        dp[i] = nums[i];
//        dp[i + 1] = max(nums[i], nums[i + 1]);
//        for (int k = i + 2; k <= j; ++k)
//        {
//            dp[k] = max(dp[k - 1], dp[k - 2] + nums[k]);
//        }
//        return dp[j];
//    }
//    int rob(vector<int>& nums) {
//        if (nums.size() == 1)    return nums[0];
//        if (nums.size() == 2)    return max(nums[0], nums[1]);
//        //使用闭区间
//        int result1 = find_rob(nums, 0, nums.size() - 2);
//        int result2 = find_rob(nums, 1, nums.size() - 1);
//        return max(result1, result2);
//    }
//};

