#include <iostream>
#include <vector>
using namespace std;

//class Solution {
//public:
//    ListNode* reverseKGroup(ListNode* head, int k) {
//        if (head == nullptr || head->next == nullptr) return head;
//        //1.统计个数
//        ListNode* cur = head;
//        int n = 0;
//        while (cur)
//        {
//            ++n;
//            cur = cur->next;
//        }
//        n /= k;  //反转的次数
//        //2.每k个一组进行反转
//        ListNode* guard = new ListNode(0);
//        ListNode* prev = guard;
//        cur = head;
//        for (int i = 0; i < n; ++i)
//        {
//            ListNode* tmp = cur;
//            //每次进行链表的头插即可
//            for (int j = 0; j < k; ++j)
//            {
//                ListNode* next = cur->next;
//                cur->next = prev->next;
//                prev->next = cur;
//                //更新cur
//                cur = next;
//            }
//            prev = tmp;
//        }
//        //把后面的连接起来
//        prev->next = cur;
//        head = guard->next;
//        delete guard;
//        return head;
//    }
//};



//class Solution {
//public:
//    vector<int> twoSum(vector<int>& nums, int target) {
//        //使用hash
//        unordered_map<int, int> hash;  //建立数字和下标的映射关系
//        for (int i = 0; i < nums.size(); ++i)
//        {
//            auto it = hash.find(target - nums[i]);
//            if (it != hash.end())
//            {
//                //找到，返回
//                return { it->second,i };
//            }
//            //放入hash
//            hash[nums[i]] = i;
//        }
//        return { -1,-1 };
//    }
//};



//class Solution {
//public:
//    bool CheckPermutation(string s1, string s2) {
//        int hash[26] = { 0 };
//        //利用hash映射元素与个数的关系
//        for (auto ch : s1) hash[ch - 'a']++;
//        //遍历第2个s，去抵消
//        for (auto ch : s2)
//        {
//            if (--hash[ch - 'a'] < 0) return false;
//        }
//        //遍历hash，看是否都为0
//        for (auto e : hash) if (e > 0) return false;
//        return true;
//    }
//};


//class Solution {
//public:
//    bool containsDuplicate(vector<int>& nums) {
//        unordered_map<int, int> hash; //元素和次数映射关系
//        for (auto e : nums)
//        {
//            ++hash[e];
//            if (hash[e] >= 2) return true;
//        }
//        return false;
//    }
//};


//class Solution {
//public:
//    vector<vector<string>> groupAnagrams(vector<string>& strs) {
//        unordered_map<string, vector<string>> hash;  //建立排序后的字符串和分组数组的关系
//        for (auto& str : strs)
//        {
//            string tmp = str;
//            sort(tmp.begin(), tmp.end());
//            hash[tmp].push_back(str);
//        }
//        //在hash中收集结果
//        vector<vector<string>> ret;
//        for (auto& [x, y] : hash)
//        {
//            ret.push_back(y);
//        }
//        return ret;
//    }
//};


//class Solution {
//public:
//    string longestCommonPrefix(vector<string>& strs) {
//        int n = strs.size();
//        if (n == 1) return strs[0];
//        string ret;
//        for (int i = 0; i < strs[0].size() && i < strs[1].size(); ++i)
//        {
//            if (strs[0][i] == strs[1][i]) ret.push_back(strs[0][i]);
//            else break;
//        }
//        //后面进行追一比对
//        for (int i = 2; i < strs.size(); ++i)
//        {
//            if (strs[i].size() == 0) return "";
//            for (int j = 0; j < ret.size(); ++j)
//            {
//                if (j == strs[i].size())
//                {
//                    ret.erase(j);
//                    continue;
//                }
//                if (ret[j] != strs[i][j]) ret.erase(j);
//            }
//        }
//        return ret;
//    }
//};


//class Solution {
//public:
//    string longestCommonPrefix(vector<string>& strs) {
//        string ret = strs[0];
//        //两两进行比较
//        for (int i = 1; i < strs.size(); ++i)
//        {
//            ret = FindCommon(ret, strs[i]);
//        }
//        return ret;
//    }
//    string FindCommon(string& s1, string& s2)
//    {
//        int i = 0;
//        while (i < min(s1.size(), s2.size()) && s1[i] == s2[i]) ++i;
//        return s1.substr(0, i);
//    }
//};


//class Solution {
//public:
//    string longestPalindrome(string s) {
//        int n = s.size();
//        int len = 0;  //记录最长的长度
//        int begin = 0;
//        vector<vector<bool>> dp(n, vector<bool>(n, false));
//        for (int i = n - 1; i >= 0; --i)
//        {
//            for (int j = 0; j < n; ++j)
//            {
//                if (s[i] == s[j])
//                {
//                    dp[i][j] = i + 1 < j ? dp[i + 1][j - 1] : true;
//                }
//                //统计结果
//                if (dp[i][j] && j - i + 1 > len)
//                {
//                    len = j - i + 1;
//                    begin = i;
//                }
//            }
//        }
//        return s.substr(begin, len);
//    }
//};



//class Solution {
//public:
//    string longestPalindrome(string s) {
//        //中心扩展算法
//        int n = s.size();
//        int len = 0;  //记录最长
//        int begin = 0;  //记录起始下标
//        for (int i = 0; i < n; ++i)
//        {
//            //奇数个
//            int left = i, right = i;
//            while (left >= 0 && right < n && s[left] == s[right])
//            {
//                --left;
//                ++right;
//            }
//            if (len < right - left - 1)
//            {
//                len = right - left - 1;
//                begin = left + 1;
//            }
//            //统计偶数个
//            left = i, right = i + 1;
//            while (left >= 0 && right < n && s[left] == s[right])
//            {
//                --left;
//                ++right;
//            }
//            if (len < right - left - 1)
//            {
//                len = right - left - 1;
//                begin = left + 1;
//            }
//        }
//        return s.substr(begin, len);
//    }
//};


//class Solution {
//public:
//    string addBinary(string a, string b) {
//        string ret;
//        int i = a.size() - 1, j = b.size() - 1;
//        int carry = 0; //进位
//        while (i >= 0 || j >= 0)
//        {
//            char atmp = '0', btmp = '0';
//            if (i >= 0) atmp = a[i];
//            if (j >= 0) btmp = b[j];
//            int sum = (atmp + btmp - 2 * '0') + carry;
//            carry = sum / 2;
//            ret.push_back((sum % 2) + '0');
//            --i;
//            --j;
//        }
//        //处理最后一个进位
//        if (carry == 1) ret += '1';
//        reverse(ret.begin(), ret.end());
//        return ret;
//    }
//};
//
//
//int main()
//{
//    Solution().addBinary("11", "1");
//    return 0;
//}


//class Solution {
//public:
//    string addBinary(string a, string b) {
//        int i = a.size() - 1, j = b.size() - 1, t = 0;
//        string ret;
//        while (i >= 0 || j >= 0 || t)
//        {
//            if (i >= 0) t += a[i--] - '0';
//            if (j >= 0) t += b[j--] - '0';
//            ret += t % 2 + '0';
//            t /= 2;
//        }
//        reverse(ret.begin(), ret.end());
//        return ret;
//    }
//};


//class Solution {
//public:
//    string multiply(string num1, string num2) {
//        //1.逆置1，2
//        reverse(num1.begin(), num1.end());
//        reverse(num2.begin(), num2.end());
//        //2.处理无进位相加
//        int sz1 = num1.size();
//        int sz2 = num2.size();
//        vector<int> tmp(sz1 + sz2 - 1);  //用来处理无进位相加
//        for (int i = 0; i < sz1; ++i)
//        {
//            for (int j = 0; j < sz2; ++j)
//            {
//                tmp[i + j] += (num1[i] - '0') * (num2[j] - '0');
//            }
//        }
//        //3.处理进位
//        string ret;
//        int carry = 0;
//        for (int i = 0; i < tmp.size(); ++i)
//        {
//            int res = tmp[i] + carry;
//            carry = res / 10;
//            ret += res % 10 + '0';
//        }
//        if (carry > 0) ret += carry + '0';
//        //4.处理前导0
//        while (ret.size() > 1 && ret.back() == '0') ret.pop_back();
//        //5.逆置结果
//        reverse(ret.begin(), ret.end());
//        return ret;
//    }
//};


