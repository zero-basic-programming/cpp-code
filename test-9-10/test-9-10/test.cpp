#include <iostream>
#include <vector>
using namespace std;


//class Solution {
//public:
//    bool isUnique(string astr) {
//        //利用鸽槽原理优化
//        if (astr.size() > 26) return false;
//        //因为只有26个字母，所以这里只需要使用位图即可解决这个问题
//        int bitMap = 0;
//        for (auto ch : astr)
//        {
//            int i = ch - 'a';
//            if (((bitMap >> i) & 1) == 1)
//            {
//                //说明已经出现过了
//                return false;
//            }
//            bitMap |= 1 << i;
//        }
//        return true;
//    }
//};



//class Solution {
//public:
//    int getSum(int a, int b) {
//        //i表示无进位相加的结果
//        //j表示进位相加的结果
//        int i = a ^ b;
//        int j = (a & b) << 1;
//        while (j)
//        {
//            int tmp = i;
//            i = i ^ j;
//            j = (tmp & j) << 1;
//        }
//        return i;
//    }
//};



 /*class Solution {
 public:
     vector<int> countBits(int n) {
         vector<int> ret(n+1);
         for(int i = 0;i<=n;++i)
         {
             int count = 0;
             while(i)
             {
                 i &= (i-1);
                 ++count;
             }
             ret[i] = count;
         }
         return ret;
     }
 };*/
// 
//class Solution {
//public:
//    int countOnes(int x) {
//        int ones = 0;
//        while (x > 0) {
//            x &= (x - 1);
//            ones++;
//        }
//        return ones;
//    }
//
//    vector<int> countBits(int n) {
//        vector<int> bits(n + 1);
//        for (int i = 0; i <= n; i++) {
//            bits[i] = countOnes(i);
//        }
//        return bits;
//    }
//};
//// 
//int main()
//{
//    Solution().countBits(2);
//	return 0;
//}

//


//class Solution {
//public:
//    int hammingDistance(int x, int y) {
//        //异或完之后找1即可
//        int ret = x ^ y;
//        int count = 0;
//        while (ret)
//        {
//            ret &= (ret - 1);
//            ++count;
//        }
//        return count;
//    }
//};


//class Solution {
//public:
//    int singleNumber(vector<int>& nums) {
//        int ret = 0;
//        for (auto e : nums) ret ^= e;
//        return ret;
//    }
//};

//class Solution {
//public:
//    int lengthOfLIS(vector<int>& nums) {
//        int n = nums.size();
//        vector<int> dp(n, 1);
//        int ret = 1;
//        for (int i = 1; i < n; ++i)
//        {
//            for (int j = i - 1; j >= 0; --j)
//            {
//                if (nums[j] < nums[i])
//                {
//                    dp[i] = max(dp[i], dp[j] + 1);
//                }
//            }
//            ret = max(ret, dp[i]);
//        }
//        return ret;
//    }
//};


