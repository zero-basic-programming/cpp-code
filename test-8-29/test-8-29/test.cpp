#include <iostream>
#include <vector>
using namespace std;

//int main() {
//    int n, q;
//    cin >> n >> q;
//    vector<int> nums(n + 1);
//    for (int i = 1; i <= n; ++i) cin >> nums[i];
//    vector<long long> dp(n + 1); // 防止数据溢出
//    //统计出1到i为止的数的和
//    for (int i = 1; i <= n; ++i) {
//        dp[i] = dp[i - 1] + nums[i];
//    }
//    int l, r;
//    while (q--) {
//        cin >> l >> r;
//        cout << dp[r] - dp[l - 1] << endl;
//    }
//    return 0;
//}


#include <iostream>
#include <vector>
using namespace std;

int main() {
    int n, m, q;
    cin >> n >> m >> q;
    vector<vector<int>> nums(n + 1, vector<int>(m + 1, 0));
    for (int i = 1; i <= n; ++i)
    {
        for (int j = 1; j <= m; ++j)
        {
            cin >> nums[i][j];
        }
    }
    vector<vector<long long>> dp(n + 1, vector<long long>(m + 1, 0));
    //统计不同子矩阵的数之和
    for (int i = 1; i <= n; ++i)
    {
        for (int j = 1; j <= m; ++j)
        {
            dp[i][j] = dp[i - 1][j] + dp[i][j - 1] + nums[i][j] - dp[i - 1][j - 1];
        }
    }
    int x1, y1, x2, y2;
    while (q--)
    {
        cin >> x1 >> x2 >> y1 >> y2;
        cout << dp[x2][y2] - dp[x1 - 1][y2] - dp[x2][y1 - 1] + dp[x1 - 1][y1 - 1] << endl;
    }
    return 0;
}