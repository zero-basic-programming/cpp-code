#include <iostream>
#include <vector>
using namespace std;


//class Solution {
//public:
//    int minimumDeleteSum(string s1, string s2) {
//        int m = s1.size(), n = s2.size();
//        vector<vector<int>> dp(m + 1, vector<int>(n + 1));
//        for (int i = 1; i <= m; ++i)
//        {
//            for (int j = 1; j <= n; ++j)
//            {
//                dp[i][j] = max(dp[i - 1][j], dp[i][j - 1]);
//                if (s1[i - 1] == s2[j - 1]) dp[i][j] = max(dp[i][j], dp[i - 1][j - 1] + s1[i - 1]);
//            }
//        }
//        int sum = 0;
//        for (auto ch : s1) sum += ch;
//        for (auto ch : s2) sum += ch;
//        return sum - dp[m][n] * 2;
//    }
//};



//class Solution {
//public:
//    int findLength(vector<int>& nums1, vector<int>& nums2) {
//        int m = nums1.size(), n = nums2.size();
//        vector<vector<int>> dp(m + 1, vector<int>(n + 1));
//        int ret = 0;
//        for (int i = 1; i <= m; ++i)
//        {
//            for (int j = 1; j <= n; ++j)
//            {
//                if (nums1[i - 1] == nums2[j - 1])
//                    dp[i][j] = dp[i - 1][j - 1] + 1;
//                ret = max(ret, dp[i][j]);
//            }
//        }
//        return ret;
//    }
//};



//int main() {
//    int n, V;
//    cin >> n >> V;
//    vector<int> v(n + 1);
//    vector<int> w(n + 1);
//    for (int i = 1; i <= n; ++i)
//    {
//        cin >> v[i] >> w[i];
//    }
//    vector<vector<int>> dp(n + 1, vector<int>(V + 1));
//    for (int i = 1; i <= n; ++i)  //遍历物品
//    {
//        for (int j = 0; j <= V; ++j)  //遍历背包
//        {
//            dp[i][j] = dp[i - 1][j];  //不选当前第i个商品
//            if (j >= v[i])
//            {
//                dp[i][j] = max(dp[i][j], dp[i - 1][j - v[i]] + w[i]);
//            }
//        }
//    }
//    cout << dp[n][V] << endl;
//    //第二题
//    //当物品为0时，背包一定不可能被填满
//    for (int i = 1; i <= V; ++i) dp[0][i] = -1;
//    for (int i = 1; i <= n; ++i)
//    {
//        for (int j = 0; j <= V; ++j)
//        {
//            dp[i][j] = dp[i - 1][j];
//            if (j >= v[i] && dp[i - 1][j - v[i]] != -1)
//            {
//                dp[i][j] = max(dp[i][j], dp[i - 1][j - v[i]] + w[i]);
//            }
//        }
//    }
//    cout << (dp[n][V] == -1 ? 0 : dp[n][V]) << endl;
//    return 0;
//}



////滚动数组优化
//int main() {
//    int n, V;
//    cin >> n >> V;
//    vector<int> v(n + 1);
//    vector<int> w(n + 1);
//    for (int i = 1; i <= n; ++i)
//    {
//        cin >> v[i] >> w[i];
//    }
//    vector<int> dp(V + 1);
//    for (int i = 1; i <= n; ++i)  //遍历物品
//    {
//        for (int j = V; j >= v[i]; --j)  //遍历背包
//            dp[j] = max(dp[j], dp[j - v[i]] + w[i]);
//    }
//    cout << dp[V] << endl;
//    //第二题
//    //当物品为0时，背包一定不可能被填满
//    for (int i = 1; i <= V; ++i) dp[i] = -1;
//    for (int i = 1; i <= n; ++i)
//    {
//        for (int j = V; j >= v[i]; --j)
//        {
//            if (dp[j - v[i]] != -1)
//            {
//                dp[j] = max(dp[j], dp[j - v[i]] + w[i]);
//            }
//        }
//    }
//    cout << (dp[V] == -1 ? 0 : dp[V]) << endl;
//    return 0;
//}

//
//class Solution {
//public:
//    bool canPartition(vector<int>& nums) {
//        int n = nums.size();
//        int aim = 0;
//        for (auto e : nums) aim += e;
//        if (aim % 2 == 1) return false;   //奇数的情况一定不行
//        aim /= 2;
//        vector<vector<bool>> dp(n + 1, vector<bool>(aim + 1, false));
//        //初始化
//        dp[0][0] = true;
//        for (int i = 1; i <= n; ++i)
//        {
//            for (int j = 0; j <= aim; ++j)
//            {
//                dp[i][j] = dp[i - 1][j];
//                if (j >= nums[i - 1])
//                    dp[i][j] = dp[i][j] || dp[i - 1][j - nums[i - 1]];
//            }
//        }
//        return dp[n][aim];
//    }
//};


//class Solution {
//public:
//    bool canPartition(vector<int>& nums) {
//        int n = nums.size();
//        int aim = 0;
//        for (auto e : nums) aim += e;
//        if (aim % 2 == 1) return false;   //奇数的情况一定不行
//        aim /= 2;
//        vector<bool> dp(aim + 1, false);
//        //初始化
//        dp[0] = true;
//        for (int i = 1; i <= n; ++i)
//        {
//            for (int j = aim; j >= nums[i - 1]; --j)
//            {
//                dp[j] = dp[j] || dp[j - nums[i - 1]];
//            }
//        }
//        return dp[aim];
//    }
//};


//class Solution {
//    int ret = 0;
//public:
//    int findTargetSumWays(vector<int>& nums, int target) {
//        dfs(nums, target, 0, 0);
//        return ret;
//    }
//    void dfs(vector<int>& nums, int target, int sum, int i)
//    {
//        //返回
//        if (i == nums.size())
//        {
//            if (sum == target)   ++ret;
//            return; //没有结果
//        }
//        dfs(nums, target, sum + nums[i], i + 1);
//        dfs(nums, target, sum - nums[i], i + 1);
//    }
//};


//class Solution {
//public:
//    int findTargetSumWays(vector<int>& nums, int target) {
//        int n = nums.size();
//        int sum = 0;
//        for (auto e : nums) sum += e;
//        int v = (sum + target) / 2;  //在数组中找到部分和为v
//        //处理边界条件
//        if (v < 0 || (sum + target) % 2) return 0;
//        vector<vector<int>> dp(n + 1, vector<int>(v + 1));
//        dp[0][0] = 1;
//        for (int i = 1; i <= n; ++i)
//        {
//            for (int j = 0; j <= v; ++j)
//            {
//                dp[i][j] = dp[i - 1][j];
//                if (j >= nums[i - 1]) dp[i][j] += dp[i - 1][j - nums[i - 1]];
//            }
//        }
//        return dp[n][v];
//    }
//};


//class Solution {
//public:
//    int lastStoneWeightII(vector<int>& stones) {
//        int n = stones.size();
//        int sum = 0;
//        for (auto e : stones) sum += e;
//        int aim = sum / 2;
//        vector<vector<int>> dp(n + 1, vector<int>(aim + 1));
//        for (int i = 1; i <= n; ++i)
//        {
//            for (int j = 0; j <= aim; ++j)
//            {
//                dp[i][j] = dp[i - 1][j];
//                if (j >= stones[i - 1])
//                    dp[i][j] = max(dp[i][j], dp[i - 1][j - stones[i - 1]] + stones[i - 1]);
//            }
//        }
//        return sum - 2 * dp[n][aim];
//    }
//};


//class Solution {
//public:
//    int lastStoneWeightII(vector<int>& stones) {
//        int n = stones.size();
//        int sum = 0;
//        for (auto e : stones) sum += e;
//        int aim = sum / 2;
//        vector<int> dp(aim + 1);
//        for (int i = 1; i <= n; ++i)
//        {
//            for (int j = aim; j >= stones[i - 1]; --j)
//            {
//                dp[j] = max(dp[j], dp[j - stones[i - 1]] + stones[i - 1]);
//            }
//        }
//        return sum - 2 * dp[aim];
//    }
//};


