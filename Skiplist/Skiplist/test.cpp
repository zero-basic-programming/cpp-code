#include <iostream>
#include <vector>
#include <time.h>
using namespace std;


#include <stdlib.h>
class Node
{
public:
    int _val; //数据
    vector<Node*> _VNode;
    Node(int val, int level) :_val(val), _VNode(level, nullptr)
    {}
};

class Skiplist {
public:
    Skiplist() {
        srand(time(nullptr));  //生成随机数
        //创建头节点
        _head = new Node(-1, 1);
    }

    bool search(int target) {
        int level = _head->_VNode.size() - 1;
        Node* cur = _head;
        while (level--)
        {
            if (cur->_VNode[level] == nullptr || cur->_VNode[level]->_val > target)
            {
                //向下走
                --level;
            }
            else if (cur->_VNode[level] && cur->_VNode[level]->_val < target)
            {
                //向右走
                cur = cur->_VNode[level];
            }
            else return true; //找到了
        }
        return false;
    }
    vector<Node*> FindPrevNode(int target)
    {
        int level = _head->_VNode.size() - 1;
        vector<Node*> ret(level + 1, nullptr);
        Node* cur = _head;
        while (level >= 0)
        {
            //只有向下走的时候才需要记录他上一个节点
            //这里相等也要向下走，因为这里需要获取到全部的前面的节点
            if (cur->_VNode[level] == nullptr || cur->_VNode[level]->_val >= target)
            {
                //向下走
                ret[level--] = cur;
            }
            else if (cur->_VNode[level] && cur->_VNode[level]->_val < target)
            {
                //向右走
                cur = cur->_VNode[level];
            }
        }
        return ret;
    }
    int randomNum()
    {
        int level = 1;
        while (rand() <= RAND_MAX * _p && level < _maxlevels)
        {
            ++level;
        }
        return level;
    }
    void add(int num) {
        //找到上一个的节点,然后连接起来即可
        vector<Node*> Prev = FindPrevNode(num);
        int n = randomNum();
        Node* newnode = new Node(num, n);
        if (n > Prev.size())
        {
            _head->_VNode.resize(n, nullptr);
            Prev.resize(n, _head);
        }
        //连接
        for (int i = 0; i < n; ++i)
        {
            newnode->_VNode[i] = Prev[i]->_VNode[i];
            Prev[i]->_VNode[i] = newnode;
        }
    }

    bool erase(int num) {
        //找到那个数字，然后改变连接后删除
        vector<Node*> Prev = FindPrevNode(num);
        int level = Prev.size();
        //判定有可能改数字不存在
        if (Prev[0]->_VNode[0] == nullptr || Prev[0]->_VNode[0]->_val != num) return false;
        else
        {
            Node* del = Prev[0]->_VNode[0];
            for (int i = 0; i < level; ++i)
            {
                Prev[i]->_VNode[i] = del->_VNode[i];
            }
            delete del;
            //这里删除节点之后可以把头节点的高度也可以尝试降下来
            int i = _head->_VNode.size();
            while (i >= 0)
            {
                if (_head->_VNode[i] == nullptr) --i;
                else break;
            }
            _head->_VNode.resize(i + 1);
            return true;
        }
        void Print()
        {
            Node* cur = _head;
            while (cur)
            {
                printf("%2d\n", cur->_val);
                // 打印每个每个cur节点
                for (auto e : cur->_VNode)
                {
                    printf("%2s", "↓");
                }
                printf("\n");

                cur = cur->_nextV[0];
            }
        }
    }
private:
    Node* _head;
    int _maxlevels = 32;
    double _p = 0.5;
};

int main()
{
    Skiplist sl;
    //int a[] = { 5, 2, 3, 8, 9, 6, 5, 2, 3, 8, 9, 6, 5, 2, 3, 8, 9, 6 };
    int a[] = { 5, 2, 3, 8, 9, 6 };
    for (auto e : a)
    {
        //sl.Print();
        //printf("--------------------------------------\n");

        sl.add(e);
    }
    sl.Print();

    return 0;
}