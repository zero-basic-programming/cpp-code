#include <iostream>
#include <vector>
using namespace std;


//class CQueue {
//private:
//    queue<int> q;
//public:
//    CQueue() {
//
//    }
//
//    void appendTail(int value) {
//        q.push(value);
//    }
//
//    int deleteHead() {
//        if (q.size() == 0) return -1;
//        int ret = q.front();
//        q.pop();
//        return ret;
//    }
//};

#include <stack>

//class CQueue {
//private:
//    //使用两个栈来模拟队列
//    stack<int> instack;  //用来还书
//    stack<int> outstack;  //用来借书
//public:
//    void appendTail(int value) {
//        instack.push(value);
//    }
//
//    int deleteHead() {
//        //首先看有没有outstack中有没有新的书
//        if (outstack.size())
//        {
//            int ret = outstack.top();
//            outstack.pop();
//            return ret;
//        }
//        else
//        {
//            //把instack中的书全部放入到instack中
//            while (instack.size())
//            {
//                outstack.push(instack.top());
//                instack.pop();
//            }
//            int ret = outstack.empty() ? -1 : outstack.top();
//            outstack.pop();
//            return ret;
//        }
//    }
//};
//
//
//int main()
//{
//    CQueue q;
//    q.appendTail(1);
//    q.appendTail(2);
//    int ret = q.deleteHead();
//    cout << ret;
//    return 0;
//}


//class Solution {
//public:
//    const int N = 1e9 + 7;
//    int fib(int n) {
//        if (n == 0 || n == 1) return n;
//        int first = 0;
//        int second = 1;
//        int third = 1;
//        for (int i = 2; i <= n; ++i)
//        {
//            third = (first + second) % N;
//            //迭代
//            first = second;
//            second = third;
//        }
//        return third;
//    }
//};


//class Solution {
//public:
//    const int N = 1e+7;
//    int fib(int n) {
//        //动态规划，数组版本
//        if (n == 0 || n == 1) return n;
//        vector<int> dp(n + 1);
//        //初始化
//        dp[1] = 1;
//        for (int i = 2; i <= n; ++i)
//        {
//            //状态转移方程
//            dp[i] = (dp[i - 1] + dp[i - 2]) % N;
//        }
//        return dp[n];
//    }
//};


