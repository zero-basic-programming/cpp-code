#include <iostream>
#include <vector>
using namespace std;


//class Solution {
//public:
//    string addBinary(string a, string b) {
//        int an = a.size() - 1;
//        int bn = b.size() - 1;
//        int t = 0; //进位
//        string ret;
//        while (an >= 0 || bn >= 0 || t)
//        {
//            if (an >= 0) t += a[an--] - '0';
//            if (bn >= 0) t += b[bn--] - '0';
//            ret += (t % 2) + '0';
//            t /= 2;
//        }
//        reverse(ret.begin(), ret.end());
//        return ret;
//    }
//};


//class Solution {
//public:
//    string longestPalindrome(string s) {
//        int n = s.size();
//        int len = 0;
//        int begin = 0;
//        vector<vector<bool>> dp(n, vector<bool>(n));
//        for (int i = n - 1; i >= 0; --i)
//        {
//            for (int j = i; j < n; ++j)
//            {
//                if (s[i] == s[j])
//                    dp[i][j] = (i + 1 < j) ? dp[i + 1][j - 1] : true;
//                if (dp[i][j] && (j - i + 1 > len))
//                {
//                    len = j - i + 1;
//                    begin = i;
//                }
//            }
//        }
//        return s.substr(begin, len);
//    }
//};


class Solution {
public:
    bool checkPartitioning(string s) {
        //1.利用动态规划求出所有的非空字符串
        int n = s.size();
        vector<vector<bool>> dp(n, vector<bool>(n));
        for (int i = n - 1; i >= 0; --i)
        {
            for (int j = i; j < n; ++j)
            {
                if (s[i] == s[j])
                    dp[i][j] = (i + 1 < j) ? dp[i + 1][j - 1] : true;
            }
        }
        //2.从中找到3个能拼接的结果
        for (int left = 1; left < n - 1; ++left)
        {
            for (int right = left; right < n - 1; ++right)
            {
                //[0,left-1][left,right][right+1,n-1]
                if (dp[0][left - 1] && dp[left][right] && dp[right + 1][n - 1])
                    return true;
            }
        }
        return false;
    }
};