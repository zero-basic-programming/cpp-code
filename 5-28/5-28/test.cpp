#include <iostream>
#include <vector>
using namespace std;


//class Solution {
//public:
//    ListNode* getIntersectionNode(ListNode* headA, ListNode* headB) {
//        //找到a和b两个链表的大小
//        int asz = 0, bsz = 0;
//        ListNode* a = headA, * b = headB;
//        while (a)
//        {
//            ++asz;
//            a = a->next;
//        }
//        while (b)
//        {
//            ++bsz;
//            b = b->next;
//        }
//        ListNode* longlist = nullptr, * shortlist = nullptr;
//        if (asz > bsz)
//        {
//            longlist = headA;
//            shortlist = headB;
//        }
//        else
//        {
//            longlist = headB;
//            shortlist = headA;
//        }
//        //通过大的链表减去差值然后用前后指针的方式求解
//        int gap = abs(asz - bsz);
//        while (gap--)
//        {
//            longlist = longlist->next;
//        }
//        while (longlist)
//        {
//            if (longlist == shortlist) return longlist;
//            longlist = longlist->next;
//            shortlist = shortlist->next;
//        }
//        return nullptr;
//    }
//};


//class Solution {
//public:
//    ListNode* getIntersectionNode(ListNode* headA, ListNode* headB) {
//        //如果两个链表是重合的，那么一定存在当两个链表都走一遍的时候会遇到交点
//        ListNode* a = headA, * b = headB;
//        while (a != b)
//        {
//            a = a != nullptr ? a->next : headB;
//            b = b != nullptr ? b->next : headA;
//        }
//        return a;
//    }
//};


//class Solution {
//public:
//    int countTarget(vector<int>& scores, int target) {
//        int n = scores.size();
//        if (n == 0) return 0;
//        int left = 0, right = n - 1;
//        //找左边界
//        while (left < right)
//        {
//            int mid = left + (right - left) / 2;
//            if (scores[mid] < target) left = mid + 1;
//            else right = mid;
//        }
//        //这里可能没有找到
//        if (scores[left] != target) return 0;
//        int leftborder = left;
//        //找右边界
//        left = 0, right = n - 1;
//        while (left < right)
//        {
//            int mid = left + (right - left + 1) / 2;
//            if (scores[mid] > target) right = mid - 1;
//            else left = mid;
//        }
//        return left - leftborder + 1;
//    }
//};


